﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Microsoft.HockeyApp;
using Windows.Storage;
using System.Xml;
using System.IO;

namespace SharpHmi
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        public App()
        {
            Microsoft.HockeyApp.HockeyClient.Current.Configure("b4334df912d442af924fff73156d89eb");
            InitializeComponent();
            Suspending += OnSuspending;
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
            startStopUILogCapture(false,true);
        }

        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
        private void startStopUILogCapture(Boolean prevUILoggingEnabled, Boolean isUILoggingEnabled)
        {
            if (!prevUILoggingEnabled && isUILoggingEnabled)
            {
              //  createNewFile();
              /*  if (sfileName != null)
                {
                    this.appSetting.setLogFileName(sfileName);
                    _msgAdapter.startStopLogging(prevUILoggingEnabled, isUILoggingEnabled, this.appSetting.getLogFileName());
                }
                else
                {
                    appSetting.setUILoggingEnabled(!isUILoggingEnabled); // Rolling back to previous state of UIlogging since there is issue in file creation.
                    Toast.makeText(getActivity(), "File not created.Check SD Card presence.", Toast.LENGTH_LONG).show();
                }*/
            }
        }
        
        }
}
