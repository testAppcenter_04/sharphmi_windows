﻿using SharpHmi.src.MainScreen;
using SharpHmi.src.Models;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using HmiApiLib.Base;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;
using Windows.ApplicationModel.Core;

namespace SharpHmi
{
    public sealed partial class AppsPage : Page
    {
        public Frame parentFrame;
        String selectedAppName = null;
        public static Boolean isfullHMI = false;
        public static TrulyObservableCollection<AppItem> appList = new TrulyObservableCollection<AppItem>();
        HmiApiLib.Controllers.BasicCommunication.IncomingRequests.ActivateApp activateAppRpc;

        public TrulyObservableCollection<AppItem> appsList { get { return appList; } }
        AppInstanceManager instance;

        public AppsPage()
        {
            this.InitializeComponent();

            AppsPageAppListView.ItemsSource = appList;

            if (instance == null)
                instance = AppInstanceManager.AppInstance;

            isfullHMI = false;
        }

        public static void addItem(AppItem appItem)
        {
            appList.Add(appItem);
        }

        public static void removeItem(AppItem appItem)
        {
            appList.Remove(appItem);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.Parameter != null)
            {
                activateAppRpc = (HmiApiLib.Controllers.BasicCommunication.IncomingRequests.ActivateApp)e.Parameter;
                AppList();
            }                

            var frame = this.Frame;

            if (frame != null && frame is Frame)
                AppInstanceManager.insideFrame = frame;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            activateAppRpc = null;
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AppList();
        }

        private bool isAppMedia(List<AppHMIType> appHMITypeList, HmiApiLib.Common.Structs.HMIApplication hmiApplication)
        {
            if (appHMITypeList == null)
                return false;

            if (hmiApplication == null)
                return false;

            if (hmiApplication.getIsMediaApplication() == null)
                return false;

            if ((bool)hmiApplication.getIsMediaApplication())
                return true;

            return false;
        }


        private bool isAppProjected(List<AppHMIType> appHMITypeList)
        {
            if (appHMITypeList == null)
                return false;

            if ((appHMITypeList.Contains(AppHMIType.NAVIGATION)) || (appHMITypeList.Contains(AppHMIType.PROJECTION)))
                return true;

            return false;
        }

        public async void AppList()
        {
            int? selectedAppID = null;
            List<HmiApiLib.Common.Enums.AppHMIType> appHMITypeList = null;
            int index = AppsPageAppListView.SelectedIndex;

            AppItem myAppItem = null;

            if (instance.appList.Count > 0)
            {
                if (activateAppRpc == null)
                {
                    if (index == -1) return;
                    AppUtils.saveAppIdInstanceIndex(index);
                    myAppItem = appList[index];
                }
                else
                {
                    myAppItem = instance.getAppItem(activateAppRpc.getAppId());
                    if (myAppItem == null) return;
                }

                selectedAppID = myAppItem.getApplication().getAppID();
                selectedAppName = myAppItem.getApplication().getAppName();
                HmiApiLib.Common.Structs.HMIApplication hmiApplication = myAppItem.getApplication();
                appHMITypeList = hmiApplication.appType;

                if (appHMITypeList == null)
                {
                    List<AppHMIType> appHMITypes = new List<AppHMIType>();
                    appHMITypes.Add(AppHMIType.DEFAULT);
                    hmiApplication.appType = appHMITypes;
                    appHMITypeList = hmiApplication.appType;
                }

                RpcRequest rpcMessage = BuildRpc.buildSdlActivateAppRequest(BuildRpc.getNextId(), selectedAppID);
                AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
                instance.sendRpc(rpcMessage);
                isfullHMI = true;

                if ( isAppProjected(appHMITypeList) )
                {
                    if (instance != null && instance.refMainPage != null)
                        instance.refMainPage.mediaProjection.IsSelected = true;
                    try
                    {
                        var parameters = (AppItem)appList[index];
                        AppInstanceManager.activatedAppId = (int)parameters.getApplication().getAppID();
                        AppInstanceManager.lastProjectedAppId = (int)parameters.getApplication().getAppID();
                        AppInstanceManager.activatedAppName = parameters.getApplication().getAppName();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        Frame.Navigate(typeof(ProjectionTemplate));
                    });
                }
                else if (isAppMedia(appHMITypeList, myAppItem.getApplication()))
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        Frame.Navigate(typeof(MediaTemplate), myAppItem);
                    });
                }
                else
                {
                    //todo display the default non-media template after its created.
                    //Currently its handled in MediaTemplate Class only and can be handled separately.
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        Frame.Navigate(typeof(MediaTemplate), myAppItem);
                    });
                }
            }
        }
    }
}
