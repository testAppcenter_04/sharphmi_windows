﻿using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.UI.IncomingRequests;
using Windows.UI.Xaml;

namespace SharpHmi.src.Models
{
    public class MediaClock
    {
        private SetMediaClockTimer mediaClockTimer = null;
        private int? curTime = null;
        private ClockUpdateMode? lastClockUpdateMode = null;

        public void setLastClockUpdateMode(ClockUpdateMode clockUpdateMode)
        {
            this.lastClockUpdateMode = clockUpdateMode;
        }
        public ClockUpdateMode? getLastClockUpdateMode()
        {
            return lastClockUpdateMode;
        }

        public void setCurTime(int? curTime)
        {
            this.curTime = curTime;
        }
        public int? getCurTime()
        {
            return curTime;
        }

        public SetMediaClockTimer getMediaClockTimer()
        {
            return mediaClockTimer;
        }
        public void setMediaClockTimer(SetMediaClockTimer mediaClockTimer)
        {
            this.mediaClockTimer = mediaClockTimer;
            setCurTime(null);
        }

        public MediaClock()
        {
        }
    }
}
