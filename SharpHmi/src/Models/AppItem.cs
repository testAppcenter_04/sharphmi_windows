﻿
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;
using Windows.UI.Xaml.Media.Imaging;
using HmiApiLib.Controllers.UI.IncomingRequests;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using System.Collections.ObjectModel;
using SharpHmi.src.Utility;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using SharpHmi.src.CachedRPC;
using SharpHmi.src.Manager;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SharpHmi.src.Models
{
    public class AppItem : INotifyPropertyChanged
    {
        public GraphicManager graphicManager = null;
        private CachedShow cachedShow = null;


        private BitmapImage appIcon;
        private string appName;

        private string appIconPath;
        private OnAppRegistered onAppRegistered;
        private Show showRpc = null;
        private ObservableCollection<OnButtonSubscription> buttonSubscriptions = new ObservableCollection<OnButtonSubscription>();
        private SetDisplayLayout displayLayout = null;
        private SetGlobalProperties globalProperties = null;
        private PerformAudioPassThru performAudioPassThru = null;
        private SetAppIcon setAppIcon = null;
        private MediaClock mediaClock = null;
        private List<RpcRequest> menuCommandsList = new List<RpcRequest>();
        private Dictionary<int, List<AddCommand>> subMenuCommandsList = new Dictionary<int, List<AddCommand>>();

        public event PropertyChangedEventHandler PropertyChanged;


        public string appsName
        {
            get
            {
                return this.appName;
            }

            set
            {
                if (value != this.appName)
                {
                    this.appName = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public BitmapImage appsIcon
        {
            get
            {
                return this.appIcon;
            }

            set
            {
                if (value != this.appIcon)
                {
                    this.appIcon = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void setPerformAudioPassThru(PerformAudioPassThru performAudioPassThru)
        {
            this.performAudioPassThru = performAudioPassThru;
        }

        public PerformAudioPassThru getPerformAudioPassThru()
        {
            return this.performAudioPassThru;
        }

        public void setGlobalProperties(SetGlobalProperties globalProperties)
        {
            this.globalProperties = globalProperties;
        }

        public SetGlobalProperties getGlobalProperties()
        {
            return this.globalProperties;
        }

        public void setSetAppIcon(SetAppIcon setAppIcon)
        {
            this.setAppIcon = setAppIcon;
        }

        public SetAppIcon getSetAppIcon()
        {
            return this.setAppIcon;
        }

        public List<RpcRequest> getMenuCommandsList()
        {
            return this.menuCommandsList;
        }

        public List<AddCommand> getCommandsInSubMenu(int? menuId)
        {
            if (menuId == null || menuId < 1) return null;

            if (subMenuCommandsList.ContainsKey((int) menuId))
            {
                return subMenuCommandsList[(int) menuId];
            } else
            {
                return null;
            }
        }

        public void addToCommandsList(RpcRequest rpcRequest)
        {
            if (rpcRequest == null) return;

            int? position = null;
            int? parentId = null;

            if (rpcRequest.GetType() == typeof(AddCommand))
            {
                position = ((AddCommand)rpcRequest).getMenuParams().getPosition();
                parentId = ((AddCommand)rpcRequest).getMenuParams().getParentID();

                if (parentId != null)
                {
                    if (parentId > 0 && subMenuCommandsList.ContainsKey((int)parentId))
                    {
                        addAddCommandToSubMenu((AddCommand)rpcRequest);
                    } else
                    {
                        if ((position != null) && (position < menuCommandsList.Count))
                        {
                            menuCommandsList.Insert((int)position, (AddCommand)rpcRequest);
                        }
                        else
                        {
                            menuCommandsList.Add((AddCommand)rpcRequest);
                        }
                    }
                }
            } else if (rpcRequest.GetType() == typeof(AddSubMenu))
            {
                position = ((AddSubMenu)rpcRequest).getMenuParams().getPosition();

                if ((position != null) && (position < menuCommandsList.Count))
                {
                    menuCommandsList.Insert((int)position, (AddSubMenu)rpcRequest);
                }
                else
                {
                    menuCommandsList.Add((AddSubMenu)rpcRequest);
                }

                if (((AddSubMenu)rpcRequest).getMenuID() != null && ((AddSubMenu)rpcRequest).getMenuID() > 0)
                {
                    subMenuCommandsList.Add((int)((AddSubMenu)rpcRequest).getMenuID(), new List<AddCommand>());
                }
            }
        }

        public bool isCommandPartOfCommandsList(int? cmdId)
        {
            if (cmdId == null) return false;

            foreach (RpcRequest rpc in menuCommandsList)
            {
                if ((rpc.GetType() == typeof(AddCommand)) && (((AddCommand)rpc).getCmdId() == cmdId))
                {
                    return true;
                }
            }

            return false;
        }
        public bool isSubMenuAvailable(int? menuId)
        {
            if (menuId == null) return false;

            foreach (RpcRequest rpc in menuCommandsList)
            {
                if ((rpc.GetType() == typeof(AddSubMenu)) && (((AddSubMenu)rpc).getMenuID() == menuId))
                {
                    return true;
                }
            }

            return false;
        }

        public bool isCommandPartOfSubMenu(int? cmdId)
        {
            if (cmdId == null) return false;

            for (int i = 1; i < (subMenuCommandsList.Count + 1); i++)
            {
                for (int j = 0; j < subMenuCommandsList[i].Count; j++)
                {
                    if ((subMenuCommandsList[i][j]).getCmdId() == cmdId)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void deleteFromCommandsList(RpcRequest rpcRequest)
        {
            if (rpcRequest == null) return;

            int? cmdId = null;
            int? menuId = null;

            if (rpcRequest.GetType() == typeof(DeleteCommand))
            {
                cmdId = ((DeleteCommand)rpcRequest).getCmdId();
                bool found = true;
                // notes from https://stackoverflow.com/questions/7193294/intelligent-way-of-removing-items-from-a-listt-while-enumerating-in-c-sharp
                List<RpcRequest> toRemove = new List<RpcRequest>();

                //Let's first check out if AddCommand is part of the Menu.
                foreach (RpcRequest rpc in menuCommandsList)
                {
                    if ((rpc.GetType() == typeof(AddCommand)) && (((AddCommand) rpc).getCmdId() == cmdId))
                    {
                        toRemove.Add(rpc);
                        found = true;
                    }
                }
                // Remove everything here
                menuCommandsList.RemoveAll(x => toRemove.Contains(x));

                //Not found in Menu, let's check out in SubMenu and delete.
                if (!found)
                {
                    deleteAddCommandFromSubMenu(cmdId);
                }
            }
            else if (rpcRequest.GetType() == typeof(DeleteSubMenu))
            {
                menuId = ((DeleteSubMenu)rpcRequest).getMenuID();
                List<RpcRequest> toRemove = new List<RpcRequest>();

                foreach (RpcRequest rpc in menuCommandsList)
                {
                    if ((rpc.GetType() == typeof(AddSubMenu)) && (((AddSubMenu)rpc).getMenuID() == menuId))
                    {
                        deleteSubMenuHoldings(menuId);
                        toRemove.Add(rpc);
                    }
                }
                menuCommandsList.RemoveAll(x => toRemove.Contains(x));
            }
        }

        public void addAddCommandToSubMenu(AddCommand addCommand)
        {
            if (addCommand == null || addCommand.getMenuParams() == null) return;

            int? position = addCommand.getMenuParams().getPosition();
            int? parentId = addCommand.getMenuParams().getParentID();

            if ((parentId == null) || (parentId < 1)) return;

            if ((position != null) && (position < subMenuCommandsList[(int)parentId].Count))
            {
                subMenuCommandsList[(int)parentId].Insert((int)position, addCommand);
            }
            else
            {
                subMenuCommandsList[(int)parentId].Add(addCommand);
            }
        }

        public void deleteAddCommandFromSubMenu(int? cmdId)
        {
            if (cmdId == null) return;

            for (int i = 1; i < (subMenuCommandsList.Count + 1); i++)
            {
                for (int j = 0; j < subMenuCommandsList[i].Count; j++)
                {
                    if ((subMenuCommandsList[i][j]).getCmdId() == cmdId)
                    {
                        subMenuCommandsList[i].RemoveAt(j);
                    }
                }
            }
        }
        public void deleteSubMenuHoldings(int? parentId)
        {
            if (parentId == null) return;

            for (int i = 1; i < (subMenuCommandsList.Count + 1); i++)
            {
                if (i == parentId)
                {
                    subMenuCommandsList.Remove(i);
                    break;
                }
            }
        }

        public void setDisplayLayout(SetDisplayLayout displayLayout)
        {
            this.displayLayout = displayLayout;
        }

        public SetDisplayLayout getDisplayLayout()
        {
            return this.displayLayout;
        }

        public void setButtonSubscriptions(ObservableCollection<OnButtonSubscription> buttonSubscriptions)
        {
            this.buttonSubscriptions = buttonSubscriptions;
        }

        public ObservableCollection<OnButtonSubscription> getButtonSubscriptions()
        {
            return this.buttonSubscriptions;
        }

        public void setMediaClock(SetMediaClockTimer mediaClockTimer)
        {
            this.mediaClock = AppUtils.updateMediaClock(this.mediaClock, mediaClockTimer);
        }

        public MediaClock getMediaClock()
        {
            return this.mediaClock;
        }

        public void setShowRequest(Show showRpc)
        {
            cachedShow.onUpdateReceived(showRpc);

            this.showRpc = showRpc;
        }

        public CachedShow getCachedShow()
        {
            return cachedShow;
        }

        public Show getShowRequest()
        {
            return this.showRpc;
        }

        public void setAppIconPath(string appIconPath)
        {
            this.appIconPath = appIconPath;
        }

        public string getAppIconPath()
        {
            return appIconPath;
        }

        /*public void setAppIcon(BitmapImage appIcon)
        {
            this.appIcon = appIcon;
        }

        public BitmapImage getAppIcon()
        {
            return appIcon;
        }*/

        public Windows.UI.Xaml.Visibility getDynamicIconVisisble()
        {
            if (appsIcon != null)
                return Windows.UI.Xaml.Visibility.Visible;

            return Windows.UI.Xaml.Visibility.Collapsed;
        }

        public Windows.UI.Xaml.Visibility getStaticIconVisisble()
        {
            if (appsIcon != null)
                return Windows.UI.Xaml.Visibility.Collapsed;

            return Windows.UI.Xaml.Visibility.Visible;
        }

        public int getAppId()
        {
            if (null == getApplication()) return -1;

            return (int)getApplication().getAppID();
        }

        public string getAppName()
        {
            if (null == getApplication()) return null;

            return getApplication().getAppName();
        }

        public string getAppNameWithPadding()
        {
            if (null == getApplication()) return null;

            return "   " + getApplication().getAppName();
        }

        public string getAppIDString()
        {
            if (null == getApplication()) return null;

            return getApplication().getAppID().ToString();
        }

        public HmiApiLib.Common.Structs.HMIApplication getApplication()
        {
            if (null == onAppRegistered) return null;

            return onAppRegistered.getApplication();
        }


        public AppItem(OnAppRegistered onAppRegistered)
        {
            this.onAppRegistered = onAppRegistered;
            this.appsName = getAppName();

            this.graphicManager = new GraphicManager(this);

            cachedShow = new CachedShow(AppInstanceManager.AppInstance);
            if (onAppRegistered.getApplication() != null)
                setAppIconPath(onAppRegistered.getApplication().getIcon());
        }
    }
}
