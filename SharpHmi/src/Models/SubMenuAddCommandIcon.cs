﻿
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using Windows.UI.Xaml.Media.Imaging;

namespace SharpHmi.src.Models
{
   public class SubMenuAddCommandIcon
    {
        private string staticIconValue;
        private BitmapImage bitMapImage;
        private AddCommand addCmdReq;

        public AddCommand getAddCommand()
        {
            return this.addCmdReq;
        }
        public string getStaticIconValue()
        {
            return staticIconValue;
        }
        public void setStaticIconValue(string staticIconValue)
        {
            this.staticIconValue = staticIconValue;
        }

        public string getCmdName()
        {
            string cmdName = null;

            if(addCmdReq != null && addCmdReq.getMenuParams() != null)
            {
                cmdName = addCmdReq.getMenuParams().getMenuName();
            }

            return cmdName;
        }

        public BitmapImage getBitmapImage()
        {
            return this.bitMapImage;
        }
        public void setBitmapImage(BitmapImage bitmapImage)
        {
            this.bitMapImage = bitmapImage;
        }
        public SubMenuAddCommandIcon(AddCommand addCmdReq)
        {
            this.addCmdReq = addCmdReq;
        }
    }
}