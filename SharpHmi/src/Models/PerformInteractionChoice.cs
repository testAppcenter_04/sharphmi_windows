﻿using HmiApiLib.Common.Structs;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Media;

namespace SharpHmi.src.Models
{
    class PerformInteractionChoice : INotifyPropertyChanged
    {
        private string staticIconValue;
        private ImageSource dynamicImgSrc;
        private Choice choice = new Choice();

        public PerformInteractionChoice()
        {

        }

        public string StaticIconVal {
            get => staticIconValue;
            set {
                staticIconValue = value;
                OnPropertyChanged();
            }
        }

        public ImageSource DynamicImageSrc
        {
            get => dynamicImgSrc;
            set
            {
                dynamicImgSrc = value;
                OnPropertyChanged();
            }
        }

        public Choice Choice {
            get => choice;
            set
            {
                choice = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
