﻿
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace SharpHmi.src.Models
{
   public class MenuIcon
    {
        private string staticIconValue;
        private BitmapImage bitMapImage;
        private RpcRequest rpcRequest;

        public RpcRequest getRpcRequest()
        {
            return this.rpcRequest;
        }
        public string getStaticIconValue()
        {
            return staticIconValue;
        }
        public void setStaticIconValue(string staticIconValue)
        {
            this.staticIconValue = staticIconValue;
        }

        public Visibility getSubMenuIconVisible()
        {
            if (rpcRequest != null)
            {
                if (rpcRequest.GetType() == typeof(AddSubMenu))
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            return Visibility.Collapsed;
        }

        public string getMenuName()
        {
            if(rpcRequest != null)
            {
                MenuParams menuParams = null;

                if (rpcRequest.GetType() == typeof(AddSubMenu))
                {
                    menuParams = ((AddSubMenu)rpcRequest).getMenuParams();
                } else if (rpcRequest.GetType() == typeof(AddCommand))
                {
                    menuParams = ((AddCommand)rpcRequest).getMenuParams();
                }

                if (menuParams != null)
                    return menuParams.menuName;
            }

            return "";
        }

        public BitmapImage getBitmapImage()
        {
            return this.bitMapImage;
        }
        public void setBitmapImage(BitmapImage bitmapImage)
        {
            this.bitMapImage = bitmapImage;
        }
        public MenuIcon(RpcRequest rpcRequest)
        {
            this.rpcRequest = rpcRequest;
        }
    }
}