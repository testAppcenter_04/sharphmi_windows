﻿using HmiApiLib.Controllers.UI.IncomingRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHmi.src
{
    public interface AppUiCallback
    {
        void onSelectionButtonClicked(bool hmiButtonClicked);
        void onUiShowRequestCallback(Show msg);
        void onUIScrollableMessageCallback(ScrollableMessage msg);
    }
}
