﻿
using System.Runtime.Serialization;

namespace SharpHmi.src.Enums
{
    public enum DisplayLayout
    {
        TILES_ONLY,
        TILES_WITH_GRAPHIC,
        GRAPHIC_WITH_TILES,
        DEFAULT,
        MEDIA,
        [EnumMember(Value = "NON-MEDIA")]
        NON_MEDIA,
        GRAPHIC_WITH_TEXT,
        TEXT_WITH_GRAPHIC,
        GRAPHIC_WITH_TEXT_AND_SOFTBUTTONS,
        TEXT_AND_SOFTBUTTONS_WITH_GRAPHIC,
        GRAPHIC_WITH_TEXTBUTTONS,
        DOUBLE_GRAPHIC_WITH_SOFTBUTTONS,
        TEXTBUTTONS_WITH_GRAPHIC,
        TEXTBUTTONS_ONLY,
        LARGE_GRAPHIC_WITH_SOFTBUTTONS,
        LARGE_GRAPHIC_ONLY
    }
}
