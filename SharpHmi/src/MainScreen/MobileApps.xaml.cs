﻿using SharpHmi.src.MainScreen;
using SharpHmi.src.Models;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using HmiApiLib.Base;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MobileApps:Page
    {
        private ObservableCollection<AppItem> appListObservableCollection ;
        public Frame parentFrame;
        String selectedAppName = null;
        public static Boolean isfullHMI=false;
        private ObservableCollection<AppItem>  appList ;
        AppInstanceManager instance;
        String navigateFrom;
        ConsentSource? consent = (ConsentSource)0;

        public MobileApps()
        {
            this.InitializeComponent();
            appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);

            appList = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
            if (appList!=null)
            AppsPageAppListView.ItemsSource = appList;
            if(instance==null)
           instance = AppInstanceManager.AppInstance;
         //  AppsPageAppListView.ItemClick += AppsPageAppListView_ItemClick;
            
            isfullHMI = false;

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigateFrom = (String)e.Parameter;

            Boolean? isSDLAllowed;
            try
            {
                if (AppInstanceManager.AppInstance.appList != null)
                {
                       isSDLAllowed = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getIsSDLAllowed(); ;
                    if (isSDLAllowed == false)
                    {
                        EnableMobileAppsToggle.IsOn = false;
                        AllApps.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        AppsPageAppListView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    

                }
            }
            catch (Exception ex) { }



        }
        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AppList();

        }
        public  void AppList() {

                int? selectedAppID = null;

                int index = AppsPageAppListView.SelectedIndex;
                if (instance.appList.Count != 0)
                {
                    selectedAppID = appList[index].getApplication().getAppID();
                    selectedAppName = appList[index].getApplication().getAppName();
                }
                RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnAppActivated(selectedAppID);
                AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
                instance.sendRpc(rpcNotification);
                isfullHMI = true;

            
                Frame.Navigate(typeof(AllApps), appList[index]);

            

            
    
        }

        private void ToggleSwitch_Toggled(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            try
            {
                AllApps.Visibility = Windows.UI.Xaml.Visibility.Visible;
                AppsPageAppListView.Visibility = Windows.UI.Xaml.Visibility.Visible;
                if (toggleSwitch != null)
                {
                    if (toggleSwitch.IsOn == true)
                    {
                        SdlOnAllowSDLFunctionality();


                    }
                    else
                    {
                        AllApps.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        AppsPageAppListView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex) { }
        }
        private void SdlOnAllowSDLFunctionality()
        {
            DeviceInfo devInfo = null;
            try
            {
                if (AppInstanceManager.AppInstance.appList != null)
                {
                    devInfo = new DeviceInfo();
                    devInfo.name = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getName();
                    devInfo.id = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getId(); ;
                    devInfo.transportType = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getTransportType();
                    devInfo.isSDLAllowed = true;
                }
            }
            catch (Exception ex) { }
            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAllowSDLFunctionality(devInfo, true, consent);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

    }
}
