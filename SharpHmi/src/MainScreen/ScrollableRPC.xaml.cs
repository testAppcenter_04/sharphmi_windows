﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace SharpHmi.src.MainScreen
{
    public sealed partial class ScrollableRPC : Page, IRefreshListener
    {
        StorageFile file;
        ScrollableMessage parameters = null;
        private bool rpcResponseSent = false;
        AppInstanceManager appInstanceManager;
        public int[] softButtonsId = new int[8];
        public bool[] softButtonHighlighted = new bool[8];

        public ScrollableRPC()
        {
            this.InitializeComponent();
            appInstanceManager = AppInstanceManager.AppInstance;
            appInstanceManager.addRefreshListener(this, typeof(ScrollableMessage));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rpcResponseSent = false;

            base.OnNavigatedTo(e);
            
            parameters = (ScrollableMessage)e.Parameter;

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.HMI_OBSCURED, parameters.getAppId()));
            }

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(parameters.getAppId());
            AppName.Text = (appItem != null) ? AppInstanceManager.AppInstance.getAppItem(parameters.getAppId()).getAppName() : ""; ;
            displayText(parameters);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (!rpcResponseSent) //if we previously never sent a response, go ahead and abort the rpc
            {
                RpcResponse scrollableResponse = BuildRpc.buildUiScrollableMessageResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED);
                sendRpcInternal(scrollableResponse);
            }

            //try to remove this page from the backstack
            try
            {
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, parameters.getAppId()));
            }
        }

        private void sendRpcInternal(RpcMessage msg)
        {
            rpcResponseSent = true;
            AppInstanceManager.AppInstance.sendRpc(msg);

            if (timeout != null)
            {
                tokenSource.Cancel();
            }
        }

        private void displayText(ScrollableMessage msg)
        {
            if (msg.getMessageText() != null)
            {
                scrollableMsg.Text = msg.getMessageText().getFieldText();
            }

            ClearSoftButtons();

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(msg.getAppId());
            if (appItem == null) return;

            displaySoftButtonsAsync(msg, appItem);

            if (msg.getTimeout() != null)
                timeOut(msg.getTimeout());
        }

        Task timeout = null;
        System.Threading.CancellationTokenSource tokenSource = new System.Threading.CancellationTokenSource();

        private async void timeOut(int? time)
        {
            timeout = Task.Delay(TimeSpan.FromMilliseconds((double)time), tokenSource.Token);
            try
            {
                await timeout;
            }
            catch (Exception ex)
            {
                Console.Write(ex); //if the task is cancelled by another response, return
                return;
            }

            if (parameters != null)
            {
                RpcResponse scrollableResponse = BuildRpc.buildUiScrollableMessageResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.TIMED_OUT);
                sendRpcInternal(scrollableResponse);
            }

            if (Frame.CurrentSourcePageType == typeof(ScrollableRPC))
            {
                Frame.GoBack();
            }
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
            {
                RpcResponse scrollableResponse = BuildRpc.buildUiScrollableMessageResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED);
                sendRpcInternal(scrollableResponse);
            }

            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void ClearSoftButtons()
        {
            try
            {
                SoftButton1.Visibility = Visibility.Collapsed;
                SoftButton2.Visibility = Visibility.Collapsed;
                SoftButton3.Visibility = Visibility.Collapsed;
                SoftButton4.Visibility = Visibility.Collapsed;
                SoftButton5.Visibility = Visibility.Collapsed;
                SoftButton6.Visibility = Visibility.Collapsed;
                SoftButton7.Visibility = Visibility.Collapsed;
                SoftButton8.Visibility = Visibility.Collapsed;
            }
            catch {

            }

        }
        private void displaySoftButtonsAsync(ScrollableMessage msg, AppItem appItem)
        {
            if (null != msg.getSoftButtons() && (msg.getSoftButtons().Count > 0))
            {
                for (int i = 0; i < msg.getSoftButtons().Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton1, SoftButton1Text, SoftButton1Image, SoftButton1StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 1:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton2, SoftButton2Text, SoftButton2Image, SoftButton2StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 2:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton3, SoftButton3Text, SoftButton3Image, SoftButton3StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 3:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton4, SoftButton4Text, SoftButton4Image, SoftButton4StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 4:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton5, SoftButton5Text, SoftButton5Image, SoftButton5StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 5:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton6, SoftButton6Text, SoftButton6Image, SoftButton6StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 6:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton7, SoftButton7Text, SoftButton7Image, SoftButton7StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 7:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton8, SoftButton8Text, SoftButton8Image, SoftButton8StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        default:
                            break;
                    }
                }
            }

        }       

        private void processButtonNotificationAndScrollableReponse(SoftButton softButton)
        {
            if (parameters != null)
            {
                AppInstanceManager.AppInstance.sendOnButtonNotifications(ButtonName.CUSTOM_BUTTON, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, ((int)softButton.getSoftButtonID()), parameters.getAppId());

                RpcResponse scrollableResponse = BuildRpc.buildUiScrollableMessageResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.SUCCESS);
                sendRpcInternal(scrollableResponse);
            }

            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void SoftButton1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[0]);
        }

        private void SoftButton2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[1]);
        }

        private void SoftButton3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[2]);
        }

        private void SoftButton4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[3]);
        }

        private void SoftButton5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[4]);
        }

        private void SoftButton6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[5]);
        }

        private void SoftButton7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[6]);
        }

        private void SoftButton8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
                processButtonNotificationAndScrollableReponse(parameters.getSoftButtons()[7]);
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState dataState)
        {
            if (rpcType == typeof(ScrollableMessage))
            {
                if (!rpcResponseSent && dataState == RefreshDataState.BEFORE_DATA_CHANGE)  //if we previously never sent a response, go ahead and abort the rpc
                {
                    RpcResponse scrollableResponse = BuildRpc.buildUiScrollableMessageResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED);
                    sendRpcInternal(scrollableResponse);
                }
            }
        }
    }
}