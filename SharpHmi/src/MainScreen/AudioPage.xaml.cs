﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>    
    public sealed partial class AudioPage : Page
    {
        AppInstanceManager appInstanceManager;
        public AudioPage()
        {
            this.InitializeComponent();
        }
        public enum Result
        {
            SUCCESS,
            UNSUPPORTED_REQUEST,
            UNSUPPORTED_RESOURCE,
            DISALLOWED,
            REJECTED,
            ABORTED,
            IGNORED,
            RETRY,
            IN_USE,
            DATA_NOT_AVAILABLE,
            TIMED_OUT,
            INVALID_DATA,
            CHAR_LIMIT_EXCEEDED,
            INVALID_ID,
            DUPLICATE_NAME,
            APPLICATION_NOT_REGISTERED,
            WRONG_LANGUAGE,
            OUT_OF_MEMORY,
            TOO_MANY_PENDING_REQUESTS,
            NO_APPS_REGISTERED,
            NO_DEVICES_CONNECTED,
            WARNINGS,
            GENERIC_ERROR,
            USER_DISALLOWED,
            TRUNCATED_DATA,
            SAVED
        }

        private void sendEventChange()
        {
            appInstanceManager = AppInstanceManager.AppInstance;
            RequestNotifyMessage eventChange = BuildRpc.buildBasicCommunicationOnEventChangedNotification(HmiApiLib.Common.Enums.EventTypes.AUDIO_SOURCE, AppInstanceManager.isEmbeddedAudioActive);
            appInstanceManager.sendRpc(eventChange);
        }

        private void EmbeddedAudioOn(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            AppInstanceManager.isEmbeddedAudioActive = true;
            sendEventChange();
        }

        private void EmbeddedAudioOff(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            AppInstanceManager.isEmbeddedAudioActive = false;
            sendEventChange();
        }

        private void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
}
