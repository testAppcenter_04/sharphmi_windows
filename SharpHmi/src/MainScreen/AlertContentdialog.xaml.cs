﻿using HmiApiLib;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.MainScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AlertContentdialog : ContentDialog
    {
        public Alert alert = null;
        StorageFile file;
        public int[] softButtonsId = new int[8];
        public bool[] softButtonHighlighted = new bool[8];

        public AlertContentdialog(PageNavigate msgData)
        {
            this.InitializeComponent();
            AppName.Text = msgData.Info;
            alert = (Alert)msgData.alertMsg;
            displayText(msgData.alertMsg);
        }
       
        private void clearText()
        {
            try
            {
                alert1.Text = "";
                alert2.Text = "";
                alert3.Text = "";
                
            }
            catch { }
        }
        
        private void displayText(Alert msg)
        {
            clearText();
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(msg.getAppId());
            if (appItem == null) return;

            if (msg.getAlertStrings() != null)
             
            {
                foreach (TextFieldStruct tfs in msg.getAlertStrings())
                {
                    switch (tfs.fieldName)
                    {
                        case TextFieldName.alertText1:

                            alert1.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.alertText2:

                            alert2.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.alertText3:

                            alert3.Text = String.Format(tfs.fieldText);
                            break;
                    }
                }
            }

            ClearSoftButtons();
            displaySoftButtons(msg, appItem);

            if (msg.getDuration() != null)
                timeOut(msg.getDuration());
        }

        private void ClearSoftButtons()
        {
            try
            {
                SoftButton1.Visibility = Visibility.Collapsed;
                SoftButton2.Visibility = Visibility.Collapsed;
                SoftButton3.Visibility = Visibility.Collapsed;
                SoftButton4.Visibility = Visibility.Collapsed;
            }
            catch
            {

            }
        }

        private void displaySoftButtons(Alert msg, AppItem appItem)
        {
            if (null != msg.getSoftButtons() && (msg.getSoftButtons().Count > 0))
            {
                CloseButton.Visibility = Visibility.Collapsed; //if we have at least one softbutton, no need to display the close button

                for (int i = 0; i < msg.getSoftButtons().Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton1, SoftButton1Text, SoftButton1Image, SoftButton1StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 1:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton2, SoftButton2Text, SoftButton2Image, SoftButton2StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 2:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton3, SoftButton3Text, SoftButton3Image, SoftButton3StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 3:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton4, SoftButton4Text, SoftButton4Image, SoftButton4StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        default:
                            break;
                    }
                }
            }

        }

        private void processButtonNotificationAndReponse(SoftButton softButton)
        {
            if (alert != null)
            {
                AppInstanceManager.AppInstance.sendOnButtonNotifications(ButtonName.CUSTOM_BUTTON, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, ((int)softButton.getSoftButtonID()), alert.getAppId());
                AppInstanceManager.AppInstance.sendRpc(BuildRpc.buildUiAlertResponse(alert.getId(), HmiApiLib.Common.Enums.Result.SUCCESS, null));
            }
            this.Hide();
        }


        private void SoftButton1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            processButtonNotificationAndReponse(alert.getSoftButtons()[0]);
        }

        private void SoftButton2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            processButtonNotificationAndReponse(alert.getSoftButtons()[1]);
        }

        private void SoftButton3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            processButtonNotificationAndReponse(alert.getSoftButtons()[2]);
        }

        private void SoftButton4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            processButtonNotificationAndReponse(alert.getSoftButtons()[3]);
        }

        private async void timeOut(int? time)
        {
            if (time == null || time < 3000) return; //minimum amount of time for alert timeout is 3 seconds

            await Task.Delay(TimeSpan.FromMilliseconds((double)time-500)); //complete the task before sdl core sends a hmi does not respond timeout

            if (alert != null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Alert);
                AppInstanceManager.AppInstance.sendRpc(BuildRpc.buildUiAlertResponse(alert.getId(), HmiApiLib.Common.Enums.Result.TIMED_OUT, null));
            }

            this.Hide();
        }

        private void CloseButtonTap(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (alert != null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Alert);
                AppInstanceManager.AppInstance.sendRpc(BuildRpc.buildUiAlertResponse(alert.getId(), HmiApiLib.Common.Enums.Result.SUCCESS, null));
            }

            this.Hide();
        }
    }
}
