﻿using SharpHmi.src.MainScreen;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SharpHmi.src.interfaces;
using Windows.UI.Xaml.Media.Imaging;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using System.ComponentModel;
using System.Collections.Generic;
using Windows.UI.Xaml.Media;
using HmiApiLib.Common.Structs;

namespace SharpHmi
{
    public sealed partial class MainPage : IApplicationCallback
    {
        public static ListBoxItem consoleMenuListItem;
        public static ListBoxItem locationMenuListItem;
        public static ListBoxItem audioMenuListItem;
        public static ListBoxItem climateMenuListItem;
        public static ListBoxItem phoneMenuListItem;
        public static ListBoxItem appsMenuListItem;
        public static ListBoxItem settingsMenuListItem;
        public static ListBoxItem projectionMenuListItem;
        public static ListBoxItem policiesMenuListItem;

        public static Frame MainPageFrame;
        public static VLC.MediaElement vidPlayer;
        public static VLC.MediaElement audPlayer;
        public ListBoxItem mediaProjection;
        AppInstanceManager appInstanceManager;
        private string _tbText;
        private string _tbVisible;
        private BitmapImage _tbIcon;
        List<string> speechList = new List<string>();
        public static AlertContentdialog alertDialog;


        //String fullNavigationString = null;
        public MainPage()
        {
            InitializeComponent();
            appInstanceManager = AppInstanceManager.AppInstance;
            NavigationCacheMode = NavigationCacheMode.Required;
            SelectionPage.applicationCallback = this;
            tbText = "text";
            tbVisible = "Collapsed";

            AppInstanceManager.initializeStorageFolder();

            AppSetting appSetting = new AppSetting();

            appInstanceManager.appSetting = appSetting;

            MainPageFrame = MainFrame;
            vidPlayer = videoPlayer;
            audPlayer = audioPlayer;

            MainFrame.Navigate(typeof(SettingsPage));

            appSetting.setSelectedMode(AppInstanceManager.SelectionMode.BASIC_MODE);

            consoleMenuListItem = ConsoleMenu;
            locationMenuListItem = NavMenu;
            audioMenuListItem = AudioMenu;
            climateMenuListItem = ClimateMenu;
            phoneMenuListItem = PhoneMenu;
            appsMenuListItem = AppsMenu;
            settingsMenuListItem = SettingsMenu;
            projectionMenuListItem = MediaProjection;
            policiesMenuListItem = AllApps;

            appInstanceManager.setApplicationCallback(this);
            mainPage = this;
            this.DataContext = this;
            mediaProjection = MediaProjection;
            appInstanceManager.refMainPage = this;



        }
        public string tbText
        {
            get { return _tbText; }
            set
            {
                _tbText = value;
                RaisePropertyChanged("tbText");
            }
        }
        public BitmapImage tbIcon
        {
            get { return _tbIcon; }
            set
            {
                _tbIcon = value;
                RaisePropertyChanged("tbIcon");
            }
        }
        public string tbVisible
        {
            get { return _tbVisible; }
            set
            {
                _tbVisible = value;
                RaisePropertyChanged("tbVisible");


            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string PropertyName)
        {
            try
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
                }
            }
            catch (Exception ex) { }

        }
        public static MainPage mainPage { get; set; }
        private void NavigationButtonClicked(object sender, RoutedEventArgs e)
        {
            NavigationMenu.IsPaneOpen = !NavigationMenu.IsPaneOpen;
        }


        private void handleMediaProjection()
        {
            appInstanceManager = AppInstanceManager.AppInstance;
            if (appInstanceManager != null)
            {
                if (AppInstanceManager.lastProjectedAppId > 0)
                {
                    src.Models.AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.lastProjectedAppId);
                    if (appItem != null)
                    {
                        //if our app item contains a projection or navigation apphmi type, activate the app since we are on the media projection page
                        if (appItem.getApplication() != null)
                        {
                            if (appItem.getApplication().getAppType() != null)
                            {
                                if (appItem.getApplication().getAppType().Contains(HmiApiLib.Common.Enums.AppHMIType.PROJECTION) || appItem.getApplication().getAppType().Contains(HmiApiLib.Common.Enums.AppHMIType.NAVIGATION))
                                {
                                    activateProjApp(appItem);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void activateProjApp(src.Models.AppItem appItem)
        {
            appInstanceManager = AppInstanceManager.AppInstance;
            if (appInstanceManager != null)
            {
                if (AppInstanceManager.lastProjectedAppId > 0)
                {
                    HmiApiLib.Base.RpcRequest rpcMessage = HmiApiLib.Builder.BuildRpc.buildSdlActivateAppRequest(HmiApiLib.Builder.BuildRpc.getNextId(), AppInstanceManager.lastProjectedAppId);
                    appInstanceManager.sendRpc(rpcMessage);
                    AppInstanceManager.activatedAppId = appItem.getAppId();
                    AppInstanceManager.activatedAppName = appItem.getAppName(); ;
                }
            }
        }

        private void deactivateApp()
        {
            appInstanceManager = AppInstanceManager.AppInstance;
            if (appInstanceManager != null)
            {
                if (AppInstanceManager.activatedAppId > 0)
                {
                    HmiApiLib.Base.RequestNotifyMessage onAppDeActivated = HmiApiLib.Builder.BuildRpc.buildBasicCommunicationOnAppDeactivated(AppInstanceManager.activatedAppId);
                    appInstanceManager.sendRpc(onAppDeActivated);
                }
            }
        }

        private void NavigationMenuList_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {

            if (AudioMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(AudioPage));
            }
            else if (ClimateMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(ClimatePage));
            }
            else if (PhoneMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(PhonePage));
            }
            else if (NavMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(NavPage));
            }
            else if (AppsMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(AppsPage));
            }
            else if (SettingsMenu.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(SettingsPage));
            }
            else if (ConsoleMenu.IsSelected)
            {
               // if we are simply checking the console log, lets not deactivate the app.
                MainFrame.Navigate(typeof(ConsolePage));
            }

            else if (MediaProjection.IsSelected)
            {
                handleMediaProjection();
                MainFrame.Navigate(typeof(ProjectionTemplate));
            }
            else if (AllApps.IsSelected)
            {
                deactivateApp();
                MainFrame.Navigate(typeof(MobileApps), "AllApps");
            }
        }

        private void ActionMenuButtonClicked(object sender, RoutedEventArgs e)
        {

        }

        public void onSelectionButtonClicked(bool hmiButtonClicked)
        {
            MainFrame.Navigate(typeof(SettingsPage));
        }

        public async void onRefreshRegisteredApps()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    AppsMenu.IsSelected = true;
                    MainFrame.Navigate(typeof(AppsPage));
                }
                catch
                {
                }
            }
            );
        }

        public async void onRedirectToVideoStreaming()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    if (MainFrame.SourcePageType.ToString() != "SharpHmi.VlcPlayerPage")
                    {
                        MediaProjection.IsSelected = true;
                        MainFrame.Navigate(typeof(ProjectionTemplate));
                    }
                }
                catch (Exception e)
                { }
            });
        }
    }
}