﻿using HmiApiLib.Base;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Builder;
using Windows.UI.Xaml;
using SharpHmi.src.interfaces;
using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using static SharpHmi.src.Manager.GraphicManager;

namespace SharpHmi.src.MainScreen 
{
    public sealed partial class MenuItems : IAddCommand
    {
        AppInstanceManager appInstanceManager;
        ObservableCollection<MenuIcon> menuCommandsIconList = new ObservableCollection<MenuIcon> ();        

        public MenuItems()
        {
            this.InitializeComponent();            

            if (appInstanceManager == null)
                appInstanceManager = AppInstanceManager.AppInstance;

            Exit.Text = "Exit" + " " + AppInstanceManager.activatedAppName;
            AppName.Text = AppInstanceManager.activatedAppName;


            AppInstanceManager.AppInstance.setCommandCallback(this);
        }

        public void UpdateList()
        {
            menuCommandsIconList.Clear();
            List<RpcRequest> commandList = null;

            if (appInstanceManager.getAppItem(AppInstanceManager.activatedAppId) != null)
                commandList = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId).getMenuCommandsList();

            if (commandList != null)
            {
                for (int i = 0; i < commandList.Count; i++)
                {
                    if (commandList[i].GetType() == typeof(AddSubMenu))
                    {
                        AddSubMenu tmpObj_SubMenu = (AddSubMenu)commandList[i];

                        MenuIcon menuIcon = new MenuIcon(tmpObj_SubMenu);

                        if (tmpObj_SubMenu.getMenuIcon() != null)
                        {
                            if (tmpObj_SubMenu.getMenuIcon().getImageType() == ImageType.STATIC)
                            {
                                string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(tmpObj_SubMenu.getMenuIcon().getValue());
                                if (staticImage != "" || null == staticImage)
                                {
                                    menuIcon.setStaticIconValue(staticImage);
                                }
                                else
                                {
                                    menuIcon.setStaticIconValue("\uEB9F");
                                }
                            }
                        }

                        menuCommandsIconList.Add(menuIcon);
                    }
                    else if (commandList[i].GetType() == typeof(AddCommand))
                    {
                        AddCommand tmpObj_AddComm = (AddCommand)commandList[i];

                        MenuIcon menuIcon = new MenuIcon(tmpObj_AddComm);

                        if (tmpObj_AddComm.getCmdIcon() != null)
                        {
                            if (tmpObj_AddComm.getCmdIcon().getImageType() == ImageType.STATIC)
                            {
                                string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(tmpObj_AddComm.getCmdIcon().getValue());
                                if (staticImage != "" || null == staticImage)
                                {
                                    menuIcon.setStaticIconValue(staticImage);
                                }
                                else
                                {
                                    menuIcon.setStaticIconValue("\uEB9F");
                                }
                            }
                        }

                        menuCommandsIconList.Add(menuIcon);
                    }
                }

                SetIcon();
            }
        }

        public void handleGraphics(AppItem appItem, MenuIcon imageHolder, string iconValue)
        {
            if (appItem == null) return;

            //try to get the currently cached bitmap
            BitmapImage myImage = appItem.graphicManager.getGraphic(iconValue);
            if (myImage != null)
            {
                imageHolder.setBitmapImage(myImage);
            }

            //setup our input parameters, both path and holder
            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = imageHolder;
            inputGraphic.fileName = iconValue;

            //wait and listen for updates for our graphic bitmap from putfile, this is our softbutton graphic
            Action<OutputGraphic> graphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != AppInstanceManager.activatedAppId) return;
                    MenuIcon image = (MenuIcon)graphicStruct.holder;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.setBitmapImage(bitmapUpdate);
                }
            };

            //subscribe for updates for our graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(graphicDelegate, inputGraphic);
        }

        public void SetIcon()
        {
            if (menuCommandsIconList.Count > 0)
            {
                for (int i = 0; i < menuCommandsIconList.Count; i++)
                {
                    if (menuCommandsIconList[i].getRpcRequest() != null)
                    {
                        string iconValue = null;
                        Type cmdType = null;

                        if (menuCommandsIconList[i].getRpcRequest().GetType() == typeof(AddSubMenu)
                            && (((AddSubMenu)menuCommandsIconList[i].getRpcRequest()).getMenuIcon() != null)
                            && ((AddSubMenu)menuCommandsIconList[i].getRpcRequest()).getMenuIcon().getImageType() == ImageType.DYNAMIC)
                        {
                            iconValue = ((AddSubMenu)menuCommandsIconList[i].getRpcRequest()).getMenuIcon().getValue();
                            cmdType = typeof(AddSubMenu);
                        } else if (menuCommandsIconList[i].getRpcRequest().GetType() == typeof(AddCommand)
                            && (((AddCommand)menuCommandsIconList[i].getRpcRequest()).getCmdIcon() != null)
                            && ((AddCommand)menuCommandsIconList[i].getRpcRequest()).getCmdIcon().getImageType() == ImageType.DYNAMIC)
                        {
                            iconValue = ((AddCommand)menuCommandsIconList[i].getRpcRequest()).getCmdIcon().getValue();
                            cmdType = typeof(AddCommand);
                        }

                        if (iconValue != null)
                        {
                            try
                            {
                                AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId);
                                handleGraphics(appItem, menuCommandsIconList[i], iconValue);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }

                    }
                }
            }

           ObservableCollection<MenuIcon> commandIconsRefresh = new ObservableCollection<MenuIcon>(menuCommandsIconList);
           ListViewImage.ItemsSource = commandIconsRefresh;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ( appInstanceManager!= null && appInstanceManager.getAppItem(AppInstanceManager.activatedAppId) != null)
            {
                AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MENU, appItem.getAppId()));
            }
            UpdateList();

        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (appInstanceManager != null && appInstanceManager.getAppItem(AppInstanceManager.activatedAppId) != null)
            {
                AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, appItem.getAppId()));
            }

            AppInstanceManager.AppInstance.setCommandCallback(null);
        }
        
        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            MenuIcon selectedCommand = (MenuIcon)e.ClickedItem;

            RpcRequest rpcRequest = selectedCommand.getRpcRequest();

            if (rpcRequest.GetType() == typeof(AddSubMenu))
            {
                var navigateFrom = new PageNavigate()
                {
                    menuId = ((AddSubMenu)rpcRequest).getMenuID()
                };
                Frame.Navigate(typeof(SubItems), navigateFrom);
            }
            else
            {
                HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand onCommand = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand();
                onCommand.setCmdID(((AddCommand)rpcRequest).getCmdId());
                onCommand.setAppId(AppInstanceManager.activatedAppId);
                AppInstanceManager.AppInstance.sendRpc(onCommand);

                backOnePage();
            }
        }

        private void backToMainPage()
        {
            Frame.Navigate(typeof(AppsPage));
        }

        private void Static_ItemClick(object sender, TappedRoutedEventArgs e)
        {
            //we need to backup before sending out our onCommand because the app may send a ui command like scrollable message which will be dismissed by our Frame.GoBack()
            backToMainPage();

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame = MainPage.MainPageFrame;

            RequestNotifyMessage onAppExit = BuildRpc.buildBasicCommunicationOnExitApplication(HmiApiLib.Common.Enums.ApplicationExitReason.USER_EXIT, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(onAppExit);

            

        }

        private void backOnePage()
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            backOnePage();
        }

        public async void Refresh()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                UpdateList();
            });
        }

        public async void RedirectToAppsPage()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    MainPage.MainPageFrame.Navigate(typeof(AppsPage));
                }
                catch
                {

                }
            }
            );
        }

        public void refreshMenu()
        {
            Refresh();
        }
    }
}
