﻿using HmiApiLib;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using static SharpHmi.src.Manager.GraphicManager;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.MainScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NavPage : Page
    {
        SendLocation sendLocation;
        StorageFile locationImageFile;

        AppInstanceManager appInstanceManager;

        public NavPage()
        {
            this.InitializeComponent();

            if (appInstanceManager == null)
                appInstanceManager = AppInstanceManager.AppInstance;

            MyMap.MapServiceToken = "NQtQrXKFZAMywjeYuVFM~CstTNFl_cxVkQXbjLTWmuQ~AnuUfumL5HgRzRlQOEYLrcAC95xSnAqVSArAfKAdSOxvlP_GOINAzttq0tymq5V_";
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void CallButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppUtils.ShowToastNotification("No Calling Service!", "Calling Service not available...", 3);
        }

        private async void SetCurrentLocation()
        {
            LocationTextBlock.Text = "";
            LocationDescriptionTextBlock.Text = "";
            LocationDetail1TextBlock.Text = "";
            LocationDetail2TextBlock.Text = "";
            LocationDetail3TextBlock.Text = "";

            var accessStatus = await Geolocator.RequestAccessAsync();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                   
                    var myPosition = new Windows.Devices.Geolocation.BasicGeoposition();

                    if (null == sendLocation)
                    {
                        var geoLocator = new Geolocator();
                        geoLocator.DesiredAccuracy = PositionAccuracy.High;
                        Geoposition pos = await geoLocator.GetGeopositionAsync(TimeSpan.FromMinutes(5), TimeSpan.FromSeconds(30));
                        LocationDescriptionTextBlock.Text = "Current Location";
                        LocationDetail1TextBlock.Text = "Latitude: " + pos.Coordinate.Point.Position.Latitude.ToString();
                        LocationDetail2TextBlock.Text = "Longitude: " + pos.Coordinate.Point.Position.Longitude.ToString();

                        myPosition.Latitude = pos.Coordinate.Point.Position.Latitude;
                        myPosition.Longitude = pos.Coordinate.Point.Position.Longitude;
                    }
                    else
                    {
                        if(null != sendLocation.getLatitudeDegrees())
                            myPosition.Latitude = (double)sendLocation.getLatitudeDegrees();

                        if (null != sendLocation.getLongitudeDegrees())
                            myPosition.Longitude = (double)sendLocation.getLongitudeDegrees();

                            LocationTextBlock.Text = sendLocation.getLocationName() == null ? AppInstanceManager.activatedAppName : sendLocation.getLocationName();
                            LocationDescriptionTextBlock.Text = sendLocation.getLocationDescription() == null ? AppInstanceManager.activatedAppName : sendLocation.getLocationDescription();

                        if (null != sendLocation.getAddressLines() && sendLocation.getAddressLines().Count > 0)
                        {
                            for (int i = 0; i < sendLocation.getAddressLines().Count; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        LocationDetail1TextBlock.Text = sendLocation.getAddressLines()[0];
                                        if (i + 1 == sendLocation.getAddressLines().Count && null != sendLocation.getPhoneNumber())
                                            LocationDetail2TextBlock.Text = sendLocation.getPhoneNumber();
                                        break;
                                    case 1:
                                        LocationDetail2TextBlock.Text = sendLocation.getAddressLines()[1];
                                        if (i + 1 == sendLocation.getAddressLines().Count && null != sendLocation.getPhoneNumber())
                                            LocationDetail3TextBlock.Text = sendLocation.getPhoneNumber();
                                        break;
                                    case 2:
                                        LocationDetail3TextBlock.Text = sendLocation.getAddressLines()[2] + ((sendLocation.getPhoneNumber() != null) ? " " +sendLocation.getPhoneNumber() : "");
                                        break;
                                }
                            }
                        }
                        else
                        {
                            if( null != sendLocation.getPhoneNumber())
                                LocationDetail1TextBlock.Text = sendLocation.getPhoneNumber();
                        }

                        setLocationImage();
                    }

                    Geopoint myPoint = new Windows.Devices.Geolocation.Geopoint(myPosition);
                    if (await MyMap.TrySetViewAsync(myPoint, 16D))
                    {
                        // Haven't really thought that through!
                    }

                    SetMapIcon(myPoint);                    
                    break;

                case GeolocationAccessStatus.Denied:


                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                    MessageDialog showDialog = new MessageDialog("Do you want to give Location Permission", (AppInstanceManager.activatedAppName != null) ? AppInstanceManager.activatedAppName : "Location Permission Required!");
                        showDialog.Commands.Add(new UICommand("Yes")
                        {
                            Id = 0
                        });
                        showDialog.Commands.Add(new UICommand("No")
                        {
                            Id = 1
                        });
                        showDialog.DefaultCommandIndex = 0;
                        showDialog.CancelCommandIndex = 1;

                        var dialog = await showDialog.ShowAsync();
                        if ((int)dialog.Id == 0)
                        {
                            AppUtils.ShowToastNotification("Location Permission!", "Please provide location permission and try again...",  3);

                            await Launcher.LaunchUriAsync(new Uri("ms-settings:privacy-location"));
                        }
                        else
                        {
                            AppUtils.ShowToastNotification("Location Permision denied!", "Please provide location permission to continue...", 3);
                        }
                    });
                    break;

                case GeolocationAccessStatus.Unspecified:
                    AppUtils.ShowToastNotification("Error!", "Please provide location permission for " + AppInstanceManager.activatedAppName, 3);

                    break;
            }
        }

        public void handleGraphics(AppItem appItem, Windows.UI.Xaml.Controls.Image imageHolder)
        {
            if (sendLocation == null) return;

            //try to get the currently cached bitmap
            BitmapImage myImage = appItem.graphicManager.getGraphic(sendLocation.getLocationImage().getValue());
            if (myImage != null)
            {
                imageHolder.Source = myImage;
            }

            //setup our input parameters, both path and holder
            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = imageHolder;
            inputGraphic.fileName = sendLocation.getLocationImage().getValue();

            //wait and listen for updates for our graphic bitmap from putfile, this is our softbutton graphic
            Action<OutputGraphic> graphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null && sendLocation != null)
                {
                    if (graphicStruct.appItem.getAppId() != sendLocation.getAppId()) return;
                    Windows.UI.Xaml.Controls.Image image = (Windows.UI.Xaml.Controls.Image)graphicStruct.holder;
                    image.Visibility = Visibility.Visible;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.Source = bitmapUpdate;
                }
            };

            //subscribe for updates for our graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(graphicDelegate, inputGraphic);
        }

        private void setLocationImage()
        {
            locationDynamicImage.Visibility = Visibility.Collapsed;
            locationStaticImage.Visibility = Visibility.Collapsed;

            if (sendLocation.getLocationImage() != null )
            {
                if (sendLocation.getLocationImage().getImageType() == ImageType.DYNAMIC)
                {
                    locationDynamicImage.Visibility = Visibility.Visible;
                    AppItem appItem = AppInstanceManager.AppInstance.getAppItem(sendLocation.getAppId());
                    if (appItem != null)
                    {
                        handleGraphics(appItem, locationDynamicImage);
                    }                    
                }
                else
                {
                    if ("" != sendLocation.getLocationImage().getValue() || null != sendLocation.getLocationImage().getValue())
                    {
                        locationStaticImage.Visibility = Visibility.Visible;

                        string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(sendLocation.getLocationImage().getValue());
                        if (staticImage != "" && null != staticImage)
                        {
                            locationStaticImage.Text = staticImage;
                        }
                        else
                        {
                            locationStaticImage.Text = "\uEB9F";
                        }
                    }
                    else
                    {
                        locationStaticImage.Text = "\uEB9F";
                    }
                }
            }
            else
            {
                locationDynamicImage.Visibility = Visibility.Visible;
                locationDynamicImage.Source = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId).appsIcon;
            }
        }
        
        private void SetMapIcon(Geopoint coordinate)
        {
            MapIcon mapIcon = new MapIcon
            {
                Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/LocPin.png")),
                Location = coordinate,
                NormalizedAnchorPoint = new Point(0.5, 1.0)
                //,Title = "Your current location"
            };
            MyMap.MapElements.Add(mapIcon);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            sendLocation = null;
            AppName.Text = "";
            BackStack.Visibility = Visibility.Collapsed;
            CallButton.Visibility = Visibility.Collapsed;
            LocationDescriptionTextBlock.Visibility = Visibility.Collapsed;
           
            //try to remove this page from the backstack
            try
            {
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (appInstanceManager != null && appInstanceManager.getAppItem(AppInstanceManager.activatedAppId) != null)
            {
                AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, appItem.getAppId()));
            }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);           
            sendLocation = (SendLocation)e.Parameter;

            if (sendLocation != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.HMI_OBSCURED, sendLocation.getAppId()));
                appInstanceManager.sendRpc(BuildDefaults.buildDefaultMessage(typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation), sendLocation.getId()));

                AppItem appItem = appInstanceManager.getAppItem(sendLocation.getAppId());
                
                if (appItem != null)
                {
                    AppName.Text = appItem.getAppName();
                }
                else
                {
                    AppName.Text = "";
                }
                
                BackStack.Visibility = Visibility.Visible;
                if (sendLocation.getPhoneNumber() == null || sendLocation.getPhoneNumber() == "")
                    CallButton.Visibility = Visibility.Collapsed;
                else
                    CallButton.Visibility = Visibility.Visible;
            }
            else
            {
                CallButton.Visibility = Visibility.Collapsed;
            }

            SetCurrentLocation();
        }

    }
}
