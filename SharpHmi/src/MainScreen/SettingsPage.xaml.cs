﻿using HmiApiLib;
using HmiApiLib.Handshaking;
using HmiApiLib.Types;
using SharpHmi.BC;
using SharpHmi.Navigation;
using SharpHmi.Buttons;
using SharpHmi.TTS;
using SharpHmi.src.Utility;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using SharpHmi.src.Rpc.UI;
using SharpHmi.src.Rpc.VehicleInfo;
using SharpHmi.src.Rpc.VR;
using SharpHmi.RC;
using Windows.Storage;
using System;
using Windows.Storage.AccessCache;
using HmiApiLib.Manager;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI;
using Windows.System;
using System.Linq;
using System.Xml;
using System.IO;
using Windows.ApplicationModel.Core;
using Windows.Storage.Streams;
using System.Collections.ObjectModel;
using System.Xml.XPath;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        private string RegisterButtonSubscription = ComponentPrefix.Buttons + "." + FunctionType.OnButtonSubscription;
        private string RegisterOnAppRegister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppRegistered;
        private string RegisterOnAppUnregister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppUnregistered;
        private string RegisterPutfile = ComponentPrefix.BasicCommunication + "." + FunctionType.OnPutFile;
        private string RegisterVideoStreaming = ComponentPrefix.Navigation + "." + FunctionType.OnVideoDataStreaming;
        private string RegisterAudioStreaming = ComponentPrefix.Navigation + "." + FunctionType.OnAudioDataStreaming;
        InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonsCapabilitiesResponse = null;
        static Windows.Storage.ApplicationDataContainer localSetting;
        public static ToggleSwitch toggleSwitch;

        public bool SwitchStatus
        {
            get
            {
                AppSetting appSetting = AppInstanceManager.AppInstance.appSetting;
                AppInstanceManager.SelectionMode mode = appSetting.getSelectedMode();
                if (mode == AppInstanceManager.SelectionMode.DEBUG_MODE)
                    return true;
                else
                    return false;
            }
            set
            {
                AppSetting appSetting = AppInstanceManager.AppInstance.appSetting;
                if (value == true)
                    appSetting.setSelectedMode(AppInstanceManager.SelectionMode.DEBUG_MODE);
                else
                    appSetting.setSelectedMode(AppInstanceManager.SelectionMode.BASIC_MODE);

            }
        }

        public SettingsPage()
        {
            InitializeComponent();
            this.DataContext = this;
            if (toggleSwitch!=null && toggleSwitch.IsOn == true) {
                EnableLogging.IsOn = true;

            }
            localSetting = Windows.Storage.ApplicationData.Current.LocalSettings;

            AppSetting appSetting = AppInstanceManager.AppInstance.appSetting;
            ServerAddressTextBox.Text = appSetting.getIPAddress();
            ServerPortTextBox.Text = appSetting.getTcpPort();
            VideoPortTextBox.Text = appSetting.getVideoPort();
            AudioPortTextBox.Text = appSetting.getAudioPort();

            InitialConnectionCommandConfig initConfig = appSetting.getInitialConnectionCommandConfig();
            if (initConfig != null)
            {
                buttonsCapabilitiesResponse = initConfig.getButtonsCapabilitiesResponseParam();
                List<InterfaceType> registerComponents = initConfig.getRegisterComponents();
                if (null != registerComponents)
                {
                    foreach (InterfaceType x in registerComponents)
                    {
                        switch (x)
                        {
                            case InterfaceType.UI:
                                RegisterUICheckbox.IsChecked = true;
                                break;
                            case InterfaceType.BasicCommunication:
                                RegisterBasicCommunicationCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.Buttons:
                                RegisterButtonsCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.VR:
                                RegisterVRCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.TTS:
                                RegisterTTSCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.Navigation:
                                RegisterNavigationCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.VehicleInfo:
                                RegisterVehicleInfoCheckbox.IsChecked = true;
                                break;
                            case InterfaceType.RC:
                                RegisterRCCheckbox.IsChecked = true;
                                break;
                        }
                    }
                }
                List<PropertyName> subscribeNotifications = initConfig.getSubscribeNotifications();
                if (null != subscribeNotifications)
                {
                    foreach (PropertyName x in subscribeNotifications)
                    {
                        if (x.propertyName.Equals(RegisterButtonSubscription))
                        {
                            SubscribeButtonSubscriptionCheckbox.IsChecked = true;
                        }
                        else if (x.propertyName.Equals(RegisterOnAppRegister))
                        {
                            SubscribeAppRegisterCheckbox.IsChecked = true;
                        }
                        else if (x.propertyName.Equals(RegisterOnAppUnregister))
                        {
                            SubscribeAppUnregisterCheckbox.IsChecked = true;
                        }
                        else if (x.propertyName.Equals(RegisterPutfile))
                        {
                            SubscribePutfileCheckbox.IsChecked = true;
                        }
                        else if (x.propertyName.Equals(RegisterVideoStreaming))
                        {
                            SubscribeVideoStreamingCheckbox.IsChecked = true;
                        }
                        else if (x.propertyName.Equals(RegisterAudioStreaming))
                        {
                            SubscribeAudioStreamingCheckbox.IsChecked = true;
                        }
                    }
                }
                BuildOnReadyCheckbox.IsChecked = initConfig.getIsBcOnReady();
            }
            BCMixingAudioSupportedCheckbox.IsChecked = appSetting.getBCMixAudioSupport();
            BCGetSystemInfoCheckbox.IsChecked = appSetting.getBCGetSystemInfo();
            ButtonsGetCapabilitiesCheckbox.IsChecked = appSetting.getButtonsGetCapabilities();
            NavigationIsReadyCheckbox.IsChecked = appSetting.getNavigationIsReady();
            RCGetCapabilitiesCheckbox.IsChecked = appSetting.getRCGetCapabilities();
            RCIsReadyCheckbox.IsChecked = appSetting.getRCIsReady();
            TTSGetCapabilitiesCheckbox.IsChecked = appSetting.getTTSGetCapabilities();
            TTSGetLanguageCheckbox.IsChecked = appSetting.getTTSGetLanguage();
            TTSGetSupportedLanguagesCheckbox.IsChecked = appSetting.getTTSGetSupportedLanguage();
            TTSIsReadyCheckbox.IsChecked = appSetting.getTTSIsReady();
            UIGetCapabilitiesCheckbox.IsChecked = appSetting.getUIGetCapabilities();
            UIGetLanguageCheckbox.IsChecked = appSetting.getUIGetLanguage();
            UIGetSupportedLanguageCheckbox.IsChecked = appSetting.getUIGetSupportedLanguage();
            UIIsReadyCheckbox.IsChecked = appSetting.getUIIsReady();
            VIGetVehicleDataCheckbox.IsChecked = appSetting.getVIGetVehicleData();
            VIGetVehicleTypeCheckbox.IsChecked = appSetting.getVIGetVehicleType();
            VIIsReadyCheckbox.IsChecked = appSetting.getVIIsReady();
            VRGetCapabilitiesCheckbox.IsChecked = appSetting.getVRGetCapabilities();
            VRGetLanguageCheckbox.IsChecked = appSetting.getVRGetLanguage();
            VRGetSupportedLanguagesCheckbox.IsChecked = appSetting.getVRGetSupportedLanguage();
            VRIsReadyCheckbox.IsChecked = appSetting.getVRIsReady();


            BCMixingAudioSupportedButton.IsEnabled = (bool)BCMixingAudioSupportedCheckbox.IsChecked;
            BCGetSystemInfoButton.IsEnabled = (bool)BCGetSystemInfoCheckbox.IsChecked;
            ButtonsGetCapabilitiesButton.IsEnabled = (bool)ButtonsGetCapabilitiesCheckbox.IsChecked;
            NavigationIsReadyButton.IsEnabled = (bool)NavigationIsReadyCheckbox.IsChecked;
            RCGetCapabilitiesButton.IsEnabled = (bool)RCGetCapabilitiesCheckbox.IsChecked;
            RCIsReadyButton.IsEnabled = (bool)RCIsReadyCheckbox.IsChecked;
            TTSGetCapabilitiesButton.IsEnabled = (bool)TTSGetCapabilitiesCheckbox.IsChecked;
            TTSGetLanguageButton.IsEnabled = (bool)TTSGetLanguageCheckbox.IsChecked;
            TTSGetSupportedLanguagesButton.IsEnabled = (bool)TTSGetSupportedLanguagesCheckbox.IsChecked;
            TTSIsReadyButton.IsEnabled = (bool)TTSIsReadyCheckbox.IsChecked;
            UIGetCapabilitiesButton.IsEnabled = (bool)UIGetCapabilitiesCheckbox.IsChecked;
            UIGetLanguageButton.IsEnabled = (bool)UIGetLanguageCheckbox.IsChecked;
            UIGetSupportedLanguagesButton.IsEnabled = (bool)UIGetSupportedLanguageCheckbox.IsChecked;
            UIIsReadyButton.IsEnabled = (bool)UIIsReadyCheckbox.IsChecked;
            VIGetVehicleDataButton.IsEnabled = (bool)VIGetVehicleDataCheckbox.IsChecked;
            VIGetVehicleTypeButton.IsEnabled = (bool)VIGetVehicleTypeCheckbox.IsChecked;
            VIIsReadyButton.IsEnabled = (bool)VIIsReadyCheckbox.IsChecked;
            VRGetCapabilitiesButton.IsEnabled = (bool)VRGetCapabilitiesCheckbox.IsChecked;
            VRGetLanguageButton.IsEnabled = (bool)VRGetLanguageCheckbox.IsChecked;
            VRGetSupportedLanguagesButton.IsEnabled = (bool)VRGetSupportedLanguagesCheckbox.IsChecked;
            VRIsReadyButton.IsEnabled = (bool)VRIsReadyCheckbox.IsChecked;

            if (AppInstanceManager.storageFolder != null)
            {
                Path.Text = AppInstanceManager.storageFolder.Path.ToString();
            }
            else
            {
                setupStorage();
            }
        }

        private async void setupStorage()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                try
                {
                    AppInstanceManager.storageFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("storage", CreationCollisionOption.OpenIfExists);
                    Path.Text = AppInstanceManager.storageFolder.Path.ToString();
                }
                catch (Exception ex)
                {
                    AppInstanceManager.storageFolder = null;
                }
            });
        }

        private void BCGetSystemInfoButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            BCGetSystemInfoResponse bcGetSystemInfoResponse = new BCGetSystemInfoResponse();
            bcGetSystemInfoResponse.ShowGetSystemInfo();
        }

        private void BCMixingAudioSupportedButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            BCMixingAudioSupportedResponse bCMixingAudioSupportedResponse = new BCMixingAudioSupportedResponse();
            bCMixingAudioSupportedResponse.ShowMixingAudioSupported();
        }

        private void ButtonsGetCapabilitiesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            ButtonsGetCapabilitiesResponse buttonsGetCapabilitiesResponse = new ButtonsGetCapabilitiesResponse();
            buttonsGetCapabilitiesResponse.ShowGetCapabilities();
        }

        private void NavigationIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            NavigationIsReadyResponse navigationIsReadyResponse = new NavigationIsReadyResponse();
            navigationIsReadyResponse.ShowIsReady();
        }

        private void RCGetCapabilitiesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            RCGetCapabilitiesResponse rcGetCapabilitiesResponse = new RCGetCapabilitiesResponse();
            rcGetCapabilitiesResponse.ShowGetCapabilities();
        }

        private void RCIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            RCIsReadyResponse rcIsReadyResponse = new RCIsReadyResponse();
            rcIsReadyResponse.ShowIsReady();
        }

        private void TTSGetCapabilitiesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            TTSGetCapabilitiesResponse ttsGetCapabilitiesResponse = new TTSGetCapabilitiesResponse();
            ttsGetCapabilitiesResponse.ShowGetCapabilities();
        }
        private void TTSGetLanguageButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            TTSGetLanguageResponse ttsGetLanguageResponse = new TTSGetLanguageResponse();
            ttsGetLanguageResponse.ShowGetLanguage();
        }

        private void TTSGetSupportedLanguagesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            TTSGetSupportedLanguagesResponse ttsGetSupportedLanguagesResponse = new TTSGetSupportedLanguagesResponse();
            ttsGetSupportedLanguagesResponse.ShowGetSupportedLanguages();
        }

        private void TTSIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            TTSIsReadyResponse ttsIsReadyResponse = new TTSIsReadyResponse();
            ttsIsReadyResponse.ShowIsReady();
        }

        private void UIGetCapabilitiesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            UIGetCapabilitiesResponse uiGetCapabilitiesResponse = new UIGetCapabilitiesResponse();
            uiGetCapabilitiesResponse.ShowGetCapabilities();
        }

        private void UIGetLanguageButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            UIGetLanguageResponse uiGetLanguageResponse = new UIGetLanguageResponse();
            uiGetLanguageResponse.ShowGetLanguage();
        }

        private void UIGetSupportedLanguagesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            UIGetSupportedLanguagesResponse uiGetSupportedLanguagesResponse = new UIGetSupportedLanguagesResponse();
            uiGetSupportedLanguagesResponse.ShowGetSupportedLanguages();
        }

        private void UIIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            UIIsReadyResponse uiIsReadyResponse = new UIIsReadyResponse();
            uiIsReadyResponse.ShowIsReady();
        }

        private void VIGetVehicleDataButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VIGetVehicleDataResponse viGetVehicleDataResponse = new VIGetVehicleDataResponse();
            viGetVehicleDataResponse.ShowGetVehicleDataResponse();
        }

        private void VIGetVehicleTypeTapped(object sender, TappedRoutedEventArgs e)
        {
            VIGetVehicleTypeResponse viGetVehicleTypeResponse = new VIGetVehicleTypeResponse();
            viGetVehicleTypeResponse.ShowGetVehicleType();
        }

        private void VIIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VIIsReadyResponse viIsReadyResponse = new VIIsReadyResponse();
            viIsReadyResponse.ShowIsReady();
        }

        private void VRGetCapabilitiesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VRGetCapabilitiesResponse vrGetCapabilitiesResponse = new VRGetCapabilitiesResponse();
            vrGetCapabilitiesResponse.ShowGetCapabilities();
        }

        private void VRGetLanguageButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VRGetLanguageResponse vrGetLanguageResponse = new VRGetLanguageResponse();
            vrGetLanguageResponse.ShowGetLanguage();
        }

        private void VRGetSupportedLanguagesButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VRGetSupportedLanguagesResponse vrGetSupportedLanguagesResponse = new VRGetSupportedLanguagesResponse();
            vrGetSupportedLanguagesResponse.ShowGetSupportedLanguages();
        }

        private void VRIsReadyButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            VRIsReadyResponse vrIsReadyResponse = new VRIsReadyResponse();
            vrIsReadyResponse.ShowIsReady();
        }

        private void ConnectButtonTapped(object sender, TappedRoutedEventArgs e)
        {
           

            //Naviagte to Console View
            MainPage.consoleMenuListItem.IsSelected = true;
            Frame.Navigate(typeof(AppsPage));

            Frame.Navigate(typeof(ConsolePage));
            
            List<InterfaceType> selectedEnumList = new List<InterfaceType>();
            if (RegisterUICheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.UI);
            }
            if (RegisterBasicCommunicationCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.BasicCommunication);
            }
            if (RegisterButtonsCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.Buttons);
            }
            if (RegisterVRCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.VR);
            }
            if (RegisterTTSCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.TTS);
            }
            if (RegisterNavigationCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.Navigation);
            }
            if (RegisterVehicleInfoCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.VehicleInfo);
            }
            if (RegisterRCCheckbox.IsChecked == true)
            {
                selectedEnumList.Add(InterfaceType.RC);
            }

            List<PropertyName> propertyNameList = new List<PropertyName>();
            PropertyName propertyName = new PropertyName();
            if (SubscribeButtonSubscriptionCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.Buttons, FunctionType.OnButtonSubscription);
                propertyNameList.Add(propertyName);
            }
            propertyName = new PropertyName();
            if (SubscribeAppRegisterCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppRegistered);
                propertyNameList.Add(propertyName);
            }
            propertyName = new PropertyName();
            if (SubscribeAppUnregisterCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppUnregistered);
                propertyNameList.Add(propertyName);
            }
            propertyName = new PropertyName();
            if (SubscribePutfileCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnPutFile);
                propertyNameList.Add(propertyName);
            }
            propertyName = new PropertyName();
            if (SubscribeVideoStreamingCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnVideoDataStreaming);
                propertyNameList.Add(propertyName);
            }
            propertyName = new PropertyName();
            if (SubscribeAudioStreamingCheckbox.IsChecked == true)
            {
                propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnAudioDataStreaming);
                propertyNameList.Add(propertyName);
            }

            InitialConnectionCommandConfig initialConfig = new InitialConnectionCommandConfig();
            initialConfig.setRegisterComponents(selectedEnumList);
            initialConfig.setSubscribeNotifications(propertyNameList);
            initialConfig.setIsBcOnReady((bool)BuildOnReadyCheckbox.IsChecked);
            initialConfig.setButtonsCapabilitiesResponseParam(buttonsCapabilitiesResponse);

            string objStr = Newtonsoft.Json.JsonConvert.SerializeObject(initialConfig);

            int portNum = int.Parse(ServerPortTextBox.Text);
            AppInstanceManager.AppInstance.SetUpConnection(ServerAddressTextBox.Text, portNum, initialConfig);

            Windows.Storage.ApplicationDataContainer localSetting =
                Windows.Storage.ApplicationData.Current.LocalSettings;

            localSetting.Values[Const.PREFS_KEY_TRANSPORT_IP] = ServerAddressTextBox.Text;
            localSetting.Values[Const.PREFS_KEY_TRANSPORT_PORT] = ServerPortTextBox.Text;
            localSetting.Values[Const.PREFS_KEY_VIDEO_PORT] = VideoPortTextBox.Text;
            localSetting.Values[Const.PREFS_KEY_AUDIO_PORT] = AudioPortTextBox.Text;

            localSetting.Values[Const.PREFS_KEY_INITIAL_CONFIG] = objStr;
            localSetting.Values[Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED] = BCMixingAudioSupportedCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_BC_GET_SYSTEM_INFO] = BCGetSystemInfoCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES] = ButtonsGetCapabilitiesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_NAVIGATION_IS_READY] = NavigationIsReadyCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_RC_GET_CAPABILITIES] = RCGetCapabilitiesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_RC_IS_READY] = RCIsReadyCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_TTS_GET_CAPABILITIES] = TTSGetCapabilitiesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_TTS_GET_LANGUAGE] = TTSGetLanguageCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE] = TTSGetSupportedLanguagesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_TTS_IS_READY] = TTSIsReadyCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_UI_GET_CAPABILITIES] = UIGetCapabilitiesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_UI_GET_LANGUAGE] = UIGetLanguageCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE] = UIGetSupportedLanguageCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_UI_IS_READY] = UIIsReadyCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_DATA] = VIGetVehicleDataCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_TYPE] = VIGetVehicleTypeCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VI_IS_READY] = VIIsReadyCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VR_GET_CAPABILITIES] = VRGetCapabilitiesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VR_GET_LANGUAGE] = VRGetLanguageCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE] = VRGetSupportedLanguagesCheckbox.IsChecked;
            localSetting.Values[Const.PREFS_KEY_VR_IS_READY] = VRIsReadyCheckbox.IsChecked;
            if (AppInstanceManager.storageFolder == null && !StorageApplicationPermissions.FutureAccessList.ContainsItem("PickedFolderToken"))
            {
                ErrorMsg(Const.NULL_PATH_ERROR_MSG);
                
            }
        }
        
        private void BCMixingAudioSupportedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BCMixingAudioSupportedButton.IsEnabled = (bool)BCMixingAudioSupportedCheckbox.IsChecked;
        }

        private void BCGetSystemInfoCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BCGetSystemInfoButton.IsEnabled = (bool)BCGetSystemInfoCheckbox.IsChecked;
        }

        private void ButtonsGetCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonsGetCapabilitiesButton.IsEnabled = (bool)ButtonsGetCapabilitiesCheckbox.IsChecked;
        }

        private void NavigationIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NavigationIsReadyButton.IsEnabled = (bool)NavigationIsReadyCheckbox.IsChecked;
        }
        private void RCGetCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RCGetCapabilitiesButton.IsEnabled = (bool)RCGetCapabilitiesCheckbox.IsChecked;
        }

        private void RCIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RCIsReadyButton.IsEnabled = (bool)RCIsReadyCheckbox.IsChecked;
        }

        private void TTSGetCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TTSGetCapabilitiesButton.IsEnabled = (bool)TTSGetCapabilitiesCheckbox.IsChecked;
        }

        private void TTSGetLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TTSGetLanguageButton.IsEnabled = (bool)TTSGetLanguageCheckbox.IsChecked;
        }

        private void TTSGetSupportedLanguagesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TTSGetSupportedLanguagesButton.IsEnabled = (bool)TTSGetSupportedLanguagesCheckbox.IsChecked;
        }

        private void TTSIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TTSIsReadyButton.IsEnabled = (bool)TTSIsReadyCheckbox.IsChecked;
        }

        private void UIGetCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UIGetCapabilitiesButton.IsEnabled = (bool)UIGetCapabilitiesCheckbox.IsChecked;
        }

        private void UIGetLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UIGetLanguageButton.IsEnabled = (bool)UIGetLanguageCheckbox.IsChecked;
        }

        private void UIGetSupportedLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UIGetSupportedLanguagesButton.IsEnabled = (bool)UIGetSupportedLanguageCheckbox.IsChecked;
        }

        private void UIIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UIIsReadyButton.IsEnabled = (bool)UIIsReadyCheckbox.IsChecked;
        }

        private void VIGetVehicleDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VIGetVehicleDataButton.IsEnabled = (bool)VIGetVehicleDataCheckbox.IsChecked;
        }

        private void VIGetVehicleTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VIGetVehicleTypeButton.IsEnabled = (bool)VIGetVehicleTypeCheckbox.IsChecked;
        }

        private void VIIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VIIsReadyButton.IsEnabled = (bool)VIIsReadyCheckbox.IsChecked;
        }

        private void VRGetCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VRGetCapabilitiesButton.IsEnabled = (bool)VRGetCapabilitiesCheckbox.IsChecked;
        }

        private void VRGetLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VRGetLanguageButton.IsEnabled = (bool) VRGetLanguageCheckbox.IsChecked;
        }

        private void VRGetSupportedLanguagesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VRGetSupportedLanguagesButton.IsEnabled = (bool) VRGetSupportedLanguagesCheckbox.IsChecked;
        }

        private void VRIsReadyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VRIsReadyButton.IsEnabled = (bool) VRIsReadyCheckbox.IsChecked;
        }

        private void SwitchRPCResponseMode(object sender, RoutedEventArgs e) //allow the user to toggle the RPC response mode
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    AppSetting appSetting = AppInstanceManager.AppInstance.appSetting;
                    appSetting.setSelectedMode(AppInstanceManager.SelectionMode.DEBUG_MODE);
                }
                else
                {
                    AppSetting appSetting = AppInstanceManager.AppInstance.appSetting;
                    appSetting.setSelectedMode(AppInstanceManager.SelectionMode.BASIC_MODE);
                }
            }


        }

        private async void PathButtonTappedAsync(object sender, TappedRoutedEventArgs e)
        {
            Path.IsEnabled = true;
            var folderPicker = new Windows.Storage.Pickers.FolderPicker();
            folderPicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            folderPicker.FileTypeFilter.Add("*");
            AppInstanceManager.storageFolder = await folderPicker.PickSingleFolderAsync();

            if (AppInstanceManager.storageFolder != null)
            {

                // Application now has read/write access to all contents in the picked folder
                // (including other sub-folder contents)
                StorageApplicationPermissions.FutureAccessList.AddOrReplace("PickedFolderToken", AppInstanceManager.storageFolder);
                AppInstanceManager.storageFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync("PickedFolderToken");
                Path.Text = AppInstanceManager.storageFolder.Path.ToString();
            }
            else
            {
                ErrorMsg(Const.PATH_ERROR_MSG);
            }

        }

        public async void OpenFileLocation(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                StorageFolder file = Windows.Storage.ApplicationData.Current.LocalFolder;
                await Windows.System.Launcher.LaunchFolderAsync(file);
            }
            catch  { }
           
        }



        public void ErrorMsg(String PATH_ERROR_MSG) {
            LogMessage logMessage = new StringLogMessage(PATH_ERROR_MSG);
            Message msg = new Message();

            StringLogMessage myStringLog = (StringLogMessage)logMessage;
            msg.lblTop = myStringLog.getMessage();
            msg.lblTime = myStringLog.getDate();
            msg.lblTopColor = new SolidColorBrush(Colors.Red);
            AppInstanceManager.AppInstance._msgAdapter.Add(msg);
        }

        private void updateServerAddressDefaultText(object sender, TextChangedEventArgs e)
        {
            localSetting.Values[Const.PREFS_KEY_TRANSPORT_IP] = ServerAddressTextBox.Text;
        }

        private void updateServerPortDefaultText(object sender, TextChangedEventArgs e)
        {
            localSetting.Values[Const.PREFS_KEY_TRANSPORT_PORT] = ServerPortTextBox.Text;
        }

        private void updateVideoPortDefaultText(object sender, TextChangedEventArgs e)
        {
            localSetting.Values[Const.PREFS_KEY_VIDEO_PORT] = VideoPortTextBox.Text;
        }

        private void updateAudioPortDefaultText(object sender, TextChangedEventArgs e)
        {
            localSetting.Values[Const.PREFS_KEY_AUDIO_PORT] = AudioPortTextBox.Text;
        }
        public async void createNewFile()
        {
            try
            {
                string fname = AppInstanceManager.AppInstance.LogsfileName;
                StorageFolder localizationDirectory = Windows.Storage.ApplicationData.Current.LocalFolder;
                StorageFile file = null;
                try
                {

                    file = await localizationDirectory.GetFileAsync(fname);

                }
                catch { }
                if (file == null || !File.Exists(file.Path))
                {

                    file = await localizationDirectory.CreateFileAsync(fname, CreationCollisionOption.ReplaceExisting);
                    System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
                    settings.Async = true;
                    settings.Indent = true;
                    settings.NewLineOnAttributes = true;
                    System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(file.Path, settings);
                    writer.WriteStartDocument();
                    writer.WriteStartElement("LogsXml");
                    writer.WriteEndDocument();
                    writer.Close();


                }
            }
            catch (Exception e)
            { }
        }

        private void ToggleSwitch_Toggled(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
             toggleSwitch = sender as ToggleSwitch;
             try
             {

                 if (toggleSwitch != null)
                {
                 if (toggleSwitch.IsOn == true)
                 {
                    createNewFile();
                 }

                }
             }
             catch (Exception ex) { }

        }

        private void AudioPassThruSettingRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton audioPassThruSettingRadioBtn = sender as RadioButton;
            if(null != audioPassThruSettingRadioBtn)
            {
                string tag = audioPassThruSettingRadioBtn.Tag.ToString();
                if (tag == "SpeechRecogintion")
                    AppInstanceManager.AppInstance.audioPassThruWithSpeechAPI = true;
                else
                    AppInstanceManager.AppInstance.audioPassThruWithSpeechAPI = false;
            }
        }
    }
}
