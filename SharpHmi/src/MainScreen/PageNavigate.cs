﻿using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHmi.src.MainScreen
{
  public  class PageNavigate
    {
        public Show InfoType;
        public string Info;
        public ScrollableMessage scrollableMsg;
        public Alert alertMsg;
        public string redirectFrom;
        public ObservableCollection<OnButtonSubscription> buttonSubscription;
        public AddSubMenu subMenus;
        public Slider slider;
        public SetGlobalProperties setGlobalProperties;
       public ObservableCollection<MenuIcon> subItems;
        public int? menuId;
        public List<string> customPresets;
    }
}