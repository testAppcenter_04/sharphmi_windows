﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace SharpHmi.src.MainScreen
{
    public sealed partial class SliderRPCPage : Page, IRefreshListener
    {
        private HmiApiLib.Controllers.UI.IncomingRequests.Slider parameters = null;
        private bool rpcResponseSent = false;
        AppInstanceManager appInstanceManager;

        public SliderRPCPage()
        {
            this.InitializeComponent();
            appInstanceManager = AppInstanceManager.AppInstance;
            appInstanceManager.addRefreshListener(this, typeof(HmiApiLib.Controllers.UI.IncomingRequests.Slider));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rpcResponseSent = false;

            base.OnNavigatedTo(e);

            parameters = (HmiApiLib.Controllers.UI.IncomingRequests.Slider)e.Parameter;

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.HMI_OBSCURED, parameters.getAppId()));
            }

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(parameters.getAppId());
            AppNameTextBlock.Text = (appItem != null) ? AppInstanceManager.AppInstance.getAppItem(parameters.getAppId()).getAppName() : "";
            displayText(parameters);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (!rpcResponseSent) //if we previously never sent a response, go ahead and abort the rpc
            {
                RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED, null);
                sendRpcInternal(sliderResponse);
            }

            //try to remove this page from the backstack
            try
            {
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, parameters.getAppId()));
            }
        }

        private void sendRpcInternal(RpcMessage msg)
        {
            rpcResponseSent = true;
            AppInstanceManager.AppInstance.sendRpc(msg);
        }

        private void displayText(HmiApiLib.Controllers.UI.IncomingRequests.Slider msg)
        {
            if (msg.getNumTicks() != null)
            {
                MySlider.Maximum = (int)msg.getNumTicks();
            }
            if (msg.getPosition() != null)
            {
                MySlider.Value = (int)msg.getPosition();
            }
            if (msg.getSliderHeader() != null)
            {
                HeaderTextBlock.Text = msg.getSliderHeader();
            }
            if (msg.getSliderFooter() != null)
            {
                if (msg.getSliderFooter().Count > 0)
                { 
                    FooterTextBlock.Text = (msg.getSliderFooter().ElementAt(0));
                }
            }

            if(msg.getTimeout() != null)
                timeOut(msg.getTimeout());

        }

        private async void timeOut(int? time)
        {
            await Task.Delay(TimeSpan.FromMilliseconds((double)time));

            if (parameters != null)
            {
                RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.TIMED_OUT, null);
                sendRpcInternal(sliderResponse);
            }

            if (Frame.CurrentSourcePageType == typeof(SliderRPCPage))
                Frame.GoBack();
        }
        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
            {
                RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED, null);
                sendRpcInternal(sliderResponse);
            }

            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void MySlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            try
            {
                double sliderValue = 0;

                Slider winSlider = sender as Slider;
                if (winSlider != null)
                {
                    sliderValue = winSlider.Value;

                    if (parameters != null)
                    {
                        if (parameters.getSliderFooter() != null)
                        {
                            if (parameters.getSliderFooter().Count >= sliderValue - 1)
                            {
                                FooterTextBlock.Text = parameters.getSliderFooter().ElementAt(Convert.ToInt32(sliderValue - 1));
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            { }
        }

        private void SaveButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
            {
                RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.SUCCESS, Convert.ToInt32(MySlider.Value));
                sendRpcInternal(sliderResponse);
            }

            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void CancelButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parameters != null)
            {
                RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED, null);
                sendRpcInternal(sliderResponse);
            }

            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState dataState)
        {
            if (rpcType == typeof(HmiApiLib.Controllers.UI.IncomingRequests.Slider))
            {
                if (!rpcResponseSent && dataState == RefreshDataState.BEFORE_DATA_CHANGE)  //if we previously never sent a response, go ahead and abort the rpc
                {
                    RpcResponse sliderResponse = BuildRpc.buildUiSliderResponse(parameters.getId(), HmiApiLib.Common.Enums.Result.ABORTED, null);
                    sendRpcInternal(sliderResponse);
                }
            }
        }
    }
}
