﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using static SharpHmi.src.Manager.GraphicManager;

namespace SharpHmi
{
    public sealed partial class Perform_Interaction : Page, IRefreshListener
    {
        TextFieldStruct textFieldStruct;
        string textStruct;
        PerformInteraction parameters;

        private TrulyObservableCollection<PerformInteractionChoice> performInteractionChoicesList;
        private bool rpcResponseSent = false;
        AppInstanceManager appInstanceManager;

        public Perform_Interaction()
        {
            this.InitializeComponent();
            appInstanceManager = AppInstanceManager.AppInstance;
            appInstanceManager.addRefreshListener(this, typeof(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction));
        }

        private void sendRpcInternal(RpcMessage msg)
        {
            rpcResponseSent = true;
            AppInstanceManager.AppInstance.sendRpc(msg);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (!rpcResponseSent) //if we previously never sent a response, go ahead and abort the rpc
            {
                RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, null, HmiApiLib.Common.Enums.Result.ABORTED);
                sendRpcInternal(uiPerformInteraction);

                RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.ABORTED);
                sendRpcInternal(vrPerformInteraction);
            }

            //try to remove this page from the backstack
            try
            {
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, parameters.getAppId()));
            }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rpcResponseSent = false;

            base.OnNavigatedTo(e);
            parameters = (PerformInteraction)e.Parameter;

            if (appInstanceManager != null && parameters != null)
            {
                appInstanceManager.sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.HMI_OBSCURED, parameters.getAppId()));
            }

            List<Choice> choices = parameters.getChoiceSet();
            performInteractionChoicesList = new TrulyObservableCollection<PerformInteractionChoice>();

            foreach (Choice choice in choices)
            {
                if (choice != null)
                {
                    PerformInteractionChoice performInteractionChoice = new PerformInteractionChoice();
                    performInteractionChoice.Choice = choice;
                    performInteractionChoice.Choice.menuName = choice.getMenuName();
                    performInteractionChoice.Choice.secondaryText = choice.getSecondaryText();
                    performInteractionChoice.Choice.tertiaryText = choice.getTertiaryText();

                    if (choice.getImage() != null)
                    {
                        if (choice.getImage().getImageType() == ImageType.STATIC)
                        {
                            if ("" != choice.getImage().getValue().Trim() || null != choice.getImage().getValue())
                            {
                                string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(choice.getImage().getValue());
                                if (staticImage != "" || null == staticImage)
                                {
                                    performInteractionChoice.StaticIconVal = (string)AppInstanceManager.AppInstance.staticImageVal(choice.getImage().getValue());
                                }
                                else
                                {
                                    performInteractionChoice.StaticIconVal = "\uEB9F";
                                }
                            }
                            else
                            {
                                performInteractionChoice.StaticIconVal = "\uEB9F";
                            }
                        }
                    }

                    performInteractionChoicesList.Add(performInteractionChoice);
                }
            }

            //Let's populate dynamic images if any
            populateDynamicImagesAsync();

            if (parameters.getInteractionLayout() == HmiApiLib.Common.Enums.LayoutMode.KEYBOARD)
            {
                KeyboardScreenStackPanel.Visibility = Visibility.Visible;
            }
            else if (parameters.getInteractionLayout() == HmiApiLib.Common.Enums.LayoutMode.ICON_ONLY)
            {
                IconOnlyStackPanel.Visibility = Visibility.Visible;
                textFieldStruct = parameters.getInitialText();
                textStruct = String.Format(textFieldStruct.fieldText);
            }
            else if (parameters.getInteractionLayout() == HmiApiLib.Common.Enums.LayoutMode.ICON_WITH_SEARCH)
            {
                IconWithSearchStackPanel.Visibility = Visibility.Visible;
                textFieldStruct = parameters.getInitialText();
                textStruct = String.Format(textFieldStruct.fieldText);
            }
            else if (parameters.getInteractionLayout() == HmiApiLib.Common.Enums.LayoutMode.LIST_ONLY)
            {
                ListOnlyStackPanel.Visibility = Visibility.Visible;
            }
            else if (parameters.getInteractionLayout() == HmiApiLib.Common.Enums.LayoutMode.LIST_WITH_SEARCH)
            {
                ListWithSearchStackPanel.Visibility = Visibility.Visible;
            }
        }

        private async Task handleGraphicsAsync(AppItem appItem, PerformInteractionChoice piChoice)
        {
            //try to get the currently cached bitmap
            BitmapImage myImage = appItem.graphicManager.getGraphic(piChoice.Choice.getImage().getValue());
            if (myImage != null)
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    piChoice.DynamicImageSrc = myImage;
                });
            }

            //setup our input parameters, both path and holder
            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = piChoice;
            inputGraphic.fileName = piChoice.Choice.getImage().getValue();

            //wait and listen for updates for our graphic bitmap from putfile
            Action<OutputGraphic> graphicDelegate = async delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null && parameters != null)
                {
                    if (graphicStruct.appItem.getAppId() != parameters.getAppId()) return;
                    PerformInteractionChoice holder = (PerformInteractionChoice)graphicStruct.holder;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        holder.DynamicImageSrc = bitmapUpdate;
                    });
                }
            };

            //subscribe for updates for our graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(graphicDelegate, inputGraphic);
        }

        public async void populateDynamicImagesAsync()
        {
            if (parameters == null) return;

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(parameters.getAppId());

            if (appItem == null) return;

            foreach (PerformInteractionChoice performInteractionChoice in performInteractionChoicesList)
            {
                if (performInteractionChoice != null && performInteractionChoice.Choice != null 
                    && performInteractionChoice.Choice.getImage() != null && (performInteractionChoice.Choice.getImage().getImageType() == ImageType.DYNAMIC)
                    && performInteractionChoice.Choice.getImage().getValue() != null)
                {
                    await handleGraphicsAsync(appItem, performInteractionChoice);
                }
            }

            GridViewIconOnly.ItemsSource = performInteractionChoicesList;
            GridViewIconWithSearch.ItemsSource = performInteractionChoicesList;
            GridViewListOnly.ItemsSource = performInteractionChoicesList;
            GridViewListWithSearch.ItemsSource = performInteractionChoicesList;
            GridViewKeyboardScreen.ItemsSource = performInteractionChoicesList;
        }

        private async void focusTxtBox1()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                SearchKeywordTextBox.Focus(FocusState.Programmatic);
            });
        }

        private async void focusTxtBox2()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                IconWithSearchKeywordTextBox.Focus(FocusState.Programmatic);
            });
        }

        private async void focusTxtBox3()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                ListWithSearchKeywordTextBox.Focus(FocusState.Programmatic);
            });
        }

        private void AbortPerformInteraction()
        {
            RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.ABORTED);
            sendRpcInternal(vrPerformInteraction);

            RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, null, HmiApiLib.Common.Enums.Result.ABORTED);
            sendRpcInternal(uiPerformInteraction);

            goBack();
        }

        private void SpeakSearchTapped()
        {
            HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput msg = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput();
            msg.setKeyboardEvent(KeyboardEvent.ENTRY_VOICE);
            sendRpcInternal(msg);

            AbortPerformInteraction();
        }

        private void AbortPerformInteraction_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AbortPerformInteraction();
        }

        private void ChoiceMenuButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            foreach (PerformInteractionChoice performInteractionChoice in performInteractionChoicesList)
            {
                if ((sender as Button).Name.Equals(performInteractionChoice.Choice.getMenuName()))
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, performInteractionChoice.Choice.getChoiceID(), HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), performInteractionChoice.Choice.getChoiceID(), null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
            }
        }

        private void SpeakIconWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SpeakSearchTapped();
        }

        private void OnKeyDownIconWithSearchKeywordTextBox(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, IconWithSearchKeywordTextBox.Text, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
                catch
                {

                }
            }
        }

        private void ClearIconWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            IconWithSearchKeywordTextBox.Text += "";
        }

        private void ChoiceMenuIconWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            foreach (PerformInteractionChoice performInteractionChoice in performInteractionChoicesList)
            {
                if ((sender as Button).Name.Equals(performInteractionChoice.Choice.getMenuName()))
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, performInteractionChoice.Choice.getChoiceID(), HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), performInteractionChoice.Choice.getChoiceID(), null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
            }
        }

        private void ListOnlyButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            foreach (PerformInteractionChoice performInteractionChoice in performInteractionChoicesList)
            {
                if ((sender as Button).Name.Equals(performInteractionChoice.Choice.getMenuName()))
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, performInteractionChoice.Choice.getChoiceID(), HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), performInteractionChoice.Choice.getChoiceID(), null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
            }
        }

        private void SpeakListWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SpeakSearchTapped();
        }

        private void OnKeyDownListWithSearchKeywordTextBox(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, ListWithSearchKeywordTextBox.Text, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
                catch
                {

                }
            }
        }

        private void ClearListWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ListWithSearchKeywordTextBox.Text += "";
        }

        private void ListWithSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            foreach (PerformInteractionChoice performInteractionChoice in performInteractionChoicesList)
            {
                if ((sender as Button).Name.Equals(performInteractionChoice.Choice.getMenuName()))
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, performInteractionChoice.Choice.getChoiceID(), HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), performInteractionChoice.Choice.getChoiceID(), null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
            }
        }

        private void SpeakSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SpeakSearchTapped();
        }

        private void OnKeyDownSearchKeywordTextBox(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(vrPerformInteraction);

                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, SearchKeywordTextBox.Text, HmiApiLib.Common.Enums.Result.SUCCESS);
                    sendRpcInternal(uiPerformInteraction);

                    goBack();
                }
                catch
                {

                }
            }
        }

        private void ClearSearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SearchKeywordTextBox.Text = "";
            IconWithSearchKeywordTextBox.Text = "";
            ListWithSearchKeywordTextBox.Text = "";
            SearchKeywordTextBox.Text = "";
        }

        private void backOnePage()
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        public async void goBack()
        {
               await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
            () =>
            {
                backOnePage();
            });
        }

        private void SearchKeywordTextBox_Loaded(object sender, RoutedEventArgs e)
        {
            focusTxtBox1();
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState dataState)
        {
          //  if (appID != AppInstanceManager.fullHMIAppId) no need to check full app id since perform interaction can also occur in other hmi states like limited
          //      return;

            if (rpcType == typeof(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction))
            {
                if (!rpcResponseSent && dataState == RefreshDataState.BEFORE_DATA_CHANGE)  //if we previously never sent a response, go ahead and abort the rpc
                {
                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(parameters.getId(), null, null, HmiApiLib.Common.Enums.Result.ABORTED);
                    sendRpcInternal(uiPerformInteraction);

                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(AppInstanceManager.AppInstance.vrPerformInteractionCorrId, null, HmiApiLib.Common.Enums.Result.ABORTED);
                    sendRpcInternal(vrPerformInteraction);
                }
            }
        }
    }
}
