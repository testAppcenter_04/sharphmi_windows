﻿using HmiApiLib;
using SharpHmi.BC;
using SharpHmi.Buttons;
using SharpHmi.Navigation;
using SharpHmi.RC;
using SharpHmi.SDl;
using SharpHmi.src.Rpc.UI;
using SharpHmi.src.Utility;
using SharpHmi.TTS;
using SharpHmi.src.Rpc.VR;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using SharpHmi.src.Rpc.VehicleInfo;
using Windows.ApplicationModel.Core;
using System.Collections.Generic;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Data;
using System.Diagnostics;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConsolePage : Page
    {
        private ObservableCollection<Message> LogsList = null;

        private static String[] RpcTypesArray = new String[] { "All", "Basic Communication", "Buttons", "Navigation", "RC", "SDL", "TTS", "UI", "VehicleInfo", "VR" };

        private static String[] RpcListArray;
        private static String[] basicCommunicationListArray ;

        private static String[] buttonsListArray;

        private static String[] navigationListArray ;

        private static String[] buttonsCommunicationListArray ;

        private static String[] rcListArray;

        private static String[] sdlListArray;

        public static String[] ttsListArray;

        public static String[] uiListArray;

        public static String[] viListArray ;
        public static String[] vrListArray ;


        //private List<String> RpcListObservableCollection;

        public ConsolePage()
        {
            this.InitializeComponent();
            InitializeData();
           
            ConsoleLogsListView.ItemClick += onClick;
            ConsoleLogsListView.IsItemClickEnabled = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }
        public async void InitializeData() {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if(RpcListArray==null)
                   RpcListArray= new String[]{"BC.ActivateApp","BC.AllowDeviceToConnect","BC.DecryptCertificate",
            "BC.DialNumber", "BC.GetSystemInfo", "BC.MixingAudioSupported", "BC.PolicyUpdate", "BC.SystemRequest", "BC.UpdateAppList",
            "BC.UpdateDeviceList", "BC.OnAppActivated", "BC.OnAppDeactivated", "BC.OnAwakeSDL", "BC.OnDeactivateHMI", "BC.OnDeviceChosen",
            "BC.OnEmergencyEvent", "BC.OnEventChanged", "BC.OnExitAllApplications", "BC.OnExitApplication", "BC.OnFindApplications",
            "BC.OnIgnitionCycleOver", "BC.OnPhoneCall", "BC.OnReady", "BC.OnStartDeviceDiscovery", "BC.OnSystemInfoChanged", "BC.OnSystemRequest",
            "BC.OnUpdateDeviceList", "Buttons.GetCapabilities", "Buttons.ButtonPress", "Buttons.OnButtonEvent", "Buttons.OnButtonPress", "Navigation.AlertManeuver",
            "Navigation.GetWayPoints", "Navigation.IsReady", "Navigation.SendLocation", "Navigation.ShowConstantTBT", "Navigation.StartAudioStream",
            "Navigation.StartStream", "Navigation.StopAudioStream", "Navigation.StopStream", "Navigation.SubscribeWayPoints", "Navigation.UnsubscribeWayPoints",
            "Navigation.UpdateTurnList", "Navigation.OnTBTClientState", "RC.GetCapabilities", "RC.GetInteriorVehicleData", "RC.GetInteriorVehicleDataConsent", "RC.IsReady",
            "RC.SetInteriorVehicleData", "RC.OnInteriorVehicleData", "RC.OnRemoteControlSettings", "SDL.ActivateApp", "SDL.GetListOfPermissions", "SDL.GetStatusUpdate",
            "SDL.GetURLs", "SDL.GetUserFriendlyMessage", "SDL.UpdateSDL", "SDL.OnAllowSDLFunctionality", "SDL.OnAppPermissionConsent", "SDL.OnPolicyUpdate",
            "SDL.OnReceivedPolicyUpdate", "TTS.ChangeRegistration", "TTS.GetCapabilities", "TTS.GetLanguage", "TTS.GetSupportedLanguages", "TTS.IsReady",
            "TTS.SetGlobalProperties", "TTS.Speak", "TTS.StopSpeaking", "TTS.OnLanguageChange", "TTS.OnResetTimeout", "TTS.Started", "TTS.Stopped",
            "UI.AddCommand", "UI.AddSubMenu", "UI.Alert", "UI.ChangeRegistration", "UI.ClosePopUp", "UI.DeleteCommand", "UI.DeletSubMenu", "UI.EndAudioPassThru",
            "UI.GetCapabilities", "UI.GetLanguage", "UI.GetSupportedLanguages", "UI.IsReady", "UI.PerformAudioPassThru", "UI.PerformInteraction",
            "UI.ScrollableMessage", "UI.SetAppIcon", "UI.SetDisplayLayout", "UI.SetGlobalProperties", "UI.SetMediaClockTimer", "UI.Show", "UI.ShowCustomForm",
            "UI.Slider", "UI.OnCommand", "UI.OnDriverDistraction", "UI.OnKeyboardInput", "UI.OnLanguageChange", "UI.OnRecordStart", "UI.OnResetTimeout",
            "UI.OnSystemContext", "UI.OnTouchEvent", "VI.DiagnosticMessage", "VI.GetDTCs", "VI.GetVehicleData", "VI.GetVehicleType", "VI.IsReady",
            "VI.ReadDID", "VI.SubscribeVehicleData", "VI.UnsubscribeVehicleData", "VI.OnVehicleData","VR.AddCommand", "VR.ChangeRegistartion", "VR.DeleteCommand",
            "VR.GetCapabilities", "VR.GetLanguage", "VR.GetSupportedLanguages", "VR.IsReady", "VR.PerformInteraction", "VR.OnCommand",
            "VR.OnLanguageChange", "VR.Started", "VR.Stopped"};

                if (basicCommunicationListArray == null)
                    basicCommunicationListArray = new String[] {"BC.ActivateApp","BC.AllowDeviceToConnect","BC.DecryptCertificate",
            "BC.DialNumber", "BC.GetSystemInfo", "BC.MixingAudioSupported", "BC.PolicyUpdate", "BC.SystemRequest", "BC.UpdateAppList",
            "BC.UpdateDeviceList", "BC.OnAppActivated", "BC.OnAppDeactivated", "BC.OnAwakeSDL", "BC.OnDeactivateHMI", "BC.OnDeviceChosen",
            "BC.OnEmergencyEvent", "BC.OnEventChanged", "BC.OnExitAllApplications", "BC.OnExitApplication", "BC.OnFindApplications",
            "BC.OnIgnitionCycleOver", "BC.OnPhoneCall", "BC.OnReady", "BC.OnStartDeviceDiscovery", "BC.OnSystemInfoChanged", "BC.OnSystemRequest",
            "BC.OnUpdateDeviceList"};

                if (buttonsListArray == null)
                    buttonsListArray = new String[] { "Buttons.GetCapabilities", "Buttons.ButtonPress", "Buttons.OnButtonEvent", "Buttons.OnButtonPress" };
                if (navigationListArray == null)
                    navigationListArray = new String[] {"Navigation.AlertManeuver",
            "Navigation.GetWayPoints", "Navigation.IsReady", "Navigation.SendLocation", "Navigation.ShowConstantTBT", "Navigation.StartAudioStream",
            "Navigation.StartStream", "Navigation.StopAudioStream", "Navigation.StopStream", "Navigation.SubscribeWayPoints", "Navigation.UnsubscribeWayPoints",
            "Navigation.UpdateTurnList", "Navigation.OnTBTClientState" };
                if (buttonsCommunicationListArray == null)
                    buttonsCommunicationListArray = new String[] { "Buttons.GetCapabilities", "Buttons.ButtonPress", "Buttons.OnButtonEvent", "Buttons.OnButtonPress" };
                if (rcListArray == null)
                    rcListArray = new String[] {"RC.GetCapabilities", "RC.GetInteriorVehicleData", "RC.GetInteriorVehicleDataConsent", "RC.IsReady",
            "RC.SetInteriorVehicleData", "RC.OnInteriorVehicleData", "RC.OnRemoteControlSettings"};
                if (sdlListArray == null)
                    sdlListArray = new String[] {"SDL.ActivateApp", "SDL.GetListOfPermissions", "SDL.GetStatusUpdate",
            "SDL.GetURLs", "SDL.GetUserFriendlyMessage", "SDL.UpdateSDL", "SDL.OnAllowSDLFunctionality", "SDL.OnAppPermissionConsent", "SDL.OnPolicyUpdate",
            "SDL.OnReceivedPolicyUpdate"};
                if (ttsListArray == null)
                    ttsListArray = new String[] {"TTS.ChangeRegistration", "TTS.GetCapabilities", "TTS.GetLanguage", "TTS.GetSupportedLanguages", "TTS.IsReady",
            "TTS.SetGlobalProperties", "TTS.Speak", "TTS.StopSpeaking", "TTS.OnLanguageChange", "TTS.OnResetTimeout", "TTS.Started", "TTS.Stopped"};
                if (uiListArray == null)
                    uiListArray = new String[] {"UI.AddCommand", "UI.AddSubMenu", "UI.Alert", "UI.ChangeRegistration", "UI.ClosePopUp", "UI.DeleteCommand", "UI.DeletSubMenu", "UI.EndAudioPassThru",
            "UI.GetCapabilities", "UI.GetLanguage", "UI.GetSupportedLanguages", "UI.IsReady", "UI.PerformAudioPassThru", "UI.PerformInteraction",
            "UI.ScrollableMessage", "UI.SetAppIcon", "UI.SetDisplayLayout", "UI.SetGlobalProperties", "UI.SetMediaClockTimer", "UI.Show", "UI.ShowCustomForm",
            "UI.Slider", "UI.OnCommand", "UI.OnDriverDistraction", "UI.OnKeyboardInput", "UI.OnLanguageChange", "UI.OnRecordStart", "UI.OnResetTimeout",
            "UI.OnSystemContext", "UI.OnTouchEvent"};
                if (viListArray == null)
                    viListArray = new String[] { "VI.DiagnosticMessage", "VI.GetDTCs", "VI.GetVehicleData", "VI.GetVehicleType", "VI.IsReady",
            "VI.ReadDID", "VI.SubscribeVehicleData", "VI.UnsubscribeVehicleData" ,"VI.OnVehicleData"};
                if(vrListArray==null)
                   vrListArray = new String[] { "VR.AddCommand", "VR.ChangeRegistartion", "VR.DeleteCommand",
            "VR.GetCapabilities", "VR.GetLanguage", "VR.GetSupportedLanguages", "VR.IsReady", "VR.PerformInteraction", "VR.OnCommand",
            "VR.OnLanguageChange", "VR.Started", "VR.Stopped" };


                LogsList = AppInstanceManager.AppInstance._msgAdapter;
                ConsoleLogsListView.ItemsSource = LogsList;

                RPCTypeComboBox.ItemsSource = RpcTypesArray;
                RPCTypeComboBox.SelectedIndex = 0;
            });
        }
         public ObservableCollection<string> SearchList { get; set; }
        private void txtSearchBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {

            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
               SearchList = new ObservableCollection<string>();
                for (int i=0;i< RpcListArray.Length;i++)

                {
                    if (sender.Text != "")
                    {
                        if (RpcListArray[i].ToLower().Contains(sender.Text.ToLower()))
                        {
                            SearchList.Add(RpcListArray[i]);
                        }
                    }
                   

                }

                txtSearchBox.ItemsSource = SearchList;
            }

        }
        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                RpcResponseList(args.ChosenSuggestion.ToString());
            }
            
        }
        private void txtSearchBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            txtSearchBox.Text = ((string)args.SelectedItem);

        }
        private void LogsList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        private async void onClick(object sender, ItemClickEventArgs e)
        {
            Object listObj = e.ClickedItem;

            LogMessage logMessage = ((Message)listObj).LogMessage;

            if (logMessage is RpcLogMessage)
            {
                ConsoleLogPreview consoleLogPreview = new ConsoleLogPreview(((RpcLogMessage)logMessage).getMessage());
                await consoleLogPreview.ShowAsync();
            }
            else if (logMessage is StringLogMessage)
            {
                var stringLogMessageDialog = new GenericDialog();

                string sMessageText = ((StringLogMessage)logMessage).getData();

                if (sMessageText == "")
                {
                    sMessageText = ((StringLogMessage)logMessage).getMessage();
                }

                stringLogMessageDialog.EditText.Text = sMessageText;
                stringLogMessageDialog.EditText.AllowFocusOnInteraction = false;
                stringLogMessageDialog.EditText.IsEnabled = false;

                stringLogMessageDialog.SecondaryButtonClick += StringLogMessageDialog_SecondaryButtonClick;
                stringLogMessageDialog.SecondaryButtonText = "OK";

                await stringLogMessageDialog.ShowAsync();
            }

        }

        private void StringLogMessageDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void SendRpcsTapped(object sender, TappedRoutedEventArgs e)
        {
        }
        private void RPCTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String SelectedRPCType = RpcTypesArray[RPCTypeComboBox.SelectedIndex];

            RpcList(SelectedRPCType);
        }
           private async void RpcList(string SelectedRPCType)
        {

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {




                if (SelectedRPCType == "All")
                {
                    SendRpcListBox.ItemsSource = RpcListArray;
                }
                else if (SelectedRPCType == "Basic Communication")
                {
                    SendRpcListBox.ItemsSource = basicCommunicationListArray;
                }
                else if (SelectedRPCType == "Buttons")
                {
                    SendRpcListBox.ItemsSource = buttonsListArray;
                }

                else if (SelectedRPCType == "Navigation")
                {
                    SendRpcListBox.ItemsSource = navigationListArray;
                }

                else if (SelectedRPCType == "RC")
                {
                    SendRpcListBox.ItemsSource = rcListArray;
                }
                else if (SelectedRPCType == "SDL")
                {
                    SendRpcListBox.ItemsSource = sdlListArray;
                }
                else if (SelectedRPCType == "TTS")
                {
                    SendRpcListBox.ItemsSource = ttsListArray;
                }
                else if (SelectedRPCType == "UI")
                {
                    SendRpcListBox.ItemsSource = uiListArray;
                }
                else if (SelectedRPCType == "VehicleInfo")
                {
                    SendRpcListBox.ItemsSource = viListArray;
                }
                else if (SelectedRPCType == "VR")
                {
                    SendRpcListBox.ItemsSource = vrListArray;
                }
            });
        }
        private void SendRpcListBoxItemSelected(object sender, SelectionChangedEventArgs e)
        {
            String SelectedRPC = (sender as ListBox).SelectedItem as String;
            RpcResponseList(SelectedRPC);
        } 

        private async void RpcResponseList(string SelectedRPC)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {

           

            SendRpcListBox.SelectedIndex = -1;

            //BC
            if (SelectedRPC == "BC.ActivateApp")
            {
                BCActivateAppResponse bcActivateAppResponse = new BCActivateAppResponse();
                bcActivateAppResponse.ShowActivateApp();
            }
            else if (SelectedRPC == "BC.AllowDeviceToConnect")
            {
                BCAllowDeviceToConnectResponse bcAllowDeviceToConnectResponse = new BCAllowDeviceToConnectResponse();
                bcAllowDeviceToConnectResponse.ShowAllowDeviceToConnect();
            }
            else if (SelectedRPC == "BC.DecryptCertificate")
            {
                BCDecryptCertificateResponse bcDecryptCertificateResponse = new BCDecryptCertificateResponse();
                bcDecryptCertificateResponse.ShowDecryptCertificate();
            }
            else if (SelectedRPC == "BC.DialNumber")
            {
                BCDialNumberResponse bcDialNumberResponse = new BCDialNumberResponse();
                bcDialNumberResponse.ShowDialNumber();
            }
            else if (SelectedRPC == "BC.GetSystemInfo")
            {
                BCGetSystemInfoResponse bcGetSystemInfoResponse = new BCGetSystemInfoResponse();
                bcGetSystemInfoResponse.ShowGetSystemInfo();
            }
            else if (SelectedRPC == "BC.MixingAudioSupported")
            {
                BCMixingAudioSupportedResponse bCMixingAudioSupportedResponse = new BCMixingAudioSupportedResponse();
                bCMixingAudioSupportedResponse.ShowMixingAudioSupported();
            }
            else if (SelectedRPC == "BC.PolicyUpdate")
            {
                BCPolicyUpdateResponse bcPolicyUpdateResponse = new BCPolicyUpdateResponse();
                bcPolicyUpdateResponse.ShowPolicyUpdate();
            }
            else if (SelectedRPC == "BC.SystemRequest")
            {
                BCSystemRequestResponse bcSystemRequestResponse = new BCSystemRequestResponse();
                bcSystemRequestResponse.ShowSystemRequest();
            }
            else if (SelectedRPC == "BC.UpdateAppList")
            {
                BCUpdateAppListResponse bcUpdateAppListResponse = new BCUpdateAppListResponse();
                bcUpdateAppListResponse.ShowUpdateAppList();
            }
            else if (SelectedRPC == "BC.UpdateDeviceList")
            {
                BCUpdateDeviceListResponse bcUpdateDeviceListResponse = new BCUpdateDeviceListResponse();
                bcUpdateDeviceListResponse.ShowUpdateDeviceList();
            }
            else if (SelectedRPC == "BC.OnAppActivated")
            {
                BCOnAppActivated bcOnAppActivated = new BCOnAppActivated();
                bcOnAppActivated.ShowOnAppActivated();
            }
            else if (SelectedRPC == "BC.OnAppDeactivated")
            {
                BCOnAppDeactivated bcOnAppDeactivated = new BCOnAppDeactivated();
                bcOnAppDeactivated.ShowOnAppDeactivated();
            }
            else if (SelectedRPC == "BC.OnAwakeSDL")
            {
                BCOnAwakeSDL bcOnAwakeSDL = new BCOnAwakeSDL();
                bcOnAwakeSDL.ShowOnAwakeSDL();
            }
            else if (SelectedRPC == "BC.OnDeactivateHMI")
            {
                BCOnDeactivatHMI bcOnDeactivatHMI = new BCOnDeactivatHMI();
                bcOnDeactivatHMI.ShowOnDeactivateHMI();
            }
            else if (SelectedRPC == "BC.OnDeviceChosen")
            {
                BCOnDeviceChosen bcOnDeviceChosen = new BCOnDeviceChosen();
                bcOnDeviceChosen.ShowOnDeviceChosen();
            }
            else if (SelectedRPC == "BC.OnEmergencyEvent")
            {
                BCOnEmergencyEvent bcOnEmergencyEvent = new BCOnEmergencyEvent();
                bcOnEmergencyEvent.ShowOnEmergencyEvent();
            }
            else if (SelectedRPC == "BC.OnEventChanged")
            {
                BCOnEventChanged bcOnEventChanged = new BCOnEventChanged();
                bcOnEventChanged.ShowOnEventChanged();
            }
            else if (SelectedRPC == "BC.OnExitAllApplications")
            {
                BCOnExitAllApplications bcOnExitAllApplications = new BCOnExitAllApplications();
                bcOnExitAllApplications.ShowOnExitAllApplications();
            }
            else if (SelectedRPC == "BC.OnExitApplication")
            {
                BCOnExitApplication bcOnExitApplication = new BCOnExitApplication();
                bcOnExitApplication.ShowOnExitApplication();
            }
            else if (SelectedRPC == "BC.OnFindApplications")
            {
                BCOnFindApplications bcOnFindApplications = new BCOnFindApplications();
                bcOnFindApplications.ShowOnFindApplications();
            }
            else if (SelectedRPC == "BC.OnIgnitionCycleOver")
            {
                BCOnIgnitionCycleOver bcOnIgnitionCycleOver = new BCOnIgnitionCycleOver();
                bcOnIgnitionCycleOver.ShowOnIgnitionCycleOver();
            }
            else if (SelectedRPC == "BC.OnPhoneCall")
            {
                BCOnPhoneCall bcOnPhoneCall = new BCOnPhoneCall();
                bcOnPhoneCall.ShowOnPhoneCall();
            }
            else if (SelectedRPC == "BC.OnReady")
            {
                BCOnReady bcOnReady = new BCOnReady();
                bcOnReady.ShowOnReady();
            }
            else if (SelectedRPC == "BC.OnStartDeviceDiscovery")
            {
                BCOnStartDeviceDiscovery bcOnStartDeviceDiscovery = new BCOnStartDeviceDiscovery();
                bcOnStartDeviceDiscovery.ShowOnStartDeviceDiscovery();
            }
            else if (SelectedRPC == "BC.OnSystemInfoChanged")
            {
                BCOnSystemInfoChanged bcOnSystemInfoChanged = new BCOnSystemInfoChanged();
                bcOnSystemInfoChanged.ShowOnSystemInfoChanged();
            }
            else if (SelectedRPC == "BC.OnSystemRequest")
            {
                BCOnSystemRequest bcOnSystemRequest = new BCOnSystemRequest();
                bcOnSystemRequest.ShowOnSystemRequest();
            }
            else if (SelectedRPC == "BC.OnUpdateDeviceList")
            {
                BCOnUpdateDeviceList bcOnUpdateDeviceList = new BCOnUpdateDeviceList();
                bcOnUpdateDeviceList.ShowOnUpdateDeviceList();
            }

            //Buttons
            else if (SelectedRPC == "Buttons.GetCapabilities")
            {
                ButtonsGetCapabilitiesResponse buttonsGetCapabilitiesResponse = new ButtonsGetCapabilitiesResponse();
                buttonsGetCapabilitiesResponse.ShowGetCapabilities();
            }
            else if (SelectedRPC == "Buttons.ButtonPress")
            {
                ButtonsButtonPressResponse buttonsButtonPressResponse = new ButtonsButtonPressResponse();
                buttonsButtonPressResponse.ShowButtonPress();
            }
            else if (SelectedRPC == "Buttons.OnButtonEvent")
            {
                ButtonsOnButtonEvent buttonsOnButtonEvent = new ButtonsOnButtonEvent();
                buttonsOnButtonEvent.ShowOnButtonEvent();
            }
            else if (SelectedRPC == "Buttons.OnButtonPress")
            {
                ButtonsOnButtonPress buttonsOnButtonPress = new ButtonsOnButtonPress();
                buttonsOnButtonPress.ShowOnButtonPress();
            }

            //Navigation
            else if (SelectedRPC == "Navigation.AlertManeuver")
            {
                NavigationAlertManeuverResponse navigationAlertManeuverResponse = new NavigationAlertManeuverResponse();
                navigationAlertManeuverResponse.ShowAlertManeuver();
            }
            else if (SelectedRPC == "Navigation.GetWayPoints")
            {
                NavigationGetWayPointsResponse navigationGetWayPointsResponse = new NavigationGetWayPointsResponse();
                navigationGetWayPointsResponse.ShowGetWayPoints();
            }
            else if (SelectedRPC == "Navigation.IsReady")
            {
                NavigationIsReadyResponse navigationIsReadyResponse = new NavigationIsReadyResponse();
                navigationIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "Navigation.SendLocation")
            {
                NavigationSendLocationResponse navigationSendLocationResponse = new NavigationSendLocationResponse();
                navigationSendLocationResponse.ShowSendLocation();
            }
            else if (SelectedRPC == "Navigation.ShowConstantTBT")
            {
                NavigationShowConstantTBTResponse navigationShowConstantTBTResponse = new NavigationShowConstantTBTResponse();
                navigationShowConstantTBTResponse.ShowConstantTBT(); ;
            }
            else if (SelectedRPC == "Navigation.StartAudioStream")
            {
                NavigationStartAudioStreamResponse navigationStartAudioStreamResponse = new NavigationStartAudioStreamResponse();
                navigationStartAudioStreamResponse.ShowStartAudioStream();
            }
            else if (SelectedRPC == "Navigation.StartStream")
            {
                NavigationStartStreamResponse navigationStartStreamResponse = new NavigationStartStreamResponse();
                navigationStartStreamResponse.ShowStartStream();
            }
            else if (SelectedRPC == "Navigation.StopAudioStream")
            {
                NavigationStopAudioStreamResponse navigationStopAudioStreamResponse = new NavigationStopAudioStreamResponse();
                navigationStopAudioStreamResponse.ShowStopAudioStream();
            }
            else if (SelectedRPC == "Navigation.StopStream")
            {
                NavigationStopStreamResponse navigationStopStreamResponse = new NavigationStopStreamResponse();
                navigationStopStreamResponse.ShowStopStream();
            }
            else if (SelectedRPC == "Navigation.SubscribeWayPoints")
            {
                NavigationSubscribeWayPointsResponse navigationSubscribeWayPointsResponse = new NavigationSubscribeWayPointsResponse();
                navigationSubscribeWayPointsResponse.ShowSubscribeWayPoints();
            }
            else if (SelectedRPC == "Navigation.UnsubscribeWayPoints")
            {
                NavigationUnsubscribeWayPointsResponse navigationUnsubscribeWayPointsResponse = new NavigationUnsubscribeWayPointsResponse();
                navigationUnsubscribeWayPointsResponse.ShowUnsubscribeWayPoints();
            }
            else if (SelectedRPC == "Navigation.UpdateTurnList")
            {
                NavigationUpdateTurnListResponse navigationUpdateTurnListResponse = new NavigationUpdateTurnListResponse();
                navigationUpdateTurnListResponse.ShowUpdateTurnList();
            }
            else if (SelectedRPC == "Navigation.OnTBTClientState")
            {
                NavigationOnTBTClientState navigationOnTBTClientState = new NavigationOnTBTClientState();
                navigationOnTBTClientState.ShowOnTBTClientState();
            }

            //RC
            else if (SelectedRPC == "RC.GetCapabilities")
            {
                RCGetCapabilitiesResponse rcGetCapabilitiesResponse = new RCGetCapabilitiesResponse();
                rcGetCapabilitiesResponse.ShowGetCapabilities();
            }
            else if (SelectedRPC == "RC.GetInteriorVehicleData")
            {
                RCGetInteriorVehicleDataResponse rcGetInteriorVehicleDataResponse = new RCGetInteriorVehicleDataResponse();
                rcGetInteriorVehicleDataResponse.ShowGetInteriorVehicleData();
            }
            else if (SelectedRPC == "RC.GetInteriorVehicleDataConsent")
            {
                RCGetInteriorVehicleDataConsentResponse rcGetInteriorVehicleDataConsent = new RCGetInteriorVehicleDataConsentResponse();
                rcGetInteriorVehicleDataConsent.ShowGetInteriorVehicleDataConsent();
            }
            else if (SelectedRPC == "RC.IsReady")
            {
                RCIsReadyResponse rcIsReadyResponse = new RCIsReadyResponse();
                rcIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "RC.SetInteriorVehicleData")
            {
                RCSetInteriorVehicleDataResponse rcSetInteriorVehicleDataResponse = new RCSetInteriorVehicleDataResponse();
                rcSetInteriorVehicleDataResponse.ShowSetInteriorVehicleData();
            }
            else if (SelectedRPC == "RC.OnInteriorVehicleData")
            {
                RCOnInteriorVehicleData rcOnInteriorVehicleData = new RCOnInteriorVehicleData();
                rcOnInteriorVehicleData.ShowOnInteriorVehicleData();
            }
            else if (SelectedRPC == "RC.OnRemoteControlSettings")
            {
                RCOnRemoteControlSettings rcOnRemoteControlSettings = new RCOnRemoteControlSettings();
                rcOnRemoteControlSettings.ShowOnRemoteControlSettings();
            }

            //SDL
            else if (SelectedRPC == "SDL.ActivateApp")
            {
                SDLActivateAppRequest sDLActivateAppResponse = new SDLActivateAppRequest();
                sDLActivateAppResponse.ShowActivateApp();
            }
            else if (SelectedRPC == "SDL.GetListOfPermissions")
            {
                SDLGetListOfPermissionsResponse sDLGetListOfPermissionsResponse = new SDLGetListOfPermissionsResponse();
                sDLGetListOfPermissionsResponse.ShowGetListOfPermissions();
            }
            else if (SelectedRPC == "SDL.GetStatusUpdate")
            {
                SDLGetStatusUpdateResponse sDLGetStatusUpdateResponse = new SDLGetStatusUpdateResponse();
                sDLGetStatusUpdateResponse.ShowGetStatusUpdate();
            }
            else if (SelectedRPC == "SDL.GetURLs")
            {
                SDLGetURLsResponse sDLGetURLsResponse = new SDLGetURLsResponse();
                sDLGetURLsResponse.ShowGetURLs();
            }
            else if (SelectedRPC == "SDL.GetUserFriendlyMessage")
            {
                SDLGetUserFriendlyMessageResponse sdlGetUserFriendlyMessageResponse = new SDLGetUserFriendlyMessageResponse();
                sdlGetUserFriendlyMessageResponse.ShowGetUserFriendlyMessage();
            }
            else if (SelectedRPC == "SDL.UpdateSDL")
            {
                SDLUpdateSDLResponse sDLUpdateSDLResponse = new SDLUpdateSDLResponse();
                sDLUpdateSDLResponse.ShowUpdateSDL();
            }
            else if (SelectedRPC == "SDL.OnAllowSDLFunctionality")
            {
                SDlOnAllowSDLFunctionality sDlOnAllowSDLFunctionality = new SDlOnAllowSDLFunctionality();
                sDlOnAllowSDLFunctionality.ShowOnAllowSDLFunctionality();
            }
            else if (SelectedRPC == "SDL.OnAppPermissionConsent")
            {
                SDLOnAppPermissionConsent sDLOnAppPermissionConsent = new SDLOnAppPermissionConsent();
                sDLOnAppPermissionConsent.ShowOnAppPermissionConsent();
            }
            else if (SelectedRPC == "SDL.OnPolicyUpdate")
            {
                SDLOnPolicyUpdate sDLOnPolicyUpdate = new SDLOnPolicyUpdate();
                sDLOnPolicyUpdate.ShowOnPolicyUpdate();
            }
            else if (SelectedRPC == "SDL.OnReceivedPolicyUpdate")
            {
                SDLOnReceivedPolicyUpdate sDLOnReceivedPolicyUpdate = new SDLOnReceivedPolicyUpdate();
                sDLOnReceivedPolicyUpdate.ShowOnReceivedPolicyUpdate();
            }

            //TTS - Text To Speak
            else if (SelectedRPC == "TTS.ChangeRegistration")
            {
                TTSChangeRegistrationResponse ttsChangeRegistrationResponse = new TTSChangeRegistrationResponse();
                ttsChangeRegistrationResponse.ShowChangeRegistration();
            }
            else if (SelectedRPC == "TTS.GetCapabilities")
            {
                TTSGetCapabilitiesResponse ttsGetCapabilitiesResponse = new TTSGetCapabilitiesResponse();
                ttsGetCapabilitiesResponse.ShowGetCapabilities();
            }
            else if (SelectedRPC == "TTS.GetLanguage")
            {
                TTSGetLanguageResponse ttsGetLanguageResponse = new TTSGetLanguageResponse();
                ttsGetLanguageResponse.ShowGetLanguage();
            }
            else if (SelectedRPC == "TTS.GetSupportedLanguages")
            {
                TTSGetSupportedLanguagesResponse ttsGetSupportedLanguagesResponse = new TTSGetSupportedLanguagesResponse();
                ttsGetSupportedLanguagesResponse.ShowGetSupportedLanguages();
            }
            else if (SelectedRPC == "TTS.IsReady")
            {
                TTSIsReadyResponse ttsIsReadyResponse = new TTSIsReadyResponse();
                ttsIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "TTS.SetGlobalProperties")
            {
                TTSSetGlobalPropertiesResponse ttsSetGlobalPropertiesResponse = new TTSSetGlobalPropertiesResponse();
                ttsSetGlobalPropertiesResponse.ShowSetGlobalProperties();
            }
            else if (SelectedRPC == "TTS.Speak")
            {
                TTSSpeakResponse tTSSpeakResponse = new TTSSpeakResponse();
                tTSSpeakResponse.ShowSpeak();
            }
            else if (SelectedRPC == "TTS.StopSpeaking")
            {
                TTSStopSpeakingResponse tTSStopSpeakingResponse = new TTSStopSpeakingResponse();
                tTSStopSpeakingResponse.ShowStopSpeaking();
            }
            else if (SelectedRPC == "TTS.OnLanguageChange")
            {
                TTSOnLanguageChange tTSOnLanguageChange = new TTSOnLanguageChange();
                tTSOnLanguageChange.ShowOnLanguageChange();
            }
            else if (SelectedRPC == "TTS.OnResetTimeout")
            {
                TTSOnResetTimeout tTSOnResetTimeout = new TTSOnResetTimeout();
                tTSOnResetTimeout.ShowOnResetTimeout();
            }
            else if (SelectedRPC == "TTS.Started")
            {
                TTSStarted tTSStarted = new TTSStarted();
                tTSStarted.ShowStarted();
            }
            else if (SelectedRPC == "TTS.Stopped")
            {
                TTSStopped tTSStopped = new TTSStopped();
                tTSStopped.ShowStopped();
            }

            //User Interface
            else if (SelectedRPC == "UI.AddCommand")
            {
                UIAddCommandResponse uiAddCommandResponse = new UIAddCommandResponse();
                uiAddCommandResponse.ShowAddCommand();
            }
            else if (SelectedRPC == "UI.AddSubMenu")
            {
                UIAddSubMenuResponse uiAddSubMenuResponse = new UIAddSubMenuResponse();
                uiAddSubMenuResponse.ShowAddSubMenu();
            }
            else if (SelectedRPC == "UI.Alert")
            {
                UIAlertResponse uiAlertResponse = new UIAlertResponse();
                uiAlertResponse.ShowAlert();
            }
            else if (SelectedRPC == "UI.ChangeRegistration")
            {
                UIChangeRegistrationResponse uiChangeRegistrationResponse = new UIChangeRegistrationResponse();
                uiChangeRegistrationResponse.ShowChangeRegistration();
            }
            else if (SelectedRPC == "UI.ClosePopUp")
            {
                UIClosePopUpResponse uiClosePopUpResponse = new UIClosePopUpResponse();
                uiClosePopUpResponse.ShowClosePopUp();
            }
            else if (SelectedRPC == "UI.DeleteCommand")
            {
                UIDeleteCommandResponse uiDeleteCommandResponse = new UIDeleteCommandResponse();
                uiDeleteCommandResponse.ShowDeleteCommand();
            }
            else if (SelectedRPC == "UI.DeletSubMenu")
            {
                UIDeleteSubMenuResponse uiDeleteSubMenuResponse = new UIDeleteSubMenuResponse();
                uiDeleteSubMenuResponse.ShowDeleteSubMenu();
            }
            else if (SelectedRPC == "UI.EndAudioPassThru")
            {
                UIEndAudioPassThruResponse uiEndAudioPassThruResponse = new UIEndAudioPassThruResponse();
                uiEndAudioPassThruResponse.ShowEndAudioPassThru();
            }
            else if (SelectedRPC == "UI.GetCapabilities")
            {
                UIGetCapabilitiesResponse uiGetCapabilitiesResponse = new UIGetCapabilitiesResponse();
                uiGetCapabilitiesResponse.ShowGetCapabilities();
            }
            else if (SelectedRPC == "UI.GetLanguage")
            {
                UIGetLanguageResponse uiGetLanguageResponse = new UIGetLanguageResponse();
                uiGetLanguageResponse.ShowGetLanguage();
            }
            else if (SelectedRPC == "UI.GetSupportedLanguages")
            {
                UIGetSupportedLanguagesResponse uiGetSupportedLanguagesResponse = new UIGetSupportedLanguagesResponse();
                uiGetSupportedLanguagesResponse.ShowGetSupportedLanguages();
            }
            else if (SelectedRPC == "UI.IsReady")
            {
                UIIsReadyResponse uiIsReadyResponse = new UIIsReadyResponse();
                uiIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "UI.PerformAudioPassThru")
            {
                UIPerformAudioPassThruResponse uiPerformAudioPassThruResponse = new UIPerformAudioPassThruResponse();
                uiPerformAudioPassThruResponse.ShowPerformAudioPassThru();
            }
            else if (SelectedRPC == "UI.PerformInteraction")
            {
                UIPerformInteractionResponse uiPerformInteractionResponse = new UIPerformInteractionResponse();
                uiPerformInteractionResponse.ShowPerformInteraction();
            }
            else if (SelectedRPC == "UI.ScrollableMessage")
            {
                UIScrollableMessageResponse uiScrollableMessageResponse = new UIScrollableMessageResponse();
                uiScrollableMessageResponse.ShowScrollableMessage();
            }
            else if (SelectedRPC == "UI.SetAppIcon")
            {
                UISetAppIconResponse uiSetAppIconResponse = new UISetAppIconResponse();
                uiSetAppIconResponse.ShowSetAppIcon();
            }
            else if (SelectedRPC == "UI.SetDisplayLayout")
            {
                UISetDisplayLayoutResponse uiSetDisplayLayoutResponse = new UISetDisplayLayoutResponse();
                uiSetDisplayLayoutResponse.ShowSetDisplayLayout();
            }
            else if (SelectedRPC == "UI.SetGlobalProperties")
            {
                UISetGlobalPropertiesResponse uiSetGlobalPropertiesResponse = new UISetGlobalPropertiesResponse();
                uiSetGlobalPropertiesResponse.ShowSetGlobalProperties();
            }
            else if (SelectedRPC == "UI.SetMediaClockTimer")
            {
                UISetMediaClockTimerResponse uiSetMediaClockTimerResponse = new UISetMediaClockTimerResponse();
                uiSetMediaClockTimerResponse.ShowSetMediaClockTimer();
            }
            else if (SelectedRPC == "UI.Show")
            {
                UIShowResponse uiShowResponse = new UIShowResponse();
                uiShowResponse.Show();
            }
            else if (SelectedRPC == "UI.ShowCustomForm")
            {
                UIShowCustomFormResponse uiShowCustomFormResponse = new UIShowCustomFormResponse();
                uiShowCustomFormResponse.ShowCustomForm();
            }
            else if (SelectedRPC == "UI.Slider")
            {
                UISliderResponse uiSliderResponse = new UISliderResponse();
                uiSliderResponse.ShowSlider();
            }
            else if (SelectedRPC == "UI.OnCommand")
            {
                UIOnCommand uiOnCommand = new UIOnCommand();
                uiOnCommand.ShowOnCommand();
            }
            else if (SelectedRPC == "UI.OnDriverDistraction")
            {
                UIOnDriverDistraction uiOnDriverDistraction = new UIOnDriverDistraction();
                uiOnDriverDistraction.ShowOnDriverDistraction();
            }
            else if (SelectedRPC == "UI.OnKeyboardInput")
            {
                UIOnKeyboardInput uiOnKeyboardInput = new UIOnKeyboardInput();
                uiOnKeyboardInput.ShowOnKeyboardInput();
            }
            else if (SelectedRPC == "UI.OnLanguageChange")
            {
                UIOnLanguageChange uiOnLanguageChange = new UIOnLanguageChange();
                uiOnLanguageChange.ShowOnLanguageChange();
            }
            else if (SelectedRPC == "UI.OnResetTimeout")
            {
                UIOnResetTimeout uiOnResetTimeout = new UIOnResetTimeout();
                uiOnResetTimeout.ShowOnResetTimeout();
            }
            else if (SelectedRPC == "UI.OnSystemContext")
            {
                UIOnSystemContext uiOnSystemContext = new UIOnSystemContext();
                uiOnSystemContext.ShowOnSystemContext();
            }
            else if (SelectedRPC == "UI.OnTouchEvent")
            {
                UIOnTouchEvent uiOnTouchEvent = new UIOnTouchEvent();
                uiOnTouchEvent.ShowOnTouchEvent();
            }

            //Vehicle Info
            else if (SelectedRPC == "VI.DiagnosticMessage")
            {
                VIDiagnosticMessage viDiagnosticMessage = new VIDiagnosticMessage();
                viDiagnosticMessage.ShowDiagnosticMessage();
            }
            else if (SelectedRPC == "VI.GetDTCs")
            {
                VIGetDTCs GetDTCs = new VIGetDTCs();
                GetDTCs.ShowGetDTCs();
            }
            else if (SelectedRPC == "VI.GetVehicleData")
            {
                VIGetVehicleDataResponse viGetVehicleDataResponse = new VIGetVehicleDataResponse();
                viGetVehicleDataResponse.ShowGetVehicleDataResponse();
            }
            else if (SelectedRPC == "VI.GetVehicleType")
            {
                VIGetVehicleTypeResponse viGetVehicleTypeResponse = new VIGetVehicleTypeResponse();
                viGetVehicleTypeResponse.ShowGetVehicleType();
            }
            else if (SelectedRPC == "VI.IsReady")
            {
                VIIsReadyResponse viIsReadyResponse = new VIIsReadyResponse();
                viIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "VI.ReadDID")
            {
                ViReadDID viReadDID = new ViReadDID();
                viReadDID.ShowViReadDid();
            }
            else if (SelectedRPC == "VI.SubscribeVehicleData")
            {
                VISubscribeVehicleData subscribeVehicleData = new VISubscribeVehicleData();
                subscribeVehicleData.ShowViSubVehicle();
            }
            else if (SelectedRPC == "VI.UnsubscribeVehicleData")
            {
                VIUnsubscribeVehicleData unsubscribeVehicleData = new VIUnsubscribeVehicleData();
                unsubscribeVehicleData.ShowViUnsubVehicle();
            }
            else if (SelectedRPC == "VI.OnVehicleData")
            {
                VIOnVehicleData viOnVehicleData = new VIOnVehicleData();
                viOnVehicleData.ShowOnVehicleData();
            }

            // Voice Recognition
            else if (SelectedRPC == "VR.AddCommand")
            {
                VRAddCommandResponse vrAddCommanDresponse = new VRAddCommandResponse();
                vrAddCommanDresponse.ShowAddCommand();
            }
            else if (SelectedRPC == "VR.ChangeRegistartion")
            {
                VRChangeRegistrationResponse vrChangeRegistrationResponse = new VRChangeRegistrationResponse();
                vrChangeRegistrationResponse.ShowChangeRegistration();
            }
            else if (SelectedRPC == "VR.DeleteCommand")
            {
                VRDeleteCommandResponse vrDeleteCommandResponse = new VRDeleteCommandResponse();
                vrDeleteCommandResponse.ShowDeleteCommand();
            }
            else if (SelectedRPC == "VR.GetCapabilities")
            {
                VRGetCapabilitiesResponse vrGetCapabilitiesResponse = new VRGetCapabilitiesResponse();
                vrGetCapabilitiesResponse.ShowGetCapabilities();
            }
            else if (SelectedRPC == "VR.GetLanguage")
            {
                VRGetLanguageResponse vrGetLanguageResponse = new VRGetLanguageResponse();
                vrGetLanguageResponse.ShowGetLanguage();
            }
            else if (SelectedRPC == "VR.GetSupportedLanguages")
            {
                VRGetSupportedLanguagesResponse vrGetSupportedLanguageResponse = new VRGetSupportedLanguagesResponse();
                vrGetSupportedLanguageResponse.ShowGetSupportedLanguages();
            }
            else if (SelectedRPC == "VR.IsReady")
            {
                VRIsReadyResponse vrIsReadyResponse = new VRIsReadyResponse();
                vrIsReadyResponse.ShowIsReady();
            }
            else if (SelectedRPC == "VR.PerformInteraction")
            {
                VRPerformInteractionResponse vrPerformInteractionResponse = new VRPerformInteractionResponse();
                vrPerformInteractionResponse.ShowPerformInteraction();
            }
            else if (SelectedRPC == "VR.OnCommand")
            {
                VROnCommand vrOnCommand = new VROnCommand();
                vrOnCommand.ShowOnCommand();
            }
            else if (SelectedRPC == "VR.OnLanguageChange")
            {
                VROnLanguageChange vrOnLanguageChange = new VROnLanguageChange();
                vrOnLanguageChange.ShowOnLanguageChange();
            }
            else if (SelectedRPC == "VR.Started")
            {
                VRStarted vrStarted = new VRStarted();
                vrStarted.ShowStarted();
            }
            else if (SelectedRPC == "VR.Stopped")
            {
                VRStopped vrStopped = new VRStopped();
                vrStopped.ShowStopped();
            }
            });
        }
            
    }
   
}
