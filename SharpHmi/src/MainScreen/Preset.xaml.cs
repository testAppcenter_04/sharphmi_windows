﻿using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using HmiApiLib;
using HmiApiLib.Common.Enums;

namespace SharpHmi.src.MainScreen
{
    public sealed partial class Preset : Page
    {
        public static PageNavigate parameters = new PageNavigate();
        HmiApiLib.ButtonName unsubscribedBtnName = HmiApiLib.ButtonName.VOLUME_UP;
        AppInstanceManager appInstanceManager;

        public Preset()
        {
            this.InitializeComponent();
            appInstanceManager = AppInstanceManager.AppInstance;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            parameters = (PageNavigate)e.Parameter;
            presetButtons();
        }

        private void presetButtons()
        {
            if (unsubscribedBtnName != ButtonName.VOLUME_UP)
                hidePresetButton(unsubscribedBtnName);

            if (parameters.buttonSubscription.Count > 0)
            {
                foreach (OnButtonSubscription btnSubsc in parameters.buttonSubscription)
                {
                    displayPresetSubsButtons(btnSubsc);
                }
            }

            if (parameters.customPresets != null)
                UpdatePresetButtons(parameters.customPresets);

            AppName.Text = parameters.Info + ":" + "Presets";
        }

        private void UpdatePresetButtons(List<string> customPresets)
        {
            for (int i = 0; i < customPresets.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        preset0.Content = customPresets[0];
                        break;
                    case 1:
                        preset1.Content = customPresets[1];
                        break;
                    case 2:
                        preset2.Content = customPresets[2];
                        break;
                    case 3:
                        preset3.Content = customPresets[3];
                        break;
                    case 4:
                        preset4.Content = customPresets[4];
                        break;
                    case 5:
                        preset5.Content = customPresets[5];
                        break;
                    case 6:
                        preset6.Content = customPresets[6];
                        break;
                    case 7:
                        preset7.Content = customPresets[7];
                        break;
                    case 8:
                        preset8.Content = customPresets[8];
                        break;
                    case 9:
                        preset9.Content = customPresets[9];
                        break;
                    default:
                        break;
                }
            }
        }

        private void backOnePage()
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            backOnePage();
        }

        private void Preset0_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_0, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset1_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_1, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset2_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_2, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset3_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_3, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset4_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_4, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset5_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_5, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset6_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_6, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset7_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_7, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset8_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_8, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }
        private void Preset9_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appInstanceManager.sendOnButtonNotifications(ButtonName.PRESET_9, new List<ButtonEventMode>() { ButtonEventMode.BUTTONDOWN, ButtonEventMode.BUTTONUP }, new List<ButtonPressMode>() { ButtonPressMode.SHORT }, null, AppInstanceManager.activatedAppId);
            backOnePage();
        }

        private void hidePresetButton(ButtonName unsubscribedBtnName)
        {
            switch ((int)unsubscribedBtnName)
            {
                case 5:
                    preset0.Visibility = Visibility.Collapsed;
                    break;
                case 6:
                    preset1.Visibility = Visibility.Collapsed;
                    break;
                case 7:
                    preset2.Visibility = Visibility.Collapsed;
                    break;
                case 8:
                    preset3.Visibility = Visibility.Collapsed;
                    break;
                case 9:
                    preset4.Visibility = Visibility.Collapsed;
                    break;
                case 10:
                    preset5.Visibility = Visibility.Collapsed;
                    break;
                case 11:
                    preset6.Visibility = Visibility.Collapsed;
                    break;
                case 12:
                    preset7.Visibility = Visibility.Collapsed;
                    break;
                case 13:
                    preset8.Visibility = Visibility.Collapsed;
                    break;
                case 14:
                    preset9.Visibility = Visibility.Collapsed;
                    break;
                default:
                    unsubscribedBtnName = ButtonName.VOLUME_UP;
                    break;
            }
        }

        private void displayPresetSubsButtons(OnButtonSubscription msg)
        {
            if (msg.getSubscribe() != null && (bool)msg.getSubscribe())
            {
                if (msg.getName() == HmiApiLib.ButtonName.PRESET_0)
                {
                    preset0.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_1)
                {
                    preset1.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_2)
                {
                    preset2.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_3)
                {
                    preset3.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_4)
                {
                    preset4.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_5)
                {
                    preset5.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_6)
                {
                    preset6.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_7)
                {
                    preset7.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_8)
                {
                    preset8.Visibility = Visibility.Visible;
                }
                else if (msg.getName() == HmiApiLib.ButtonName.PRESET_9)
                {
                    preset9.Visibility = Visibility.Visible;
                }
            }
        }
    }
}
