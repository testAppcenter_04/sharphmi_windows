﻿using SharpHmi.src.Utility;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using SharpHmi.src.interfaces;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.MainScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectionPage : Page
    {
        public static IApplicationCallback applicationCallback;

        public SelectionPage()
        {
            InitializeComponent();

        }

        private void HmiButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            AppUtils.saveSelectionScreenPreference(true);
            if (null != applicationCallback)
              applicationCallback.onSelectionButtonClicked(true);
        }

        private void ManualButtontapped(object sender, TappedRoutedEventArgs e)
        {
            AppUtils.saveSelectionScreenPreference(false);
            if (null != applicationCallback)
                applicationCallback.onSelectionButtonClicked(false);
        }

    }
}
