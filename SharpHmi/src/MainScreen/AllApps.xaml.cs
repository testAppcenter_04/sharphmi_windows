﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using HmiApiLib.Controllers.UI.IncomingRequests;
using HmiApiLib.Interfaces;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using static System.Net.Mime.MediaTypeNames;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.MainScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AllApps : Page, IPolicies
    {

        // AllAppsModel model = new AllAppsModel();
        public ObservableCollection<AllAppsModel> _item;
        public ObservableCollection<PermissionItem> permissionItemList ;
        ConsentSource? consent  = (ConsentSource)0;
        public IMessageListener messageListener;
        List<PermissionItem> permissionList;
        public Dictionary<String, bool?> checkBox = new Dictionary<String, bool?>();
        public Windows.Storage.ApplicationDataCompositeValue composite =new Windows.Storage.ApplicationDataCompositeValue();
        Boolean flag;
        

        Windows.Storage.ApplicationDataCompositeValue SavedComposite;
        public AllApps()
        {
        AppInstanceManager.AppInstance.setPoliciesCallback(this);
            
        _item =new ObservableCollection<AllAppsModel>();
        this.InitializeComponent();
        SavedComposite =   AppUtils.getAppPermissionForVehicle();
     // SdlOnAllowSDLFunctionality();
         SdlGetListOfPermissions();
      //CreateData();

        }
        public ObservableCollection<AllAppsModel> item
        {
            get { return _item; }

        }

        /* void CreateData()
         {
             try
             {

                 for (int i = 0; i < model._name.Count(); i++)
                 {
                     if (SavedComposite != null && SavedComposite.Count != 0)
                     {  if (SavedComposite[model._name[i]] == null)
                             SavedComposite[model._name[i]] = false;
                         _item.Add(new AllAppsModel { allowed = SavedComposite[model._name[i]], name = model._name[i] });
                     }
                     else
                         _item.Add(new AllAppsModel { allowed = false, name = model._name[i] });

                 }
             }
             catch (Exception e)
             {

             }
         }*/
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            try
            {
                var parameters = (AppItem)e.Parameter;
                AppInstanceManager.activatedAppId = (int)parameters.getApplication().getAppID();
               
            }
            catch { }
        }
            private  void CheckBoxChecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;
           
            /*switch (cb.Content) {
                case "GPS & Speed":
                    {
                        permissionList= AppPermissionData("Location-1",1, cb.IsChecked);
                        break;
                    }
                case "Driving Info":
                    {
                        permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                        break;
                    }
                case "Vehicle Info":
                    {
                        permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                        break;
                    }
                case "Notifications":
                    {
                        permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                        break;
                    }

                default: {
                        break;
                    }
                    

            }
            

            composite[cb.Content.ToString()] = cb.IsChecked;
            


            AppUtils.saveAppPermissionForVehicle(composite);*/


            appPermissionConsent(cb);
            




        }
        private void SdlOnAllowSDLFunctionality() {
            DeviceInfo devInfo = null;
            try
            {
                if (AppInstanceManager.AppInstance.appList != null)
                {
                    devInfo = new DeviceInfo();
                    devInfo.name = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getName();
                    devInfo.id = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getId(); ;
                    devInfo.transportType = AppInstanceManager.AppInstance.appList[0].getApplication().getDeviceInfo().getTransportType();
                    devInfo.isSDLAllowed = true;
                }
            }
            catch (Exception ex) { }
            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAllowSDLFunctionality(devInfo, true, consent);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }
        private  void CheckBoxUnchecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            


            appPermissionConsent(cb);
           
        }
        private void SdlGetListOfPermissions()
        {
            try
            {
               // long appId = Int64.Parse(AppInstanceManager.appIdPolicyIdDictionary[AppInstanceManager.fullHMIAppId]);
                RpcRequest rpcMessage = BuildRpc.buildSDLGetListOfPermissionsRequest(BuildRpc.getNextId(), AppInstanceManager.activatedAppId);
                AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.AppInstance.sendRpc(rpcMessage);
                
            }
            catch (Exception e) { }

        }
        private void appPermissionConsent(CheckBox cb)
        {
            /* switch (cb.Content)
             {
                 case "Location":
                     {
                         permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);
                         break;
                     }
                 case "DrivingCharacteristics":
                     {
                         permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                         break;
                     }
                 case "VehicleInfo":
                     {
                         permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                         break;
                     }
                 case "Notifications":
                     {
                         permissionList = AppPermissionData("Location-1", 1, cb.IsChecked);

                         break;
                     }

                 default:
                     {
                         break;
                     }


             }*/
            
           AllAppsModel val = cb.DataContext as AllAppsModel;
           permissionList = AppPermissionData(cb.Content.ToString(), val.idVal, cb.IsChecked);



            composite[cb.Content.ToString()] = cb.IsChecked;
            AppUtils.saveAppPermissionForVehicle(composite);
           
            //call for permission Consent notification
            // int appId = int.Parse(AppInstanceManager.appIdPolicyIdDictionary[AppInstanceManager.fullHMIAppId]);

            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAppPermissionConsent(AppInstanceManager.activatedAppId, permissionList, consent);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);



            //onvehiclecall if toggle is enabled
          /*  if (OnVehicleData.IsChecked == true && cb.Content.ToString().Equals("Notification") && cb.IsChecked==true) {
               // RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAppPermissionConsent(AppInstanceManager.fullHMIAppId, permissionList, consent);
               // AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
               // AppInstanceManager.AppInstance.sendRpc(rpcNotification);


            }*/
        }
        public List<PermissionItem> AppPermissionData(String name,int?
            id,bool? isAllow)
        {
            PermissionItem permissionItem = new PermissionItem();
            permissionItemList = new ObservableCollection<PermissionItem>();
            permissionItem.name = name;
            permissionItem.id = id;
            permissionItem.allowed = isAllow;
            permissionItemList.Add(permissionItem);
            permissionList = new List<PermissionItem>(); ;
            permissionList.AddRange(permissionItemList);
            return permissionList;

        }


       private void SelectAll_Checked(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < AppInstanceManager.AppInstance.permissionList.Count(); i++)
                {

                    _item[i] = new AllAppsModel(AppInstanceManager.AppInstance.permissionList[i].id) { allowed = true, name = AppInstanceManager.AppInstance.permissionList[i].name };
                   
                    composite[AppInstanceManager.AppInstance.permissionList[i].name] = true;
                }
                AppUtils.saveAppPermissionForVehicle(composite);

            }
            catch (Exception ex) { }
        }

        private void SelectAll_Unchecked(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < AppInstanceManager.AppInstance.permissionList.Count(); i++)
            {
                    permissionList = AppPermissionData(AppInstanceManager.AppInstance.permissionList[i].name, AppInstanceManager.AppInstance.permissionList[i].id, false);

                    _item[i] = new AllAppsModel(AppInstanceManager.AppInstance.permissionList[i].id) { allowed = false, name = AppInstanceManager.AppInstance.permissionList[i].name };
                    RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAppPermissionConsent(AppInstanceManager.activatedAppId, permissionList, consent);
                    AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
                    AppInstanceManager.AppInstance.sendRpc(rpcNotification);
                    composite[AppInstanceManager.AppInstance.permissionList[i].name] = false;

            }

            AppUtils.saveAppPermissionForVehicle(composite);
            }
            catch (Exception ex) { }
        }

        private async void showInfo(object sender, TappedRoutedEventArgs e)
        {
            TextBlock cb = sender as TextBlock;
            AllAppsModel val = cb.DataContext as AllAppsModel;
            var dialog=new MessageDialog("");
            if (val.name.Equals("Location"))
            {
                dialog = new MessageDialog("Location data");
            }
            else if (val.name.Equals("VehicleInfo"))
            {
                dialog = new MessageDialog("vehicleInfo data");
            }
            else if (val.name.Equals("DrivingCharacteristics"))
            {
                dialog = new MessageDialog("DrivingCharacteristics data");
            }
            else if (val.name.Equals("Notifications"))
            {
                dialog = new MessageDialog("Notification data");
            }
            else
                dialog = new MessageDialog(val.name);


            await dialog.ShowAsync();
        }
       
        public void RegisterUnregisterApps()
        {
            ClearData();
        }
        public async void ClearData()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    list.ItemsSource = null;
                    ApplicationData.Current.LocalSettings.Values.Remove("PERMISSION");

                }
                catch
                {
                }
            }
            );
        }
       

       async void IPolicies.UpdateAppPermissionList()
        {
            try
            {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    try
                    {
                        for (int i = 0; i < AppInstanceManager.AppInstance.permissionList.Count(); i++)
                        {
                            if (SavedComposite != null && SavedComposite.Count != 0)
                            {
                                if (SavedComposite[AppInstanceManager.AppInstance.permissionList[i].name] == null)
                                    SavedComposite[AppInstanceManager.AppInstance.permissionList[i].name] = false;
                                _item.Add(new AllAppsModel(AppInstanceManager.AppInstance.permissionList[i].id) { allowed = SavedComposite[AppInstanceManager.AppInstance.permissionList[i].name], name = AppInstanceManager.AppInstance.permissionList[i].name });
                            }
                            else
                                _item.Add(new AllAppsModel(AppInstanceManager.AppInstance.permissionList[i].id) { allowed = AppInstanceManager.AppInstance.permissionList[i].allowed, name = AppInstanceManager.AppInstance.permissionList[i].name });
                        }
                        list.ItemsSource = item;

                    }
                    catch (Exception exp)

                    { }

                });
            }
            catch (Exception e)
            { }
        }

       
    }
}
