﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;
using Windows.Media.SpeechSynthesis;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Perform_Interaction_VR : Page
    {
        public List<VrHelpItem> choices;
        TextFieldStruct textFieldStruct;
        string textStruct;
        String textToSynthesize;
        PerformInteraction ManualData;
        private SpeechSynthesizer synthesizer;
        private ResourceContext speechContext;
        private ResourceMap speechResourceMap;
        public Perform_Interaction_VR()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = (PerformInteraction)e.Parameter;
             ManualData = parameters;
             choices = parameters.getVrHelp();
             textFieldStruct = parameters.getInitialText();
            if(textFieldStruct!=null)
             textStruct= String.Format(textFieldStruct.fieldText);
            SpeechInteractionAsync();
        }
        private CoreDispatcher dispatcher;

        public async void SpeechInteractionAsync() {
            dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;

            bool permissionGained = await AudioCapturePermissions.RequestMicrophonePermission();
            if (permissionGained)
            {
                // Enable the recognition buttons.                
                await InitializeRecognizer(SpeechRecognizer.SystemSpeechLanguage);
            }
            else
            {
            }
        }
        private async Task InitializeRecognizer(Language recognizerLanguage)
        {
            if (speechRecognizer != null)
            {
                // cleanup prior to re-initializing this scenario.
                speechRecognizer.StateChanged -= SpeechRecognizer_StateChanged;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }

            // Create an instance of SpeechRecognizer.
            speechRecognizer = new SpeechRecognizer(recognizerLanguage);

            // Provide feedback to the user about the state of the recognizer.
            speechRecognizer.StateChanged += SpeechRecognizer_StateChanged;

            // Add a web search topic constraint to the recognizer.
            var webSearchGrammar = new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario.WebSearch, "webSearch");
            speechRecognizer.Constraints.Add(webSearchGrammar);

            // Compile the constraint.
            SpeechRecognitionCompilationResult compilationResult = await speechRecognizer.CompileConstraintsAsync();
            if (compilationResult.Status != SpeechRecognitionResultStatus.Success)
            {

            }
        }
        private async void SpeechRecognizer_StateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {

            });
        }





        private IAsyncOperation<SpeechRecognitionResult> recognitionOperation;
        private SpeechRecognizer speechRecognizer;


        private async void OnListenAsync(object sender, RoutedEventArgs e)
        {

        // Start recognition.
        try
        {
            recognitionOperation = speechRecognizer.RecognizeAsync();
            SpeechRecognitionResult speechRecognitionResult = await recognitionOperation;
            // If successful, display the recognition result.
            if (speechRecognitionResult.Status == SpeechRecognitionResultStatus.Success)
            {
                    switch (speechRecognitionResult.Text)
                    {
                        case "help":
                            Frame.Navigate(typeof(Perform_Interaction), ManualData);
                            break;

                        case "cancel":
                            if (Frame.CanGoBack)
                                Frame.GoBack();
                            break;
                        default:
                            break;
                    }
                    // Access to the recognized text through speechRecognitionResult.Text;
                }
            else
            {
                // Handle speech recognition failure
            }
        }
        catch (TaskCanceledException exception)
        {
            // TaskCanceledException will be thrown if you exit the scenario while the recognizer is actively
            // processing speech. Since this happens here when we navigate out of the scenario, don't try to 
            // show a message dialog for this exception.
            System.Diagnostics.Debug.WriteLine("TaskCanceledException caught while recognition in progress (can be ignored):");
            System.Diagnostics.Debug.WriteLine(exception.ToString());
        }
        catch (Exception exception)
        {

            var messageDialog = new Windows.UI.Popups.MessageDialog(exception.Message, "Exception");
await messageDialog.ShowAsync();
        }

      //  buttonOnListen.IsEnabled = true;
    }

        private async void speechRecognizer_RecognitionQualityDegrading(
    Windows.Media.SpeechRecognition.SpeechRecognizer sender,
    Windows.Media.SpeechRecognition.SpeechRecognitionQualityDegradingEventArgs args)
        {
            // Create an instance of a speech synthesis engine (voice).
            var speechSynthesizer =
                new Windows.Media.SpeechSynthesis.SpeechSynthesizer();

            // If input speech is too quiet, prompt the user to speak louder.
            if (args.Problem == Windows.Media.SpeechRecognition.SpeechRecognitionAudioProblem.TooQuiet)
            {
                // Generate the audio stream from plain text.
                Windows.Media.SpeechSynthesis.SpeechSynthesisStream stream;
                try
                {
                    stream = await speechSynthesizer.SynthesizeTextToStreamAsync("Try speaking louder");
                    stream.Seek(0);
                }
                catch (Exception)
                {
                    stream = null;
                }
            }
        }
        public bool CheckMicrophone()
        {

                return true;
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void SwitchToManual(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Perform_Interaction), ManualData);

        }

        private void CancelTapped(object sender, TappedRoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void HelpTapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Perform_Interaction), ManualData);

        }
    }
}
