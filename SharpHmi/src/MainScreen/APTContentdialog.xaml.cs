﻿using HmiApiLib.Controllers.UI.IncomingRequests;
using System.Linq;
using Windows.UI.Xaml.Controls;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.MainScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class APTContentdialog : ContentDialog
    {
        public Button doneButton;
        public Button cancelButton;
        private PerformAudioPassThru aptMsg = null;
        public APTContentdialog(PerformAudioPassThru aptMsg)
        {
            InitializeComponent();
            this.aptMsg = aptMsg;
            if (aptMsg == null)
                return;

            if (AppInstanceManager.AppInstance == null)
                return;

            Models.AppItem appItem = AppInstanceManager.AppInstance.getAppItem(aptMsg.getAppId());

            if (appItem != null)
            {
                this.AppName.Text = appItem.getAppName();
            }            

            if (aptMsg.getAudioPassThruDisplayTexts() != null)
            {
                Line1.Text = "";
                Line2.Text = "";

                if (aptMsg.getAudioPassThruDisplayTexts().Count > 0)
                    Line1.Text = aptMsg.getAudioPassThruDisplayTexts().ElementAt(0).getFieldText();
                if (aptMsg.getAudioPassThruDisplayTexts().Count > 1)
                    Line2.Text = aptMsg.getAudioPassThruDisplayTexts().ElementAt(1).getFieldText();
            }

            doneButton = Done;
            cancelButton = Cancel;
        }
    }
}
