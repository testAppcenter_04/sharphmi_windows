﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using static SharpHmi.src.Manager.GraphicManager;

namespace SharpHmi.src.MainScreen
{
    public sealed partial class SubItems : IAddSubMenu
    {
        AppInstanceManager appInstanceManager;
        ObservableCollection<SubMenuAddCommandIcon> subMenuAddCommandsIconList = new ObservableCollection<SubMenuAddCommandIcon>();

        public SubItems()
        {
            this.InitializeComponent();
            if (appInstanceManager == null)
                appInstanceManager = AppInstanceManager.AppInstance;

            AppName.Text = AppInstanceManager.activatedAppName;

            appInstanceManager.setAddSubMenuCallback(this);
        }

        public async void RefreshAsync()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                UpdateList();         
            }); 
            
        }

        int? menuId;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var parameters = (PageNavigate)e.Parameter;
            menuId = parameters.menuId;       
            UpdateList();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            AppInstanceManager.AppInstance.setAddSubMenuCallback(null);
        }

        public void UpdateList()
        {
            subMenuAddCommandsIconList.Clear();

            List<AddCommand> addCommandsList = null;

            if (appInstanceManager.getAppItem(AppInstanceManager.activatedAppId) != null)
                addCommandsList = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId).getCommandsInSubMenu(menuId);

            if (addCommandsList != null)
            {
                for (int i = 0; i < addCommandsList.Count; i++)
                {
                    if (addCommandsList[i].getMenuParams() != null &&
                        addCommandsList[i].getMenuParams().getParentID() != null &&
                        addCommandsList[i].getMenuParams().getParentID() > 0)
                    {
                        SubMenuAddCommandIcon subMenuAddCommandIcon = new SubMenuAddCommandIcon(addCommandsList[i]);

                        if (addCommandsList[i].getCmdIcon() != null)
                        {
                            if (addCommandsList[i].getCmdIcon().getImageType() == ImageType.STATIC)
                            {
                                string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(addCommandsList[i].getCmdIcon().getValue());
                                if (staticImage != "" || null == staticImage)
                                {
                                    subMenuAddCommandIcon.setStaticIconValue(staticImage);
                                }
                                else
                                {
                                    subMenuAddCommandIcon.setStaticIconValue("\uEB9F");
                                }
                            }
                        }

                        subMenuAddCommandsIconList.Add(subMenuAddCommandIcon);
                    }
                }
                SetIcon();
            }
        }


        public void handleGraphics(AppItem appItem, SubMenuAddCommandIcon imageHolder, string iconValue)
        {
            if (appItem == null) return;

            //try to get the currently cached bitmap
            BitmapImage myImage = appItem.graphicManager.getGraphic(iconValue);
            if (myImage != null)
            {
                imageHolder.setBitmapImage(myImage);
            }

            //setup our input parameters, both path and holder
            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = imageHolder;
            inputGraphic.fileName = iconValue;

            //wait and listen for updates for our graphic bitmap from putfile, this is our softbutton graphic
            Action<OutputGraphic> graphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != AppInstanceManager.activatedAppId) return;
                    SubMenuAddCommandIcon image = (SubMenuAddCommandIcon)graphicStruct.holder;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.setBitmapImage(bitmapUpdate);
                }
            };

            //subscribe for updates for our graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(graphicDelegate, inputGraphic);
        }

        public void SetIcon()
        {
            if(subMenuAddCommandsIconList.Count > 0)
            {
                for (int i = 0; i < subMenuAddCommandsIconList.Count; i++)
                {
                    if (subMenuAddCommandsIconList[i].getAddCommand() != null &&
                        subMenuAddCommandsIconList[i].getAddCommand().getCmdIcon() != null &&
                        subMenuAddCommandsIconList[i].getAddCommand().getCmdIcon().getImageType() == ImageType.DYNAMIC)
                    {
                        try
                        {
                            string iconValue = subMenuAddCommandsIconList[i].getAddCommand().getCmdIcon().getValue();
                            if (iconValue != null)
                            {
                                try
                                {
                                    AppItem appItem = appInstanceManager.getAppItem(AppInstanceManager.activatedAppId);
                                    handleGraphics(appItem, subMenuAddCommandsIconList[i], iconValue);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }                          
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }
            }

            ObservableCollection<SubMenuAddCommandIcon> commandUnderSubMenuIconsRefresh = new ObservableCollection<SubMenuAddCommandIcon>(subMenuAddCommandsIconList);
            ListView.ItemsSource = commandUnderSubMenuIconsRefresh;
        }

        public void refreshSubMenu()
        {
            RefreshAsync();
        }

        private void backOnePage()
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void backOnePage(object sender, TappedRoutedEventArgs e)
        {
            backOnePage();
        }

        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            //we need to backup before sending out our onCommand because the app may send a ui command like scrollable message which will be dismissed by our Frame.GoBack()
            //back to the main menu commands
            backOnePage();
            //back to the original screen that triggered the submenu page
            backOnePage();

            SubMenuAddCommandIcon selectedCommand = (SubMenuAddCommandIcon)e.ClickedItem;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand onCommand = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand();
            onCommand.setCmdID(selectedCommand.getAddCommand().getCmdId());
            onCommand.setAppId(AppInstanceManager.activatedAppId);
            AppInstanceManager.AppInstance.sendRpc(onCommand);


        }
    }
}
