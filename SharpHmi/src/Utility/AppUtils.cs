﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.Models;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Notifications;

namespace SharpHmi.src.Utility
{
    class AppUtils
    {
        private static Windows.Storage.ApplicationDataContainer localSetting =
                Windows.Storage.ApplicationData.Current.LocalSettings;
        public static readonly String SELECTION_PREFERENCE = "selection_pref";
        public static readonly String PERMISSION = "permissions";
        public static readonly String INDEX = "index";

        public static void savePreferenceValueForRpc(String key, RpcMessage rpcMessage)
        {
            String json = Newtonsoft.Json.JsonConvert.SerializeObject(rpcMessage);
            localSetting.Values[key] = json;
        }

        public static RpcMessage getSavedPreferenceValueForRpc<T>(String key)
        {
            RpcMessage rpcResponse = null;
            String json = (String) localSetting.Values[key];
            if (null != json)
            {
                Object msg = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
                rpcResponse = (RpcMessage)Convert.ChangeType(msg, typeof(T));
            }

            return rpcResponse;
        }

        internal static void RemoveSavedPreferencesValueForRpc(String key)
        {
            localSetting.Values[key] = null;
        }

        public static void saveSelectionScreenPreference(bool isHMIButtonPressed)
        {
            localSetting.Values[SELECTION_PREFERENCE] = isHMIButtonPressed;
        }
        public static void saveAppIdInstanceIndex(int index)
        {
            localSetting.Values[INDEX] = index;
        }
        public static void saveAppPermissionForVehicle(Windows.Storage.ApplicationDataCompositeValue permissions)
        {
            localSetting.Values[PERMISSION] = permissions;
        }
        public static Windows.Storage.ApplicationDataCompositeValue getAppPermissionForVehicle()
        {
            return (Windows.Storage.ApplicationDataCompositeValue)localSetting.Values[PERMISSION];
        }
        public static int getAppIdInstanceIndex()
        {
            return (int)localSetting.Values[INDEX];
        }
        public static bool? getSavedSelectionPreference()
        {
            return (bool?)localSetting.Values[SELECTION_PREFERENCE];
        }

        public static void ShowToastNotification(string title, string stringContent, long timePeriod)
        {
            ToastNotifier ToastNotifier = ToastNotificationManager.CreateToastNotifier();
            Windows.Data.Xml.Dom.XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
            Windows.Data.Xml.Dom.XmlNodeList toastNodeList = toastXml.GetElementsByTagName("text");
            toastNodeList.Item(0).AppendChild(toastXml.CreateTextNode(title));
            toastNodeList.Item(1).AppendChild(toastXml.CreateTextNode(stringContent));

            ToastNotification toast = new ToastNotification(toastXml);
            toast.ExpirationTime = System.DateTime.Now.AddSeconds(timePeriod);
            ToastNotifier.Show(toast);
        }

        public static MediaClock updateMediaClock(MediaClock mediaClock, SetMediaClockTimer setMediaClockTimer)
        {
            if (mediaClock == null && setMediaClockTimer == null)
            {
                return null;
            } else if (mediaClock != null && setMediaClockTimer == null)
            {
                return mediaClock;
            } else if (mediaClock == null && setMediaClockTimer != null)
            {
                MediaClock media = new MediaClock();
                media.setMediaClockTimer(setMediaClockTimer);
                return media;
            }

            if (setMediaClockTimer.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.CLEAR)
            {
                return null;
            }
            else if (setMediaClockTimer.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.PAUSE || setMediaClockTimer.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.RESUME)
            {
                if (mediaClock.getMediaClockTimer() != null)
                {
                    if (mediaClock.getMediaClockTimer().getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.COUNTUP 
                        || mediaClock.getMediaClockTimer().getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.COUNTDOWN)
                    {
                        mediaClock.setLastClockUpdateMode(mediaClock.getMediaClockTimer().getUpdateMode());
                    }

                    if (setMediaClockTimer.getStartTime() != null)
                    {
                        mediaClock.setMediaClockTimer(setMediaClockTimer);
                    }

                    mediaClock.getMediaClockTimer().@params.updateMode = setMediaClockTimer.getUpdateMode();
                }
                else
                {
                    mediaClock.setMediaClockTimer(setMediaClockTimer);
                }
            }
            else
            {
                mediaClock.setMediaClockTimer(setMediaClockTimer);
            }

            return mediaClock;
        }

        public static int getMediaClockTime(TimeFormat inpTime)
        {
            int seconds = 0;

            if (inpTime == null) return seconds;

            if (inpTime.getHours() != null)
            {
                seconds = (int)inpTime.getHours() * 3600;
            }

            if (inpTime.getMinutes() != null)
            {
                seconds += (int)inpTime.getMinutes() * 60;
            }

            if (inpTime.getSeconds() != null)
            {
                seconds += (int)inpTime.getSeconds();
            }

            return seconds;
        }

        public static string getMediaClockTimeText(TimeFormat inpTime)
        {
            string timeText = "";

            if (inpTime == null) return timeText;

            if (inpTime.getHours() != null)
            {
                timeText = String.Format("{0:00}", inpTime.getHours().ToString()) + ":";
            }

            if (inpTime.getMinutes() != null)
            {
                timeText += String.Format("{0:00}", inpTime.getMinutes().ToString()) + ":";
            }

            if (inpTime.getSeconds() != null)
            {
                timeText += String.Format("{0:00}", inpTime.getSeconds().ToString());
            }

            return timeText;
        }

        public static string getMediaClockTimeText(int? inpVal)
        {
            if (inpVal == null) return "";

            int hours = (int)(inpVal / 3600);
            int minutes = (int)(inpVal % 3600) / 60;
            int seconds = (int)((inpVal % 3600) % 60);

            return String.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
        }
       
    }
}
