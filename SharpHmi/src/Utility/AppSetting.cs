﻿using HmiApiLib;
using System;
using static SharpHmi.AppInstanceManager;

namespace SharpHmi.src.Utility
{
    class AppSetting
    {
        private SelectionMode selectedMode = SelectionMode.BASIC_MODE;
        //private ISharedPreferences prefs = null;

        private static string sIPAddress = null;
        private static string sTcpPort = null;
        private static string sVideoPort = null;
        private static string sAudioPort = null;

        private static InitialConnectionCommandConfig initialConnectionCommandConfig;
        private static Windows.Storage.ApplicationDataContainer localSetting = null;

        public AppSetting()
        {
            localSetting =
                Windows.Storage.ApplicationData.Current.LocalSettings;
        }

        public SelectionMode getSelectedMode()
        {
           return selectedMode;
        }

        public void setSelectedMode(SelectionMode mode)
        {
            selectedMode = mode;
        }

        public String getIPAddress()
        {
            if (localSetting != null)
                if (null != (String)localSetting.Values[Const.PREFS_KEY_TRANSPORT_IP])
                    sIPAddress = (String)localSetting.Values[Const.PREFS_KEY_TRANSPORT_IP];
            if (sIPAddress == null)
                sIPAddress = Const.PREFS_DEFAULT_TRANSPORT_IP;
            return sIPAddress;
        }

        public String getTcpPort()
        {
            if (localSetting != null)
                if (null != (String)localSetting.Values[Const.PREFS_KEY_TRANSPORT_PORT])
                    sTcpPort = (String)localSetting.Values[Const.PREFS_KEY_TRANSPORT_PORT];
            if (sTcpPort == null)
                sTcpPort = Const.PREFS_DEFAULT_TRANSPORT_PORT;
            return sTcpPort;
        }

        public String getVideoPort()
        {
            if (localSetting != null)
                if (null != (String)localSetting.Values[Const.PREFS_KEY_VIDEO_PORT])
                    sVideoPort = (String)localSetting.Values[Const.PREFS_KEY_VIDEO_PORT];
            if (sVideoPort == null)
                sVideoPort = Const.PREFS_DEFAULT_VIDEO_PORT;
            return sVideoPort;
        }

        public String getAudioPort()
        {
            if (localSetting != null)
                if (null != (String)localSetting.Values[Const.PREFS_KEY_AUDIO_PORT])
                    sAudioPort = (String)localSetting.Values[Const.PREFS_KEY_AUDIO_PORT];
            if (sAudioPort == null)
                sAudioPort = Const.PREFS_DEFAULT_AUDIO_PORT;
            return sAudioPort;
        }


        public InitialConnectionCommandConfig getInitialConnectionCommandConfig()
        {
            if (localSetting != null)
            {
                if (initialConnectionCommandConfig == null)
                {
                    String str = (String)localSetting.Values[Const.PREFS_KEY_INITIAL_CONFIG];
                    if (str != null)
                    {
                        initialConnectionCommandConfig = 
                            Newtonsoft.Json.JsonConvert.DeserializeObject<InitialConnectionCommandConfig>(str);
                    }
                }
            }
            return initialConnectionCommandConfig;
        }

        public bool getBCMixAudioSupport()
        {
            bool isBcMixAudioSupportedChecked = Const.PREFS_DEFAULT_BC_MIX_AUDIO_SUPPORTED;
            if (localSetting != null && null != localSetting.Values[Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED])
                isBcMixAudioSupportedChecked = (bool)localSetting.Values[Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED];
            return isBcMixAudioSupportedChecked;
        }

        public bool getButtonsGetCapabilities()
        {
            bool isButtonsGetCapabilitiesChecked = Const.PREFS_DEFAULT_BUTTONS_GET_CAPABILITIES;
            if ((localSetting != null) && (null != localSetting.Values[Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES]))
                isButtonsGetCapabilitiesChecked = (bool)localSetting.Values[Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES];
            return isButtonsGetCapabilitiesChecked;
        }

        public bool getNavigationIsReady()
        {
            bool isNavIsReadyChecked = Const.PREFS_DEFAULT_NAVIGATION_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_NAVIGATION_IS_READY] != null))
                isNavIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_NAVIGATION_IS_READY];
            return isNavIsReadyChecked;
        }

        public bool getTTSGetCapabilities()
        {
            bool isTTSGetCapabilityChecked = Const.PREFS_DEFAULT_TTS_GET_CAPABILITIES;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_TTS_GET_CAPABILITIES] != null))
                isTTSGetCapabilityChecked = (bool)localSetting.Values[Const.PREFS_KEY_TTS_GET_CAPABILITIES];
            return isTTSGetCapabilityChecked;
        }

        public bool getTTSGetLanguage()
        {
            bool isTTSGetLanguageChecked = Const.PREFS_DEFAULT_TTS_GET_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_TTS_GET_LANGUAGE] != null))
                isTTSGetLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_TTS_GET_LANGUAGE];
            return isTTSGetLanguageChecked;
        }

        public bool getTTSGetSupportedLanguage()
        {
            bool isTTSGetSupportedLanguageChecked = Const.PREFS_DEFAULT_TTS_GET_SUPPORTED_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE] != null))
                isTTSGetSupportedLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE];
            return isTTSGetSupportedLanguageChecked;
        }

        public bool getTTSIsReady()
        {
            bool isNavIsReadyChecked = Const.PREFS_DEFAULT_TTS_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_TTS_IS_READY] != null))
                isNavIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_TTS_IS_READY];
            return isNavIsReadyChecked;
        }

        public bool getUIGetCapabilities()
        {
            bool isUIGetCapabilityChecked = Const.PREFS_DEFAULT_UI_GET_CAPABILITIES;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_UI_GET_CAPABILITIES] != null))
                isUIGetCapabilityChecked = (bool)localSetting.Values[Const.PREFS_KEY_UI_GET_CAPABILITIES];
            return isUIGetCapabilityChecked;
        }

        public bool getUIGetLanguage()
        {
            bool isUIGetLanguageChecked = Const.PREFS_DEFAULT_UI_GET_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_UI_GET_LANGUAGE] != null))
                isUIGetLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_UI_GET_LANGUAGE];
            return isUIGetLanguageChecked;
        }

        public bool getUIGetSupportedLanguage()
        {
            bool isUIGetSupportedLanguageChecked = Const.PREFS_DEFAULT_UI_GET_SUPPORTED_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE] != null))
                isUIGetSupportedLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE];
            return isUIGetSupportedLanguageChecked;
        }

        public bool getUIIsReady()
        {
            bool isUIIsReadyChecked = Const.PREFS_DEFAULT_UI_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_UI_IS_READY] != null))
                isUIIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_UI_IS_READY];
            return isUIIsReadyChecked;
        }

        public bool getVIGetVehicleData()
        {
            bool isVIGetVehicleDataChecked = Const.PREFS_DEFAULT_VI_GET_VEHICLE_DATA;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_DATA] != null))
                isVIGetVehicleDataChecked = (bool)localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_DATA];
            return isVIGetVehicleDataChecked;
        }

        public bool getVIGetVehicleType()
        {
            bool isVIGetVehicleTypeChecked = Const.PREFS_DEFAULT_VI_GET_VEHICLE_TYPE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_TYPE] != null))
                isVIGetVehicleTypeChecked = (bool)localSetting.Values[Const.PREFS_KEY_VI_GET_VEHICLE_TYPE];
            return isVIGetVehicleTypeChecked;
        }

        public bool getVIIsReady()
        {
            bool isVIIsReadyChecked = Const.PREFS_DEFAULT_VI_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VI_IS_READY] != null))
                isVIIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_VI_IS_READY];
            return isVIIsReadyChecked;
        }

        public bool getVRGetCapabilities()
        {
            bool isVRGetCapabilityChecked = Const.PREFS_DEFAULT_VR_GET_CAPABILITIES;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VR_GET_CAPABILITIES] != null))
                isVRGetCapabilityChecked = (bool)localSetting.Values[Const.PREFS_KEY_VR_GET_CAPABILITIES];
            return isVRGetCapabilityChecked;
        }

        public bool getVRGetLanguage()
        {
            bool isVRGetLanguageChecked = Const.PREFS_DEFAULT_VR_GET_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VR_GET_LANGUAGE] != null))
                isVRGetLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_VR_GET_LANGUAGE];
            return isVRGetLanguageChecked;
        }

        public bool getVRGetSupportedLanguage()
        {
            bool isVRGetSupportedLanguageChecked = Const.PREFS_DEFAULT_VR_GET_SUPPORTED_LANGUAGE;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE] != null))
                isVRGetSupportedLanguageChecked = (bool)localSetting.Values[Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE];
            return isVRGetSupportedLanguageChecked;
        }

        public bool getVRIsReady()
        {
            bool isVRIsReadyChecked = Const.PREFS_DEFAULT_VR_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_VR_IS_READY] != null))
                isVRIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_VR_IS_READY];
            return isVRIsReadyChecked;
        }

        public bool getRCGetCapabilities()
        {
            bool isRCGetCapabilityChecked = Const.PREFS_DEFAULT_RC_GET_CAPABILITIES;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_RC_GET_CAPABILITIES] != null))
                isRCGetCapabilityChecked = (bool)localSetting.Values[Const.PREFS_KEY_RC_GET_CAPABILITIES];
            return isRCGetCapabilityChecked;
        }

        public bool getRCIsReady()
        {
            bool isRCIsReadyChecked = Const.PREFS_DEFAULT_RC_IS_READY;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_RC_IS_READY] != null))
                isRCIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_RC_IS_READY];
            return isRCIsReadyChecked;
        }

        
        public bool getBCGetSystemInfo()
        {
            bool isBCIsReadyChecked = Const.PREFS_DEFAULT_BC_GET_SYSTEM_INFO;
            if ((localSetting != null) && (localSetting.Values[Const.PREFS_KEY_BC_GET_SYSTEM_INFO] != null))
                isBCIsReadyChecked = (bool)localSetting.Values[Const.PREFS_KEY_BC_GET_SYSTEM_INFO];
            return isBCIsReadyChecked;
        }
    }
}
