﻿using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using SharpHmi.src.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using static SharpHmi.src.Manager.GraphicManager;

namespace SharpHmi.src.Utility
{
    public class RpcUtils
    {
        public static void handleSoftButtonGraphics(AppItem appItem, SoftButton btn, Windows.UI.Xaml.Controls.Image imageHolder)
        {
            //try to get the currently cached bitmap
            BitmapImage myImage = appItem.graphicManager.getGraphic(btn.getImage().getValue());
            if (myImage != null)
            {
                imageHolder.Source = myImage;
            }

            //setup our input parameters, both path and holder
            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = imageHolder;
            inputGraphic.fileName = btn.getImage().getValue();

            //wait and listen for updates for our softbutton graphic bitmap from putfile, this is our softbutton graphic
            Action<OutputGraphic> sbGraphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != appItem.getAppId()) return;
                    Windows.UI.Xaml.Controls.Image image = (Windows.UI.Xaml.Controls.Image)graphicStruct.holder;
                    image.Visibility = Visibility.Visible;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.Source = bitmapUpdate;
                }
            };

            //subscribe for updates for our softbutton graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(sbGraphicDelegate, inputGraphic);
        }

        public static void handleSoftButton(AppItem appItem, SoftButton btn, Windows.UI.Xaml.Controls.Primitives.ToggleButton tgBtn, TextBlock txtBlk, Windows.UI.Xaml.Controls.Image tgBtnImg, TextBlock tgBtnStaticImg, int Index, int[] softButtonsId, bool[] softButtonHighlighted)
        {
            tgBtn.Visibility = Visibility.Visible;
            tgBtn.IsChecked = (bool)btn.getIsHighlighted();
            softButtonHighlighted[Index] = (bool)btn.getIsHighlighted();
            softButtonsId[Index] = (int)btn.getSoftButtonID();

            tgBtnImg.Visibility = Visibility.Collapsed;
            tgBtnStaticImg.Visibility = Visibility.Collapsed;
            txtBlk.Visibility = Visibility.Collapsed;

            if (btn.getImage() != null && (btn.getType() == SoftButtonType.IMAGE || btn.getType() == SoftButtonType.BOTH))
            {
                if (btn.getImage().getImageType() == ImageType.DYNAMIC)
                {
                    tgBtnImg.Visibility = Visibility.Visible;
                    handleSoftButtonGraphics(appItem, btn, tgBtnImg);
                }
                else
                {
                    tgBtnStaticImg.Visibility = Visibility.Visible;
                    if (btn.getImage().getValue() != null && btn.getImage().getValue().Trim() != "")
                    {
                        string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(btn.getImage().getValue());
                        if (staticImage != null && staticImage != "")
                        {
                            tgBtnStaticImg.Text = staticImage;
                        }
                        else
                        {
                            tgBtnStaticImg.Text = "\uEB9F";
                        }
                    }
                    else
                    {
                        tgBtnStaticImg.Text = "\uEB9F";
                    }
                }
            }

            if (btn.getType() == SoftButtonType.TEXT || btn.getType() == SoftButtonType.BOTH)
            {
                txtBlk.Visibility = Visibility.Visible;
                txtBlk.Text = btn.getText();

                if (btn.getType() == SoftButtonType.BOTH)
                    txtBlk.Margin = new Thickness(5, 0, 0, 0);
            }

        }
    }
}
