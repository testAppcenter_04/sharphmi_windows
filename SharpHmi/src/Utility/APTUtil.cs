﻿using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Media.Capture;
using Windows.Media.SpeechRecognition;
using Windows.Media.SpeechSynthesis;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SharpHmi.src.Utility
{
    class APTUtil : IRefreshListener
    {
        private SpeechRecognizer speechRecognizer;
        private StringBuilder dictatedTextBuilder = new StringBuilder();
        SpeechRecognitionCompilationResult result;
        PerformAudioPassThru performAudioPassThruObj;
        bool IsMicAvailable = true;
        SpeechSynthesizer synthesizer;
        ContentDialog showDialog;
        DispatcherTimer speechRecognizerTimer;
        DateTimeOffset startTime;
        private int? millisec = 0;
        private string speechStatus;
        Windows.Storage.StorageFolder storageFolder;
        Windows.Storage.StorageFile audioFile;
        AppInstanceManager appInstanceManager;

        public APTUtil(AppInstanceManager instance)
        {
            appInstanceManager = instance;
            appInstanceManager.addRefreshListener(this, typeof(PerformAudioPassThru));
        }

        public void onUiPerformAudioPassThruRequestCallback()
        {
            if (AppInstanceManager.AppInstance == null)
                return;

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

            if (appItem == null)
                return;

            performAudioPassThruObj = appItem.getPerformAudioPassThru();

            if (performAudioPassThruObj == null)
                return;

            checkMicPermission();

            if (performAudioPassThruObj.getMaxDuration() != null)
                millisec = performAudioPassThruObj.getMaxDuration();

        }
        public void SpeechRecognizerTimer()
        {
            speechRecognizerTimer = new DispatcherTimer();
            speechRecognizerTimer.Tick += speechRecognizer_Tick;
            speechRecognizerTimer.Interval = new TimeSpan(0, 0, 1);
            startTime = DateTimeOffset.Now;
            speechRecognizerTimer.Start();
        }

        void speechRecognizer_Tick(object sender, object e)
        {
            DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - startTime;

            if (span.TotalMilliseconds >= millisec)
            {
                StopRecognitionSession();
            }
        }
        public async void StopRecognitionSession()
        {
            try
            {
                speechRecognizerTimer.Stop();
                speechStatus = "done";
                if (speechRecognizer.ContinuousRecognitionSession != null)
                {
                    try
                    {
                        await speechRecognizer.ContinuousRecognitionSession.StopAsync();
                    }
                    catch
                    {

                    }
                }
                showDialog.Hide();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public IAsyncInfo AsyncInfo { get; set; }

        private async void checkMicPermission()
        {
            try
            {
                var MyMediaCapture = new MediaCapture();
                var settings = new MediaCaptureInitializationSettings();
                settings.StreamingCaptureMode = StreamingCaptureMode.Audio;
                await MyMediaCapture.InitializeAsync(settings);
            }
            catch (Exception ex)
            {
                IsMicAvailable = false;
                Console.WriteLine(ex);
            }
            if (IsMicAvailable)
            {
                doPerformAudioPassThru();
            }
            else
            {
                AppUtils.ShowToastNotification("Microphone Permision not given!", "Please give microphone permission before AudioPassThru request", 3);
                CheckUserWantToGivePermissionAgain();
            }
        }

        private async void CheckUserWantToGivePermissionAgain()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog showDialog = new MessageDialog("Do you want to give Mic Permission", AppInstanceManager.activatedAppName);
                showDialog.Commands.Add(new UICommand("Yes")
                {
                    Id = 0
                });
                showDialog.Commands.Add(new UICommand("No")
                {
                    Id = 1
                });
                showDialog.DefaultCommandIndex = 0;
                showDialog.CancelCommandIndex = 1;

                var dialog = await showDialog.ShowAsync();
                if ((int)dialog.Id == 0)
                {
                    AppUtils.ShowToastNotification("Mic Permission!", "Please switch on mic permission for " + AppInstanceManager.activatedAppName, 3);

                    await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings:privacy-microphone"));
                    IsMicAvailable = true;
                }
                else
                {
                    AppUtils.ShowToastNotification("Microphone Permision denied!", "Please give microphone permission before AudioPassThru request", 3);

                    HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru performAudioPassThru = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
                    performAudioPassThru.setId(performAudioPassThruObj.getId());
                    performAudioPassThru.setMethod();
                    performAudioPassThru.setResultCode(HmiApiLib.Common.Enums.Result.ABORTED);
                    AppInstanceManager.AppInstance.sendRpc(performAudioPassThru);
                }
            });
        }
        public static ContentDialog contentdialog;
        private async void doPerformAudioPassThru()
        {
            this.speechRecognizer = new SpeechRecognizer();
            result = await speechRecognizer.CompileConstraintsAsync();
            synthesizer = new SpeechSynthesizer();

            string level1FolderName = "storage";
            storageFolder = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFolderAsync(level1FolderName);

            audioFile = await storageFolder.CreateFileAsync("audio.wav", Windows.Storage.CreationCollisionOption.ReplaceExisting);

            speechRecognizer.ContinuousRecognitionSession.ResultGenerated += ContinuousRecognitionSession_ResultGenerated;
            speechRecognizer.ContinuousRecognitionSession.Completed += ContinuousRecognitionSession_Completed;

            //Timeout on Silence
            //speechRecognizer.ContinuousRecognitionSession.AutoStopSilenceTimeout = TimeSpan.FromSeconds(4);
            dictatedTextBuilder.Clear();

            if (speechRecognizer.State == SpeechRecognizerState.Idle)
            {
                try
                {
                    await speechRecognizer.ContinuousRecognitionSession.StartAsync(SpeechContinuousRecognitionMode.Default);

                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                        SpeechRecognizerTimer();
                        showDialog = new ContentDialog
                        {
                            Content = "Speak",

                            Title = AppInstanceManager.activatedAppName,
                            PrimaryButtonText = "Done",
                            SecondaryButtonText = "Cancel"
                        };

                        ContentDialogResult dialog = await showDialog.ShowAsync();
                        if (dialog == ContentDialogResult.Primary)
                        {
                            speechStatus = "done";
                            try
                            {
                                await speechRecognizer.ContinuousRecognitionSession.StopAsync();
                            }
                            catch
                            {

                            }
                        }
                        else if (dialog == ContentDialogResult.Secondary)
                        {
                            speechStatus = "cancelled";
                            try
                            {
                                await speechRecognizer.ContinuousRecognitionSession.CancelAsync();
                            }
                            catch
                            {

                            }
                        }
                    }
                );
                }

                catch
                {
                    HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru performAudioPassThru = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
                    performAudioPassThru.setId(performAudioPassThruObj.getId());
                    performAudioPassThru.setMethod();
                    performAudioPassThru.setResultCode(HmiApiLib.Common.Enums.Result.ABORTED);
                    AppInstanceManager.AppInstance.sendRpc(performAudioPassThru);

                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                        ContentDialog Dialog = new ContentDialog()
                        {
                            Title = "The speech privacy policy was not accepted",
                            Content = "You need to turn on Speech Services using 'Get to know me' button.",
                            PrimaryButtonText = "Go to settings",
                            SecondaryButtonText = "Cancel"
                        };
                        if (await Dialog.ShowAsync() == ContentDialogResult.Primary)
                        {
                            const string uriToLaunch = "ms-settings:privacy-speechtyping";

                            var uri = new Uri(uriToLaunch);

                            var success = await Windows.System.Launcher.LaunchUriAsync(uri);

                            if (!success) await new ContentDialog
                            {
                                Title = "Oops! Something went wrong...",
                                Content = "The settings app could not be opened.",
                                PrimaryButtonText = "Ok"
                            }.ShowAsync();
                        }
                    });
                }
            }
        }

        private async void ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            dictatedTextBuilder.Append(args.Result.Text + " ");
            SpeechSynthesisStream stream = await synthesizer.SynthesizeTextToStreamAsync(dictatedTextBuilder.ToString());
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    IBuffer buffer = reader.ReadBuffer((uint)stream.Size);
                    await FileIO.WriteBufferAsync(audioFile, buffer);
                }
            });
        }

        private void ContinuousRecognitionSession_Completed(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            if (speechStatus == "cancelled")
            {
                HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru performAudioPassThru = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
                performAudioPassThru.setId(performAudioPassThruObj.getId());
                performAudioPassThru.setMethod();
                performAudioPassThru.setResultCode(HmiApiLib.Common.Enums.Result.ABORTED);
                AppInstanceManager.AppInstance.sendRpc(performAudioPassThru);
            }
            else
            {
                HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru performAudioPassThru = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
                performAudioPassThru.setId(performAudioPassThruObj.getId());
                performAudioPassThru.setMethod();
                performAudioPassThru.setResultCode(HmiApiLib.Common.Enums.Result.SUCCESS);
                AppInstanceManager.AppInstance.sendRpc(performAudioPassThru);

                millisec = 0;
            }
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState refreshDataState)
        {
            if (rpcType == typeof(PerformAudioPassThru))
                 onUiPerformAudioPassThruRequestCallback();
        }
    }
}
