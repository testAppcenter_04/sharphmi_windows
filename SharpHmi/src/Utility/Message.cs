﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace SharpHmi.src.Utility
{
    class Message
    {
        LogMessage logMessage = null;
       public string lblTime;
        public string lblTop;
        public SolidColorBrush lblTopColor;
        string lblBottom;
        Visibility lblBottomVisibility;
        SolidColorBrush lblBottomColor;

        public LogMessage LogMessage { get => logMessage; set => logMessage = value; }
        public string LblTop { get => lblTop; set => lblTop = value; }
        public string LblBottom { get => lblBottom; set => lblBottom = value; }
        public string LblTime { get => lblTime; set => lblTime = value; }
        public SolidColorBrush LblTopColor { get => lblTopColor; set => lblTopColor = value; }
        public SolidColorBrush LblBottomColor { get => lblBottomColor; set => lblBottomColor = value; }
        public Visibility LblBottomVisibility { get => lblBottomVisibility; set => lblBottomVisibility = value; }

        public static Message processLogMessage(LogMessage message)
        {
            if (null == message) return null;

            Message msg = new Message();

            msg.LogMessage = message;
            msg.LblBottomVisibility = Visibility.Collapsed;

            if (message is StringLogMessage)
            {
                StringLogMessage myStringLog = (StringLogMessage)message;
                msg.lblTop = myStringLog.getMessage();
                msg.lblTime = myStringLog.getDate();
                msg.lblTopColor = new SolidColorBrush(Colors.Black);
            }
            
            else if (message is RpcLogMessage)
            {
                RpcMessage func = ((RpcLogMessage)message).getMessage();


                if (func.getRpcMessageType() == RpcMessageType.REQUEST)
                {
                    msg.lblTop = message.getPrependComment() + ((RpcRequest)func).method + " (" + func.getRpcMessageFlow().ToString().ToLower() + " " + func.getRpcMessageType().ToString().ToLower() + ")";
                    msg.lblTopColor = new SolidColorBrush(Colors.Blue);
                }
                else if (func.getRpcMessageType() == RpcMessageType.RESPONSE)
                {
                    msg.LblBottomVisibility = Visibility.Visible;
                    RpcResponse resp = (RpcResponse)func;

                    HmiApiLib.Common.Enums.Result resultCode = resp.getResultCode();

                    msg.lblTop = message.getPrependComment() + resp.getMethod() + " (" + resp.getRpcMessageFlow().ToString().ToLower() + " " + resp.getRpcMessageType().ToString().ToLower() + ")";

                    if ((resultCode == HmiApiLib.Common.Enums.Result.SUCCESS) ||
                        (resultCode == HmiApiLib.Common.Enums.Result.WARNINGS))
                    {
                        msg.lblBottomColor = new SolidColorBrush(Colors.Green);
                        msg.lblTopColor = new SolidColorBrush(Colors.Green);
                    }
                    else
                    {
                        msg.lblBottomColor = new SolidColorBrush(Colors.Red);
                        msg.lblTopColor = new SolidColorBrush(Colors.Red);
                    }
                    msg.lblBottom = (resultCode).ToString();
                }
                else if ((func.getRpcMessageType() == RpcMessageType.NOTIFICATION) || (func.getRpcMessageType() == RpcMessageType.REQUEST_NOTIFY))
                {
                    if (func.getRpcMessageType() == RpcMessageType.NOTIFICATION)
                    {
                        msg.lblTop = message.getPrependComment() + ((RpcNotification)func).method + " (" + func.getRpcMessageFlow().ToString().ToLower() + " " + func.getRpcMessageType().ToString().ToLower() + ")";
                    }
                    else
                    {
                        msg.LblTop = message.getPrependComment() + ((RequestNotifyMessage)func).method + " (" + func.getRpcMessageFlow().ToString().ToLower() + " " + RpcMessageType.NOTIFICATION.ToString().ToLower() + ")";
                    }
                    msg.lblTopColor = new SolidColorBrush(Colors.YellowGreen);
                }

                msg.lblTime = message.getDate();
            }

            return msg;
        }
    }
}
