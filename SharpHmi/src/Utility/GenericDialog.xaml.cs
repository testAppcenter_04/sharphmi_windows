﻿using Windows.UI.Xaml.Controls;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.Utility
{
    public sealed partial class GenericDialog : ContentDialog
    {
        private TextBox editText;
        public GenericDialog()
        {
            InitializeComponent();
            EditText = editTextField;
        }

        public TextBox EditText { get => editText; set => editText = value; }
    }
}
