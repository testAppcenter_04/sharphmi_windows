﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System;
using Windows.UI.Xaml.Controls;


// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.Utility
{
    public sealed partial class ConsoleLogPreview : ContentDialog
    {
        RpcMessage message = null;

        public ConsoleLogPreview(RpcMessage message)
        {
            this.InitializeComponent();
            this.message = message;
            ProcessConsoleLogPreview();
        }

        public void ProcessConsoleLogPreview()
        {
            int? corrId = -1;

            consoleLogPreview_editChkBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            consoleLogPreview_jsonContent.AllowFocusOnInteraction = (bool)consoleLogPreview_editChkBox.IsChecked;
            consoleLogPreview_jsonContent.IsEnabled = false;

            if (message is RpcRequest)
            {
                corrId = ((RpcRequest)message).getId();
            }
            else if (message is RpcResponse)
            {
                corrId = (int)((RpcResponse)message).getId();
            }

            if (message.getRpcMessageFlow() == RpcMessageFlow.OUTGOING)
            {
                consoleLogPreview_editChkBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

            string rawJSON = Utils.getSerializedRpcMessage(message);
            if (rawJSON == null) return;


            string finalJSON = rawJSON;

            consoleLogPreview_jsonContent.Text = finalJSON;

            consoleLogPreview_editChkBox.Tapped += (s, args) =>
            {
                if (!(bool)consoleLogPreview_editChkBox.IsChecked)
                {
                    consoleLogPreview_jsonContent.Text = finalJSON;
                    consoleLogPreview_jsonContent.AllowFocusOnInteraction = false;
                    consoleLogPreview_jsonContent.IsEnabled = false;
                }
                else
                {
                    consoleLogPreview_jsonContent.AllowFocusOnInteraction = true;
                    consoleLogPreview_jsonContent.IsEnabled = true;
                }
            };


            Title = "Raw JSON" + (corrId != -1 ? " (Corr ID " + corrId + ")" : "");

            PrimaryButtonClick += ContentDialog_PrimaryButtonClick;
            PrimaryButtonText = "Getters";
            SecondaryButtonClick += ContentDialog_SecondaryButtonClick;
            SecondaryButtonText = "Resend";
            CloseButtonClick += ContentDialog_CloseButtonClick;
            CloseButtonText = "Cancel";

            if (message.rpcMessageFlow == RpcMessageFlow.OUTGOING)
            {
                IsSecondaryButtonEnabled = true;
            }
            else
            {
                IsSecondaryButtonEnabled = false;
            }
        }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Hide();

            string sInfo = RpcMessageGetterInfo.viewDetails(message, false, 0);
            var gettersDialog = new GenericDialog();

            gettersDialog.Title = "GetterInfo";
            gettersDialog.EditText.Text = sInfo;
            gettersDialog.EditText.AllowFocusOnInteraction = false;
            gettersDialog.EditText.IsEnabled = false;

            gettersDialog.PrimaryButtonClick += GettersDialog_PrimaryButtonClick;
            gettersDialog.PrimaryButtonText = "Back";

            gettersDialog.CloseButtonClick += ContentDialog_CloseButtonClick;
            gettersDialog.CloseButtonText = "Cancel";

            await gettersDialog.ShowAsync();
        }

        private async void GettersDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await ShowAsync();
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RpcMessage updMsg = null;
            updMsg = Utils.getDeserializedRpcMessage(consoleLogPreview_jsonContent.Text);
            if (updMsg != null)
            {
                AppInstanceManager.AppInstance.sendRpc(updMsg);
            }
        }

        private void ContentDialog_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
