﻿using System;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpHmi.src.Utility;
using Windows.ApplicationModel.Core;
using Windows.UI.Popups;

namespace SharpHmi.src.Manager
{
    public class MicManager
    {
        private const string audioFilename = "audio.wav";
       // private string filename;
        private MediaCapture capture;
        private InMemoryRandomAccessStream buffer;
        private int corrID = 0;
        private int millisec = 0;
        private static bool Recording;
        private MainScreen.APTContentdialog aptContentdialog;

        DispatcherTimer dispatcherTimer;
        DateTimeOffset startTime;

        private async Task<bool> init()
        {
            if (buffer != null)
            {
                buffer.Dispose();
            }
            buffer = new InMemoryRandomAccessStream();
            if (capture != null)
            {
                capture.Dispose();
            }
            try
            {
                MediaCaptureInitializationSettings settings = new MediaCaptureInitializationSettings
                {
                    StreamingCaptureMode = StreamingCaptureMode.Audio
                };
                capture = new MediaCapture();
                await capture.InitializeAsync(settings);
                capture.RecordLimitationExceeded += (MediaCapture sender) =>
                {
                    Stop();
                    throw new Exception("Exceeded Record Limitation");
                };
                capture.Failed += (MediaCapture sender, MediaCaptureFailedEventArgs errorEventArgs) =>
                {
                    Recording = false;
                    throw new Exception(string.Format("Code: {0}. {1}", errorEventArgs.Code, errorEventArgs.Message));
                };
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UnauthorizedAccessException))
                {
                    throw ex.InnerException;
                }
                throw;
            }
            return true;
        }


        public void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            //IsEnabled defaults to false
            startTime = DateTimeOffset.Now;
            dispatcherTimer.Start();
            //IsEnabled should now be true after calling start
        }

        void dispatcherTimer_Tick(object sender, object e)
        {
            WriteFile();
             DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - startTime;

            if (span.TotalMilliseconds >= millisec)
            {
                dispatcherTimer.Stop();
                Stop();

                HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru);
                    AppInstanceManager.AppInstance.sendRpc(HmiApiLib.Builder.BuildDefaults.buildDefaultMessage(type, corrID));
                }
                else
                {
                    tmpObj.setId(corrID);
                    AppInstanceManager.AppInstance.sendRpc(tmpObj);
                }
            }
        }

        public async void Record(int corrId, int millisecs, HmiApiLib.Controllers.UI.IncomingRequests.PerformAudioPassThru msg)
        {

            try
            {
                StorageFolder storageFolder = AppInstanceManager.storageFolder;

                if (storageFolder == null) return;

                StorageFile original = await storageFolder.GetFileAsync(audioFilename);

                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    try
                    {
                        await original.DeleteAsync();
                    }
                    catch (Exception ex) { }
                });
            }
            catch (Exception ex) { }

            await init();
            try
            {
                await capture.StartRecordToStreamAsync(MediaEncodingProfile.CreateWav(AudioEncodingQuality.Low), buffer);
            }
            catch
            {

            }
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {

                aptContentdialog = new MainScreen.APTContentdialog(msg);

                aptContentdialog.doneButton.Tapped += doneButtonDialog_Tapped;
                aptContentdialog.cancelButton.Tapped += cancelButtonDialog_Tapped;

                await aptContentdialog.ShowAsync();

            });
            if (Recording) throw new InvalidOperationException("cannot excute two records at the same time");

            corrID = corrId;
            millisec = millisecs;

            Recording = true;
            DispatcherTimerSetup();

        }

        private void cancelButtonDialog_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            Stop();
            HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru performAudioPassThru = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
            performAudioPassThru.setId(corrID);
            performAudioPassThru.setMethod();
            performAudioPassThru.setResultCode(HmiApiLib.Common.Enums.Result.ABORTED);
            AppInstanceManager.AppInstance.sendRpc(performAudioPassThru);
            return;
        }

        private void doneButtonDialog_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            millisec = 0;
        }

        public async void Stop()
        {
            try
            {
                await capture.StopRecordAsync();
            }
            catch
            {

            }
            Recording = false;
            if (aptContentdialog != null)
                aptContentdialog.Hide();
        }


        public async Task WriteFile()
        {
            MediaElement playback = new MediaElement();
            IRandomAccessStream audio = buffer.CloneStream();
            if (audio == null) throw new ArgumentNullException("buffer");
            StorageFolder storageFolder = AppInstanceManager.storageFolder;//await ApplicationData.Current.LocalFolder.GetFolderAsync("storage");

            try
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    try
                    {
                        StorageFile storageFile = await storageFolder.CreateFileAsync(audioFilename, CreationCollisionOption.ReplaceExisting);


                       // filename = storageFile.Name;
                        using (IRandomAccessStream fileStream = await storageFile.OpenAsync(FileAccessMode.ReadWrite))
                        {
                            await RandomAccessStream.CopyAndCloseAsync(audio.GetInputStreamAt(0), fileStream.GetOutputStreamAt(0));
                            await audio.FlushAsync();
                            audio.Dispose();
                        }
                    }
                    catch (Exception ex)
                    { }

                });
            }
            catch (Exception ex)
            {

            }
        }
    }
}
