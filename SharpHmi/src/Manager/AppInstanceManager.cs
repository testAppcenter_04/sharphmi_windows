﻿using HmiApiLib.Proxy;
using System;
using HmiApiLib;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;
using HmiApiLib.Controllers.BasicCommunication.IncomingRequests;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingNotifications;
using HmiApiLib.Controllers.RC.IncomingRequests;
using HmiApiLib.Controllers.SDL.IncomingNotifications;
using HmiApiLib.Controllers.SDL.IncomingResponses;
using HmiApiLib.Controllers.VehicleInfo.IncomingRequests;
using HmiApiLib.Interfaces;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using HmiApiLib.Builder;
using SharpHmi.src.Models;
using System.Collections.Generic;
using SharpHmi.src.Utility;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System.Linq;
using HmiApiLib.Controllers.RC.IncomingNotifications;

using SharpHmi.src.interfaces;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Storage.AccessCache;
using Windows.ApplicationModel.Core;
using SharpHmi.src.MainScreen;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.UI.IncomingRequests;
using System.Diagnostics;
using Windows.UI.Xaml.Media;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Drawing;
using SharpHmi.src.Manager;
using HmiApiLib.Controllers.UI.IncomingNotifications;
using Windows.UI.Xaml.Controls;
using Windows.Media.SpeechSynthesis;
using Microsoft.Toolkit.Uwp.Notifications;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using SharpHmi.src.CachedClasses;
using SharpHmi.src.CachedRPC;
using static SharpHmi.src.Manager.GraphicManager;

namespace SharpHmi
{
    class AppInstanceManager : ProxyHelper, IConnectionListener, IDispatchingHelper<LogMessage>, ICachedClassListener
    {
        private static volatile AppInstanceManager instance;
        private static object syncRoot = new Object();
        public ObservableCollection<Message> _msgAdapter = new ObservableCollection<Message>();
        public static Boolean bRecycled = true;
        public enum ConnectionState { CONNECTED, DISCONNECTED, NOT_INITIALIZED }
        public static ConnectionState currentState;
        public TrulyObservableCollection<AppItem> appList = AppsPage.appList;

        public List<MenuParams> menuParamList = new List<MenuParams>();
        public List<AllAppsModel> permissionList;
        public string LogsfileName = "SharpHMI_" + System.DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xml";
        XmlDocument doc;

        public static Dictionary<int, string> appIdPolicyIdDictionary = new Dictionary<int, string>();

        public Dictionary<int, List<int?>> commandIdList = new Dictionary<int, List<int?>>();
        private IApplicationCallback applicationCallback;
        public IAddCommand addCommandCallback;
        public IPolicies policiesCallback;
        public IAddSubMenu addSubMenuCallback;
        public static bool isEmbeddedAudioActive = false;
        public static bool isEmbeddedPhoneActive = false;
        public static bool isEmbeddedNavActive = false;

        public static Windows.UI.Xaml.Controls.Frame insideFrame = null;

        public MainPage refMainPage = null;

        public static VLC.MediaElement mediaElement;
        public enum SelectionMode { NONE, DEBUG_MODE, BASIC_MODE };
        public AppSetting appSetting = null;
        StorageFile file = null;
        public static StorageFolder storageFolder = null;
        public static StorageFolder storageFolderForXML = null;
        public bool audioPassThruWithSpeechAPI = true;

        public static int activatedAppId = -1;
        public static int lastProjectedAppId = -1;
        public static string activatedAppName;
        private bool videoStreamAvail = false;
        APTUtil audioPassThruUtil = null;

        List<string> speechList = new List<string>();
        private List<KeyValuePair<IRefreshListener, Type>> listenerList = new List<KeyValuePair<IRefreshListener, Type>>();

        public void addRefreshListener(IRefreshListener listener, Type type)
        {
            listenerList.Add(new KeyValuePair<IRefreshListener, Type>(listener, type));
        }

        private void callRefreshListeners(Type type, int? appID, RefreshDataState refreshDataState)
        {
            foreach (KeyValuePair<IRefreshListener,Type> pair in listenerList)
            {
                if (type == pair.Value)
                {
                    IRefreshListener myListener = pair.Key;
                    myListener.onRefreshAvailable(type, appID, refreshDataState);
                }
            }
        }

        public bool isVideoStreamingAvailable
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return videoStreamAvail; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set {  videoStreamAvail = value; }
        }

        private bool audioStreamAvail;
        public bool isAudioStreamingAvailable
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return audioStreamAvail; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { audioStreamAvail = value; }
        }

        public Process audioPlayer;
        Windows.Storage.ApplicationDataCompositeValue SavedComposite =  AppUtils.getAppPermissionForVehicle();

        VLC.MediaElement HmiMediaPlayer = new VLC.MediaElement();
        public int vrPerformInteractionCorrId;
        public JObject staticImagesJsonObject;

        public static AppInstanceManager AppInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            currentState = ConnectionState.NOT_INITIALIZED;
                            instance = new AppInstanceManager();
                        }
                    }
                }
                else
                {
                    bRecycled = true;
                }
                return instance;
            }
        }

        private AppInstanceManager()
        {
            using (StreamReader file = File.OpenText(@"src\Utility\StaticImages.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                staticImagesJsonObject = (JObject)JToken.ReadFrom(reader);
            }
            audioPassThruUtil = new APTUtil(this);
        }

        public string staticImageVal(String inpuVal)
        {
            string processedVal = "";

            if (inpuVal != null && inpuVal.Trim().Length > 0)
            {
                if (inpuVal.StartsWith("0x", StringComparison.CurrentCultureIgnoreCase))
                {
                    processedVal = inpuVal.Substring(2);
                }
                else
                {
                    processedVal = inpuVal;
                }

                if (processedVal.Trim().Length > 0)
                {
                    if (checkIfValidHexValInString(processedVal.Trim()))
                    {
                        if (processedVal.Trim().Length == 1)
                        {
                            processedVal = "0x" + "0" + processedVal.Trim();  //Append with 0 for mapping from StaticImages file.
                        }
                        else
                        {
                            processedVal = "0x" + processedVal.Trim();
                        }

                        processedVal = (string)staticImagesJsonObject.SelectToken(processedVal);

                    } else
                    {
                        processedVal = "";
                    }
                } else
                {
                    processedVal = "";
                }
            }

            return processedVal;
        }

        public bool checkIfValidHexValInString(string inpVal)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(inpVal, @"\A\b[0-9a-fA-F]+\b\Z");
        }

        public async static void initializeStorageFolder()
        {
            if (storageFolder == null)
            {
                if (StorageApplicationPermissions.FutureAccessList.ContainsItem("PickedFolderToken"))
                {
                    storageFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync("PickedFolderToken");
                }
            }
        }

        public void setApplicationCallback(IApplicationCallback callback)
        {
            this.applicationCallback = callback;
        }

        public void setPoliciesCallback(IPolicies callback)
        {
            policiesCallback = callback;
        }
        public void setCommandCallback(MenuItems addMenu)
        {
            addCommandCallback = addMenu;
        }

        public void setAddSubMenuCallback(SubItems submenu)
        {
            addSubMenuCallback = submenu;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ObservableCollection<Message> getMessageAdapter()
        {
            return _msgAdapter;
        }

        public void onRefreshRegisteredApps()
        {
            if (refMainPage != null)
            {
                refMainPage.onRefreshRegisteredApps();
            }

        }


        public override void onBcAllowDeviceToConnectRequest(AllowDeviceToConnect msg)
        {
             
        }

        public AppItem getAppItem(int? id)
        {
            if (appList == null || id == null) return null;

            foreach (AppItem appItem in appList)
            {
                if (appItem == null || appItem.getApplication() == null) continue;

                if (appItem.getApplication().getAppID() == id)
                    return appItem;
            }

            return null;
        }

        private String getStorageDirFromIcon(String path)
        {
            if (path == null) return null;

            String value = null;

            int index = path.LastIndexOf("/");

            try
            {
                value = path.Substring(0, index);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            return value;
        }

        private async void addPersistentFiles(OnAppRegistered msg, AppItem appItem)
        {
            if (msg == null) return;

            if (msg.getApplication() == null) return;
            HMIApplication myApp = msg.getApplication();

            String icon = myApp.getIcon();
            if (icon == null) return;

            String storageDir = getStorageDirFromIcon(icon);

            if (storageDir == null) return;

            if (myApp.getPolicyAppID() == null) return;
            String policyAppID = myApp.getPolicyAppID();

            if (myApp.getDeviceInfo() == null) return;
            String deviceInfo = myApp.getDeviceInfo().id;

            if (deviceInfo == null) return;

            //naming format for app's sandbox directory is policy app id underscore device id, e.g. 584422004_cbbb33250ad75b18e19c73714eafaae429fdd5105d14d299ce5a81eb5b873d17
            String appsSandBox = policyAppID + "_" + deviceInfo;

            storageDir = storageDir + "/" + appsSandBox;

            if (AppInstanceManager.storageFolder == null) return;

            StorageFolder sandBoxFolder = null;

            try
            {
                sandBoxFolder = await AppInstanceManager.storageFolder.GetFolderAsync(appsSandBox);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }

            if (sandBoxFolder == null) return;

            try
            {
                IReadOnlyList<StorageFile> fileList = await sandBoxFolder.GetFilesAsync();
                foreach (StorageFile file in fileList)
                {
                    appItem.graphicManager.addGraphic(storageDir + "/" + file.DisplayName);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }
        }

        public override async void onBcAppRegisteredNotification(OnAppRegistered msg)
        {
            if (msg != null) {

                AppItem appItem = getAppItem(msg.getApplication().getAppID());

                if(appItem == null)
                {
                    appItem = new AppItem(msg);
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                    {
                        AppsPage.addItem(appItem);
                    });
                }

                addPersistentFiles(msg, appItem);

                if (appItem.getSetAppIcon() == null) //only try to set the app icon if one is not present
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                    {
                        //wait and listen for updates for our main graphic bitmap from putfile
                        Action<OutputGraphic> mainGraphicDelegate = delegate (OutputGraphic outputGraphic)
                        {
                            if (outputGraphic.holder != null)
                            {
                                AppItem myItem = (AppItem)outputGraphic.holder;
                                appItem.appsIcon = outputGraphic.graphic;
                            }
                        };

                        InputGraphic inputGraphic = new InputGraphic();
                        inputGraphic.holder = appItem;
                        inputGraphic.fileName = appItem.getAppIconPath();
                        //subscribe for updates for our main graphic bitmap from putfile
                        appItem.graphicManager.addRefreshListener(mainGraphicDelegate, inputGraphic);
                        BitmapImage bitmap = appItem.graphicManager.getGraphic(appItem.getAppIconPath());
                        if (bitmap != null)
                            appItem.appsIcon = bitmap;
                        appItem.graphicManager.addGraphic(msg);
                    });
                }
                callRefreshListeners(typeof(OnAppRegistered), appItem.getApplication().getAppID(), RefreshDataState.AFTER_DATA_CHANGE);

                if (null != msg.getApplication())
                {
                    if(!appIdPolicyIdDictionary.ContainsKey((int)msg.getApplication().getAppID()))
                        appIdPolicyIdDictionary.Add((int)msg.getApplication().getAppID(), msg.getApplication().getPolicyAppID());
                }
            }
        }

        public async override void onBcAppUnRegisteredNotification(OnAppUnregistered msg)
        {
            int appID = (int)msg.getAppId();

            callRefreshListeners(typeof(OnAppUnregistered), appID, RefreshDataState.BEFORE_DATA_CHANGE);

            AppItem myItem = null;

            for (int i = 0; i < appList.Count; i++)
			{
                if (null == appList[i].getApplication()) continue;

                if ((appList[i].getApplication().getAppID() == appID) || (appList[i].getApplication().getAppID() == getCorrectAppId(appID)))
				{
					int tmpAppId =(int) appID;

                    if (appList[i].getApplication().getAppID() == activatedAppId)
                        activatedAppId = -1;

                    if (appList[i].getApplication().getAppID() == lastProjectedAppId)
                    {
                        lastProjectedAppId = -1;
                    }
                    if (appList[i].getApplication().getAppID() == getCorrectAppId(tmpAppId))
					{
						tmpAppId = getCorrectAppId(tmpAppId);
					}

                    if (appIdPolicyIdDictionary.ContainsKey(tmpAppId))
                    {
                        appIdPolicyIdDictionary.Remove(tmpAppId);
                    }

                    myItem = appList[i];

                    if (appList.Count == 0 && MediaTemplate.timer != null)
                    {
                        //Let's release the Timer resource
                        MediaTemplate.timer.Stop();
                        MediaTemplate.timer = null;
                    }

                    break;
                }
            }

            if (myItem != null)
            {

                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                {
                    AppsPage.removeItem(myItem);
                });
            }

            if (permissionList != null)
                permissionList.Clear();
           

        }

        public override void onBcDecryptCertificateRequest(DecryptCertificate msg)
        {
             
        }

        public override void onBcDialNumberRequest(DialNumber msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onBcGetSystemInfoRequest(GetSystemInfo msg)
        {
            if (appSetting.getBCGetSystemInfo())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onBcMixingAudioSupportedRequest(MixingAudioSupported msg)
        {
            if (appSetting.getBCMixAudioSupport())
            {
                int corrID = msg.getId();
                HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrID));
                }
                else
                {
                    tmpObj.setId(corrID);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onBcOnFileRemovedNotification(OnFileRemoved msg)
        {
             
        }

        public override void onBcOnResumeAudioSourceNotification(OnResumeAudioSource msg)
        {
             
        }

        public override void onBcOnSDLCloseNotification(OnSDLClose msg)
        {
             
        }

        public override void onBcOnSDLPersistenceCompleteNotification(OnSDLPersistenceComplete msg)
        {
             
        }

        public override void onBcPolicyUpdateRequest(PolicyUpdate msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onBcPutfileNotification(OnPutFile msg)
        {
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(msg.getAppID());

            if (appItem != null)
            {
                appItem.graphicManager.addGraphic(msg);
            }                
        }

        public override void onBcSystemRequestRequest(SystemRequest msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onBcUpdateAppListRequest(UpdateAppList msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onBcUpdateDeviceListRequest(UpdateDeviceList msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onButtonsButtonPressRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.ButtonPress msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                sendRpc(BuildRpc.buildButtonsButtonPressResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onButtonsGetCapabilitiesRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.GetCapabilities msg, InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam)
        {
            if (appSetting.getButtonsGetCapabilities())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities();
                tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void OnButtonSubscriptionNotification(OnButtonSubscription msg)
        {
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(msg.getAppId());

            if (appItem != null)
            {
                bool isButtonAlreadyAdded = false;

                if (msg.getSubscribe() != null)
                {
                    foreach (OnButtonSubscription btnSubsc in appItem.getButtonSubscriptions())
                    {
                        if (btnSubsc.getName() == msg.getName())
                        {
                            isButtonAlreadyAdded = true;

                            if (!(bool)msg.getSubscribe())
                                appItem.getButtonSubscriptions().Remove(btnSubsc);
                            break;
                        }
                    }

                    if (!isButtonAlreadyAdded)
                    {
                        if ((bool)msg.getSubscribe())
                            appItem.getButtonSubscriptions().Add(msg);
                    }
                }
            }
            callRefreshListeners(typeof(OnButtonSubscription), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
        }

        public override void onNavAlertManeuverRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.AlertManeuver msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavGetWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.GetWayPoints msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavIsReadyRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.IsReady msg)
        {
            if (appSetting.getNavigationIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.Navigation, true, HmiApiLib.Common.Enums.Result.SUCCESS));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        private object audioPlayerLock = new object();
        

        public void audioStreaming()
        {            
            string audio = "http://" + AppInstanceManager.AppInstance.getAppSetting().getIPAddress() + ":" + AppInstanceManager.AppInstance.getAppSetting().getAudioPort();

            Dictionary<string, object> audioOptions = new Dictionary<string, object>();
            audioOptions.Add("demux", "rawaud");
            audioOptions.Add("rawaud-channels", "1");
            audioOptions.Add("rawaud-samplerate", "16000");
            audioOptions.Add("network-caching", "1000");
            MainPage.audPlayer.Options = audioOptions;
            MainPage.audPlayer.AutoPlay = false;
            MainPage.audPlayer.Source = audio;
            

            if ((MainPage.audPlayer != null) && AppInstanceManager.AppInstance.isAudioStreamingAvailable)
            {
                MainPage.audPlayer.Stop();
                MainPage.audPlayer.Play();

            }
        }

        public override async void onNavOnAudioDataStreamingNotification(OnAudioDataStreaming msg)
        {
            if (msg != null && msg.getAvailable() != null)
            {
                isAudioStreamingAvailable = (bool)msg.getAvailable();

                try
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        if (!isAudioStreamingAvailable)
                        {
                            lock (audioPlayerLock)
                            {
                                releaseAudioPlayer();
                            }
                            
                        }
                        else
                        {
                            lock (audioPlayerLock)
                            {
                                audioStreaming();
                            }
                        }
                    });
                }
                catch (Exception ex)
                {
                }
            }
        }

        public void releaseAudioPlayer()
        {
            if (MainPage.audPlayer != null)
            {
                System.Threading.Thread.Sleep(1000);
                //do nothing, stop the player just before we start streaming again
                AppInstance.addMessageToUI(new StringLogMessage("Releasing Audio Player Called"));
            }
        }

        private void AudPlayer_MediaEnded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }

        private void AudPlayer_CurrentStateChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }

        public async override void onNavOnVideoDataStreamingNotification(OnVideoDataStreaming msg)
        {
            if (msg != null && msg.getAvailable() != null)
            {
                isVideoStreamingAvailable = (bool)msg.getAvailable();
                try
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, async () =>
                    {
                        if (isVideoStreamingAvailable)
                        {
                            await handleMediaStreaming();
                        }
                        else
                        {
                            releaseVideoPlayer();
                        }


                    });
                }
                catch (Exception ex)
                {
                }
            }
        }

        private object vidPlayerLock = new object();
        private async System.Threading.Tasks.Task PlayVideo()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
            {
                lock (vidPlayerLock)
                {
                    MainPage.vidPlayer.Play();
                    //MainPage.vidPlayer.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
            });
        }

        public async Task handleMediaStreaming()
        {
            try
            {
                if (AppInstanceManager.AppInstance.isVideoStreamingAvailable)
                {
                    await PlayVideo();
                }
            }
            catch (Exception ex)
            {
                AppInstance.addMessageToUI(new StringLogMessage("Exception during PlayVideo"));
            }
        }

        public void releaseVideoPlayer()
        {
            if (mediaElement != null)
            {
                lock (vidPlayerLock)
                {
                    mediaElement.Stop();
                }
                AppInstance.addMessageToUI(new StringLogMessage("Releasing Video Player"));
            }
        }

        public override void onNavOnWayPointChangeNotification(OnWayPointChange msg)
        {
             
        }

        public async override void onNavSendLocationRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SendLocation msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation>(tmpObj.getMethod());
          
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation);
                try
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        if (insideFrame != null)
                        {
                            insideFrame.Navigate(typeof(NavPage), msg);
                        }
                        else
                        {
                            sendRpc(BuildRpc.buildNavSendLocationResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                        }
                    });
                }
                catch (Exception e)
                {
                    if (e.Message.Equals("Disallowed"))
                        sendRpc(BuildRpc.buildNavSendLocationResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                    else
                        sendRpc(BuildRpc.buildNavSendLocationResponse(corrId, HmiApiLib.Common.Enums.Result.RETRY));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        List<TextFieldStruct> navigationText;
        public string logo { get; set; }
        ToastContent toastContent;

        private async Task onShowConstantTBTAsync(ShowConstantTBT msg)
        {
            String fullNavigationString = null;


            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                navigationText = new List<TextFieldStruct>();


                if ((msg.getNavigationTexts() != null) && (msg.getNavigationTexts().Count > 0))
                {
                    foreach (TextFieldStruct item in msg.getNavigationTexts())
                    {
                        if (item != null)
                            navigationText.Add(item);
                    }


                    for (int i = 0; i < navigationText.Count; i++)
                    {
                        TextFieldStruct textFieldStruct = navigationText[i];
                        if (textFieldStruct == null)
                            continue;
                        else if (textFieldStruct.fieldName != null && textFieldStruct.fieldName == TextFieldName.totalDistance)
                            continue; //we can skip over displaying these fields for now
                        else if (textFieldStruct.fieldName != null && textFieldStruct.fieldName == TextFieldName.timeToDestination)
                            continue; //we can skip over displaying these fields for now
                        else
                            fullNavigationString = fullNavigationString + String.Format(textFieldStruct.fieldText) + " ";
                    }
                }
                try
                {
                    if (msg.getTurnIcon() != null && msg.getTurnIcon().getValue() != null)
                    {
                        string storedFileName = HttpUtility.getStoredFileName(msg.getTurnIcon().getValue());
                        storedFileName = storedFileName.Replace(@"/", @"\");
                        if (AppInstanceManager.storageFolder != null)
                            logo = AppInstanceManager.storageFolder.Path + @"\" + storedFileName;
                    }
                }
                catch { }

                if (logo == null)
                {
                    logo = "ms-appx:///Assets/icon.png";
                }
                try
                {
                    toastContent = new ToastContent()
                    {
                        Scenario = ToastScenario.Reminder,

                        Visual = new ToastVisual()
                        {
                            BindingGeneric = new ToastBindingGeneric()
                            {
                                Children =
                        {
                        new AdaptiveText()
                        {
                        Text = fullNavigationString
                        },

                        },
                                AppLogoOverride = new ToastGenericAppLogo()
                                {
                                    Source = logo,
                                    HintCrop = ToastGenericAppLogoCrop.Circle
                                }
                            }
                        }


                    };
                }
                catch { }

                var toast = new ToastNotification(toastContent.GetXml());
                toast.ExpirationTime = System.DateTime.Now.AddSeconds(30);// System.DateTime.Now.AddDays(2);
                ToastNotificationManager.CreateToastNotifier().Show(toast);
            });
        }


        public async override void onNavShowConstantTBTRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.ShowConstantTBT msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                if (msg.getNavigationTexts().Count != 0)
                {
                    AppItem appItem = getAppItem(msg.getAppId());

                    if (appItem != null)
                    {
                        await onShowConstantTBTAsync(msg);
                    }
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavStartAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartAudioStream msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavStartStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartStream msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavStopAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopAudioStream msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavStopStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopStream msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavSubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SubscribeWayPoints msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavUnsubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onNavUpdateTurnListRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UpdateTurnList msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onRcGetCapabilitiesRequest(HmiApiLib.Controllers.RC.IncomingRequests.GetCapabilities msg)
        {
            if (appSetting.getRCGetCapabilities())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities();
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onRcGetInteriorVehicleDataConsentRequest(GetInteriorVehicleDataConsent msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onRcGetInteriorVehicleDataRequest(GetInteriorVehicleData msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onRcIsReadyRequest(HmiApiLib.Controllers.RC.IncomingRequests.IsReady msg)
        {
            if (appSetting.getRCIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.RC.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onRcSetInteriorVehicleDataRequest(SetInteriorVehicleData msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onSDLActivateAppResponse(HmiApiLib.Controllers.SDL.IncomingResponses.ActivateApp msg)
        {

        }

        public override void OnSDLAddStatisticsInfoNotification(AddStatisticsInfo msg)
        {
             
        }
        public override void onSDLGetListOfPermissionsResponse(GetListOfPermissions msg)
        {
           permissionList = new List<AllAppsModel>();
            for (int i = 0; i < msg.getAllowedFunctions().Count(); i++)
            {
                permissionList.Add(new AllAppsModel(msg.getAllowedFunctions()[i].getId()) );
                   permissionList[i].name = msg.getAllowedFunctions()[i].getName();
                permissionList[i].allowed = msg.getAllowedFunctions()[i].getAllowed();
                permissionList[i].id = msg.getAllowedFunctions()[i].getId();
            }

            if(policiesCallback != null) policiesCallback.UpdateAppPermissionList();
        }

        public override void onSDLGetStatusUpdateResponse(GetStatusUpdate msg)
        {
             
        }

        public override void onSDLGetURLSResponse(GetURLS msg)
        {
             
        }

        public override void onSDLGetUserFriendlyMessageResponse(GetUserFriendlyMessage msg)
        {
             
        }

        public override void OnSDLOnAppPermissionChangedNotification(OnAppPermissionChanged msg)
        {
             
        }

        public override void OnSDLOnDeviceStateChangedNotification(OnDeviceStateChanged msg)
        {
             
        }

        public override void OnSDLOnSDLConsentNeededNotification(OnSDLConsentNeeded msg)
        {
             
        }

        public override void OnSDLOnStatusUpdateNotification(OnStatusUpdate msg)
        {
             
        }

        public override void OnSDLOnSystemErrorNotification(OnSystemError msg)
        {
             
        }

        public override void onSDLUpdateSDLResponse(UpdateSDL msg)
        {
             
        }

        public override void onTtsChangeRegistrationRequest(HmiApiLib.Controllers.TTS.IncomingRequests.ChangeRegistration msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onTtsGetCapabilitiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetCapabilities msg)
        {
            if (appSetting.getTTSGetCapabilities())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities();
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onTtsGetLanguageRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetLanguage msg)
        {
            if (appSetting.getTTSGetLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage();
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onTtsGetSupportedLanguagesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetSupportedLanguages msg)
        {
            if (appSetting.getTTSGetSupportedLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onTtsIsReadyRequest(HmiApiLib.Controllers.TTS.IncomingRequests.IsReady msg)
        {
            if (appSetting.getTTSIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }
        
        public override void onTtsSetGlobalPropertiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.SetGlobalProperties msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }


        public async void SpeakRequestAsync()
        {
            MediaElement mediaplayer = new MediaElement();
            string textToSpeak;
            using (var speech = new SpeechSynthesizer())
            {
                speech.Voice = SpeechSynthesizer.AllVoices.First(gender => gender.Gender == VoiceGender.Female);
                String fullString = "";
                for (int i = 0; i < speechList.Count; i++)
                {
                    fullString = fullString + speechList[i] + " ";

                }
                textToSpeak = @"<speak version='1.0' " + "xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>" + fullString + "</speak>";

                SpeechSynthesisStream stream = await speech.SynthesizeSsmlToStreamAsync(textToSpeak);
                mediaplayer.SetSource(stream, stream.ContentType);
                mediaplayer.Play();

            }
            speechList.Clear();
        }


        public async void HandleSpeakRequestAsync()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => SpeakRequestAsync());
        }

        public override void onTtsSpeakRequest(HmiApiLib.Controllers.TTS.IncomingRequests.Speak msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.Speak tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.Speak();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.Speak>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.Speak);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));

                if ((msg.getTtsChunkList() != null) && (msg.getTtsChunkList().Count > 0))
                {
                    foreach (TTSChunk item in msg.getTtsChunkList())
                    {
                        if (item.type == SpeechCapabilities.TEXT)
                        {
                            speechList.Add(item.text);
                        }
                    }
                }


                HandleSpeakRequestAsync();
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onTtsStopSpeakingRequest(HmiApiLib.Controllers.TTS.IncomingRequests.StopSpeaking msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiAddCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand>(tmpObj.getMethod());

            int appID = (int)msg.getAppId();

                AppItem appItem = getAppItem(appID);

                if (appItem != null)
                {
                    appItem.addToCommandsList(msg);

                    if (addCommandCallback != null)
                    {
                        addCommandCallback.refreshMenu();
                    }
                    else if (addSubMenuCallback != null)
                    {
                        addSubMenuCallback.refreshSubMenu();
                    }

                    if (null == tmpObj)
                    {
                        Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand);
                        sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                    }
                    else
                    {
                        tmpObj.setId(corrId);
                        sendRpc(tmpObj);
                    }
                }
            callRefreshListeners(typeof(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
        }

        public override void onUiAddSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu msg)
        {
            int corrId = msg.getId();
            int? appID = msg.getAppId();

            HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu>(tmpObj.getMethod());


                AppItem appItem = getAppItem(appID);

                if (appItem != null)
                {
                    appItem.addToCommandsList(msg);

                    if (addCommandCallback != null)
                    {
                        addCommandCallback.refreshMenu();
                    }

                    if (null == tmpObj)
                    {
                        Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu);
                        sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                    }
                    else
                    {
                        tmpObj.setId(corrId);
                        sendRpc(tmpObj);
                    }
                }
        }

        AlertContentdialog alertDialog = null;
        public override async void onUiAlertRequest(HmiApiLib.Controllers.UI.IncomingRequests.Alert msg)
        {
            int corrId = msg.getId();
            int? appId = msg.getAppId();

            sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.ALERT, appId));

            HmiApiLib.Controllers.UI.OutgoingResponses.Alert tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Alert();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Alert>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppItem appItem = getAppItem(msg.getAppId());
                if (appItem != null)
                {
                    try
                    {
                        await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                        {
                            var navigateFrom = new PageNavigate()
                            {
                                alertMsg = msg,
                                Info = appItem.getAppName(),
                            };

                            if (alertDialog != null)
                            {
                                var popups = VisualTreeHelper.GetOpenPopups(Window.Current);
                                foreach (var popup in popups)
                                {
                                    if (popup.Child is AlertContentdialog)
                                    {
                                        Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Alert);
                                        sendRpc(BuildRpc.buildUiAlertResponse(alertDialog.alert.getId(), HmiApiLib.Common.Enums.Result.ABORTED, null));

                                        alertDialog.Hide();
                                        break;
                                    }
                                }
                            }

                            alertDialog = new AlertContentdialog(navigateFrom);
                            await alertDialog.ShowAsync();
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception", ex);
                    }

                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
            sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, appId));
        }

        public override void onUiChangeRegistrationRequest(HmiApiLib.Controllers.UI.IncomingRequests.ChangeRegistration msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiClosePopUpRequest(HmiApiLib.Controllers.UI.IncomingRequests.ClosePopUp msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiDeleteCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand>(tmpObj.getMethod());
            
                AppItem appItem = getAppItem(msg.getAppId());

                if (appItem != null)
                {
                    if (appItem.isCommandPartOfCommandsList(msg.getCmdId())
                        || appItem.isCommandPartOfSubMenu(msg.getCmdId()))
                    {
                        if (null == tmpObj)
                        {
                            Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand);
                            sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                        }
                        else
                        {
                            tmpObj.setId(corrId);
                            sendRpc(tmpObj);
                        }

                        appItem.deleteFromCommandsList(msg);

                        if (addCommandCallback != null)
                        {
                            addCommandCallback.refreshMenu();
                        }
                        else if (addSubMenuCallback != null)
                        {
                            addSubMenuCallback.refreshSubMenu();
                        }
                    } else
                    {
                        sendRpc(BuildRpc.buildUiDeleteCommandResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_ID));
                    }
                }
            callRefreshListeners(typeof(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
        }

        public override void onUiDeleteSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteSubMenu msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu>(tmpObj.getMethod());


            AppItem appItem = getAppItem(msg.getAppId());

                if (appItem != null)
                {
                    if (appItem.isSubMenuAvailable(msg.getMenuID()))
                    {
                        if (null == tmpObj)
                        {
                            Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu);
                            sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                        }
                        else
                        {
                            tmpObj.setId(corrId);
                            sendRpc(tmpObj);
                        }

                        appItem.deleteFromCommandsList(msg);

                        if (addCommandCallback != null)
                            addCommandCallback.refreshMenu();
                        else if (addSubMenuCallback != null)
                            addSubMenuCallback.refreshSubMenu();
                    }
                    else
                    {
                        sendRpc(BuildRpc.buildUiDeleteSubMenuResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_ID));
                    }
                }
        }

        public override void onUiEndAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.EndAudioPassThru msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiGetCapabilitiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetCapabilities msg)
        {
            if (appSetting.getUIGetCapabilities())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities();
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onUiGetLanguageRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetLanguage msg)
        {
            if (appSetting.getUIGetLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage();
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onUiGetSupportedLanguagesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetSupportedLanguages msg)
        {
            if (appSetting.getUIGetSupportedLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages();
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onUiIsReadyRequest(HmiApiLib.Controllers.UI.IncomingRequests.IsReady msg)
        {
            if (appSetting.getUIIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.UI.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }
        public async override void onUiPerformAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformAudioPassThru msg)
        {
            MicManager micManager = new MicManager();

            sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.HMI_OBSCURED, msg.getAppId()));

            try
            {
                if (AppInstanceManager.AppInstance.audioPassThruWithSpeechAPI)
                {
                    AppItem appItem = getAppItem(msg.getAppId());

                    if (appItem != null)
                    {
                        appItem.setPerformAudioPassThru(msg);
                        callRefreshListeners(typeof(PerformAudioPassThru), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
                    }
                }
                else
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        micManager.Record(msg.getId(), msg.getMaxDuration().Value, msg);
                    });
                }
            }
            catch (Exception ex)
            {
            }

            sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, msg.getAppId()));

            return;
        }

        public async override void onUiPerformInteractionRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformInteraction msg)
        {
            int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction();

            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction>(tmpObj.getMethod());

            if (null != msg.getChoiceSet())
            {
                if (null == tmpObj)
                {
                    try
                    {
                        AppItem appItem = getAppItem(msg.getAppId());

                        if (appItem != null)
                        {
                            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                            {
                                if (insideFrame != null)
                                {
                                    insideFrame.Navigate(typeof(Perform_Interaction), msg);
                                }
                                else
                                {
                                    RpcResponse vrPerformInteraction = BuildRpc.buildVrPerformInteractionResponse(msg.getId() - 1, null, HmiApiLib.Common.Enums.Result.DISALLOWED);
                                    AppInstanceManager.AppInstance.sendRpc(vrPerformInteraction);
                                    RpcResponse uiPerformInteraction = BuildRpc.buildUiPerformInteractionResponse(msg.getId(), null, null, HmiApiLib.Common.Enums.Result.DISALLOWED);
                                    AppInstanceManager.AppInstance.sendRpc(uiPerformInteraction);
                                }
                            });
                        }
                        else
                        {
                            sendRpc(BuildRpc.buildUiPerformInteractionResponse(corrId, null, null, HmiApiLib.Common.Enums.Result.DISALLOWED));
                        }
                    }
                    catch { }
                }
                else
                {
                    tmpObj.setId(corrId);
                }
            }
            else
            {
                sendRpc(BuildRpc.buildUiPerformInteractionResponse(corrId, null, null, HmiApiLib.Common.Enums.Result.INVALID_DATA));
            }
        }

        public async override void onUiScrollableMessageRequest(HmiApiLib.Controllers.UI.IncomingRequests.ScrollableMessage msg)
        {
            //need to call our refresh listener before updating the correlation id
            callRefreshListeners(typeof(ScrollableMessage), msg.getAppId(), RefreshDataState.BEFORE_DATA_CHANGE);

            int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppItem appItem = getAppItem(msg.getAppId());

                if (appItem != null)
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        if (insideFrame != null)
                        {
                            insideFrame.Navigate(typeof(ScrollableRPC), msg);
                        }
                        else
                        {
                            sendRpc(BuildRpc.buildUiScrollableMessageResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                        }

                    });
                }
                else
                {
                    sendRpc(BuildRpc.buildUiScrollableMessageResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AppSetting getAppSetting()
        {
            return this.appSetting;
        }
        public async override void onUiSetAppIconRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetAppIcon msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon>(tmpObj.getMethod());

            if (tmpObj == null)
            {
                if (AppInstanceManager.storageFolder != null)
                {
                    AppItem appItem = getAppItem(msg.getAppId());
                    
                    if (appItem == null)
                    {
                        sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.GENERIC_ERROR));
                        return;
                    }

                    appItem.setSetAppIcon(msg);

                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                    {
                        //wait and listen for updates for our main graphic bitmap from putfile
                        Action<OutputGraphic> mainGraphicDelegate = delegate (OutputGraphic outputGraphic)
                        {
                            if (outputGraphic.holder != null)
                            {
                                AppItem myItem = (AppItem)outputGraphic.holder;
                                appItem.appsIcon = outputGraphic.graphic;
                            }
                        };

                        InputGraphic inputGraphic = new InputGraphic();
                        inputGraphic.holder = appItem;
                        inputGraphic.fileName = msg.getAppIcon().getValue();
                        //subscribe for updates for our main graphic bitmap from putfile
                        appItem.graphicManager.addRefreshListener(mainGraphicDelegate, inputGraphic);

                        BitmapImage bitmap = appItem.graphicManager.getGraphic(msg.getAppIcon().getValue());
                        if (bitmap != null)
                            appItem.appsIcon = bitmap;

                    });
                    callRefreshListeners(typeof(SetAppIcon), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);


                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));

                    return;
                }
                else
                {
                    sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.GENERIC_ERROR));
                    return;
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }
     
        public override void onUiSetDisplayLayoutRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetDisplayLayout msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout);
                AppItem appItem = getAppItem(msg.getAppId());

                if (appItem != null)
                {
                    if (appItem != null)
                    {
                        appItem.setDisplayLayout(msg);
                    }

                    callRefreshListeners(typeof(SetDisplayLayout), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);

                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                } else
                {
                    sendRpc(BuildRpc.buildUiSetDisplayLayoutResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED, null, null, null, null));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiSetGlobalPropertiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties);

                AppItem appItem = getAppItem(msg.getAppId());
                if (appItem != null)
                {

                    appItem.setGlobalProperties(msg);
                    callRefreshListeners(typeof(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiSetMediaClockTimerRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetMediaClockTimer msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppItem appItem = getAppItem(msg.getAppId());

                if (appItem != null && (appItem.getAppId() == activatedAppId))
                {
                    if (msg.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.PAUSE)
                    {
                        if (appItem.getMediaClock() == null ||
                            (appItem.getMediaClock() != null && appItem.getMediaClock().getMediaClockTimer() != null && (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.PAUSE)))
                        {
                            sendRpc(BuildRpc.buildUiSetMediaClockTimerResponse(corrId, HmiApiLib.Common.Enums.Result.IGNORED));
                            return;
                        }
                    }
                    else if (msg.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.RESUME)
                    {
                        if (appItem.getMediaClock() == null ||
                            (appItem.getMediaClock() != null && appItem.getMediaClock().getMediaClockTimer() != null && (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.RESUME)))
                        {
                            sendRpc(BuildRpc.buildUiSetMediaClockTimerResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_DATA));
                            return;
                        }
                    }
                    else if (msg.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.COUNTUP || msg.getUpdateMode() == HmiApiLib.Common.Enums.ClockUpdateMode.COUNTDOWN)
                    {
                        if (msg.getStartTime() == null)
                        {
                            sendRpc(BuildRpc.buildUiSetMediaClockTimerResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_DATA));
                            return;
                        }
                    }

                    appItem.setMediaClock(msg);

                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));

                    callRefreshListeners(typeof(SetMediaClockTimer), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
                }
                else
                {
                    sendRpc(BuildRpc.buildUiSetMediaClockTimerResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiShowCustomFormRequest(HmiApiLib.Controllers.UI.IncomingRequests.ShowCustomForm msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm>(tmpObj.getMethod());
            if (null == tmpObj )
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiShowRequest(HmiApiLib.Controllers.UI.IncomingRequests.Show msg)
        {
            int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.Show tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Show();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Show>(tmpObj.getMethod());

            SelectionMode selectionMode = SelectionMode.BASIC_MODE;
            if (appSetting != null)
            {
                selectionMode = appSetting.getSelectedMode();
            }

            if (null == tmpObj || selectionMode == SelectionMode.BASIC_MODE) //always update the HMI display in basic mode
            {
             Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Show);
                try
                {
                    AppItem appItem = getAppItem(msg.getAppId());
                    if (appItem != null)
                    {
                        appItem.setShowRequest(msg);
                        callRefreshListeners(typeof(Show), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
                        sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                    } else
                    {
                        sendRpc(BuildRpc.buildUiShowResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                    }
                } 
                catch (Exception e)
                {
                    if(e.Message.Equals("Disallowed"))
                       sendRpc(BuildRpc.buildUiShowResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED));
                    else
                        sendRpc(BuildRpc.buildUiShowResponse(corrId, HmiApiLib.Common.Enums.Result.DATA_NOT_AVAILABLE));
                }
            }
            else if (AppInstanceManager.storageFolder == null)
                sendRpc(BuildRpc.buildUiShowResponse(corrId, HmiApiLib.Common.Enums.Result.DATA_NOT_AVAILABLE));
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public async override void onUiSliderRequest(HmiApiLib.Controllers.UI.IncomingRequests.Slider msg)
        {
            //need to call our refresh listener before updating the correlation id
            callRefreshListeners(typeof(HmiApiLib.Controllers.UI.IncomingRequests.Slider), msg.getAppId(), RefreshDataState.BEFORE_DATA_CHANGE);

            int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppItem appItem = getAppItem(msg.getAppId());
                if (appItem != null)
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        if (insideFrame != null)
                        {
                            insideFrame.Navigate(typeof(SliderRPCPage), msg);
                        }
                        else
                        {
                            sendRpc(BuildRpc.buildUiSliderResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED, null));
                        }
                    });
                }
                else
                {
                    sendRpc(BuildRpc.buildUiSliderResponse(corrId, HmiApiLib.Common.Enums.Result.DISALLOWED, null));
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVehicleInfoDiagnosticMessageRequest(DiagnosticMessage msg)
        {
            int corrId = msg.getId();   

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVehicleInfoGetDTCsRequest(GetDTCs msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVehicleInfoGetVehicleDataRequest(GetVehicleData msg)
        {
            if (appSetting.getVIGetVehicleData())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVehicleInfoGetVehicleTypeRequest(GetVehicleType msg)
        {
            if (appSetting.getVIGetVehicleType())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVehicleInfoIsReadyRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.IsReady msg)
        {
            if (appSetting.getVIIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.VehicleInfo, true, HmiApiLib.Common.Enums.Result.SUCCESS));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVehicleInfoReadDIDRequest(ReadDID msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVehicleInfoSubscribeVehicleDataRequest(SubscribeVehicleData msg)
        {
            int corrId = msg.getId();
            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();   
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVehicleInfoUnsubscribeVehicleDataRequest(UnsubscribeVehicleData msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVrAddCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.AddCommand msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand>(tmpObj.getMethod());

            if (null == tmpObj)
            {
                try
                {
                    int? cmdId = msg.getCmdId();
                    int? grammerId = msg.getGrammarId();

                    if ((vrGrammerAddCommandDictionary != null) && (grammerId != null)
                        && (cmdId != null) && (grammerId != -1))
                    {
                        if (!vrGrammerAddCommandDictionary.ContainsKey((int)grammerId))
                            vrGrammerAddCommandDictionary.Add((int)grammerId, (int)cmdId);
                    }
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }
        public override void onVrChangeRegistrationRequest(HmiApiLib.Controllers.VR.IncomingRequests.ChangeRegistration msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVrDeleteCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.DeleteCommand msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onVrGetCapabilitiesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetCapabilities msg)
        {
            if (appSetting.getVRGetCapabilities())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVrGetLanguageRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetLanguage msg)
        {
            if (appSetting.getVRGetLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage();
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVrGetSupportedLanguagesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetSupportedLanguages msg)
        {
            if (appSetting.getVRGetSupportedLanguage())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages();
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVrIsReadyRequest(HmiApiLib.Controllers.VR.IncomingRequests.IsReady msg)
        {
            if (appSetting.getVRIsReady())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.VR.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.IsReady();
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.IsReady>(tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
        }

        public override void onVrPerformInteractionRequest(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction msg)
        {
            //need to call our refresh listener before updating the correlation id for the vr portion
            callRefreshListeners(typeof(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction), msg.getAppId(), RefreshDataState.BEFORE_DATA_CHANGE);

            vrPerformInteractionCorrId = msg.getId();
            HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                if (null != msg.getGrammarID())
                {
                    int returnVal = getVrAddCommandId(msg.getGrammarID());

                    if (returnVal != -1)
                    {
                        //sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, returnVal, HmiApiLib.Common.Enums.Result.SUCCESS));
                    }
                    else
                    {
                        //sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, null, HmiApiLib.Common.Enums.Result.GENERIC_ERROR));
                    }
                }
                else
                {
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction);
                    //sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
            }
            else
            {
                tmpObj.setId(vrPerformInteractionCorrId);
                //sendRpc(tmpObj);
            }
        }

        public void onOpen()
        {
            currentState = ConnectionState.CONNECTED;
        }

        public void onClose()
        {
            currentState = ConnectionState.DISCONNECTED;
        }

        public void onError()
        {
            currentState = ConnectionState.DISCONNECTED;
        }

        public void SetUpConnection(String ipAddress, int portNum, InitialConnectionCommandConfig initConfig)
        {
            initConnectionManager(ipAddress, portNum, this, this, this, null);
        }

        public void dispatch(LogMessage message)
        {
            addMessageToUI(message);
        }

        public void handleDispatchingError(string info, Exception ex)
        {
            LogMessage logMessage = new StringLogMessage(info);
            addMessageToUI(logMessage);
        }

        public void handleQueueingError(string info, Exception ex)
        {
            LogMessage logMessage = new StringLogMessage(info);
            addMessageToUI(logMessage);
        }

        public async void addMessageToUI(LogMessage message)
        {
            if (null == _msgAdapter) return;
            try
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                    () =>
                    {
                        Message msg = Message.processLogMessage(message);
                        _msgAdapter.Add(msg);
                        if(SettingsPage.toggleSwitch != null && SettingsPage.toggleSwitch.IsOn == true)
                        writeToLogFile(message);

                    });
            }
            catch { }
            }
        public async void writeToLogFile(LogMessage msg)
        {
            string fname = LogsfileName;

            StorageFile file = null;

            StorageFolder localizationDirectory = Windows.Storage.ApplicationData.Current.LocalFolder;
            try
            {
               file = await localizationDirectory.GetFileAsync(fname);

           }
            catch { }
            try
           {
                doc = new XmlDocument();

                doc.Load(file.Path);
                var node = doc.SelectSingleNode("//LogsXml");
                string color= HexConverter(Color.White);
                if (msg is StringLogMessage) {
                   
                    //Create a new element
                    XmlElement nodeLogMessage = doc.CreateElement("LogMessage");
                    node.AppendChild(nodeLogMessage);
                    XmlElement ele = doc.CreateElement("StringText");
                    ele.InnerText = ((StringLogMessage) msg).getMessage();
                    nodeLogMessage.AppendChild(ele);

                    ele = doc.CreateElement("StringValue");
                    ele.InnerText = ((StringLogMessage) msg).getData();
                    nodeLogMessage.AppendChild(ele);

                    ele = doc.CreateElement("DisplayColor");
                    ele.InnerText = color.ToLower();
                    nodeLogMessage.AppendChild(ele);

                    ele = doc.CreateElement("TimeStamp");
                    ele.InnerText = msg.getDate();
                   nodeLogMessage.AppendChild(ele);
                }
                else if (msg is RpcLogMessage) {

                    RpcMessage func = ((RpcLogMessage)msg).getMessage();
                    string rawJSON = Utils.getSerializedRpcMessage(func);
                    if (func.getRpcMessageType().Equals(RpcMessageType.REQUEST))
                    {
                        color= HexConverter(Color.Cyan);
                    }
                    else if (func.getRpcMessageType().Equals(RpcMessageType.NOTIFICATION))
                    {
                        color = HexConverter(Color.Yellow);
                    }
                    else if (func.getRpcMessageType().Equals(RpcMessageType.RESPONSE))
                    {
                        color = HexConverter(Color.FromArgb(255, 32, 161, 32));
                    }
                    XmlElement nodeLogMessage = doc.CreateElement("LogMessage");
                    node.AppendChild(nodeLogMessage);

                    XmlElement ele = doc.CreateElement("StringText");
                    ele.InnerText = Message.processLogMessage(msg).lblTop;
                    nodeLogMessage.AppendChild(ele);
                    if (func.getRpcMessageType().Equals(RpcMessageType.RESPONSE))
                    {
                        RpcResponse resp = (RpcResponse)func;

                        HmiApiLib.Common.Enums.Result resultCode = resp.getResultCode();
                        if ((resultCode == HmiApiLib.Common.Enums.Result.SUCCESS) ||
                        (resultCode == HmiApiLib.Common.Enums.Result.WARNINGS))
                        {
                            color = HexConverter(Color.Lime);

                        }
                        else
                        {
                            color = HexConverter(Color.Red);

                        }
                       
                    }


                    ele = doc.CreateElement("StringValue");
                   if(rawJSON!=null)
                    ele.InnerText = rawJSON;
                   nodeLogMessage.AppendChild(ele);

                    ele = doc.CreateElement("DisplayColor");
                    ele.InnerText =color.ToLower();
                    nodeLogMessage.AppendChild(ele);

                    ele = doc.CreateElement("TimeStamp");
                    ele.InnerText = msg.getDate();
                    nodeLogMessage.AppendChild(ele);
                    
                }

              
                doc.Save(file.Path);


            }
            catch (Exception e) {
                //No file found
            }

       }
        private static String HexConverter(System.Drawing.Color c)
        {
            return  c.A.ToString("X2")+ c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
        
        public int getCorrectAppId(int? matchValue)
        {
            int appId = -1;

            if (matchValue == null) return appId;

            if (appIdPolicyIdDictionary.ContainsKey(matchValue.Value))
            {
                try
                {
                    appId = int.Parse(appIdPolicyIdDictionary[matchValue.Value]);
                }
                catch (Exception e)
                {

                }
            }

            if(appId == -1)
            {
                if (appIdPolicyIdDictionary.ContainsValue(matchValue.Value.ToString()))
                {
                    appId = appIdPolicyIdDictionary.Where(x => x.Value == matchValue.Value.ToString()).FirstOrDefault().Key;
                }
            }

            return appId;
        }

        public override void onRcOnRCStatusNotification(OnRCStatus msg)
        {

        }

        public override void onNavSetVideoConfigRequest(SetVideoConfig msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public async override void onBcActivateAppRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.ActivateApp msg, HmiApiLib.Common.Enums.Result? result)
        {
            //core has requested that the app be activated
            RpcResponse bcActivateApp = BuildRpc.buildBasicCommunicationActivateAppResponse(msg.getId(), result);
            sendRpc(bcActivateApp);
            try
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    if (insideFrame != null)
                        insideFrame.Navigate(typeof(AppsPage), msg);
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public override void onBcGetSystemTimeRequest(GetSystemTime msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiShowAppMenuRequest(ShowAppMenu msg)
        {
        }

        public override void onUiCloseApplicationRequest(CloseApplication msg)
        {
        }

        public override void onUiRecordStartNotification(OnRecordStart msg)
        {

        }

        public void sendOnButtonNotifications(ButtonName buttonName, List<ButtonEventMode> buttonEventModes, List<ButtonPressMode> buttonPressModes, int? customButtonID, int? appID)
        {
            foreach (ButtonEventMode buttonEventMode in buttonEventModes)
            {
                RequestNotifyMessage buttonEvent = BuildRpc.buildButtonsOnButtonEvent(buttonName, buttonEventMode, customButtonID, appID);
                sendRpc(buttonEvent);
            }

            foreach (ButtonPressMode buttonPressMode in buttonPressModes)
            {
                RequestNotifyMessage rpc = BuildRpc.buildButtonsOnButtonPress(buttonName, buttonPressMode, customButtonID, appID);
                sendRpc(rpc);
            }
        }

        public void onCachedShowUpdated(CachedShow msg)
        {
            callRefreshListeners(typeof(CachedShow), msg.getAppId(), RefreshDataState.AFTER_DATA_CHANGE);
        }
    }
}
