﻿using HmiApiLib;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;
using SharpHmi.src.interfaces;
using SharpHmi.src.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace SharpHmi.src.Manager
{
    public class GraphicManager
    {
        private AppItem appItem = null;
        public GraphicManager(AppItem appItem)
        {
            this.appItem = appItem;
        }

        public struct OutputGraphic
        {
            public BitmapImage graphic;
            public Object holder;
            public AppItem appItem;
        }

        public struct InputGraphic
        {
            public String fileName;
            public Object holder;
        }

        private List<KeyValuePair<InputGraphic, Action<OutputGraphic>>> actionList = new List<KeyValuePair<InputGraphic, Action<OutputGraphic>>>();
        private List<KeyValuePair<String, BitmapImage>> graphicList = new List<KeyValuePair<String, BitmapImage>>();


        public BitmapImage getGraphic(String fileName)
        {
            foreach (KeyValuePair<String, BitmapImage> pair in graphicList.ToList())
            {
                if (fileName == pair.Key)
                {
                    return pair.Value;
                }
            }
            return null;
        }

        public void addGraphicPairToList(KeyValuePair<String, BitmapImage> item)
        {
            foreach (KeyValuePair<String, BitmapImage> pair in graphicList.ToList())
            {
                if (item.Key == pair.Key)
                {
                    graphicList.Remove(pair);
                    break;
                }
            }
            graphicList.Add(item);
        }

        public void addRefreshListener(Action<OutputGraphic> action, InputGraphic inputGraphic)
        {
            foreach (KeyValuePair<InputGraphic, Action<OutputGraphic>> pair in actionList.ToList())
            {
                if (inputGraphic.Equals(pair.Key))
                {
                    actionList.Remove(pair);
                    break;
                }
            }
            actionList.Add(new KeyValuePair<InputGraphic, Action<OutputGraphic>>(inputGraphic, action));
        }

        private void callRefreshListeners(String fileName, BitmapImage bitmapImage)
        {
            foreach (KeyValuePair<InputGraphic, Action<OutputGraphic>> pair in actionList.ToList())
            {
                InputGraphic inputGraphic = pair.Key;

                if (fileName == inputGraphic.fileName)
                {
                    OutputGraphic outputGraphic = new OutputGraphic();
                    outputGraphic.graphic = bitmapImage;
                    outputGraphic.holder = inputGraphic.holder;
                    outputGraphic.appItem = this.appItem;

                    Action<OutputGraphic> myAction = pair.Value;
                    myAction(outputGraphic);
                }
            }
        }

        public async void addGraphic(String fileName)
        {
            if (fileName == null) return;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                BitmapImage bitmapGraphic = await populateImageToBitmap(fileName);
                addGraphicPairToList(new KeyValuePair<String, BitmapImage>(fileName, bitmapGraphic));
                callRefreshListeners(fileName, bitmapGraphic);
            });
        }

        public async void addGraphic(OnAppRegistered msg)
        {
            if (msg == null || msg.getApplication() == null || msg.getApplication().getIcon() == null) return;

            String appIconPath = msg.getApplication().getIcon();
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                BitmapImage bitmapGraphic = await populateImageToBitmap(appIconPath);
                addGraphicPairToList(new KeyValuePair<String, BitmapImage>(appIconPath, bitmapGraphic));
                callRefreshListeners(appIconPath, bitmapGraphic);
            });
        }

        public async void addGraphic(OnPutFile msg)
        {
            if (msg.getSyncFileName() == null || msg.getSyncFileName().Trim() == "") return;


            if (msg.getFileType() == HmiApiLib.Common.Enums.FileType.GRAPHIC_PNG || msg.getFileType() == HmiApiLib.Common.Enums.FileType.GRAPHIC_JPEG || msg.getFileType() == HmiApiLib.Common.Enums.FileType.GRAPHIC_BMP)
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                {
                    BitmapImage bitmapGraphic = await populateImageToBitmap(msg.getSyncFileName());
                    addGraphicPairToList(new KeyValuePair<String, BitmapImage>(msg.getSyncFileName(), bitmapGraphic));
                    callRefreshListeners(msg.getSyncFileName(), bitmapGraphic);
                });
            }
        }

        public async Task<BitmapImage> populateImageToBitmap(String fileNameAndPath)
        {
            if (fileNameAndPath == null || fileNameAndPath.Trim() == "") return null;

            StorageFile file = null;
            String storedFileName = HttpUtility.getStoredFileName(fileNameAndPath);

            if (storedFileName == null) return null;

            storedFileName = storedFileName.Replace(@"/", @"\");
            if (AppInstanceManager.storageFolder != null)
            {
                try
                {
                    if (AppInstanceManager.storageFolder != null)
                    {
                        file = await AppInstanceManager.storageFolder.GetFileAsync(storedFileName);
                    }

                    if (file == null)
                        return null;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            }
            using (var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                try
                {
                    BitmapImage btGraphic = new BitmapImage();
                    btGraphic.SetSource(stream);
                    return btGraphic;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            }
        }
    }
}
