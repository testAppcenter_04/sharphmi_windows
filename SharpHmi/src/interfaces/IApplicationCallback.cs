﻿using HmiApiLib.Controllers.UI.IncomingRequests;
using Windows.UI.Xaml.Media.Imaging;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using HmiApiLib.Controllers.Navigation.IncomingRequests;

namespace SharpHmi.src.interfaces
{
   public interface IApplicationCallback
    {
        void onSelectionButtonClicked(bool hmiButtonClicked);
        void onRefreshRegisteredApps();
        void onRedirectToVideoStreaming();
    }
}
