﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHmi.src.interfaces
{
    public enum RefreshDataState { BEFORE_DATA_CHANGE, AFTER_DATA_CHANGE };

    interface IRefreshListener
    {
        void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState refreshDataState);
    }
}
