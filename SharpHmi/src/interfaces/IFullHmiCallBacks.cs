﻿using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.interfaces;
using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace SharpHmi.src.MainScreen
{
    interface IFullHmiCallBacks : IMediaLayout
    {
        void onUiShowRequestCallback();
        void onButtonSubscriptionNotificationCallback();
        void onUiSetGlobalPropertiesCallback(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties msg);
        void onUiPerformAudioPassThruRequestCallback(PerformAudioPassThru msg);
        void onUIScrollableMessageCallbackAsync(ScrollableMessage msg);
        void onUiAlertRequestCallbackAsync(Alert msg);
        void onUiPerformInteractionRequestCallback(PerformInteraction msg);
        void onTtsSpeakRequestCallback(Speak msg);
        void onUiSliderCallback(Slider msg);
        void onShowConstantTBT(ShowConstantTBT msg);
        void onUiSetAppIcon(BitmapImage bitmap);
        void onRefresh(SetDisplayLayout setDisplayLayout);
        void onNavSendLocationCallback(SendLocation sendLocation);
        Task<StorageFile> setImageAsync(String storedFilePath, Type type, bool? isSecondaryGraphic);
    }
}
