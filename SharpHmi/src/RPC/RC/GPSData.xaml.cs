﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class GPSData : UserControl
    {
        public CheckBox longitudeDegreesCheckBox;
        public TextBox  longitudeDegreesTextBox;
        public CheckBox latitudeDegreesCheckBox;
        public TextBox  latitudeDegreesTextBox;
        public CheckBox altitudeMetersIdCheckBox;
        public TextBox  altitudeMetersIdTextBox;
        public GPSData(HmiApiLib.Common.Structs.GPSData gpsData)
        {
            this.InitializeComponent();
            if (gpsData != null) {
                if (gpsData.getLatitudeDegrees() != null)
                {
                    LatitudeDegreesTextBox.Text = gpsData.getLatitudeDegrees().ToString();

                }
                else {
                    LatitudeDegreesCheckBox.IsChecked = false;
                    LatitudeDegreesTextBox.IsEnabled = false;
                }
                if (gpsData.getLongitudeDegrees() != null)
                {
                    LongitudeDegreesTextBox.Text = gpsData.getLongitudeDegrees().ToString();

                }
                else
                {
                    LongitudeDegreesCheckBox.IsChecked = false;
                    LongitudeDegreesTextBox.IsEnabled = false;

                }
                if (gpsData.getAltitude() != null)
                {
                    AltitudeTextBox.Text = gpsData.getAltitude().ToString();

                }
                else
                {
                    AltitudeCheckBox.IsChecked = false;
                    AltitudeTextBox.IsEnabled = false;
                }

            }
            longitudeDegreesCheckBox = LongitudeDegreesCheckBox;
            longitudeDegreesTextBox=LongitudeDegreesTextBox;
            latitudeDegreesCheckBox = LatitudeDegreesCheckBox;
            latitudeDegreesTextBox = LatitudeDegreesTextBox;
            altitudeMetersIdCheckBox = AltitudeCheckBox;
            altitudeMetersIdTextBox = AltitudeTextBox;
        }

        private void LongitudeDegreesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LongitudeDegreesTextBox.IsEnabled = (bool)LongitudeDegreesCheckBox.IsChecked;

        }

        private void LatitudeDegreesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LatitudeDegreesTextBox.IsEnabled = (bool)LatitudeDegreesCheckBox.IsChecked;

        }

        private void AltitudeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AltitudeTextBox.IsEnabled = (bool)AltitudeCheckBox.IsChecked;

        }
    }
}
