﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Buttons.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Buttons
{
    public sealed partial class ButtonsButtonPressResponse : UserControl
    {
        ButtonPress tmpObj = null;
        public ButtonsButtonPressResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowButtonPress()
        {
            ContentDialog buttonsButtonPressCD = new ContentDialog();
            buttonsButtonPressCD.Content = this;

            buttonsButtonPressCD.PrimaryButtonText = Const.TxLater;
            buttonsButtonPressCD.PrimaryButtonClick += ButtonsButtonPressCD_PrimaryButtonClick;

            buttonsButtonPressCD.SecondaryButtonText = Const.Reset;
            buttonsButtonPressCD.SecondaryButtonClick += ButtonsButtonPressCD_ResetButtonClick;

            buttonsButtonPressCD.CloseButtonText = Const.Close;
            buttonsButtonPressCD.CloseButtonClick += ButtonsButtonPressCD_CloseButtonClick;

            tmpObj = new ButtonPress();
            tmpObj = (ButtonPress)AppUtils.getSavedPreferenceValueForRpc<ButtonPress>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ButtonPress);
                tmpObj = (ButtonPress)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await buttonsButtonPressCD.ShowAsync();
        }

        private void ButtonsButtonPressCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildButtonsButtonPressResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ButtonsButtonPressCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ButtonsButtonPressCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
