﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Buttons.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Buttons
{
    public sealed partial class ButtonsOnButtonEvent : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        OnButtonEvent tmpObj = null;
        public ButtonsOnButtonEvent()
        {
            InitializeComponent();
            ButtonNameComboBox.ItemsSource = Enum.GetNames(typeof(ButtonName));
            ButtonNameComboBox.SelectedIndex = 0;

            ButtonEventModeComboBox.ItemsSource = Enum.GetNames(typeof(ButtonEventMode));
            ButtonEventModeComboBox.SelectedIndex = 0;

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnButtonEvent()
        {
            ContentDialog buttonsOnButtonEventCD = new ContentDialog();
            buttonsOnButtonEventCD.Content = this;

            buttonsOnButtonEventCD.PrimaryButtonText = Const.TxNow;
            buttonsOnButtonEventCD.PrimaryButtonClick += ButtonsOnButtonEventCD_PrimaryButtonClick;

            buttonsOnButtonEventCD.SecondaryButtonText = Const.Reset;
            buttonsOnButtonEventCD.SecondaryButtonClick += ButtonsOnButtonEventCD_ResetButtonClick;

            buttonsOnButtonEventCD.CloseButtonText = Const.Close;
            buttonsOnButtonEventCD.CloseButtonClick += ButtonsOnButtonEventCD_CloseButtonClick;

            tmpObj = new OnButtonEvent();
            tmpObj = (OnButtonEvent)AppUtils.getSavedPreferenceValueForRpc<OnButtonEvent>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnButtonEvent);
                tmpObj = (OnButtonEvent)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getName())
                    ButtonNameComboBox.SelectedIndex = (int)tmpObj.getName();
                else
                {
                    ButtonNameCheckBox.IsChecked = false;
                    ButtonNameComboBox.IsEnabled = false;
                }

                if (null != tmpObj.getMode())
                    ButtonEventModeComboBox.SelectedIndex = (int)tmpObj.getMode();
                else {
                    ButtonEventModeComboBox.IsEnabled = false;
                    ButtonEventModeCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getCustomButtonID())
                    CustomButtonIdTextBox.Text = tmpObj.getCustomButtonID().ToString();
                else
                {
                    CustomButtonIdCheckBox.IsChecked = false;
                    CustomButtonIdTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdTextBox.IsEnabled = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }

            await buttonsOnButtonEventCD.ShowAsync();
        }

        private void ButtonsOnButtonEventCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }

            ButtonName? buttonName = null;
            if (ButtonNameCheckBox.IsChecked == true)
                buttonName = (ButtonName)ButtonNameComboBox.SelectedIndex;

            ButtonEventMode? mode = null;
            if (ButtonEventModeCheckBox.IsChecked == true)
                mode = (ButtonEventMode)ButtonEventModeComboBox.SelectedIndex;

            int? customButtonId = null;
            if (CustomButtonIdCheckBox.IsChecked == true)
            {
                try
                {
                    customButtonId = Int32.Parse(CustomButtonIdTextBox.Text.ToString());
                }
                catch (Exception e)
                {

                }
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildButtonsOnButtonEvent(buttonName, mode, null, selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void ButtonsOnButtonEventCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ButtonsOnButtonEventCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ButtonNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonNameComboBox.IsEnabled = (bool)ButtonNameCheckBox.IsChecked;
        }

        private void ButtonEventModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonEventModeComboBox.IsEnabled = (bool)ButtonEventModeCheckBox.IsChecked;
        }

        private void CustomButtonIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CustomButtonIdTextBox.IsEnabled = (bool)CustomButtonIdCheckBox.IsChecked;
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;                
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }
        
        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true )
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if(RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }                    
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }
    }
}