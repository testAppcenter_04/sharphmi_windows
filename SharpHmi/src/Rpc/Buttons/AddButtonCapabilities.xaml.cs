﻿using HmiApiLib;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Buttons
{
    public sealed partial class AddButtonCapabilities : UserControl
    {
        public ComboBox buttonNameComboBox;
        public CheckBox buttonNameCheckBox;

        public CheckBox shortPressAvailableCheckBox;
        public ToggleSwitch shortPressAvailableTgl;

        public CheckBox longPressAvailableCheckBox;
        public ToggleSwitch longPressAvailableTgl;

        public CheckBox upDownAvailableCheckBox;
        public ToggleSwitch upDownAvailableTgl;

        public AddButtonCapabilities()
        {
            InitializeComponent();
            ButtonNameComboBox.ItemsSource = Enum.GetNames(typeof(ButtonName));
            ButtonNameComboBox.SelectedIndex = 0;

            buttonNameComboBox = ButtonNameComboBox;
            buttonNameCheckBox = ButtonNameCheckBox;

            shortPressAvailableCheckBox = ShortPressAvailableCheckBox;
            longPressAvailableCheckBox = LongPressAvailableCheckBox;
            upDownAvailableCheckBox = UpDownAvailableCheckBox;

            shortPressAvailableTgl = ShortPressAvailableTgl;
            longPressAvailableTgl = LongPressAvailableTgl;
            upDownAvailableTgl = UpDownAvailableTgl;
        }

        private void ButtonNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonNameComboBox.IsEnabled = (bool)ButtonNameCheckBox.IsChecked;
        }

        private void LongPressAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LongPressAvailableTgl.IsEnabled = (bool)LongPressAvailableCheckBox.IsChecked;

        }

        private void ShortPressAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ShortPressAvailableTgl.IsEnabled = (bool)ShortPressAvailableCheckBox.IsChecked;

        }

        private void UpDownAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UpDownAvailableTgl.IsEnabled = (bool)UpDownAvailableCheckBox.IsChecked;

        }
    }
}
