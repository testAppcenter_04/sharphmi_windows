﻿using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.Buttons.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Buttons
{
    public sealed partial class ButtonsOnButtonPress : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        OnButtonPress tmpObj = null;
        public ButtonsOnButtonPress()
        {
            InitializeComponent();
            ButtonNameComboBox.ItemsSource = Enum.GetNames(typeof(ButtonName));
            ButtonNameComboBox.SelectedIndex = 0;

            ButtonPressModeComboBox.ItemsSource = Enum.GetNames(typeof(ButtonPressMode));
            ButtonPressModeComboBox.SelectedIndex = 0;

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnButtonPress()
        {
            ContentDialog buttonsOnButtonPressCD = new ContentDialog();
            buttonsOnButtonPressCD.Content = this;

            buttonsOnButtonPressCD.PrimaryButtonText = Const.TxNow;
            buttonsOnButtonPressCD.PrimaryButtonClick += ButtonsOnButtonPressCD_PrimaryButtonClick;

            buttonsOnButtonPressCD.SecondaryButtonText = Const.Reset;
            buttonsOnButtonPressCD.SecondaryButtonClick += ButtonsOnButtonPressCD_ResetButtonClick;

            buttonsOnButtonPressCD.CloseButtonText = Const.Close;
            buttonsOnButtonPressCD.CloseButtonClick += ButtonsOnButtonPressCD_CloseButtonClick;

            tmpObj = new OnButtonPress();
            tmpObj = (OnButtonPress)AppUtils.getSavedPreferenceValueForRpc<OnButtonPress>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnButtonPress);
                tmpObj = (OnButtonPress)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getName())
                    ButtonNameComboBox.SelectedIndex = (int)tmpObj.getName();
                else
                {
                    ButtonNameComboBox.IsEnabled = false;
                    ButtonNameCheckBox.IsChecked = false;

                }
                if (null != tmpObj.getMode())
                    ButtonPressModeComboBox.SelectedIndex = (int)tmpObj.getMode();
                else
                {
                    ButtonPresstModeCheckBox.IsChecked = false;
                    ButtonPressModeComboBox.IsEnabled = false;
                }

                if (null != tmpObj.getCustomButtonID())
                    CustomButtonIdTextBox.Text = tmpObj.getCustomButtonID().ToString();
                else {
                    CustomButtonIdCheckBox.IsChecked = false;
                    CustomButtonIdTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else
                {
                    AppIdCheckBox.IsChecked = false;

                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }

            }

            await buttonsOnButtonPressCD.ShowAsync();
        }

        private void ButtonsOnButtonPressCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }

            ButtonName? buttonName = null;
            if (ButtonNameCheckBox.IsChecked == true)
                buttonName = (ButtonName)ButtonNameComboBox.SelectedIndex;

            ButtonPressMode? mode = null;
            if (ButtonPresstModeCheckBox.IsChecked == true)
                mode = (ButtonPressMode)ButtonPressModeComboBox.SelectedIndex;

            int? customButtonId = null;
            if (CustomButtonIdCheckBox.IsChecked == true)
            {
                try
                {
                    customButtonId = Int32.Parse(CustomButtonIdTextBox.Text.ToString());
                }
                catch (Exception e)
                {

                }
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildButtonsOnButtonPress(buttonName, mode, customButtonId, selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void ButtonsOnButtonPressCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ButtonsOnButtonPressCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ButtonNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonNameComboBox.IsEnabled = (bool)ButtonNameCheckBox.IsChecked;
        }

        private void ButtonPressModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonPressModeComboBox.IsEnabled = (bool)ButtonPresstModeCheckBox.IsChecked;
        }

        private void CustomButtonIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CustomButtonIdTextBox.IsEnabled = (bool)CustomButtonIdCheckBox.IsChecked;
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }
    }
}
