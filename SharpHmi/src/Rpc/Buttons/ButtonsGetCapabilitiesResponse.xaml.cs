﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.Buttons.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Buttons
{
    public sealed partial class ButtonsGetCapabilitiesResponse : UserControl
    {
        GetCapabilities tmpObj = null;
        ObservableCollection<ButtonCapabilities> btnCap = new ObservableCollection<ButtonCapabilities>();
        ContentDialog buttonsGetCapabilitiesCD = new ContentDialog();
        
        public ButtonsGetCapabilitiesResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetCapabilities()
        {
            buttonsGetCapabilitiesCD.Content = this;

            buttonsGetCapabilitiesCD.PrimaryButtonText = Const.TxLater;
            buttonsGetCapabilitiesCD.PrimaryButtonClick += ButtonsGetCapabilitiesCD_PrimaryButtonClick;

            buttonsGetCapabilitiesCD.SecondaryButtonText = Const.Reset;
            buttonsGetCapabilitiesCD.SecondaryButtonClick += ButtonsGetCapabilitiesCD_ResetButtonClick;

            buttonsGetCapabilitiesCD.CloseButtonText = Const.Close;
            buttonsGetCapabilitiesCD.CloseButtonClick += ButtonsGetCapabilitiesCD_CloseButtonClick;

            ButtonGetCapabilitiesListView.ItemsSource = btnCap;

            tmpObj = new GetCapabilities();
            tmpObj = (GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<GetCapabilities>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetCapabilities);
                tmpObj = (GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable()== null)
                {
                    OnScreenAvailableCheckBox.IsChecked = false;
                    OnScreenAvailableTgl.IsEnabled = false;
                }
                else
                {
                    OnScreenAvailableCheckBox.IsChecked = true;
                    OnScreenAvailableTgl.IsOn = (bool)tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable();
                }

                if (tmpObj.getButtonCapabilities() != null)
                {
                    foreach (ButtonCapabilities tmp in tmpObj.getButtonCapabilities())
                    {
                        btnCap.Add(tmp);
                    }
                }
                else
                {
                    AddButtonCapabilitiesButton.IsEnabled = false;
                    AddButtonCapabilitiesCheckBox.IsChecked = false;
                }

            }

            await buttonsGetCapabilitiesCD.ShowAsync();
        }
        
        private void ButtonsGetCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            List<ButtonCapabilities> capabilities = null;
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            if (AddButtonCapabilitiesCheckBox.IsChecked == true)
            {
                capabilities = new List<ButtonCapabilities>();
                capabilities.AddRange(btnCap);
            }
            PresetBankCapabilities presetCap = new PresetBankCapabilities();

            if (OnScreenAvailableCheckBox.IsChecked == true)
            {
                presetCap.onScreenPresetsAvailable = OnScreenAvailableTgl.IsOn;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildButtonsGetCapabilitiesResponse(
                BuildRpc.getNextId(), capabilities, presetCap, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ButtonsGetCapabilitiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ButtonsGetCapabilitiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private async void AddButtonCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            buttonsGetCapabilitiesCD.Hide();
            ContentDialog addButtonCapabilitiesCD = new ContentDialog();
            var addButtonCapabilities = new AddButtonCapabilities();                        
            addButtonCapabilitiesCD.Content = addButtonCapabilities;

            addButtonCapabilitiesCD.PrimaryButtonText = Const.OK;
            addButtonCapabilitiesCD.PrimaryButtonClick += AddButtonCapabilitiesCD_PrimaryButtonClick;

            addButtonCapabilitiesCD.SecondaryButtonText = Const.Close;
            addButtonCapabilitiesCD.SecondaryButtonClick += AddButtonCapabilitiesCD_SecondaryButtonClick;

            await addButtonCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addButtonCapabilities = sender.Content as AddButtonCapabilities;
            
            ButtonCapabilities buttonCapabilities = new ButtonCapabilities();
            if (addButtonCapabilities.buttonNameCheckBox.IsChecked == true)
                buttonCapabilities.name = (HmiApiLib.ButtonName) addButtonCapabilities.buttonNameComboBox.SelectedIndex;
            if (addButtonCapabilities.shortPressAvailableCheckBox.IsChecked == true)
                buttonCapabilities.shortPressAvailable = addButtonCapabilities.shortPressAvailableTgl.IsOn;
            if (addButtonCapabilities.longPressAvailableCheckBox.IsChecked == true)
                buttonCapabilities.longPressAvailable = addButtonCapabilities.longPressAvailableTgl.IsOn;
            if (addButtonCapabilities.upDownAvailableCheckBox.IsChecked == true)
                buttonCapabilities.upDownAvailable = addButtonCapabilities.upDownAvailableTgl.IsOn;

            btnCap.Add(buttonCapabilities);
            sender.Hide();
            await buttonsGetCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await buttonsGetCapabilitiesCD.ShowAsync();
        }              
       
        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AddButtonCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddButtonCapabilitiesButton.IsEnabled = (bool)AddButtonCapabilitiesCheckBox.IsChecked;
        }

        private void OnScreenAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OnScreenAvailableTgl.IsEnabled = (bool)OnScreenAvailableCheckBox.IsChecked;

        }
    }
}