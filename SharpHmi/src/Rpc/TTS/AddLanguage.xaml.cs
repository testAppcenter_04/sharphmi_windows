﻿using HmiApiLib.Common.Enums;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class AddLanguage : UserControl
    {
        private static String[] AddLanguageArray = Enum.GetNames(typeof(Language));
        public ObservableCollection<Language> innerLanguageList = new ObservableCollection<Language>();

        public AddLanguage()
        {
            InitializeComponent();
            AddLanguageListBox.ItemsSource = AddLanguageArray;
        }

        private void AddLanguageCheckboxSelected(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            Language language = (Language)HmiApiLib.Utils.getEnumValue(typeof(Language), (sender as CheckBox).Content.ToString());
            if (((CheckBox)sender).IsChecked == true)
                innerLanguageList.Add(language);
            else
            {
                if (innerLanguageList.Contains(language))
                    innerLanguageList.Remove(language);
            }
        }
    }
}
