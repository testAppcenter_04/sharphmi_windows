﻿using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSStopped : UserControl
    {
        public TTSStopped()
        {
            InitializeComponent();
        }

        public async void ShowStopped()
        {
            ContentDialog ttsStoppedCD = new ContentDialog();
            ttsStoppedCD.Content = this;

            ttsStoppedCD.PrimaryButtonText = Const.TxNow;
            ttsStoppedCD.PrimaryButtonClick += TtsStoppedCD_PrimaryButtonClick;

            ttsStoppedCD.SecondaryButtonText = Const.Reset;
            ttsStoppedCD.SecondaryButtonClick += TtsStoppedCD_ResetButtonClick;

            ttsStoppedCD.CloseButtonText = Const.Close;
            ttsStoppedCD.CloseButtonClick += TtsStoppedCD_CloseButtonClick;

            await ttsStoppedCD.ShowAsync();
        }

        private void TtsStoppedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Base.RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStoppedNotification();
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
            AppInstanceManager.AppInstance.sendRpc(rpcMessage);
        }

        private void TtsStoppedCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void TtsStoppedCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
