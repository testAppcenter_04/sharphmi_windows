﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSGetLanguageResponse : UserControl
    {
        GetLanguage tmpObj = null;
        public TTSGetLanguageResponse()
        {
            InitializeComponent();

            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetLanguage()
        {
            ContentDialog ttsGetLanguageCD = new ContentDialog();
            ttsGetLanguageCD.Content = this;

            ttsGetLanguageCD.PrimaryButtonText = Const.TxLater;
            ttsGetLanguageCD.PrimaryButtonClick += TtsGetLanguageCD_PrimaryButtonClick;

            ttsGetLanguageCD.SecondaryButtonText = Const.Reset;
            ttsGetLanguageCD.SecondaryButtonClick += TtsGetLanguageCD_ResetButtonClick;

            ttsGetLanguageCD.CloseButtonText = Const.Close;
            ttsGetLanguageCD.CloseButtonClick += TTSGetLanguageCD_CloseButtonClick;

            tmpObj = new GetLanguage();
            tmpObj = (GetLanguage)AppUtils.getSavedPreferenceValueForRpc<GetLanguage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetLanguage);
                tmpObj = (GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getLanguage() != null)
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else {
                    LanguageCheckBox.IsChecked = false;
                }


            }

            await ttsGetLanguageCD.ShowAsync();
        }

        private void TtsGetLanguageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            Language? lang = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                lang = (Language)LanguageComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTtsGetLanguageResponse(
                BuildRpc.getNextId(), lang, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsGetLanguageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TTSGetLanguageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
