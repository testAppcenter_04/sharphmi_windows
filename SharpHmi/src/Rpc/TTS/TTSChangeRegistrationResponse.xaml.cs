﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSChangeRegistrationResponse : UserControl
    {
        ChangeRegistration tmpObj = null;
        public TTSChangeRegistrationResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowChangeRegistration()
        {
            ContentDialog ttsChangeRegistrationCD = new ContentDialog();
            ttsChangeRegistrationCD.Content = this;

            ttsChangeRegistrationCD.PrimaryButtonText = Const.TxLater;
            ttsChangeRegistrationCD.PrimaryButtonClick += TtsChangeRegistrationCD_PrimaryButtonClick;

            ttsChangeRegistrationCD.SecondaryButtonText = Const.Reset;
            ttsChangeRegistrationCD.SecondaryButtonClick += TtsChangeRegistrationCD_ResetButtonClick;

            ttsChangeRegistrationCD.CloseButtonText = Const.Close;
            ttsChangeRegistrationCD.CloseButtonClick += TtsChangeRegistrationCD_CloseButtonClick;

            tmpObj = new ChangeRegistration();
            tmpObj = (ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ChangeRegistration);
                tmpObj = (ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await ttsChangeRegistrationCD.ShowAsync();
        }

        private void TtsChangeRegistrationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTTSChangeRegistrationResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsChangeRegistrationCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsChangeRegistrationCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
