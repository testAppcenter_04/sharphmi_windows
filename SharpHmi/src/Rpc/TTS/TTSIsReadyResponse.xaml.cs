﻿using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using HmiApiLib.Builder;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;
        public TTSIsReadyResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            ContentDialog ttsIsReadyCD = new ContentDialog();
            ttsIsReadyCD.Content = this;

            ttsIsReadyCD.PrimaryButtonText = Const.TxLater;
            ttsIsReadyCD.PrimaryButtonClick += TtsIsReadyCD_PrimaryButtonClick;

            ttsIsReadyCD.SecondaryButtonText = Const.Reset;
            ttsIsReadyCD.SecondaryButtonClick += TtsIsReadyCD_ResetButtonClick;

            ttsIsReadyCD.CloseButtonText = Const.Close;
            ttsIsReadyCD.CloseButtonClick += TtsIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(IsReady);
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                AvailableCheckBox.IsChecked = tmpObj.getAvailable();
                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableTgl.IsEnabled = false;
                }
                else
                {
                    AvailableCheckBox.IsChecked = true;
                    AvailableTgl.IsOn = (bool)tmpObj.getAvailable();
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await ttsIsReadyCD.ShowAsync();
        }

        private void TtsIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableTgl.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(
                BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.TTS, toggle, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableTgl.IsEnabled = (bool)AvailableCheckBox.IsChecked;

        }
    }
}
