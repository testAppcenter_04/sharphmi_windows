﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using HmiApiLib.Common.Enums;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class SpeechCapabilities : UserControl
    {
        private static String[] SpeechCapabilitiesArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.SpeechCapabilities));
        public ObservableCollection<HmiApiLib.Common.Enums.SpeechCapabilities> speechCapabilitiesCheckedList = new ObservableCollection<HmiApiLib.Common.Enums.SpeechCapabilities>();
        private List<HmiApiLib.Common.Enums.SpeechCapabilities> speechList;

        public SpeechCapabilities(ObservableCollection<HmiApiLib.Common.Enums.SpeechCapabilities> speechList)
        {
            InitializeComponent();

            SpeechCapabilitiesListBox.ItemsSource = SpeechCapabilitiesArray;
            CheckBox_checked.ToString();
            //SpeechCapabilitiesArray.GetEnumerator();
            //if (speechCapabilitiesCheckedList!=null)

            while (speechList.GetEnumerator().MoveNext()) {


            }

        }

        public SpeechCapabilities()
        {
            this.speechList = speechList;
        }

        private void SpeechCapabilitiesCheckBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.SpeechCapabilities speechCapabilities = (HmiApiLib.Common.Enums.SpeechCapabilities)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.SpeechCapabilities), (sender as CheckBox).Content.ToString());
            //for (int i = 0; i < sizeof(HmiApiLib.Common.Enums.SpeechCapabilities); i++)
            //{
            //        speechCapabilitiesCheckedList.Insert(i, speechCapabilities);

            //speechCapabilitiesCheckedList.Add(speechCapabilities);

            if (((CheckBox)sender).IsChecked == true)
            {
                speechCapabilitiesCheckedList.Insert(0,speechCapabilities);
                //speechCapabilitiesCheckedList.Add(speechCapabilities);

            }

            else
            {
                if (speechCapabilitiesCheckedList.Contains(speechCapabilities))
                    speechCapabilitiesCheckedList.Remove(speechCapabilities);
            }
            //}
            //Console.WriteLine("index", speechCapabilitiesCheckedList);
        }

        private void SpeechCapCBCheckedChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

            if (speechCapabilitiesCheckedList.Contains((HmiApiLib.Common.Enums.SpeechCapabilities)(Array.IndexOf(SpeechCapabilitiesArray, (sender as CheckBox).Content))))
            {
                (sender as CheckBox).IsChecked = true;
            }
            else
            {
                (sender as CheckBox).IsChecked = false;
            }
        }

      /* private  void CheckBox_checked(object sender, RoutedEventArgs e)
        {
            CheckBox chkbox = (CheckBox)sender;
            chkbox.Content.ToString();
        }*/
        bool m_check;
        public bool CheckBox_checked
        {
            get
            {
                return m_check;
            }
            set
            {
                m_check = true;
            }
        }
    }
}
