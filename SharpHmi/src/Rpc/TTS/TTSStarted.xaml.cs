﻿using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSStarted : UserControl
    {
        public TTSStarted()
        {
            InitializeComponent();
        }

        public async void ShowStarted()
        {
            ContentDialog ttsStartedCD = new ContentDialog();
            ttsStartedCD.Content = this;

            ttsStartedCD.PrimaryButtonText = Const.TxNow;
            ttsStartedCD.PrimaryButtonClick += TtsStartedCD_PrimaryButtonClick;

            ttsStartedCD.SecondaryButtonText = Const.Reset;
            ttsStartedCD.SecondaryButtonClick += TtsStartedCD_ResetButtonClick;

            ttsStartedCD.CloseButtonText = Const.Close;
            ttsStartedCD.CloseButtonClick += TtsStartedCD_CloseButtonClick;

            await ttsStartedCD.ShowAsync();
        }

        private void TtsStartedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Base.RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStartedNotification();
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
            AppInstanceManager.AppInstance.sendRpc(rpcMessage);
        }

        private void TtsStartedCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void TtsStartedCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
