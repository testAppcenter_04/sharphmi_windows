﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSGetSupportedLanguagesResponse : UserControl
    {
        static ContentDialog ttsGetSupportedLanguagesCD = new ContentDialog();
        ObservableCollection<Language> languageList = new ObservableCollection<Language>();

        GetSupportedLanguages tmpObj = null;

        public TTSGetSupportedLanguagesResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetSupportedLanguages()
        {
            var ttsGetSupportedLanguages = new TTSGetSupportedLanguagesResponse();
            ttsGetSupportedLanguagesCD.Content = this;

            ttsGetSupportedLanguagesCD.PrimaryButtonText = Const.TxLater;
            ttsGetSupportedLanguagesCD.PrimaryButtonClick += TtsGetSupportedLanguagesCD_PrimaryButtonClick;

            ttsGetSupportedLanguagesCD.SecondaryButtonText = Const.Reset;
            ttsGetSupportedLanguagesCD.SecondaryButtonClick += TtsGetSupportedLanguagesCD_ResetButtonClick;

            ttsGetSupportedLanguagesCD.CloseButtonText = Const.Close;
            ttsGetSupportedLanguagesCD.CloseButtonClick += TtsGetSupportedLanguagesCD_CloseButtonClick;

            TTSGetSupportedLanguageList.ItemsSource = languageList;

            tmpObj = new GetSupportedLanguages();
            tmpObj = (GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<GetSupportedLanguages>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetSupportedLanguages);
                tmpObj = (GetSupportedLanguages)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();

                if (tmpObj.getLanguages() != null)
                {
                    List<Language> langList = tmpObj.getLanguages();
                    foreach (Language item in langList)
                    {
                        languageList.Add(item);
                    }
                }
                else {
                    AddLanguageCheckBox.IsChecked = false;
                    AddLanguageButton.IsEnabled = false;
                }
            }

            await ttsGetSupportedLanguagesCD.ShowAsync();
        }

        private void TtsGetSupportedLanguagesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            List<Language> lngList = null;
            if (AddLanguageCheckBox.IsChecked == true)
            {
                lngList = new List<Language>();
                lngList.AddRange(languageList);
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTTSGetSupportedLanguagesResponse(
                BuildRpc.getNextId(), resultCode, lngList);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsGetSupportedLanguagesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsGetSupportedLanguagesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }


        private void AddLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddLanguageButton.IsEnabled = (bool)AddLanguageCheckBox.IsChecked;
        }

        private async void AddLanguageTapped(object sender, TappedRoutedEventArgs e)
        {
            ttsGetSupportedLanguagesCD.Hide();

            ContentDialog ttsAddLanguageCD = new ContentDialog();

            var ttsAddLanguage = new AddLanguage();
            ttsAddLanguageCD.Content = ttsAddLanguage;

            ttsAddLanguageCD.PrimaryButtonText = Const.OK;
            ttsAddLanguageCD.PrimaryButtonClick += TtsAddLanguageCD_PrimaryButtonClick;

            ttsAddLanguageCD.SecondaryButtonText = Const.Close;
            ttsAddLanguageCD.SecondaryButtonClick += TtsAddLanguageCD_SecondaryButtonClick;

            await ttsAddLanguageCD.ShowAsync();
        }

        private async void TtsAddLanguageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLanguage = sender.Content as AddLanguage;
            foreach (Language lang in addLanguage.innerLanguageList)
            {
                languageList.Add(lang);
            }

            sender.Hide();
            await ttsGetSupportedLanguagesCD.ShowAsync();
        }

        private async void TtsAddLanguageCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await ttsGetSupportedLanguagesCD.ShowAsync();
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
