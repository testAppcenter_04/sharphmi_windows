﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSStopSpeakingResponse : UserControl
    {
        StopSpeaking tmpObj = null;
        public TTSStopSpeakingResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowStopSpeaking()
        {
            ContentDialog ttsStopSpeakingCD = new ContentDialog();
            ttsStopSpeakingCD.Content = this;

            ttsStopSpeakingCD.PrimaryButtonText = Const.TxLater;
            ttsStopSpeakingCD.PrimaryButtonClick += TtsStopSpeakingCD_PrimaryButtonClick;

            ttsStopSpeakingCD.SecondaryButtonText = Const.Reset;
            ttsStopSpeakingCD.SecondaryButtonClick += TtsStopSpeakingCD_ResetButtonClick;

            ttsStopSpeakingCD.CloseButtonText = Const.Close;
            ttsStopSpeakingCD.CloseButtonClick += TtsStopSpeakingCD_CloseButtonClick;

            tmpObj = new StopSpeaking();
            tmpObj = (StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<StopSpeaking>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(StopSpeaking);
                tmpObj = (StopSpeaking)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await ttsStopSpeakingCD.ShowAsync();
        }

        private void TtsStopSpeakingCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTtsStopSpeakingResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsStopSpeakingCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsStopSpeakingCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}