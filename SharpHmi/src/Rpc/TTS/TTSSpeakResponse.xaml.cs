﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSSpeakResponse : UserControl
    {
        Speak tmpObj = null;
        public TTSSpeakResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSpeak()
        {
            ContentDialog ttsSpeakCD = new ContentDialog();
            ttsSpeakCD.Content = this;

            ttsSpeakCD.PrimaryButtonText = Const.TxLater;
            ttsSpeakCD.PrimaryButtonClick += TtsSpeakCD_PrimaryButtonClick;

            ttsSpeakCD.SecondaryButtonText = Const.Reset;
            ttsSpeakCD.SecondaryButtonClick += TtsSpeakCD_ResetButtonClick;

            ttsSpeakCD.CloseButtonText = Const.Close;
            ttsSpeakCD.CloseButtonClick += TtsSpeakCD_CloseButtonClick;

            tmpObj = new Speak();
            tmpObj = (Speak)AppUtils.getSavedPreferenceValueForRpc<Speak>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(Speak);
                tmpObj = (Speak)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await ttsSpeakCD.ShowAsync();
        }

        private void TtsSpeakCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTtsSpeakResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsSpeakCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsSpeakCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
