﻿using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using HmiApiLib.Builder;
using System.Collections.Generic;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSGetCapabilitiesResponse : UserControl
    {
        GetCapabilities tmpObj = null;
        ContentDialog ttsGetCapabilitiesCD = new ContentDialog();
        ObservableCollection<HmiApiLib.Common.Enums.SpeechCapabilities> speechCapabilitiesList = new ObservableCollection<HmiApiLib.Common.Enums.SpeechCapabilities>();
        ObservableCollection<HmiApiLib.Common.Enums.PrerecordedSpeech> prerecordedSpeechList = new ObservableCollection<HmiApiLib.Common.Enums.PrerecordedSpeech>();
        
        public TTSGetCapabilitiesResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetCapabilities()
        {
            ttsGetCapabilitiesCD = new ContentDialog();
            ttsGetCapabilitiesCD.Content = this;

            ttsGetCapabilitiesCD.PrimaryButtonText = Const.TxLater;
            ttsGetCapabilitiesCD.PrimaryButtonClick += TtsGetCapabilitiesCD_PrimaryButtonClick;

            ttsGetCapabilitiesCD.SecondaryButtonText = Const.Reset;
            ttsGetCapabilitiesCD.SecondaryButtonClick += TtsGetCapabilitiesCD_ResetButtonClick;

            ttsGetCapabilitiesCD.CloseButtonText = Const.Close;
            ttsGetCapabilitiesCD.CloseButtonClick += TtsGetCapabilitiesCD_CloseButtonClick;

            TTSGetCapabilitiesSpeechCapabilities.ItemsSource = speechCapabilitiesList;
            TTSGetCapabilitiesPrerecordedSpeech.ItemsSource = prerecordedSpeechList;

            tmpObj = new GetCapabilities();
            tmpObj = (GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<GetCapabilities>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetCapabilities);
                tmpObj = (GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();

                if (tmpObj.getSpeechCapabilities() != null)
                {
                    List<HmiApiLib.Common.Enums.SpeechCapabilities> speechList = tmpObj.getSpeechCapabilities();
                    foreach (HmiApiLib.Common.Enums.SpeechCapabilities item in speechList)
                    {
                        speechCapabilitiesList.Add(item);
                    }
                }
                else
                {
                    SpeechCapabilitiesButton.IsEnabled = false;
                    SpeechCapabilitiesCheckBox.IsChecked = false;
                }

                
                if(tmpObj.getPrerecordedSpeechCapabilities() != null)
                {
                    List<HmiApiLib.Common.Enums.PrerecordedSpeech> prerecordedList = tmpObj.getPrerecordedSpeechCapabilities();
                    foreach (HmiApiLib.Common.Enums.PrerecordedSpeech item in prerecordedList)
                    {
                        prerecordedSpeechList.Add(item);
                    }
                }
                else
                {
                    PrerecordedSpeechButton.IsEnabled = false;
                    PrerecordedSpeechCheckBox.IsChecked = false;
                }

            }

            await ttsGetCapabilitiesCD.ShowAsync();
        }

        private void TtsGetCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            List<HmiApiLib.Common.Enums.SpeechCapabilities> speechCapabilities = null;
            if (SpeechCapabilitiesCheckBox.IsChecked == true)
            {
                speechCapabilities = new List<HmiApiLib.Common.Enums.SpeechCapabilities>();
                speechCapabilities.AddRange(speechCapabilitiesList);
            }
            List<HmiApiLib.Common.Enums.PrerecordedSpeech> precordedSpeech = null;
            if (PrerecordedSpeechCheckBox.IsChecked == true)
            {
                precordedSpeech = new List<HmiApiLib.Common.Enums.PrerecordedSpeech>();
                precordedSpeech.AddRange(prerecordedSpeechList);
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTTSGetCapabilitiesResponse(
                BuildRpc.getNextId(), resultCode, speechCapabilities, precordedSpeech);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsGetCapabilitiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsGetCapabilitiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void SpeechCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeechCapabilitiesButton.IsEnabled = (bool)SpeechCapabilitiesCheckBox.IsChecked;
        }

        private void PrerecordedSpeechCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PrerecordedSpeechButton.IsEnabled = (bool)PrerecordedSpeechCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private async void SpeechCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            ttsGetCapabilitiesCD.Hide();
            ContentDialog addSpeechCapabilitiesCD = new ContentDialog();
            //List<HmiApiLib.Common.Enums.SpeechCapabilities> speechList = tmpObj.getSpeechCapabilities();

            var addSpeechCapabilities = new SpeechCapabilities(speechCapabilitiesList);
            addSpeechCapabilitiesCD.Content = addSpeechCapabilities;

            addSpeechCapabilitiesCD.PrimaryButtonText = Const.OK;
            addSpeechCapabilitiesCD.PrimaryButtonClick += AddSpeechCapabilitiesCD_PrimaryButtonClick;

            addSpeechCapabilitiesCD.SecondaryButtonText = Const.Close;
            addSpeechCapabilitiesCD.SecondaryButtonClick += AddSpeechCapabilitiesCD_SecondaryButtonClick;

            await addSpeechCapabilitiesCD.ShowAsync();
        }

        private async void AddSpeechCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            SpeechCapabilities addSpeechCapabilities = sender.Content as SpeechCapabilities;
            foreach(HmiApiLib.Common.Enums.SpeechCapabilities s in addSpeechCapabilities.speechCapabilitiesCheckedList)
            {
                speechCapabilitiesList.Add(s);
            }
            

            sender.Hide();
            await ttsGetCapabilitiesCD.ShowAsync();
        }

        private async void AddSpeechCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await ttsGetCapabilitiesCD.ShowAsync();
        }

        private async void PrerecordedSpeechTapped(object sender, TappedRoutedEventArgs e)
        {
            ttsGetCapabilitiesCD.Hide();
            ContentDialog addPrerecordedSpeechCD = new ContentDialog();

            var addPrerecordedSpeech = new PrerecordedSpeech();
            addPrerecordedSpeechCD.Content = addPrerecordedSpeech;

            addPrerecordedSpeechCD.PrimaryButtonText = "Ok";
            addPrerecordedSpeechCD.PrimaryButtonClick += AddPrerecordedSpeechCD_PrimaryButtonClick;

            addPrerecordedSpeechCD.SecondaryButtonText = "Cancel";
            addPrerecordedSpeechCD.SecondaryButtonClick += AddPrerecordedSpeechCD_SecondaryButtonClick;

            await addPrerecordedSpeechCD.ShowAsync();
        }

        private async void AddPrerecordedSpeechCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addPrerecordedSpeech = sender.Content as PrerecordedSpeech;
            foreach (HmiApiLib.Common.Enums.PrerecordedSpeech prerecordedSpeech in addPrerecordedSpeech.prerecordedSpeechCheckedList)
            {
                prerecordedSpeechList.Add(prerecordedSpeech);
            }

            sender.Hide();
            await ttsGetCapabilitiesCD.ShowAsync();
        }

        private async void AddPrerecordedSpeechCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await ttsGetCapabilitiesCD.ShowAsync();
        }
    }
}
