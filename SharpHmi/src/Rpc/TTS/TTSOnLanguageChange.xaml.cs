﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSOnLanguageChange : UserControl
    {
        OnLanguageChange tmpObj = null;
        public TTSOnLanguageChange()
        {
            InitializeComponent();
            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;
        }

        public async void ShowOnLanguageChange()
        {
            ContentDialog ttsOnLanguageChangeCD = new ContentDialog();
            ttsOnLanguageChangeCD.Content = this;

            ttsOnLanguageChangeCD.PrimaryButtonText = Const.TxNow;
            ttsOnLanguageChangeCD.PrimaryButtonClick += TtsOnLanguageChangeCD_PrimaryButtonClick;

            ttsOnLanguageChangeCD.SecondaryButtonText = Const.Reset;
            ttsOnLanguageChangeCD.SecondaryButtonClick += TtsOnLanguageChangeCD_ResetButtonClick;

            ttsOnLanguageChangeCD.CloseButtonText = Const.Close;
            ttsOnLanguageChangeCD.CloseButtonClick += TtsOnLanguageChangeCD_CloseButtonClick;

            tmpObj = new OnLanguageChange();
            tmpObj = (OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<OnLanguageChange>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnLanguageChange);
                tmpObj = (OnLanguageChange)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getLanguage() != null)
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else
                {
                    LanguageCheckBox.IsChecked = false;
                    LanguageComboBox.IsEnabled = false;
                }
            }

            await ttsOnLanguageChangeCD.ShowAsync();
        }

        private void TtsOnLanguageChangeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Language? language = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                language = (Language)LanguageComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcMessage = BuildRpc.buildTtsOnLanguageChange(language);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
            AppInstanceManager.AppInstance.sendRpc(rpcMessage);
        }

        private void TtsOnLanguageChangeCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsOnLanguageChangeCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }
    }
}
