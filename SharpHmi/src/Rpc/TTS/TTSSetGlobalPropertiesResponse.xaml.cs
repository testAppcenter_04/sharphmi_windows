﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.TTS.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class TTSSetGlobalPropertiesResponse : UserControl
    {
        SetGlobalProperties tmpObj = null;
        public TTSSetGlobalPropertiesResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSetGlobalProperties()
        {
            ContentDialog ttsSetGlobalPropertiesCD = new ContentDialog();
            ttsSetGlobalPropertiesCD.Content = this;

            ttsSetGlobalPropertiesCD.PrimaryButtonText = Const.TxLater;
            ttsSetGlobalPropertiesCD.PrimaryButtonClick += TtsSetGlobalPropertiesCD_PrimaryButtonClick;

            ttsSetGlobalPropertiesCD.SecondaryButtonText = Const.Reset;
            ttsSetGlobalPropertiesCD.SecondaryButtonClick += TtsSetGlobalPropertiesCD_ResetButtonClick;

            ttsSetGlobalPropertiesCD.CloseButtonText = Const.Close;
            ttsSetGlobalPropertiesCD.CloseButtonClick += TtsSetGlobalPropertiesCD_CloseButtonClick;

            tmpObj = new SetGlobalProperties();
            tmpObj = (SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<SetGlobalProperties>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SetGlobalProperties);
                tmpObj = (SetGlobalProperties)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await ttsSetGlobalPropertiesCD.ShowAsync();
        }

        private void TtsSetGlobalPropertiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildTTSSetGlobalPropertiesResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void TtsSetGlobalPropertiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void TtsSetGlobalPropertiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}