﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.TTS
{
    public sealed partial class PrerecordedSpeech : UserControl
    {
        private static String[] PrerecordedSpeechArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.PrerecordedSpeech));
        public ObservableCollection<HmiApiLib.Common.Enums.PrerecordedSpeech> prerecordedSpeechCheckedList = new ObservableCollection<HmiApiLib.Common.Enums.PrerecordedSpeech>();

        public PrerecordedSpeech()
        {
            InitializeComponent();
            PrerecordedSpeechListBox.ItemsSource = PrerecordedSpeechArray;
        }

        private void PrerecordedSpeechCheckBox_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.PrerecordedSpeech prerecordedSpeech = (HmiApiLib.Common.Enums.PrerecordedSpeech)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.PrerecordedSpeech), (sender as CheckBox).Content.ToString());

            if (((CheckBox)sender).IsChecked == true)
                prerecordedSpeechCheckedList.Add(prerecordedSpeech);
            else
            {
                if (prerecordedSpeechCheckedList.Contains(prerecordedSpeech))
                    prerecordedSpeechCheckedList.Remove(prerecordedSpeech);
            }
        }

        private void PrerecordedSpeechCBChecked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
}
