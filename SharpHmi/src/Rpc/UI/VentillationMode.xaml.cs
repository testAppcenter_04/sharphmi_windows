﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class VentillationMode : UserControl
    {
        private String[] ventilationModeArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VentilationMode));
        public ObservableCollection<HmiApiLib.Common.Enums.VentilationMode> ventilationModeList = 
            new ObservableCollection<HmiApiLib.Common.Enums.VentilationMode>();

        public VentillationMode()
        {
            InitializeComponent();
            VentilationModeListBox.ItemsSource = ventilationModeArray;
        }

        private void VentillationItemCheckBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.VentilationMode ventMode = 
                (HmiApiLib.Common.Enums.VentilationMode)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.VentilationMode), (sender as CheckBox).Content.ToString());
            if (((CheckBox)sender).IsChecked == true)
                ventilationModeList.Add(ventMode);
            else
            {
                if (ventilationModeList.Contains(ventMode))
                    ventilationModeList.Remove(ventMode);
            }
        }
    }
}
