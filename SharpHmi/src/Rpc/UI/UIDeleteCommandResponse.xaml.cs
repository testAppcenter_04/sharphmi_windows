﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIDeleteCommandResponse : UserControl
    {
        DeleteCommand tmpObj;
        public UIDeleteCommandResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowDeleteCommand()
        {
            ContentDialog uiDeleteCommandCD = new ContentDialog();
            uiDeleteCommandCD.Content = this;

            uiDeleteCommandCD.PrimaryButtonText = Const.TxLater;
            uiDeleteCommandCD.PrimaryButtonClick += UiAddCommandCD_PrimaryButtonClick;

            uiDeleteCommandCD.SecondaryButtonText = Const.Reset;
            uiDeleteCommandCD.SecondaryButtonClick += UiAddCommandCD_SecondaryButtonClick;

            uiDeleteCommandCD.CloseButtonText = Const.Close;
            uiDeleteCommandCD.CloseButtonClick += UiAddCommandCD_CloseButtonClick;

            tmpObj = new DeleteCommand();
            tmpObj = (DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<DeleteCommand>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(DeleteCommand);
                tmpObj = (DeleteCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiDeleteCommandCD.ShowAsync();
        }

        private void UiAddCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiDeleteCommandResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiAddCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiAddCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
