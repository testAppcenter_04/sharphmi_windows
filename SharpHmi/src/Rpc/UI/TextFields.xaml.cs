﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class TextFields : UserControl
    {
        public CheckBox nameCheckBox;
        public ComboBox nameComboBox;

        public CheckBox characterSetCheckBox;
        public ComboBox characterSetComboBox;

        public CheckBox widthCheckbox;
        public TextBox widthTextblock;

        public CheckBox rowCheckBox;
        public TextBox rowTextblock;

        public TextFields()
        {
            this.InitializeComponent();

            NameComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.TextFieldName));
            NameComboBox.SelectedIndex = 0;

            CharacterSetComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.CharacterSet));
            CharacterSetComboBox.SelectedIndex = 0;

            nameCheckBox = NameCheckBox;
            nameComboBox = NameComboBox;
            characterSetCheckBox = CharacterSetCheckBox;
            characterSetComboBox = CharacterSetComboBox;
            widthCheckbox = WidthCheckBox;
            widthTextblock = WidthTextBox;
            rowCheckBox = RowCheckBox;
            rowTextblock = RowTextBox;
        }

        private void NameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NameComboBox.IsEnabled = (bool)NameCheckBox.IsChecked;
        }

        private void CharacterSetCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CharacterSetComboBox.IsEnabled = (bool)CharacterSetCheckBox.IsChecked;
        }

        private void WidthCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WidthTextBox.IsEnabled = (bool)WidthCheckBox.IsChecked;
        }

        private void RowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RowTextBox.IsEnabled = (bool)RowCheckBox.IsChecked;
        }
    }
}
