﻿using HmiApiLib.Common.Structs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class TouchEvent : UserControl
    {

        public CheckBox idCheckBox;
        public TextBox idTextBox;
        public CheckBox tsCheckBox;
        public TextBox tsTextBox;

        public List<TouchCoord> touchCoordList = new List<TouchCoord>();
        ObservableCollection<String> touchCoordObservableCollection = new ObservableCollection<String>();

        ContentDialog touchCoordCD = new ContentDialog();

        public TouchEvent()
        {
            this.InitializeComponent();

            idCheckBox = IDCheckBox;
            idTextBox = IDTextBox;

            tsCheckBox = TSCheckBox;
            tsTextBox = TSTextBox;

            UiTouchCoordList.ItemsSource = touchCoordObservableCollection;
        }

        private void IDCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IDTextBox.IsEnabled = (bool)IDCheckBox.IsChecked;
        }

        private void TSCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TSTextBox.IsEnabled = (bool)TSCheckBox.IsChecked;
        }

        private void AddTouchCoordCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddTouchCoordButton.IsEnabled = (bool)AddTouchCoordCheckBox.IsChecked;
            UiTouchCoordList.IsEnabled = (bool)AddTouchCoordCheckBox.IsChecked;
        }

        private async void AddTouchCoordTapped(object sender, TappedRoutedEventArgs e)
        {
            UIOnTouchEvent.touchEventCD.Hide();

            touchCoordCD = new ContentDialog();

            var touchCoord = new TouchCord();
            touchCoordCD.Content = touchCoord;

            touchCoordCD.PrimaryButtonText = "Ok";
            touchCoordCD.PrimaryButtonClick += TouchCoordCD_PrimaryButtonClick;

            touchCoordCD.SecondaryButtonText = "Cancel";
            touchCoordCD.SecondaryButtonClick += TouchCoordCD_SecondaryButtonClick;

            await touchCoordCD.ShowAsync();
        }

        private async void TouchCoordCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var touchCoord = sender.Content as TouchCord;
            TouchCoord touch = new TouchCoord();
            if (AddTouchCoordCheckBox.IsChecked == true)
            {
                touchCoordObservableCollection.Add("X=" + touchCoord.xCoordTextBox.Text + ", Y=" + touchCoord.yCoordTextBox.Text);
                try
                {
                    touch.x = Int32.Parse(touchCoord.xCoordTextBox.Text);
                    touch.y = Int32.Parse(touchCoord.yCoordTextBox.Text);
                }
                catch
                {
                    touch.x = 0;
                    touch.y = 0;
                }
                touchCoordList.Add(touch);
            }
            touchCoordCD.Hide();
            await UIOnTouchEvent.touchEventCD.ShowAsync();

        }

        private async void TouchCoordCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            touchCoordCD.Hide();
            await UIOnTouchEvent.touchEventCD.ShowAsync();
        }
    }
}
