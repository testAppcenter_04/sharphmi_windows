﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UISetMediaClockTimerResponse : UserControl
    {
        SetMediaClockTimer tmpObj;

        public UISetMediaClockTimerResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSetMediaClockTimer()
        {
            ContentDialog uiSetMediaClockTimerCD = new ContentDialog();
            uiSetMediaClockTimerCD.Content = this;

            uiSetMediaClockTimerCD.PrimaryButtonText = Const.TxLater;
            uiSetMediaClockTimerCD.PrimaryButtonClick += UiSetMediaClockTimerCD_PrimaryButtonClick;

            uiSetMediaClockTimerCD.SecondaryButtonText = Const.Reset;
            uiSetMediaClockTimerCD.SecondaryButtonClick += UiSetMediaClockTimerCD_SecondaryButtonClick;

            uiSetMediaClockTimerCD.CloseButtonText = Const.Close;
            uiSetMediaClockTimerCD.CloseButtonClick += UiSetMediaClockTimerCD_CloseButtonClick;

            tmpObj = new SetMediaClockTimer();
            tmpObj = (SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<SetMediaClockTimer>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SetMediaClockTimer);
                tmpObj = (SetMediaClockTimer)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiSetMediaClockTimerCD.ShowAsync();
        }

        private void UiSetMediaClockTimerCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiSetMediaClockTimerResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiSetMediaClockTimerCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiSetMediaClockTimerCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
