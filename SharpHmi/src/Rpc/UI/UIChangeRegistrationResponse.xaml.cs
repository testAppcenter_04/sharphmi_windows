﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIChangeRegistrationResponse : UserControl
    {
        ChangeRegistration tmpObj;

        public UIChangeRegistrationResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowChangeRegistration()
        {
            ContentDialog uiChangeRegistrationCD = new ContentDialog();
            uiChangeRegistrationCD.Content = this;

            uiChangeRegistrationCD.PrimaryButtonText = Const.TxLater;
            uiChangeRegistrationCD.PrimaryButtonClick += UiChangeRegistrationCD_PrimaryButtonClick;

            uiChangeRegistrationCD.SecondaryButtonText = Const.Reset;
            uiChangeRegistrationCD.SecondaryButtonClick += UiChangeRegistrationCD_SecondaryButtonClick;

            uiChangeRegistrationCD.CloseButtonText = Const.Close;
            uiChangeRegistrationCD.CloseButtonClick += UiChangeRegistrationCD_CloseButtonClick;

            tmpObj = new ChangeRegistration();
            tmpObj = (ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ChangeRegistration);
                tmpObj = (ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }
            

            await uiChangeRegistrationCD.ShowAsync();
        }

        private void UiChangeRegistrationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiChangeRegistrationResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiChangeRegistrationCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiChangeRegistrationCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;

        }
        
    }
}
