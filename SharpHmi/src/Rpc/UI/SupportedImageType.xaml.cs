﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class SupportedImageType : UserControl
    {
        private static String[] FileTypeArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.FileType));
        public ObservableCollection<HmiApiLib.Common.Enums.FileType> fileTypeList = new ObservableCollection<HmiApiLib.Common.Enums.FileType>();

        public SupportedImageType()
        {
            InitializeComponent();
            FileTypeListBox.ItemsSource = FileTypeArray;
        }

        private void FileTypeListBoxItemSelected(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.FileType fileType = (HmiApiLib.Common.Enums.FileType)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.FileType), (sender as CheckBox).Content.ToString());

            if (((CheckBox)sender).IsChecked == true)
                fileTypeList.Add(fileType);
            else
            {
                if (fileTypeList.Contains(fileType))
                    fileTypeList.Remove(fileType);
            }
        }
    }
}
