﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class SoftButtonCapabilities : UserControl
    {
        public CheckBox shortPressAvailableCB;
        public CheckBox longPressAvailableCB;
        public CheckBox upDownAvailableCB;
        public CheckBox imageSupportedCB;

        public SoftButtonCapabilities()
        {
            this.InitializeComponent();

            shortPressAvailableCB = ShortPressAvailableCheckBox;
            longPressAvailableCB = LongPressAvailableCheckBox;
            upDownAvailableCB = UpDownAvailableCheckBox;
            imageSupportedCB = ImageSupportedCheckBox;

        }

        private void ShortPressAvailable(object sender, TappedRoutedEventArgs e)
        {
            ShortPressAvailableTgl.IsEnabled = (bool)ShortPressAvailableCheckBox.IsChecked;
        }

        private void LongPressAvailableCBCheckedChange(object sender, TappedRoutedEventArgs e)
        {
            LongPressAvailableTgl.IsEnabled = (bool)LongPressAvailableCheckBox.IsChecked;

        }

        private void UpDownAvailableCBCheckedChange(object sender, TappedRoutedEventArgs e)
        {
            UpDownAvailableTgl.IsEnabled = (bool)UpDownAvailableCheckBox.IsChecked;
        }

        private void ImageSupportedCBCheckedChange(object sender, TappedRoutedEventArgs e)
        {
            ImageSupportedTgl.IsEnabled = (bool)ImageSupportedCheckBox.IsChecked;
        }
    }
}
