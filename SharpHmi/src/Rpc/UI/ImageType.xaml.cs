﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class ImageType : UserControl
    {
        private static String[] imageTypeArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ImageType));
        public ObservableCollection<HmiApiLib.Common.Enums.ImageType> imageTypeList = new ObservableCollection<HmiApiLib.Common.Enums.ImageType>();

        public ImageType()
        {
            InitializeComponent();
            ImageCapabilitiesListBox.ItemsSource = imageTypeArray;
        }

        private void ImageCapabilitiesListBoxItemTapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.ImageType imageType = (HmiApiLib.Common.Enums.ImageType)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.ImageType), (sender as CheckBox).Content.ToString());
            if (((CheckBox)sender).IsChecked == true)
                imageTypeList.Add(imageType);
            else
            {
                if (imageTypeList.Contains(imageType))
                    imageTypeList.Remove(imageType);
            }
        }
    }
}
