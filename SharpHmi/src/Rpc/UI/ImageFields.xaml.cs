﻿using HmiApiLib.Common.Enums;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class ImageFields : UserControl
    {
        public ObservableCollection<FileType> fileTypeList = new ObservableCollection<FileType>();
        public CheckBox nameCheckBox;
        public ComboBox nameComboBox;
        public CheckBox imageResolutionCheckBox;
        public CheckBox resolutionWidthCheckBox;
        public TextBox resolutionWidthTextBox;
        public CheckBox resolutionHeightCheckBox;
        public TextBox resolutionHeightTextBox;
        public CheckBox supportedImageTypeCheckBox;

        public ImageFields()
        {
            InitializeComponent();

            NameComboBox.ItemsSource = Enum.GetNames(typeof(ImageFieldName));
            NameComboBox.SelectedIndex = 0;

            UIGetCapImageFieldListView.ItemsSource = fileTypeList;

            nameCheckBox = NameCheckBox;
            nameComboBox = NameComboBox;
            imageResolutionCheckBox = ImageResolutionCheckBox;
            resolutionWidthCheckBox = ResolutionWidthCheckBox;
            resolutionWidthTextBox = ResolutionWidthTextBox;
            resolutionHeightCheckBox = ResolutionHeightCheckBox;
            resolutionHeightTextBox = ResolutionHeightTextBox;
            supportedImageTypeCheckBox = SupportedImageTypeCheckBox;
        }

        private void NameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NameComboBox.IsEnabled = (bool)NameCheckBox.IsChecked;
        }

        private void ImageResolutionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ImageResolutionCheckBox.IsChecked == true)
            {
                ResolutionWidthCheckBox.IsEnabled = true;
                ResolutionHeightCheckBox.IsEnabled = true;

                if (ResolutionWidthCheckBox.IsChecked == true)
                {
                    ResolutionWidthTextBox.IsEnabled = true;
                }
                else
                {
                    ResolutionWidthTextBox.IsEnabled = false;
                }
                if (ResolutionHeightCheckBox.IsChecked == true)
                {
                    ResolutionHeightTextBox.IsEnabled = true;
                }
                else
                {
                    ResolutionHeightTextBox.IsEnabled = false;
                }
            }
            else
            {
                ResolutionWidthCheckBox.IsEnabled = false;
                ResolutionWidthTextBox.IsEnabled = false;
                ResolutionHeightCheckBox.IsEnabled = false;
                ResolutionHeightTextBox.IsEnabled = false;
            }
        }

        private void ResolutionWidthCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResolutionWidthTextBox.IsEnabled = (bool)ResolutionWidthCheckBox.IsChecked;
        }

        private void ResolutionHeightCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResolutionHeightTextBox.IsEnabled = (bool)ResolutionHeightCheckBox.IsChecked;
        }

        private void SupportedImageTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SupportedImageTypeButton.IsEnabled = (bool)SupportedImageTypeCheckBox.IsChecked;
        }

        private async void SupportedImageTypeButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            DisplayCapabilities.addImageFiledsCD.Hide();
            
            var supportedImageType = new SupportedImageType();
            ContentDialog supportedImageTypeCD = new ContentDialog();
            supportedImageTypeCD.Content = supportedImageType;

            supportedImageTypeCD.PrimaryButtonText = "Ok";
            supportedImageTypeCD.PrimaryButtonClick += SupportedImageTypeCD_PrimaryButtonClick; ;

            supportedImageTypeCD.SecondaryButtonText = "Cancel";
            supportedImageTypeCD.SecondaryButtonClick += SupportedImageTypeCD_SecondaryButtonClick;

            await supportedImageTypeCD.ShowAsync();
        }

        private async void SupportedImageTypeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {                        
            sender.Hide();
            await DisplayCapabilities.addImageFiledsCD.ShowAsync();
        }

        private async void SupportedImageTypeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var fileType = sender.Content as SupportedImageType;
            
            foreach (FileType file in fileType.fileTypeList)
            {
                fileTypeList.Add(file);
            }

            sender.Hide();
            await DisplayCapabilities.addImageFiledsCD.ShowAsync();
        }
    }
}