﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class TouchCord : UserControl
    {
        public CheckBox xCoordCheckBox;
        public TextBox xCoordTextBox;

        public CheckBox yCoordCheckBox;
        public TextBox yCoordTextBox;

        public TouchCord()
        {
            this.InitializeComponent();

            xCoordCheckBox = XCoordCheckBox;
            xCoordTextBox = XCoordTextBox;

            yCoordCheckBox = YCoordCheckBox;
            yCoordTextBox = YCoordTextBox;
        }

        private void XCoordCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            XCoordTextBox.IsEnabled = (bool)XCoordCheckBox.IsChecked;
        }

        private void YCoordCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            YCoordTextBox.IsEnabled = (bool)YCoordCheckBox.IsChecked;
        }
    }
}
