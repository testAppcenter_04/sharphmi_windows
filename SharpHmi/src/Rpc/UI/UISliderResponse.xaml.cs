﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UISliderResponse : UserControl
    {
        HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj;
        public UISliderResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSlider()
        {
            ContentDialog uiSliderCD = new ContentDialog();
            uiSliderCD.Content = this;

            uiSliderCD.PrimaryButtonText = Const.TxLater;
            uiSliderCD.PrimaryButtonClick += UiSliderCD_PrimaryButtonClick;

            uiSliderCD.SecondaryButtonText = Const.Reset;
            uiSliderCD.SecondaryButtonClick += UiSliderCD_SecondaryButtonClick;

            uiSliderCD.CloseButtonText = Const.Close;
            uiSliderCD.CloseButtonClick += UiSliderCD_CloseButtonClick;

            tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Slider);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getSliderPosition() != null)
                    SliderPositionTextBox.Text = tmpObj.getSliderPosition().ToString();
                else {
                    SliderPositionTextBox.IsEnabled = false;
                    SliderPositionCheckBox.IsChecked = false;
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiSliderCD.ShowAsync();
        }

        private void UiSliderCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            
            int? sliderPosition = null;
            if (SliderPositionCheckBox.IsChecked == true)
            {
                try
                {
                    sliderPosition = Int32.Parse(SliderPositionTextBox.Text);
                }
                catch
                {
                    sliderPosition = 0;

                }
            }

            RpcResponse rpcResponse = BuildRpc.buildUiSliderResponse(
                BuildRpc.getNextId(), resultCode, sliderPosition);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiSliderCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiSliderCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SliderPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SliderPositionTextBox.IsEnabled = (bool)SliderPositionCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
