﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIPerformAudioPassThruResponse : UserControl
    {
        PerformAudioPassThru tmpObj;
        public UIPerformAudioPassThruResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowPerformAudioPassThru()
        {
            ContentDialog uiPerformAudioPassThruCD = new ContentDialog();
            uiPerformAudioPassThruCD.Content = this;

            uiPerformAudioPassThruCD.PrimaryButtonText = Const.TxLater;
            uiPerformAudioPassThruCD.PrimaryButtonClick += UiPerformAudioPassThruCD_PrimaryButtonClick;

            uiPerformAudioPassThruCD.SecondaryButtonText = Const.Reset;
            uiPerformAudioPassThruCD.SecondaryButtonClick += UiPerformAudioPassThruCD_SecondaryButtonClick;

            uiPerformAudioPassThruCD.CloseButtonText = Const.Close;
            uiPerformAudioPassThruCD.CloseButtonClick += UiPerformAudioPassThruCD_CloseButtonClick;

            tmpObj = new PerformAudioPassThru();
            tmpObj = (PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<PerformAudioPassThru>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(PerformAudioPassThru);
                tmpObj = (PerformAudioPassThru)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiPerformAudioPassThruCD.ShowAsync();
        }

        private void UiPerformAudioPassThruCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiPerformAudioPassThruResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiPerformAudioPassThruCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiPerformAudioPassThruCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
