﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using System.Collections.ObjectModel;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using SharpHmi.Buttons;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UISetDisplayLayoutResponse : UserControl
    {
        SetDisplayLayout tmpObj;

        ContentDialog setDisplayLayoutCD = new ContentDialog();
        public static ContentDialog displayCapabilitiesCD = new ContentDialog();
        ContentDialog addButtonCapabilitiesCD = new ContentDialog();

        String SoftButtonCap = "SoftButtonCap ";
        int SoftButtonCapIndex = 0;
        HmiApiLib.Common.Structs.DisplayCapabilities disp = null;
        ObservableCollection<ButtonCapabilities> btnCap = new ObservableCollection<ButtonCapabilities>();
        List<ButtonCapabilities> buttonCapabilitiesList = null;
        PresetBankCapabilities presetBankCap = null;
        ObservableCollection<String> softButtonList = new ObservableCollection<string>();
        List<HmiApiLib.Common.Structs.SoftButtonCapabilities> softButtonCapabilitiesList = null;
        public UISetDisplayLayoutResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSetDisplayLayout()
        {
            setDisplayLayoutCD = new ContentDialog();
            setDisplayLayoutCD.Content = this;

            setDisplayLayoutCD.PrimaryButtonText = Const.TxLater;
            setDisplayLayoutCD.PrimaryButtonClick += SetDisplayLayoutCD_PrimaryButtonClick;

            setDisplayLayoutCD.SecondaryButtonText = Const.Reset;
            setDisplayLayoutCD.SecondaryButtonClick += SetDisplayLayoutCD_SecondaryButtonClick;

            setDisplayLayoutCD.CloseButtonText = Const.Close;
            setDisplayLayoutCD.CloseButtonClick += SetDisplayLayoutCD_CloseButtonClick;

            ButtonGetCapabilitiesListView.ItemsSource = btnCap;
            SoftButtonCapabilitiesList.ItemsSource = softButtonList;

            tmpObj = new SetDisplayLayout();
            tmpObj = (SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<SetDisplayLayout>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SetDisplayLayout);
                tmpObj = (SetDisplayLayout)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                
                if (tmpObj.getDisplayCapabilities() == null) {
                    DisplayCapabilitiesCheckBox.IsChecked = false;
                    DisplayCapabilitiesButton.IsEnabled = false;
                }
                else
                    disp = tmpObj.getDisplayCapabilities();


                if (tmpObj.getButtonCapabilities() != null)
                {
                    foreach (ButtonCapabilities cap in tmpObj.getButtonCapabilities())
                    {
                        btnCap.Add(cap);
                    }
                    buttonCapabilitiesList = tmpObj.getButtonCapabilities();

                }
                else {
                    ButtonCapabilitiesCheckBox.IsChecked = false;
                    ButtonCapabilitiesButton.IsEnabled = false;
                }
                if (tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable() != null)
                {
                    //presetBankCap = tmpObj.getPresetBankCapabilities();

                     AllowOnScreenPresetsAvailableToggle.IsOn = (bool)tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable();
                }
                else {
                    OnScreenPresetsAvailableCheckBox.IsChecked = false;
                    AllowOnScreenPresetsAvailableToggle.IsEnabled = false;
                }

                softButtonCapabilitiesList = tmpObj.getSoftButtonCapabilities();

                if (null != tmpObj.getSoftButtonCapabilities())
                {
                    for (int i = 0; i < softButtonCapabilitiesList.Count; i++)
                    {
                        softButtonList.Add(SoftButtonCap + SoftButtonCapIndex);
                        SoftButtonCapIndex++;
                    }
                }
                else
                {
                    SoftButtonCapabilitiesCheckBox.IsChecked = false;
                    SoftButtonCapabilitiesButton.IsEnabled = false;
                }
            }

            await setDisplayLayoutCD.ShowAsync();

        }

        private void SetDisplayLayoutCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {            
            if (DisplayCapabilitiesCheckBox.IsChecked != true)
            {
                disp = null;
            }
            if (ButtonCapabilitiesCheckBox.IsChecked != true)
            {
               // buttonCapabilitiesList.Clear();
                buttonCapabilitiesList = null;
            }

            if (SoftButtonCapabilitiesCheckBox.IsChecked != true)
            {
                //softButtonCapabilitiesList.Clear();
                softButtonCapabilitiesList = null;
            }

            presetBankCap = new PresetBankCapabilities();
            if (OnScreenPresetsAvailableCheckBox.IsChecked == true)
            {
                presetBankCap.onScreenPresetsAvailable = AllowOnScreenPresetsAvailableToggle.IsOn;
            }
            else
                presetBankCap.onScreenPresetsAvailable = null ;

            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            
            HmiApiLib.Base.RpcResponse rpcMessage = BuildRpc.buildUiSetDisplayLayoutResponse(BuildRpc.getNextId(), resultCode, disp,
                buttonCapabilitiesList, softButtonCapabilitiesList, presetBankCap);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void SetDisplayLayoutCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SetDisplayLayoutCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void DisplayCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DisplayCapabilitiesButton.IsEnabled = (bool)DisplayCapabilitiesCheckBox.IsChecked;
        }

        private async void DisplayCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            setDisplayLayoutCD.Hide();

            displayCapabilitiesCD = new ContentDialog();

            var displayCapabilities = new DisplayCapabilities(disp, false);
            displayCapabilitiesCD.Content = displayCapabilities;

            displayCapabilitiesCD.PrimaryButtonText = Const.OK;
            displayCapabilitiesCD.PrimaryButtonClick += DisplayCapabilitiesCD_PrimaryButtonClick;

            displayCapabilitiesCD.SecondaryButtonText = Const.Close;
            displayCapabilitiesCD.SecondaryButtonClick += DisplayCapabilitiesCD_SecondaryButtonClick;

            await displayCapabilitiesCD.ShowAsync();
        }

        private async void DisplayCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var displayCapabilities = sender.Content as DisplayCapabilities;
            disp = new HmiApiLib.Common.Structs.DisplayCapabilities();
            if (displayCapabilities.displayTypeCheckBox.IsChecked == true)
            {
                disp.displayType = (DisplayType)displayCapabilities.displayTypeComboBox.SelectedIndex;
            }
            if (displayCapabilities.addTextFieldsCheckBox.IsChecked == true)
            {
                ObservableCollection<TextField> txtField = displayCapabilities.textFieldList;
                List<TextField> textFieldList = new List<TextField>();
                textFieldList.AddRange(txtField);
                disp.textFields = textFieldList;
            }
            if (displayCapabilities.addImageFieldsCheckBox.IsChecked == true)
            {
                ObservableCollection<ImageField> imgField = displayCapabilities.imageFieldList;
                List<ImageField> imgFieldList = new List<ImageField>();
                imgFieldList.AddRange(imgField);
                disp.imageFields = imgFieldList;
            }
            if (displayCapabilities.addMediaClockFormatCheckBox.IsChecked == true)
            {
                ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat> mList = displayCapabilities.mediaClockList;
                List<HmiApiLib.Common.Enums.MediaClockFormat> mediaList = new List<HmiApiLib.Common.Enums.MediaClockFormat>();
                mediaList.AddRange(mList);
                disp.mediaClockFormats = mediaList;
            }
            if (displayCapabilities.imageTypeCheckBox.IsChecked == true)
            {
                ObservableCollection<HmiApiLib.Common.Enums.ImageType> mList = displayCapabilities.imageTypeList;
                List<HmiApiLib.Common.Enums.ImageType> imgList = new List<HmiApiLib.Common.Enums.ImageType>();
                imgList.AddRange(mList);
                disp.imageCapabilities = imgList;
            }
            if (displayCapabilities.graphicSupportedCheckBox.IsChecked == true)
            {
                disp.graphicSupported = displayCapabilities.graphicSupportedToggle.IsOn;
            }
            if (displayCapabilities.templateAvailable.IsChecked == true)
            {
                String tempString = displayCapabilities.templateAvailableTextBox.Text.ToString();
                String[] tempArray = tempString.Split(",");
                List<String> templateList = new List<string>();
                templateList.AddRange(tempArray);
                disp.templatesAvailable = templateList;
            }
            if (displayCapabilities.screenParamsCheckBox.IsChecked == true)
            {
                disp.screenParams = displayCapabilities.screenParams;
            }
            if (displayCapabilities.numCustomPresetsAvailableCheckBox.IsChecked == true)
            {
                try
                {
                    disp.numCustomPresetsAvailable = Int32.Parse(displayCapabilities.numCustomPresetsAvailableTB.Text.ToString());
                }
                catch (Exception e)
                {

                }
            }

            sender.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private async void DisplayCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private void ButtonCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ButtonCapabilitiesButton.IsEnabled = (bool)ButtonCapabilitiesCheckBox.IsChecked;
        }

        private async void ButtonCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            setDisplayLayoutCD.Hide();

            addButtonCapabilitiesCD = new ContentDialog();

            var addButtonCapabilities = new AddButtonCapabilities();
            addButtonCapabilitiesCD.Content = addButtonCapabilities;

            addButtonCapabilitiesCD.PrimaryButtonText = "Ok";
            addButtonCapabilitiesCD.PrimaryButtonClick += AddButtonCapabilitiesCD_PrimaryButtonClick;

            addButtonCapabilitiesCD.SecondaryButtonText = "Cancel";
            addButtonCapabilitiesCD.SecondaryButtonClick += AddButtonCapabilitiesCD_SecondaryButtonClick;

            await addButtonCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addButtonCapabilities = sender.Content as AddButtonCapabilities;
            buttonCapabilitiesList = new List<ButtonCapabilities>();
            ButtonCapabilities buttonCapabilities = new ButtonCapabilities();
            if (addButtonCapabilities.buttonNameCheckBox.IsChecked == true)
                buttonCapabilities.name = (HmiApiLib.ButtonName)addButtonCapabilities.buttonNameComboBox.SelectedIndex;
            buttonCapabilities.shortPressAvailable = (bool)addButtonCapabilities.shortPressAvailableCheckBox.IsChecked;
            buttonCapabilities.longPressAvailable = (bool)addButtonCapabilities.longPressAvailableCheckBox.IsChecked;
            buttonCapabilities.upDownAvailable = (bool)addButtonCapabilities.upDownAvailableCheckBox.IsChecked;

            btnCap.Add(buttonCapabilities);
            buttonCapabilitiesList.Add(buttonCapabilities);
            addButtonCapabilitiesCD.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addButtonCapabilitiesCD.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private void SoftButtonCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SoftButtonCapabilitiesButton.IsEnabled = (bool)SoftButtonCapabilitiesCheckBox.IsChecked;
        }
        private async void SoftButtonCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            setDisplayLayoutCD.Hide();

            ContentDialog softButtonCapabilitiesCD = new ContentDialog();

            var softButtonCapabilities = new SoftButtonCapabilities();
            softButtonCapabilitiesCD.Content = softButtonCapabilities;

            softButtonCapabilitiesCD.PrimaryButtonText = Const.OK;
            softButtonCapabilitiesCD.PrimaryButtonClick += SoftButtonCapabilitiesCD_PrimaryButtonClick;

            softButtonCapabilitiesCD.SecondaryButtonText = Const.Close;
            softButtonCapabilitiesCD.SecondaryButtonClick += SoftButtonCapabilitiesCD_SecondaryButtonClick;

            await softButtonCapabilitiesCD.ShowAsync();
        }

        private async void SoftButtonCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var softButtonCapabilities = sender.Content as SoftButtonCapabilities;
            softButtonCapabilitiesList = new List<HmiApiLib.Common.Structs.SoftButtonCapabilities>();

            HmiApiLib.Common.Structs.SoftButtonCapabilities softBtnCap = new HmiApiLib.Common.Structs.SoftButtonCapabilities();
            softBtnCap.shortPressAvailable = (bool)softButtonCapabilities.shortPressAvailableCB.IsChecked;
            softBtnCap.longPressAvailable = (bool)softButtonCapabilities.longPressAvailableCB.IsChecked;
            softBtnCap.upDownAvailable = (bool)softButtonCapabilities.upDownAvailableCB.IsChecked;
            softBtnCap.imageSupported = (bool)softButtonCapabilities.imageSupportedCB.IsChecked;
            if (softBtnCap.shortPressAvailable != false || softBtnCap.longPressAvailable != false || softBtnCap.upDownAvailable != false || softBtnCap.imageSupported != false)
            {
            
                softButtonList.Add(SoftButtonCap + SoftButtonCapIndex);
                SoftButtonCapIndex++;
                softButtonCapabilitiesList.Add(softBtnCap);
            }
            sender.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private async void SoftButtonCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await setDisplayLayoutCD.ShowAsync();
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AllowOnScreenPresetsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowOnScreenPresetsAvailableToggle.IsEnabled =(bool) OnScreenPresetsAvailableCheckBox.IsChecked;
        }
    }
}