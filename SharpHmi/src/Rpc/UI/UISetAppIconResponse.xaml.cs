﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UISetAppIconResponse : UserControl
    {
        SetAppIcon tmpObj;
        public UISetAppIconResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSetAppIcon()
        {
            ContentDialog uiSetAppIconCD = new ContentDialog();
            uiSetAppIconCD.Content = this;

            uiSetAppIconCD.PrimaryButtonText = Const.TxLater;
            uiSetAppIconCD.PrimaryButtonClick += UiSetAppIconCD_PrimaryButtonClick;

            uiSetAppIconCD.SecondaryButtonText = Const.Reset;
            uiSetAppIconCD.SecondaryButtonClick += UiSetAppIconCD_SecondaryButtonClick;

            uiSetAppIconCD.CloseButtonText = Const.Close;
            uiSetAppIconCD.CloseButtonClick += UiSetAppIconCD_CloseButtonClick;

            tmpObj = new SetAppIcon();
            tmpObj = (SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<SetAppIcon>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SetAppIcon);
                tmpObj = (SetAppIcon)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiSetAppIconCD.ShowAsync();
        }

        private void UiSetAppIconCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiSetAppIconResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiSetAppIconCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiSetAppIconCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}