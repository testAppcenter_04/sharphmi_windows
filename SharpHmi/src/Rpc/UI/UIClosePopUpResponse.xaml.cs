﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIClosePopUpResponse : UserControl
    {
        ClosePopUp tmpObj;
        public UIClosePopUpResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowClosePopUp()
        {
            ContentDialog uiClosePopUpCD = new ContentDialog();
            uiClosePopUpCD.Content = this;

            uiClosePopUpCD.PrimaryButtonText = Const.TxLater;
            uiClosePopUpCD.PrimaryButtonClick += UiAddCommandCD_PrimaryButtonClick;

            uiClosePopUpCD.SecondaryButtonText = Const.Reset;
            uiClosePopUpCD.SecondaryButtonClick += UiAddCommandCD_SecondaryButtonClick;

            uiClosePopUpCD.CloseButtonText = Const.Close;
            uiClosePopUpCD.CloseButtonClick += UiAddCommandCD_CloseButtonClick;

            tmpObj = new ClosePopUp();
            tmpObj = (ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<ClosePopUp>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ClosePopUp);
                tmpObj = (ClosePopUp)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiClosePopUpCD.ShowAsync();
        }

        private void UiAddCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiClosePopUpResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiAddCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiAddCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
