﻿using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class DisplayCapabilities : UserControl
    {        
        //static ContentDialog addMediaClockFormatsCD = new ContentDialog();
        static ContentDialog imageTypeCD = new ContentDialog();
        static ContentDialog screenParamsCD = new ContentDialog();
        HmiApiLib.Common.Structs.DisplayCapabilities disp = null;
        public ComboBox displayTypeComboBox;
        public CheckBox displayTypeCheckBox;
        public CheckBox addTextFieldsCheckBox;
        public ObservableCollection<TextField> textFieldList = new ObservableCollection<TextField>();
        public CheckBox addImageFieldsCheckBox;
        public CheckBox addMediaClockFormatCheckBox;
        public CheckBox imageTypeCheckBox;
        public CheckBox graphicSupportedCheckBox;
        public ToggleSwitch graphicSupportedToggle;

        public CheckBox templateAvailable;
        public TextBox templateAvailableTextBox;
        public CheckBox screenParamsCheckBox;
        public CheckBox numCustomPresetsAvailableCheckBox;
        public TextBox numCustomPresetsAvailableTB;
        public HmiApiLib.Common.Structs.ScreenParams screenParams = null;

        public ObservableCollection<ImageField> imageFieldList = new ObservableCollection<ImageField>();
        public ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat> mediaClockList = new ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat>();
        public ObservableCollection<HmiApiLib.Common.Enums.ImageType> imageTypeList = new ObservableCollection<HmiApiLib.Common.Enums.ImageType>();


        public static ContentDialog addImageFiledsCD = new ContentDialog();
        bool isCalledFromGetCapabilities = false;

        public DisplayCapabilities(HmiApiLib.Common.Structs.DisplayCapabilities dispCap, bool isCalledFromGetCapabilities)
        {
            InitializeComponent();
            DisplayTypeComboBox.ItemsSource = Enum.GetNames(typeof(DisplayType));
            DisplayTypeComboBox.SelectedIndex = 0;

            disp = dispCap;
            this.isCalledFromGetCapabilities = isCalledFromGetCapabilities;

            if (null != disp)
            {    if (disp.getDisplayType()!=null)
                    DisplayTypeComboBox.SelectedIndex = (int)disp.getDisplayType();
                else {
                    DisplayTypeCheckBox.IsChecked = false;
                    DisplayTypeComboBox.IsEnabled = false;
                }
                List<TextField> textList = disp.getTextFields();
                if (null != textList)
                {
                    foreach (TextField t in textList)
                    {
                        textFieldList.Add(t);
                    }
                }
                else
                {
                    AddTextFieldsButton.IsEnabled = false;
                    AddTextFieldsCheckBox.IsChecked = false;
                }
                List<ImageField> imageList = disp.getImageFields();
                if (null != imageList)
                {
                    foreach (ImageField t in imageList)
                    {
                        imageFieldList.Add(t);
                    }
                }
                else {
                    AddImageFieldsButton.IsEnabled = false;

                    AddImageFieldsCheckBox.IsChecked = false;
                }
                List<HmiApiLib.Common.Enums.MediaClockFormat> mediaList = disp.getMediaClockFormats();
                if (null != mediaList)
                {
                    foreach (HmiApiLib.Common.Enums.MediaClockFormat m in mediaList)
                    {
                        mediaClockList.Add(m);
                    }
                }
                else {
                    AddMediaClockFormatButton.IsEnabled = false;

                    AddMediaClockFormatCheckBox.IsChecked = false;
                }
                List<HmiApiLib.Common.Enums.ImageType> imgList = disp.getImageCapabilities();
                if (imgList != null)
                {
                    foreach (HmiApiLib.Common.Enums.ImageType i in imgList)
                    {
                        imageTypeList.Add(i);
                    }
                }
                else {
                    ImageTypeButton.IsEnabled = false;
                    ImageTypeCheckBox.IsChecked = false;
                }
                


                if (disp.getGraphicSupported() == null)
                {
                    GraphicSupportedCheckBox.IsChecked = false;
                    GraphicSupportedToggle.IsEnabled = false;
                }
                else
                {
                    GraphicSupportedToggle.IsOn = (bool)disp.getGraphicSupported();
                }
                if (disp.getTemplatesAvailable() != null)
                {
                    List<String> templatesList = disp.getTemplatesAvailable();
                    String tempString = "";
                    if (null != templatesList && templatesList.Count > 0)
                    {
                        for (int i = 0; i < templatesList.Count; i++)
                        {
                            if (i == templatesList.Count - 1)
                            {
                                tempString = templatesList[i];
                            }
                            else
                            {
                                tempString = templatesList[i] + ",";
                            }
                        }

                    }
                    TemplateAvailableTextBox.Text = tempString;
                }
                else {
                    TemplateAvailableCheckBox.IsChecked = false;
                    TemplateAvailableTextBox.IsEnabled = false;
                }
                if (disp.getScreenParams() != null)
                    screenParams = disp.getScreenParams();
                else
                {
                    ScreenParamsButton.IsEnabled = false;
                    ScreenParamsCheckBox.IsChecked = false;
                }
                if (disp.numCustomPresetsAvailable != null)
                    NumCustomPresetsAvailableTextBox.Text = disp.numCustomPresetsAvailable.ToString();
                else {
                    NumCustomPresetsAvailableTextBox.IsEnabled = false;
                    NumCustomPresetsAvailableCheckBox.IsChecked = false;
                }
            }

            displayTypeComboBox = DisplayTypeComboBox;
            displayTypeCheckBox = DisplayTypeCheckBox;
            addTextFieldsCheckBox = AddTextFieldsCheckBox;
            addImageFieldsCheckBox = AddImageFieldsCheckBox;
            addMediaClockFormatCheckBox = AddMediaClockFormatCheckBox;
            imageTypeCheckBox = ImageTypeCheckBox;
            graphicSupportedCheckBox = GraphicSupportedCheckBox;
            graphicSupportedToggle = GraphicSupportedToggle;
            templateAvailable = TemplateAvailableCheckBox;
            templateAvailableTextBox = TemplateAvailableTextBox;
            screenParamsCheckBox = ScreenParamsCheckBox;
            numCustomPresetsAvailableCheckBox = NumCustomPresetsAvailableCheckBox;
            numCustomPresetsAvailableTB = NumCustomPresetsAvailableTextBox;

            UIGetCapTextFieldListView.ItemsSource = textFieldList;
            UIGetCapImageFieldListView.ItemsSource = imageFieldList;
            UIGetCapMediaClockListView.ItemsSource = mediaClockList;
            UIGetCapImageTypeListView.ItemsSource = imageTypeList;
        }

        private void DisplayTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DisplayTypeComboBox.IsEnabled = (bool)DisplayTypeCheckBox.IsChecked;
        }

        private async void AddTextFieldsTapped(object sender, TappedRoutedEventArgs e)
        {
            if (isCalledFromGetCapabilities)
                UIGetCapabilitiesResponse.displayCapabilitiesCD.Hide();
            else
                UISetDisplayLayoutResponse.displayCapabilitiesCD.Hide();

            ContentDialog addTextFieldsCD = new ContentDialog();

            var addTextFields = new TextFields();
            addTextFieldsCD.Content = addTextFields;

            addTextFieldsCD.PrimaryButtonText = "Ok";
            addTextFieldsCD.PrimaryButtonClick += AddTextFieldsCD_PrimaryButtonClick;

            addTextFieldsCD.SecondaryButtonText = "Cancel";
            addTextFieldsCD.SecondaryButtonClick += AddTextFieldsCD_SecondaryButtonClick;
           
            


            await addTextFieldsCD.ShowAsync();
        }

        private async void AddTextFieldsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void AddTextFieldsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addTextFields = sender.Content as TextFields;
            TextField textField = new TextField();
            if (addTextFields.nameCheckBox.IsChecked == true)
                textField.name = (TextFieldName)addTextFields.nameComboBox.SelectedIndex;
            if (addTextFields.characterSetCheckBox.IsChecked == true)
                textField.characterSet = (CharacterSet)addTextFields.characterSetComboBox.SelectedIndex;
            if (addTextFields.widthCheckbox.IsChecked == true)
            {
                try
                {
                    textField.width = Int32.Parse(addTextFields.widthTextblock.Text);
                }
                catch
                {
                    textField.width = 0;
                }
            }
            if (addTextFields.rowCheckBox.IsChecked == true)
            {
                try
                {
                    textField.rows = Int32.Parse(addTextFields.rowTextblock.Text);
                }
                catch
                {
                    textField.rows = 0;
                }
            }

            textFieldList.Add(textField);

            sender.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void AddImageFieldsTapped(object sender, TappedRoutedEventArgs e)
        {
            if (isCalledFromGetCapabilities)
                UIGetCapabilitiesResponse.displayCapabilitiesCD.Hide();
            else
                UISetDisplayLayoutResponse.displayCapabilitiesCD.Hide();

            var addImageField = new ImageFields();
            addImageFiledsCD = new ContentDialog();
            addImageFiledsCD.Content = addImageField;

            addImageFiledsCD.PrimaryButtonText = "Ok";
            addImageFiledsCD.PrimaryButtonClick += AddImageFiledsCD_PrimaryButtonClick;

            addImageFiledsCD.SecondaryButtonText = "Cancel";
            addImageFiledsCD.SecondaryButtonClick += AddImageFiledsCD_SecondaryButtonClick;

            await addImageFiledsCD.ShowAsync();
        }

        private async void AddImageFiledsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addImageFiledsCD.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void AddImageFiledsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addImageFields = sender.Content as ImageFields;

            ImageField imageField = new ImageField();
            if (addImageFields.nameCheckBox.IsChecked == true)
            {
                imageField.name = (ImageFieldName)addImageFields.nameComboBox.SelectedIndex;
            }
            ImageResolution imageResolution = null;
            if (addImageFields.imageResolutionCheckBox.IsChecked == true)
            {
                imageResolution = new ImageResolution();
                if (addImageFields.resolutionWidthCheckBox.IsChecked == true)
                {
                    try
                    {
                        imageResolution.resolutionWidth = Int32.Parse(addImageFields.resolutionWidthTextBox.Text);
                    }
                    catch
                    {
                        imageResolution.resolutionWidth = 0;
                    }
                }
                if (addImageFields.resolutionHeightCheckBox.IsChecked == true)
                {
                    try
                    {
                        imageResolution.resolutionHeight = Int32.Parse(addImageFields.resolutionHeightTextBox.Text);
                    }
                    catch
                    {
                        imageResolution.resolutionHeight = 0;
                    }
                }
                imageField.imageResolution = imageResolution;
            }
            if (addImageFields.supportedImageTypeCheckBox.IsChecked == true)
            {
                List<FileType> list = new List<FileType>();
                ObservableCollection<FileType> fileTypeList = addImageFields.fileTypeList;
                list.AddRange(fileTypeList);
                imageField.imageTypeSupported = list;
            }
            imageFieldList.Add(imageField);

            sender.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void AddMediaClockFormatTapped(object sender, TappedRoutedEventArgs e)
        {
            if (isCalledFromGetCapabilities)
                UIGetCapabilitiesResponse.displayCapabilitiesCD.Hide();
            else
                UISetDisplayLayoutResponse.displayCapabilitiesCD.Hide();

            ContentDialog addMediaClockFormatsCD = new ContentDialog();

            var addMediaClockFormat = new MediaClockFormat();
            addMediaClockFormatsCD.Content = addMediaClockFormat;

            addMediaClockFormatsCD.PrimaryButtonText = "Ok";
            addMediaClockFormatsCD.PrimaryButtonClick += AddMediaClockFormatsCD_PrimaryButtonClick;

            addMediaClockFormatsCD.SecondaryButtonText = "Cancel";
            addMediaClockFormatsCD.SecondaryButtonClick += AddMediaClockFormatsCD_SecondaryButtonClick;

            await addMediaClockFormatsCD.ShowAsync();
        }

        private async void AddMediaClockFormatsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void AddMediaClockFormatsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addMediaClockFormats = sender.Content as MediaClockFormat;
            ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat> mediaClkList = addMediaClockFormats.mediaClockList;
            foreach (HmiApiLib.Common.Enums.MediaClockFormat m in mediaClkList)
            {
                mediaClockList.Add(m);
            }
            sender.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void ImageTypeTapped(object sender, TappedRoutedEventArgs e)
        {
            if (isCalledFromGetCapabilities)
                UIGetCapabilitiesResponse.displayCapabilitiesCD.Hide();
            else
                UISetDisplayLayoutResponse.displayCapabilitiesCD.Hide();

            imageTypeCD = new ContentDialog();

            var addimageType = new ImageType();
            imageTypeCD.Content = addimageType;

            imageTypeCD.PrimaryButtonText = "Ok";
            imageTypeCD.PrimaryButtonClick += ImageTypeCD_PrimaryButtonClick;

            imageTypeCD.SecondaryButtonText = "Cancel";
            imageTypeCD.SecondaryButtonClick += ImageTypeCD_SecondaryButtonClick;

            await imageTypeCD.ShowAsync();
        }

        private async void ImageTypeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            imageTypeCD.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void ImageTypeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addImageType = sender.Content as ImageType;
            
            foreach (HmiApiLib.Common.Enums.ImageType m in addImageType.imageTypeList)
            {
                imageTypeList.Add(m);
            }

            imageTypeCD.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private void TemplateAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TemplateAvailableTextBox.IsEnabled = (bool)TemplateAvailableCheckBox.IsChecked;
        }

        private async void ScreenParamsTapped(object sender, TappedRoutedEventArgs e)
        {
            if (isCalledFromGetCapabilities)
                UIGetCapabilitiesResponse.displayCapabilitiesCD.Hide();
            else
                UISetDisplayLayoutResponse.displayCapabilitiesCD.Hide();

            screenParamsCD = new ContentDialog();

            var screenParam = new ScreenParams(screenParams);
            screenParamsCD.Content = screenParam;

            screenParamsCD.PrimaryButtonText = "Ok";
            screenParamsCD.PrimaryButtonClick += ScreenParamsCD_PrimaryButtonClick; ;

            screenParamsCD.SecondaryButtonText = "Cancel";
            screenParamsCD.SecondaryButtonClick += ScreenParamsCD_SecondaryButtonClick; ;

            await screenParamsCD.ShowAsync();
        }

        private async void ScreenParamsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            screenParamsCD.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private async void ScreenParamsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var screenParam = sender.Content as ScreenParams;
            screenParams = new HmiApiLib.Common.Structs.ScreenParams();

            ImageResolution imageResolution = null;
            TouchEventCapabilities touchEventCap = null;

            if (screenParam.imageResolutionCheckBox.IsChecked == true)
            {
                imageResolution = new ImageResolution();
                if (screenParam.resolutionWidthCheckBox.IsChecked == true && screenParam.resolutionWidthTextBox.Text !="")
                {
                    try
                    {
                        imageResolution.resolutionWidth = int.Parse(screenParam.resolutionWidthTextBox.Text); ;
                    }
                    catch
                    {
                    }
                }

                if (screenParam.resolutionHeightCheckBox.IsChecked == true)
                {
                    try
                    {
                        imageResolution.resolutionHeight = int.Parse(screenParam.resolutionHeightTextBox.Text);
                    }
                    catch
                    {
                    }
                }
            }
            screenParams.resolution = imageResolution;

            if (screenParam.touchEventCapabilityCheckBox.IsChecked == true)
            {
                touchEventCap = new TouchEventCapabilities();
                if ((bool)screenParam.pressAvailableCheckBox.IsChecked)
                    touchEventCap.pressAvailable = screenParam.pressAvailableToggle.IsOn;
                if ((bool)screenParam.multiTouchAvailableCheckBox.IsChecked)
                    touchEventCap.multiTouchAvailable = screenParam.multiTouchAvailableToggle.IsOn;
                if ((bool)screenParam.doublePressAvailableCheckBox.IsChecked)
                    touchEventCap.doublePressAvailable = screenParam.doublePressAvailableToggle.IsOn;
            }
            screenParams.touchEventAvailable = touchEventCap;

            screenParamsCD.Hide();
            if (isCalledFromGetCapabilities)
                await UIGetCapabilitiesResponse.displayCapabilitiesCD.ShowAsync();
            else
                await UISetDisplayLayoutResponse.displayCapabilitiesCD.ShowAsync();
        }

        private void NumCustomPresetsAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NumCustomPresetsAvailableTextBox.IsEnabled = (bool)NumCustomPresetsAvailableCheckBox.IsChecked;
        }

        private void AddTextFieldsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddTextFieldsButton.IsEnabled = (bool)AddTextFieldsCheckBox.IsChecked;
        }

        private void AddImageFieldsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddImageFieldsButton.IsEnabled = (bool)AddImageFieldsCheckBox.IsChecked;
        }

        private void AddMediaClockFormatCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddMediaClockFormatButton.IsEnabled = (bool)AddMediaClockFormatCheckBox.IsChecked;
        }

        private void ImageTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ImageTypeButton.IsEnabled = (bool)ImageTypeCheckBox.IsChecked;
        }

        private void ScreenParamsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ScreenParamsButton.IsEnabled = (bool)ScreenParamsCheckBox.IsChecked;
        }

        private void GraphicSupportedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GraphicSupportedToggle.IsEnabled = (bool)GraphicSupportedCheckBox.IsChecked;

        }
    }
}
