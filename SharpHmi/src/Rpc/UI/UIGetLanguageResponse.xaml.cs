﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIGetLanguageResponse : UserControl
    {
        GetLanguage tmpObj = null;
        public UIGetLanguageResponse()
        {
            InitializeComponent();
            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetLanguage()
        {
            ContentDialog uiGetLanguageCD = new ContentDialog();
            uiGetLanguageCD.Content = this;

            uiGetLanguageCD.PrimaryButtonText = Const.TxLater;
            uiGetLanguageCD.PrimaryButtonClick += UiGetLanguageCD_PrimaryButtonClick;

            uiGetLanguageCD.SecondaryButtonText = Const.Reset;
            uiGetLanguageCD.SecondaryButtonClick += UiGetLanguageCD_ResetButtonClick;

            uiGetLanguageCD.CloseButtonText = Const.Close;
            uiGetLanguageCD.CloseButtonClick += UiGetLanguageCD_CloseButtonClick;

            tmpObj = new GetLanguage();
            tmpObj = (GetLanguage)AppUtils.getSavedPreferenceValueForRpc<GetLanguage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetLanguage);
                tmpObj = (GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getLanguage() != null)
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }

            }

            await uiGetLanguageCD.ShowAsync();
        }

        private void UiGetLanguageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            Language? lang = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                lang = (Language)LanguageComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildUiGetLanguageResponse(
                BuildRpc.getNextId(), lang, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiGetLanguageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiGetLanguageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
