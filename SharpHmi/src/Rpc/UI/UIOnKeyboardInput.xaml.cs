﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnKeyboardInput : UserControl
    {
        OnKeyboardInput tmpObj;
        public UIOnKeyboardInput()
        {
            this.InitializeComponent();

            KeyBoardEventComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.KeyboardEvent));
            KeyBoardEventComboBox.SelectedIndex = 0;
        }

        public async void ShowOnKeyboardInput()
        {
            ContentDialog uiOnKeyboardInputCD = new ContentDialog();
            uiOnKeyboardInputCD.Content = this;

            uiOnKeyboardInputCD.PrimaryButtonText = Const.TxNow;
            uiOnKeyboardInputCD.PrimaryButtonClick += UiOnKeyboardInputCD_PrimaryButtonClick;

            uiOnKeyboardInputCD.SecondaryButtonText = Const.Reset;
            uiOnKeyboardInputCD.SecondaryButtonClick += UiOnKeyboardInputCD_SecondaryButtonClick;

            uiOnKeyboardInputCD.CloseButtonText = Const.Close;
            uiOnKeyboardInputCD.CloseButtonClick += UiOnKeyboardInputCD_CloseButtonClick;

            tmpObj = new OnKeyboardInput();
            tmpObj = (OnKeyboardInput)AppUtils.getSavedPreferenceValueForRpc<OnKeyboardInput>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnKeyboardInput);
                tmpObj = (OnKeyboardInput)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getData() != null)
                    DataTextBox.Text = tmpObj.getData();
                else {
                    DataTextBox.IsEnabled = false;
                    DataCheckBox.IsChecked = false;
                }
                if(tmpObj.getKeyboardEvent() != null)
                    KeyBoardEventComboBox.SelectedIndex = (int)tmpObj.getKeyboardEvent();
            }

            await uiOnKeyboardInputCD.ShowAsync();
        }

        private void UiOnKeyboardInputCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.KeyboardEvent? keyBoardEvent = null;
            if (KeyBoardEventCheckBox.IsChecked == true)
            {
                keyBoardEvent = (HmiApiLib.Common.Enums.KeyboardEvent)KeyBoardEventComboBox.SelectedIndex;
            }

            String data = null;
            if (DataCheckBox.IsChecked == true)
            {
                data = DataTextBox.Text;             
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildUIOnKeyboardInput(keyBoardEvent, data);

            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void UiOnKeyboardInputCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnKeyboardInputCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void DataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DataTextBox.IsEnabled = (bool)DataCheckBox.IsChecked;
        }

        private void KeyBoardEventCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            KeyBoardEventComboBox.IsEnabled = (bool)KeyBoardEventCheckBox.IsChecked;
        }
    }
}
