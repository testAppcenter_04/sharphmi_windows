﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;
        public UIIsReadyResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            
            ContentDialog uiIsReadyCD = new ContentDialog();

            uiIsReadyCD.Content = this;

            uiIsReadyCD.PrimaryButtonText = Const.TxLater;
            uiIsReadyCD.PrimaryButtonClick += UiIsReadyCD_PrimaryButtonClick;

            uiIsReadyCD.SecondaryButtonText = Const.Reset;
            uiIsReadyCD.SecondaryButtonClick += UiIsReadyCD_ResetButtonClick;

            uiIsReadyCD.CloseButtonText = Const.Close;
            uiIsReadyCD.CloseButtonClick += UiIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(IsReady);
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableTgl.IsEnabled = false;
                }
                else
                {
                    AvailableTgl.IsOn = (bool)tmpObj.getAvailable();
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiIsReadyCD.ShowAsync();
        }

        private void UiIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableTgl.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(
                BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.UI, toggle, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableTgl.IsEnabled = (bool)AvailableCheckBox.IsChecked;
        }
    }
}
