﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class AudioPassThruCapabilities : UserControl
    {
        HmiApiLib.Common.Structs.AudioPassThruCapabilities aptc = null;
        public CheckBox samplingRateCheckBox;
        public ComboBox samplingRateComboBox;
        public CheckBox bitsPerSampleCheckBox;
        public ComboBox bitsPerSampleComboBox;
        public CheckBox audioTypeCheckBox;
        public ComboBox audioTypeComboBox;

        public AudioPassThruCapabilities(HmiApiLib.Common.Structs.AudioPassThruCapabilities audioPassThruCapabilities)
        {
            InitializeComponent();

            aptc = audioPassThruCapabilities;
            SamplingRateComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.SamplingRate));
            SamplingRateComboBox.SelectedIndex = 0;

            BitsPerSampleComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.BitsPerSample));
            BitsPerSampleComboBox.SelectedIndex = 0;

            AudioTypeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.AudioType));
            AudioTypeComboBox.SelectedIndex = 0;

            if (null != aptc)
            {
                if (aptc.getSamplingRate() != null)
                    SamplingRateComboBox.SelectedIndex = (int)aptc.getSamplingRate();
                else {
                    SamplingRateComboBox.IsEnabled = false;
                    SamplingRateCheckBox.IsChecked = false;
                }
                if (aptc.getBitsPerSample() != null)
                    BitsPerSampleComboBox.SelectedIndex = (int)aptc.getBitsPerSample();
                else {
                    BitsPerSampleComboBox.IsEnabled = false;
                    BitsPerSampleCheckBox.IsChecked = false;
                }
                if (aptc.getAudioType() != null)
                    AudioTypeComboBox.SelectedIndex = (int)aptc.getAudioType();
                else
                {
                    AudioTypeComboBox.IsEnabled = false;
                    AudioTypeCheckBox.IsChecked = false;
                }
            }

            samplingRateCheckBox = SamplingRateCheckBox;
            samplingRateComboBox = SamplingRateComboBox;
            bitsPerSampleCheckBox = BitsPerSampleCheckBox; 
            bitsPerSampleComboBox = BitsPerSampleComboBox;
            audioTypeCheckBox = AudioTypeCheckBox;
            audioTypeComboBox = AudioTypeComboBox;
        }


        private void SamplingRateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SamplingRateComboBox.IsEnabled = (bool)SamplingRateCheckBox.IsChecked;
        }

        private void BitsPerSampleCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BitsPerSampleComboBox.IsEnabled = (bool)BitsPerSampleCheckBox.IsChecked;
        }

        private void AudioTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AudioTypeComboBox.IsEnabled = (bool)AudioTypeCheckBox.IsChecked;
        }

    }
}
