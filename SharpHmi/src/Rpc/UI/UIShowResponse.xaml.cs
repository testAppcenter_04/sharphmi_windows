﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIShowResponse : UserControl
    {
        Show tmpObj;
        public UIShowResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void Show()
        {
            ContentDialog uiShowCD = new ContentDialog();
            uiShowCD.Content = this;

            uiShowCD.PrimaryButtonText = Const.TxLater;
            uiShowCD.PrimaryButtonClick += UiShowCD_PrimaryButtonClick;

            uiShowCD.SecondaryButtonText = Const.Reset;
            uiShowCD.SecondaryButtonClick += UiShowCD_SecondaryButtonClick;

            uiShowCD.CloseButtonText = Const.Close;
            uiShowCD.CloseButtonClick += UiShowCD_CloseButtonClick;

            tmpObj = new Show();
            tmpObj = (Show)AppUtils.getSavedPreferenceValueForRpc<Show>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(Show);
                tmpObj = (Show)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiShowCD.ShowAsync();
        }

        private void UiShowCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiShowResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiShowCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiShowCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}