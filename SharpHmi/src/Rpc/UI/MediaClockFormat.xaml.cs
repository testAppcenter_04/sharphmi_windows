﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class MediaClockFormat : UserControl
    {
        private static String[] MediaClockFormatsArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.MediaClockFormat));
        public ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat> mediaClockList = new ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat>();

        public MediaClockFormat()
        {
            InitializeComponent();
            MediaClockFormatsListBox.ItemsSource = MediaClockFormatsArray;
        }

        private void MediaClockFormatsListBoxItemSelected(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.MediaClockFormat mediaClockFormat = (HmiApiLib.Common.Enums.MediaClockFormat)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.MediaClockFormat), (sender as CheckBox).Content.ToString());
                        
            if (((CheckBox)sender).IsChecked == true)
                mediaClockList.Add(mediaClockFormat);
            else
            {
                if (mediaClockList.Contains(mediaClockFormat))
                    mediaClockList.Remove(mediaClockFormat);
            }
        }
    }
}