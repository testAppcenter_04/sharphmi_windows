﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using System.Collections.ObjectModel;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnResetTimeout : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        OnResetTimeout tmpObj;

        public UIOnResetTimeout()
        {
            this.InitializeComponent();

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnResetTimeout()
        {
            ContentDialog uiOnResetTimeoutCD = new ContentDialog();
            uiOnResetTimeoutCD.Content = this;

            uiOnResetTimeoutCD.PrimaryButtonText = Const.TxNow;
            uiOnResetTimeoutCD.PrimaryButtonClick += UiOnResetTimeoutCD_PrimaryButtonClick;

            uiOnResetTimeoutCD.SecondaryButtonText = Const.Reset;
            uiOnResetTimeoutCD.SecondaryButtonClick += UiOnResetTimeoutCD_SecondaryButtonClick;

            uiOnResetTimeoutCD.CloseButtonText = Const.Close;
            uiOnResetTimeoutCD.CloseButtonClick += UiOnResetTimeoutCD_CloseButtonClick;

            tmpObj = new OnResetTimeout();
            tmpObj = (OnResetTimeout)AppUtils.getSavedPreferenceValueForRpc<OnResetTimeout>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnResetTimeout);
                tmpObj = (OnResetTimeout)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getMethodName())
                    MethodNameTextBox.Text = tmpObj.getMethodName().ToString();
                else

                {
                    MethodNameTextBox.IsEnabled = false;
                    MethodNameCheckBox.IsChecked = false;
                }

                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;

                }
            }

            await uiOnResetTimeoutCD.ShowAsync();
        }

        private void UiOnResetTimeoutCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }

            String methodName = null;
            if (MethodNameCheckBox.IsChecked == true)
            {
                try
                {
                    methodName = MethodNameTextBox.Text;
                }
                catch (Exception e)
                {

                }
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildUIOnResetTimeout(selectedAppID, methodName);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void UiOnResetTimeoutCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnResetTimeoutCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }

        private void MethodNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MethodNameTextBox.IsEnabled = (bool)MethodNameCheckBox.IsChecked;
        }
    }
}
