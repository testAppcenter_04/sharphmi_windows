﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnDriverDistraction : UserControl
    {
        OnDriverDistraction tmpObj;
        public UIOnDriverDistraction()
        {
            this.InitializeComponent();

            DriverDistractionStateComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.DriverDistractionState));
            DriverDistractionStateComboBox.SelectedIndex = 0;
        }

        public async void ShowOnDriverDistraction()
        {
            ContentDialog uiOnDriverDistractionCD = new ContentDialog();
            uiOnDriverDistractionCD.Content = this;

            uiOnDriverDistractionCD.PrimaryButtonText = Const.TxNow;
            uiOnDriverDistractionCD.PrimaryButtonClick += UiOnDriverDistractionCD_PrimaryButtonClick;

            uiOnDriverDistractionCD.SecondaryButtonText = Const.Reset;
            uiOnDriverDistractionCD.SecondaryButtonClick += UiOnDriverDistractionCD_SecondaryButtonClick;

            uiOnDriverDistractionCD.CloseButtonText = Const.Close;
            uiOnDriverDistractionCD.CloseButtonClick += UiOnDriverDistractionCD_CloseButtonClick;

            tmpObj = new OnDriverDistraction();
            tmpObj = (OnDriverDistraction)AppUtils.getSavedPreferenceValueForRpc<OnDriverDistraction>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnDriverDistraction);
                tmpObj = (OnDriverDistraction)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getDriverDistractionState() != null)
                    DriverDistractionStateComboBox.SelectedIndex = (int)tmpObj.getDriverDistractionState();
                else {

                    DriverDistractionStateComboBox.IsEnabled = false;
                    DriverDistractionStateCheckBox.IsChecked = false;

                }
            }

            await uiOnDriverDistractionCD.ShowAsync();
        }

        private void UiOnDriverDistractionCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.DriverDistractionState? driverDistractionState = null;
            if (DriverDistractionStateCheckBox.IsChecked == true)
            {
                driverDistractionState = (HmiApiLib.Common.Enums.DriverDistractionState)DriverDistractionStateComboBox.SelectedIndex;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildUIOnDriverDistraction(driverDistractionState);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void UiOnDriverDistractionCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnDriverDistractionCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void DriverDistractionStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverDistractionStateComboBox.IsEnabled = (bool)DriverDistractionStateCheckBox.IsChecked;
        }
    }
}
