﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIAlertResponse : UserControl
    {
        Alert tmpObj;
        public UIAlertResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowAlert()
        {
            ContentDialog uiAddCommandCD = new ContentDialog();
            uiAddCommandCD.Content = this;

            uiAddCommandCD.PrimaryButtonText = Const.TxLater;
            uiAddCommandCD.PrimaryButtonClick += UiAddCommandCD_PrimaryButtonClick;

            uiAddCommandCD.SecondaryButtonText = Const.Reset;
            uiAddCommandCD.SecondaryButtonClick += UiAddCommandCD_SecondaryButtonClick;

            uiAddCommandCD.CloseButtonText = Const.Close;
            uiAddCommandCD.CloseButtonClick += UiAddCommandCD_CloseButtonClick;

            tmpObj = new Alert();
            tmpObj = (Alert)AppUtils.getSavedPreferenceValueForRpc<Alert>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(Alert);
                tmpObj = (Alert)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getTryAgainTime() != null)
                    TryAgainTimeTextBox.Text = tmpObj.getTryAgainTime().ToString();
                else
                {
                    TryAgainTimeCheckBox.IsChecked = false;
                    TryAgainTimeTextBox.IsEnabled = false;
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiAddCommandCD.ShowAsync();
        }

        private void UiAddCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            int? tryAgainTime = null;
            if(TryAgainTimeCheckBox.IsChecked == true)
            {
                try
                {
                    tryAgainTime = Int32.Parse(TryAgainTimeTextBox.Text);
                }
                catch
                {
                    tryAgainTime = 0;

                }
            }
            RpcResponse rpcResponse = BuildRpc.buildUiAlertResponse(
                BuildRpc.getNextId(), resultCode, tryAgainTime);

            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiAddCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiAddCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void TryAgainTimeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TryAgainTimeTextBox.IsEnabled = (bool)TryAgainTimeCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
