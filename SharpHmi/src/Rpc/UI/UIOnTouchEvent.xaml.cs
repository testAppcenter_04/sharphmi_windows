﻿using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnTouchEvent : UserControl
    {
        OnTouchEvent tmpObj;
        ContentDialog onTouchEventCD = new ContentDialog();
        public static ContentDialog touchEventCD = new ContentDialog();
        
        ObservableCollection<HmiApiLib.Common.Structs.TouchEvent> touchEventObservableCollection = new ObservableCollection<HmiApiLib.Common.Structs.TouchEvent>();

        public UIOnTouchEvent()
        {
            this.InitializeComponent();

            TouchTypeComboBox.ItemsSource = Enum.GetNames(typeof(TouchType));
            TouchTypeComboBox.SelectedIndex = 0;

            UiTouchEventList.ItemsSource = touchEventObservableCollection;
            
        }



        public async void ShowOnTouchEvent()
        {
            onTouchEventCD = new ContentDialog();
            onTouchEventCD.Content = this;

            onTouchEventCD.PrimaryButtonText = Const.TxNow;
            onTouchEventCD.PrimaryButtonClick += UiOnTouchEventCD_PrimaryButtonClick;

            onTouchEventCD.SecondaryButtonText = Const.Reset;
            onTouchEventCD.SecondaryButtonClick += UiOnTouchEventCD_SecondaryButtonClick;

            onTouchEventCD.CloseButtonText = Const.Close;
            onTouchEventCD.CloseButtonClick += UiOnTouchEventCD_CloseButtonClick;

            tmpObj = new OnTouchEvent();
            tmpObj = (OnTouchEvent)AppUtils.getSavedPreferenceValueForRpc<OnTouchEvent>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnTouchEvent);
                tmpObj = (OnTouchEvent)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getTouchType())
                    TouchTypeComboBox.SelectedIndex = (int)tmpObj.getTouchType();
                else
                {
                    TouchTypeCheckBox.IsChecked = false;
                    TouchTypeComboBox.IsEnabled = false;
                }


                if (tmpObj.getTouchEvent() != null)
                {
                    foreach (HmiApiLib.Common.Structs.TouchEvent touch in tmpObj.getTouchEvent())
                    {
                        touchEventObservableCollection.Add(touch);
                    }
                }
                else
                {
                    TouchEventButton.IsEnabled = false;
                    TouchEventCheckBox.IsChecked = false;

                }
            }

            await onTouchEventCD.ShowAsync();
        }

        private void UiOnTouchEventCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            List<HmiApiLib.Common.Structs.TouchEvent> touchEvent = null;

            if (TouchEventCheckBox.IsChecked == true)
            {
                touchEvent = new List<HmiApiLib.Common.Structs.TouchEvent>();
                touchEvent.AddRange(touchEventObservableCollection);
            }

            TouchType? touchType = null;
            if (TouchTypeCheckBox.IsChecked == true)
                touchType = (TouchType)TouchTypeComboBox.SelectedIndex;

            RequestNotifyMessage rpcNotification = BuildRpc.buildUiOnTouchEvent(touchType, touchEvent);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void UiOnTouchEventCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnTouchEventCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void TouchTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TouchTypeComboBox.IsEnabled = (bool)TouchTypeCheckBox.IsChecked;
        }

        private void TouchEventCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TouchEventButton.IsEnabled = (bool)TouchEventCheckBox.IsChecked;
        }

        private async void TouchEventTapped(object sender, TappedRoutedEventArgs e)
        {
            onTouchEventCD.Hide();

            touchEventCD = new ContentDialog();

            var touchEvent = new TouchEvent();
            touchEventCD.Content = touchEvent;

            touchEventCD.PrimaryButtonText = "Ok";
            touchEventCD.PrimaryButtonClick += TouchEventCD_PrimaryButtonClick;

            touchEventCD.SecondaryButtonText = "Cancel";
            touchEventCD.SecondaryButtonClick += TouchEventCD_SecondaryButtonClick;

            await touchEventCD.ShowAsync();
        }

        private async void TouchEventCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var touchEvnt = sender.Content as TouchEvent;
            HmiApiLib.Common.Structs.TouchEvent touchEvent = new HmiApiLib.Common.Structs.TouchEvent();

            if (touchEvnt.idCheckBox.IsChecked == true)
            {
                try
                {
                    touchEvent.id = Int32.Parse(touchEvnt.idTextBox.Text);
                }
                catch
                {
                    touchEvent.id = 0;
                }
            }


            List<String> tsStringList = null;
            List<int> tsIntList = null;

            if (touchEvnt.tsCheckBox.IsChecked == true)
            {
                tsStringList = new List<String>();
                tsIntList = new List<int>();
                if (null != touchEvnt.tsTextBox.Text)
                {
                    tsStringList.AddRange(touchEvnt.tsTextBox.Text.Split(','));

                    foreach (String s in tsStringList)
                    {
                        try
                        {
                            tsIntList.Add(Int32.Parse(s));
                        }
                        catch
                        {

                        }
                    }
                }
            }

            touchEvent.ts = tsIntList;
            touchEvent.c = touchEvnt.touchCoordList;

            touchEventObservableCollection.Add(touchEvent);

            touchEventCD.Hide();
            await onTouchEventCD.ShowAsync();
        }

        private async void TouchEventCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            touchEventCD.Hide();
            await onTouchEventCD.ShowAsync();
        }
    }
}
