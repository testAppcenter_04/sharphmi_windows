﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIPerformInteractionResponse : UserControl
    {
        PerformInteraction tmpObj;
        public UIPerformInteractionResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowPerformInteraction()
        {
            ContentDialog uiPerformInteractionCD = new ContentDialog();
            uiPerformInteractionCD.Content = this;

            uiPerformInteractionCD.PrimaryButtonText = Const.TxLater;
            uiPerformInteractionCD.PrimaryButtonClick += UiPerformInteractionCD_PrimaryButtonClick;

            uiPerformInteractionCD.SecondaryButtonText = Const.Reset;
            uiPerformInteractionCD.SecondaryButtonClick += UiPerformInteractionCD_SecondaryButtonClick;

            uiPerformInteractionCD.CloseButtonText = Const.Close;
            uiPerformInteractionCD.CloseButtonClick += UiPerformInteractionCD_CloseButtonClick;

            tmpObj = new PerformInteraction();
            tmpObj = (PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<PerformInteraction>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(PerformInteraction);
                tmpObj = (PerformInteraction)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getChoiceID() != null)
                    ChoiceIDTextBox.Text = tmpObj.getChoiceID().ToString();
                else {
                    ChoiceIDCheckBox.IsChecked = false;
                    ChoiceIDTextBox.IsEnabled = false;
                }

                if (tmpObj.getManualTextEntry()!= null)
                    ManualTextEntryTextBox.Text = tmpObj.getManualTextEntry().ToString();
                else {
                    ManualTextEntryTextBox.IsEnabled = false;
                    ManualTextEntryCheckBox.IsChecked = false;
                }

                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiPerformInteractionCD.ShowAsync();
        }

        private void UiPerformInteractionCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            int? choiceId = null;
            if (ChoiceIDCheckBox.IsChecked == true)
            {
                try
                {
                    choiceId = Int32.Parse(ChoiceIDTextBox.Text);
                }
                catch
                {
                    choiceId = 0;
                }
            }

            String manualTextEntry = null;
            if (ManualTextEntryCheckBox.IsChecked == true)
            {
                manualTextEntry = ManualTextEntryTextBox.Text;
            }

            RpcResponse rpcResponse = BuildRpc.buildUiPerformInteractionResponse(
            BuildRpc.getNextId(), choiceId, manualTextEntry, resultCode);

            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiPerformInteractionCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiPerformInteractionCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ChoiceIDCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ChoiceIDTextBox.IsEnabled = (bool)ChoiceIDCheckBox.IsChecked;
        }

        private void ManualTextEntryCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ManualTextEntryTextBox.IsEnabled = (bool)ManualTextEntryCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
