﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using System.Collections.ObjectModel;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnSystemContext : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        OnSystemContext tmpObj;

        public UIOnSystemContext()
        {
            this.InitializeComponent();

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnSystemContext()
        {
            ContentDialog uiOnSystemContextCD = new ContentDialog();
            uiOnSystemContextCD.Content = this;

            uiOnSystemContextCD.PrimaryButtonText = Const.TxNow;
            uiOnSystemContextCD.PrimaryButtonClick += UiOnSystemContextCD_PrimaryButtonClick;

            uiOnSystemContextCD.SecondaryButtonText = Const.Reset;
            uiOnSystemContextCD.SecondaryButtonClick += UiOnSystemContextCD_SecondaryButtonClick;

            uiOnSystemContextCD.CloseButtonText = Const.Close;
            uiOnSystemContextCD.CloseButtonClick += UiOnSystemContextCD_CloseButtonClick;

            tmpObj = new OnSystemContext();
            tmpObj = (OnSystemContext)AppUtils.getSavedPreferenceValueForRpc<OnSystemContext>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnSystemContext);
                tmpObj = (OnSystemContext)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getSystemContext() != null)
                {
                    if (appListObservableCollection.Count > 0)
                        SystemContextComboBox.SelectedIndex = (int)tmpObj.getSystemContext();
                }
                else
                {
                    SystemContextCheckBox.IsChecked = false;
                    SystemContextComboBox.IsEnabled = false;

                }
                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else
                {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }
            }

            await uiOnSystemContextCD.ShowAsync();
        }

        private void UiOnSystemContextCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text);
                    }
                    catch
                    {

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }

            HmiApiLib.Common.Enums.SystemContext? systemContext = null;
            if (SystemContextCheckBox.IsChecked == true)
            {
                systemContext = (HmiApiLib.Common.Enums.SystemContext)SystemContextComboBox.SelectedIndex;
            }
            
            RequestNotifyMessage rpcNotification = BuildRpc.buildUiOnSystemContext(systemContext, selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void UiOnSystemContextCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnSystemContextCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void SystemContextCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SystemContextComboBox.IsEnabled = (bool)SystemContextCheckBox.IsChecked;
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }
    }
}
