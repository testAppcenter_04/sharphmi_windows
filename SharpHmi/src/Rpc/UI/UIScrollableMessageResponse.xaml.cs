﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIScrollableMessageResponse : UserControl
    {
        ScrollableMessage tmpObj;
        public UIScrollableMessageResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowScrollableMessage()
        {
            ContentDialog uiScrollableMessageCD = new ContentDialog();
            uiScrollableMessageCD.Content = this;

            uiScrollableMessageCD.PrimaryButtonText = Const.TxLater;
            uiScrollableMessageCD.PrimaryButtonClick += UiScrollableMessageCD_PrimaryButtonClick;

            uiScrollableMessageCD.SecondaryButtonText = Const.Reset;
            uiScrollableMessageCD.SecondaryButtonClick += UiScrollableMessageCD_SecondaryButtonClick;

            uiScrollableMessageCD.CloseButtonText = Const.Close;
            uiScrollableMessageCD.CloseButtonClick += UiScrollableMessageCD_CloseButtonClick;

            tmpObj = new ScrollableMessage();
            tmpObj = (ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<ScrollableMessage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ScrollableMessage);
                tmpObj = (ScrollableMessage)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiScrollableMessageCD.ShowAsync();
        }

        private void UiScrollableMessageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiScrollableMessageResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiScrollableMessageCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiScrollableMessageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
