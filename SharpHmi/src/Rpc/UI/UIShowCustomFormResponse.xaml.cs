﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIShowCustomFormResponse : UserControl
    {
        ShowCustomForm tmpObj;

        public UIShowCustomFormResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowCustomForm()
        {
            ContentDialog uiShowCustomFormCD = new ContentDialog();
            uiShowCustomFormCD.Content = this;

            uiShowCustomFormCD.PrimaryButtonText = Const.TxLater;
            uiShowCustomFormCD.PrimaryButtonClick += UiShowCustomFormCD_PrimaryButtonClick;

            uiShowCustomFormCD.SecondaryButtonText = Const.Reset;
            uiShowCustomFormCD.SecondaryButtonClick += UiShowCustomFormCD_SecondaryButtonClick;

            uiShowCustomFormCD.CloseButtonText = Const.Close;
            uiShowCustomFormCD.CloseButtonClick += UiShowCustomFormCD_CloseButtonClick;

            tmpObj = new ShowCustomForm();
            tmpObj = (ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<ShowCustomForm>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ShowCustomForm);
                tmpObj = (ShowCustomForm)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getInfo() != null)
                    InfoTextBox.Text = tmpObj.getInfo();
                else
                {
                    InfoTextBox.IsEnabled = false;
                    InfoCheckBox.IsChecked = false;
                }

                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiShowCustomFormCD.ShowAsync();
        }

        private void UiShowCustomFormCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            String info = null ;
            if (InfoCheckBox.IsChecked == true)
            {
                info = InfoTextBox.Text;                
            }
            RpcResponse rpcResponse = BuildRpc.buildUiShowCustomFormResponse(
                BuildRpc.getNextId(), resultCode, info);

            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void UiShowCustomFormCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiShowCustomFormCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void InfoCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InfoTextBox.IsEnabled = (bool)InfoCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
