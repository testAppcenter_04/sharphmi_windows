﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutGoingNotifications;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIOnLanguageChange : UserControl
    {
        OnLanguageChange tmpObj;

        public UIOnLanguageChange()
        {
            this.InitializeComponent();

            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Language));
            LanguageComboBox.SelectedIndex = 0;
        }

        public async void ShowOnLanguageChange()
        {
            ContentDialog uiOnLanguageChangeCD = new ContentDialog();
            uiOnLanguageChangeCD.Content = this;

            uiOnLanguageChangeCD.PrimaryButtonText = Const.TxNow;
            uiOnLanguageChangeCD.PrimaryButtonClick += UiOnLanguageChangeCD_PrimaryButtonClick;

            uiOnLanguageChangeCD.SecondaryButtonText = Const.Reset;
            uiOnLanguageChangeCD.SecondaryButtonClick += UiOnLanguageChangeCD_SecondaryButtonClick;

            uiOnLanguageChangeCD.CloseButtonText = Const.Close;
            uiOnLanguageChangeCD.CloseButtonClick += UiOnLanguageChangeCD_CloseButtonClick;

            tmpObj = new OnLanguageChange();
            tmpObj = (OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<OnLanguageChange>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnLanguageChange);
                tmpObj = (OnLanguageChange)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getLanguage() != null)
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else
                {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }
            }

            await uiOnLanguageChangeCD.ShowAsync();
        }

        private void UiOnLanguageChangeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Language? language = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                language = (HmiApiLib.Common.Enums.Language)LanguageComboBox.SelectedIndex;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildUIOnLanguageChange(language);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void UiOnLanguageChangeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiOnLanguageChangeCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }
    }
}
