﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class ScreenParams : UserControl
    {
        public CheckBox imageResolutionCheckBox;
        public CheckBox resolutionWidthCheckBox;
        public TextBox resolutionWidthTextBox;
        public CheckBox resolutionHeightCheckBox;
        public TextBox resolutionHeightTextBox;
        public CheckBox touchEventCapabilityCheckBox;
        public CheckBox pressAvailableCheckBox;
        public CheckBox multiTouchAvailableCheckBox;
        public CheckBox doublePressAvailableCheckBox;
        public ToggleSwitch pressAvailableToggle;
        public ToggleSwitch multiTouchAvailableToggle;
        public ToggleSwitch doublePressAvailableToggle;

        public ScreenParams(HmiApiLib.Common.Structs.ScreenParams scrParams)
        {
            InitializeComponent();

            imageResolutionCheckBox = ImageResolutionCheckBox;
            resolutionWidthCheckBox = ResolutionWidthCheckBox;
            resolutionWidthTextBox = ResolutionWidthTextBox;
            resolutionHeightCheckBox = ResolutionHeightCheckBox;
            resolutionHeightTextBox = ResolutionHeightTextBox;
            touchEventCapabilityCheckBox = TouchEventCapabilitiyCheckBox;
            pressAvailableCheckBox = PressAvailableCheckBox;
            multiTouchAvailableCheckBox = MultiTouchAvailableCheckBox;
            doublePressAvailableCheckBox = DoublePressAvailableCheckBox;
            pressAvailableToggle = PressAvailableToggle;
            multiTouchAvailableToggle = MultiTouchAvailableToggle;
            doublePressAvailableToggle = DoublePressAvailableToggle;

            if (scrParams != null)
            {
                if (scrParams.getResolution() != null)
                {
                    ResolutionWidthTextBox.Text = scrParams.getResolution().resolutionWidth.ToString();
                    ResolutionHeightTextBox.Text = scrParams.getResolution().resolutionHeight.ToString();
                }
                else
                {
                    ResolutionWidthCheckBox.IsEnabled = false;
                    ResolutionWidthTextBox.IsEnabled = false;
                    ResolutionHeightCheckBox.IsEnabled = false;
                    ResolutionHeightTextBox.IsEnabled = false;
                    ImageResolutionCheckBox.IsChecked = false;
                }




                if (scrParams.getTouchEventAvailable() == null)
                {
                    TouchEventCapabilitiyCheckBox.IsChecked = false;
                    PressAvailableToggle.IsEnabled = false;
                    MultiTouchAvailableToggle.IsEnabled = false;
                    DoublePressAvailableToggle.IsEnabled = false;
                    PressAvailableCheckBox.IsEnabled = false;
                    MultiTouchAvailableCheckBox.IsEnabled = false;
                    DoublePressAvailableCheckBox.IsEnabled = false;
                }
                else
                {
                    if (scrParams.getTouchEventAvailable().getPressAvailable() == null)
                    {
                        PressAvailableCheckBox.IsChecked = false;
                        PressAvailableToggle.IsEnabled = false;
                    }
                    else
                    {
                        PressAvailableToggle.IsOn = (bool)scrParams.getTouchEventAvailable().getPressAvailable();
                    }
                    if (scrParams.getTouchEventAvailable().getMultiTouchAvailable() == null)
                    {
                        MultiTouchAvailableCheckBox.IsChecked = false;
                        MultiTouchAvailableToggle.IsEnabled = false;
                    }
                    else
                    {
                        MultiTouchAvailableToggle.IsOn = (bool)scrParams.getTouchEventAvailable().getMultiTouchAvailable();
                    }
                    if (scrParams.getTouchEventAvailable().getDoublePressAvailable() == null)
                    {
                        DoublePressAvailableCheckBox.IsChecked = false;
                        DoublePressAvailableToggle.IsEnabled = false;
                    }
                    else
                    {
                        DoublePressAvailableToggle.IsOn = (bool)scrParams.getTouchEventAvailable().getDoublePressAvailable();
                    }
                }
            }
        }

        private void ImageResolutionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ImageResolutionCheckBox.IsChecked == true)
            {
                ResolutionWidthCheckBox.IsEnabled = true;
                ResolutionHeightCheckBox.IsEnabled = true;

                ResolutionWidthTextBox.IsEnabled = (bool)ResolutionWidthCheckBox.IsChecked;
                ResolutionHeightTextBox.IsEnabled = (bool)ResolutionHeightCheckBox.IsChecked;
            }
            else
            {
                ResolutionWidthCheckBox.IsEnabled = false;
                ResolutionWidthTextBox.IsEnabled = false;
                ResolutionHeightCheckBox.IsEnabled = false;
                ResolutionHeightTextBox.IsEnabled = false;
            }
        }

        private void ResolutionWidthCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResolutionWidthTextBox.IsEnabled = (bool)ResolutionWidthCheckBox.IsChecked;
        }

        private void ResolutionHeightCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResolutionHeightTextBox.IsEnabled = (bool)ResolutionHeightCheckBox.IsChecked;
        }

        private void TouchEventCapabilitiyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (TouchEventCapabilitiyCheckBox.IsChecked == true)
            {
                PressAvailableCheckBox.IsEnabled = true;
                MultiTouchAvailableCheckBox.IsEnabled = true;
                DoublePressAvailableCheckBox.IsEnabled = true;
            }
            else
            {
                PressAvailableCheckBox.IsEnabled = false;
                MultiTouchAvailableCheckBox.IsEnabled = false;
                DoublePressAvailableCheckBox.IsEnabled = false;
                MultiTouchAvailableToggle.IsEnabled = false;
                PressAvailableToggle.IsEnabled = false;
                DoublePressAvailableToggle.IsEnabled=false;
            }
        }

        private void PressAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PressAvailableToggle.IsEnabled=(bool) PressAvailableCheckBox.IsChecked ;
        }

        private void MultiTouchAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MultiTouchAvailableToggle.IsEnabled =(bool) MultiTouchAvailableCheckBox.IsChecked;

        }

        private void DoublePressAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DoublePressAvailableToggle.IsEnabled = (bool)DoublePressAvailableCheckBox.IsChecked;

        }
    }
}
