﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UIGetCapabilitiesResponse : UserControl
    {
        ContentDialog getCapabilitiesCD = new ContentDialog();
        public static ContentDialog displayCapabilitiesCD = new ContentDialog();
        GetCapabilities tmpObj = null;
        ObservableCollection<String> softButtonList = new ObservableCollection<string>();
        String SoftButtonCap = "SoftButtonCap ";
        int SoftButtonCapIndex = 0;
        HmiApiLib.Common.Structs.DisplayCapabilities disp = null;
        HmiApiLib.Common.Structs.AudioPassThruCapabilities audioPassThruCapabilities = null;
        List<HmiApiLib.Common.Structs.SoftButtonCapabilities> softButtonCapabilitiesList = null;

        public UIGetCapabilitiesResponse()
        {
            InitializeComponent();

            HmiZoneCapabilitiesComboBox.ItemsSource = Enum.GetNames(typeof(HmiZoneCapabilities));
            HmiZoneCapabilitiesComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetCapabilities()
        {
            getCapabilitiesCD = new ContentDialog();
            getCapabilitiesCD.Content = this;

            getCapabilitiesCD.PrimaryButtonText = Const.TxLater;
            getCapabilitiesCD.PrimaryButtonClick += UiGetCapabilitiesCD_PrimaryButtonClick;

            getCapabilitiesCD.SecondaryButtonText = Const.Reset;
            getCapabilitiesCD.SecondaryButtonClick += UiGetCapabilitiesCD_ResetButtonClick;

            getCapabilitiesCD.CloseButtonText = Const.Close;
            getCapabilitiesCD.CloseButtonClick += UiGetCapabilitiesCD_CloseButtonClick;

            UiGetSoftButtonCapabilitiesList.ItemsSource = softButtonList;

            tmpObj = new GetCapabilities();
            tmpObj = (GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<GetCapabilities>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetCapabilities);
                tmpObj = (GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getDisplayCapabilities() != null)
                    disp = tmpObj.getDisplayCapabilities();
                else {
                    DisplayCapabilitiesButton.IsEnabled = false;
                    DisplayCapabilitiesCheckBox.IsChecked = false;

                }
                if (tmpObj.getAudioPassThruCapabilities() != null)
                    audioPassThruCapabilities = tmpObj.getAudioPassThruCapabilities();
                else {
                    AudioPassThruCapabilitiesButton.IsEnabled = false;
                    AudioPassThruCapabilitiesCheckBox.IsChecked = false;
                }
                if (tmpObj.getHmiZoneCapabilities() != null)
                    HmiZoneCapabilitiesComboBox.SelectedIndex = (int)tmpObj.getHmiZoneCapabilities();
                else {
                    HmiZoneCapabilitiesComboBox.IsEnabled = false;
                    HmiZoneCapabilitiesCheckBox.IsChecked = false;
                }
                if (tmpObj.getHMICapabilities() != null)
                {
                    if(tmpObj.getHMICapabilities().getNavigation() == null)
                    {
                        NavigationCheckBox.IsChecked = false;
                        NavigationToggle.IsEnabled = false;
                    }
                    else
                    {
                        NavigationToggle.IsOn = (bool)tmpObj.getHMICapabilities().getNavigation();
                    }
                    if(tmpObj.getHMICapabilities().getPhoneCall() == null)
                    {
                        PhoneCallCheckBox.IsChecked = false;
                        PhoneCallToggle.IsEnabled = false;
                    }
                    else
                    {
                        PhoneCallToggle.IsOn = (bool)tmpObj.getHMICapabilities().getPhoneCall();
                    }
                }
                else {
                    HMICapabilitiesCheckBox.IsChecked = false;
                    NavigationCheckBox.IsEnabled = false;
                    PhoneCallCheckBox.IsEnabled = false;
                    PhoneCallToggle.IsEnabled = false;
                    NavigationToggle.IsEnabled = false;
                }

                //List<HmiApiLib.Common.Structs.SoftButtonCapabilities> softList = tmpObj.getSoftButtonCapabilities();
                softButtonCapabilitiesList = tmpObj.getSoftButtonCapabilities();

                if (null != softButtonCapabilitiesList)
                {
                    for (int i = 0; i < softButtonCapabilitiesList.Count; i++)
                    {
                        softButtonList.Add(SoftButtonCap + SoftButtonCapIndex);
                        SoftButtonCapIndex++;
                    }
                }
                else {
                    SoftButtonCapabilitiesCheckBox.IsChecked = false;
                    SoftButtonCapabilitiesButton.IsEnabled = false;
                }
            }

            await getCapabilitiesCD.ShowAsync();
        }

        private void UiGetCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }

            if(DisplayCapabilitiesCheckBox.IsChecked != true)
            {
                disp = null;
            }

            if (AudioPassThruCapabilitiesCheckBox.IsChecked != true)
            {
                audioPassThruCapabilities = null;
            }

            HmiZoneCapabilities? hmiZoneCapabilities = null;
            if(HmiZoneCapabilitiesCheckBox.IsChecked == true)
            {
                hmiZoneCapabilities = (HmiZoneCapabilities)HmiZoneCapabilitiesComboBox.SelectedIndex;
            }
                        
            HMICapabilities hMICapabilities =  new HMICapabilities();


            if (HMICapabilitiesCheckBox.IsChecked == true)
            {
                hMICapabilities.phoneCall = (bool)PhoneCallToggle.IsOn;
                hMICapabilities.navigation = (bool)NavigationToggle.IsOn;
            }
            else
            {
                //hMICapabilities.phoneCall = null;
                //hMICapabilities.navigation = null;
                hMICapabilities = null;
            }

            if (SoftButtonCapabilitiesCheckBox.IsChecked != true)
            {
                softButtonCapabilitiesList = null;
            }

            HmiApiLib.Base.RpcResponse rpcMessage = BuildRpc.buildUiGetCapabilitiesResponse(BuildRpc.getNextId(), resultCode, disp, 
                audioPassThruCapabilities, hmiZoneCapabilities, softButtonCapabilitiesList, hMICapabilities, null);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void UiGetCapabilitiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiGetCapabilitiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private async void DisplayCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            getCapabilitiesCD.Hide();

            displayCapabilitiesCD = new ContentDialog();

            var displayCapabilities = new DisplayCapabilities(disp, true);
            displayCapabilitiesCD.Content = displayCapabilities;

            displayCapabilitiesCD.PrimaryButtonText = Const.OK;
            displayCapabilitiesCD.PrimaryButtonClick += DisplayCapabilitiesCD_PrimaryButtonClick;

            displayCapabilitiesCD.SecondaryButtonText = Const.Close;
            displayCapabilitiesCD.SecondaryButtonClick += DisplayCapabilitiesCD_SecondaryButtonClick;

            await displayCapabilitiesCD.ShowAsync();
        }

        private async void DisplayCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }

        private async void DisplayCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var displayCapabilities = sender.Content as DisplayCapabilities;
            disp = new HmiApiLib.Common.Structs.DisplayCapabilities();
            if (displayCapabilities.displayTypeCheckBox.IsChecked == true)
            {
                disp.displayType = (DisplayType)displayCapabilities.displayTypeComboBox.SelectedIndex;
            }
            if (displayCapabilities.addTextFieldsCheckBox.IsChecked == true)
            {
                ObservableCollection<TextField> txtField = displayCapabilities.textFieldList;
                List<TextField> textFieldList = new List<TextField>();
                textFieldList.AddRange(txtField);
                disp.textFields = textFieldList;
            }
            if (displayCapabilities.addImageFieldsCheckBox.IsChecked == true)
            {
                ObservableCollection<ImageField> imgField = displayCapabilities.imageFieldList;
                List<ImageField> imgFieldList = new List<ImageField>();
                imgFieldList.AddRange(imgField);
                disp.imageFields = imgFieldList;
            }
            if (displayCapabilities.addMediaClockFormatCheckBox.IsChecked == true)
            {
                ObservableCollection<HmiApiLib.Common.Enums.MediaClockFormat> mList = displayCapabilities.mediaClockList;
                List<HmiApiLib.Common.Enums.MediaClockFormat> mediaList = new List<HmiApiLib.Common.Enums.MediaClockFormat>();
                mediaList.AddRange(mList);
                disp.mediaClockFormats = mediaList;
            }
            if (displayCapabilities.imageTypeCheckBox.IsChecked == true)
            {
                ObservableCollection<HmiApiLib.Common.Enums.ImageType> mList = displayCapabilities.imageTypeList;
                List<HmiApiLib.Common.Enums.ImageType> imgList = new List<HmiApiLib.Common.Enums.ImageType>();
                imgList.AddRange(mList);
                disp.imageCapabilities = imgList;
            }
            if (displayCapabilities.graphicSupportedCheckBox.IsChecked == true)
            {
                disp.graphicSupported = displayCapabilities.graphicSupportedToggle.IsOn;
            }
            if (displayCapabilities.templateAvailable.IsChecked == true)
            {
                String tempString = displayCapabilities.templateAvailableTextBox.Text.ToString();
                String[] tempArray = tempString.Split(",");
                List<String> templateList = new List<string>();
                templateList.AddRange(tempArray);
                disp.templatesAvailable = templateList;
            }
            if(displayCapabilities.screenParamsCheckBox.IsChecked == true)
            {
                disp.screenParams = displayCapabilities.screenParams;
            }
            if (displayCapabilities.numCustomPresetsAvailableCheckBox.IsChecked == true)
            {
                try
                {
                    disp.numCustomPresetsAvailable = Int32.Parse(displayCapabilities.numCustomPresetsAvailableTB.Text.ToString());
                }
                catch (Exception e)
                {

                }
            }
            
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }


        private async void AudioPassThruTapped(object sender, TappedRoutedEventArgs e)
        {
            getCapabilitiesCD.Hide();

            ContentDialog audioPassThruCD = new ContentDialog();

            var audioPassThru = new AudioPassThruCapabilities(audioPassThruCapabilities);
            audioPassThruCD.Content = audioPassThru;

            audioPassThruCD.PrimaryButtonText = Const.OK;
            audioPassThruCD.PrimaryButtonClick += AudioPassThruCD_PrimaryButtonClick;

            audioPassThruCD.SecondaryButtonText = Const.Close;
            audioPassThruCD.SecondaryButtonClick += AudioPassThruCD_SecondaryButtonClick;

            await audioPassThruCD.ShowAsync();
        }

        private async void AudioPassThruCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }

        private async void AudioPassThruCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var audioPassThru = sender.Content as AudioPassThruCapabilities;
            audioPassThruCapabilities = new HmiApiLib.Common.Structs.AudioPassThruCapabilities();
            if (audioPassThru.samplingRateCheckBox.IsChecked == true)
                audioPassThruCapabilities.samplingRate = (SamplingRate)audioPassThru.samplingRateComboBox.SelectedIndex;
            if (audioPassThru.bitsPerSampleCheckBox.IsChecked == true)
                audioPassThruCapabilities.bitsPerSample = (BitsPerSample)audioPassThru.bitsPerSampleComboBox.SelectedIndex;
            if (audioPassThru.audioTypeCheckBox.IsChecked == true)
                audioPassThruCapabilities.audioType = (AudioType)audioPassThru.audioTypeComboBox.SelectedIndex;
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }


        private async void SoftButtonCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            getCapabilitiesCD.Hide();

            ContentDialog softButtonCapabilitiesCD = new ContentDialog();

            var softButtonCapabilities = new SoftButtonCapabilities();
            softButtonCapabilitiesCD.Content = softButtonCapabilities;

            softButtonCapabilitiesCD.PrimaryButtonText = Const.OK;
            softButtonCapabilitiesCD.PrimaryButtonClick += SoftButtonCapabilitiesCD_PrimaryButtonClick;

            softButtonCapabilitiesCD.SecondaryButtonText = Const.Close;
            softButtonCapabilitiesCD.SecondaryButtonClick += SoftButtonCapabilitiesCD_SecondaryButtonClick;

            await softButtonCapabilitiesCD.ShowAsync();
        }

        private async void SoftButtonCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }

        private async void SoftButtonCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var softButtonCapabilities = sender.Content as SoftButtonCapabilities;
            softButtonCapabilitiesList = new List<HmiApiLib.Common.Structs.SoftButtonCapabilities>();
            HmiApiLib.Common.Structs.SoftButtonCapabilities softBtnCap = new HmiApiLib.Common.Structs.SoftButtonCapabilities();
            softBtnCap.shortPressAvailable = (bool)softButtonCapabilities.shortPressAvailableCB.IsChecked;
            softBtnCap.longPressAvailable = (bool)softButtonCapabilities.longPressAvailableCB.IsChecked;
            softBtnCap.upDownAvailable = (bool)softButtonCapabilities.upDownAvailableCB.IsChecked;
            softBtnCap.imageSupported = (bool)softButtonCapabilities.imageSupportedCB.IsChecked;
            if (softBtnCap.shortPressAvailable != false || softBtnCap.longPressAvailable != false || softBtnCap.upDownAvailable != false || softBtnCap.imageSupported != false)
            {
                softButtonList.Add(SoftButtonCap + SoftButtonCapIndex);
                SoftButtonCapIndex++;
                softButtonCapabilitiesList.Add(softBtnCap);
            }
            sender.Hide();
            await getCapabilitiesCD.ShowAsync();
        }


        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void DisplayCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DisplayCapabilitiesButton.IsEnabled = (bool)DisplayCapabilitiesCheckBox.IsChecked;
        }

        private void HmiZoneCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HmiZoneCapabilitiesComboBox.IsEnabled = (bool)HmiZoneCapabilitiesCheckBox.IsChecked;
        }

        private void HMICapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NavigationCheckBox.IsEnabled = (bool)HMICapabilitiesCheckBox.IsChecked;
            PhoneCallCheckBox.IsEnabled = (bool)HMICapabilitiesCheckBox.IsChecked;
            PhoneCallToggle.IsEnabled = (bool)PhoneCallCheckBox.IsChecked && (bool)HMICapabilitiesCheckBox.IsChecked;
            NavigationToggle.IsEnabled = (bool)NavigationCheckBox.IsChecked && (bool)HMICapabilitiesCheckBox.IsChecked;
        }

        private void SoftButtonCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SoftButtonCapabilitiesButton.IsEnabled = (bool)SoftButtonCapabilitiesCheckBox.IsChecked;
        }

        private void AudioPassThruCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AudioPassThruCapabilitiesButton.IsEnabled = (bool)AudioPassThruCapabilitiesCheckBox.IsChecked;
        }

        private void NavigationCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NavigationToggle.IsEnabled = (bool)NavigationCheckBox.IsChecked;
        }

        private void PhoneCallCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PhoneCallToggle.IsEnabled = (bool)PhoneCallCheckBox.IsChecked;
        }
    }
}