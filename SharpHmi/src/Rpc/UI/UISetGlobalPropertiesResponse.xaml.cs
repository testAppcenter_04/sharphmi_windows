﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.UI.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.UI
{
    public sealed partial class UISetGlobalPropertiesResponse : UserControl
    {
        SetGlobalProperties tmpObj;
        public UISetGlobalPropertiesResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSetGlobalProperties()
        {
            ContentDialog uiSetGlobalPropertiesCD = new ContentDialog();
            uiSetGlobalPropertiesCD.Content = this;

            uiSetGlobalPropertiesCD.PrimaryButtonText = Const.TxLater;
            uiSetGlobalPropertiesCD.PrimaryButtonClick += UiSetGlobalPropertiesCD_PrimaryButtonClick;

            uiSetGlobalPropertiesCD.SecondaryButtonText = Const.Reset;
            uiSetGlobalPropertiesCD.SecondaryButtonClick += UiSetGlobalPropertiesCD_SecondaryButtonClick;

            uiSetGlobalPropertiesCD.CloseButtonText = Const.Close;
            uiSetGlobalPropertiesCD.CloseButtonClick += UiSetGlobalPropertiesCD_CloseButtonClick;

            tmpObj = new SetGlobalProperties();
            tmpObj = (SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<SetGlobalProperties>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SetGlobalProperties);
                tmpObj = (SetGlobalProperties)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await uiSetGlobalPropertiesCD.ShowAsync();
        }

        private void UiSetGlobalPropertiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildUiSetGlobalPropertiesResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse); ;
        }

        private void UiSetGlobalPropertiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void UiSetGlobalPropertiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
