﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.SDL.OutgoingRequests;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLGetListOfPermissionsResponse : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        GetListOfPermissions tmpObj = null;
        public SDLGetListOfPermissionsResponse()
        {
            InitializeComponent();
            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowGetListOfPermissions()
        {
            ContentDialog sdlGetListOfPermissionsCD = new ContentDialog();
            sdlGetListOfPermissionsCD.Content = this;

            sdlGetListOfPermissionsCD.PrimaryButtonText = Const.TxLater;
            sdlGetListOfPermissionsCD.PrimaryButtonClick += SdlGetListOfPermissionsCD_PrimaryButtonClick;

            sdlGetListOfPermissionsCD.SecondaryButtonText = Const.Reset;
            sdlGetListOfPermissionsCD.SecondaryButtonClick += SdlGetListOfPermissionsCD_ResetButtonClick;

            sdlGetListOfPermissionsCD.CloseButtonText = Const.Close;
            sdlGetListOfPermissionsCD.CloseButtonClick += SdlGetListOfPermissionsCD_CloseButtonClick;

            tmpObj = new GetListOfPermissions();
            tmpObj = (GetListOfPermissions)AppUtils.getSavedPreferenceValueForRpc<GetListOfPermissions>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else
                {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }
            }

            await sdlGetListOfPermissionsCD.ShowAsync();
        }

        private void SdlGetListOfPermissionsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {
                         selectedAppID = null;

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }
            RpcRequest rpcMessage = BuildRpc.buildSDLGetListOfPermissionsRequest(BuildRpc.getNextId(), selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void SdlGetListOfPermissionsCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }                                   
        }

        private void SdlGetListOfPermissionsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdTextBox != null)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdComboBox != null)
            {
                if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    RegisteredAppIdComboBox.IsEnabled = true;
                    ManualAppIdTextBox.IsEnabled = false;
                }
                else
                {
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = true;

                }
            }
        }
    }
}
