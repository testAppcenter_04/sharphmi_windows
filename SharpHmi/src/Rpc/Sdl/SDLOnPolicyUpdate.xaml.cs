﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLOnPolicyUpdate : UserControl
    {
        public SDLOnPolicyUpdate()
        {
            InitializeComponent();
        }

        public async void ShowOnPolicyUpdate()
        {
            var sdlOnPolicyUpdate = new SDLOnPolicyUpdate();

            ContentDialog sdlOnPolicyUpdateCD = new ContentDialog();
            sdlOnPolicyUpdateCD.Content = sdlOnPolicyUpdate;

            sdlOnPolicyUpdateCD.PrimaryButtonText = Const.TxNow;
            sdlOnPolicyUpdateCD.PrimaryButtonClick += SdlOnPolicyUpdateCD_PrimaryButtonClick;

            sdlOnPolicyUpdateCD.SecondaryButtonText = Const.Reset;
            sdlOnPolicyUpdateCD.SecondaryButtonClick += SdlOnPolicyUpdateCD_SecondaryButtonClick;

            sdlOnPolicyUpdateCD.CloseButtonText = Const.Close;
            sdlOnPolicyUpdateCD.CloseButtonClick += SdlOnPolicyUpdateCD_CloseButtonClick;

            await sdlOnPolicyUpdateCD.ShowAsync();
        }

        private void SdlOnPolicyUpdateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnPolicyUpdate();
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void SdlOnPolicyUpdateCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SdlOnPolicyUpdateCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
