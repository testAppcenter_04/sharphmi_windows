﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.SDL.OutgoingRequests;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLGetUserFriendlyMessageResponse : UserControl
    {
        GetUserFriendlyMessage tmpObj = null;
        public SDLGetUserFriendlyMessageResponse()
        {
            InitializeComponent();
            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;
        }

        public async void ShowGetUserFriendlyMessage()
        {
            ContentDialog sdlGetUserFriendlyMessageCD = new ContentDialog();
            sdlGetUserFriendlyMessageCD.Content = this;

            sdlGetUserFriendlyMessageCD.PrimaryButtonText = Const.TxLater;
            sdlGetUserFriendlyMessageCD.PrimaryButtonClick += SdlGetUserFriendlyMessageCD_PrimaryButtonClick;

            sdlGetUserFriendlyMessageCD.SecondaryButtonText = Const.Reset;
            sdlGetUserFriendlyMessageCD.SecondaryButtonClick += SdlGetUserFriendlyMessageCD_ResetButtonClick;

            sdlGetUserFriendlyMessageCD.CloseButtonText = Const.Close;
            sdlGetUserFriendlyMessageCD.CloseButtonClick += SdlGetUserFriendlyMessageCD_ResetButtonClick;

            tmpObj = new GetUserFriendlyMessage();
            tmpObj = (GetUserFriendlyMessage)AppUtils.getSavedPreferenceValueForRpc<GetUserFriendlyMessage>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (null != tmpObj.getMessageCodes())
                {
                    List<String> messageDataList = tmpObj.getMessageCodes();
                    int count = messageDataList.Count;
                    String message = "";
                    for (int i = 0; i < count; i++)
                    {
                        if (i == count - 1)
                            message = message + messageDataList[i];
                        else
                            message = message + messageDataList[i] + ",";
                    }
                    MessageCodeTextBox.Text = message;
                }
                else {
                    MessageCodeCheckBox.IsChecked = false;
                    MessageCodeTextBox.IsEnabled = false;
                }

                if (null != tmpObj.getLanguage())
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }
            }

            await sdlGetUserFriendlyMessageCD.ShowAsync();
        }

        private void SdlGetUserFriendlyMessageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            List<String> messageList = null;
            Language? selectedLang = null;
            if (MessageCodeCheckBox.IsChecked == true)
            {
                messageList = new List<string>();
                if (null != MessageCodeTextBox.Text)
                    messageList.AddRange(MessageCodeTextBox.Text.ToString().Split(','));
            }
            if (LanguageCheckBox.IsChecked == true)
            {
                selectedLang = (Language)LanguageComboBox.SelectedIndex;
            }
            RpcRequest rpcMessage = BuildRpc.buildSDLGetUserFriendlyMessageRequest(BuildRpc.getNextId(), messageList, selectedLang);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void SdlGetUserFriendlyMessageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SdlGetUserFriendlyMessageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void MessageCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MessageCodeTextBox.IsEnabled = (bool)MessageCodeCheckBox.IsChecked;
        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }
    }
}
