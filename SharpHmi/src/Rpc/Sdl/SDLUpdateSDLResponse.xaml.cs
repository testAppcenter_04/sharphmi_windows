﻿using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLUpdateSDLResponse : UserControl
    {
        public SDLUpdateSDLResponse()
        {
            InitializeComponent();
        }

        public async void ShowUpdateSDL()
        {
            ContentDialog sdlUpdateSDLCD = new ContentDialog();
            sdlUpdateSDLCD.Content = this;

            sdlUpdateSDLCD.PrimaryButtonText = Const.TxLater;
            sdlUpdateSDLCD.PrimaryButtonClick += SdlUpdateSDLCD_PrimaryButtonClick;

            sdlUpdateSDLCD.SecondaryButtonText = Const.Reset;
            sdlUpdateSDLCD.SecondaryButtonClick += SdlUpdateSDLCD_ResetButtonClick;

            sdlUpdateSDLCD.CloseButtonText = Const.Close;
            sdlUpdateSDLCD.CloseButtonClick += SdlUpdateSDLCD_CloseButtonClick;

            await sdlUpdateSDLCD.ShowAsync();
        }

        private void SdlUpdateSDLCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SdlUpdateSDLCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SdlUpdateSDLCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
