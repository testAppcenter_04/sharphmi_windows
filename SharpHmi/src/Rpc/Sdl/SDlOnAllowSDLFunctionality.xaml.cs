﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.SDL.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDlOnAllowSDLFunctionality : UserControl
    {
        OnAllowSDLFunctionality tmpObj = null;
        public SDlOnAllowSDLFunctionality()
        {
            InitializeComponent();

            TransportTypeComboBox.ItemsSource = Enum.GetNames(typeof(TransportType));
            TransportTypeComboBox.SelectedIndex = 0;

            ConsentedSourceComboBox.ItemsSource = Enum.GetNames(typeof(ConsentSource));
            ConsentedSourceComboBox.SelectedIndex = 0;
        }

        public async void ShowOnAllowSDLFunctionality()
        {
            ContentDialog sdlOnAllowSDLFunctionalityCd = new ContentDialog();
            sdlOnAllowSDLFunctionalityCd.Content = this;

            sdlOnAllowSDLFunctionalityCd.PrimaryButtonText = Const.TxNow;
            sdlOnAllowSDLFunctionalityCd.PrimaryButtonClick += SdlOnAllowSDLFunctionalityCd_PrimaryButtonClick;

            sdlOnAllowSDLFunctionalityCd.SecondaryButtonText = Const.Reset;
            sdlOnAllowSDLFunctionalityCd.SecondaryButtonClick += SdlOnAllowSDLFunctionalityCd_ResetButtonClick;

            sdlOnAllowSDLFunctionalityCd.CloseButtonText = Const.Close;
            sdlOnAllowSDLFunctionalityCd.CloseButtonClick += SdlOnAllowSDLFunctionalityCd_CloseButtonClick;

            tmpObj = new OnAllowSDLFunctionality();
            tmpObj = (OnAllowSDLFunctionality)AppUtils.getSavedPreferenceValueForRpc<OnAllowSDLFunctionality>(tmpObj.getMethod());
            if (null != tmpObj)
            {
                if (null != tmpObj.getDevice())
                {
                    if (null != tmpObj.getDevice().getName())
                    {
                        DeviceNameTextBox.Text = tmpObj.getDevice().getName();
                    }
                    else
                    {
                        DeviceNameCheckBox.IsChecked = false;
                        DeviceNameTextBox.IsEnabled = false;
                    }

                    if (null != tmpObj.getDevice().getId())
                    {
                        DeviceIdTextBox.Text = tmpObj.getDevice().getId();
                    }
                    else
                    {
                        DeviceIdTextBox.IsEnabled = false;
                        DeviceIdCheckBox.IsChecked = false;

                    }
                    if (tmpObj.getDevice().getTransportType() != null)
                        TransportTypeComboBox.SelectedIndex = (int)tmpObj.getDevice().getTransportType();
                    else
                    {
                        TransportTypeComboBox.IsEnabled = false;
                        TransportTypeCheckBox.IsChecked = false;
                    }

                    if (tmpObj.getDevice().getIsSDLAllowed() == null)
                    {
                        IsSDLAllowedCheckBox.IsChecked = false;
                        IsSDLAllowedTgl.IsEnabled = false;
                    }
                    else
                    {
                        IsSDLAllowedTgl.IsOn = (bool)tmpObj.getDevice().getIsSDLAllowed();
                    }

                }
                else {
                    DeviceInfoCheckBox.IsChecked = false;
                    DeviceNameTextBox.IsEnabled = false;
                    DeviceIdTextBox.IsEnabled = false;
                    TransportTypeComboBox.IsEnabled = false;
                    DeviceNameCheckBox.IsEnabled = false;
                    DeviceIdCheckBox.IsEnabled = false;
                    TransportTypeCheckBox.IsEnabled = false;
                    IsSDLAllowedCheckBox.IsEnabled = false;
                    IsSDLAllowedTgl.IsEnabled = false;
                }
              
                if (tmpObj.getAllowed() == null)
                {
                    AllowCheckBox.IsChecked = false;
                    AllowTgl.IsEnabled = false;
                }
                else
                {
                    AllowTgl.IsOn = (bool)tmpObj.getAllowed();
                }
                if (null != tmpObj.getSource())
                    ConsentedSourceComboBox.SelectedIndex = (int)tmpObj.getSource();
                else
                {
                    ConsentedSourceComboBox.IsEnabled = false;
                    ConsentedSourceCheckBox.IsChecked = false;
                }
            }

            await sdlOnAllowSDLFunctionalityCd.ShowAsync();
        }

        private void SdlOnAllowSDLFunctionalityCd_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            DeviceInfo devInfo = null;
            if (DeviceInfoCheckBox.IsChecked == true)
            {
                devInfo = new DeviceInfo();
                if (DeviceNameCheckBox.IsChecked == true)
                {
                    if (null != DeviceNameTextBox.Text)
                        devInfo.name = DeviceNameTextBox.Text.ToString();
                }
                if (DeviceIdCheckBox.IsChecked == true)
                {
                    if (null != DeviceIdTextBox.Text)
                        devInfo.id = DeviceIdTextBox.Text.ToString();
                }
                if (TransportTypeCheckBox.IsChecked == true)
                {
                    devInfo.transportType = (TransportType)TransportTypeComboBox.SelectedIndex;
                }
                if (true == IsSDLAllowedCheckBox.IsChecked)
                    devInfo.isSDLAllowed = IsSDLAllowedTgl.IsOn;
            }

            ConsentSource? source = null;
            if (ConsentedSourceCheckBox.IsChecked == true)
            {
                source = (ConsentSource)ConsentedSourceComboBox.SelectedIndex;
            }

            bool? allowed = null;
            if (AllowCheckBox.IsChecked == true)
            {
                allowed = AllowTgl.IsOn;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAllowSDLFunctionality(devInfo, allowed, source);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void SdlOnAllowSDLFunctionalityCd_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SdlOnAllowSDLFunctionalityCd_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void DeviceInfoCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceNameCheckBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked;
            DeviceIdCheckBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked;
            TransportTypeCheckBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked;
            IsSDLAllowedCheckBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked;

            if (DeviceInfoCheckBox.IsChecked == true)
            {
                if (DeviceNameCheckBox.IsChecked == true)
                    DeviceNameTextBox.IsEnabled = true;
                else
                    DeviceNameTextBox.IsEnabled = false;

                if (DeviceIdCheckBox.IsChecked == true)
                    DeviceIdTextBox.IsEnabled = true;
                else
                    DeviceIdTextBox.IsEnabled = false;

                if (TransportTypeCheckBox.IsChecked == true)
                    TransportTypeComboBox.IsEnabled = true;
                else
                    TransportTypeComboBox.IsEnabled = false;
                if (IsSDLAllowedCheckBox.IsChecked == true)
                    IsSDLAllowedTgl.IsEnabled = true;
                else
                    IsSDLAllowedTgl.IsEnabled = false;
            }
            else
            {
                DeviceNameTextBox.IsEnabled = false;
                DeviceIdTextBox.IsEnabled = false;
                TransportTypeComboBox.IsEnabled = false;
                IsSDLAllowedTgl.IsEnabled = false;

            }

        }

        private void DeviceNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceNameTextBox.IsEnabled = (bool)DeviceNameCheckBox.IsChecked;
        }

        private void DeviceIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceIdTextBox.IsEnabled = (bool)DeviceIdCheckBox.IsChecked;
        }

        private void TransportTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TransportTypeComboBox.IsEnabled = (bool)TransportTypeCheckBox.IsChecked;
        }

        private void ConsentedSourceCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ConsentedSourceComboBox.IsEnabled = (bool)ConsentedSourceCheckBox.IsChecked;
        }

        private void AllowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowTgl.IsEnabled = (bool)AllowCheckBox.IsChecked;
        }

        private void IsSDLAllowedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IsSDLAllowedTgl.IsEnabled= (bool)IsSDLAllowedCheckBox.IsChecked;
        }
    }
}
