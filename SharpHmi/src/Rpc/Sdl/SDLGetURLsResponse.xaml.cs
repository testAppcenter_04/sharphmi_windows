﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.SDL.OutgoingRequests;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLGetURLsResponse : UserControl
    {
        GetURLS tmpObj = null;
        public SDLGetURLsResponse()
        {
            InitializeComponent();
        }

        public async void ShowGetURLs()
        {
            ContentDialog sdlGetURLsCD = new ContentDialog();
            sdlGetURLsCD.Content = this;

            sdlGetURLsCD.PrimaryButtonText = Const.TxLater;
            sdlGetURLsCD.PrimaryButtonClick += SdlGetURLsCD_PrimaryButtonClick;

            sdlGetURLsCD.SecondaryButtonText = Const.Reset;
            sdlGetURLsCD.SecondaryButtonClick += SdlGetURLsCD_ResetButtonClick;

            sdlGetURLsCD.CloseButtonText = Const.Close;
            sdlGetURLsCD.CloseButtonClick += SdlGetURLsCD_CloseButtonClick;

            tmpObj = new GetURLS();
            tmpObj = (GetURLS)AppUtils.getSavedPreferenceValueForRpc<GetURLS>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (null != tmpObj.getService())
                    ServiceTextBox.Text = tmpObj.getService().ToString();
                else
                {
                    ServiceTextBox.IsEnabled = false;
                    ServiceCheckBox.IsChecked = false;
                }

            }

            await sdlGetURLsCD.ShowAsync();
        }

        private void SdlGetURLsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? service = null;
            if (ServiceCheckBox.IsChecked == true)
                try
                {
                    service = Int32.Parse(ServiceTextBox.Text.ToString());
                }
                catch (Exception e)
                {

                }
            RpcRequest rpcMessage = BuildRpc.buildSDLGetURLSRequest(BuildRpc.getNextId(), service);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void SdlGetURLsCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SdlGetURLsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ServiceCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ServiceTextBox.IsEnabled = (bool)ServiceCheckBox.IsChecked;
        }
    }
}
