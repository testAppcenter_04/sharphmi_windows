﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class AddPermissionItem : UserControl
    {
        public CheckBox nameCheckBox;
        public TextBox nameTextBox;
        public CheckBox idCheckBox;
        public TextBox idTextBox;
        public CheckBox allowCheckBox;

        public AddPermissionItem()
        {
            InitializeComponent();

            nameCheckBox = NameCheckBox;
            nameTextBox = NameTextBox;
            idCheckBox = IDCheckBox;
            idTextBox = IDTextBox;
            allowCheckBox = AllowCheckBox;
        }

        private void NameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            NameTextBox.IsEnabled = (bool)NameCheckBox.IsChecked;
        }

        private void IDCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IDTextBox.IsEnabled = (bool)IDCheckBox.IsChecked;
        }

        private void AllowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowToggle.IsEnabled = (bool)AllowCheckBox.IsChecked;

        }
    }
}
