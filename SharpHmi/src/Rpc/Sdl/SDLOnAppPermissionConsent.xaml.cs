﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.SDL.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLOnAppPermissionConsent : UserControl
    {
        ContentDialog sdlOnAppPermissionConsentCD = new ContentDialog();
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        public ObservableCollection<PermissionItem> permissionItemList = new ObservableCollection<PermissionItem>();
        OnAppPermissionConsent tmpObj = null;

        public SDLOnAppPermissionConsent()
        {
            InitializeComponent();
            SourceComboBox.ItemsSource = Enum.GetNames(typeof(ConsentSource));
            SourceComboBox.SelectedIndex = 0;

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnAppPermissionConsent()
        {
            sdlOnAppPermissionConsentCD = new ContentDialog();

            //var sdlOnAppPermissionConsent = new AddPermissionItem();
            sdlOnAppPermissionConsentCD.Content = this;

            sdlOnAppPermissionConsentCD.PrimaryButtonText = Const.TxNow;
            sdlOnAppPermissionConsentCD.PrimaryButtonClick += SdlOnAppPermissionConsentCD_PrimaryButtonClick;

            sdlOnAppPermissionConsentCD.SecondaryButtonText = Const.Reset;
            sdlOnAppPermissionConsentCD.SecondaryButtonClick += SdlOnAppPermissionConsentCD_ResetButtonClick;

            sdlOnAppPermissionConsentCD.CloseButtonText = Const.Close;
            sdlOnAppPermissionConsentCD.CloseButtonClick += SdlOnAppPermissionConsentCD_CloseButtonClick;

            tmpObj = new OnAppPermissionConsent();
            tmpObj = (OnAppPermissionConsent)AppUtils.getSavedPreferenceValueForRpc<OnAppPermissionConsent>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else
                {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getConsentedFunctions())
                {
                    List<PermissionItem> list = tmpObj.getConsentedFunctions();
                    if (null != list)
                    {
                        foreach (PermissionItem p in list)
                        {
                            permissionItemList.Add(p);
                        }
                    }
                }
                else {
                    AppPermissionItemCheckBox.IsChecked = false;
                    AppPermissionItemButton.IsEnabled = false;
                }


                if (null != tmpObj.getSource())
                {
                    SourceComboBox.SelectedIndex = (int)tmpObj.getSource();
                }
                else
                {
                    SourceComboBox.IsEnabled = false;
                    SourceCheckBox.IsChecked = false;
                }
            }

            SDLAppPermissionItemListView.ItemsSource = permissionItemList;

            await sdlOnAppPermissionConsentCD.ShowAsync();
        }

        private void SdlOnAppPermissionConsentCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch
                    {
                        selectedAppID = 0;
                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }
            ConsentSource? consent = null;
            if (SourceCheckBox.IsChecked == true)
            {
                consent = (ConsentSource)SourceComboBox.SelectedIndex;
            }
            List<PermissionItem> permissionList = null;
            if (AppPermissionItemCheckBox.IsChecked == true)
            {
                permissionList = new List<PermissionItem>();
                permissionList.AddRange(permissionItemList);
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildSDLOnAppPermissionConsent(selectedAppID, permissionList, consent);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void SdlOnAppPermissionConsentCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SdlOnAppPermissionConsentCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdTextBox != null)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdComboBox != null)
            {
                if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    RegisteredAppIdComboBox.IsEnabled = true;
                    ManualAppIdTextBox.IsEnabled = false;
                }
                else
                {
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = true;
                }
            }
        }

        private void SourceCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SourceComboBox.IsEnabled = (bool)SourceCheckBox.IsChecked;
        }

        private void AppPermissionItemCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AppPermissionItemButton.IsEnabled = (bool)AppPermissionItemCheckBox.IsChecked;
        }

        private async void AppPermissionItemTapped(object sender, TappedRoutedEventArgs e)
        {
            sdlOnAppPermissionConsentCD.Hide();

            ContentDialog addOnAppPermissionConsentCD = new ContentDialog();

            var addOnAppPermissionConsent = new AddPermissionItem();
            addOnAppPermissionConsentCD.Content = addOnAppPermissionConsent;

            addOnAppPermissionConsentCD.PrimaryButtonText = Const.OK;
            addOnAppPermissionConsentCD.PrimaryButtonClick += AddOnAppPermissionConsentCD_PrimaryButtonClick;

            addOnAppPermissionConsentCD.SecondaryButtonText = Const.Close;
            addOnAppPermissionConsentCD.SecondaryButtonClick += AddOnAppPermissionConsentCD_SecondaryButtonClick;

            await addOnAppPermissionConsentCD.ShowAsync();
        }

        private async void AddOnAppPermissionConsentCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var permission = sender.Content as AddPermissionItem;
            PermissionItem permissionItem = new PermissionItem();
            if (permission.nameCheckBox.IsChecked == true)
            {
                if (null != permission.nameTextBox.Text)
                    permissionItem.name = permission.nameTextBox.Text.ToString();
            }
            if (permission.idCheckBox.IsChecked == true)
            {
                if (null != permission.idTextBox.Text)
                {
                    try
                    {
                        permissionItem.id = Int32.Parse(permission.idTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            permissionItem.allowed = (bool)permission.allowCheckBox.IsChecked;
            permissionItemList.Add(permissionItem);

            sender.Hide();
            await sdlOnAppPermissionConsentCD.ShowAsync();
        }

        private void AddOnAppPermissionConsentCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
        }
    }
}
