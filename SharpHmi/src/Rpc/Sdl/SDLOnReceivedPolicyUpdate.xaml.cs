﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.SDL.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLOnReceivedPolicyUpdate : UserControl
    {
        OnReceivedPolicyUpdate tmpObj = null;
        public SDLOnReceivedPolicyUpdate()
        {
            InitializeComponent();
        }

        public async void ShowOnReceivedPolicyUpdate()
        {
            ContentDialog sdlOnReceivedPolicyUpdateCD = new ContentDialog();
            sdlOnReceivedPolicyUpdateCD.Content = this;

            sdlOnReceivedPolicyUpdateCD.PrimaryButtonText = Const.TxNow;
            sdlOnReceivedPolicyUpdateCD.PrimaryButtonClick += SdlOnReceivedPolicyUpdateCD_PrimaryButtonClick;

            sdlOnReceivedPolicyUpdateCD.SecondaryButtonText = Const.Reset;
            sdlOnReceivedPolicyUpdateCD.SecondaryButtonClick += SdlOnReceivedPolicyUpdateCD_ResetButtonClick;

            sdlOnReceivedPolicyUpdateCD.CloseButtonText = Const.Close;
            sdlOnReceivedPolicyUpdateCD.CloseButtonClick += SdlOnReceivedPolicyUpdateCD_CloseButtonClick;

            tmpObj = new OnReceivedPolicyUpdate();
            tmpObj = (OnReceivedPolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<OnReceivedPolicyUpdate>(tmpObj.getMethod());
            if (null != tmpObj)
            {
                if (null != tmpObj.getPolicyfile())
                    PolicyFileTextBox.Text = tmpObj.getPolicyfile();
                else
                {
                    PolicyFileTextBox.IsEnabled = false;
                    PolicyFileCheckBox.IsChecked = false;
                }

            }

            await sdlOnReceivedPolicyUpdateCD.ShowAsync();
        }

        private void SdlOnReceivedPolicyUpdateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            String file = null;
            if (PolicyFileCheckBox.IsChecked == true)
            {
                if (null != PolicyFileTextBox.Text)
                    file = PolicyFileTextBox.Text.ToString();
            }
            RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnReceivedPolicyUpdate(file);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
        }

        private void SdlOnReceivedPolicyUpdateCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void SdlOnReceivedPolicyUpdateCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void PolicyFileCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PolicyFileTextBox.IsEnabled = (bool)PolicyFileCheckBox.IsChecked;
        }
    }
}
