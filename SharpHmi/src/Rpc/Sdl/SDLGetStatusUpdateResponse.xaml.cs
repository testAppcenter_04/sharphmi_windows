﻿using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.SDl
{
    public sealed partial class SDLGetStatusUpdateResponse : UserControl
    {
        public SDLGetStatusUpdateResponse()
        {
            InitializeComponent();
        }

        public async void ShowGetStatusUpdate()
        {
            ContentDialog sdlGetStatusUpdateCD = new ContentDialog();
            sdlGetStatusUpdateCD.Content = this;

            sdlGetStatusUpdateCD.PrimaryButtonText = Const.TxLater;
            sdlGetStatusUpdateCD.PrimaryButtonClick += SdlGetStatusUpdateCD_PrimaryButtonClick;

            sdlGetStatusUpdateCD.SecondaryButtonText = Const.Reset;
            sdlGetStatusUpdateCD.SecondaryButtonClick += SdlGetStatusUpdateCD_ResetButtonClick;

            sdlGetStatusUpdateCD.CloseButtonText = Const.Close;
            sdlGetStatusUpdateCD.CloseButtonClick += SdlGetStatusUpdateCD_CloseButtonClick;

            await sdlGetStatusUpdateCD.ShowAsync();
        }

        private void SdlGetStatusUpdateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SdlGetStatusUpdateCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void SdlGetStatusUpdateCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
