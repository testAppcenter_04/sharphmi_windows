﻿using HmiApiLib.Common.Enums;
using SharpHmi.RC;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class LightControlCapabilities : UserControl
    {
        public CheckBox moduleNameCheckBox;
        public TextBox moduleNameTextBox;
        public CheckBox supportedLightsCheckBox;
        
        public ObservableCollection<HmiApiLib.Common.Structs.LightCapabilities> lightCapabilitiesCollection = new ObservableCollection<HmiApiLib.Common.Structs.LightCapabilities>();

        static ContentDialog SupportedLightsCD = new ContentDialog();
        
        public LightControlCapabilities(HmiApiLib.Common.Structs.LightControlCapabilities lightControlCap)
        {
            this.InitializeComponent();
            moduleNameCheckBox = ModuleNameCheckBox;
            moduleNameTextBox = ModuleNameTextBox;
            supportedLightsCheckBox = SupportedLightsCheckBox;

            if (lightControlCap != null)
            {
                if(lightControlCap.getModuleName() != null)
                {
                    ModuleNameTextBox.Text = lightControlCap.getModuleName();
                }
                if(lightControlCap.getSupportedLights() != null)
                {
                    foreach (HmiApiLib.Common.Structs.LightCapabilities cap in lightControlCap.getSupportedLights())
                    {
                        lightCapabilitiesCollection.Add(cap);
                    }
                }
            }
            SupportedLightsListView.ItemsSource = lightCapabilitiesCollection;
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModuleNameTextBox.IsEnabled = (bool)ModuleNameCheckBox.IsChecked;
        }

        private void SupportedLightsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SupportedLightsButton.IsEnabled = (bool)SupportedLightsCheckBox.IsChecked;
            SupportedLightsListView.IsEnabled = (bool)SupportedLightsCheckBox.IsChecked;

        }

        private async void SupportedLightsTapped(object sender, TappedRoutedEventArgs e)
        {
            RCGetCapabilitiesResponse.addLightControlCapabilitiesCD.Hide();

            SupportedLightsCD = new ContentDialog();

            var lightCapabilities = new LightCapabilities();
            SupportedLightsCD.Content = lightCapabilities;

            SupportedLightsCD.PrimaryButtonText = Const.OK;
            SupportedLightsCD.PrimaryButtonClick += SupportedLightsCD_PrimaryButtonClick;

            SupportedLightsCD.SecondaryButtonText = Const.Close;
            SupportedLightsCD.SecondaryButtonClick += SupportedLightsCD_SecondaryButtonClick;

            await SupportedLightsCD.ShowAsync();
        }

        private async void SupportedLightsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var light = sender.Content as LightCapabilities;
            HmiApiLib.Common.Structs.LightCapabilities lightCap = new HmiApiLib.Common.Structs.LightCapabilities();
            
            if (light.lightNameCheckBox.IsChecked == true)
            {
                lightCap.name = (LightName)HmiApiLib.Utils.getEnumValue(typeof(LightName), (String)light.lightNameComboBox.SelectedItem);
            }
            else
            {
                lightCap.name = null;
            }

            if(light.densityAvailableCheckBox.IsChecked == true)
            {
                lightCap.densityAvailable = light.densityAvailableToggleSwitch.IsOn;
            }
            else
            {
                lightCap.densityAvailable = false;
            }

            if(light.sRGBColorSpaceAvailableCheckBox.IsChecked == true)
            {
                lightCap.rgbColorSpaceAvailable = light.sRGBColorSpaceAvailableToggleSwitch.IsOn;
            }
            else
            {
                lightCap.rgbColorSpaceAvailable = false;
            }
            
            lightCapabilitiesCollection.Add(lightCap);

            SupportedLightsCD.Hide();
            await RCGetCapabilitiesResponse.addLightControlCapabilitiesCD.ShowAsync();
        }

        private async void SupportedLightsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            SupportedLightsCD.Hide();
            await RCGetCapabilitiesResponse.addLightControlCapabilitiesCD.ShowAsync();
        }
    }
}
