﻿using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using SharpHmi.RC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class AddAudioControlData : UserControl
    {
        public CheckBox sourceCheckBox;
        public ComboBox sourceComboBox;
        public CheckBox keepContextCheckBox;
        public ToggleSwitch keepContextToggle;
        public CheckBox volumeCheckBox;
        public TextBox volumeTextBox;
        public CheckBox addeqCB;

        public ObservableCollection<HmiApiLib.Common.Structs.EqualizerSettings> eqList = 
            new ObservableCollection<HmiApiLib.Common.Structs.EqualizerSettings>();

        // null for GetInteriorVehicleData
        // true fro SetInteriorVehicleData
        // false for OnInteriorVehicleData
        public bool? isCalledFromGetInerior = null;

        public AddAudioControlData(AudioControlData audioControlData, bool? isCalledFromGetInerior)
        {
            InitializeComponent();

            sourceCheckBox = SourceCheckBox;
            sourceComboBox = SourceComboBox;
            keepContextCheckBox = KeepContextCheckBox;
            volumeCheckBox = VolumeCheckBox;
            volumeTextBox = VolumeTextBox;
            addeqCB = addEqCheckBox;
           keepContextToggle = KeepContextToggle;



            this.isCalledFromGetInerior = isCalledFromGetInerior;

            SourceComboBox.ItemsSource = Enum.GetNames(typeof(PrimaryAudioSource));
            SourceComboBox.SelectedIndex = 0;

            if (null != audioControlData)
            {
                if (null != audioControlData.getSource())
                {
                    SourceComboBox.SelectedIndex = (int)audioControlData.getSource();
                }
                else
                {
                    SourceCheckBox.IsChecked = false;
                    SourceComboBox.IsEnabled = false;
                }

                
                if (audioControlData.getKeepContext() == null)
                {
                    KeepContextCheckBox.IsChecked = false;
                    KeepContextToggle.IsEnabled = false;
                }
                else
                {
                    KeepContextCheckBox.IsChecked = true;
                    KeepContextToggle.IsOn = (bool)audioControlData.getKeepContext();
                }
                if (null != audioControlData.getVolume())
                {
                    VolumeTextBox.Text = audioControlData.getVolume().ToString();
                }
                else
                {
                    VolumeCheckBox.IsChecked = false;
                    VolumeTextBox.IsEnabled = false;
                }

                List<HmiApiLib.Common.Structs.EqualizerSettings> equalizerList = audioControlData.getEqualizerSettings();
                if (null != equalizerList)
                {
                    if (equalizerList.Count > 0)
                    {
                        foreach (HmiApiLib.Common.Structs.EqualizerSettings eq in equalizerList)
                        {
                            eqList.Add(eq);
                        }
                    }
                }
                else
                {
                    addEqCheckBox.IsChecked = false;
                    AddEqSettingButton.IsEnabled = false;
                }
            }
            EqualizerSettingsListView.ItemsSource = eqList;
        }

        private void SourceCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SourceComboBox.IsEnabled = (bool)SourceCheckBox.IsChecked;
        }

        private void VolumeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VolumeTextBox.IsEnabled = (bool)VolumeCheckBox.IsChecked;
        }

        private async void AddEqButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            if (null == isCalledFromGetInerior)
                RCGetInteriorVehicleDataResponse.addAudioControlDataCD.Hide();
            else if (true == isCalledFromGetInerior)
                RCSetInteriorVehicleDataResponse.addAudioControlDataCD.Hide();
            else
                RCOnInteriorVehicleData.addAudioControlDataCD.Hide();

            ContentDialog AudioControlCD = new ContentDialog();

            var addLightStateData = new EqualizerSettings();
            AudioControlCD.Content = addLightStateData;

            AudioControlCD.PrimaryButtonText = "Ok";
            AudioControlCD.PrimaryButtonClick += AddLightStateCD_PrimaryButtonClick;

            AudioControlCD.SecondaryButtonText = "Cancel";
            AudioControlCD.SecondaryButtonClick += AddLightStateCD_SecondaryButtonClick; ;

            await AudioControlCD.ShowAsync();
        }

        private async void AddLightStateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var eq = sender.Content as EqualizerSettings;
            HmiApiLib.Common.Structs.EqualizerSettings eqState = new HmiApiLib.Common.Structs.EqualizerSettings();
            if ((bool)eq.channelIdCheckBox.IsChecked)
            {
                try
                {
                    eqState.channelId = Int32.Parse(eq.channelIdTextbox.Text);
                }
                catch (Exception e) { }
            }
            if ((bool)eq.channelNameCheckBox.IsChecked)
            {
                eqState.channelName = eq.channelNameTextBox.Text;
            }
            if ((bool)eq.channelSettingCheckBox.IsChecked)
            {
                try
                {
                    eqState.channelSetting = Int32.Parse(eq.channelSettingTextBox.Text);
                }
                catch (Exception e) { }
            }
            eqList.Add(eqState);
            sender.Hide();

            if (null == isCalledFromGetInerior)
                await RCGetInteriorVehicleDataResponse.addAudioControlDataCD.ShowAsync();
            else if (true == isCalledFromGetInerior)
                await RCSetInteriorVehicleDataResponse.addAudioControlDataCD.ShowAsync();
            else
                await RCOnInteriorVehicleData.addAudioControlDataCD.ShowAsync();
        }

        private async void AddLightStateCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (null == isCalledFromGetInerior)
                await RCGetInteriorVehicleDataResponse.addAudioControlDataCD.ShowAsync();
            else if (true == isCalledFromGetInerior)
                await RCSetInteriorVehicleDataResponse.addAudioControlDataCD.ShowAsync();
            else
                await RCOnInteriorVehicleData.addAudioControlDataCD.ShowAsync();
        }

        private void AddEqualizerCBTapped(object sender, TappedRoutedEventArgs e)
        {
            AddEqSettingButton.IsEnabled = (bool)addEqCheckBox.IsChecked;
        }
        private void KeepContextCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            KeepContextToggle.IsEnabled = (bool)KeepContextCheckBox.IsChecked;
        }
        
    }
}