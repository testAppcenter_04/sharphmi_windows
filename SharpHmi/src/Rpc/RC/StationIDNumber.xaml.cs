﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class StationIDNumber : UserControl
    {
        public CheckBox countryCodeCheckBox;
        public TextBox countryCodeTextBox;
        public CheckBox fCCFacilityIdCheckBox;
        public TextBox fCCFacilityIdTextBox;


        public StationIDNumber(HmiApiLib.Common.Structs.StationIDNumber stationIDNumber)
        {
            this.InitializeComponent();
            if (stationIDNumber != null)
            {
                if (stationIDNumber.getCountryCode() != null)
                {
                    CountryCodeTextBox.Text = stationIDNumber.getCountryCode().ToString();
                }
                else {
                    CountryCodeTextBox.IsEnabled = false;
                    CountryCodeCheckBox.IsChecked = false;
                }
                if (stationIDNumber.getFccFacilityId() != null)
                {
                    FCCFacilityIdTextBox.Text = stationIDNumber.getFccFacilityId().ToString();
                }
                else
                {
                    FCCFacilityIdTextBox.IsEnabled = false;
                    FCCFacilityIdCheckBox.IsChecked = false;
                }


            }
            countryCodeCheckBox = CountryCodeCheckBox;
            countryCodeTextBox = CountryCodeTextBox;
            fCCFacilityIdCheckBox = FCCFacilityIdCheckBox;
            fCCFacilityIdTextBox = FCCFacilityIdTextBox;
        }

        private void CountryCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CountryCodeTextBox.IsEnabled = (bool)CountryCodeCheckBox.IsChecked;

        }

        private void FCCFacilityIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FCCFacilityIdTextBox.IsEnabled = (bool)FCCFacilityIdCheckBox.IsChecked;

        }
    }
}
