﻿using HmiApiLib.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class ClimateControlData : UserControl
    {
        public CheckBox fanSpeedCB;
        public TextBox fanSpeedTB;

        public CheckBox currentTemperatureCB;
        public CheckBox currentTemperatureUnitCB;
        public ComboBox currentTemperatureUnitComboBox;
        public CheckBox currentTemperatureValueCB;
        public TextBox currentTemperatureValueTextBox;

        public CheckBox desiredTemperatureCB;
        public CheckBox desiredTemperatureUnitCB;
        public ComboBox desiredTemperatureUnitComboBox;
        public CheckBox desiredTemperatureValueCB;
        public TextBox desiredTemperatureValueTextBox;

        public CheckBox acEnableCB;
        public CheckBox circulatedAirEnableCB;
        public CheckBox autoModeEnableCB;

        public ToggleSwitch acEnableTgl;
        public ToggleSwitch circulatedAirEnableTgl;
        public ToggleSwitch autoModeEnableTgl;

        public CheckBox defrostZoneCB;
        public ComboBox defrostZoneComboBox;

        public CheckBox dualModeEnableCB;
        public CheckBox acMaxEnableCB;

        public ToggleSwitch dualModeEnableTgl;
        public ToggleSwitch acMaxEnableTgl;

        public CheckBox ventilationModeCB;
        public ComboBox ventilationModeComboBox;

        public CheckBox heatedSteeringWheelEnableCB;
        public CheckBox heatedRearWindowEnableCB;
        public CheckBox heatedMirrorEnableCB;
        public CheckBox heatedWindshieldEnableCB;

        public ToggleSwitch heatedSteeringWheelEnableTgl;
        public ToggleSwitch heatedRearWindowEnableTgl;
        public ToggleSwitch heatedMirrorEnableTgl;
        public ToggleSwitch heatedWindshieldEnableTgl;

        public ClimateControlData(HmiApiLib.Common.Structs.ClimateControlData climateControlData)
        {
            this.InitializeComponent();

            CurrentTemperatureUnitComboBox.ItemsSource = Enum.GetNames(typeof(TemperatureUnit));
            CurrentTemperatureUnitComboBox.SelectedIndex = 0;

            DesiredTemperatureUnitComboBox.ItemsSource = Enum.GetNames(typeof(TemperatureUnit));
            DesiredTemperatureUnitComboBox.SelectedIndex = 0;

            DefrostZoneComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.DefrostZone));
            DefrostZoneComboBox.SelectedIndex = 0;

            VentillationModeComboBox.ItemsSource = Enum.GetNames(typeof(VentilationMode));
            VentillationModeComboBox.SelectedIndex = 0;


            if (climateControlData.getFanSpeed() != null)
                FanSpeedTextBox.Text = climateControlData.getFanSpeed().ToString();
            else {
                FanSpeedTextBox.IsEnabled = false;
                FanSpeedCheckBox.IsChecked = false;
            }

            if (climateControlData.getCurrentTemperature() != null)
            {
               if(climateControlData.getCurrentTemperature().getUnit()!=null)
                CurrentTemperatureUnitComboBox.SelectedIndex = (int)climateControlData.getCurrentTemperature().getUnit();
                if (climateControlData.getCurrentTemperature().getValue() != null) 
                CurrentTemperatureValueTextBox.Text = climateControlData.getCurrentTemperature().getValue().ToString();
            }
            else {
               
                
                CurrentTemperatureCheckBox.IsChecked = false;
                CurrentTemperatureUnitCheckBox.IsEnabled = false;
                CurrentTemperatureUnitComboBox.IsEnabled = false;

                CurrentTemperatureValueCheckBox.IsEnabled = false;
                CurrentTemperatureValueTextBox.IsEnabled = false;
            }

            if (climateControlData.getDesiredTemperature() != null)
            {
                if (climateControlData.getDesiredTemperature().getUnit() != null)
                 DesiredTemperatureUnitComboBox.SelectedIndex = (int)climateControlData.getDesiredTemperature().getUnit();
                if (climateControlData.getDesiredTemperature().getValue() != null)
                 DesiredTemperatureValueTextBox.Text = climateControlData.getDesiredTemperature().getValue().ToString();
            }
            else {
                DesiredTemperatureCheckBox.IsChecked = false;
                DesiredTemperatureUnitCheckBox.IsEnabled = false;
                DesiredTemperatureUnitComboBox.IsEnabled = false;

                DesiredTemperatureValueCheckBox.IsEnabled = false;
                DesiredTemperatureValueTextBox.IsEnabled = false;
            }
            if (climateControlData.getAcEnable() == null)
            {
                AcEnableCheckBox.IsChecked = false;
                AcEnableToggleSwitch.IsEnabled = false;
            }
            else
            {
                AcEnableCheckBox.IsChecked = true;
                AcEnableToggleSwitch.IsOn = (bool)climateControlData.getAcEnable();
            }
            if (climateControlData.getCirculateAirEnable() == null)
            {
                CirculatedAirEnableCheckBox.IsChecked = false;
                CirculatedAirEnableToggleSwitch.IsEnabled = false;
            }
            else
            {
                CirculatedAirEnableCheckBox.IsChecked = true;
                CirculatedAirEnableToggleSwitch.IsOn = (bool)climateControlData.getCirculateAirEnable();
            }
            if (climateControlData.getAutoModeEnable() == null)
            {
                AutoModeEnableCheckBox.IsChecked = false;
                AutoModeEnableToggleSwitch.IsEnabled = false;
            }
            else
            {
                AutoModeEnableCheckBox.IsChecked = true;
                AutoModeEnableToggleSwitch.IsOn = (bool)climateControlData.getAutoModeEnable();
            }

            if (climateControlData.getDefrostZone() != null)
                DefrostZoneComboBox.SelectedIndex = (int)climateControlData.getDefrostZone();
            else
            {
                DefrostZoneComboBox.IsEnabled = false;
                DefrostZoneCheckBox.IsChecked = false;
            }

            if (climateControlData.getDualModeEnable() == null)
            {
                DualModeEnableCheckBox.IsChecked = false;
                DualModeEnableToggleSwitch.IsEnabled = false;
            }
            else
            {
                DualModeEnableCheckBox.IsChecked = true;
                DualModeEnableToggleSwitch.IsOn = (bool)climateControlData.getDualModeEnable();
            }
            if (climateControlData.getAcMaxEnable() == null)
            {
                ACMaxEnableCheckBox.IsChecked = false;
                ACMaxEnableToggleSwitch.IsEnabled = false;
            }
            else
            {
                ACMaxEnableCheckBox.IsChecked = true;

                ACMaxEnableToggleSwitch.IsOn = (bool)climateControlData.getAcMaxEnable();
            }


            if (climateControlData.getVentilationMode() != null)
                VentillationModeComboBox.SelectedIndex = (int)climateControlData.getVentilationMode();
            else
            {
                VentillationModeComboBox.IsEnabled = false;
                VentillationModeCheckBox.IsChecked = false;

            }
            if (climateControlData.getHeatedMirrorsEnable() != null)
                HeatedMirrorEnableToggleSwitch.IsOn = (bool)climateControlData.getHeatedMirrorsEnable();
            else
            {
                HeatedMirrorEnableToggleSwitch.IsEnabled = false;
                HeatedMirrorsEnableCheckBox.IsChecked = false;

            }
            if (climateControlData.getHeatedRearWindowEnable() != null)
                HeatedRearWindowEnableToggleSwitch.IsOn = (bool)climateControlData.getHeatedRearWindowEnable();
            else
            {
                HeatedRearWindowEnableToggleSwitch.IsEnabled = false;
                HeatedRearWindowEnableCheckBox.IsChecked = false;

            }
            if (climateControlData.getHeatedSteeringWheelEnable() != null)
                HeatedSteeringWheelEnableToggleSwitch.IsOn = (bool)climateControlData.getHeatedSteeringWheelEnable();
            else
            {
                HeatedSteeringWheelEnableToggleSwitch.IsEnabled = false;
                HeatedSteeringWheelEnableCheckBox.IsChecked = false;

            }
            if (climateControlData.getHeatedWindshieldEnable() != null)
                HeatedWindshieldEnableToggleSwitch.IsOn = (bool)climateControlData.getHeatedWindshieldEnable();
            else
            {
                HeatedWindshieldEnableToggleSwitch.IsEnabled = false;
                HeatedWindshieldEnableCheckBox.IsChecked = false;

            }


            fanSpeedCB = FanSpeedCheckBox;
            fanSpeedTB = FanSpeedTextBox;

            currentTemperatureCB = CurrentTemperatureCheckBox;
            currentTemperatureUnitCB = CurrentTemperatureUnitCheckBox;
            currentTemperatureUnitComboBox = CurrentTemperatureUnitComboBox;
            currentTemperatureValueCB = CurrentTemperatureValueCheckBox;
            currentTemperatureValueTextBox = CurrentTemperatureValueTextBox;

            desiredTemperatureCB = DesiredTemperatureCheckBox;
            desiredTemperatureUnitCB = DesiredTemperatureUnitCheckBox;
            desiredTemperatureUnitComboBox = DesiredTemperatureUnitComboBox;
            desiredTemperatureValueCB = DesiredTemperatureValueCheckBox;
            desiredTemperatureValueTextBox = DesiredTemperatureValueTextBox;

            acEnableCB = AcEnableCheckBox;
            circulatedAirEnableCB = CirculatedAirEnableCheckBox;
            autoModeEnableCB = AutoModeEnableCheckBox;

            acEnableTgl = AcEnableToggleSwitch;
            circulatedAirEnableTgl = CirculatedAirEnableToggleSwitch;
            autoModeEnableTgl = AutoModeEnableToggleSwitch;

            defrostZoneCB = DefrostZoneCheckBox;
            defrostZoneComboBox = DefrostZoneComboBox;

            dualModeEnableCB = DualModeEnableCheckBox;
            acMaxEnableCB = ACMaxEnableCheckBox;

            dualModeEnableTgl = DualModeEnableToggleSwitch;
            acMaxEnableTgl = ACMaxEnableToggleSwitch;


            heatedSteeringWheelEnableCB = HeatedSteeringWheelEnableCheckBox;
            heatedRearWindowEnableCB = HeatedRearWindowEnableCheckBox;
            heatedMirrorEnableCB = HeatedMirrorsEnableCheckBox;
            heatedWindshieldEnableCB = HeatedWindshieldEnableCheckBox;

            heatedSteeringWheelEnableTgl= HeatedSteeringWheelEnableToggleSwitch;
            heatedRearWindowEnableTgl= HeatedRearWindowEnableToggleSwitch;
            heatedMirrorEnableTgl= HeatedMirrorEnableToggleSwitch;
            heatedWindshieldEnableTgl= HeatedWindshieldEnableToggleSwitch;




        ventilationModeCB = VentillationModeCheckBox;
            ventilationModeComboBox = VentillationModeComboBox;
    }

        private void FanSpeedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (FanSpeedCheckBox.IsChecked == true)
                FanSpeedTextBox.IsEnabled = true;
            else
                FanSpeedTextBox.IsEnabled = false;
        }

        private void CurrentTemperatureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (CurrentTemperatureCheckBox.IsChecked == true)
            {
                CurrentTemperatureUnitCheckBox.IsEnabled = true;

                if (CurrentTemperatureUnitCheckBox.IsChecked == true)
                    CurrentTemperatureUnitComboBox.IsEnabled = true;
                else
                    CurrentTemperatureUnitComboBox.IsEnabled = false;


                CurrentTemperatureValueCheckBox.IsEnabled = true;

                if (CurrentTemperatureValueCheckBox.IsChecked == true)
                    CurrentTemperatureValueTextBox.IsEnabled = true;
                else
                    CurrentTemperatureValueTextBox.IsEnabled = false;
            }
            else
            {
                CurrentTemperatureUnitCheckBox.IsEnabled = false;
                CurrentTemperatureUnitComboBox.IsEnabled = false;

                CurrentTemperatureValueCheckBox.IsEnabled = false;
                CurrentTemperatureValueTextBox.IsEnabled = false;
            }
        }

        private void CurrentTemperatureUnitCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (CurrentTemperatureUnitCheckBox.IsChecked == true)
                CurrentTemperatureUnitComboBox.IsEnabled = true;
            else
                CurrentTemperatureUnitComboBox.IsEnabled = false;
        }

        private void CurrentTemperatureValueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (CurrentTemperatureValueCheckBox.IsChecked == true)
                CurrentTemperatureValueTextBox.IsEnabled = true;
            else
                CurrentTemperatureValueTextBox.IsEnabled = false;
        }

        private void DesiredTemperatureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (DesiredTemperatureCheckBox.IsChecked == true)
            {
                DesiredTemperatureUnitCheckBox.IsEnabled = true;

                if (DesiredTemperatureUnitCheckBox.IsChecked == true)
                    DesiredTemperatureUnitComboBox.IsEnabled = true;
                else
                    DesiredTemperatureUnitComboBox.IsEnabled = false;


                DesiredTemperatureValueCheckBox.IsEnabled = true;

                if (DesiredTemperatureValueCheckBox.IsChecked == true)
                    DesiredTemperatureValueTextBox.IsEnabled = true;
                else
                    DesiredTemperatureValueTextBox.IsEnabled = false;
            }
            else
            {
                DesiredTemperatureUnitCheckBox.IsEnabled = false;
                DesiredTemperatureUnitComboBox.IsEnabled = false;

                DesiredTemperatureValueCheckBox.IsEnabled = false;
                DesiredTemperatureValueTextBox.IsEnabled = false;
            }
        }

        private void DesiredTemperatureUnitCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (DesiredTemperatureUnitCheckBox.IsChecked == true)
                DesiredTemperatureUnitComboBox.IsEnabled = true;
            else
                DesiredTemperatureUnitComboBox.IsEnabled = false;
        }

        private void DesiredTemperatureValueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (DesiredTemperatureValueCheckBox.IsChecked == true)
                DesiredTemperatureValueTextBox.IsEnabled = true;
            else
                DesiredTemperatureValueTextBox.IsEnabled = false;
        }

        private void DefrostZoneCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (DefrostZoneCheckBox.IsChecked == true)
                DefrostZoneComboBox.IsEnabled = true;
            else
                DefrostZoneComboBox.IsEnabled = false;
        }

        private void VentillationModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (VentillationModeCheckBox.IsChecked == true)
                VentillationModeComboBox.IsEnabled = true;
            else
                VentillationModeComboBox.IsEnabled = false;
        }

        private void HeatedSteeringWheelEnableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedSteeringWheelEnableToggleSwitch.IsEnabled = (bool)HeatedSteeringWheelEnableCheckBox.IsChecked;

        }

        private void HeatedWindshieldEnableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedWindshieldEnableToggleSwitch.IsEnabled = (bool)HeatedWindshieldEnableCheckBox.IsChecked;

        }

        private void HeatedRearWindowEnableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedRearWindowEnableToggleSwitch.IsEnabled = (bool)HeatedRearWindowEnableCheckBox.IsChecked;

        }

        private void HeatedMirrorsEnableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedMirrorEnableToggleSwitch.IsEnabled = (bool)HeatedMirrorsEnableCheckBox.IsChecked;

        }
        private void AcEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AcEnableToggleSwitch.IsEnabled = (bool)AcEnableCheckBox.IsChecked;

        }
        private void CirculatedAirEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CirculatedAirEnableToggleSwitch.IsEnabled = (bool)CirculatedAirEnableCheckBox.IsChecked;

        }
        private void AutoModeEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AutoModeEnableToggleSwitch.IsEnabled = (bool)AutoModeEnableCheckBox.IsChecked;

        }
        private void DualModeEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DualModeEnableToggleSwitch.IsEnabled = (bool)DualModeEnableCheckBox.IsChecked;


        }
        private void ACMaxEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ACMaxEnableToggleSwitch.IsEnabled = (bool)ACMaxEnableCheckBox.IsChecked;

        }
    }
}
