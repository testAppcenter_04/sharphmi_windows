﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class MassageCushionFirmness : UserControl
    {
        public CheckBox cushionCheckBox;
        public ComboBox cushionComboBox;

        public CheckBox firmnessCheckBox;
        public TextBox firmnessTextBox;

        public MassageCushionFirmness()
        {
            this.InitializeComponent();

            cushionCheckBox = CushionCheckBox;
            cushionComboBox = CushionComboBox;

            firmnessCheckBox = FirmnessCheckBox;
            firmnessTextBox = FirmnessTextBox;

            cushionComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.MassageCushion));
            CushionComboBox.SelectedIndex = 0;
        }

        private void CushionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CushionComboBox.IsEnabled = (bool)cushionCheckBox.IsChecked;
        }

        private void FirmnessCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FirmnessTextBox.IsEnabled = (bool)FirmnessCheckBox.IsChecked;
        }
    }
}
