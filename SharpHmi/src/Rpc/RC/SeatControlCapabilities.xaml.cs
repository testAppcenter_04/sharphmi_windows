﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class SeatControlCapabilities : UserControl
    {
        public CheckBox moduleNameCheckBox;
        public TextBox moduleNameTextBox;

        public CheckBox heaCheckBox;
        public ToggleSwitch heaToggleSwitch;

        public CheckBox ceaCheckBox;
        public ToggleSwitch ceaToggleSwitch;

        public CheckBox hlaCheckBox;
        public ToggleSwitch hlaToggleSwitch;

        public CheckBox claCheckBox;
        public ToggleSwitch claToggleSwitch;

        public CheckBox hpaCheckBox;
        public ToggleSwitch hpaToggleSwitch;

        public CheckBox vpaCheckBox;
        public ToggleSwitch vpaToggleSwitch;

        public CheckBox fvpaEnabledCheckBox;
        public ToggleSwitch fvpaToggleSwitch;

        public CheckBox bvpaCheckBox;
        public ToggleSwitch bvpaToggleSwitch;

        public CheckBox btaaCheckBox;
        public ToggleSwitch btaaToggleSwitch;

        public CheckBox hshpaCheckBox;
        public ToggleSwitch hshpaToggleSwitch;

        public CheckBox hsvpaCheckBox;
        public ToggleSwitch hsvpaToggleSwitch;

        public CheckBox meaCheckBox;
        public ToggleSwitch meaToggleSwitch;

        public CheckBox mmaCheckBox;
        public ToggleSwitch mmaToggleSwitch;

        public CheckBox mcfaCheckBox;
        public ToggleSwitch mcfaToggleSwitch;

        public CheckBox memoryAvailableCheckBox;
        public ToggleSwitch memoryAvailableToggleSwitch;
        

        public SeatControlCapabilities()
        {
            this.InitializeComponent();


            moduleNameCheckBox = ModuleNameCheckBox;
            moduleNameTextBox = ModuleNameTextBox;

            heaCheckBox = HEACheckBox;
            heaToggleSwitch = HEAToggleSwitch;

            ceaCheckBox = CEACheckBox;
            ceaToggleSwitch = CEAToggleSwitch;

            hlaCheckBox = HLACheckBox;
            hlaToggleSwitch = HLAToggleSwitch;

            claCheckBox = CLACheckBox;
            claToggleSwitch = CLAToggleSwitch;

            hpaCheckBox = HPACheckBox;
            hpaToggleSwitch = HPAToggleSwitch;

            vpaCheckBox = VPACheckBox;
            vpaToggleSwitch = VPAToggleSwitch;

            fvpaEnabledCheckBox = FVPAEnabledCheckBox;
            fvpaToggleSwitch = FVPAToggleSwitch;

            bvpaCheckBox = BVPACheckBox;
            bvpaToggleSwitch = BVPAToggleSwitch;

            btaaCheckBox = BTAACheckBox;
            btaaToggleSwitch = BTAAToggleSwitch;

            hshpaCheckBox = HSHPACheckBox;
            hshpaToggleSwitch = HSHPAToggleSwitch;

            hsvpaCheckBox = HSVPACheckBox;
            hsvpaToggleSwitch = HSVPAToggleSwitch;

            meaCheckBox = MEACheckBox;
            meaToggleSwitch = MEAToggleSwitch;

            mmaCheckBox = MMACheckBox;
            mmaToggleSwitch = MMAToggleSwitch;

            mcfaCheckBox = MCFACheckBox;
            mcfaToggleSwitch = MCFAToggleSwitch;

            memoryAvailableCheckBox = MemoryAvailableCheckBox;
            memoryAvailableToggleSwitch = MemoryAvailableToggleSwitch;
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModuleNameTextBox.IsEnabled = (bool)ModuleNameCheckBox.IsChecked;
        }

        private void HEACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HEAToggleSwitch.IsEnabled = (bool)HEACheckBox.IsChecked;
        }

        private void CEACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CEAToggleSwitch.IsEnabled = (bool)CEACheckBox.IsChecked;
        }

        private void HLACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HLAToggleSwitch.IsEnabled = (bool)HLACheckBox.IsChecked;
        }

        private void CLACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CLAToggleSwitch.IsEnabled = (bool)CLACheckBox.IsChecked;
        }

        private void HPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HPAToggleSwitch.IsEnabled = (bool)HPACheckBox.IsChecked;
        }

        private void VPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VPAToggleSwitch.IsEnabled = (bool)VPACheckBox.IsChecked;
        }

        private void FVPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FVPAToggleSwitch.IsEnabled = (bool)FVPAEnabledCheckBox.IsChecked;
        }

        private void BVPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BVPAToggleSwitch.IsEnabled = (bool)BVPACheckBox.IsChecked;
        }

        private void BTAACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BTAAToggleSwitch.IsEnabled = (bool)BTAACheckBox.IsChecked;
        }

        private void HSHPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HSHPAToggleSwitch.IsEnabled = (bool)HSHPACheckBox.IsChecked;
        }

        private void HSVPACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HSVPAToggleSwitch.IsEnabled = (bool)HSVPACheckBox.IsChecked;
        }

        private void MEACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MEAToggleSwitch.IsEnabled = (bool)MEACheckBox.IsChecked;
        }

        private void MMACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MMAToggleSwitch.IsEnabled = (bool)MMACheckBox.IsChecked;
        }

        private void MCFACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MCFAToggleSwitch.IsEnabled = (bool)MCFACheckBox.IsChecked;
        }

        private void MemoryAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MemoryAvailableToggleSwitch.IsEnabled = (bool)MemoryAvailableCheckBox.IsChecked;
        }
    }
}
