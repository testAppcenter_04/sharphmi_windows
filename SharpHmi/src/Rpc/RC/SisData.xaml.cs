﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class SisData : UserControl
    {
        public CheckBox stationShortNameCheckBox;
        public TextBox stationShortNameTextBox;

        public CheckBox stationIDNumberCheckBox;
        public Button stationIDNumberButton;

        public CheckBox stationLongNameCheckBox;
        public TextBox stationLongNameTextBox;

        public CheckBox stationLocationCheckBox;
        public Button stationLocationButton;
        public HmiApiLib.Common.Structs.StationIDNumber stationIDNumber;
        public HmiApiLib.Common.Structs.GPSData gpsData;

        public CheckBox stationMessageCheckBox;
        public TextBox stationMessageTextBox;
        static ContentDialog stationIdNumberCD= new ContentDialog();
        static ContentDialog stationLocationCD = new ContentDialog();

        public SisData(HmiApiLib.Common.Structs.SisData sisData)
        {
            this.InitializeComponent();
            if (sisData != null) {
                if (sisData.getStationShortName() != null)
                {
                    StationShortNameTextBox.Text = sisData.getStationShortName();
                }
                else
                {
                    StationShortNameCheckBox.IsChecked = false;
                    StationShortNameTextBox.IsEnabled = false;
                }


                if (sisData.getStationIDNumber() != null)
                {
                    stationIDNumber= sisData.getStationIDNumber();
                }
                else
                {
                    StationIDNumberCheckBox.IsChecked = false;
                    StationIDNumberButton.IsEnabled = false;
                }
                if (sisData.getStationLocation() != null)
                {
                    gpsData = sisData.getStationLocation();
                }
                else
                {
                    StationLocationCheckBox.IsChecked = false;
                    StationLocationButton.IsEnabled = false;
                }

                if (sisData.getStationLongName() != null)
                {
                    StationLongNameTextBox.Text = sisData.getStationLongName();
                }
                else
                {
                    StationLongNameCheckBox.IsChecked = false;
                    StationLongNameTextBox.IsEnabled = false;
                }
                if (sisData.getStationMessage() != null)
                {
                    StationMessageTextBox.Text = sisData.getStationMessage();
                }
                else
                {
                    StationMessageCheckBox.IsChecked = false;
                    StationMessageTextBox.IsEnabled = false;
                }
            }
            stationShortNameCheckBox = StationShortNameCheckBox;
            stationShortNameTextBox = StationShortNameTextBox;
            stationIDNumberCheckBox = StationIDNumberCheckBox;
            stationIDNumberButton = StationIDNumberButton;
            stationLongNameCheckBox = StationLongNameCheckBox;
            stationLongNameTextBox = StationLongNameTextBox;
            stationLocationCheckBox = StationLocationCheckBox;
            stationLocationButton = StationLocationButton;
            stationMessageCheckBox = StationMessageCheckBox;
            stationMessageTextBox = StationMessageTextBox;
        }

        private void StationShortNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StationShortNameTextBox.IsEnabled = (bool)StationShortNameCheckBox.IsChecked;

        }

        private void StationIDNumberCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StationIDNumberButton.IsEnabled = (bool)StationIDNumberCheckBox.IsChecked;


        }

        private async void StationIDNumberTapped(object sender, TappedRoutedEventArgs e)
        {
            SharpHmi.RC.RadioControlData.sisDataCD.Hide();
            stationIdNumberCD = new ContentDialog();

            var stationIdNumber = new StationIDNumber(stationIDNumber);

            stationIdNumberCD.Content = stationIdNumber;
            stationIdNumberCD.PrimaryButtonText = "Ok";
            stationIdNumberCD.PrimaryButtonClick += StationIdNumberCD_PrimaryButtonClick;

            stationIdNumberCD.SecondaryButtonText = "Cancel";
            stationIdNumberCD.SecondaryButtonClick += StationIdNumberCD_SecondaryButtonClick;

            await stationIdNumberCD.ShowAsync();
        }
        private async void StationIdNumberCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await SharpHmi.RC.RadioControlData.sisDataCD.ShowAsync();
            
        }
        private async void StationIdNumberCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var mmd = sender.Content as src.Rpc.RC.StationIDNumber;
            stationIDNumber = new HmiApiLib.Common.Structs.StationIDNumber();
            if (mmd.countryCodeCheckBox.IsChecked == true)

                stationIDNumber.countryCode = Int32.Parse(mmd.countryCodeTextBox.Text);

            if (mmd.fCCFacilityIdCheckBox.IsChecked == true)
            {
                stationIDNumber.fccFacilityId = Int32.Parse(mmd.fCCFacilityIdTextBox.Text);

                
            }
            sender.Hide();
            await SharpHmi.RC.RadioControlData.sisDataCD.ShowAsync();
        }
            private void StationLongNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StationLongNameTextBox.IsEnabled = (bool)StationLongNameCheckBox.IsChecked;
            
        }

        private void StationLocationCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StationLocationButton.IsEnabled = (bool)StationLocationCheckBox.IsChecked;

        }

        private async void StationLocationTapped(object sender, TappedRoutedEventArgs e)
        {
            SharpHmi.RC.RadioControlData.sisDataCD.Hide();
            stationLocationCD = new ContentDialog();

            var stationLocation = new src.Rpc.RC.GPSData(gpsData);

            stationLocationCD.Content = stationLocation;
            stationLocationCD.PrimaryButtonText = "Ok";
            stationLocationCD.PrimaryButtonClick += StationLocationCD_PrimaryButtonClick;

            stationLocationCD.SecondaryButtonText = "Cancel";
            stationLocationCD.SecondaryButtonClick += StationLocationCD_SecondaryButtonClick;

            await stationLocationCD.ShowAsync();
        }

        private async void StationLocationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var mmd = sender.Content as src.Rpc.RC.GPSData;
            gpsData = new HmiApiLib.Common.Structs.GPSData();
            if (mmd.latitudeDegreesCheckBox.IsChecked == true)

                gpsData.latitudeDegrees = Int32.Parse(mmd.latitudeDegreesTextBox.Text);

            if (mmd.longitudeDegreesCheckBox.IsChecked == true)
            {
                gpsData.longitudeDegrees = Int32.Parse(mmd.longitudeDegreesTextBox.Text);


            }
            if (mmd.altitudeMetersIdCheckBox.IsChecked == true)
            {
                gpsData.altitude = Int32.Parse(mmd.altitudeMetersIdTextBox.Text);


            }
            sender.Hide();
            await SharpHmi.RC.RadioControlData.sisDataCD.ShowAsync();
        }
        private async void StationLocationCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await SharpHmi.RC.RadioControlData.sisDataCD.ShowAsync();

        }


        private void StationMessageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StationMessageTextBox.IsEnabled = (bool)StationMessageCheckBox.IsChecked;

        }
    }
}
