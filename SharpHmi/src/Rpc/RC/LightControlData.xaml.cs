﻿using SharpHmi.RC;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class LightControlData : UserControl
    {
        public CheckBox lightStateCheckBox;
        public ListView lightStateList;

        public ObservableCollection<HmiApiLib.Common.Structs.LightState> LightStateCollection = new ObservableCollection<HmiApiLib.Common.Structs.LightState>();

        static ContentDialog LightStateCD = new ContentDialog();

        // null for GetInteriorVehicleData
        // true fro SetInteriorVehicleData
        // false for OnInteriorVehicleData
        public bool? isCalledFromGetInerior = null;

        public LightControlData(HmiApiLib.Common.Structs.LightControlData lightControlData, bool? isCalledFromGetInerior)
        {
            InitializeComponent();

            lightStateCheckBox = LightStateCheckBox;

            this.isCalledFromGetInerior = isCalledFromGetInerior;

            if (null != lightControlData && null != lightControlData.getLightState())
            {
                foreach (HmiApiLib.Common.Structs.LightState ls in lightControlData.getLightState())
                {
                    LightStateCollection.Add(ls);
                }
            }
            else {
                LightStateCheckBox.IsChecked = false;
                LightStateListView.IsEnabled = false;
                AddLightStateButton.IsEnabled = false;
            }
            LightStateListView.ItemsSource = LightStateCollection;
        }

        private async void LightStateTapped(object sender, TappedRoutedEventArgs e)
        {
            if (null == isCalledFromGetInerior)
                RCGetInteriorVehicleDataResponse.addLightControlDataCD.Hide();
            else if (true == isCalledFromGetInerior)
                RCSetInteriorVehicleDataResponse.addLightControlDataCD.Hide();
            else
                RCOnInteriorVehicleData.addLightControlDataCD.Hide();

            LightStateCD = new ContentDialog();

            var addLightStateData = new LightState();
            LightStateCD.Content = addLightStateData;

            LightStateCD.PrimaryButtonText = "Ok";
            LightStateCD.PrimaryButtonClick += AddLightStateCD_PrimaryButtonClick;

            LightStateCD.SecondaryButtonText = "Cancel";
            LightStateCD.SecondaryButtonClick += AddLightStateCD_SecondaryButtonClick; ;

            await LightStateCD.ShowAsync();
        }

        private async void AddLightStateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLightState = sender.Content as LightState;
            HmiApiLib.Common.Structs.LightState lightState = new HmiApiLib.Common.Structs.LightState();
            if ((bool)addLightState.lightNameCheckBox.IsChecked)
            {
                lightState.id = (HmiApiLib.Common.Enums.LightName)addLightState.lightNameComboBox.SelectedIndex;
            }
            if ((bool)addLightState.lightStatusCheckBox.IsChecked)
            {
                lightState.status = (HmiApiLib.Common.Enums.LightStatus)addLightState.lightStatusComboBox.SelectedIndex;
            }
            if ((bool)addLightState.densityCheckBox.IsChecked)
            {
                try
                {
                    lightState.density = float.Parse(addLightState.densityTextBox.Text);
                }
                catch (Exception e) { }
            }
            if ((bool)addLightState.rgbColorCheckBox.IsChecked)
            {
                HmiApiLib.Common.Structs.RGBColor sRGBColor = new HmiApiLib.Common.Structs.RGBColor();
                if ((bool)addLightState.redCheckBox.IsChecked)
                {
                    try
                    {
                        sRGBColor.red = Int32.Parse(addLightState.redTextBox.Text);
                    }
                    catch (Exception e) { }
                }
                if ((bool)addLightState.greenCheckBox.IsChecked)
                {
                    try
                    {
                        sRGBColor.green = Int32.Parse(addLightState.greenTextBox.Text);
                    }
                    catch (Exception e) { }
                }
                if ((bool)addLightState.blueCheckBox.IsChecked)
                {
                    try
                    {
                        sRGBColor.blue = Int32.Parse(addLightState.blueTextBox.Text);
                    }
                    catch (Exception e) { }
                }
                lightState.color = sRGBColor;
            }
            LightStateCollection.Add(lightState);
            sender.Hide();

            if (null == isCalledFromGetInerior)
                await RCGetInteriorVehicleDataResponse.addLightControlDataCD.ShowAsync();
            else if (true == isCalledFromGetInerior)
                await RCSetInteriorVehicleDataResponse.addLightControlDataCD.ShowAsync();
            else
                await RCOnInteriorVehicleData.addLightControlDataCD.ShowAsync();
        }

        private async void AddLightStateCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (null == isCalledFromGetInerior)
                await RCGetInteriorVehicleDataResponse.addLightControlDataCD.ShowAsync();
            else if (true == isCalledFromGetInerior)
                await RCSetInteriorVehicleDataResponse.addLightControlDataCD.ShowAsync();
            else
                await RCOnInteriorVehicleData.addLightControlDataCD.ShowAsync();
        }

        private void LightStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddLightStateButton.IsEnabled = (bool)LightStateCheckBox.IsChecked;
        }
    }
}
