﻿using HmiApiLib.Common.Enums;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class HMISettingsControlData : UserControl
    {
        public CheckBox displayModeCheckBox;
        public ComboBox displayModeComboBox;

        public CheckBox temperatureUnitCheckBox;
        public ComboBox temperatureUnitComboBox;

        public CheckBox distanceUnitCheckBox;
        public ComboBox distanceUnitComboBox;

        public HMISettingsControlData(HmiApiLib.Common.Structs.HMISettingsControlData hmiSettingsControlData)
        {
            InitializeComponent();

            displayModeCheckBox = DisplayModeCheckBox;
            displayModeComboBox = DisplayModeComboBox;

            temperatureUnitCheckBox = TemperatureUnitCheckBox;
            temperatureUnitComboBox = TemperatureUnitComboBox;

            distanceUnitCheckBox = DistanceUnitCheckBox;
            distanceUnitComboBox = DistanceUnitComboBox;


            DisplayModeComboBox.ItemsSource = Enum.GetNames(typeof(DisplayMode));
            DisplayModeComboBox.SelectedIndex = 0;

            TemperatureUnitComboBox.ItemsSource = Enum.GetNames(typeof(TemperatureUnit));
            TemperatureUnitComboBox.SelectedIndex = 0;

            DistanceUnitComboBox.ItemsSource = Enum.GetNames(typeof(DistanceUnit));
            DistanceUnitComboBox.SelectedIndex = 0;

            if (null != hmiSettingsControlData)
            {
                if (null != hmiSettingsControlData.getDisplayMode())
                {
                    DisplayModeComboBox.SelectedIndex = (int) hmiSettingsControlData.getDisplayMode();
                }
                else
                {
                    DisplayModeCheckBox.IsChecked = false;
                    DisplayModeComboBox.IsEnabled = false;
                }

                if (null != hmiSettingsControlData.getTemperatureUnit())
                {
                    TemperatureUnitComboBox.SelectedIndex = (int)hmiSettingsControlData.getTemperatureUnit();
                }
                else
                {
                    TemperatureUnitCheckBox.IsChecked = false;
                    TemperatureUnitComboBox.IsEnabled = false;
                }

                if (null != hmiSettingsControlData.getDistanceUnit())
                {
                    DistanceUnitComboBox.SelectedIndex = (int)hmiSettingsControlData.getDistanceUnit();
                }
                else
                {
                    DistanceUnitCheckBox.IsChecked = false;
                    DistanceUnitComboBox.IsEnabled = false;
                }
            }
        }

        private void DisplayModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DisplayModeComboBox.IsEnabled = (bool)DisplayModeCheckBox.IsChecked;
        }

        private void TemperatureUnitCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TemperatureUnitComboBox.IsEnabled = (bool)TemperatureUnitCheckBox.IsChecked;
        }

        private void DistanceUnitCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DistanceUnitComboBox.IsEnabled = (bool)DistanceUnitCheckBox.IsChecked;
        }
    }
}
