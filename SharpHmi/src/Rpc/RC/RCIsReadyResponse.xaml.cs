﻿using HmiApiLib.Builder;
using HmiApiLib.Controllers.RC.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RCIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;
        public RCIsReadyResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            ContentDialog rcIsReadyCD = new ContentDialog();

           // var rcIsReady = new RCIsReadyResponse();
            rcIsReadyCD.Content = this;

            rcIsReadyCD.PrimaryButtonText = Const.TxLater;
            rcIsReadyCD.PrimaryButtonClick += RcIsReadyCD_PrimaryButtonClick;

            rcIsReadyCD.SecondaryButtonText = Const.Reset;
            rcIsReadyCD.SecondaryButtonClick += RcIsReadyCD_ResetButtonClick;

            rcIsReadyCD.CloseButtonText = Const.Close;
            rcIsReadyCD.CloseButtonClick += RcIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());

            if (tmpObj == null)
            {
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(typeof(IsReady), 0);
            }
            if(tmpObj != null)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableToggle.IsEnabled = false;
                }
                else
                {
                    AvailableCheckBox.IsChecked = true;
                    AvailableToggle.IsOn = (bool)tmpObj.getAvailable();
                }
            }

            await rcIsReadyCD.ShowAsync();
        }

        private void RcIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            // var rcIsReadyCD = sender.Content as RCIsReadyResponse;
            HmiApiLib.Common.Enums.Result? result = null;

            if(ResultCodeCheckBox.IsChecked == true)
            {
                result = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex; ;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableToggle.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.RC, toggle, result);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void RcIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void RcIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }
        
        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableToggle.IsEnabled = (bool)AvailableCheckBox.IsChecked;
        }
    }
}
