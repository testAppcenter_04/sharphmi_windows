﻿using HmiApiLib.Builder;
using HmiApiLib.Controllers.RC.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RCOnRemoteControlSettings : UserControl
    {
        OnRemoteControlSettings tmpObj = null;
        public RCOnRemoteControlSettings()
        {
            this.InitializeComponent();
            RCAccessModeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.RCAccessMode));
            RCAccessModeComboBox.SelectedIndex = 0;
        }

        public async void ShowOnRemoteControlSettings()
        {
            ContentDialog rcOnRemoteControlSettingsCD = new ContentDialog();
            
            rcOnRemoteControlSettingsCD.Content = this;

            rcOnRemoteControlSettingsCD.PrimaryButtonText = Const.TxNow;
            rcOnRemoteControlSettingsCD.PrimaryButtonClick += RcOnRemoteControlSettingsCD_PrimaryButtonClick;

            rcOnRemoteControlSettingsCD.SecondaryButtonText = Const.Reset;
            rcOnRemoteControlSettingsCD.SecondaryButtonClick += RcOnRemoteControlSettingsCD_SecondaryButtonClick;


            rcOnRemoteControlSettingsCD.CloseButtonText = Const.Close;
            rcOnRemoteControlSettingsCD.CloseButtonClick += RcOnRemoteControlSettingsCD_CloseButtonClick;

            tmpObj = new OnRemoteControlSettings();
            tmpObj = (OnRemoteControlSettings)AppUtils.getSavedPreferenceValueForRpc<OnRemoteControlSettings>(tmpObj.getMethod());

            if (tmpObj == null) 
            {
                tmpObj = (OnRemoteControlSettings)BuildDefaults.buildDefaultMessage(typeof(OnRemoteControlSettings), 0);
            }
            if (tmpObj != null)
            {
                if(tmpObj.getAccessMode() != null)
                {
                    RCAccessModeComboBox.SelectedIndex = (int)tmpObj.getAccessMode();
                }
                if (tmpObj.getAllowed() == null)
                {
                    AllowedCheckBox.IsChecked = false;
                    AllowToggle.IsEnabled = false;
                }
                else
                {
                    AllowedCheckBox.IsChecked = true;
                    AllowToggle.IsOn = (bool)tmpObj.getAllowed();
                }

            }

            await rcOnRemoteControlSettingsCD.ShowAsync();
        }

        private void RcOnRemoteControlSettingsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void RcOnRemoteControlSettingsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.RCAccessMode? accessMode = null;

            if (RCAccessModeCheckBox.IsChecked == true)
            {
                accessMode = (HmiApiLib.Common.Enums.RCAccessMode)RCAccessModeComboBox.SelectedIndex; ;
            }
            bool? toggle = null;
            if (AllowedCheckBox.IsChecked == true)
            {
                toggle = AllowToggle.IsOn;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildRcOnRemoteControlSettings(toggle, accessMode);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void RcOnRemoteControlSettingsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void RCAccessModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RCAccessModeComboBox.IsEnabled = (bool)RCAccessModeCheckBox.IsChecked;
        }

        private void AllowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowToggle.IsEnabled = (bool)AllowedCheckBox.IsChecked;

        }
    }
}
