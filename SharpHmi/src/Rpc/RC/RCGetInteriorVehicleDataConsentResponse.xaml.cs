﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.RC.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RCGetInteriorVehicleDataConsentResponse : UserControl
    {
        GetInteriorVehicleDataConsent tmpObj = null;
        public RCGetInteriorVehicleDataConsentResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetInteriorVehicleDataConsent()
        {
            ContentDialog rcGetInteriorVehicleDataConsentCD = new ContentDialog();
            
            rcGetInteriorVehicleDataConsentCD.Content = this;

            rcGetInteriorVehicleDataConsentCD.PrimaryButtonText = Const.TxLater;
            rcGetInteriorVehicleDataConsentCD.PrimaryButtonClick += RcGetInteriorVehicleDataConsentCD_PrimaryButtonClick;

            rcGetInteriorVehicleDataConsentCD.SecondaryButtonText = Const.Reset;
            rcGetInteriorVehicleDataConsentCD.SecondaryButtonClick += RcGetInteriorVehicleDataConsentCD_ResetButtonClick;

            rcGetInteriorVehicleDataConsentCD.CloseButtonText = Const.Close;
            rcGetInteriorVehicleDataConsentCD.CloseButtonClick += RcGetInteriorVehicleDataConsentCD_CloseButtonClick;

            tmpObj = new GetInteriorVehicleDataConsent();
            tmpObj = (GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<GetInteriorVehicleDataConsent>(tmpObj.getMethod());

            if (tmpObj == null)
            {
                tmpObj = (GetInteriorVehicleDataConsent)BuildDefaults.buildDefaultMessage(typeof(GetInteriorVehicleDataConsent), 0);
            }
            if (tmpObj != null)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getAllowed() == null)
                {
                    AllowCheckBox.IsChecked = false;
                    AllowToggle.IsEnabled = false;
                }
                else
                {
                    AllowCheckBox.IsChecked = true;
                    AllowToggle.IsOn = (bool)tmpObj.getAllowed();
                }
            }

            await rcGetInteriorVehicleDataConsentCD.ShowAsync();
        }

        private void RcGetInteriorVehicleDataConsentCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AllowCheckBox.IsChecked == true)
            {
                toggle = AllowToggle.IsOn;
            }
            RpcResponse rpcResponse = BuildRpc.buildRcGetInteriorVehicleDataConsentResponse(
                BuildRpc.getNextId(), resultCode, toggle);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void RcGetInteriorVehicleDataConsentCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void RcGetInteriorVehicleDataConsentCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AllowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowToggle.IsEnabled = (bool)AllowCheckBox.IsChecked;

        }
    }
}
