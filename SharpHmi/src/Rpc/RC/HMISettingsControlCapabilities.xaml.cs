﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class HMISettingsControlCapabilities : UserControl
    {
        public CheckBox moduleNameCheckBox;
        public TextBox moduleNameTextBox;

        public CheckBox distanceUnitAvailableCheckBox;
        public ToggleSwitch distanceUnitAvailableToggleSwitch;

        public CheckBox temperatureUnitAvailableCheckBox;
        public ToggleSwitch temperatureUnitAvailableToggleSwitch;

        public CheckBox displayModeUnitAvailableCheckBox;
        public ToggleSwitch displayModeUnitAvailableToggleSwitch;

        public HMISettingsControlCapabilities(HmiApiLib.Common.Structs.HMISettingsControlCapabilities hmiSettingsControlCap)
        {
            this.InitializeComponent();

            moduleNameCheckBox = ModuleNameCheckBox;
            moduleNameTextBox = ModuleNameTextBox;

            distanceUnitAvailableCheckBox = DistanceUnitAvailableCheckBox;
            distanceUnitAvailableToggleSwitch = DistanceUnitAvailableToggleSwitch;

            temperatureUnitAvailableCheckBox = TemperatureUnitAvailableCheckBox;
            temperatureUnitAvailableToggleSwitch = TemperatureUnitAvailableToggleSwitch;

            displayModeUnitAvailableCheckBox = DisplayModeUnitAvailableCheckBox;
            displayModeUnitAvailableToggleSwitch = DisplayModeUnitAvailableToggleSwitch;

            if(hmiSettingsControlCap != null)
            {
                if (hmiSettingsControlCap.getModuleName() != null)
                    ModuleNameTextBox.Text = hmiSettingsControlCap.getModuleName();
                else
                {
                    ModuleNameTextBox.IsEnabled = false;
                    ModuleNameCheckBox.IsChecked = false;
                }

                if (hmiSettingsControlCap.getDistanceUnitAvailable() != null)
                    DistanceUnitAvailableToggleSwitch.IsOn = (bool)hmiSettingsControlCap.getDistanceUnitAvailable();
                else {
                    DistanceUnitAvailableToggleSwitch.IsEnabled = false;
                    DistanceUnitAvailableCheckBox.IsChecked = false;
                }
                if (hmiSettingsControlCap.getTemperatureUnitAvailable() != null)
                    TemperatureUnitAvailableToggleSwitch.IsOn = (bool)hmiSettingsControlCap.getTemperatureUnitAvailable();
                else
                {
                    TemperatureUnitAvailableToggleSwitch.IsEnabled = false;
                    TemperatureUnitAvailableCheckBox.IsChecked = false;
                }
                if (hmiSettingsControlCap.getDisplayModeUnitAvailable() != null)
                    DisplayModeUnitAvailableToggleSwitch.IsOn = (bool)hmiSettingsControlCap.getDisplayModeUnitAvailable();
                else
                {
                    DisplayModeUnitAvailableToggleSwitch.IsEnabled = false;
                    DisplayModeUnitAvailableCheckBox.IsChecked = false;
                }
            }
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModuleNameTextBox.IsEnabled = (bool)ModuleNameCheckBox.IsChecked;
        }

        private void DistanceUnitAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DistanceUnitAvailableToggleSwitch.IsEnabled = (bool)DistanceUnitAvailableCheckBox.IsChecked;
        }

        private void TemperatureUnitAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TemperatureUnitAvailableToggleSwitch.IsEnabled = (bool)TemperatureUnitAvailableCheckBox.IsChecked;
        }

        private void DisplayModeUnitAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DisplayModeUnitAvailableToggleSwitch.IsEnabled = (bool)DisplayModeUnitAvailableCheckBox.IsChecked;
        }
    }
}
