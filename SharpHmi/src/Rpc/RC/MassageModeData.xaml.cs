﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class MassageModeData : UserControl
    {
        public CheckBox massageZoneCheckBox;
        public ComboBox massageZoneComboBox;

        public CheckBox massageModeCheckBox;
        public ComboBox massageModeComboBox;

        public MassageModeData()
        {
            this.InitializeComponent();

            massageZoneCheckBox = MassageZoneCheckBox;
            massageZoneComboBox = MassageZoneComboBox;

            massageModeCheckBox = MassageModeCheckBox;
            massageModeComboBox = MassageModeComboBox;

            MassageZoneComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.MassageZone));
            MassageZoneComboBox.SelectedIndex = 0;

            MassageModeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.MassageMode));
            MassageModeComboBox.SelectedIndex = 0;
        }

        private void MassageZoneCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MassageZoneComboBox.IsEnabled = (bool)MassageZoneCheckBox.IsChecked;
        }

        private void MassageModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MassageModeComboBox.IsEnabled = (bool)MassageModeCheckBox.IsChecked;
        }
    }
}
