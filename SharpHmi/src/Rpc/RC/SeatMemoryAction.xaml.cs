﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class SeatMemoryAction : UserControl
    {
        public CheckBox idCheckBox;
        public TextBox idTextBox;

        public CheckBox labelCheckBox;
        public TextBox labelTextBox;

        public CheckBox actionCheckBox;
        public ComboBox actionComboBox;

        public SeatMemoryAction( HmiApiLib.Common.Structs.SeatMemoryAction seatMemoryAction)
        {
            this.InitializeComponent();

            idCheckBox = IdCheckBox;
            idTextBox = IdTextBox;

            labelCheckBox = LabelCheckBox;
            labelTextBox = LabelTextBox;

            actionCheckBox = ActionCheckBox;
            actionComboBox = ActionComboBox;
            
            ActionComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.SeatMemoryActionType));
            ActionComboBox.SelectedIndex = 0;

            if (seatMemoryAction != null) 
            {
                if (seatMemoryAction.getId() != null)
                {
                    IdTextBox.Text = seatMemoryAction.getId().ToString();
                }
                else
                {
                    IdCheckBox.IsChecked = false;
                    IdTextBox.IsEnabled = false;
                }
                if(seatMemoryAction.getLabel() != null)
                {
                    LabelTextBox.Text = seatMemoryAction.getLabel();
                }
                else
                {
                    LabelCheckBox.IsChecked = false;
                    LabelTextBox.IsEnabled = false;
                }
                if (seatMemoryAction.getAction() != null)
                {
                    ActionComboBox.SelectedIndex = (int)seatMemoryAction.getAction();
                }
                else {
                    ActionCheckBox.IsChecked = false;
                    ActionComboBox.IsEnabled = false;
                }
            }

        }

        private void IdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IdTextBox.IsEnabled = (bool)IdCheckBox.IsChecked;
        }

        private void LabelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LabelTextBox.IsEnabled = (bool)LabelCheckBox.IsChecked;
        }

        private void ActionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ActionComboBox.IsEnabled = (bool)ActionCheckBox.IsChecked;
        }
    }
}
