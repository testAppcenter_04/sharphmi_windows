﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class LightCapabilities : UserControl
    {
        public CheckBox lightNameCheckBox;
        public ComboBox lightNameComboBox;

        public CheckBox densityAvailableCheckBox;
        public ToggleSwitch densityAvailableToggleSwitch;

        public CheckBox sRGBColorSpaceAvailableCheckBox;
        public ToggleSwitch sRGBColorSpaceAvailableToggleSwitch;

        public LightCapabilities()
        {
            this.InitializeComponent();

            lightNameCheckBox = LightNameCheckBox;
            lightNameComboBox = LightNameComboBox;

            densityAvailableCheckBox = DensityAvailableCheckBox;
            densityAvailableToggleSwitch = DensityAvailableToggleSwitch;

            sRGBColorSpaceAvailableCheckBox = SRGBColorSpaceAvailableCheckBox;
            sRGBColorSpaceAvailableToggleSwitch = SRGBColorSpaceAvailableToggleSwitch;

            LightNameComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.LightName));
            LightNameComboBox.SelectedIndex = 0;
        }

        private void LightNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LightNameComboBox.IsEnabled = (bool)LightNameCheckBox.IsChecked;
        }

        private void DensityAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DensityAvailableToggleSwitch.IsEnabled = (bool)DensityAvailableCheckBox.IsChecked;
        }

        private void SRGBColorSpaceAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SRGBColorSpaceAvailableToggleSwitch.IsEnabled = (bool)SRGBColorSpaceAvailableCheckBox.IsChecked;
        }
    }
}
