﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class EqualizerSettings : UserControl
    {
        public CheckBox channelIdCheckBox;
        public TextBox channelIdTextbox;

        public CheckBox channelNameCheckBox;
        public TextBox channelNameTextBox;

        public CheckBox channelSettingCheckBox;
        public TextBox channelSettingTextBox;

        public ObservableCollection<HmiApiLib.Common.Structs.EqualizerSettings> eqList;
        public EqualizerSettings()
        {
            InitializeComponent();
            channelIdCheckBox = ChannelIdCheckBox;
            channelIdTextbox = ChannelIdTextBox;
            channelNameCheckBox = ChannelNameCheckBox;
            channelNameTextBox = ChannelNameTextBox;
            channelSettingCheckBox = ChannelSettingCheckBox;
            channelSettingTextBox = ChannelSettingTextBox;
        }

        private void ChannelIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ChannelIdTextBox.IsEnabled = (bool)ChannelIdCheckBox.IsChecked;
        }

        private void ChannelNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ChannelNameTextBox.IsEnabled = (bool)ChannelNameCheckBox.IsChecked;
        }

        private void ChannelSettingCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ChannelSettingTextBox.IsEnabled = (bool)ChannelSettingCheckBox.IsChecked;
        }
    }
}
