﻿using HmiApiLib.Common.Enums;
using SharpHmi.RC;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class SeatControlData : UserControl
    {
        static ContentDialog massageModeCD = new ContentDialog();
        static ContentDialog massageCushionFirmnessCD = new ContentDialog();
        static ContentDialog memoryCD = new ContentDialog();
        
        public CheckBox idCheckBox;
        public ComboBox idComboBox;

        public CheckBox heatingEnabledCheckBox;
        public ToggleSwitch heatingEnabledToggleSwitch;

        public CheckBox coolingEnabledCheckBox;
        public ToggleSwitch coolingEnabledToggleSwitch;


        public CheckBox heatingLevelCheckBox;
        public TextBox heatingLevelTextBox;

        public CheckBox coolingLevelCheckBox;
        public TextBox coolingLevelTextBox;

        public CheckBox horizontalPositionCheckBox;
        public TextBox horizontalPositionTextBox;

        public CheckBox verticalPositionCheckBox;
        public TextBox verticalPositionTextBox;



        public CheckBox frontVerticalPositionCheckBox;
        public TextBox frontVerticalPositionTextBox;

        public CheckBox backVerticalPositionCheckBox;
        public TextBox backVerticalPositionTextBox;

        public CheckBox backTiltAngleCheckBox;
        public TextBox backTiltAngleTextBox;

        public CheckBox headSupportHorizontalPositionCheckBox;
        public TextBox headSupportHorizontalPositionTextBox;

        public CheckBox headSupportVerticalPositionCheckBox;
        public TextBox headSupportVerticalPositionTextBox;

        public CheckBox massageEnabledCheckBox;
        public ToggleSwitch massageEnabledToggleSwitch;


        public CheckBox massageModeDataCheckBox;
        public ObservableCollection<HmiApiLib.Common.Structs.MassageModeData> massageModeDataList =
            new ObservableCollection<HmiApiLib.Common.Structs.MassageModeData>();
        
        public CheckBox massageCushionFirmnessCheckBox;
        public ObservableCollection<HmiApiLib.Common.Structs.MassageCushionFirmness> massageCushionFirmnessList=
            new ObservableCollection<HmiApiLib.Common.Structs.MassageCushionFirmness>();
        
        public CheckBox memoryCheckBox;
        public HmiApiLib.Common.Structs.SeatMemoryAction seatMemory = new HmiApiLib.Common.Structs.SeatMemoryAction();

        String calledFrom ="";

        public SeatControlData(String calledFrom , HmiApiLib.Common.Structs.SeatControlData seatControlData)
        {
            this.InitializeComponent();
            this.calledFrom = calledFrom;
            IdComboBox.ItemsSource = Enum.GetNames(typeof(SupportedSeat));
            IdComboBox.SelectedIndex = 0;
            if (seatControlData.getId() != null)
                IdComboBox.SelectedIndex = (int)seatControlData.getId();

            else
            {
                IdComboBox.IsEnabled = false;
                IdCheckBox.IsChecked = false;
            }
            if (seatControlData.getHeatingEnabled() == null)
            {
                HeatingEnabledCheckBox.IsChecked = false;
                HeatingEnabledToggleSwitch.IsEnabled = false;
            }
            else
            {
                HeatingEnabledToggleSwitch.IsOn = (bool)seatControlData.getHeatingEnabled();
            }
            if (seatControlData.getCoolingEnabled() == null)
            {
                CoolingEnabledCheckBox.IsChecked = false;
                CoolingEnabledToggleSwitch.IsEnabled = false;
            }
            else
            {
                CoolingEnabledToggleSwitch.IsOn = (bool)seatControlData.getCoolingEnabled();
            }
            if (seatControlData.getHeatingLevel() != null)
                HeatingLevelTextBox.Text =seatControlData.getHeatingLevel().ToString();
            else
            {
                HeatingLevelTextBox.IsEnabled = false;
                HeatingLevelCheckBox.IsChecked = false;
            }
            if (seatControlData.getCoolingLevel() != null)
                CoolingLevelTextBox.Text = seatControlData.getCoolingLevel().ToString();
            else
            {
                CoolingLevelTextBox.IsEnabled = false;
                CoolingLevelCheckBox.IsChecked = false;
            }
            if (seatControlData.getHorizontalPosition() != null)
                HorizontalPositionTextBox.Text = seatControlData.getHorizontalPosition().ToString();
            else
            {
                HorizontalPositionTextBox.IsEnabled = false;
                HorizontalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getVerticalPosition() != null)
                VerticalPositionTextBox.Text = seatControlData.getVerticalPosition().ToString();
            else
            {
                VerticalPositionTextBox.IsEnabled = false;
                VerticalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getFrontVerticalPosition() != null)
                FrontVerticalPositionTextBox.Text = seatControlData.getFrontVerticalPosition().ToString();
            else
            {
                FrontVerticalPositionTextBox.IsEnabled = false;
                FrontVerticalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getBackVerticalPosition() != null)
                BackVerticalPositionTextBox.Text = seatControlData.getBackVerticalPosition().ToString();
            else
            {
               BackVerticalPositionTextBox.IsEnabled = false;
               BackVerticalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getBackTiltAngle() != null)
                BackTiltAngleTextBox.Text = seatControlData.getBackTiltAngle().ToString();
            else
            {
                BackTiltAngleTextBox.IsEnabled = false;
                BackTiltAngleCheckBox.IsChecked = false;
            }
            if (seatControlData.getHeadSupportHorizontalPosition() != null)
                HeadSupportHorizontalPositionTextBox.Text = seatControlData.getHeadSupportHorizontalPosition().ToString();
            else
            {
                HeadSupportHorizontalPositionTextBox.IsEnabled = false;
                HeadSupportHorizontalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getHeadSupportVerticalPosition() != null)
                HeadSupportVerticalPositionTextBox.Text = seatControlData.getHeadSupportVerticalPosition().ToString();
            else
            {
                HeadSupportVerticalPositionTextBox.IsEnabled = false;
                HeadSupportVerticalPositionCheckBox.IsChecked = false;
            }
            if (seatControlData.getMassageEnabled() == null)
            {
                MassageEnabledCheckBox.IsChecked = false;
                MassageEnabledToggleSwitch.IsEnabled = false;
            }
            else
            {
                MassageEnabledToggleSwitch.IsOn = (bool)seatControlData.getMassageEnabled();
            }
            if (seatControlData.getMassageMode() != null)
            {
                foreach (HmiApiLib.Common.Structs.MassageModeData msgData in seatControlData.getMassageMode())
                {
                    massageModeDataList.Add(msgData);
                }
            }
            else
            {

                MassageModeCheckBox.IsChecked = false;
                MassageModeButton.IsEnabled = false;

            }
            if (seatControlData.getMassageCushionFirmness() != null)
            {
                foreach (HmiApiLib.Common.Structs.MassageCushionFirmness msgCushionData in seatControlData.getMassageCushionFirmness())
                {
                    massageCushionFirmnessList.Add(msgCushionData);
                }
            }
            else
            {

                MassageCushionFirmnessCheckBox.IsChecked = false;
                MassageCushionFirmnessButton.IsEnabled = false;

            }
            if (seatControlData.getMemory() != null)
            {
                seatMemory = seatControlData.getMemory();
            }
            else
            {

                MemoryCheckBox.IsChecked = false;
                MemoryButton.IsEnabled = false;

            }

            idCheckBox = IdCheckBox;
            idComboBox = IdComboBox;

            heatingEnabledCheckBox = HeatingEnabledCheckBox;
            heatingEnabledToggleSwitch = HeatingEnabledToggleSwitch;

            coolingEnabledCheckBox = CoolingEnabledCheckBox;
            coolingEnabledToggleSwitch = CoolingEnabledToggleSwitch;


            heatingLevelCheckBox = HeatingLevelCheckBox;
            heatingLevelTextBox = HeatingLevelTextBox;

            coolingLevelCheckBox = CoolingLevelCheckBox;
            coolingLevelTextBox = CoolingLevelTextBox;

            horizontalPositionCheckBox = HorizontalPositionCheckBox;
            horizontalPositionTextBox = HorizontalPositionTextBox;

            verticalPositionCheckBox = VerticalPositionCheckBox;
            verticalPositionTextBox = VerticalPositionTextBox;

            frontVerticalPositionCheckBox = FrontVerticalPositionCheckBox;
            frontVerticalPositionTextBox = FrontVerticalPositionTextBox;

            backVerticalPositionCheckBox = BackVerticalPositionCheckBox;
            backVerticalPositionTextBox = BackVerticalPositionTextBox;

            backTiltAngleCheckBox = BackTiltAngleCheckBox;
            backTiltAngleTextBox = BackTiltAngleTextBox;

            headSupportHorizontalPositionCheckBox = HeadSupportHorizontalPositionCheckBox;
            headSupportHorizontalPositionTextBox = HeadSupportHorizontalPositionTextBox;

            headSupportVerticalPositionCheckBox = HeadSupportVerticalPositionCheckBox;
            headSupportVerticalPositionTextBox = HeadSupportVerticalPositionTextBox;

            massageEnabledCheckBox = MassageEnabledCheckBox;
            massageEnabledToggleSwitch = MassageEnabledToggleSwitch;

            massageModeDataCheckBox = MassageModeCheckBox;
            MassageModeListView.ItemsSource = massageModeDataList;

            massageCushionFirmnessCheckBox = MassageCushionFirmnessCheckBox;
            MassageCushionFirmnessListView.ItemsSource = massageCushionFirmnessList;

            memoryCheckBox = MemoryCheckBox;

            IdComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.SupportedSeat));
            IdComboBox.SelectedIndex = 0;

        }

        private void IdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IdComboBox.IsEnabled = (bool)IdCheckBox.IsChecked;
        }

        private void HeatingEnabledCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatingEnabledToggleSwitch.IsEnabled = (bool)HeatingEnabledCheckBox.IsChecked;
        }

        private void CoolingEnabledCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CoolingEnabledToggleSwitch.IsEnabled = (bool)CoolingEnabledCheckBox.IsChecked;
        }


        private void HeatingLevelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatingLevelTextBox.IsEnabled = (bool)HeatingLevelCheckBox.IsChecked;
        }

        private void CoolingLevelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CoolingLevelTextBox.IsEnabled = (bool)CoolingLevelCheckBox.IsChecked;
        }

        private void HorizontalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HorizontalPositionTextBox.IsEnabled = (bool)HorizontalPositionCheckBox.IsChecked;
        }

        private void VerticalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VerticalPositionTextBox.IsEnabled = (bool)VerticalPositionCheckBox.IsChecked;
        }



        private void FrontVerticalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FrontVerticalPositionTextBox.IsEnabled = (bool)FrontVerticalPositionCheckBox.IsChecked;
        }

        private void BackVerticalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BackVerticalPositionTextBox.IsEnabled = (bool)BackVerticalPositionCheckBox.IsChecked;
        }

        private void BackTiltAngleCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BackTiltAngleTextBox.IsEnabled = (bool)BackTiltAngleCheckBox.IsChecked;
        }

        private void HeadSupportHorizontalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadSupportHorizontalPositionTextBox.IsEnabled = (bool)HeadSupportHorizontalPositionCheckBox.IsChecked;
        }

        private void HeadSupportVerticalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadSupportVerticalPositionTextBox.IsEnabled = (bool)HeadSupportVerticalPositionCheckBox.IsChecked;
        }

        private void MassageEnabledCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MassageEnabledToggleSwitch.IsEnabled = (bool)MassageEnabledCheckBox.IsChecked;
        }

        private void MassageModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MassageModeButton.IsEnabled = (bool)MassageModeCheckBox.IsChecked;
        }

        private async void MassageModeTapped(object sender, TappedRoutedEventArgs e)
        {
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
            {
                RCSetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
            {
                RCGetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
            {
                RCOnInteriorVehicleData.addSeatControlDataCD.Hide();
            }

            massageModeCD = new ContentDialog();

            var massageMode = new MassageModeData();
            massageModeCD.Content = massageMode;


            massageModeCD.PrimaryButtonText = Const.OK;
            massageModeCD.PrimaryButtonClick += MassageModeCD_PrimaryButtonClick;

            massageModeCD.SecondaryButtonText = Const.Close;
            massageModeCD.SecondaryButtonClick += MassageModeCD_SecondaryButtonClick;
             await massageModeCD.ShowAsync(); 
            
        }

        private async void MassageModeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }

        private async void MassageModeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var mmd = sender.Content as MassageModeData;
            HmiApiLib.Common.Structs.MassageModeData massageModeData = new HmiApiLib.Common.Structs.MassageModeData();
            
            if (mmd.massageZoneCheckBox.IsChecked == true)
                massageModeData.massageZone = (MassageZone)mmd.massageZoneComboBox.SelectedIndex;

            if (mmd.massageModeCheckBox.IsChecked == true)
                massageModeData.massageMode = (MassageMode)mmd.massageModeComboBox.SelectedIndex;

            massageModeDataList.Add(massageModeData);

            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }

        private void MassageCushionFirmnessCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MassageCushionFirmnessButton.IsEnabled = (bool)MassageCushionFirmnessCheckBox.IsChecked;
        }

        private async void MassageCushionFirmnessTapped(object sender, TappedRoutedEventArgs e)
        {
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
            {
                RCSetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
            {
                RCGetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
            {
                RCOnInteriorVehicleData.addSeatControlDataCD.Hide();
            }

            massageCushionFirmnessCD = new ContentDialog();

            var massageCushionFirmness = new src.Rpc.RC.MassageCushionFirmness();
            
            massageCushionFirmnessCD.Content = massageCushionFirmness;
            //massageCushionFirmnessCD.Content = this;
            massageCushionFirmnessCD.PrimaryButtonText = Const.OK;
            massageCushionFirmnessCD.PrimaryButtonClick += MassageCushionFirmnessCD_PrimaryButtonClick;

            massageCushionFirmnessCD.SecondaryButtonText = Const.Close;
            massageCushionFirmnessCD.SecondaryButtonClick += MassageCushionFirmnessCD_SecondaryButtonClick;

            await massageCushionFirmnessCD.ShowAsync();
        }

        private async void MassageCushionFirmnessCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var mcf = sender.Content as MassageCushionFirmness;
            HmiApiLib.Common.Structs.MassageCushionFirmness massageCushionFirmness = new HmiApiLib.Common.Structs.MassageCushionFirmness();

            if (mcf.cushionCheckBox.IsChecked == true)
                massageCushionFirmness.cushion = (MassageCushion)mcf.cushionComboBox.SelectedIndex;

            if (mcf.firmnessCheckBox.IsChecked == true)
            {
                try
                {
                    massageCushionFirmness.firmness = Int32.Parse(mcf.firmnessTextBox.Text);
                }
                catch
                {
                    massageCushionFirmness.firmness = 0;
                }
            }

            massageCushionFirmnessList.Add(massageCushionFirmness);

            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }

        private async void MassageCushionFirmnessCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }

        private void MemoryCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MemoryButton.IsEnabled = (bool)MemoryCheckBox.IsChecked;
        }

        private async void MemoryTapped(object sender, TappedRoutedEventArgs e)
        {
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
            {
                RCSetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
            {
                RCGetInteriorVehicleDataResponse.addSeatControlDataCD.Hide();
            }
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
            {
                RCOnInteriorVehicleData.addSeatControlDataCD.Hide();
            }

            memoryCD = new ContentDialog();

            var memory = new SeatMemoryAction(seatMemory);
            memoryCD.Content = memory;

            memoryCD.PrimaryButtonText = Const.OK;
            memoryCD.PrimaryButtonClick += MemoryCD_PrimaryButtonClick;

            memoryCD.SecondaryButtonText = Const.Close;
            memoryCD.SecondaryButtonClick += MemoryCD_SecondaryButtonClick;

            await memoryCD.ShowAsync();
        }

        private async void MemoryCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var sma = sender.Content as SeatMemoryAction;
            HmiApiLib.Common.Structs.SeatMemoryAction seatMemoryAction = new HmiApiLib.Common.Structs.SeatMemoryAction();

            if (sma.idCheckBox.IsChecked == true)
            {
                try
                {
                    seatMemoryAction.id = Int32.Parse(sma.idTextBox.Text);
                }
                catch
                {
                    seatMemoryAction.id = 0;
                }
            }

            if (sma.labelCheckBox.IsChecked == true && sma.labelTextBox.Text != null)
                seatMemoryAction.label = sma.labelTextBox.Text;

            if (sma.actionCheckBox.IsChecked == true)
                seatMemoryAction.action = (SeatMemoryActionType)sma.actionComboBox.SelectedIndex;

            
            seatMemory = seatMemoryAction;

            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }

        private async void MemoryCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addSeatControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addSeatControlDataCD.ShowAsync();
        }
    }
}