﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class LightState : UserControl
    {
        public CheckBox lightNameCheckBox;
        public ComboBox lightNameComboBox;

        public CheckBox lightStatusCheckBox;
        public ComboBox lightStatusComboBox;

        public CheckBox densityCheckBox;
        public TextBox densityTextBox;

        public CheckBox rgbColorCheckBox;

        public CheckBox redCheckBox;
        public TextBox redTextBox;

        public CheckBox greenCheckBox;
        public TextBox greenTextBox;

        public CheckBox blueCheckBox;
        public TextBox blueTextBox;

        public HmiApiLib.Common.Structs.RGBColor rgbColor = new HmiApiLib.Common.Structs.RGBColor();

        public LightState()
        {
            InitializeComponent();

            lightNameCheckBox = LightNameCheckBox;
            lightNameComboBox = LightNameComboBox;

            lightStatusCheckBox = LightStatusCheckBox;
            lightStatusComboBox = LightStatusComboBox;

            densityCheckBox = DensityCheckBox;
            densityTextBox = DensityTextBox;

            rgbColorCheckBox = RGBColorCheckBox;

            redCheckBox = RedCheckBox;
            redTextBox = RedTextBox;

            greenCheckBox = GreenCheckBox;
            greenTextBox = GreenTextBox;

            blueCheckBox = BlueCheckBox;
            blueCheckBox = BlueCheckBox;

            LightNameComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.LightName));
            LightNameComboBox.SelectedIndex = 0;

            LightStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.LightStatus));
            LightStatusComboBox.SelectedIndex = 0;
        }

        private void LightNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LightNameComboBox.IsEnabled = (bool)LightNameCheckBox.IsChecked;
        }

        private void LightStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LightStatusComboBox.IsEnabled = (bool)LightStatusCheckBox.IsChecked;
        }

        private void DensityCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DensityTextBox.IsEnabled = (bool)DensityCheckBox.IsChecked;
        }

        private void RGBColorCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if ((bool)RGBColorCheckBox.IsChecked)
            {
                RedCheckBox.IsEnabled = true;
                GreenCheckBox.IsEnabled = true;
                BlueCheckBox.IsEnabled = true;

                RedTextBox.IsEnabled = (bool)RedCheckBox.IsChecked;
                GreenTextBox.IsEnabled = (bool)GreenCheckBox.IsChecked;
                BlueTextBox.IsEnabled = (bool)BlueCheckBox.IsChecked;
            }
            else
            {
                RedCheckBox.IsEnabled = false;
                GreenCheckBox.IsEnabled = false;
                BlueCheckBox.IsEnabled = false;

                RedTextBox.IsEnabled = false;
                GreenTextBox.IsEnabled = false;
                BlueTextBox.IsEnabled = false;
            }
            
        }

        private void RedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RedTextBox.IsEnabled = (bool)RedCheckBox.IsChecked;
        }

        private void GreenCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GreenTextBox.IsEnabled = (bool)GreenCheckBox.IsChecked;
        }

        private void BlueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BlueTextBox.IsEnabled = (bool)BlueCheckBox.IsChecked;
        }
    }
}
