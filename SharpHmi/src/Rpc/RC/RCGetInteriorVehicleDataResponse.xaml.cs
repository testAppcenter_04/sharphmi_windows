﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HmiApiLib.Controllers.RC.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using HmiApiLib.Builder;
using System.Collections.Generic;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RCGetInteriorVehicleDataResponse : UserControl
    {
        GetInteriorVehicleData tmpObj = null;
        public static ContentDialog rcGetInteriorVehicleDataCD = new ContentDialog();

        public static ContentDialog addRadioControlDataCD = new ContentDialog();
        public static ContentDialog addClimateControlDataCD = new ContentDialog();
        public static ContentDialog addSeatControlDataCD = new ContentDialog();
        public static ContentDialog addAudioControlDataCD = new ContentDialog();
        public static ContentDialog addLightControlDataCD = new ContentDialog();
        public static ContentDialog addHmiSettingControlDataCD = new ContentDialog();

        ModuleData moduleData;
        HmiApiLib.Common.Structs.ClimateControlData climateControlData = new HmiApiLib.Common.Structs.ClimateControlData();
        HmiApiLib.Common.Structs.RadioControlData radioControlData = new HmiApiLib.Common.Structs.RadioControlData();
        HmiApiLib.Common.Structs.SeatControlData seatControlData = new HmiApiLib.Common.Structs.SeatControlData();
        AudioControlData audioControlData = null;
        HMISettingsControlData hmiSettingsControlData = null;
        LightControlData lightControlData = null;


        public RCGetInteriorVehicleDataResponse()
        {
            InitializeComponent();

            ModuleTypeComboBox.ItemsSource = Enum.GetNames(typeof(ModuleType));
            ModuleTypeComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetInteriorVehicleData()
        {
            rcGetInteriorVehicleDataCD = new ContentDialog();
            rcGetInteriorVehicleDataCD.Content = this;

            rcGetInteriorVehicleDataCD.PrimaryButtonText = Const.TxLater;
            rcGetInteriorVehicleDataCD.PrimaryButtonClick += RcGetInteriorVehicleDataCD_PrimaryButtonClick;

            rcGetInteriorVehicleDataCD.SecondaryButtonText = Const.Reset;
            rcGetInteriorVehicleDataCD.SecondaryButtonClick += RcGetInteriorVehicleDataCD_SecondaryButtonClick;

            rcGetInteriorVehicleDataCD.CloseButtonText = Const.Close;
            rcGetInteriorVehicleDataCD.CloseButtonClick += RcGetInteriorVehicleDataCD_CloseButtonClick;


            tmpObj = new GetInteriorVehicleData();
            tmpObj = (GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<GetInteriorVehicleData>(tmpObj.getMethod());
            if (tmpObj == null)
            {
                tmpObj = (GetInteriorVehicleData)BuildDefaults.buildDefaultMessage(typeof(GetInteriorVehicleData), 0);
            }

            if (tmpObj != null)
            {
                moduleData = tmpObj.getModuleData();
                if (moduleData != null)
                {
                    if (moduleData.getModuleType() != null)
                        ModuleTypeComboBox.SelectedIndex = (int)moduleData.getModuleType();
                    else
                    {
                        ModuleTypeComboBox.IsEnabled = false;
                        ModuleTypeCheckBox.IsChecked = false;
                    }
                    if (moduleData.getClimateControlData() != null)
                    {
                        climateControlData = moduleData.getClimateControlData();
                    }
                    else
                    {
                        ClimateControlDataCheckBox.IsChecked = false;
                        ClimateControlDataButton.IsEnabled = false;
                    }

                    if (moduleData.getRadioControlData() != null)
                    {
                        radioControlData = moduleData.getRadioControlData();
                    }
                    else
                    {
                        RadioControlDataCheckBox.IsChecked = false;
                        RadioControlDataButton.IsEnabled = false;

                    }

                    if (moduleData.getSeatControlData() != null)
                    {
                        seatControlData = moduleData.getSeatControlData();
                    }
                    else
                    {
                        SeatControlDataCheckBox.IsChecked = false;
                        SeatControlDataButton.IsEnabled = false;

                    }
                    if (null != moduleData.getAudioControlData())
                    {
                        audioControlData = moduleData.getAudioControlData();

                    }
                    else
                    {
                        AudioControlDataCheckBox.IsChecked = false;
                        AudioControlDataButton.IsEnabled = false;

                    }
                    if (null != moduleData.getLightControlData())
                        lightControlData = moduleData.getLightControlData();
                   else
                    {
                        LightControlDataCheckBox.IsChecked = false;
                        LightControlDataButton.IsEnabled = false;
                    }
                    if (null != moduleData.getHmiSettingsControlData())
                         hmiSettingsControlData = moduleData.getHmiSettingsControlData();
                    else
                    {
                        HMISettingsControlDataCheckBox.IsChecked = false;
                        HMISettingsControlDataButton.IsEnabled = false;
                    }
                }
                else {
                    ModuleDataCheckBox.IsChecked = false;

                    ModuleTypeCheckBox.IsEnabled = false;
                    ModuleTypeComboBox.IsEnabled = false;

                    RadioControlDataCheckBox.IsEnabled = false;
                    RadioControlDataButton.IsEnabled = false;

                    ClimateControlDataCheckBox.IsEnabled = false;
                    ClimateControlDataButton.IsEnabled = false;

                    AudioControlDataCheckBox.IsEnabled = false;
                    AudioControlDataButton.IsEnabled = false;

                    LightControlDataCheckBox.IsEnabled = false;
                    LightControlDataButton.IsEnabled = false;

                    HMISettingsControlDataCheckBox.IsEnabled = false;
                    HMISettingsControlDataButton.IsEnabled = false;
                    SeatControlDataCheckBox.IsEnabled = false;
                    SeatControlDataCheckBox.IsEnabled = false;

                }
                if (tmpObj.getIsSubscribed() == null)
                {
                    IsSubscribedCheckBox.IsChecked = false;
                    IsSubscribedToggle.IsEnabled = false;
                }
                else
                {
                    IsSubscribedToggle.IsOn = (bool)tmpObj.getIsSubscribed();

                    IsSubscribedCheckBox.IsChecked = true;
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                
            }


            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void RcGetInteriorVehicleDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            ModuleData moduleData = null;
            if(ModuleDataCheckBox.IsChecked == true)
            {
                moduleData = new ModuleData();
                if (ModuleTypeCheckBox.IsChecked == true)
                {
                    moduleData.moduleType = (ModuleType)ModuleTypeComboBox.SelectedIndex;
                }
                if (ClimateControlDataCheckBox.IsChecked == true)
                {
                    moduleData.climateControlData = climateControlData;
                }
                if (RadioControlDataCheckBox.IsChecked == true)
                {
                    moduleData.radioControlData = radioControlData;
                }
                if (SeatControlDataCheckBox.IsChecked == true)
                {
                    moduleData.seatControlData = seatControlData;
                }
                if (AudioControlDataCheckBox.IsChecked == true)
                {
                    moduleData.audioControlData = audioControlData;
                }
                if (LightControlDataCheckBox.IsChecked == true)
                {
                    moduleData.lightControlData = lightControlData;
                }
                if (HMISettingsControlDataCheckBox.IsChecked == true)
                {
                    moduleData.hmiSettingsControlData = hmiSettingsControlData;
                }
            }
            
            Result? result = null;
            if(ResultCodeCheckBox.IsChecked == true)
            {
                result = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (IsSubscribedCheckBox.IsChecked == true)
            {
                toggle = IsSubscribedToggle.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildRcGetInteriorVehicleDataResponse(BuildRpc.getNextId(), result, moduleData, toggle);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void RcGetInteriorVehicleDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (tmpObj != null)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void RcGetInteriorVehicleDataCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ModuleDataCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ModuleDataCheckBox.IsChecked == true)
            {
                ModuleTypeCheckBox.IsEnabled = true;
                RadioControlDataCheckBox.IsEnabled = true;
                ClimateControlDataCheckBox.IsEnabled = true;
                AudioControlDataCheckBox.IsEnabled = true;
                SeatControlDataCheckBox.IsEnabled = true;
                LightControlDataCheckBox.IsEnabled = true;
                HMISettingsControlDataCheckBox.IsEnabled = true;

                if (ModuleTypeCheckBox.IsChecked == true)
                    ModuleTypeComboBox.IsEnabled = true;
                else
                    ModuleTypeComboBox.IsEnabled = false;

                if(RadioControlDataCheckBox.IsChecked == true)
                    RadioControlDataButton.IsEnabled = true;
                else
                    RadioControlDataButton.IsEnabled = false;
                
                if (ClimateControlDataCheckBox.IsChecked == true)
                    ClimateControlDataButton.IsEnabled = true;   
                else
                    ClimateControlDataButton.IsEnabled = false;

                if (AudioControlDataCheckBox.IsChecked == true)
                    AudioControlDataButton.IsEnabled = true;
                else
                    AudioControlDataButton.IsEnabled = false;

                if (LightControlDataCheckBox.IsChecked == true)
                    LightControlDataButton.IsEnabled = true;
                else
                    LightControlDataButton.IsEnabled = false;

                if (HMISettingsControlDataCheckBox.IsChecked == true)
                    HMISettingsControlDataButton.IsEnabled = true;
                else
                    HMISettingsControlDataButton.IsEnabled = false;

                if (SeatControlDataCheckBox.IsChecked == true)
                    SeatControlDataButton.IsEnabled = true;
                else
                    SeatControlDataButton.IsEnabled = false;
            }
            else
            {
                ModuleTypeCheckBox.IsEnabled = false;
                ModuleTypeComboBox.IsEnabled = false;

                RadioControlDataCheckBox.IsEnabled = false;
                RadioControlDataButton.IsEnabled = false;

                ClimateControlDataCheckBox.IsEnabled = false;
                ClimateControlDataButton.IsEnabled = false;

                AudioControlDataCheckBox.IsEnabled = false;
                AudioControlDataButton.IsEnabled = false;

                SeatControlDataCheckBox.IsEnabled = false;
                SeatControlDataButton.IsEnabled = false;

                LightControlDataCheckBox.IsEnabled = false;
                LightControlDataButton.IsEnabled = false;

                HMISettingsControlDataCheckBox.IsEnabled = false;
                HMISettingsControlDataButton.IsEnabled = false;
            }
        }

        private void ModuleTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ModuleTypeCheckBox.IsChecked == true)
                ModuleTypeComboBox.IsEnabled = true;
            else
                ModuleTypeComboBox.IsEnabled = false;
        }

        private void RadioControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RadioControlDataCheckBox.IsChecked == true)
                RadioControlDataButton.IsEnabled = true;
            else
                RadioControlDataButton.IsEnabled = false;
        }

        private async void RadioControlDataTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addRadioControlDataCD = new ContentDialog();

            var addRadioControlData = new RadioControlData("RCGetInteriorVehicleDataResponse",radioControlData);
            addRadioControlDataCD.Content = addRadioControlData;

            addRadioControlDataCD.PrimaryButtonText = "Ok";
            addRadioControlDataCD.PrimaryButtonClick += AddRadioControlDataCD_PrimaryButtonClick;

            addRadioControlDataCD.SecondaryButtonText = "Cancel";
            addRadioControlDataCD.SecondaryButtonClick += AddRadioControlDataCD_SecondaryButtonClick; ;

            await addRadioControlDataCD.ShowAsync();
        }

        private async void AddRadioControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addRadioControlData = sender.Content as RadioControlData;
            radioControlData = new HmiApiLib.Common.Structs.RadioControlData();

            if (addRadioControlData.frequencyIntegerCB.IsChecked == true && addRadioControlData.frequencyIntegerTB.Text.Trim() != "")
            {
                try
                {
                    radioControlData.frequencyInteger = Int32.Parse(addRadioControlData.frequencyIntegerTB.Text);
                }
                catch
                {

                }
            }

            if (addRadioControlData.frequencyFractionCB.IsChecked == true && addRadioControlData.frequencyFractionTB.Text.Trim() != "")
            {
                try
                {
                    radioControlData.frequencyFraction = Int32.Parse(addRadioControlData.frequencyFractionTB.Text);
                }
                catch
                {
                }
            }

            if (addRadioControlData.radioBandCB.IsChecked == true)
            {
                radioControlData.band = (RadioBand)addRadioControlData.radioBandComboBox.SelectedIndex;
            }

            if (addRadioControlData.rdsDataCB.IsChecked == true)
            {
                RdsData rdsData = new RdsData();

                if (addRadioControlData.psCB.IsChecked == true && addRadioControlData.psTB.Text != null)
                {
                    rdsData.PS = addRadioControlData.psTB.Text;
                }

                if (addRadioControlData.rtCB.IsChecked == true && addRadioControlData.rtTB.Text != null)
                {
                    rdsData.RT = addRadioControlData.rtTB.Text;
                }
                if (addRadioControlData.ctCB.IsChecked == true && addRadioControlData.ctTB.Text != null)
                {
                    rdsData.CT = addRadioControlData.ctTB.Text;
                }
                if (addRadioControlData.ctCB.IsChecked == true && addRadioControlData.ctTB.Text != null)
                {
                    rdsData.PI = addRadioControlData.piTB.Text;
                }
                if (addRadioControlData.ptyCB.IsChecked == true && addRadioControlData.ptyTB.Text != null)
                {
                    try
                    {
                        rdsData.PTY = Int32.Parse(addRadioControlData.ptyTB.Text);
                    }
                    catch
                    {
                    }
                }
                if (addRadioControlData.tpCB.IsChecked == true)
                {
                    rdsData.TP = addRadioControlData.tpTgl.IsOn;
                }
                if (addRadioControlData.taCB.IsChecked == true)
                {
                    rdsData.TA = addRadioControlData.taTgl.IsOn;
                }



                if (addRadioControlData.regCB.IsChecked == true && addRadioControlData.regTB.Text.Trim() != "")
                {
                    rdsData.REG = addRadioControlData.regTB.Text;
                }

                radioControlData.rdsData = rdsData;
            }

            if (addRadioControlData.availableHDsCB.IsChecked == true && addRadioControlData.availableHDsTB.Text.Trim() != "")
            {
                try
                {
                    radioControlData.availableHDs = Int32.Parse(addRadioControlData.availableHDsTB.Text);
                }
                catch
                {
                }
            }

            if (addRadioControlData.hdChannelCB.IsChecked == true)
            {
                try
                {
                    radioControlData.hdChannel = Int32.Parse(addRadioControlData.hdChannelTB.Text);
                }
                catch
                {
                }
            }

            if (addRadioControlData.signalStrengthCb.IsChecked == true)
            {
                try
                {
                    radioControlData.signalStrength = Int32.Parse(addRadioControlData.signalStrengthTB.Text);
                }
                catch
                {
                }
            }

            if (addRadioControlData.signalChangeThresholdCB.IsChecked == true)
            {
                try
                {
                    radioControlData.signalChangeThreshold = Int32.Parse(addRadioControlData.signalChangeThresholdTB.Text);
                }
                catch
                {
                }
            }
            if (addRadioControlData.RadioEnabledCB.IsChecked == true)
            {
                radioControlData.radioEnable = addRadioControlData.RadioEnabledTgl.IsOn;
            }
           
            
            if (addRadioControlData.radioStateCB.IsChecked == true)
            {
                radioControlData.state = (RadioState)addRadioControlData.radioStateComboBox.SelectedIndex;
            }

            if (addRadioControlData.sisDataCheckBox.IsChecked == true)
            {
                  //sisData = new HmiApiLib.Common.Structs.SisData();
                radioControlData.sisData = addRadioControlData.sisData;
            }

            addRadioControlDataCD.Hide();

            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddRadioControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addRadioControlDataCD.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void ClimateControlDataTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addClimateControlDataCD = new ContentDialog();

            var addClimateControlData = new ClimateControlData(climateControlData);
            addClimateControlDataCD.Content = addClimateControlData;

            addClimateControlDataCD.PrimaryButtonText = "Ok";
            addClimateControlDataCD.PrimaryButtonClick += AddClimateControlDataCD_PrimaryButtonClick;

            addClimateControlDataCD.SecondaryButtonText = "Cancel";
            addClimateControlDataCD.SecondaryButtonClick += AddClimateControlDataCD_SecondaryButtonClick;

            await addClimateControlDataCD.ShowAsync();
        }

        private async void AddClimateControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addClimateControlData = sender.Content as ClimateControlData;
            climateControlData = new HmiApiLib.Common.Structs.ClimateControlData();

            if (addClimateControlData.fanSpeedCB.IsChecked == true)
            {
                try
                {
                    climateControlData.fanSpeed = Int32.Parse(addClimateControlData.fanSpeedTB.Text);
                }
                catch
                {
                }
            }

            if (addClimateControlData.currentTemperatureCB.IsChecked == true)
            {
                Temperature currentTemperature = new Temperature();

                if (addClimateControlData.currentTemperatureUnitCB.IsChecked == true)
                {
                    currentTemperature.unit = (TemperatureUnit)addClimateControlData.currentTemperatureUnitComboBox.SelectedIndex;
                }
                if (addClimateControlData.currentTemperatureValueCB.IsChecked == true)
                {
                    try
                    {
                        currentTemperature.value = float.Parse(addClimateControlData.currentTemperatureValueTextBox.Text);
                    }
                    catch
                    {
                    }
                }

                climateControlData.currentTemperature = currentTemperature;
            }

            if (addClimateControlData.desiredTemperatureCB.IsChecked == true)
            {
                Temperature desiredTemperature = new Temperature();

                if (addClimateControlData.desiredTemperatureCB.IsChecked == true)
                {
                    desiredTemperature.unit = (TemperatureUnit)addClimateControlData.desiredTemperatureUnitComboBox.SelectedIndex;
                }
                if (addClimateControlData.desiredTemperatureValueCB.IsChecked == true)
                {
                    try
                    {
                        desiredTemperature.value = float.Parse(addClimateControlData.desiredTemperatureValueTextBox.Text);
                    }
                    catch
                    {
                    }
                }
                climateControlData.desiredTemperature = desiredTemperature;
            }

            if (addClimateControlData.acEnableCB.IsChecked == true)
            {
                climateControlData.acEnable = addClimateControlData.acEnableTgl.IsOn;
            }
            if (addClimateControlData.circulatedAirEnableCB.IsChecked == true)
            {
                climateControlData.circulateAirEnable = addClimateControlData.circulatedAirEnableTgl.IsOn;
            }
            if (addClimateControlData.autoModeEnableCB.IsChecked == true)
            {
                climateControlData.autoModeEnable = addClimateControlData.autoModeEnableTgl.IsOn;
            }

            
            if (addClimateControlData.defrostZoneCB.IsChecked == true)
            {
                climateControlData.defrostZone = (HmiApiLib.Common.Enums.DefrostZone)addClimateControlData.defrostZoneComboBox.SelectedIndex;
            }
            if (addClimateControlData.dualModeEnableCB.IsChecked == true)
            {
                climateControlData.dualModeEnable = addClimateControlData.dualModeEnableTgl.IsOn;
            }
            if (addClimateControlData.acMaxEnableCB.IsChecked == true)
            {
                climateControlData.acMaxEnable = addClimateControlData.acMaxEnableTgl.IsOn;
            }

            if (addClimateControlData.ventilationModeCB.IsChecked == true)
            {
                climateControlData.ventilationMode = (VentilationMode)addClimateControlData.ventilationModeComboBox.SelectedIndex;
            }

            if (addClimateControlData.heatedMirrorEnableCB.IsChecked == true)
            {
                climateControlData.heatedMirrorsEnable = addClimateControlData.heatedMirrorEnableTgl.IsOn;
            }
            if (addClimateControlData.heatedRearWindowEnableCB.IsChecked == true)
            {
                climateControlData.heatedRearWindowEnable = addClimateControlData.heatedRearWindowEnableTgl.IsOn;
            }
            if (addClimateControlData.heatedWindshieldEnableCB.IsChecked == true)
            {
                climateControlData.heatedWindshieldEnable = addClimateControlData.heatedWindshieldEnableTgl.IsOn;

            }
            if (addClimateControlData.heatedSteeringWheelEnableCB.IsChecked == true)
            {
                climateControlData.heatedSteeringWheelEnable = addClimateControlData.heatedSteeringWheelEnableTgl.IsOn;
            }
            addClimateControlDataCD.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddClimateControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addClimateControlDataCD.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void ClimateControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ClimateControlDataCheckBox.IsChecked == true)
                ClimateControlDataButton.IsEnabled = true;
            else
                ClimateControlDataButton.IsEnabled = false;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ResultCodeCheckBox.IsChecked == true)
                ResultCodeComboBox.IsEnabled = true;
            else
                ResultCodeComboBox.IsEnabled = false;
        }

        private async void SeatControlDataTappedAsync(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addSeatControlDataCD = new ContentDialog();

            var addSeatControlData = new src.Rpc.RC.SeatControlData("RCGetInteriorVehicleDataResponse",seatControlData);
            addSeatControlDataCD.Content = addSeatControlData;

            addSeatControlDataCD.PrimaryButtonText = "Ok";
            addSeatControlDataCD.PrimaryButtonClick += AddSeatControlDataCD_PrimaryButtonClick;

            addSeatControlDataCD.SecondaryButtonText = "Cancel";
            addSeatControlDataCD.SecondaryButtonClick += AddSeatControlDataCD_SecondaryButtonClick;

            await addSeatControlDataCD.ShowAsync();
        }

        private async void AddSeatControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addSeatControlData = sender.Content as SharpHmi.src.Rpc.RC.SeatControlData;
            seatControlData = new HmiApiLib.Common.Structs.SeatControlData();
            List<HmiApiLib.Common.Structs.MassageModeData> MsgData = null;
            List<HmiApiLib.Common.Structs.MassageCushionFirmness> MsgCushionData = null;

            if (addSeatControlData.idCheckBox.IsChecked == true)
                {
                    seatControlData.id = (SupportedSeat)addSeatControlData.idComboBox.SelectedIndex;
                }

                if (addSeatControlData.heatingEnabledCheckBox.IsChecked == true)
                {
                    seatControlData.heatingEnabled = addSeatControlData.heatingEnabledToggleSwitch.IsOn;
                }

                if (addSeatControlData.coolingEnabledCheckBox.IsChecked == true)
                {
                    seatControlData.coolingEnabled = addSeatControlData.coolingEnabledToggleSwitch.IsOn;
                }

                if (addSeatControlData.heatingLevelCheckBox.IsChecked == true && addSeatControlData.heatingLevelTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.heatingLevel = int.Parse(addSeatControlData.heatingLevelTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.heatingLevel = 0;
                    }
                }

                if (addSeatControlData.coolingLevelCheckBox.IsChecked == true && addSeatControlData.coolingLevelTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.coolingLevel = int.Parse(addSeatControlData.coolingLevelTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.coolingLevel = 0;
                    }
                }

                if (addSeatControlData.horizontalPositionCheckBox.IsChecked == true && addSeatControlData.horizontalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.horizontalPosition = int.Parse(addSeatControlData.horizontalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.horizontalPosition = 0;
                    }
                }

                if (addSeatControlData.verticalPositionCheckBox.IsChecked == true && addSeatControlData.verticalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.verticalPosition = int.Parse(addSeatControlData.verticalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.verticalPosition = 0;
                    }
                }

                if (addSeatControlData.frontVerticalPositionCheckBox.IsChecked == true && addSeatControlData.frontVerticalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.frontVerticalPosition = int.Parse(addSeatControlData.frontVerticalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.frontVerticalPosition = 0;
                    }
                }

                if (addSeatControlData.backVerticalPositionCheckBox.IsChecked == true && addSeatControlData.backVerticalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.backVerticalPosition = int.Parse(addSeatControlData.backVerticalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.backVerticalPosition = 0;
                    }
                }

                if (addSeatControlData.backTiltAngleCheckBox.IsChecked == true && addSeatControlData.backTiltAngleTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.backTiltAngle = int.Parse(addSeatControlData.backTiltAngleTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.backTiltAngle = 0;
                    }
                }

                if (addSeatControlData.headSupportHorizontalPositionCheckBox.IsChecked == true && addSeatControlData.headSupportHorizontalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.headSupportHorizontalPosition = int.Parse(addSeatControlData.headSupportHorizontalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.headSupportHorizontalPosition = 0;
                    }
                }

                if (addSeatControlData.headSupportVerticalPositionCheckBox.IsChecked == true && addSeatControlData.headSupportVerticalPositionTextBox.Text != "")
                {
                    try
                    {
                        seatControlData.headSupportVerticalPosition = int.Parse(addSeatControlData.headSupportVerticalPositionTextBox.Text);
                    }
                    catch
                    {
                        seatControlData.headSupportVerticalPosition = 0;
                    }
                }

                if (addSeatControlData.massageEnabledCheckBox.IsChecked == true)
                {
                seatControlData.massageEnabled = addSeatControlData.massageEnabledToggleSwitch.IsOn;
                }

                if (addSeatControlData.massageModeDataCheckBox.IsChecked == true && addSeatControlData.massageModeDataList != null)
                {
                MsgData = new List<HmiApiLib.Common.Structs.MassageModeData>();
                MsgData.AddRange(addSeatControlData.massageModeDataList);
                seatControlData.massageMode = MsgData;
                }



                if (addSeatControlData.massageModeDataCheckBox.IsChecked == true && addSeatControlData.massageCushionFirmnessList != null)
                {
                MsgCushionData = new List<HmiApiLib.Common.Structs.MassageCushionFirmness>();
                MsgCushionData.AddRange(addSeatControlData.massageCushionFirmnessList);
                seatControlData.massageCushionFirmness = MsgCushionData;
                }

                if (addSeatControlData.memoryCheckBox.IsChecked == true && addSeatControlData.seatMemory != null)
                {
                    seatControlData.memory = addSeatControlData.seatMemory;
                }
            
            addSeatControlDataCD.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddSeatControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addSeatControlDataCD.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void SeatControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SeatControlDataButton.IsEnabled = (bool)SeatControlDataCheckBox.IsChecked;
        }

        private void AudioControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AudioControlDataButton.IsEnabled = (bool)AudioControlDataCheckBox.IsChecked;
        }

        private async void AudioControlDataTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addAudioControlDataCD = new ContentDialog();

            var addHmiSettingControlData = new src.Rpc.RC.AddAudioControlData(audioControlData, null);
            addAudioControlDataCD.Content = addHmiSettingControlData;

            addAudioControlDataCD.PrimaryButtonText = "Ok";
            addAudioControlDataCD.PrimaryButtonClick += AddAudioControlDataCD_PrimaryButtonClick;

            addAudioControlDataCD.SecondaryButtonText = "Cancel";
            addAudioControlDataCD.SecondaryButtonClick += AddAudioControlDataCD_SecondaryButtonClick; ;

            await addAudioControlDataCD.ShowAsync();
        }

        private async void AddAudioControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var audiControlData = sender.Content as src.Rpc.RC.AddAudioControlData;
            audioControlData = new AudioControlData();

            if (audiControlData.sourceCheckBox.IsChecked == true)
            {
                audioControlData.source = (PrimaryAudioSource)audiControlData.sourceComboBox.SelectedIndex;
            }
            if (audiControlData.keepContextCheckBox.IsChecked == true)
            {
                audioControlData.keepContext = audiControlData.keepContextToggle.IsOn;
            }
             
            if (audiControlData.volumeCheckBox.IsChecked == true)
            {
                try
                {
                    audioControlData.volume = Int32.Parse(audiControlData.volumeTextBox.Text);
                }
                catch (Exception e) { }
            }
            if (audiControlData.addeqCB.IsChecked == true)
            {
                List<EqualizerSettings> list = new List<EqualizerSettings>();
                list.AddRange(audiControlData.eqList);
                audioControlData.equalizerSettings = list;
            }


            sender.Hide();

            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddAudioControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void HMISettingsControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HMISettingsControlDataButton.IsEnabled = (bool)HMISettingsControlDataCheckBox.IsChecked;
        }

        private async void HMISettingsControlDataTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addHmiSettingControlDataCD = new ContentDialog();

            var addHmiSettingControlData = new src.Rpc.RC.HMISettingsControlData(hmiSettingsControlData);
            addHmiSettingControlDataCD.Content = addHmiSettingControlData;

            addHmiSettingControlDataCD.PrimaryButtonText = "Ok";
            addHmiSettingControlDataCD.PrimaryButtonClick += AddHmiSettingControlDataCD_PrimaryButtonClick;

            addHmiSettingControlDataCD.SecondaryButtonText = "Cancel";
            addHmiSettingControlDataCD.SecondaryButtonClick += AddHmiSettingControlDataCD_SecondaryButtonClick; ;

            await addHmiSettingControlDataCD.ShowAsync();
        }

        private async void AddHmiSettingControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var hmiSetControlData = sender.Content as src.Rpc.RC.HMISettingsControlData;
            hmiSettingsControlData = new HMISettingsControlData();

            if (hmiSetControlData.displayModeCheckBox.IsChecked == true)
            {
                hmiSettingsControlData.displayMode = (DisplayMode)hmiSetControlData.displayModeComboBox.SelectedIndex;
            }

            if (hmiSetControlData.temperatureUnitCheckBox.IsChecked == true)
            {
                hmiSettingsControlData.temperatureUnit = (TemperatureUnit)hmiSetControlData.temperatureUnitComboBox.SelectedIndex;
            }

            if (hmiSetControlData.distanceUnitCheckBox.IsChecked == true)
            {
                hmiSettingsControlData.distanceUnit = (DistanceUnit)hmiSetControlData.distanceUnitComboBox.SelectedIndex;
            }
            sender.Hide();

            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddHmiSettingControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void LightControlDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LightControlDataButton.IsEnabled = (bool)LightControlDataCheckBox.IsChecked;
        }

        private async void LightControlDataTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetInteriorVehicleDataCD.Hide();

            addLightControlDataCD = new ContentDialog();

            var addLightControlData = new src.Rpc.RC.LightControlData(lightControlData, null); ;
            addLightControlDataCD.Content = addLightControlData;

            addLightControlDataCD.PrimaryButtonText = "Ok";
            addLightControlDataCD.PrimaryButtonClick += AddLightControlDataCD_PrimaryButtonClick;

            addLightControlDataCD.SecondaryButtonText = "Cancel";
            addLightControlDataCD.SecondaryButtonClick += AddLightControlDataCD_SecondaryButtonClick; ;

            await addLightControlDataCD.ShowAsync();
        }

        private async void AddLightControlDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLightState = sender.Content as src.Rpc.RC.LightControlData;
            if ((bool)addLightState.lightStateCheckBox.IsChecked)
            {
                lightControlData = new LightControlData();
                lightControlData.lightState = new List<LightState>();
                lightControlData.lightState.AddRange(addLightState.LightStateCollection);
            }
            else if (addLightState.lightStateList != null)
            {
                addLightState.lightStateList.IsEnabled = false;

            }
               

            sender.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private async void AddLightControlDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await rcGetInteriorVehicleDataCD.ShowAsync();
        }

        private void IsSubscribedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IsSubscribedToggle.IsEnabled = (bool)IsSubscribedCheckBox.IsChecked;

        }
    }
}
