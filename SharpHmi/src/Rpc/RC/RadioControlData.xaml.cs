﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Common.Enums;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RadioControlData : UserControl
    {
        public CheckBox frequencyIntegerCB;
        public TextBox frequencyIntegerTB;
        public CheckBox frequencyFractionCB;
        public TextBox frequencyFractionTB;
        public CheckBox radioBandCB;
        public ComboBox radioBandComboBox;
        public CheckBox rdsDataCB;
        public CheckBox psCB;
        public TextBox psTB;
        public CheckBox rtCB;
        public TextBox rtTB;
        public CheckBox ctCB;
        public TextBox ctTB;
        public CheckBox piCB;
        public TextBox piTB;
        public CheckBox ptyCB;
        public TextBox ptyTB;
        public CheckBox tpCB;
        public CheckBox taCB;
        public ToggleSwitch tpTgl;
        public ToggleSwitch taTgl;
        public CheckBox regCB;
        public TextBox regTB;
        public CheckBox availableHDsCB;
        public TextBox availableHDsTB;
        public CheckBox hdChannelCB;
        public TextBox hdChannelTB;
        public CheckBox signalStrengthCb;
        public TextBox signalStrengthTB;
        public CheckBox signalChangeThresholdCB;
        public TextBox signalChangeThresholdTB;
        public CheckBox RadioEnabledCB;
        public ToggleSwitch RadioEnabledTgl;
        public CheckBox sisDataCheckBox;
        public CheckBox radioStateCB;
        public ComboBox radioStateComboBox;
        public static ContentDialog sisDataCD = new ContentDialog();
        public static ContentDialog stationLocationCD = new ContentDialog();

        public HmiApiLib.Common.Structs.SisData sisData = new HmiApiLib.Common.Structs.SisData();

        String calledFrom = "";
        public RadioControlData(String calledFrom ,HmiApiLib.Common.Structs.RadioControlData radioControlData)
        {
            this.InitializeComponent();
            this.calledFrom = calledFrom;
            RadioBandComboBox.ItemsSource = Enum.GetNames(typeof(RadioBand));
            RadioBandComboBox.SelectedIndex = 0;

            RadioStateComboBox.ItemsSource = Enum.GetNames(typeof(RadioState));
            RadioStateComboBox.SelectedIndex = 0;

            if (radioControlData != null)
            {
                if (radioControlData.getFrequencyInteger() != null)
                    FrequencyIntegerTextBox.Text = radioControlData.getFrequencyInteger().ToString();
                else {
                    FrequencyIntegerTextBox.IsEnabled = false;
                    FrequencyIntegerCheckBox.IsChecked = false;
                }
                if (radioControlData.getFrequencyFraction() != null)
                    FrequencyFractionTextBox.Text = radioControlData.getFrequencyFraction().ToString();
                else {
                    FrequencyFractionTextBox.IsEnabled = false;
                    FrequencyFractionCheckBox.IsChecked = false;
                }
                if (radioControlData.getBand() != null)
                    RadioBandComboBox.SelectedIndex = (int)radioControlData.getBand();
                else {
                    RadioBandComboBox.IsEnabled = false;
                    RadioBandCheckBox.IsChecked = false;
                }



                if (radioControlData.getRdsData() != null)
                {
                    if (radioControlData.getRdsData().PS != null)
                        PSTextBox.Text = radioControlData.getRdsData().PS;
                    else
                    {
                        PSTextBox.IsEnabled = false;
                        PSCheckBox.IsChecked = false;
                    }

                    if (radioControlData.getRdsData().RT != null)
                        RTTextBox.Text = radioControlData.getRdsData().RT;
                    else
                    {
                        RTTextBox.IsEnabled = false;
                        RTCheckBox.IsChecked = false;

                    }
                    if (radioControlData.getRdsData().CT != null)
                        CTTextBox.Text = radioControlData.getRdsData().CT;
                    else
                    {
                        CTTextBox.IsEnabled = false;
                        CTCheckBox.IsChecked = false;
                    }
                    if (radioControlData.getRdsData().PI != null)
                        PITextBox.Text = radioControlData.getRdsData().PI;
                    else
                    {
                        PITextBox.IsEnabled = false;
                        PICheckBox.IsChecked = false;
                    }
                    if (radioControlData.getRdsData().PTY.ToString() != null)
                        PTYTextBox.Text = radioControlData.getRdsData().PTY.ToString();
                    else
                    {
                        PTYTextBox.IsEnabled = false;
                        PTYCheckBox.IsChecked = false;
                    }

                    if (radioControlData.getRdsData().TP == null)
                    {
                        TPCheckBox.IsChecked = false;
                        TPToggle.IsEnabled = false;
                    }
                    else
                    {
                        TPToggle.IsOn = (bool)radioControlData.getRdsData().TP;
                    }
                    if (radioControlData.getRdsData().TA == null)
                    {
                        TACheckBox.IsChecked = false;
                        TAToggle.IsEnabled = false;
                    }
                    else
                    {
                        TAToggle.IsOn = (bool)radioControlData.getRdsData().TA;
                    }

                    if (radioControlData.getRdsData().REG != null)
                        REGTextBox.Text = radioControlData.getRdsData().REG;
                    else
                    {
                        REGTextBox.IsEnabled = false;
                        REGCheckBox.IsChecked = false;
                    }
                }
                else {
                    RDSDataCheckBox.IsChecked = false;
                    PSCheckBox.IsEnabled = false;
                    PSTextBox.IsEnabled = false;

                    RTCheckBox.IsEnabled = false;
                    RTTextBox.IsEnabled = false;

                    CTCheckBox.IsEnabled = false;
                    CTTextBox.IsEnabled = false;


                    PICheckBox.IsEnabled = false;
                    PITextBox.IsEnabled = false;

                    PTYCheckBox.IsEnabled = false;
                    PTYTextBox.IsEnabled = false;

                    TPCheckBox.IsEnabled = false;
                    TPToggle.IsEnabled = false;

                    TACheckBox.IsEnabled = false;
                    TAToggle.IsEnabled = false;

                    REGCheckBox.IsEnabled = false;
                    REGTextBox.IsEnabled = false;
                }
                if (radioControlData.getHdChannel() != null)
                    HDChannelTextBox.Text = radioControlData.getHdChannel().ToString();
                else {
                    HDChannelTextBox.IsEnabled = false;
                    HDChannelCheckBox.IsChecked = false;
                }
                if (radioControlData.getAvailableHDs() != null)
                    AvailableHDsTextBox.Text = radioControlData.getAvailableHDs().ToString();
                else
                {
                    AvailableHDsTextBox.IsEnabled = false;
                    AvailableHDsCheckBox.IsChecked = false;
                }
                if (radioControlData.getSignalStrength() != null)
                    SignalStrengthTextBox.Text = radioControlData.getSignalStrength().ToString();
                else
                {
                    SignalStrengthTextBox.IsEnabled = false;
                    SignalStrengthCheckBox.IsChecked = false;
                }
                if (radioControlData.getSignalChangeThreshold() != null)
                    SignalChangeThresholdTextBox.Text = radioControlData.getSignalChangeThreshold().ToString();
                else
                {
                    SignalChangeThresholdTextBox.IsEnabled = false;
                    SignalChangeThresholdCheckBox.IsChecked = false;
                }
                

                if (radioControlData.getRadioEnable() == null)
                {
                    RadioEnableCheckBox.IsChecked = false;
                    RadioEnableToggle.IsEnabled = false;
                }
                else
                {
                    RadioEnableCheckBox.IsChecked = true;
                    RadioEnableToggle.IsOn = (bool)radioControlData.getRadioEnable();
                }


                if (radioControlData.getState() != null)
                    RadioStateComboBox.SelectedIndex = (int)radioControlData.getState();
                else {
                    RadioStateComboBox.IsEnabled = false;
                    RadioStateCheckBox.IsChecked = false;
                }
                if (radioControlData.getSisData() != null)
                    sisData =radioControlData.getSisData();
                else
                {
                    SisDataButton.IsEnabled = false;
                    SisDataCheckBox.IsChecked = false;
                }



                frequencyIntegerCB = FrequencyIntegerCheckBox;
                frequencyIntegerTB = FrequencyIntegerTextBox;
                frequencyFractionCB = FrequencyFractionCheckBox;
                frequencyFractionTB = FrequencyFractionTextBox;
                radioBandCB = RadioBandCheckBox;
                radioBandComboBox = RadioBandComboBox;
                rdsDataCB = RDSDataCheckBox;
                psCB = PSCheckBox;
                psTB = PSTextBox;
                rtCB = RTCheckBox;
                rtTB = RTTextBox;
                ctCB = CTCheckBox;
                ctTB = CTTextBox;
                piCB = PICheckBox;
                piTB = PITextBox;
                ptyCB = PTYCheckBox;
                ptyTB = PTYTextBox;
                tpCB = TPCheckBox;
                taCB = TACheckBox;
                tpTgl = TPToggle;
                taTgl = TAToggle;
                regCB = REGCheckBox;
                regTB = REGTextBox;
                availableHDsCB = AvailableHDsCheckBox;
                availableHDsTB = AvailableHDsTextBox;
                hdChannelCB = HDChannelCheckBox;
                hdChannelTB = HDChannelTextBox;
                signalStrengthCb = SignalStrengthCheckBox;
                signalStrengthTB = SignalStrengthTextBox;
                signalChangeThresholdCB = SignalChangeThresholdCheckBox;
                signalChangeThresholdTB = SignalChangeThresholdTextBox;
                RadioEnabledCB = RadioEnableCheckBox;
                RadioEnabledTgl = RadioEnableToggle;
                sisDataCheckBox = SisDataCheckBox;
                radioStateCB = RadioStateCheckBox;
                radioStateComboBox = RadioStateComboBox;
            }
    }

        private void FrequencyIntegerCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (FrequencyIntegerCheckBox.IsChecked == true)
                FrequencyIntegerTextBox.IsEnabled = true;
            else
                FrequencyIntegerTextBox.IsEnabled = false;
        }

        private void FrequencyFractionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (FrequencyFractionCheckBox.IsChecked == true)
                FrequencyFractionTextBox.IsEnabled = true;
            else
                FrequencyFractionTextBox.IsEnabled = false;
        }

        private void RadioBandCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RadioBandCheckBox.IsChecked == true)
                RadioBandComboBox.IsEnabled = true;
            else
                RadioBandComboBox.IsEnabled = false;
        }

        private void RDSDataCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RDSDataCheckBox.IsChecked == true)
            {
                PSCheckBox.IsEnabled = true;

                if (PSCheckBox.IsChecked == true)
                    PSTextBox.IsEnabled = true;
                else
                    PSTextBox.IsEnabled = false;


                RTCheckBox.IsEnabled = true;

                if (RTCheckBox.IsChecked == true)
                    RTTextBox.IsEnabled = true;
                else
                    RTTextBox.IsEnabled = false;

                CTCheckBox.IsEnabled = true;
                if (CTCheckBox.IsChecked == true)
                    CTTextBox.IsEnabled = true;
                else
                    CTTextBox.IsEnabled = false;


                PICheckBox.IsEnabled = true;

                if (PICheckBox.IsChecked == true)
                    PITextBox.IsEnabled = true;
                else
                    PITextBox.IsEnabled = false;


                PTYCheckBox.IsEnabled = true;

                if (PTYCheckBox.IsChecked == true)
                    PTYTextBox.IsEnabled = true;
                else
                    PTYTextBox.IsEnabled = false;

                if (TPCheckBox.IsEnabled == true)
                    TPToggle.IsEnabled = true;
                else
                    TPToggle.IsEnabled = false;
                if (TACheckBox.IsEnabled == true)
                    TAToggle.IsEnabled = true;
                else
                    TAToggle.IsEnabled = false;
                


                REGCheckBox.IsEnabled = true;

                if (REGCheckBox.IsChecked == true)
                    REGTextBox.IsEnabled = true;
                else
                    REGTextBox.IsEnabled = false;

            }
            else
            {
                PSCheckBox.IsEnabled = false;
                PSTextBox.IsEnabled = false;

                RTCheckBox.IsEnabled = false;
                RTTextBox.IsEnabled = false;

                CTCheckBox.IsEnabled = false;
                CTTextBox.IsEnabled = false;


                PICheckBox.IsEnabled = false;
                PITextBox.IsEnabled = false;

                PTYCheckBox.IsEnabled = false;
                PTYTextBox.IsEnabled = false;

                TPCheckBox.IsEnabled = false;
                TPToggle.IsEnabled = false;

                TACheckBox.IsEnabled = false;
                TAToggle.IsEnabled = false;

                REGCheckBox.IsEnabled = false;
                REGTextBox.IsEnabled = false;
            }
        }

        private void PSCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (PSCheckBox.IsChecked == true)
                PSTextBox.IsEnabled = true;
            else
                PSTextBox.IsEnabled = false;
        }

        private void RTCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RTCheckBox.IsChecked == true)
                RTTextBox.IsEnabled = true;
            else
                RTTextBox.IsEnabled = false;
        }

        private void CTCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (CTCheckBox.IsChecked == true)
                CTTextBox.IsEnabled = true;
            else
                CTTextBox.IsEnabled = false;
        }

        private void PICBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (PICheckBox.IsChecked == true)
                PITextBox.IsEnabled = true;
            else
                PITextBox.IsEnabled = false;
        }

        private void PTYCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (PTYCheckBox.IsChecked == true)
                PTYTextBox.IsEnabled = true;
            else
                PTYTextBox.IsEnabled = false;
        }

        private void REGCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (REGCheckBox.IsChecked == true)
                REGTextBox.IsEnabled = true;
            else
                REGTextBox.IsEnabled = false;
        }

        private void AvailableHDsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AvailableHDsCheckBox.IsChecked == true)
                AvailableHDsTextBox.IsEnabled = true;
            else
                AvailableHDsTextBox.IsEnabled = false;
        }

        private void HDChannelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (HDChannelCheckBox.IsChecked == true)
                HDChannelTextBox.IsEnabled = true;
            else
                HDChannelTextBox.IsEnabled = false;
        }

        private void SignalStrengthCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (SignalStrengthCheckBox.IsChecked == true)
                SignalStrengthTextBox.IsEnabled = true;
            else
                SignalStrengthTextBox.IsEnabled = false;
        }

        private void SignalChangeThresholdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (SignalChangeThresholdCheckBox.IsChecked == true)
                SignalChangeThresholdTextBox.IsEnabled = true;
            else
                SignalChangeThresholdTextBox.IsEnabled = false;
        }

        private void RadioStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RadioStateCheckBox.IsChecked == true)
                RadioStateComboBox.IsEnabled = true;
            else
                RadioStateComboBox.IsEnabled = false;
        }

        private void SisDataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SisDataButton.IsEnabled = (bool)SisDataCheckBox.IsChecked;

        }

        private async void SisDataTapped(object sender, TappedRoutedEventArgs e)
        {
            
                if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                {
                    RCSetInteriorVehicleDataResponse.addRadioControlDataCD.Hide();
                }
                else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                {
                    RCGetInteriorVehicleDataResponse.addRadioControlDataCD.Hide();
                }
                else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                {
                    RCOnInteriorVehicleData.addRadioControlDataCD.Hide();
                }

            sisDataCD = new ContentDialog();
            var addSisData = new src.Rpc.RC.SisData(sisData);
            sisDataCD.Content = addSisData;
            sisDataCD.PrimaryButtonText = "Ok";
            sisDataCD.PrimaryButtonClick += sisDataCD_PrimaryButtonClick;
            sisDataCD.SecondaryButtonText = "Cancel";
            sisDataCD.SecondaryButtonClick += sisDataCD_SecondaryButtonClick;

                await sisDataCD.ShowAsync();
            }
        private async void sisDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addRadioControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addRadioControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addRadioControlDataCD.ShowAsync();
        }
        private async void sisDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
           

            var mmd = sender.Content as src.Rpc.RC.SisData;
            sisData = new HmiApiLib.Common.Structs.SisData();

            if (mmd.stationShortNameCheckBox.IsChecked == true)
                sisData.stationShortName = mmd.stationShortNameTextBox.Text;

            if (mmd.stationIDNumberCheckBox.IsChecked == true) {
                
                sisData.stationIDNumber = mmd.stationIDNumber;
                }
            if (mmd.stationLongNameCheckBox.IsChecked == true)
                sisData.stationLongName = mmd.stationLongNameTextBox.Text;
           
            if (mmd.stationMessageCheckBox.IsChecked == true)
                sisData.stationMessage = mmd.stationMessageTextBox.Text;
            if (mmd.stationLocationCheckBox.IsChecked == true)
            {
                
                sisData.stationLocation = mmd.gpsData;
            }


            sender.Hide();
            if (calledFrom.Equals("RCSetInteriorVehicleDataResponse"))
                await RCSetInteriorVehicleDataResponse.addRadioControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCGetInteriorVehicleDataResponse"))
                await RCGetInteriorVehicleDataResponse.addRadioControlDataCD.ShowAsync();
            else if (calledFrom.Equals("RCOnInteriorVehicleDataResponse"))
                await RCOnInteriorVehicleData.addRadioControlDataCD.ShowAsync();
        }

        private void TACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TAToggle.IsEnabled = (bool)TACheckBox.IsChecked;

        }

        private void TPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TPToggle.IsEnabled = (bool)TPCheckBox.IsChecked;

        }
        private void RadioEnableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RadioEnableToggle.IsEnabled = (bool)RadioEnableCheckBox.IsChecked;

        }
        
    }
}
