﻿using SharpHmi.Buttons;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.RC.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using System.Collections.ObjectModel;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RCGetCapabilitiesResponse : UserControl
    {
        GetCapabilities tmpObj = null;

        ObservableCollection<HmiApiLib.Common.Structs.ClimateControlCapabilities> climateControlCap = new ObservableCollection<HmiApiLib.Common.Structs.ClimateControlCapabilities>();
        ObservableCollection<HmiApiLib.Common.Structs.RadioControlCapabilities> radioControlCap = new ObservableCollection<HmiApiLib.Common.Structs.RadioControlCapabilities>();
        ObservableCollection<ButtonCapabilities> btnCap = new ObservableCollection<ButtonCapabilities>();
        ObservableCollection<SeatControlCapabilities> seatControlCap = new ObservableCollection<SeatControlCapabilities>();
        ObservableCollection<AudioControlCapabilities> audioControlCap = new ObservableCollection<AudioControlCapabilities>();
        HmiApiLib.Common.Structs.HMISettingsControlCapabilities hmiSettingsControlCap = new HMISettingsControlCapabilities();
        HmiApiLib.Common.Structs.LightControlCapabilities lightControlCap = new HmiApiLib.Common.Structs.LightControlCapabilities();


        static ContentDialog rcGetCapabilitiesCD = new ContentDialog();

        public static ContentDialog addClimateControlCapabilitiesCD = new ContentDialog();
        static ContentDialog addRadioControlCapabilitiesCD = new ContentDialog();
        static ContentDialog addButtonCapabilitiesCD = new ContentDialog();
        static ContentDialog addSeatControlCapabilitiesCD = new ContentDialog();
        static ContentDialog addAudioControlCapabilitiesCD = new ContentDialog();
        static ContentDialog addHMISettingsControlCapabilitiesCD = new ContentDialog();
        public static ContentDialog addLightControlCapabilitiesCD = new ContentDialog();

        public RCGetCapabilitiesResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result)); 
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetCapabilities()
        {
            rcGetCapabilitiesCD = new ContentDialog();

            var rcGetCapabilities = this;
            rcGetCapabilitiesCD.Content = rcGetCapabilities;

            rcGetCapabilitiesCD.PrimaryButtonText = Const.TxLater; ;
            rcGetCapabilitiesCD.PrimaryButtonClick += RcGetCapabilitiesCD_PrimaryButtonClick;

            rcGetCapabilitiesCD.CloseButtonText = Const.Close;
            rcGetCapabilitiesCD.CloseButtonClick += RcGetCapabilitiesCD_CloseButtonClick;

            rcGetCapabilitiesCD.SecondaryButtonText = Const.Reset;
            rcGetCapabilitiesCD.SecondaryButtonClick += RcGetCapabilitiesCD_ResetButtonClick;

            tmpObj = new GetCapabilities();
            tmpObj = (GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<GetCapabilities>(tmpObj.getMethod());
            
            if(tmpObj == null)
            {
                Type type = typeof(GetCapabilities);
                tmpObj = (GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getRemoteControlCapabilities() != null)
                {
                    if (tmpObj.getRemoteControlCapabilities().climateControlCapabilities != null)
                    {
                        foreach (HmiApiLib.Common.Structs.ClimateControlCapabilities cap in tmpObj.getRemoteControlCapabilities().climateControlCapabilities)
                        {
                            climateControlCap.Add(cap);
                        }
                    }
                    else
                    {
                        ClimateControlCapabilitiesCheckBox.IsChecked = false;
                        ClimateControlCapabilitiesButton.IsEnabled = false;
                    }

                    if (tmpObj.getRemoteControlCapabilities().radioControlCapabilities != null)
                    {
                        foreach (HmiApiLib.Common.Structs.RadioControlCapabilities cap in tmpObj.getRemoteControlCapabilities().radioControlCapabilities)
                        {
                            radioControlCap.Add(cap);
                        }
                    }
                    else
                    {
                        RadioControlCapabilitiesCheckBox.IsChecked = false;
                        RadioControlCapabilitiesButton.IsEnabled = false;
                    }
                    if (tmpObj.getRemoteControlCapabilities().buttonCapabilities != null)
                    {
                        foreach (ButtonCapabilities cap in tmpObj.getRemoteControlCapabilities().buttonCapabilities)
                        {
                            btnCap.Add(cap);
                        }
                    }
                    else
                    {

                        ButtonCapabilitiesCheckBox.IsChecked = false;
                        ButtonCapabilitiesButton.IsEnabled = false;

                    }
                    if (tmpObj.getRemoteControlCapabilities().seatControlCapabilities != null)
                    {
                        foreach (SeatControlCapabilities cap in tmpObj.getRemoteControlCapabilities().seatControlCapabilities)
                        {
                            seatControlCap.Add(cap);
                        }
                    }
                    else
                    {
                        SeatControlCapabilitiesCheckBox.IsChecked = false;
                        SeatControlCapabilitiesButton.IsEnabled = false;
                    }
                    if (tmpObj.getRemoteControlCapabilities().audioControlCapabilities != null)
                    {
                        foreach (AudioControlCapabilities cap in tmpObj.getRemoteControlCapabilities().audioControlCapabilities)
                        {
                            audioControlCap.Add(cap);
                        }
                    }
                    else
                    {
                        AudioControlCapabilitiesCheckBox.IsChecked = false;
                        AudioControlCapabilitiesButton.IsEnabled = false;
                    }
                    if (tmpObj.getRemoteControlCapabilities().hmiSettingsControlCapabilities != null)
                    {
                        hmiSettingsControlCap = tmpObj.getRemoteControlCapabilities().hmiSettingsControlCapabilities;
                    }
                    else
                    {
                        HMISettingsControlCapabilitiesButton.IsEnabled = false;
                        HMISettingsControlCapabilitiesCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getRemoteControlCapabilities().lightControlCapabilities != null)
                    {
                        lightControlCap = tmpObj.getRemoteControlCapabilities().lightControlCapabilities;
                    }
                    else
                    {

                        LightControlCapabilitiesButton.IsEnabled = false;
                        LightControlCapabilitiesCheckBox.IsChecked = false;
                    }
                }
                else {
                    RemoteControlCapabilitiesCheckBox.IsChecked = false;

                    ClimateControlCapabilitiesCheckBox.IsEnabled = false;
                    ClimateControlCapabilitiesButton.IsEnabled = false;

                    RadioControlCapabilitiesCheckBox.IsEnabled = false;
                    RadioControlCapabilitiesButton.IsEnabled = false;

                    ButtonCapabilitiesCheckBox.IsEnabled = false;
                    ButtonCapabilitiesButton.IsEnabled = false;

                    SeatControlCapabilitiesCheckBox.IsEnabled = false;
                    SeatControlCapabilitiesButton.IsEnabled = false;

                    AudioControlCapabilitiesCheckBox.IsEnabled = false;
                    AudioControlCapabilitiesButton.IsEnabled = false;

                    HMISettingsControlCapabilitiesButton.IsEnabled = false;
                    HMISettingsControlCapabilitiesCheckBox.IsEnabled = false;

                    LightControlCapabilitiesButton.IsEnabled = false;
                    LightControlCapabilitiesCheckBox.IsEnabled = false;

                }

                ClimateControlCapabilitiesListView.ItemsSource = climateControlCap;
                RadioControlCapabilitiesListView.ItemsSource = radioControlCap;
                ButtonCapabilitiesListView.ItemsSource = btnCap;
                SeatControlCapabilitiesListView.ItemsSource = seatControlCap;
                AudioControlCapabilitiesListView.ItemsSource = audioControlCap;

            }
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private void RcGetCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RemoteControlCapabilities remoteControlCap = null;
            List<HmiApiLib.Common.Structs.ClimateControlCapabilities> climateCap = null;
            List<HmiApiLib.Common.Structs.RadioControlCapabilities> radioCap = null;
            List<HmiApiLib.Common.Structs.ButtonCapabilities> buttonCap = null;
            List<HmiApiLib.Common.Structs.SeatControlCapabilities> seatCap = null;
            List<HmiApiLib.Common.Structs.AudioControlCapabilities> audioCap = null;
            HmiApiLib.Common.Structs.HMISettingsControlCapabilities hmiCap = null;
            HmiApiLib.Common.Structs.LightControlCapabilities lightCap = null;

            HmiApiLib.Common.Enums.Result? resultCode = null;


            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            if (RemoteControlCapabilitiesCheckBox.IsChecked == true)
            {
                remoteControlCap = new RemoteControlCapabilities();

                if (ClimateControlCapabilitiesCheckBox.IsChecked == true)
                {
                    climateCap = new List<HmiApiLib.Common.Structs.ClimateControlCapabilities>();
                    climateCap.AddRange(climateControlCap);
                    remoteControlCap.climateControlCapabilities = climateCap;
                }
                if (RadioControlCapabilitiesCheckBox.IsChecked == true)
                {
                    radioCap = new List<HmiApiLib.Common.Structs.RadioControlCapabilities>();
                    radioCap.AddRange(radioControlCap);
                    remoteControlCap.radioControlCapabilities = radioCap;
                }
                if (ButtonCapabilitiesCheckBox.IsChecked == true)
                {
                    buttonCap = new List<ButtonCapabilities>();
                    buttonCap.AddRange(btnCap);
                    remoteControlCap.buttonCapabilities = buttonCap;
                }
                if (SeatControlCapabilitiesCheckBox.IsChecked == true)
                {
                    seatCap = new List<SeatControlCapabilities>();
                    seatCap.AddRange(seatControlCap);
                    remoteControlCap.seatControlCapabilities = seatCap;
                }
                if (AudioControlCapabilitiesCheckBox.IsChecked == true)
                {
                    audioCap = new List<AudioControlCapabilities>();
                    audioCap.AddRange(audioControlCap);
                    remoteControlCap.audioControlCapabilities = audioCap;
                }
                if (HMISettingsControlCapabilitiesCheckBox.IsChecked == true)
                {
                    remoteControlCap.hmiSettingsControlCapabilities = hmiSettingsControlCap;
                }
                if (LightControlCapabilitiesCheckBox.IsChecked == true)
                {
                    remoteControlCap.lightControlCapabilities = lightControlCap;
                }
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildRcGetCapabilitiesResponse(BuildRpc.getNextId(), resultCode, remoteControlCap);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void RcGetCapabilitiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if(tmpObj != null)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void RcGetCapabilitiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }


        private void RemoteControlCapabilityCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if(RemoteControlCapabilitiesCheckBox.IsChecked == false)
            {
                ClimateControlCapabilitiesCheckBox.IsEnabled = false;
                ClimateControlCapabilitiesButton.IsEnabled = false;

                RadioControlCapabilitiesCheckBox.IsEnabled = false;
                RadioControlCapabilitiesButton.IsEnabled = false;

                ButtonCapabilitiesCheckBox.IsEnabled = false;
                ButtonCapabilitiesButton.IsEnabled = false;
            }
            else
            {
                ClimateControlCapabilitiesCheckBox.IsEnabled = true;
                RadioControlCapabilitiesCheckBox.IsEnabled = true;
                ButtonCapabilitiesCheckBox.IsEnabled = true;

                if (ClimateControlCapabilitiesCheckBox.IsChecked == true)
                    ClimateControlCapabilitiesButton.IsEnabled = true;
                else
                    ClimateControlCapabilitiesButton.IsEnabled = false;

                if (RadioControlCapabilitiesCheckBox.IsChecked == true)
                    RadioControlCapabilitiesButton.IsEnabled = true;
                else
                    RadioControlCapabilitiesButton.IsEnabled = false;

                if (ButtonCapabilitiesCheckBox.IsChecked == true)
                    ButtonCapabilitiesButton.IsEnabled = true;
                else
                    ButtonCapabilitiesButton.IsEnabled = false;
            }
        }

        private void ClimateControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ClimateControlCapabilitiesCheckBox.IsChecked == true)
                ClimateControlCapabilitiesButton.IsEnabled = true;
            else
                ClimateControlCapabilitiesButton.IsEnabled = false;
        }

        private void RadioControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RadioControlCapabilitiesCheckBox.IsChecked == true)
                RadioControlCapabilitiesButton.IsEnabled = true;
            else
                RadioControlCapabilitiesButton.IsEnabled = false;
        }

        private void ButtonCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ButtonCapabilitiesCheckBox.IsChecked == true)
                ButtonCapabilitiesButton.IsEnabled = true;
            else
                ButtonCapabilitiesButton.IsEnabled = false;
        }

        private async void AddClimateControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addClimateControlCapabilitiesCD = new ContentDialog();

            var addClimateControlCapabilities = new ClimateControlCapabilities();
            addClimateControlCapabilitiesCD.Content = addClimateControlCapabilities;

            addClimateControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addClimateControlCapabilitiesCD.PrimaryButtonClick += AddClimateControlCapabilitiesCD_PrimaryButtonClick;

            addClimateControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addClimateControlCapabilitiesCD.SecondaryButtonClick += AddClimateControlCapabilitiesCD_SecondaryButtonClick;

            await addClimateControlCapabilitiesCD.ShowAsync();
        }

        private async void AddClimateControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addClimateControlCapabilities = sender.Content as ClimateControlCapabilities;

            HmiApiLib.Common.Structs.ClimateControlCapabilities climateCap = new HmiApiLib.Common.Structs.ClimateControlCapabilities();

            if (addClimateControlCapabilities.moduleNameCheckBox.IsChecked == true)
            {
                climateCap.moduleName = addClimateControlCapabilities.moduleName.Text;
            }

            climateCap.currentTemperatureAvailable = (bool)addClimateControlCapabilities.currentTempAvailableCB.IsChecked;
            climateCap.fanSpeedAvailable = (bool)addClimateControlCapabilities.fanSpeedAvailableCB.IsChecked;
            climateCap.desiredTemperatureAvailable = (bool)addClimateControlCapabilities.desiredTemperatureAvailableCB.IsChecked;
            climateCap.acEnableAvailable = (bool)addClimateControlCapabilities.acEnableAvailableCB.IsChecked;
            climateCap.acMaxEnableAvailable = (bool)addClimateControlCapabilities.acMaxEnableAvailable.IsChecked;
            climateCap.circulateAirEnableAvailable = (bool)addClimateControlCapabilities.circulateAirEnableAvailableCB.IsChecked;
            climateCap.autoModeEnableAvailable = (bool)addClimateControlCapabilities.autoModeEnableAvailableCB.IsChecked;
            climateCap.dualModeEnableAvailable = (bool)addClimateControlCapabilities.dualModeEnableAvailableCB.IsChecked;
            climateCap.ventilationModeAvailable = (bool)addClimateControlCapabilities.ventilationModeAvailableCB.IsChecked;

            if(addClimateControlCapabilities.defrostZoneAvailableCB.IsChecked == true)
            {
                if (climateCap.defrostZone == null)
                    climateCap.defrostZone = new List<HmiApiLib.Common.Enums.DefrostZone>();

                climateCap.defrostZone.AddRange(addClimateControlCapabilities.defrostZoneCollection);
            }

            climateCap.currentTemperatureAvailable = (bool)addClimateControlCapabilities.currentTempAvailableCB.IsChecked;

            if(addClimateControlCapabilities.ventilationModeAvailableCB.IsChecked == true)
            {
                if (climateCap.ventilationMode == null)
                    climateCap.ventilationMode = new List<HmiApiLib.Common.Enums.VentilationMode>();

                climateCap.ventilationMode.AddRange(addClimateControlCapabilities.ventilationModeCollection);
            }

            climateControlCap.Add(climateCap);

            addClimateControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddClimateControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addClimateControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }


        private async void AddRadioControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addRadioControlCapabilitiesCD = new ContentDialog();

            var addRadioControlCapabilities = new RadioControlCapabilities();
            addRadioControlCapabilitiesCD.Content = addRadioControlCapabilities;

            addRadioControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addRadioControlCapabilitiesCD.PrimaryButtonClick += AddRadioControlCapabilitiesCD_PrimaryButtonClick;

            addRadioControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addRadioControlCapabilitiesCD.SecondaryButtonClick += AddRadioControlCapabilitiesCD_SecondaryButtonClick;

            await addRadioControlCapabilitiesCD.ShowAsync();
        }

        private async void AddRadioControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addRadioControlCapabilities = sender.Content as RadioControlCapabilities;

            HmiApiLib.Common.Structs.RadioControlCapabilities radioCap = new HmiApiLib.Common.Structs.RadioControlCapabilities();

            if(addRadioControlCapabilities.moduleNameCheckBox.IsChecked == true)
            {
                radioCap.moduleName = addRadioControlCapabilities.moduleName.Text;
            }

            radioCap.radioEnableAvailable = (bool)addRadioControlCapabilities.radioEnableAvailableCB.IsChecked;
            radioCap.radioBandAvailable = (bool)addRadioControlCapabilities.radioBandAvailableCB.IsChecked;
            radioCap.radioFrequencyAvailable = (bool)addRadioControlCapabilities.radioFrequencyAvailableCB.IsChecked;
            radioCap.hdChannelAvailable = (bool)addRadioControlCapabilities.hdChannelAvailableCB.IsChecked;
            radioCap.rdsDataAvailable = (bool)addRadioControlCapabilities.rdsDataAvailableCB.IsChecked;
            radioCap.availableHDsAvailable = (bool)addRadioControlCapabilities.availableHDsAvailableCB.IsChecked;
            radioCap.stateAvailable = (bool)addRadioControlCapabilities.stateAvailableCB.IsChecked;
            radioCap.signalStrengthAvailable = (bool)addRadioControlCapabilities.signalStrengthAvailableCB.IsChecked;
            radioCap.signalChangeThresholdAvailable = (bool)addRadioControlCapabilities.signalChangeThresholdAvailableCB.IsChecked;

            radioControlCap.Add(radioCap);

            addRadioControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddRadioControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addRadioControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addButtonCapabilitiesCD = new ContentDialog();

            var addButtonCapabilities = new AddButtonCapabilities();
            addButtonCapabilitiesCD.Content = addButtonCapabilities;

            addButtonCapabilitiesCD.PrimaryButtonText = "Ok";
            addButtonCapabilitiesCD.PrimaryButtonClick += AddButtonCapabilitiesCD_PrimaryButtonClick;

            addButtonCapabilitiesCD.SecondaryButtonText = "Cancel";
            addButtonCapabilitiesCD.SecondaryButtonClick += AddButtonCapabilitiesCD_SecondaryButtonClick;

            await addButtonCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addButtonCapabilities = sender.Content as AddButtonCapabilities;
            
            ButtonCapabilities buttonCapabilities = new ButtonCapabilities();
            if(addButtonCapabilities.buttonNameCheckBox.IsChecked == true)
                buttonCapabilities.name = (HmiApiLib.ButtonName)addButtonCapabilities.buttonNameComboBox.SelectedIndex;
            buttonCapabilities.shortPressAvailable = (bool)addButtonCapabilities.shortPressAvailableCheckBox.IsChecked;
            buttonCapabilities.longPressAvailable = (bool)addButtonCapabilities.longPressAvailableCheckBox.IsChecked;
            buttonCapabilities.upDownAvailable = (bool)addButtonCapabilities.upDownAvailableCheckBox.IsChecked;

            btnCap.Add(buttonCapabilities);

            addButtonCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddButtonCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addButtonCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ResultCodeCheckBox.IsChecked == true)
            {
                ResultCodeComboBox.IsEnabled = true;
            }
            else
            {
                ResultCodeComboBox.IsEnabled = false;
            }
        }
        
        private void RemoteControlCapabilitiesCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (RemoteControlCapabilitiesCheckBox.IsChecked == true)
            {
                ClimateControlCapabilitiesCheckBox.IsEnabled = true;
                RadioControlCapabilitiesCheckBox.IsEnabled = true;
                ButtonCapabilitiesCheckBox.IsEnabled = true;

                AudioControlCapabilitiesCheckBox.IsEnabled = true;
                SeatControlCapabilitiesCheckBox.IsEnabled = true;
                
                LightControlCapabilitiesCheckBox.IsEnabled = true;
                HMISettingsControlCapabilitiesCheckBox.IsEnabled = true;



                if (ClimateControlCapabilitiesCheckBox.IsChecked == true)
                    ClimateControlCapabilitiesButton.IsEnabled = true;
                else
                    ClimateControlCapabilitiesButton.IsEnabled = false;

                if (RadioControlCapabilitiesCheckBox.IsChecked == true)
                    RadioControlCapabilitiesButton.IsEnabled = true;
                else
                    RadioControlCapabilitiesButton.IsEnabled = false;

                if (ButtonCapabilitiesCheckBox.IsChecked == true)
                    ButtonCapabilitiesButton.IsEnabled = true;
                else
                    ButtonCapabilitiesButton.IsEnabled = false;
                if (SeatControlCapabilitiesCheckBox.IsChecked == true)
                    SeatControlCapabilitiesButton.IsEnabled = true;
                else
                    SeatControlCapabilitiesButton.IsEnabled = false;
                if (AudioControlCapabilitiesCheckBox.IsChecked == true)
                    AudioControlCapabilitiesButton.IsEnabled = true;
                else
                    AudioControlCapabilitiesButton.IsEnabled = false;
                if (HMISettingsControlCapabilitiesCheckBox.IsChecked == true)
                    HMISettingsControlCapabilitiesButton.IsEnabled = true;
                else
                    HMISettingsControlCapabilitiesButton.IsEnabled = false;
                if (LightControlCapabilitiesCheckBox.IsChecked == true)
                    LightControlCapabilitiesButton.IsEnabled = true;
                else
                    LightControlCapabilitiesButton.IsEnabled = false;

            }
            else
            {
                ClimateControlCapabilitiesCheckBox.IsEnabled = false;
                ClimateControlCapabilitiesButton.IsEnabled = false;

                RadioControlCapabilitiesCheckBox.IsEnabled = false;
                RadioControlCapabilitiesButton.IsEnabled = false;

                ButtonCapabilitiesCheckBox.IsEnabled = false;
                ButtonCapabilitiesButton.IsEnabled = false;

                LightControlCapabilitiesCheckBox.IsChecked =false;
                LightControlCapabilitiesButton.IsEnabled = false ;

                HMISettingsControlCapabilitiesCheckBox.IsChecked = false;
                HMISettingsControlCapabilitiesButton.IsEnabled = false ;

                AudioControlCapabilitiesCheckBox.IsChecked = false;
                AudioControlCapabilitiesButton.IsEnabled = false;

                SeatControlCapabilitiesCheckBox.IsChecked = false;
                SeatControlCapabilitiesButton.IsEnabled = false;
                
            }
        }

        private void SeatControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SeatControlCapabilitiesButton.IsEnabled = (bool)SeatControlCapabilitiesCheckBox.IsChecked;
        }

        private async void SeatControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addSeatControlCapabilitiesCD = new ContentDialog();

            var addSeatControlCapabilities = new SharpHmi.src.Rpc.RC.SeatControlCapabilities();
            addSeatControlCapabilitiesCD.Content = addSeatControlCapabilities;

            addSeatControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addSeatControlCapabilitiesCD.PrimaryButtonClick += AddSeatControlCapabilitiesCD_PrimaryButtonClick;

            addSeatControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addSeatControlCapabilitiesCD.SecondaryButtonClick += AddSeatControlCapabilitiesCD_SecondaryButtonClick;

            await addSeatControlCapabilitiesCD.ShowAsync();
        }

        private async void AddSeatControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addSeatControlData = sender.Content as SharpHmi.src.Rpc.RC.SeatControlCapabilities;

            SeatControlCapabilities seatControlCapabilities = new SeatControlCapabilities();

            if (addSeatControlData.moduleNameCheckBox.IsChecked == true && addSeatControlData.moduleNameTextBox.Text != null)
            {
                seatControlCapabilities.moduleName = addSeatControlData.moduleNameTextBox.Text;
            }

            if (addSeatControlData.heaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.heatingEnabledAvailable = addSeatControlData.heaToggleSwitch.IsOn;
            }

            if (addSeatControlData.ceaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.coolingEnabledAvailable = addSeatControlData.ceaToggleSwitch.IsOn;
            }

            if (addSeatControlData.hlaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.heatingLevelAvailable = addSeatControlData.hlaToggleSwitch.IsOn;
            }

            if (addSeatControlData.claCheckBox.IsChecked == true)
            {
                seatControlCapabilities.coolingLevelAvailable = addSeatControlData.claToggleSwitch.IsOn;
            }

            if (addSeatControlData.hpaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.horizontalPositionAvailable = addSeatControlData.hpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.vpaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.verticalPositionAvailable = addSeatControlData.vpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.fvpaEnabledCheckBox.IsChecked == true)
            {
                seatControlCapabilities.frontVerticalPositionAvailable = addSeatControlData.fvpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.bvpaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.backVerticalPositionAvailable = addSeatControlData.bvpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.btaaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.backTiltAngleAvailable = addSeatControlData.btaaToggleSwitch.IsOn;
            }

            if (addSeatControlData.hshpaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.headSupportHorizontalPositionAvailable = addSeatControlData.hshpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.hsvpaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.headSupportVerticalPositionAvailable = addSeatControlData.hsvpaToggleSwitch.IsOn;
            }

            if (addSeatControlData.meaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.massageEnabledAvailable = addSeatControlData.meaToggleSwitch.IsOn;
            }

            if (addSeatControlData.mmaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.massageModeAvailable = addSeatControlData.mmaToggleSwitch.IsOn;
            }

            if (addSeatControlData.mcfaCheckBox.IsChecked == true)
            {
                seatControlCapabilities.massageCushionFirmnessAvailable = addSeatControlData.mcfaToggleSwitch.IsOn;
            }

            if (addSeatControlData.memoryAvailableCheckBox.IsChecked == true)
            {
                seatControlCapabilities.memoryAvailable = addSeatControlData.memoryAvailableToggleSwitch.IsOn;
            }

            seatControlCap.Add(seatControlCapabilities);

            addSeatControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddSeatControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addSeatControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private void AudioControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AudioControlCapabilitiesButton.IsEnabled = (bool)AudioControlCapabilitiesCheckBox.IsChecked;
        }

        private async void AudioControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addAudioControlCapabilitiesCD = new ContentDialog();

            var addAudioControlCapabilities = new SharpHmi.src.Rpc.RC.AudioControlCapabilities();
            addAudioControlCapabilitiesCD.Content = addAudioControlCapabilities;

            addAudioControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addAudioControlCapabilitiesCD.PrimaryButtonClick += AddAudioControlCapabilitiesCD_PrimaryButtonClick;

            addAudioControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addAudioControlCapabilitiesCD.SecondaryButtonClick += AddAudioControlCapabilitiesCD_SecondaryButtonClick;

            await addAudioControlCapabilitiesCD.ShowAsync();
        }

        private async void AddAudioControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addAudioControlCapabilities = sender.Content as SharpHmi.src.Rpc.RC.AudioControlCapabilities;

            AudioControlCapabilities audioControlCapabilities = new AudioControlCapabilities();

            if (addAudioControlCapabilities.moduleNameCheckBox.IsChecked == true && addAudioControlCapabilities.moduleNameTextBox.Text != null)
            {
                audioControlCapabilities.moduleName = addAudioControlCapabilities.moduleNameTextBox.Text;
            }

            if (addAudioControlCapabilities.sourceAvailableCheckBox.IsChecked == true)
            {
                audioControlCapabilities.sourceAvailable = addAudioControlCapabilities.sourceAvailableToggleSwitch.IsOn;
            }

            if (addAudioControlCapabilities.volumeAvailableCheckBox.IsChecked == true)
            {
                audioControlCapabilities.volumeAvailable = addAudioControlCapabilities.volumeAvailableToggleSwitch.IsOn;
            }

            if (addAudioControlCapabilities.equalizerAvailableCheckBox.IsChecked == true)
            {
                audioControlCapabilities.equalizerAvailable = addAudioControlCapabilities.equalizerAvailableToggleSwitch.IsOn;
            }

            if (addAudioControlCapabilities.moduleNameCheckBox.IsChecked == true && addAudioControlCapabilities.moduleNameTextBox.Text != null)
            {
                audioControlCapabilities.moduleName = addAudioControlCapabilities.moduleNameTextBox.Text;
            }

            audioControlCap.Add(audioControlCapabilities);

            addAudioControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddAudioControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addAudioControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private void HMISettingsControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HMISettingsControlCapabilitiesButton.IsEnabled = (bool)HMISettingsControlCapabilitiesCheckBox.IsChecked;
        }

        private async void HMISettingsControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addHMISettingsControlCapabilitiesCD = new ContentDialog();

            var addHMISettingsControlCapabilities = new SharpHmi.src.Rpc.RC.HMISettingsControlCapabilities(hmiSettingsControlCap);
            addHMISettingsControlCapabilitiesCD.Content = addHMISettingsControlCapabilities;

            addHMISettingsControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addHMISettingsControlCapabilitiesCD.PrimaryButtonClick += AddHMISettingsControlCapabilitiesCD_PrimaryButtonClick;

            addHMISettingsControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addHMISettingsControlCapabilitiesCD.SecondaryButtonClick += AddHMISettingsControlCapabilitiesCD_SecondaryButtonClick;

            await addHMISettingsControlCapabilitiesCD.ShowAsync();
        }

        private async void AddHMISettingsControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addHMISettingsControlCapabilities = sender.Content as SharpHmi.src.Rpc.RC.HMISettingsControlCapabilities;
                        
            if (addHMISettingsControlCapabilities.moduleNameCheckBox.IsChecked == true && addHMISettingsControlCapabilities.moduleNameTextBox.Text != null)
            {
                hmiSettingsControlCap.moduleName = addHMISettingsControlCapabilities.moduleNameTextBox.Text;
            }
            else{
                hmiSettingsControlCap.moduleName = "";
            }

            if (addHMISettingsControlCapabilities.distanceUnitAvailableCheckBox.IsChecked == true)
            {
                hmiSettingsControlCap.distanceUnitAvailable = addHMISettingsControlCapabilities.distanceUnitAvailableToggleSwitch.IsOn;
            }
            else
            {
                hmiSettingsControlCap.distanceUnitAvailable = null;
            }

            if (addHMISettingsControlCapabilities.temperatureUnitAvailableCheckBox.IsChecked == true)
            {
                hmiSettingsControlCap.temperatureUnitAvailable = addHMISettingsControlCapabilities.temperatureUnitAvailableToggleSwitch.IsOn;
            }
            else
            {
                hmiSettingsControlCap.temperatureUnitAvailable = null;
            }

            if (addHMISettingsControlCapabilities.displayModeUnitAvailableCheckBox.IsChecked == true)
            {
                hmiSettingsControlCap.displayModeUnitAvailable = addHMISettingsControlCapabilities.displayModeUnitAvailableToggleSwitch.IsOn;
            }
            else
            {
                hmiSettingsControlCap.displayModeUnitAvailable = null;
            }

            addHMISettingsControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private async void AddHMISettingsControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addHMISettingsControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }

        private void LightControlCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LightControlCapabilitiesButton.IsEnabled = (bool)LightControlCapabilitiesCheckBox.IsChecked;
        }

        private async void LightControlCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            rcGetCapabilitiesCD.Hide();

            addLightControlCapabilitiesCD = new ContentDialog();

            var addLightControlCapabilities = new SharpHmi.src.Rpc.RC.LightControlCapabilities(lightControlCap);
            addLightControlCapabilitiesCD.Content = addLightControlCapabilities;

            addLightControlCapabilitiesCD.PrimaryButtonText = "Ok";
            addLightControlCapabilitiesCD.PrimaryButtonClick += AddLightControlCapabilitiesCD_PrimaryButtonClick;

            addLightControlCapabilitiesCD.SecondaryButtonText = "Cancel";
            addLightControlCapabilitiesCD.SecondaryButtonClick += AddLightControlCapabilitiesCD_SecondaryButtonClick;

            await addLightControlCapabilitiesCD.ShowAsync();
        }

        private async void AddLightControlCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLightControlCapabilities = sender.Content as SharpHmi.src.Rpc.RC.LightControlCapabilities;

            if (addLightControlCapabilities.moduleNameCheckBox.IsChecked == true && addLightControlCapabilities.moduleNameTextBox.Text != null)
            {
                lightControlCap.moduleName = addLightControlCapabilities.moduleNameTextBox.Text;
            }
            else{
                lightControlCap.moduleName = "";
            }

            List<LightCapabilities> lightCapabilitiesList;
            if (addLightControlCapabilities.supportedLightsCheckBox.IsChecked == true)
            {
                lightCapabilitiesList = new  List<LightCapabilities>();
                lightCapabilitiesList.AddRange(addLightControlCapabilities.lightCapabilitiesCollection);
                lightControlCap.supportedLights = lightCapabilitiesList;
            }
            else if(lightControlCap.supportedLights!=null)
            {
                lightControlCap.supportedLights.Clear();
            }

            addLightControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }
   
        private async void AddLightControlCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            addLightControlCapabilitiesCD.Hide();
            await rcGetCapabilitiesCD.ShowAsync();
        }
    }
}