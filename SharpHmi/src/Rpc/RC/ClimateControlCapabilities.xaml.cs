﻿using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class ClimateControlCapabilities : UserControl
    {
        static ContentDialog defrostZoneCD = new ContentDialog();
        static ContentDialog ventillationModeCD = new ContentDialog();

        public TextBox moduleName;
        public CheckBox moduleNameCheckBox;
        
        public CheckBox currentTempAvailableCB;
        public CheckBox fanSpeedAvailableCB;
        public CheckBox desiredTemperatureAvailableCB;
        public CheckBox acEnableAvailableCB;
        public CheckBox acMaxEnableAvailable;
        public CheckBox circulateAirEnableAvailableCB;
        public CheckBox autoModeEnableAvailableCB;
        public CheckBox dualModeEnableAvailableCB;
        public CheckBox defrostZoneAvailableCB;
        public CheckBox ventilationModeAvailableCB;

        public ObservableCollection<HmiApiLib.Common.Enums.DefrostZone> defrostZoneCollection = 
            new ObservableCollection<HmiApiLib.Common.Enums.DefrostZone>();
        public ObservableCollection<HmiApiLib.Common.Enums.VentilationMode> ventilationModeCollection = 
            new ObservableCollection<HmiApiLib.Common.Enums.VentilationMode>();


        public ClimateControlCapabilities()
        {
            InitializeComponent();

            moduleName = ModuleNameTextBox;
            moduleNameCheckBox = ModuleNameCheckBox;
            
            currentTempAvailableCB = CurrentTemperatureAvailableCheckBox;
            fanSpeedAvailableCB = FanSpeedAvailableCheckBox;
            desiredTemperatureAvailableCB = DesiredTemperatureAvailableCheckBox;
            acEnableAvailableCB = ACEnableAvailableCheckBox;
            acMaxEnableAvailable = ACMaxEnableAvailableCheckBox;
            circulateAirEnableAvailableCB = CirculateAirEnableAvailableCheckBox;
            autoModeEnableAvailableCB = AutoModeEnableAvailableCheckBox;
            dualModeEnableAvailableCB = DualModeEnableAvailableCheckBox;
            defrostZoneAvailableCB = DefrostZoneAvailableCheckBox;
            ventilationModeAvailableCB = VentilationModeAvailableCheckBox;

            DefrostZoneListView.ItemsSource = defrostZoneCollection;
            VentillationModeListView.ItemsSource = ventilationModeCollection;
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModuleNameTextBox.IsEnabled = (bool)ModuleNameCheckBox.IsChecked;
        }

        private void DefrostZoneAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DefrostZoneAvailableToggle.IsEnabled = (bool)DefrostZoneAvailableCheckBox.IsChecked;

        }

        private async void AddDefrostZoneAvailableTapped(object sender, TappedRoutedEventArgs e)
        {
            RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.Hide();

            defrostZoneCD = new ContentDialog();

            var defrostZone = new DefrostZone();
            defrostZoneCD.Content = defrostZone;

            defrostZoneCD.PrimaryButtonText = Const.OK;
            defrostZoneCD.PrimaryButtonClick += DefrostZoneCD_PrimaryButtonClick;

            defrostZoneCD.SecondaryButtonText = Const.Close;
            defrostZoneCD.SecondaryButtonClick += DefrostZoneCD_SecondaryButtonClick;

            await defrostZoneCD.ShowAsync();
        }

        private async void DefrostZoneCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var zone = sender.Content as DefrostZone;
            foreach (HmiApiLib.Common.Enums.DefrostZone tmp in zone.defrostZoneList)
            {
                defrostZoneCollection.Add(tmp);
            }

            defrostZoneCD.Hide();
            await RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.ShowAsync();
        }

        private async void DefrostZoneCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {            
            defrostZoneCD.Hide();
            await RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.ShowAsync();
        }

        private void VentillationModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VentilationModeButton.IsEnabled = (bool)VentillationModeCheckBox.IsChecked;
        }

        private async void AddVentillationModeTapped(object sender, TappedRoutedEventArgs e)
        {
            RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.Hide();

            ventillationModeCD = new ContentDialog();

            var ventillationMode = new VentillationMode();
            ventillationModeCD.Content = ventillationMode;

            ventillationModeCD.PrimaryButtonText = Const.OK;
            ventillationModeCD.PrimaryButtonClick += VentillationModeCD_PrimaryButtonClick;

            ventillationModeCD.SecondaryButtonText = Const.Close;
            ventillationModeCD.SecondaryButtonClick += VentillationModeCD_SecondaryButtonClick;
            
            await ventillationModeCD.ShowAsync();
        }

        private async void VentillationModeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var ventillationMode = sender.Content as VentillationMode;
            foreach (HmiApiLib.Common.Enums.VentilationMode tmp in ventillationMode.ventilationModeList)
            {
                ventilationModeCollection.Add(tmp);
            }

            ventillationModeCD.Hide();
            await RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.ShowAsync();
        }

        private async void VentillationModeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            ventillationModeCD.Hide();
            await RCGetCapabilitiesResponse.addClimateControlCapabilitiesCD.ShowAsync();
        }

        private void HeatedSteeringWheelAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedSteeringWheelAvailableToggleSwitch.IsEnabled = (bool)HeatedSteeringWheelAvailableCheckBox.IsChecked;

        }

        private void HeatedWindshieldAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedWindshieldAvailableToggleSwitch.IsEnabled = (bool)HeatedWindshieldAvailableCheckBox.IsChecked;

        }

        private void HeatedRearWindowAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        
        {
            HeatedRearWindowAvailableToggleSwitch.IsEnabled = (bool)HeatedRearWindowAvailableCheckBox.IsChecked;

        }

        private void HeatedMirrorsAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeatedMirrorsAvailableToggleSwitch.IsEnabled = (bool)HeatedMirrorsAvailableCheckBox.IsChecked;

        }

        private void CurrentTemperatureAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CurrentTemperatureAvailableToggle.IsEnabled = (bool)CurrentTemperatureAvailableCheckBox.IsChecked;

        }

        private void FanSpeedAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FanSpeedAvailableToggle.IsEnabled = (bool)FanSpeedAvailableCheckBox.IsChecked;

        }

        private void DesiredTemperatureAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DesiredTemperatureAvailableToggle.IsEnabled = (bool)DesiredTemperatureAvailableCheckBox.IsChecked;

        }

        private void ACEnableAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ACEnableAvailableToggle.IsEnabled = (bool)ACEnableAvailableCheckBox.IsChecked;

        }

        private void ACMaxEnableAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ACMaxEnableAvailableToggle.IsEnabled = (bool)ACMaxEnableAvailableCheckBox.IsChecked;

        }

        private void CirculateAirEnableAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CirculateAirEnableAvailableToggle.IsEnabled = (bool)CirculateAirEnableAvailableCheckBox.IsChecked;

        }

        private void AutoModeEnableAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AutoModeEnableAvailableToggle.IsEnabled = (bool)AutoModeEnableAvailableCheckBox.IsChecked;

        }

        private void DualModeEnableAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DualModeEnableAvailableToggle.IsEnabled = (bool)DualModeEnableAvailableCheckBox.IsChecked;

        }

        private void DefrostZoneCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DefrostZoneAvailableButton.IsEnabled = (bool)DefrostZoneAvailableCheckBox.IsChecked;

        }
        private void VentilationModeAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
          VentilationModeAvailableToggle.IsEnabled= (bool)VentilationModeAvailableCheckBox.IsChecked;
        }
    }
}
