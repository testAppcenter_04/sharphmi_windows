﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class RadioControlCapabilities : UserControl
    {
        public TextBox moduleName;
        public CheckBox moduleNameCheckBox;

        public CheckBox radioEnableAvailableCB;
        public CheckBox radioBandAvailableCB;
        public CheckBox radioFrequencyAvailableCB;
        public CheckBox hdChannelAvailableCB;
        public CheckBox rdsDataAvailableCB;
        public CheckBox availableHDsAvailableCB;
        public CheckBox stateAvailableCB;
        public CheckBox signalStrengthAvailableCB;
        public CheckBox signalChangeThresholdAvailableCB;

        public RadioControlCapabilities()
        {
            this.InitializeComponent();

            moduleName = ModuleNameTextBox;
            moduleNameCheckBox = ModuleNameCheckBox;
            
            radioEnableAvailableCB = RadioEnableAvailableCheckBox;
            radioBandAvailableCB = RadioBandAvailableCheckBox;
            radioFrequencyAvailableCB = RadioFrequencyAvailableCheckBox;
            hdChannelAvailableCB = HDChannelAvailableCheckBox;
            rdsDataAvailableCB = RDSDataAvailableCheckBox;
            availableHDsAvailableCB = AvailableHDsAvailableCheckBox;
            stateAvailableCB = StateAvailableCheckBox;
            signalStrengthAvailableCB = SignalStrengthAvailableCheckBox;
            signalChangeThresholdAvailableCB = SignalStrengthAvailableCheckBox;
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ModuleNameCheckBox.IsChecked == true)
            {
                ModuleNameTextBox.IsEnabled = true;
            }
            else
            {
                ModuleNameTextBox.IsEnabled = false;
            }
        }

        private void SisDataAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SisDataAvailableToggle.IsEnabled = (bool)SisDataAvailableCheckBox.IsChecked;

        }

        private void RadioEnableAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RadioEnableAvailableToggle.IsEnabled = (bool)RadioEnableAvailableCheckBox.IsChecked;

        }

        private void RadioBandAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RadioBandAvailableToggle.IsEnabled = (bool)RadioBandAvailableCheckBox.IsChecked;


        }

        private void RadioFrequencyAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RadioFrequencyAvailableToggle.IsEnabled = (bool)RadioFrequencyAvailableCheckBox.IsChecked;


        }

        private void HDChannelAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HDChannelAvailableToggle.IsEnabled = (bool)HDChannelAvailableCheckBox.IsChecked;

        }

        private void RDSDataAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RDSDataAvailableToggle.IsEnabled = (bool)RDSDataAvailableCheckBox.IsChecked;

        }

        private void AvailableHDsAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableHDsAvailableToggle.IsEnabled = (bool)AvailableHDsAvailableCheckBox.IsChecked;

        }

        private void StateAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StateAvailableToggle.IsEnabled = (bool)StateAvailableCheckBox.IsChecked;

        }

        private void SignalStrengthAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SignalStrengthAvailableToggle.IsEnabled = (bool)SignalStrengthAvailableCheckBox.IsChecked;

        }

        private void SignalChangeThresholdAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SignalChangeThresholdAvailableToggle.IsEnabled = (bool)SignalChangeThresholdAvailableCheckBox.IsChecked;


        }
    }
}
