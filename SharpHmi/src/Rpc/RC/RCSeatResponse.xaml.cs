﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class RCSeatResponse : UserControl
    {
        public RCSeatResponse()
        {
            this.InitializeComponent();
        }

        private void SHECBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SCECBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SHLCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SCLCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SHPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SVPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SFVPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SBVPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SBTACBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void HSHPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void HSVPCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SMECBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void MassageModeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void MassageModeTapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void MCFCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void MCFTapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SeatMemoryCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {

        }

        private void SeatMemoryTapped(object sender, TappedRoutedEventArgs e)
        {

        }
    }
}
