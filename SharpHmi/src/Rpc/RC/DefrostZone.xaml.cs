﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.RC
{
    public sealed partial class DefrostZone : UserControl
    {
        private String[] defrostZoneArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.DefrostZone));
        public ObservableCollection<HmiApiLib.Common.Enums.DefrostZone> defrostZoneList = new ObservableCollection<HmiApiLib.Common.Enums.DefrostZone>();

        public DefrostZone()
        {
            InitializeComponent();
            DefrostZoneListBox.ItemsSource = defrostZoneArray;
        }

        private void DefrostZoneItemCheckBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.DefrostZone defrostZone = 
                (HmiApiLib.Common.Enums.DefrostZone)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.DefrostZone), (sender as CheckBox).Content.ToString());
            if (((CheckBox)sender).IsChecked == true)
                defrostZoneList.Add(defrostZone);
            else
            {
                if (defrostZoneList.Contains(defrostZone))
                    defrostZoneList.Remove(defrostZone);
            }
        }
    }
}
