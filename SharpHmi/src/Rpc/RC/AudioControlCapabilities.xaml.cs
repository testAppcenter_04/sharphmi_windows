﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.RC
{
    public sealed partial class AudioControlCapabilities : UserControl
    {
        public CheckBox moduleNameCheckBox;
        public TextBox moduleNameTextBox;

        public CheckBox sourceAvailableCheckBox;
        public ToggleSwitch sourceAvailableToggleSwitch;

        public CheckBox volumeAvailableCheckBox;
        public ToggleSwitch volumeAvailableToggleSwitch;

        public CheckBox equalizerAvailableCheckBox;
        public ToggleSwitch equalizerAvailableToggleSwitch;

        public CheckBox equalizerMaxChannelIdCheckBox;
        public TextBox equalizerMaxChannelIdTextBox;

        public AudioControlCapabilities()
        {
            this.InitializeComponent();

            moduleNameCheckBox = ModuleNameCheckBox;
            moduleNameTextBox = ModuleNameTextBox;

            sourceAvailableCheckBox = SourceAvailableCheckBox;
            sourceAvailableToggleSwitch = SourceAvailableToggleSwitch;

            volumeAvailableCheckBox = VolumeAvailableCheckBox;
            volumeAvailableToggleSwitch = VolumeAvailableToggleSwitch;

            equalizerAvailableCheckBox = EqualizerAvailableCheckBox;
            equalizerAvailableToggleSwitch = EqualizerAvailableToggleSwitch;

            equalizerMaxChannelIdCheckBox = EqualizerMaxChannelIdCheckBox;
            equalizerMaxChannelIdTextBox = EqualizerMaxChannelIdTextBox;
        }

        private void ModuleNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModuleNameTextBox.IsEnabled = (bool)ModuleNameCheckBox.IsChecked;
        }

        private void SourceAvailableCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SourceAvailableToggleSwitch.IsEnabled = (bool)SourceAvailableCheckBox.IsChecked;
        }

        private void VolumeAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VolumeAvailableToggleSwitch.IsEnabled = (bool)VolumeAvailableCheckBox.IsChecked;
        }

        private void EqualizerAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EqualizerAvailableToggleSwitch.IsEnabled = (bool)EqualizerAvailableCheckBox.IsChecked;
        }

        private void EqualizerMaxChannelIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EqualizerMaxChannelIdTextBox.IsEnabled = (bool)EqualizerMaxChannelIdCheckBox.IsChecked;
        }
    }
}
