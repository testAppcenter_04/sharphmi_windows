﻿using HmiApiLib.Common.Enums;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class AddLocationDetails : UserControl
    {
        public CheckBox coordinateCheckBox;
        public CheckBox latitudeDegreeCheckBox;
        public TextBox latitudeDegreeTextBox;
        public CheckBox longitudeDegreeCheckBox;
        public TextBox longitudeDegreeTextBox;
        public CheckBox locationNameCheckBox;
        public TextBox locationNameTextBox;
        public CheckBox addressCheckBox;
        public TextBox addressLine1;
        public TextBox addressLine2;
        public TextBox addressLine3;
        public TextBox addressLine4;
        public CheckBox locationDescriptionCheckBox;
        public TextBox locationDescriptionTextBox;
        public CheckBox phoneNumberCheckBox;
        public TextBox phoneNumberTextBox;
        public CheckBox locationImageCheckBox;
        public CheckBox imageValueCheckBox;
        public TextBox imageValueTextBox;
        public CheckBox imageTypeCheckBox;
        public ComboBox imageTypeComboBox;
        public CheckBox oASISAddressCheckBox;
        public CheckBox countryNameCheckBox;
        public TextBox countryNameTextBox;
        public CheckBox countryCodeCheckBox;
        public TextBox countryCodeTextBox;
        public CheckBox postalCodeCheckBox;
        public TextBox postalCodeTextBox;
        public CheckBox administrativeAreaCheckBox;
        public TextBox administrativeAreaTextBox;
        public CheckBox subAdministrativeAreaCheckBox;
        public TextBox subAdministrativeAreaTextBox;
        public CheckBox localityCheckBox;
        public TextBox localityTextBox;
        public CheckBox subLocalityCheckBox;
        public TextBox subLocalityTextBox;
        public CheckBox thoroughFareCheckBox;
        public TextBox thoroughFareTextBox;
        public CheckBox subThoroughFareCheckBox;
        public TextBox subThoroughFareTextBox;



        public AddLocationDetails()
        {
            InitializeComponent();

            ImageTypeComboBox.ItemsSource = Enum.GetNames(typeof(ImageType));
            ImageTypeComboBox.SelectedIndex = 0;

            coordinateCheckBox = CoordinateCheckBox;
            latitudeDegreeCheckBox = LatitudeDegreeCheckBox;
            latitudeDegreeTextBox = LatitudeDegreeTextBox;
            longitudeDegreeCheckBox = LongitudeDegreeCheckBox;
            longitudeDegreeTextBox = LongitudeDegreeTextBox;
            locationNameCheckBox = LocationNameCheckBox;
            locationNameTextBox = LocationNameTextBox;
            addressCheckBox = AddressCheckBox;
            addressLine1 = AddressLine1;
            addressLine2 = AddressLine2;
            addressLine3 = AddressLine3;
            addressLine4 = AddressLine4;
            locationDescriptionCheckBox = LocationDescriptionCheckBox;
            locationDescriptionTextBox = LocationDescriptionTextBox;
            phoneNumberCheckBox = PhoneNumberCheckBox;
            phoneNumberTextBox = PhoneNumberTextBox;
            locationImageCheckBox = LocationImageCheckBox;
            imageValueCheckBox = ImageValueCheckBox;
            imageValueTextBox = ImageValueTextBox;
            imageTypeCheckBox = ImageTypeCheckBox;
            imageTypeComboBox = ImageTypeComboBox;
            oASISAddressCheckBox = OASISAddressCheckBox;
            countryNameCheckBox = CountryNameCheckBox;
            countryNameTextBox = CountryNameTextBox;
            countryCodeCheckBox = CountryCodeCheckBox;
            countryCodeTextBox = CountryCodeTextBox;
            postalCodeCheckBox = PostalCodeCheckBox;
            postalCodeTextBox = PostalCodeTextBox;
            administrativeAreaCheckBox = AdministrativeAreaCheckBox;
            administrativeAreaTextBox = AdministrativeAreaTextBox;
            subAdministrativeAreaCheckBox = SubAdministrativeAreaCheckBox;
            subAdministrativeAreaTextBox = SubAdministrativeAreaTextBox;
            localityCheckBox = LocalityCheckBox;
            localityTextBox = LocalityTextBox;
            subLocalityCheckBox = SubLocalityCheckBox;
            subLocalityTextBox = SubLocalityTextBox;
            thoroughFareCheckBox = ThoroughFareCheckBox;
            thoroughFareTextBox = ThoroughFareTextBox;
            subThoroughFareCheckBox = SubThoroughFareCheckBox;
            subThoroughFareTextBox = SubThoroughFareTextBox;
        }

        private void CoordinateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (CoordinateCheckBox.IsChecked == false)
            {
                LatitudeDegreeCheckBox.IsEnabled = false;
                LatitudeDegreeTextBox.IsEnabled = false;
                LongitudeDegreeCheckBox.IsEnabled = false;
                LongitudeDegreeTextBox.IsEnabled = false;
            }
            else
            {
                LatitudeDegreeCheckBox.IsEnabled = true;
                LongitudeDegreeCheckBox.IsEnabled = true;

                if (LatitudeDegreeCheckBox.IsChecked == true)
                    LatitudeDegreeTextBox.IsEnabled = true;
                else
                    LatitudeDegreeTextBox.IsEnabled = false;

                if (LongitudeDegreeCheckBox.IsChecked == true)
                    LongitudeDegreeTextBox.IsEnabled = true;
                else
                    LongitudeDegreeTextBox.IsEnabled = false;
            }
        }

        private void LatitudeDegreeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (LatitudeDegreeCheckBox.IsChecked == true)
                LatitudeDegreeTextBox.IsEnabled = true;
            else
                LatitudeDegreeTextBox.IsEnabled = false;
        }

        private void LongitudeDegreeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LongitudeDegreeTextBox.IsEnabled = (bool)LongitudeDegreeCheckBox.IsChecked;
        }

        private void LocationNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LocationNameTextBox.IsEnabled = (bool)LocationNameCheckBox.IsChecked;
        }

        private void AddressCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AddressCheckBox.IsChecked == true)
            {
                AddressLine1.IsEnabled = true;
                AddressLine2.IsEnabled = true;
                AddressLine3.IsEnabled = true;
                AddressLine4.IsEnabled = true;
            }
            else
            {
                AddressLine1.IsEnabled = false;
                AddressLine2.IsEnabled = false;
                AddressLine3.IsEnabled = false;
                AddressLine4.IsEnabled = false;
            }
        }

        private void LocationDescriptionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LocationDescriptionTextBox.IsEnabled = (bool)LocationDescriptionCheckBox.IsChecked;
        }

        private void PhoneNumberCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PhoneNumberTextBox.IsEnabled = (bool)PhoneNumberCheckBox.IsChecked;
        }

        private void LocationImageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (LocationImageCheckBox.IsChecked == false)
            {
                ImageValueCheckBox.IsEnabled = false;
                ImageValueTextBox.IsEnabled = false;

                ImageTypeCheckBox.IsEnabled = false;
                ImageTypeComboBox.IsEnabled = false;
            }
            else
            {
                ImageValueCheckBox.IsEnabled = true;
                ImageTypeCheckBox.IsEnabled = true;

                ImageValueTextBox.IsEnabled = (bool)ImageValueCheckBox.IsChecked;
                ImageTypeComboBox.IsEnabled = (bool)ImageTypeCheckBox.IsChecked;
            }
        }

        private void ImageValueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ImageValueTextBox.IsEnabled = (bool)ImageValueCheckBox.IsChecked;
        }

        private void ImageTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ImageTypeComboBox.IsEnabled = (bool)ImageTypeCheckBox.IsChecked;
        }

        private void OASISAddressCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (OASISAddressCheckBox.IsChecked == false)
            {
                CountryNameCheckBox.IsEnabled = false;
                CountryNameTextBox.IsEnabled = false;

                CountryCodeCheckBox.IsEnabled = false;
                CountryCodeTextBox.IsEnabled = false;

                PostalCodeCheckBox.IsEnabled = false;
                PostalCodeTextBox.IsEnabled = false;

                AdministrativeAreaCheckBox.IsEnabled = false;
                AdministrativeAreaTextBox.IsEnabled = false;

                SubAdministrativeAreaCheckBox.IsEnabled = false;
                SubAdministrativeAreaTextBox.IsEnabled = false;

                LocalityCheckBox.IsEnabled = false;
                LocalityTextBox.IsEnabled = false;

                SubLocalityCheckBox.IsEnabled = false;
                SubLocalityTextBox.IsEnabled = false;

                ThoroughFareCheckBox.IsEnabled = false;
                ThoroughFareTextBox.IsEnabled = false;

                SubThoroughFareCheckBox.IsEnabled = false;
                SubThoroughFareTextBox.IsEnabled = false;
            }

            else
            {
                CountryNameCheckBox.IsEnabled = true;
                CountryCodeCheckBox.IsEnabled = true;
                PostalCodeCheckBox.IsEnabled = true;
                AdministrativeAreaCheckBox.IsEnabled = true;
                SubAdministrativeAreaCheckBox.IsEnabled = true;
                LocalityCheckBox.IsEnabled = true;
                SubLocalityCheckBox.IsEnabled = true;
                ThoroughFareCheckBox.IsEnabled = true;
                SubThoroughFareCheckBox.IsEnabled = true;

                CountryNameTextBox.IsEnabled = (bool)CountryNameCheckBox.IsChecked;
                CountryCodeTextBox.IsEnabled = (bool)CountryCodeCheckBox.IsChecked;
                PostalCodeTextBox.IsEnabled = (bool)PostalCodeCheckBox.IsChecked;
                AdministrativeAreaTextBox.IsEnabled = (bool)AdministrativeAreaCheckBox.IsChecked;
                SubAdministrativeAreaTextBox.IsEnabled = (bool)SubAdministrativeAreaCheckBox.IsChecked;
                LocalityTextBox.IsEnabled = (bool)LocalityCheckBox.IsChecked;
                SubLocalityTextBox.IsEnabled = (bool)SubLocalityCheckBox.IsChecked;
                ThoroughFareTextBox.IsEnabled = (bool)ThoroughFareCheckBox.IsChecked;
                SubThoroughFareTextBox.IsEnabled = (bool)SubThoroughFareCheckBox.IsChecked;
            }
        }

        private void CountryNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CountryNameTextBox.IsEnabled = (bool)CountryNameCheckBox.IsChecked;
        }

        private void CountryCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CountryCodeTextBox.IsEnabled = (bool)CountryCodeCheckBox.IsChecked;
        }

        private void PostalCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PostalCodeTextBox.IsEnabled = (bool)PostalCodeCheckBox.IsChecked;
        }

        private void AdministrativeAreaCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AdministrativeAreaTextBox.IsEnabled = (bool)AdministrativeAreaCheckBox.IsChecked;
        }

        private void SubAdministrativeAreaCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SubAdministrativeAreaTextBox.IsEnabled = (bool)SubAdministrativeAreaCheckBox.IsChecked;
        }

        private void LocalityCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LocalityTextBox.IsEnabled = (bool)LocalityCheckBox.IsChecked;
        }

        private void SubLocalityCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SubLocalityTextBox.IsEnabled = (bool)SubLocalityCheckBox.IsChecked;
        }

        private void ThoroughFareCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ThoroughFareTextBox.IsEnabled = (bool)ThoroughFareCheckBox.IsChecked;
        }

        private void SubThoroughFareCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SubThoroughFareTextBox.IsEnabled = (bool)SubThoroughFareCheckBox.IsChecked;
        }
    }
}
