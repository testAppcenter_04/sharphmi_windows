﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationShowConstantTBTResponse : UserControl
    {
        ShowConstantTBT tmpObj = null;
        public NavigationShowConstantTBTResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowConstantTBT()
        {
            ContentDialog navigationShowConstantTBTCD = new ContentDialog();

            navigationShowConstantTBTCD.Content = this;

            navigationShowConstantTBTCD.PrimaryButtonText = Const.TxLater;
            navigationShowConstantTBTCD.PrimaryButtonClick += NavigationShowConstantTBTCD_PrimaryButtonClick;

            navigationShowConstantTBTCD.SecondaryButtonText = Const.Reset;
            navigationShowConstantTBTCD.SecondaryButtonClick += NavigationShowConstantTBTCD_ResetButtonClick;

            navigationShowConstantTBTCD.CloseButtonText = Const.Close;
            navigationShowConstantTBTCD.CloseButtonClick += NavigationShowConstantTBTCD_CloseButtonClick;

            tmpObj = new ShowConstantTBT();
            tmpObj = (ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<ShowConstantTBT>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ShowConstantTBT);
                tmpObj = (ShowConstantTBT)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationShowConstantTBTCD.ShowAsync();
        }

        private void NavigationShowConstantTBTCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavShowConstantTBTResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationShowConstantTBTCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationShowConstantTBTCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
