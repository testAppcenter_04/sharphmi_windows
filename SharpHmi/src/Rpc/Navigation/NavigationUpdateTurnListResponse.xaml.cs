﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationUpdateTurnListResponse : UserControl
    {
        UpdateTurnList tmpObj = null;
        public NavigationUpdateTurnListResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowUpdateTurnList()
        {
            ContentDialog navigationUpdateTurnListCD = new ContentDialog();
            navigationUpdateTurnListCD.Content = this;

            navigationUpdateTurnListCD.PrimaryButtonText = Const.TxLater;
            navigationUpdateTurnListCD.PrimaryButtonClick += NavigationUpdateTurnListCD_PrimaryButtonClick;

            navigationUpdateTurnListCD.SecondaryButtonText = Const.Reset;
            navigationUpdateTurnListCD.SecondaryButtonClick += NavigationUpdateTurnListCD_ResetButtonClick;

            navigationUpdateTurnListCD.CloseButtonText = Const.Close;
            navigationUpdateTurnListCD.CloseButtonClick += NavigationUpdateTurnListCD_CloseButtonClick;

            tmpObj = new UpdateTurnList();
            tmpObj = (UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<UpdateTurnList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(UpdateTurnList);
                tmpObj = (UpdateTurnList)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationUpdateTurnListCD.ShowAsync();
        }

        private void NavigationUpdateTurnListCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavUpdateTurnListResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationUpdateTurnListCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationUpdateTurnListCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
