﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationAlertManeuverResponse : UserControl
    {
        AlertManeuver tmpObj = null;
        public NavigationAlertManeuverResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowAlertManeuver()
        {
            ContentDialog navigationAlertManeuverCD = new ContentDialog();
            navigationAlertManeuverCD.Content = this;

            navigationAlertManeuverCD.PrimaryButtonText = Const.TxLater;
            navigationAlertManeuverCD.PrimaryButtonClick += NavigationAlertManeuverCD_PrimaryButtonClick;

            navigationAlertManeuverCD.SecondaryButtonText = Const.Reset;
            navigationAlertManeuverCD.SecondaryButtonClick += NavigationAlertManeuverCD_ResetButtonClick;

            navigationAlertManeuverCD.CloseButtonText = Const.Close;
            navigationAlertManeuverCD.CloseButtonClick += NavigationAlertManeuverCD_CloseButtonClick;

            tmpObj = new AlertManeuver();
            tmpObj = (AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<AlertManeuver>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(AlertManeuver);
                tmpObj = (AlertManeuver)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationAlertManeuverCD.ShowAsync();
        }

        private void NavigationAlertManeuverCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavAlertManeuverResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationAlertManeuverCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationAlertManeuverCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
