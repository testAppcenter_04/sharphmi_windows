﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationSendLocationResponse : UserControl
    {
        SendLocation tmpObj;
        public NavigationSendLocationResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSendLocation()
        {
            ContentDialog navigationSendLocationCD = new ContentDialog();

            navigationSendLocationCD.Content = this;

            navigationSendLocationCD.PrimaryButtonText = Const.TxLater;
            navigationSendLocationCD.PrimaryButtonClick += NavigationSendLocationCD_PrimaryButtonClick;

            navigationSendLocationCD.SecondaryButtonText = Const.Reset;
            navigationSendLocationCD.SecondaryButtonClick += NavigationSendLocationCD_ResetButtonClick;

            navigationSendLocationCD.CloseButtonText = Const.Close;
            navigationSendLocationCD.CloseButtonClick += NavigationSendLocationCD_CloseButtonClick;

            tmpObj = new SendLocation();
            tmpObj = (SendLocation)AppUtils.getSavedPreferenceValueForRpc<SendLocation>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SendLocation);
                tmpObj = (SendLocation)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationSendLocationCD.ShowAsync();
        }

        private void NavigationSendLocationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavSendLocationResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationSendLocationCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationSendLocationCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
