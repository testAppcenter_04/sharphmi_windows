﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationStopAudioStreamResponse : UserControl
    {
        StopAudioStream tmpObj = null;
        public NavigationStopAudioStreamResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowStopAudioStream()
        {
            ContentDialog navigationStopAudioStreamCD = new ContentDialog();
            navigationStopAudioStreamCD.Content = this;

            navigationStopAudioStreamCD.PrimaryButtonText = Const.TxLater;
            navigationStopAudioStreamCD.PrimaryButtonClick += NavigationStopAudioStreamCD_PrimaryButtonClick;

            navigationStopAudioStreamCD.SecondaryButtonText = Const.Reset;
            navigationStopAudioStreamCD.SecondaryButtonClick += NavigationStopAudioStreamCD_ResetButtonClick;

            navigationStopAudioStreamCD.CloseButtonText = Const.Close;
            navigationStopAudioStreamCD.CloseButtonClick += NavigationStopAudioStreamCD_CloseButtonClick;

            tmpObj = new StopAudioStream();
            tmpObj = (StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<StopAudioStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(StopAudioStream);
                tmpObj = (StopAudioStream)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationStopAudioStreamCD.ShowAsync();
        }

        private void NavigationStopAudioStreamCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavStopAudioStreamResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationStopAudioStreamCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationStopAudioStreamCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
