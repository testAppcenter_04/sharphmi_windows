﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationGetWayPointsResponse : UserControl
    {
        GetWayPoints tmpObj = null;
        ObservableCollection<LocationDetails> wayPoints = new ObservableCollection<LocationDetails>();
        static ContentDialog navigationGetWayPointsCD = new ContentDialog();
        List<LocationDetails> locationDetailList = null;
        public NavigationGetWayPointsResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetWayPoints()
        {
            navigationGetWayPointsCD = new ContentDialog();
            navigationGetWayPointsCD.Content = this;

            navigationGetWayPointsCD.PrimaryButtonText = Const.TxLater;
            navigationGetWayPointsCD.PrimaryButtonClick += NavigationGetWayPointsCD_PrimaryButtonClick;

            navigationGetWayPointsCD.SecondaryButtonText = Const.Reset;
            navigationGetWayPointsCD.SecondaryButtonClick += NavigationGetWayPointsCD_ResetButtonClick;

            navigationGetWayPointsCD.CloseButtonText = Const.Close;
            navigationGetWayPointsCD.CloseButtonClick += NavigationGetWayPointsCD_CloseButtonClick;

            tmpObj = new GetWayPoints();
            tmpObj = (GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<GetWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetWayPoints);
                tmpObj = (GetWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getWayPoints() != null)
                {
                    List<LocationDetails> locationDetails = tmpObj.getWayPoints();
                    foreach (LocationDetails l in locationDetails)
                    {
                        wayPoints.Add(l);
                    }
                }

                else
                {
                    AddLocationDetailsButton.IsEnabled = false;
                    AddLocationDetailsCheckBox.IsChecked = false;
                }
            }
            
            NavGetWayPointsListView.ItemsSource = wayPoints;

            await navigationGetWayPointsCD.ShowAsync();
        }

        private void NavigationGetWayPointsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            if (AddLocationDetailsCheckBox.IsChecked == true)
            {
                locationDetailList = new List<LocationDetails>();

                locationDetailList.AddRange(wayPoints);
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavGetWayPointsResponse(BuildRpc.getNextId(), resultCode, locationDetailList);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationGetWayPointsCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationGetWayPointsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AddLocationDetailsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddLocationDetailsButton.IsEnabled = (bool)AddLocationDetailsCheckBox.IsChecked;
        }

        private void AddLocationDetailsButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            ShowAddLocationDetails();
        }

        public async void ShowAddLocationDetails()
        {
            var addLocationDetails = new AddLocationDetails();
            navigationGetWayPointsCD.Hide();

            ContentDialog addLocationDetailsCD = new ContentDialog();
            addLocationDetailsCD.Content = addLocationDetails;

            addLocationDetailsCD.PrimaryButtonText = Const.OK;
            addLocationDetailsCD.PrimaryButtonClick += AddLocationDetailsCD_PrimaryButtonClick;

            addLocationDetailsCD.SecondaryButtonText = Const.Close;
            addLocationDetailsCD.SecondaryButtonClick += AddLocationDetailsCD_SecondaryButtonClick;

            await addLocationDetailsCD.ShowAsync();
        }

        private async void AddLocationDetailsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLocationDetails = sender.Content as AddLocationDetails;
            if (addLocationDetails.coordinateCheckBox.IsChecked == true || addLocationDetails.locationNameCheckBox.IsChecked == true
                || addLocationDetails.addressCheckBox.IsChecked == true || addLocationDetails.locationDescriptionCheckBox.IsChecked == true
                || addLocationDetails.phoneNumberCheckBox.IsChecked == true || addLocationDetails.locationImageCheckBox.IsChecked == true
                || addLocationDetails.oASISAddressCheckBox.IsChecked == true)
            {
                LocationDetails details = new LocationDetails();
                if (addLocationDetails.coordinateCheckBox.IsChecked == true)
                {
                    Coordinate coordinate = new Coordinate();
                    if (addLocationDetails.latitudeDegreeCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.latitudeDegreeTextBox.Text)
                        {
                            try
                            {
                                coordinate.latitudeDegrees = float.Parse(addLocationDetails.latitudeDegreeTextBox.Text);
                            }
                            catch
                            {
                                coordinate.latitudeDegrees = 0;
                            }
                        }
                    }
                    if (addLocationDetails.longitudeDegreeCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.longitudeDegreeTextBox.Text)
                        {
                            try
                            {
                                coordinate.longitudeDegrees = float.Parse(addLocationDetails.longitudeDegreeTextBox.Text);
                            }
                            catch
                            {
                            coordinate.longitudeDegrees = 0;
                            }
                        }
                    }
                    details.coordinate = coordinate;
                }
                if (addLocationDetails.locationNameCheckBox.IsChecked == true)
                {
                    details.locationName = addLocationDetails.locationNameTextBox.Text;
                }
                if (addLocationDetails.addressCheckBox.IsChecked == true)
                {
                    List<String> addresslines = new List<string>();
                    if (null != addLocationDetails.addressLine1.Text)
                        addresslines.Add(addLocationDetails.addressLine1.Text.ToString());
                    if (null != addLocationDetails.addressLine2.Text)
                        addresslines.Add(addLocationDetails.addressLine2.Text.ToString());
                    if (null != addLocationDetails.addressLine3.Text)
                        addresslines.Add(addLocationDetails.addressLine3.Text.ToString());
                    if (null != addLocationDetails.addressLine4.Text)
                        addresslines.Add(addLocationDetails.addressLine4.Text.ToString());
                    details.addressLines = addresslines;
                }
                if (addLocationDetails.locationDescriptionCheckBox.IsChecked == true)
                {
                    if (null != addLocationDetails.locationDescriptionTextBox.Text)
                        details.locationDescription = addLocationDetails.locationDescriptionTextBox.Text.ToString();
                }
                if (addLocationDetails.phoneNumberCheckBox.IsChecked == true)
                {
                    if (null != addLocationDetails.phoneNumberTextBox.Text)
                        details.phoneNumber = addLocationDetails.phoneNumberTextBox.Text;
                }
                if (addLocationDetails.locationImageCheckBox.IsChecked == true)
                {
                    HmiApiLib.Common.Structs.Image locationImage = new HmiApiLib.Common.Structs.Image();
                    if (addLocationDetails.imageValueCheckBox.IsChecked == true)
                    {
                        locationImage.imageType = (ImageType)addLocationDetails.imageTypeComboBox.SelectedIndex;
                    }
                    if (addLocationDetails.imageTypeCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.imageValueTextBox.Text)
                            locationImage.value = addLocationDetails.imageValueTextBox.Text;
                    }
                    details.locationImage = locationImage;
                }
                if (addLocationDetails.oASISAddressCheckBox.IsChecked == true)
                {
                    OASISAddress oASISAddress = new OASISAddress();
                    if (addLocationDetails.countryNameCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.countryNameTextBox.Text)
                            oASISAddress.countryName = addLocationDetails.countryNameTextBox.Text;
                    }
                    if (addLocationDetails.countryCodeCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.countryCodeTextBox.Text)
                            oASISAddress.countryCode = addLocationDetails.countryCodeTextBox.Text;
                    }
                    if (addLocationDetails.postalCodeCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.postalCodeTextBox.Text)
                            oASISAddress.postalCode = addLocationDetails.postalCodeTextBox.Text;
                    }
                    if (addLocationDetails.administrativeAreaCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.administrativeAreaTextBox.Text)
                            oASISAddress.administrativeArea = addLocationDetails.administrativeAreaTextBox.Text;
                    }
                    if (addLocationDetails.subAdministrativeAreaCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.subAdministrativeAreaTextBox.Text)
                            oASISAddress.subAdministrativeArea = addLocationDetails.subAdministrativeAreaTextBox.Text;
                    }
                    if (addLocationDetails.localityCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.localityTextBox.Text)
                            oASISAddress.locality = addLocationDetails.localityTextBox.Text;
                    }
                    if (addLocationDetails.subLocalityCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.subLocalityTextBox.Text)
                            oASISAddress.subLocality = addLocationDetails.subLocalityTextBox.Text;
                    }
                    if (addLocationDetails.thoroughFareCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.thoroughFareTextBox.Text)
                            oASISAddress.thoroughfare = addLocationDetails.thoroughFareTextBox.Text;
                    }
                    if (addLocationDetails.subThoroughFareCheckBox.IsChecked == true)
                    {
                        if (null != addLocationDetails.subThoroughFareTextBox.Text)
                            oASISAddress.subThoroughfare = addLocationDetails.subThoroughFareTextBox.Text;
                    }
                    details.searchAddress = oASISAddress;
                }
                wayPoints.Add(details);
            }

            sender.Hide();
            await navigationGetWayPointsCD.ShowAsync();
        }

        private async void AddLocationDetailsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await navigationGetWayPointsCD.ShowAsync();
        }

    }
}
