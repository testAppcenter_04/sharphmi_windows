﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationStopStreamResponse : UserControl
    {
        StopStream tmpObj = null;
        public NavigationStopStreamResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowStopStream()
        {
            ContentDialog navigationStopStreamCD = new ContentDialog();
            navigationStopStreamCD.Content = this;

            navigationStopStreamCD.PrimaryButtonText = Const.TxLater;
            navigationStopStreamCD.PrimaryButtonClick += NavigationStopStreamCD_PrimaryButtonClick;

            navigationStopStreamCD.SecondaryButtonText = Const.Reset;
            navigationStopStreamCD.SecondaryButtonClick += NavigationStopStreamCD_ResetButtonClick;

            navigationStopStreamCD.CloseButtonText = Const.Close;
            navigationStopStreamCD.CloseButtonClick += NavigationStopStreamCD_CloseButtonClick;

            tmpObj = new StopStream();
            tmpObj = (StopStream)AppUtils.getSavedPreferenceValueForRpc<StopStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(StopStream);
                tmpObj = (StopStream)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationStopStreamCD.ShowAsync();
        }

        private void NavigationStopStreamCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavStopStreamResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationStopStreamCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationStopStreamCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
