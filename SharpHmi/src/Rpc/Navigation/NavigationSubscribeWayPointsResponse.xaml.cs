﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationSubscribeWayPointsResponse : UserControl
    {
        SubscribeWayPoints tmpObj = null;
        public NavigationSubscribeWayPointsResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSubscribeWayPoints()
        {
            ContentDialog navigationSubscribeWayPointsCD = new ContentDialog();
            navigationSubscribeWayPointsCD.Content = this;

            navigationSubscribeWayPointsCD.PrimaryButtonText = Const.TxLater;
            navigationSubscribeWayPointsCD.PrimaryButtonClick += NavigationSubscribeWayPointsCD_PrimaryButtonClick;

            navigationSubscribeWayPointsCD.SecondaryButtonText = Const.Reset;
            navigationSubscribeWayPointsCD.SecondaryButtonClick += NavigationSubscribeWayPointsCD_ResetButtonClick;

            navigationSubscribeWayPointsCD.CloseButtonText = Const.Close;
            navigationSubscribeWayPointsCD.CloseButtonClick += NavigationSubscribeWayPointsCD_CloseButtonClick;

            tmpObj = new SubscribeWayPoints();
            tmpObj = (SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<SubscribeWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SubscribeWayPoints);
                tmpObj = (SubscribeWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationSubscribeWayPointsCD.ShowAsync();
        }

        private void NavigationSubscribeWayPointsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavSubscribeWayPointsResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationSubscribeWayPointsCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationSubscribeWayPointsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
