﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;
        public NavigationIsReadyResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            ContentDialog navigationIsReadyCD = new ContentDialog();

            navigationIsReadyCD.Content = this;

            navigationIsReadyCD.PrimaryButtonText = Const.TxLater;
            navigationIsReadyCD.PrimaryButtonClick += NavigationIsReadyCD_PrimaryButtonClick;

            navigationIsReadyCD.SecondaryButtonText = Const.Reset;
            navigationIsReadyCD.SecondaryButtonClick += NavigationIsReadyCD_ResetButtonClick;

            navigationIsReadyCD.CloseButtonText = Const.Close;
            navigationIsReadyCD.CloseButtonClick += NavigationIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(IsReady);
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();

                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableTgl.IsEnabled = false;
                }
                else
                {
                    AvailableCheckBox.IsChecked = true;
                    AvailableTgl.IsOn = (bool)tmpObj.getAvailable();
                }
            }

            await navigationIsReadyCD.ShowAsync();
        }

        private void NavigationIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableTgl.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(
                BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.Navigation, toggle, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableTgl.IsEnabled = (bool)AvailableCheckBox.IsChecked;
        }
    }
}
