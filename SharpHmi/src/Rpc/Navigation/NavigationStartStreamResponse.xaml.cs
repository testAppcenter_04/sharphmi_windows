﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationStartStreamResponse : UserControl
    {
        StartStream tmpObj = null;
        public NavigationStartStreamResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowStartStream()
        {
            ContentDialog navigationStartStreamCD = new ContentDialog();
            navigationStartStreamCD.Content = this;

            navigationStartStreamCD.PrimaryButtonText = Const.TxLater;
            navigationStartStreamCD.PrimaryButtonClick += NavigationStartStreamCD_PrimaryButtonClick;

            navigationStartStreamCD.SecondaryButtonText = Const.Reset;
            navigationStartStreamCD.SecondaryButtonClick += NavigationStartStreamCD_ResetButtonClick;

            navigationStartStreamCD.CloseButtonText = Const.Close;
            navigationStartStreamCD.CloseButtonClick += NavigationStartStreamCD_CloseButtonClick;

            tmpObj = new StartStream();
            tmpObj = (StartStream)AppUtils.getSavedPreferenceValueForRpc<StartStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(StartStream);
                tmpObj = (StartStream)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationStartStreamCD.ShowAsync();
        }

        private void NavigationStartStreamCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavStartStreamResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationStartStreamCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationStartStreamCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
