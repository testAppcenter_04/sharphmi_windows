﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationUnsubscribeWayPointsResponse : UserControl
    {
        UnsubscribeWayPoints tmpObj = null;
        public NavigationUnsubscribeWayPointsResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowUnsubscribeWayPoints()
        {
            ContentDialog navigationUnsubscribeWayPointsCD = new ContentDialog();
            navigationUnsubscribeWayPointsCD.Content = this;

            navigationUnsubscribeWayPointsCD.PrimaryButtonText = Const.TxLater;
            navigationUnsubscribeWayPointsCD.PrimaryButtonClick += NavigationUnsubscribeWayPointsCD_PrimaryButtonClick;

            navigationUnsubscribeWayPointsCD.SecondaryButtonText = Const.Reset;
            navigationUnsubscribeWayPointsCD.SecondaryButtonClick += NavigationUnsubscribeWayPointsCD_ResetButtonClick;

            navigationUnsubscribeWayPointsCD.CloseButtonText = Const.Close;
            navigationUnsubscribeWayPointsCD.CloseButtonClick += NavigationUnsubscribeWayPointsCD_CloseButtonClick;

            tmpObj = new UnsubscribeWayPoints();
            tmpObj = (UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<UnsubscribeWayPoints>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(UnsubscribeWayPoints);
                tmpObj = (UnsubscribeWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationUnsubscribeWayPointsCD.ShowAsync();
        }

        private void NavigationUnsubscribeWayPointsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavUnsubscribeWayPointsResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationUnsubscribeWayPointsCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationUnsubscribeWayPointsCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
