﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationOnTBTClientState : UserControl
    {
        OnTBTClientState tmpObj = null;
        public NavigationOnTBTClientState()
        {
            InitializeComponent();
            TBTStateComboBox.ItemsSource = Enum.GetNames(typeof(TBTState));
            TBTStateComboBox.SelectedIndex = 0;
        }

        public async void ShowOnTBTClientState()
        {
            ContentDialog navigationOnTBTClientStateCD = new ContentDialog();
            navigationOnTBTClientStateCD.Content = this;

            navigationOnTBTClientStateCD.PrimaryButtonText = Const.TxNow;
            navigationOnTBTClientStateCD.PrimaryButtonClick += NavigationOnTBTClientStateCD_PrimaryButtonClick;

            navigationOnTBTClientStateCD.SecondaryButtonText = Const.Reset;
            navigationOnTBTClientStateCD.SecondaryButtonClick += NavigationOnTBTClientStateCD_ResetButtonClick;

            navigationOnTBTClientStateCD.CloseButtonText = Const.Close;
            navigationOnTBTClientStateCD.CloseButtonClick += NavigationOnTBTClientStateCD_CloseButtonClick;

            tmpObj = new OnTBTClientState();
            tmpObj = (OnTBTClientState)AppUtils.getSavedPreferenceValueForRpc<OnTBTClientState>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnTBTClientState);
                tmpObj = (OnTBTClientState)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {

                if (tmpObj.getState() != null)
                    TBTStateComboBox.SelectedIndex = (int)tmpObj.getState();
                else
                {
                    TBTStateComboBox.IsEnabled = false;
                    TBTStateCheckBox.IsChecked = false;
                }
            }
           


            await navigationOnTBTClientStateCD.ShowAsync();
        }

        private void NavigationOnTBTClientStateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            TBTState? state = null;
            if (TBTStateCheckBox.IsChecked == true)
            {
                state = (TBTState)TBTStateComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcMessage = BuildRpc.buildNavigationOnTBTClientState(state);
            AppUtils.savePreferenceValueForRpc(rpcMessage.getMethod(), rpcMessage);
            AppInstanceManager.AppInstance.sendRpc(rpcMessage);
        }

        private void NavigationOnTBTClientStateCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void NavigationOnTBTClientStateCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void TBTStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TBTStateComboBox.IsEnabled = (bool)TBTStateCheckBox.IsChecked;
        }
    }
}
