﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.Navigation
{
    public sealed partial class NavigationStartAudioStreamResponse : UserControl
    {
        StartAudioStream tmpObj = null;
        public NavigationStartAudioStreamResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowStartAudioStream()
        {
            ContentDialog navigationStartAudioStreamCD = new ContentDialog();

            navigationStartAudioStreamCD.Content = this;

            navigationStartAudioStreamCD.PrimaryButtonText = Const.TxLater;
            navigationStartAudioStreamCD.PrimaryButtonClick += NavigationStartAudioStreamCD_PrimaryButtonClick;

            navigationStartAudioStreamCD.SecondaryButtonText = Const.Reset;
            navigationStartAudioStreamCD.SecondaryButtonClick += NavigationStartAudioStreamCD_ResetButtonClick;

            navigationStartAudioStreamCD.CloseButtonText = Const.Close;
            navigationStartAudioStreamCD.CloseButtonClick += NavigationStartAudioStreamCD_CloseButtonClick;

            tmpObj = new StartAudioStream();
            tmpObj = (StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<StartAudioStream>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(StartAudioStream);
                tmpObj = (StartAudioStream)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await navigationStartAudioStreamCD.ShowAsync();
        }

        private void NavigationStartAudioStreamCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildNavStartAudioStreamResponse(BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void NavigationStartAudioStreamCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }

        }

        private void NavigationStartAudioStreamCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
