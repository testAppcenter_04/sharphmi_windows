﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class ECallInfo : UserControl
    {
        public CheckBox eCallNotificationStatusCheckBox;
        public ComboBox eCallNotificationStatusComboBox;

        public CheckBox auxECallNotificationStatusCheckBox;
        public ComboBox auxECallNotificationStatusComboBox;

        public CheckBox eCallConfirmationStatusCheckBox;
        public ComboBox eCallConfirmationStatusComboBox;

        public ECallInfo(HmiApiLib.Common.Structs.ECallInfo ecallInfo)
        {
            InitializeComponent();

            ECallNotificationStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataNotificationStatus));
            ECallNotificationStatusComboBox.SelectedIndex = 0;

            AuxECallNotificationStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataNotificationStatus));
            AuxECallNotificationStatusComboBox.SelectedIndex = 0;

            ECallConfirmationStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ECallConfirmationStatus));
            ECallConfirmationStatusComboBox.SelectedIndex = 0;


            eCallNotificationStatusCheckBox = ECallNotificationStatusCheckBox;
            eCallNotificationStatusComboBox = ECallNotificationStatusComboBox;

            auxECallNotificationStatusCheckBox = AuxECallNotificationStatusCheckBox;
            auxECallNotificationStatusComboBox = AuxECallNotificationStatusComboBox;

            eCallConfirmationStatusCheckBox = ECallConfirmationStatusCheckBox;
            eCallConfirmationStatusComboBox = ECallConfirmationStatusComboBox;

            if (null != ecallInfo)
            {   if (ecallInfo.getECallNotificationStatus() != null)
                {
                    ECallNotificationStatusComboBox.SelectedIndex = (int)ecallInfo.getECallNotificationStatus();
                    ECallNotificationStatusCheckBox.IsChecked = true;

                }
                else
                {
                    ECallNotificationStatusComboBox.IsEnabled = false;
                }
                if (ecallInfo.getAuxECallNotificationStatus() != null)
                {
                    AuxECallNotificationStatusComboBox.SelectedIndex = (int)ecallInfo.getAuxECallNotificationStatus();
                    AuxECallNotificationStatusCheckBox.IsChecked = true;

                }
                else
                {
                    AuxECallNotificationStatusComboBox.IsEnabled = false;
                }
                if (ecallInfo.getECallConfirmationStatus() != null)
                {
                    ECallConfirmationStatusComboBox.SelectedIndex = (int)ecallInfo.getECallConfirmationStatus();
                    ECallConfirmationStatusCheckBox.IsChecked = true;

                }
                else
                {
                    ECallConfirmationStatusComboBox.IsEnabled = false;
                }


            }
    }

        private void ECallNotificationStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallNotificationStatusComboBox.IsEnabled = (bool)ECallNotificationStatusCheckBox.IsChecked;
        }

        private void AuxECallNotificationStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AuxECallNotificationStatusComboBox.IsEnabled = (bool)AuxECallNotificationStatusCheckBox.IsChecked;
        }

        private void ECallConfirmationStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallConfirmationStatusComboBox.IsEnabled = (bool)ECallConfirmationStatusCheckBox.IsChecked;
        }
    }
}
