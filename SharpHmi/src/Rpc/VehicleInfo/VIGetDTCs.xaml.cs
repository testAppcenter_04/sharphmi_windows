﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIGetDTCs : UserControl
    {
        GetDTCs tmpObj = null;
        public VIGetDTCs()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetDTCs()
        {
            ContentDialog viDiagnostocMessageCD = new ContentDialog();
            viDiagnostocMessageCD.Content = this;

            viDiagnostocMessageCD.PrimaryButtonText = Const.TxLater;
            viDiagnostocMessageCD.PrimaryButtonClick += ViDiagnostocMessageCD_PrimaryButtonClick;

            viDiagnostocMessageCD.SecondaryButtonText = Const.Reset;
            viDiagnostocMessageCD.SecondaryButtonClick += ViDiagnostocMessageCD_ResetButtonClick;

            viDiagnostocMessageCD.CloseButtonText = Const.Close;
            viDiagnostocMessageCD.CloseButtonClick += ViDiagnostocMessageCD_CloseButtonClick;

            tmpObj = new GetDTCs();
            tmpObj = (GetDTCs)AppUtils.getSavedPreferenceValueForRpc<GetDTCs>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetDTCs);
                tmpObj = (GetDTCs)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getEcuHeader())
                {
                    ECUHeaderTextBox.Text = tmpObj.getEcuHeader().ToString();
                }
                else
                {
                    ECUHeaderTextBox.IsEnabled=false;
                    ECUHeaderCheckBox.IsChecked = false;
                }
                if (tmpObj.getDtc() != null)
                {
                    List<String> dtcList = tmpObj.getDtc();
                    int count = dtcList.Count;
                    String messageDataString = "";
                    for (int i = 0; i < count; i++)
                    {
                        if (i == count - 1)
                        {
                            messageDataString = messageDataString + dtcList[i];
                        }
                        else
                        {
                            messageDataString = messageDataString + dtcList[i] + ",";
                        }
                    }
                    DtcTextBox.Text = messageDataString;
                }
                else {
                    DTCCheckBox.IsChecked = false;
                    DtcTextBox.IsEnabled = false;
                }

                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }
            await viDiagnostocMessageCD.ShowAsync();

        }

        private void ViDiagnostocMessageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? ecuHeader = null;
            if (ECUHeaderCheckBox.IsChecked == true)
            {
                if (ECUHeaderTextBox.Text != null)
                {
                    try
                    {
                        ecuHeader = Int32.Parse(ECUHeaderTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            List<String> dtcList = null;
            if (DTCCheckBox.IsChecked == true)
            {
                dtcList = new List<String>();
                if (DtcTextBox.Text != null)
                {
                    String[] messageList = DtcTextBox.Text.ToString().Split(',');
                    if (messageList != null && messageList.Length > 0)
                    {
                        foreach (String s in messageList)
                        {
                            try
                            {
                                dtcList.Add(s);
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
            }

            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetDTCsResponse(
                BuildRpc.getNextId(), resultCode, ecuHeader, dtcList);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViDiagnostocMessageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViDiagnostocMessageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ECUHeaderCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECUHeaderTextBox.IsEnabled = (bool)ECUHeaderCheckBox.IsChecked;
        }

        private void DTCCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DtcTextBox.IsEnabled = (bool)DTCCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
