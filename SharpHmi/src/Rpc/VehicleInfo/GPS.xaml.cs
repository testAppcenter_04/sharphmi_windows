﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class GPS : UserControl
    {
        public CheckBox longitudeCheckBox;
        public TextBox longitudeTextBox;
        public CheckBox latitudeCheckBox;
        public TextBox latitudeTextBox;
        public CheckBox utcYearCheckBox;
        public TextBox utcYearTextBox;
        public CheckBox utcMonthCheckBox;
        public TextBox utcMonthTextBox;
        public CheckBox utcDayCheckBox;
        public TextBox utcDayTextBox;
        public CheckBox utcHoursCheckBox;
        public TextBox utcHoursTextBox;
        public CheckBox utcMinutesCheckBox;
        public TextBox utcMinutesTextBox;
        public CheckBox utcSecondsCheckBox;
        public TextBox utcSecondsTextBox;
        public CheckBox compassDirectionCheckBox;
        public ComboBox compassDirectionComboBox;
        public CheckBox pdopCheckBox;
        public TextBox pdopTextBox;
        public CheckBox hdopCheckBox;
        public TextBox hdopTextBox;
        public CheckBox vdopCheckBox;
        public TextBox vdopTextBox;
        public CheckBox actualCheckBox;
        public CheckBox satelitesCheckBox;
        public TextBox satellitesTextBox;
        public CheckBox dimensionChecKbox;
        public ComboBox dimensionComboBox;
        public CheckBox altitudeChecKBox;
        public TextBox altitudeTextBox;
        public CheckBox headingChecKBox;
        public TextBox headingTextBox;
        public CheckBox speedChecKBox;
        public TextBox speedTextBox;


        public GPS(HmiApiLib.Common.Structs.GPSData gpsData)
        {
            InitializeComponent();
            
            CompassDirectionComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.CompassDirection));
            CompassDirectionComboBox.SelectedIndex = 0;

            DimensionComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Dimension));
            DimensionComboBox.SelectedIndex = 0;

            longitudeCheckBox = LongitudeCheckBox;
            longitudeTextBox = LongitudeTextBox;
            latitudeCheckBox = LatitudeCheckBox;
            latitudeTextBox = LatitudeTextBox;
            utcYearCheckBox = UTCYearCheckBox;
            utcYearTextBox = UTCYearTextBox;
            utcMonthCheckBox = UTCMonthCheckBox;
            utcMonthTextBox = UTCMonthTextBox;
            utcDayCheckBox = UTCDayCheckBox;
            utcDayTextBox = UTCDayTextBox;
            utcHoursCheckBox = UTCHoursCheckBox;
            utcHoursTextBox = UTCHoursTextBox;
            utcMinutesCheckBox = UTCMinutesCheckBox;
            utcMinutesTextBox = UTCMinutesTextBox;
            utcSecondsCheckBox = UTCSecondsCheckBox;
            utcSecondsTextBox = UTCSecondsTextBox;
            compassDirectionCheckBox = CompassDirectionCheckBox;
            compassDirectionComboBox = CompassDirectionComboBox;
            pdopCheckBox = PdopCheckBox;
            pdopTextBox = PdopTextBox;
            hdopCheckBox = HdopCheckBox;
            hdopTextBox = HdopTextBox;
            vdopCheckBox = VdopCheckBox;
            vdopTextBox = VdopTextBox;
            actualCheckBox = ActualCheckBox;
            satelitesCheckBox = SatellitesCheckBox;
            satellitesTextBox = SatellitesTextBox;
            dimensionChecKbox = DimensionCheckBox;
            dimensionComboBox = DimensionComboBox;
            altitudeChecKBox = AltitudeCheckBox;
            altitudeTextBox = AltitudeTextBox;
            headingChecKBox = HeadingCheckBox;
            headingTextBox = HeadingTextBox;
            speedChecKBox = SpeedCheckBox;
            speedTextBox = SpeedTextBox;

            if (null != gpsData)
            {
                if (gpsData.getLongitudeDegrees() != null)
                {
                    LongitudeTextBox.Text = gpsData.getLongitudeDegrees().ToString();
                    LongitudeCheckBox.IsChecked = true;
                }
                else
                {
                    LongitudeTextBox.IsEnabled = false;

                }
                if (gpsData.getLatitudeDegrees() != null)
                {
                    LatitudeTextBox.Text = gpsData.getLatitudeDegrees().ToString();
                    LatitudeCheckBox.IsChecked = true;

                }
                else
                {
                    LatitudeTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcYear() != null)
                {
                    UTCYearTextBox.Text = gpsData.getUtcYear().ToString();
                    UTCYearCheckBox.IsChecked = true;

                }
                else
                {
                    UTCYearTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcMonth() != null)
                {
                    UTCMonthTextBox.Text = gpsData.getUtcMonth().ToString();
                    UTCMonthCheckBox.IsChecked = true;

                }
                else
                {
                    UTCMonthTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcMonth() != null)
                {
                    UTCDayTextBox.Text = gpsData.getUtcDay().ToString();
                    UTCDayCheckBox.IsChecked = true;

                }
                else
                {
                    UTCDayTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcHours() != null)
                {
                    UTCHoursTextBox.Text = gpsData.getUtcHours().ToString();
                    UTCHoursCheckBox.IsChecked = true;

                }
                else
                {
                    UTCHoursTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcMinutes() != null)
                {
                    UTCMinutesTextBox.Text = gpsData.getUtcMinutes().ToString();
                    UTCMinutesCheckBox.IsChecked = true;

                }
                else
                {
                    UTCMinutesTextBox.IsEnabled = false;
                }
                if (gpsData.getUtcSeconds() != null)
                {
                    UTCSecondsTextBox.Text = gpsData.getUtcSeconds().ToString();
                    UTCSecondsCheckBox.IsChecked = true;
                }
                else
                {
                    UTCSecondsTextBox.IsEnabled = false;

                }
                if (gpsData.getCompassDirection() != null)
                {
                    CompassDirectionComboBox.SelectedIndex = (int)gpsData.getCompassDirection();
                    CompassDirectionCheckBox.IsChecked = true;

                }

                else
                {
                    CompassDirectionComboBox.IsEnabled = false;
                }
                if (gpsData.getPdop() != null)
                {
                    PdopTextBox.Text = gpsData.getPdop().ToString();
                    PdopCheckBox.IsChecked = true;

                }
                else
                {
                    PdopTextBox.IsEnabled = false;
                }
                if (gpsData.getHdop() != null)
                {
                    HdopTextBox.Text = gpsData.getHdop().ToString();
                    HdopCheckBox.IsChecked = true;

                }
                else
                {
                    HdopTextBox.IsEnabled = false;
                }
                if (gpsData.getVdop() != null)
                {
                    VdopTextBox.Text = gpsData.getVdop().ToString();
                    VdopCheckBox.IsChecked = true;

                }
                else
                {
                    VdopTextBox.IsEnabled = false;
                }
                if (gpsData.getActual() != null)

                    ActualCheckBox.IsChecked = gpsData.getActual();
                else
                {
                    ActualCheckBox.IsChecked = false;
                }
                if (gpsData.getSatellites() != null)
                {
                    SatellitesTextBox.Text = gpsData.getSatellites().ToString();
                    SatellitesCheckBox.IsChecked = true;

                }
                else
                {
                    SatellitesTextBox.IsEnabled = false;

                }
                if (gpsData.getDimension() != null)
                {
                    DimensionComboBox.SelectedIndex = (int)gpsData.getDimension();
                    DimensionCheckBox.IsChecked = true;

                }
                else
                {
                    DimensionComboBox.IsEnabled = false;

                }
                if (gpsData.getAltitude() != null)
                {
                    AltitudeTextBox.Text = gpsData.getAltitude().ToString();
                    AltitudeCheckBox.IsChecked = true;

                }
                else
                {
                    AltitudeTextBox.IsEnabled = false;
                }
                if (gpsData.getHeading() != null)
                {

                    HeadingTextBox.Text = gpsData.getHeading().ToString();
                    HeadingCheckBox.IsChecked = true;

                }
                else
                {
                    HeadingTextBox.IsEnabled = false;
                }
                if (gpsData.getSpeed() != null)
                {
                    SpeedTextBox.Text = gpsData.getSpeed().ToString();
                    SpeedCheckBox.IsChecked = true;

                }
                else
                {
                    SpeedTextBox.IsEnabled = false;
                }
            }
        }

        private void LongitudeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LongitudeTextBox.IsEnabled = (bool)LongitudeCheckBox.IsChecked;
        }

        private void LatitudeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LatitudeTextBox.IsEnabled = (bool)LatitudeCheckBox.IsChecked;
        }

        private void UTCYearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCYearTextBox.IsEnabled = (bool)UTCYearCheckBox.IsChecked;
        }

        private void UTCMonthLongitudeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCMonthTextBox.IsEnabled = (bool)UTCMonthCheckBox.IsChecked;
        }

        private void UTCDayCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCDayTextBox.IsEnabled = (bool)UTCDayCheckBox.IsChecked;
        }

        private void UTCHoursCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCHoursTextBox.IsEnabled = (bool)UTCHoursCheckBox.IsChecked;
        }

        private void UTCMinutesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCMinutesTextBox.IsEnabled = (bool)UTCMinutesCheckBox.IsChecked;
        }

        private void UTCSecondsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UTCSecondsTextBox.IsEnabled = (bool)UTCSecondsCheckBox.IsChecked;
        }

        private void CompassDirectionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CompassDirectionComboBox.IsEnabled = (bool)CompassDirectionCheckBox.IsChecked;
        }

        private void PdopCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PdopTextBox.IsEnabled = (bool)PdopCheckBox.IsChecked;
        }

        private void HdopCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HdopTextBox.IsEnabled = (bool)HdopCheckBox.IsChecked;
        }

        private void VdopCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VdopTextBox.IsEnabled = (bool)VdopCheckBox.IsChecked;
        }

        private void SatellitesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SatellitesTextBox.IsEnabled = (bool)SatellitesCheckBox.IsChecked;
        }

        private void DimensionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DimensionComboBox.IsEnabled = (bool)DimensionCheckBox.IsChecked;
        }

        private void AltitudeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AltitudeTextBox.IsEnabled = (bool)AltitudeCheckBox.IsChecked;
        }

        private void HeadingCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadingTextBox.IsEnabled = (bool)HeadingCheckBox.IsChecked;
        }

        private void SpeedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeedTextBox.IsEnabled = (bool)SpeedCheckBox.IsChecked;
        }
    }
}
