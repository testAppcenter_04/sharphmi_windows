﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class AirbagStatus : UserControl
    {
        public CheckBox driverAirbagDeployedCheckBox;
        public ComboBox driverAirbagDeployedComboBox;

        public CheckBox driverSideAirbagDeployedCheckBox;
        public ComboBox driverSideAirbagDeployedComboBox;

        public CheckBox driverCurtainAirbagDeployedCheckBox;
        public ComboBox driverCurtainAirbagDeployedComboBox;

        public CheckBox passengerAirbagDeployedCheckBox;
        public ComboBox passengerAirbagDeployedComboBox;

        public CheckBox passengerCurtainAirbagDeployedCheckBox;
        public ComboBox passengerCurtainAirbagDeployedComboBox;

        public CheckBox driverKneeAirbagDeployedCheckBox;
        public ComboBox driverKneeAirbagDeployedComboBox;

        public CheckBox passengerSideAirbagDeployedCheckBox;
        public ComboBox passengerSideAirbagDeployedComboBox;

        public CheckBox passengerKneeAirbagDeployedCheckBox;
        public ComboBox passengerKneeAirbagDeployedComboBox;

        public AirbagStatus(HmiApiLib.Common.Structs.AirbagStatus airbagStatus)
        {
            InitializeComponent();

            DriverAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverAirbagDeployedComboBox.SelectedIndex = 0;

            DriverSideAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverSideAirbagDeployedComboBox.SelectedIndex = 0;

            DriverCurtainAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverCurtainAirbagDeployedComboBox.SelectedIndex = 0;

            PassengerAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerAirbagDeployedComboBox.SelectedIndex = 0;

            PassengerCurtainAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerCurtainAirbagDeployedComboBox.SelectedIndex = 0;

            DriverKneeAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverKneeAirbagDeployedComboBox.SelectedIndex = 0;

            PassengerSideAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerSideAirbagDeployedComboBox.SelectedIndex = 0;

            PassengerKneeAirbagDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerKneeAirbagDeployedComboBox.SelectedIndex = 0;


            driverAirbagDeployedCheckBox =  DriverAirbagDeployedCheckBox;
            driverAirbagDeployedComboBox = DriverAirbagDeployedComboBox;

            driverSideAirbagDeployedCheckBox = DriverSideAirbagDeployedCheckBox;
            driverSideAirbagDeployedComboBox = DriverSideAirbagDeployedComboBox;

            driverCurtainAirbagDeployedCheckBox = DriverCurtainAirbagDeployedCheckBox;
            driverCurtainAirbagDeployedComboBox = DriverCurtainAirbagDeployedComboBox;

            passengerAirbagDeployedCheckBox = PassengerAirbagDeployedCheckBox;
            passengerAirbagDeployedComboBox = PassengerAirbagDeployedComboBox;

            passengerCurtainAirbagDeployedCheckBox = PassengerCurtainAirbagDeployedCheckBox;
            passengerCurtainAirbagDeployedComboBox = PassengerCurtainAirbagDeployedComboBox;

            driverKneeAirbagDeployedCheckBox = DriverKneeAirbagDeployedCheckBox;
            driverKneeAirbagDeployedComboBox = DriverKneeAirbagDeployedComboBox;

            passengerSideAirbagDeployedCheckBox = PassengerSideAirbagDeployedCheckBox;
            passengerSideAirbagDeployedComboBox = PassengerSideAirbagDeployedComboBox;

            passengerKneeAirbagDeployedCheckBox = PassengerKneeAirbagDeployedCheckBox;
            passengerKneeAirbagDeployedComboBox = PassengerKneeAirbagDeployedComboBox;

            if (null != airbagStatus)
            {
                if (airbagStatus.getDriverAirbagDeployed() != null)
                {
                    DriverAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getDriverAirbagDeployed();
                    DriverAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    DriverAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getDriverSideAirbagDeployed() != null)
                {
                    DriverSideAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getDriverSideAirbagDeployed();
                    DriverSideAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    DriverSideAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getDriverCurtainAirbagDeployed() != null)
                {
                    DriverCurtainAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getDriverCurtainAirbagDeployed();
                    DriverCurtainAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    DriverCurtainAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getPassengerAirbagDeployed() != null)
                {
                    PassengerAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getPassengerAirbagDeployed();
                    PassengerAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getPassengerCurtainAirbagDeployed() != null)
                {
                    PassengerCurtainAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getPassengerCurtainAirbagDeployed();
                    PassengerCurtainAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerCurtainAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getDriverKneeAirbagDeployed() != null)
                {
                    DriverKneeAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getDriverKneeAirbagDeployed();
                    DriverKneeAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    DriverKneeAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getPassengerSideAirbagDeployed() != null)
                {
                    PassengerSideAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getPassengerSideAirbagDeployed();
                    PassengerSideAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerSideAirbagDeployedComboBox.IsEnabled = false;
                }
                if (airbagStatus.getPassengerKneeAirbagDeployed() != null)
                {
                    PassengerKneeAirbagDeployedComboBox.SelectedIndex = (int)airbagStatus.getPassengerKneeAirbagDeployed();
                    PassengerKneeAirbagDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerKneeAirbagDeployedComboBox.IsEnabled = false;
                }
            }
    }

        private void DriverAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverAirbagDeployedComboBox.IsEnabled = (bool)DriverAirbagDeployedCheckBox.IsChecked;
        }

        private void DriverSideAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverSideAirbagDeployedComboBox.IsEnabled = (bool)DriverSideAirbagDeployedCheckBox.IsChecked;
        }

        private void DriverCurtainAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverCurtainAirbagDeployedComboBox.IsEnabled = (bool)DriverCurtainAirbagDeployedCheckBox.IsChecked;
        }

        private void PassengerAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerAirbagDeployedComboBox.IsEnabled = (bool)PassengerAirbagDeployedCheckBox.IsChecked;
        }

        private void PassengerCurtainAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerCurtainAirbagDeployedComboBox.IsEnabled = (bool)PassengerCurtainAirbagDeployedCheckBox.IsChecked;
        }

        private void DriverKneeAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverKneeAirbagDeployedComboBox.IsEnabled = (bool)DriverKneeAirbagDeployedCheckBox.IsChecked;
        }

        private void PassengerSideAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerSideAirbagDeployedComboBox.IsEnabled = (bool)PassengerSideAirbagDeployedCheckBox.IsChecked;
        }

        private void PassengerKneeAirbagDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerKneeAirbagDeployedComboBox.IsEnabled = (bool)PassengerKneeAirbagDeployedCheckBox.IsChecked;
        }
    }
}
