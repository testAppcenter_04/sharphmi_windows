﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIUnsubscribeVehicleData : UserControl
    {
        UnsubscribeVehicleData tmpObj = null;
        public VIUnsubscribeVehicleData()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;


            GpsDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            GpsDataTypeComboBox.SelectedIndex = 0;

            SpeedDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            SpeedDataTypeComboBox.SelectedIndex = 0;

            RpmDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            RpmDataTypeComboBox.SelectedIndex = 0;

            FuelLevelDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            FuelLevelDataTypeComboBox.SelectedIndex = 0;

            FuelLevelStateDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            FuelLevelStateDataTypeComboBox.SelectedIndex = 0;

            InstantFuelConsumptionDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            InstantFuelConsumptionDataTypeComboBox.SelectedIndex = 0;

            ExternalTemperatureDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            ExternalTemperatureDataTypeComboBox.SelectedIndex = 0;

            PRNDLDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            PRNDLDataTypeComboBox.SelectedIndex = 0;

            TurnSignalDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            TurnSignalDataTypeComboBox.SelectedIndex = 0;

            TirePressureDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            TirePressureDataTypeComboBox.SelectedIndex = 0;

            OdometerDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            OdometerDataTypeComboBox.SelectedIndex = 0;

            BeltStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            BeltStatusDataTypeComboBox.SelectedIndex = 0;

            BodyInformationDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            BodyInformationDataTypeComboBox.SelectedIndex = 0;

            DeviceStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            DeviceStatusDataTypeComboBox.SelectedIndex = 0;

            DriverBrakingDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            DriverBrakingDataTypeComboBox.SelectedIndex = 0;

            WiperStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            WiperStatusDataTypeComboBox.SelectedIndex = 0;

            HeadLampStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            HeadLampStatusDataTypeComboBox.SelectedIndex = 0;

            EngineTorqueDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            EngineTorqueDataTypeComboBox.SelectedIndex = 0;

            AccPedalPositionDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            AccPedalPositionDataTypeComboBox.SelectedIndex = 0;

            SteeringWheelAngleDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            SteeringWheelAngleDataTypeComboBox.SelectedIndex = 0;

            ECallInfoDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            ECallInfoDataTypeComboBox.SelectedIndex = 0;

            AirbagStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            AirbagStatusDataTypeComboBox.SelectedIndex = 0;

            EmergencyEventDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            EmergencyEventDataTypeComboBox.SelectedIndex = 0;

            ClusterModesDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            ClusterModesDataTypeComboBox.SelectedIndex = 0;

            MyKeyDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            MyKeyDataTypeComboBox.SelectedIndex = 0;

            ElectronicParkBrakeStatusDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            ElectronicParkBrakeStatusDataTypeComboBox.SelectedIndex = 0;

            EngineOilLifeDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            EngineOilLifeDataTypeComboBox.SelectedIndex = 0;

            FuelRangeDataTypeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataType));
            FuelRangeDataTypeComboBox.SelectedIndex = 0;


            GpsResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            GpsResultCodeComboBox.SelectedIndex = 0;

            SpeedResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            SpeedResultCodeComboBox.SelectedIndex = 0;

            RpmResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            RpmResultCodeComboBox.SelectedIndex = 0;

            FuelLevelResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            FuelLevelResultCodeComboBox.SelectedIndex = 0;

            FuelLevelStateResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            FuelLevelStateResultCodeComboBox.SelectedIndex = 0;

            InstantFuelConsumptionResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            InstantFuelConsumptionResultCodeComboBox.SelectedIndex = 0;

            ExternalTemperatureResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            ExternalTemperatureResultCodeComboBox.SelectedIndex = 0;

            PRNDLResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            PRNDLResultCodeComboBox.SelectedIndex = 0;

            TurnSignalResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            TurnSignalResultCodeComboBox.SelectedIndex = 0;

            TirePressureResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            TirePressureResultCodeComboBox.SelectedIndex = 0;

            OdometerResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            OdometerResultCodeComboBox.SelectedIndex = 0;

            BeltStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            BeltStatusResultCodeComboBox.SelectedIndex = 0;

            BodyInformationResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            BodyInformationResultCodeComboBox.SelectedIndex = 0;

            DeviceStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            DeviceStatusResultCodeComboBox.SelectedIndex = 0;

            DriverBrakingResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            DriverBrakingResultCodeComboBox.SelectedIndex = 0;

            WiperStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            WiperStatusResultCodeComboBox.SelectedIndex = 0;

            HeadLampStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            HeadLampStatusResultCodeComboBox.SelectedIndex = 0;

            EngineTorqueResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            EngineTorqueResultCodeComboBox.SelectedIndex = 0;

            AccPedalPositionResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            AccPedalPositionResultCodeComboBox.SelectedIndex = 0;

            SteeringWheelAngleResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            SteeringWheelAngleResultCodeComboBox.SelectedIndex = 0;

            ECallInfoResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            ECallInfoResultCodeComboBox.SelectedIndex = 0;

            AirbagStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            AirbagStatusResultCodeComboBox.SelectedIndex = 0;

            EmergencyEventResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            EmergencyEventResultCodeComboBox.SelectedIndex = 0;

            ClusterModesResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            ClusterModesResultCodeComboBox.SelectedIndex = 0;

            MyKeyResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            MyKeyResultCodeComboBox.SelectedIndex = 0;

            ElectronicParkBrakeStatusResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            ElectronicParkBrakeStatusResultCodeComboBox.SelectedIndex = 0;

            EngineOilLifeResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            EngineOilLifeResultCodeComboBox.SelectedIndex = 0;

            FuelRangeResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(VehicleDataResultCode));
            FuelRangeResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowViUnsubVehicle()
        {
            ContentDialog viSubVehicleCD = new ContentDialog();
            viSubVehicleCD.Content = this;

            viSubVehicleCD.PrimaryButtonText = Const.TxLater;
            viSubVehicleCD.PrimaryButtonClick += ViSubVehicleCD_PrimaryButtonClick;

            viSubVehicleCD.SecondaryButtonText = Const.Reset;
            viSubVehicleCD.SecondaryButtonClick += ViSubVehicleCD_ResetButtonClick;

            viSubVehicleCD.CloseButtonText = Const.Close;
            viSubVehicleCD.CloseButtonClick += ViSubVehicleCD_CloseButtonClick;

            tmpObj = new UnsubscribeVehicleData();
            tmpObj = (UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<UnsubscribeVehicleData>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(UnsubscribeVehicleData);
                tmpObj = (UnsubscribeVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (null != tmpObj.getGps())
                {
                    if (tmpObj.getGps().getDataType() != null)
                        GpsDataTypeComboBox.SelectedIndex = (int)tmpObj.getGps().getDataType();
                    else
                    {
                        GpsDataTypeComboBox.IsEnabled = false;
                        GpsDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getGps().getResultCode() != null)
                        GpsResultCodeComboBox.SelectedIndex = (int)tmpObj.getGps().getResultCode();
                    else
                    {
                        GpsResultCodeComboBox.IsEnabled = false;
                        GpsResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    GpsDataTypeComboBox.IsEnabled = false;
                    GpsResultCodeComboBox.IsEnabled = false;
                    GpsDataTypeCheckBox.IsEnabled = false;
                    GpsResultCodeCheckBox.IsEnabled = false;
                    GpsCheckBox.IsChecked = false;
                }


                if (null != tmpObj.getSpeed())
                {
                    if (tmpObj.getSpeed().getDataType() != null)
                        SpeedDataTypeComboBox.SelectedIndex = (int)tmpObj.getSpeed().getDataType();
                    else
                    {
                        SpeedDataTypeComboBox.IsEnabled = false;
                        SpeedDataTypeCheckBox.IsChecked = false;
                    }

                    if (tmpObj.getSpeed().getResultCode() != null)
                        SpeedResultCodeComboBox.SelectedIndex = (int)tmpObj.getSpeed().getResultCode();
                    else
                    {
                        SpeedResultCodeComboBox.IsEnabled = false;
                        SpeedResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    SpeedDataTypeComboBox.IsEnabled = false;
                    SpeedResultCodeComboBox.IsEnabled = false;
                    SpeedDataTypeCheckBox.IsEnabled = false;
                    SpeedResultCodeCheckBox.IsEnabled = false;
                    SpeedCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getRpm())
                {
                    if (tmpObj.getRpm().getDataType() != null)
                        RpmDataTypeComboBox.SelectedIndex = (int)tmpObj.getRpm().getDataType();
                    else
                    {
                        RpmDataTypeComboBox.IsEnabled = false;
                        RpmDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getRpm().getResultCode() != null)
                        RpmResultCodeComboBox.SelectedIndex = (int)tmpObj.getRpm().getResultCode();
                    else
                    {
                        RpmResultCodeComboBox.IsEnabled = false;
                        RpmResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    RpmDataTypeComboBox.IsEnabled = false;
                    RpmResultCodeComboBox.IsEnabled = false;
                    RpmDataTypeCheckBox.IsEnabled = false;
                    RpmResultCodeCheckBox.IsEnabled = false;
                    RpmCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getFuelLevel())
                {
                    if (tmpObj.getFuelLevel().getDataType() != null)
                        FuelLevelDataTypeComboBox.SelectedIndex = (int)tmpObj.getFuelLevel().getDataType();
                    else
                    {
                        FuelLevelDataTypeComboBox.IsEnabled = false;
                        FuelLevelDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getFuelLevel().getResultCode() != null)
                        FuelLevelResultCodeComboBox.SelectedIndex = (int)tmpObj.getFuelLevel().getResultCode();
                    else
                    {
                        FuelLevelResultCodeComboBox.IsEnabled = false;
                        FuelLevelResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    FuelLevelDataTypeComboBox.IsEnabled = false;
                    FuelLevelResultCodeComboBox.IsEnabled = false;
                    FuelLevelDataTypeCheckBox.IsEnabled = false;
                    FuelLevelResultCodeCheckBox.IsEnabled = false;
                    FuelLevelCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getFuelLevel_State())
                {
                    if (tmpObj.getFuelLevel_State().getDataType() != null)
                        FuelLevelStateDataTypeComboBox.SelectedIndex = (int)tmpObj.getFuelLevel_State().getDataType();
                    else
                    {
                        FuelLevelStateDataTypeComboBox.IsEnabled = false;
                        FuelLevelStateDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getFuelLevel_State().getResultCode() != null)
                        FuelLevelStateResultCodeComboBox.SelectedIndex = (int)tmpObj.getFuelLevel_State().getResultCode();
                    else
                    {
                        FuelLevelStateResultCodeComboBox.IsEnabled = false;
                        FuelLevelStateResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    FuelLevelStateDataTypeComboBox.IsEnabled = false;
                    FuelLevelStateResultCodeComboBox.IsEnabled = false;
                    FuelLevelStateDataTypeCheckBox.IsEnabled = false;
                    FuelLevelStateResultCodeCheckBox.IsEnabled = false;
                    FuelLevelStateCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getInstantFuelConsumption())
                {
                    if (tmpObj.getInstantFuelConsumption().getDataType() != null)
                        InstantFuelConsumptionDataTypeComboBox.SelectedIndex = (int)tmpObj.getInstantFuelConsumption().getDataType();
                    else
                    {
                        InstantFuelConsumptionDataTypeComboBox.IsEnabled = false;
                        InstantFuelConsumptionDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getInstantFuelConsumption().getResultCode() != null)
                        InstantFuelConsumptionResultCodeComboBox.SelectedIndex = (int)tmpObj.getInstantFuelConsumption().getResultCode();
                    else
                    {
                        InstantFuelConsumptionResultCodeComboBox.IsEnabled = false;
                        InstantFuelConsumptionResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    InstantFuelConsumptionDataTypeComboBox.IsEnabled = false;
                    InstantFuelConsumptionResultCodeComboBox.IsEnabled = false;
                    InstantFuelConsumptionDataTypeCheckBox.IsEnabled = false;
                    InstantFuelConsumptionResultCodeCheckBox.IsEnabled = false;
                    InstantFuelConsumptionCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getExternalTemperature())
                {
                    if (tmpObj.getExternalTemperature().getDataType() != null)
                        ExternalTemperatureDataTypeComboBox.SelectedIndex = (int)tmpObj.getExternalTemperature().getDataType();
                    else
                    {
                        ExternalTemperatureDataTypeComboBox.IsEnabled = false;
                        ExternalTemperatureDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getExternalTemperature().getResultCode() != null)
                        ExternalTemperatureResultCodeComboBox.SelectedIndex = (int)tmpObj.getExternalTemperature().getResultCode();
                    else
                    {
                        ExternalTemperatureResultCodeComboBox.IsEnabled = false;
                        ExternalTemperatureResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    ExternalTemperatureDataTypeComboBox.IsEnabled = false;
                    ExternalTemperatureResultCodeComboBox.IsEnabled = false;
                    ExternalTemperatureDataTypeCheckBox.IsEnabled = false;
                    ExternalTemperatureResultCodeCheckBox.IsEnabled = false;
                    ExternalTemperatureCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getPrndl())
                {
                    if (tmpObj.getPrndl().getDataType() != null)
                        PRNDLDataTypeComboBox.SelectedIndex = (int)tmpObj.getPrndl().getDataType();
                    else
                    {
                        PRNDLDataTypeComboBox.IsEnabled = false;
                        PRNDLDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getPrndl().getResultCode() != null)
                        PRNDLResultCodeComboBox.SelectedIndex = (int)tmpObj.getPrndl().getResultCode();
                    else
                    {
                        PRNDLResultCodeComboBox.IsEnabled = false;
                        PRNDLResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    PRNDLDataTypeComboBox.IsEnabled = false;
                    PRNDLResultCodeComboBox.IsEnabled = false;
                    PRNDLDataTypeCheckBox.IsEnabled = false;
                    PRNDLResultCodeCheckBox.IsEnabled = false;
                    PRNDLCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getTurnSignal())
                {
                    if (tmpObj.getTurnSignal().getDataType() != null)
                        TurnSignalDataTypeComboBox.SelectedIndex = (int)tmpObj.getTurnSignal().getDataType();
                    else
                    {
                        TurnSignalDataTypeComboBox.IsEnabled = false;
                        TurnSignalDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getTurnSignal().getResultCode() != null)
                        TurnSignalResultCodeComboBox.SelectedIndex = (int)tmpObj.getTurnSignal().getResultCode();
                    else
                    {
                        TurnSignalResultCodeComboBox.IsEnabled = false;
                        TurnSignalResultCodeCheckBox.IsChecked = false;
                    }

                }
                else
                {
                    TurnSignalDataTypeComboBox.IsEnabled = false;
                    TurnSignalResultCodeComboBox.IsEnabled = false;
                    TurnSignalDataTypeCheckBox.IsEnabled = false;
                    TurnSignalResultCodeCheckBox.IsEnabled = false;
                    TurnSignalCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getTirePressure())
                {
                    if (tmpObj.getTirePressure().getDataType() != null)
                        TirePressureDataTypeComboBox.SelectedIndex = (int)tmpObj.getTirePressure().getDataType();
                    else
                    {
                        TirePressureDataTypeComboBox.IsEnabled = false;
                        TirePressureDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getTirePressure().getResultCode() != null)
                        TirePressureResultCodeComboBox.SelectedIndex = (int)tmpObj.getTirePressure().getResultCode();
                    else
                    {
                        TirePressureResultCodeComboBox.IsEnabled = false;
                        TirePressureResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    TirePressureDataTypeComboBox.IsEnabled = false;
                    TirePressureResultCodeComboBox.IsEnabled = false;
                    TirePressureDataTypeCheckBox.IsEnabled = false;
                    TirePressureResultCodeCheckBox.IsEnabled = false;
                    TirePressureCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getOdometer())
                {
                    if (tmpObj.getOdometer().getDataType() != null)

                        OdometerDataTypeComboBox.SelectedIndex = (int)tmpObj.getOdometer().getDataType();
                    else
                    {
                        OdometerDataTypeComboBox.IsEnabled = false;
                        OdometerDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getOdometer().getResultCode() != null)
                        OdometerResultCodeComboBox.SelectedIndex = (int)tmpObj.getOdometer().getResultCode();
                    else
                    {
                        OdometerResultCodeComboBox.IsEnabled = false;
                        OdometerResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    OdometerDataTypeComboBox.IsEnabled = false;
                    OdometerResultCodeComboBox.IsEnabled = false;
                    OdometerDataTypeCheckBox.IsEnabled = false;
                    OdometerResultCodeCheckBox.IsEnabled = false;
                    OdometerCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getBeltStatus())
                {
                    if (tmpObj.getBeltStatus().getDataType() != null)
                        BeltStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getBeltStatus().getDataType();
                    else
                    {
                        BeltStatusDataTypeComboBox.IsEnabled = false;
                        BeltStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getBeltStatus().getResultCode() != null)
                        BeltStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getBeltStatus().getResultCode();
                    else
                    {
                        BeltStatusResultCodeComboBox.IsEnabled = false;
                        BeltStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    BeltStatusDataTypeComboBox.IsEnabled = false;
                    BeltStatusResultCodeComboBox.IsEnabled = false;
                    BeltStatusDataTypeCheckBox.IsEnabled = false;
                    BeltStatusResultCodeCheckBox.IsEnabled = false;
                    BeltStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getBodyInformation())
                {
                    if (tmpObj.getBodyInformation().getDataType() != null)
                        BodyInformationDataTypeComboBox.SelectedIndex = (int)tmpObj.getBodyInformation().getDataType();
                    else
                    {
                        BodyInformationDataTypeComboBox.IsEnabled = false;
                        BodyInformationDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getBodyInformation().getResultCode() != null)
                        BodyInformationResultCodeComboBox.SelectedIndex = (int)tmpObj.getBodyInformation().getResultCode();
                    else
                    {
                        BodyInformationResultCodeComboBox.IsEnabled = false;
                        BodyInformationResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    BodyInformationDataTypeComboBox.IsEnabled = false;
                    BodyInformationResultCodeComboBox.IsEnabled = false;
                    BodyInformationDataTypeCheckBox.IsEnabled = false;
                    BodyInformationResultCodeCheckBox.IsEnabled = false;
                    BodyInformationCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getDeviceStatus())
                {
                    if (tmpObj.getDeviceStatus().getDataType() != null)
                        DeviceStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getDeviceStatus().getDataType();
                    else
                    {
                        DeviceStatusDataTypeComboBox.IsEnabled = false;
                        DeviceStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getDeviceStatus().getResultCode() != null)
                        DeviceStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getDeviceStatus().getResultCode();
                    else
                    {
                        DeviceStatusResultCodeComboBox.IsEnabled = false;
                        DeviceStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    DeviceStatusDataTypeComboBox.IsEnabled = false;
                    DeviceStatusResultCodeComboBox.IsEnabled = false;
                    DeviceStatusDataTypeCheckBox.IsEnabled = false;
                    DeviceStatusResultCodeCheckBox.IsEnabled = false;
                    DeviceStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getdriverBraking())
                {
                    if (tmpObj.getdriverBraking().getDataType() != null)
                        DriverBrakingDataTypeComboBox.SelectedIndex = (int)tmpObj.getdriverBraking().getDataType();
                    else
                    {
                        DriverBrakingDataTypeComboBox.IsEnabled = false;
                        DriverBrakingDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getdriverBraking().getResultCode() != null)
                        DriverBrakingResultCodeComboBox.SelectedIndex = (int)tmpObj.getdriverBraking().getResultCode();
                    else
                    {
                        DriverBrakingResultCodeComboBox.IsEnabled = false;
                        DriverBrakingResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    DriverBrakingDataTypeComboBox.IsEnabled = false;
                    DriverBrakingResultCodeComboBox.IsEnabled = false;
                    DriverBrakingDataTypeCheckBox.IsEnabled = false;
                    DriverBrakingResultCodeCheckBox.IsEnabled = false;
                    DriverBrakingCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getWiperStatus())
                {
                    if (tmpObj.getWiperStatus().getDataType() != null)
                        WiperStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getWiperStatus().getDataType();
                    else
                    {
                        WiperStatusDataTypeComboBox.IsEnabled = false;
                        WiperStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getWiperStatus().getResultCode() != null)
                        WiperStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getWiperStatus().getResultCode();
                    else
                    {
                        WiperStatusResultCodeComboBox.IsEnabled = false;
                        WiperStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    WiperStatusDataTypeComboBox.IsEnabled = false;
                    WiperStatusResultCodeComboBox.IsEnabled = false;
                    WiperStatusDataTypeCheckBox.IsEnabled = false;
                    WiperStatusResultCodeCheckBox.IsEnabled = false;
                    WiperStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getHeadLampStatus())
                {
                    if (tmpObj.getHeadLampStatus().getDataType() != null)
                        HeadLampStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getHeadLampStatus().getDataType();
                    else
                    {
                        HeadLampStatusDataTypeComboBox.IsEnabled = false;
                        HeadLampStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getHeadLampStatus().getResultCode() != null)
                        HeadLampStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getHeadLampStatus().getResultCode();
                    else
                    {
                        HeadLampStatusResultCodeComboBox.IsEnabled = false;
                        HeadLampStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    HeadLampStatusDataTypeComboBox.IsEnabled = false;
                    HeadLampStatusResultCodeComboBox.IsEnabled = false;
                    HeadLampStatusDataTypeCheckBox.IsEnabled = false;
                    HeadLampStatusResultCodeCheckBox.IsEnabled = false;
                    HeadLampStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getEngineTorque())
                {
                    if (tmpObj.getHeadLampStatus().getDataType() != null)
                        EngineTorqueDataTypeComboBox.SelectedIndex = (int)tmpObj.getEngineTorque().getDataType();
                    else
                    {
                        EngineTorqueDataTypeComboBox.IsEnabled = false;
                        EngineTorqueDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getHeadLampStatus().getResultCode() != null)
                        EngineTorqueResultCodeComboBox.SelectedIndex = (int)tmpObj.getEngineTorque().getResultCode();
                    else
                    {
                        EngineTorqueResultCodeComboBox.IsEnabled = false;
                        EngineTorqueResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    EngineTorqueDataTypeComboBox.IsEnabled = false;
                    EngineTorqueResultCodeComboBox.IsEnabled = false;
                    EngineTorqueDataTypeCheckBox.IsEnabled = false;
                    EngineTorqueResultCodeCheckBox.IsEnabled = false;
                    EngineTorqueCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getAccPedalPosition())
                {
                    if (tmpObj.getAccPedalPosition().getDataType() != null)
                        AccPedalPositionDataTypeComboBox.SelectedIndex = (int)tmpObj.getAccPedalPosition().getDataType();
                    else
                    {
                        AccPedalPositionDataTypeComboBox.IsEnabled = false;
                        AccPedalPositionDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getAccPedalPosition().getResultCode() != null)
                        AccPedalPositionResultCodeComboBox.SelectedIndex = (int)tmpObj.getAccPedalPosition().getResultCode();
                    else
                    {
                        AccPedalPositionResultCodeComboBox.IsEnabled = false;
                        AccPedalPositionResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    AccPedalPositionDataTypeComboBox.IsEnabled = false;
                    AccPedalPositionResultCodeComboBox.IsEnabled = false;
                    AccPedalPositionDataTypeCheckBox.IsEnabled = false;
                    AccPedalPositionResultCodeCheckBox.IsEnabled = false;
                    AccPedalPositionCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getSteeringWheelAngle())
                {
                    if (tmpObj.getSteeringWheelAngle().getDataType() != null)
                        SteeringWheelAngleDataTypeComboBox.SelectedIndex = (int)tmpObj.getSteeringWheelAngle().getDataType();
                    else
                    {
                        SteeringWheelAngleDataTypeComboBox.IsEnabled = false;
                        SteeringWheelAngleDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getSteeringWheelAngle().getResultCode() != null)
                        SteeringWheelAngleResultCodeComboBox.SelectedIndex = (int)tmpObj.getSteeringWheelAngle().getResultCode();
                    else
                    {
                        SteeringWheelAngleResultCodeComboBox.IsEnabled = false;
                        SteeringWheelAngleResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    SteeringWheelAngleDataTypeComboBox.IsEnabled = false;
                    SteeringWheelAngleResultCodeComboBox.IsEnabled = false;
                    SteeringWheelAngleDataTypeCheckBox.IsEnabled = false;
                    SteeringWheelAngleResultCodeCheckBox.IsEnabled = false;
                    SteeringWheelAngleCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getECallInfo())
                {
                    if (tmpObj.getECallInfo().getDataType() != null)
                        ECallInfoDataTypeComboBox.SelectedIndex = (int)tmpObj.getECallInfo().getDataType();
                    else
                    {
                        ECallInfoDataTypeComboBox.IsEnabled = false;
                        ECallInfoDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getECallInfo().getResultCode() != null)
                        ECallInfoResultCodeComboBox.SelectedIndex = (int)tmpObj.getECallInfo().getResultCode();
                    else
                    {
                        ECallInfoResultCodeComboBox.IsEnabled = false;
                        ECallInfoResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    ECallInfoDataTypeComboBox.IsEnabled = false;
                    ECallInfoResultCodeComboBox.IsEnabled = false;
                    ECallInfoDataTypeCheckBox.IsEnabled = false;
                    ECallInfoResultCodeCheckBox.IsEnabled = false;
                    ECallInfoCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getAirbagStatus())
                {
                    if (tmpObj.getAirbagStatus().getDataType() != null)
                        AirbagStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getAirbagStatus().getDataType();
                    else
                    {
                        AirbagStatusDataTypeComboBox.IsEnabled = false;
                        AirbagStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getAirbagStatus().getResultCode() != null)
                        AirbagStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getAirbagStatus().getResultCode();
                    else
                    {
                        AirbagStatusResultCodeComboBox.IsEnabled = false;
                        AirbagStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    AirbagStatusDataTypeComboBox.IsEnabled = false;
                    AirbagStatusResultCodeComboBox.IsEnabled = false;
                    AirbagStatusDataTypeCheckBox.IsEnabled = false;
                    AirbagStatusResultCodeCheckBox.IsEnabled = false;
                    AirbagStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getEmergencyEvent())
                {
                    if (tmpObj.getEmergencyEvent().getDataType() != null)
                        EmergencyEventDataTypeComboBox.SelectedIndex = (int)tmpObj.getEmergencyEvent().getDataType();
                    else
                    {
                        EmergencyEventDataTypeComboBox.IsEnabled = false;
                        EmergencyEventDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getEmergencyEvent().getResultCode() != null)
                        EmergencyEventResultCodeComboBox.SelectedIndex = (int)tmpObj.getEmergencyEvent().getResultCode();
                    else
                    {
                        EmergencyEventResultCodeComboBox.IsEnabled = false;
                        EmergencyEventResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    EmergencyEventDataTypeComboBox.IsEnabled = false;
                    EmergencyEventResultCodeComboBox.IsEnabled = false;
                    EmergencyEventDataTypeCheckBox.IsEnabled = false;
                    EmergencyEventResultCodeCheckBox.IsEnabled = false;
                    EmergencyEventCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getClusterModes())
                {
                    if (tmpObj.getClusterModes().getDataType() != null)
                        ClusterModesDataTypeComboBox.SelectedIndex = (int)tmpObj.getClusterModes().getDataType();
                    else
                    {
                        ClusterModesDataTypeComboBox.IsEnabled = false;
                        ClusterModesDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getClusterModes().getResultCode() != null)
                        ClusterModesResultCodeComboBox.SelectedIndex = (int)tmpObj.getClusterModes().getResultCode();
                    else
                    {
                        ClusterModesResultCodeComboBox.IsEnabled = false;
                        ClusterModesResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    ClusterModesDataTypeComboBox.IsEnabled = false;
                    ClusterModesResultCodeComboBox.IsEnabled = false;
                    ClusterModesDataTypeCheckBox.IsEnabled = false;
                    ClusterModesResultCodeCheckBox.IsEnabled = false;
                    ClusterModesCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getMyKey())
                {
                    if (tmpObj.getMyKey().getDataType() != null)
                        MyKeyDataTypeComboBox.SelectedIndex = (int)tmpObj.getMyKey().getDataType();
                    else
                    {
                        MyKeyDataTypeComboBox.IsEnabled = false;
                        MyKeyDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getMyKey().getResultCode() != null)
                        MyKeyResultCodeComboBox.SelectedIndex = (int)tmpObj.getMyKey().getResultCode();
                    else
                    {
                        MyKeyResultCodeComboBox.IsEnabled = false;
                        MyKeyResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    MyKeyDataTypeComboBox.IsEnabled = false;
                    MyKeyResultCodeComboBox.IsEnabled = false;
                    MyKeyDataTypeCheckBox.IsEnabled = false;
                    MyKeyResultCodeCheckBox.IsEnabled = false;
                    MyKeyCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getElectronicParkBrakeStatus())
                {
                    if (tmpObj.getElectronicParkBrakeStatus().getDataType() != null)
                        ElectronicParkBrakeStatusDataTypeComboBox.SelectedIndex = (int)tmpObj.getElectronicParkBrakeStatus().getDataType();
                    else
                    {
                        ElectronicParkBrakeStatusDataTypeComboBox.IsEnabled = false;
                        ElectronicParkBrakeStatusDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getElectronicParkBrakeStatus().getResultCode() != null)
                        ElectronicParkBrakeStatusResultCodeComboBox.SelectedIndex = (int)tmpObj.getElectronicParkBrakeStatus().getResultCode();
                    else
                    {
                        ElectronicParkBrakeStatusResultCodeComboBox.IsEnabled = false;
                        ElectronicParkBrakeStatusResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    ElectronicParkBrakeStatusDataTypeComboBox.IsEnabled = false;
                    ElectronicParkBrakeStatusResultCodeComboBox.IsEnabled = false;
                    ElectronicParkBrakeStatusDataTypeCheckBox.IsEnabled = false;
                    ElectronicParkBrakeStatusResultCodeCheckBox.IsEnabled = false;
                    ElectronicParkBrakeStatusCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getEngineOilLife())
                {
                    if (tmpObj.getEngineOilLife().getDataType() != null)
                        EngineOilLifeDataTypeComboBox.SelectedIndex = (int)tmpObj.getEngineOilLife().getDataType();
                    else
                    {
                        EngineOilLifeDataTypeComboBox.IsEnabled = false;
                        EngineOilLifeDataTypeCheckBox.IsChecked = false;
                    }

                    if (tmpObj.getEngineOilLife().getResultCode() != null)
                        EngineOilLifeResultCodeComboBox.SelectedIndex = (int)tmpObj.getEngineOilLife().getResultCode();
                    else
                    {
                        EngineOilLifeResultCodeComboBox.IsEnabled = false;
                        EngineOilLifeResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    EngineOilLifeDataTypeComboBox.IsEnabled = false;
                    EngineOilLifeResultCodeComboBox.IsEnabled = false;
                    EngineOilLifeDataTypeCheckBox.IsEnabled = false;
                    EngineOilLifeResultCodeCheckBox.IsEnabled = false;
                    EngineOilLifeCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getFuelRange())
                {
                    if (tmpObj.getFuelRange().getDataType() != null)
                        FuelRangeDataTypeComboBox.SelectedIndex = (int)tmpObj.getFuelRange().getDataType();
                    else
                    {
                        FuelRangeDataTypeComboBox.IsEnabled = false;
                        FuelRangeDataTypeCheckBox.IsChecked = false;
                    }
                    if (tmpObj.getFuelRange().getResultCode() != null)
                        FuelRangeResultCodeComboBox.SelectedIndex = (int)tmpObj.getFuelRange().getResultCode();
                    else
                    {
                        FuelRangeResultCodeComboBox.IsEnabled = false;
                        FuelRangeResultCodeCheckBox.IsChecked = false;
                    }
                }
                else
                {
                    FuelRangeDataTypeComboBox.IsEnabled = false;
                    FuelRangeResultCodeComboBox.IsEnabled = false;
                    FuelRangeDataTypeCheckBox.IsEnabled = false;
                    FuelRangeResultCodeCheckBox.IsEnabled = false;
                    FuelRangeCheckBox.IsChecked = false;
                }
            }

            await viSubVehicleCD.ShowAsync();
        }

        private void ViSubVehicleCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult gps = null;
            if (GpsCheckBox.IsChecked == true)
            {
                gps = new VehicleDataResult();
                if (GpsDataTypeCheckBox.IsChecked == true)
                    gps.dataType = (VehicleDataType)GpsDataTypeComboBox.SelectedIndex;
                if (GpsResultCodeCheckBox.IsChecked == true)
                    gps.resultCode = (VehicleDataResultCode)GpsResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult speed = null;
            if (SpeedCheckBox.IsChecked == true)
            {
                speed = new VehicleDataResult();
                if (SpeedDataTypeCheckBox.IsChecked == true)
                    speed.dataType = (VehicleDataType)SpeedDataTypeComboBox.SelectedIndex;
                if (SpeedResultCodeCheckBox.IsChecked == true)
                    speed.resultCode = (VehicleDataResultCode)SpeedResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult rpm = null;
            if (RpmCheckBox.IsChecked == true)
            {
                rpm = new VehicleDataResult();
                if (RpmDataTypeCheckBox.IsChecked == true)
                    rpm.dataType = (VehicleDataType)RpmDataTypeComboBox.SelectedIndex;
                if (RpmResultCodeCheckBox.IsChecked == true)
                    rpm.resultCode = (VehicleDataResultCode)RpmResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult fuelLevel = null;
            if (FuelLevelCheckBox.IsChecked == true)
            {
                fuelLevel = new VehicleDataResult();
                if (FuelLevelDataTypeCheckBox.IsChecked == true)
                    fuelLevel.dataType = (VehicleDataType)FuelLevelDataTypeComboBox.SelectedIndex;
                if (FuelLevelResultCodeCheckBox.IsChecked == true)
                    fuelLevel.resultCode = (VehicleDataResultCode)FuelLevelResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult fuelLevelState = null;
            if (FuelLevelStateCheckBox.IsChecked == true)
            {
                fuelLevelState = new VehicleDataResult();
                if (FuelLevelStateDataTypeCheckBox.IsChecked == true)
                    fuelLevelState.dataType = (VehicleDataType)FuelLevelStateDataTypeComboBox.SelectedIndex;
                if (FuelLevelStateResultCodeCheckBox.IsChecked == true)
                    fuelLevelState.resultCode = (VehicleDataResultCode)FuelLevelStateResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult instantFuelConsumption = null;
            if (InstantFuelConsumptionCheckBox.IsChecked == true)
            {
                instantFuelConsumption = new VehicleDataResult();
                if (InstantFuelConsumptionDataTypeCheckBox.IsChecked == true)
                    instantFuelConsumption.dataType = (VehicleDataType)InstantFuelConsumptionDataTypeComboBox.SelectedIndex;
                if (InstantFuelConsumptionResultCodeCheckBox.IsChecked == true)
                    instantFuelConsumption.resultCode = (VehicleDataResultCode)InstantFuelConsumptionResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult externalTemp = null;
            if (ExternalTemperatureCheckBox.IsChecked == true)
            {
                externalTemp = new VehicleDataResult();
                if (ExternalTemperatureDataTypeCheckBox.IsChecked == true)
                    externalTemp.dataType = (VehicleDataType)ExternalTemperatureDataTypeComboBox.SelectedIndex;
                if (ExternalTemperatureResultCodeCheckBox.IsChecked == true)
                    externalTemp.resultCode = (VehicleDataResultCode)ExternalTemperatureResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult prndl = null;
            if (PRNDLCheckBox.IsChecked == true)
            {
                prndl = new VehicleDataResult();
                if (PRNDLDataTypeCheckBox.IsChecked == true)
                    prndl.dataType = (VehicleDataType)PRNDLDataTypeComboBox.SelectedIndex;
                if (PRNDLResultCodeCheckBox.IsChecked == true)
                    prndl.resultCode = (VehicleDataResultCode)PRNDLResultCodeComboBox.SelectedIndex;
            }

            VehicleDataResult turnSignal = null;
            if (TurnSignalCheckBox.IsChecked == true)
            {
                turnSignal = new VehicleDataResult();
                if (TurnSignalDataTypeCheckBox.IsChecked == true)
                    turnSignal.dataType = (VehicleDataType)TurnSignalDataTypeComboBox.SelectedIndex;
                if (TurnSignalResultCodeCheckBox.IsChecked == true)
                    turnSignal.resultCode = (VehicleDataResultCode)TurnSignalResultCodeComboBox.SelectedIndex;
            }

            VehicleDataResult tirePressure = null;
            if (TirePressureCheckBox.IsChecked == true)
            {
                tirePressure = new VehicleDataResult();
                if (TirePressureDataTypeCheckBox.IsChecked == true)
                    tirePressure.dataType = (VehicleDataType)TirePressureDataTypeComboBox.SelectedIndex;
                if (TirePressureResultCodeCheckBox.IsChecked == true)
                    tirePressure.resultCode = (VehicleDataResultCode)TirePressureResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult odometer = null;
            if (OdometerCheckBox.IsChecked == true)
            {
                odometer = new VehicleDataResult();
                if (OdometerDataTypeCheckBox.IsChecked == true)
                    odometer.dataType = (VehicleDataType)OdometerDataTypeComboBox.SelectedIndex;
                if (OdometerResultCodeCheckBox.IsChecked == true)
                    odometer.resultCode = (VehicleDataResultCode)OdometerResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult beltStatus = null;
            if (BeltStatusCheckBox.IsChecked == true)
            {
                beltStatus = new VehicleDataResult();
                if (BeltStatusDataTypeCheckBox.IsChecked == true)
                    beltStatus.dataType = (VehicleDataType)BeltStatusDataTypeComboBox.SelectedIndex;
                if (BeltStatusResultCodeCheckBox.IsChecked == true)
                    beltStatus.resultCode = (VehicleDataResultCode)BeltStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult bodyInformation = null;
            if (BodyInformationCheckBox.IsChecked == true)
            {
                bodyInformation = new VehicleDataResult();
                if (BodyInformationDataTypeCheckBox.IsChecked == true)
                    bodyInformation.dataType = (VehicleDataType)BodyInformationDataTypeComboBox.SelectedIndex;
                if (BodyInformationResultCodeCheckBox.IsChecked == true)
                    bodyInformation.resultCode = (VehicleDataResultCode)BodyInformationResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult deviceStatus = null;
            if (DeviceStatusCheckBox.IsChecked == true)
            {
                deviceStatus = new VehicleDataResult();
                if (DeviceStatusDataTypeCheckBox.IsChecked == true)
                    deviceStatus.dataType = (VehicleDataType)DeviceStatusDataTypeComboBox.SelectedIndex;
                if (DeviceStatusResultCodeCheckBox.IsChecked == true)
                    deviceStatus.resultCode = (VehicleDataResultCode)DeviceStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult driverBraking = null;
            if (DriverBrakingCheckBox.IsChecked == true)
            {
                driverBraking = new VehicleDataResult();
                if (DriverBrakingDataTypeCheckBox.IsChecked == true)
                    driverBraking.dataType = (VehicleDataType)DriverBrakingDataTypeComboBox.SelectedIndex;
                if (DriverBrakingResultCodeCheckBox.IsChecked == true)
                    driverBraking.resultCode = (VehicleDataResultCode)DriverBrakingResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult wiperStatus = null;
            if (WiperStatusCheckBox.IsChecked == true)
            {
                wiperStatus = new VehicleDataResult();
                if (WiperStatusDataTypeCheckBox.IsChecked == true)
                    wiperStatus.dataType = (VehicleDataType)WiperStatusDataTypeComboBox.SelectedIndex;
                if (WiperStatusResultCodeCheckBox.IsChecked == true)
                    wiperStatus.resultCode = (VehicleDataResultCode)WiperStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult headLampStatus = null;
            if (HeadLampStatusCheckBox.IsChecked == true)
            {
                headLampStatus = new VehicleDataResult();
                if (HeadLampStatusDataTypeCheckBox.IsChecked == true)
                    headLampStatus.dataType = (VehicleDataType)HeadLampStatusDataTypeComboBox.SelectedIndex;
                if (HeadLampStatusResultCodeCheckBox.IsChecked == true)
                    headLampStatus.resultCode = (VehicleDataResultCode)HeadLampStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult engineTorque = null;
            if (EngineTorqueCheckBox.IsChecked == true)
            {
                engineTorque = new VehicleDataResult();
                if (EngineTorqueDataTypeCheckBox.IsChecked == true)
                    engineTorque.dataType = (VehicleDataType)EngineTorqueDataTypeComboBox.SelectedIndex;
                if (EngineTorqueResultCodeCheckBox.IsChecked == true)
                    engineTorque.resultCode = (VehicleDataResultCode)EngineTorqueResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult accPedalPos = null;
            if (AccPedalPositionCheckBox.IsChecked == true)
            {
                accPedalPos = new VehicleDataResult();
                if (AccPedalPositionDataTypeCheckBox.IsChecked == true)
                    accPedalPos.dataType = (VehicleDataType)AccPedalPositionDataTypeComboBox.SelectedIndex;
                if (AccPedalPositionResultCodeCheckBox.IsChecked == true)
                    accPedalPos.resultCode = (VehicleDataResultCode)AccPedalPositionResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult steeringWheelAngle = null;
            if (SteeringWheelAngleCheckBox.IsChecked == true)
            {
                steeringWheelAngle = new VehicleDataResult();
                if (SteeringWheelAngleDataTypeCheckBox.IsChecked == true)
                    steeringWheelAngle.dataType = (VehicleDataType)SteeringWheelAngleDataTypeComboBox.SelectedIndex;
                if (SteeringWheelAngleResultCodeCheckBox.IsChecked == true)
                    steeringWheelAngle.resultCode = (VehicleDataResultCode)SteeringWheelAngleResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult ecallInfo = null;
            if (ECallInfoCheckBox.IsChecked == true)
            {
                ecallInfo = new VehicleDataResult();
                if (ECallInfoDataTypeCheckBox.IsChecked == true)
                    ecallInfo.dataType = (VehicleDataType)ECallInfoDataTypeComboBox.SelectedIndex;
                if (ECallInfoResultCodeCheckBox.IsChecked == true)
                    ecallInfo.resultCode = (VehicleDataResultCode)ECallInfoResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult airbagStatus = null;
            if (AirbagStatusCheckBox.IsChecked == true)
            {
                airbagStatus = new VehicleDataResult();
                if (AirbagStatusDataTypeCheckBox.IsChecked == true)
                    airbagStatus.dataType = (VehicleDataType)AirbagStatusDataTypeComboBox.SelectedIndex;
                if (AirbagStatusResultCodeCheckBox.IsChecked == true)
                    airbagStatus.resultCode = (VehicleDataResultCode)AirbagStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult emergencyEvent = null;
            if (EmergencyEventCheckBox.IsChecked == true)
            {
                emergencyEvent = new VehicleDataResult();
                if (EmergencyEventDataTypeCheckBox.IsChecked == true)
                    emergencyEvent.dataType = (VehicleDataType)EmergencyEventDataTypeComboBox.SelectedIndex;
                if (EmergencyEventResultCodeCheckBox.IsChecked == true)
                    emergencyEvent.resultCode = (VehicleDataResultCode)EmergencyEventResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult clusterModes = null;
            if (ClusterModesCheckBox.IsChecked == true)
            {
                clusterModes = new VehicleDataResult();
                if (ClusterModesDataTypeCheckBox.IsChecked == true)
                    clusterModes.dataType = (VehicleDataType)ClusterModesDataTypeComboBox.SelectedIndex;
                if (ClusterModesResultCodeCheckBox.IsChecked == true)
                    clusterModes.resultCode = (VehicleDataResultCode)ClusterModesResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult myKey = null;
            if (MyKeyCheckBox.IsChecked == true)
            {
                myKey = new VehicleDataResult();
                if (MyKeyDataTypeCheckBox.IsChecked == true)
                    myKey.dataType = (VehicleDataType)MyKeyDataTypeComboBox.SelectedIndex;
                if (MyKeyResultCodeCheckBox.IsChecked == true)
                    myKey.resultCode = (VehicleDataResultCode)MyKeyResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult electronicParkBrakeStatus = null;
            if (ElectronicParkBrakeStatusCheckBox.IsChecked == true)
            {
                electronicParkBrakeStatus = new VehicleDataResult();
                if (ElectronicParkBrakeStatusDataTypeCheckBox.IsChecked == true)
                    electronicParkBrakeStatus.dataType = (VehicleDataType)ElectronicParkBrakeStatusDataTypeComboBox.SelectedIndex;
                if (ElectronicParkBrakeStatusResultCodeCheckBox.IsChecked == true)
                    electronicParkBrakeStatus.resultCode = (VehicleDataResultCode)ElectronicParkBrakeStatusResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult engineOilLife = null;
            if (EngineOilLifeCheckBox.IsChecked == true)
            {
                engineOilLife = new VehicleDataResult();
                if (EngineOilLifeDataTypeCheckBox.IsChecked == true)
                    engineOilLife.dataType = (VehicleDataType)EngineOilLifeDataTypeComboBox.SelectedIndex;
                if (EngineOilLifeResultCodeCheckBox.IsChecked == true)
                    engineOilLife.resultCode = (VehicleDataResultCode)EngineOilLifeResultCodeComboBox.SelectedIndex;
            }
            VehicleDataResult fuelRange = null;
            if (FuelRangeCheckBox.IsChecked == true)
            {
                fuelRange = new VehicleDataResult();
                if (FuelRangeDataTypeCheckBox.IsChecked == true)
                    fuelRange.dataType = (VehicleDataType)FuelRangeDataTypeComboBox.SelectedIndex;
                if (FuelRangeResultCodeCheckBox.IsChecked == true)
                    fuelRange.resultCode = (VehicleDataResultCode)FuelRangeResultCodeComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVehicleInfoUnsubscribeVehicleDataResponse(
                BuildRpc.getNextId(), resultCode, gps, speed, rpm, fuelLevel, fuelLevelState, instantFuelConsumption, externalTemp, prndl, turnSignal,
                tirePressure, odometer, beltStatus, bodyInformation, deviceStatus, driverBraking, wiperStatus, headLampStatus, engineTorque,
                accPedalPos, steeringWheelAngle, ecallInfo, airbagStatus, emergencyEvent, clusterModes, myKey, electronicParkBrakeStatus,
                engineOilLife, fuelRange);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViSubVehicleCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViSubVehicleCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void GpsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GpsDataTypeCheckBox.IsEnabled = (bool)GpsCheckBox.IsChecked;
            GpsDataTypeComboBox.IsEnabled = (bool)GpsCheckBox.IsChecked && (bool)GpsDataTypeCheckBox.IsChecked;
            GpsResultCodeCheckBox.IsEnabled = (bool)GpsCheckBox.IsChecked;
            GpsResultCodeComboBox.IsEnabled = (bool)GpsCheckBox.IsChecked && (bool)GpsResultCodeCheckBox.IsChecked;
        }

        private void GpsDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GpsDataTypeComboBox.IsEnabled = (bool)GpsDataTypeCheckBox.IsChecked;
        }

        private void GpsResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GpsResultCodeComboBox.IsEnabled = (bool)GpsResultCodeCheckBox.IsChecked;
        }

        private void SpeedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeedDataTypeCheckBox.IsEnabled = (bool)SpeedCheckBox.IsChecked;
            SpeedDataTypeComboBox.IsEnabled = (bool)SpeedCheckBox.IsChecked && (bool)SpeedDataTypeCheckBox.IsChecked;
            SpeedResultCodeCheckBox.IsEnabled = (bool)SpeedCheckBox.IsChecked;
            SpeedResultCodeComboBox.IsEnabled = (bool)SpeedCheckBox.IsChecked && (bool)SpeedResultCodeCheckBox.IsChecked;
        }

        private void SpeedDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeedDataTypeComboBox.IsEnabled = (bool)SpeedDataTypeCheckBox.IsChecked;
        }

        private void SpeedResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeedResultCodeComboBox.IsEnabled = (bool)SpeedResultCodeCheckBox.IsChecked;
        }

        private void RpmCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RpmDataTypeCheckBox.IsEnabled = (bool)RpmCheckBox.IsChecked;
            RpmDataTypeComboBox.IsEnabled = (bool)RpmCheckBox.IsChecked && (bool)RpmDataTypeCheckBox.IsChecked;
            RpmResultCodeCheckBox.IsEnabled = (bool)RpmCheckBox.IsChecked;
            RpmResultCodeComboBox.IsEnabled = (bool)RpmCheckBox.IsChecked && (bool)RpmResultCodeCheckBox.IsChecked;
        }

        private void RpmDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RpmDataTypeComboBox.IsEnabled = (bool)RpmDataTypeCheckBox.IsChecked;
        }

        private void RpmResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RpmResultCodeComboBox.IsEnabled = (bool)RpmResultCodeCheckBox.IsChecked;
        }

        private void FuelLevelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelDataTypeCheckBox.IsEnabled = (bool)FuelLevelCheckBox.IsChecked;
            FuelLevelDataTypeComboBox.IsEnabled = (bool)FuelLevelCheckBox.IsChecked && (bool)FuelLevelDataTypeCheckBox.IsChecked;
            FuelLevelResultCodeCheckBox.IsEnabled = (bool)FuelLevelCheckBox.IsChecked;
            FuelLevelResultCodeComboBox.IsEnabled = (bool)FuelLevelCheckBox.IsChecked && (bool)FuelLevelResultCodeCheckBox.IsChecked;
        }

        private void FuelLevelDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelDataTypeComboBox.IsEnabled = (bool)FuelLevelDataTypeCheckBox.IsChecked;
        }

        private void FuelLevelResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelResultCodeComboBox.IsEnabled = (bool)FuelLevelResultCodeCheckBox.IsChecked;
        }

        private void FuelLevelStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelStateDataTypeCheckBox.IsEnabled = (bool)FuelLevelStateCheckBox.IsChecked;
            FuelLevelStateDataTypeComboBox.IsEnabled = (bool)FuelLevelStateCheckBox.IsChecked && (bool)FuelLevelStateDataTypeCheckBox.IsChecked;
            FuelLevelStateResultCodeCheckBox.IsEnabled = (bool)FuelLevelStateCheckBox.IsChecked;
            FuelLevelStateResultCodeComboBox.IsEnabled = (bool)FuelLevelStateCheckBox.IsChecked && (bool)FuelLevelStateResultCodeCheckBox.IsChecked;
        }

        private void FuelLevelStateDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelStateDataTypeComboBox.IsEnabled = (bool)FuelLevelStateDataTypeCheckBox.IsChecked;
        }

        private void FuelLevelStateResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelStateResultCodeComboBox.IsEnabled = (bool)FuelLevelStateResultCodeCheckBox.IsChecked;
        }

        private void InstantFuelConsumptionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InstantFuelConsumptionDataTypeCheckBox.IsEnabled = (bool)InstantFuelConsumptionCheckBox.IsChecked;
            InstantFuelConsumptionDataTypeComboBox.IsEnabled = (bool)InstantFuelConsumptionCheckBox.IsChecked && (bool)InstantFuelConsumptionDataTypeCheckBox.IsChecked;
            InstantFuelConsumptionResultCodeCheckBox.IsEnabled = (bool)InstantFuelConsumptionCheckBox.IsChecked;
            InstantFuelConsumptionResultCodeComboBox.IsEnabled = (bool)InstantFuelConsumptionCheckBox.IsChecked && (bool)InstantFuelConsumptionResultCodeCheckBox.IsChecked;
        }

        private void InstantFuelConsumptionDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InstantFuelConsumptionDataTypeComboBox.IsEnabled = (bool)InstantFuelConsumptionDataTypeCheckBox.IsChecked;
        }

        private void InstantFuelConsumptionResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InstantFuelConsumptionResultCodeComboBox.IsEnabled = (bool)InstantFuelConsumptionResultCodeCheckBox.IsChecked;
        }

        private void ExternalTemperatureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ExternalTemperatureDataTypeCheckBox.IsEnabled = (bool)ExternalTemperatureCheckBox.IsChecked;
            ExternalTemperatureDataTypeComboBox.IsEnabled = (bool)ExternalTemperatureCheckBox.IsChecked && (bool)ExternalTemperatureDataTypeCheckBox.IsChecked;
            ExternalTemperatureResultCodeCheckBox.IsEnabled = (bool)ExternalTemperatureCheckBox.IsChecked;
            ExternalTemperatureResultCodeComboBox.IsEnabled = (bool)ExternalTemperatureCheckBox.IsChecked && (bool)ExternalTemperatureResultCodeCheckBox.IsChecked;
        }

        private void ExternalTemperatureDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ExternalTemperatureDataTypeComboBox.IsEnabled = (bool)ExternalTemperatureDataTypeCheckBox.IsChecked;
        }

        private void ExternalTemperatureResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ExternalTemperatureResultCodeComboBox.IsEnabled = (bool)ExternalTemperatureResultCodeCheckBox.IsChecked;
        }

        private void PRNDLCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PRNDLDataTypeCheckBox.IsEnabled = (bool)PRNDLCheckBox.IsChecked;
            PRNDLDataTypeComboBox.IsEnabled = (bool)PRNDLCheckBox.IsChecked && (bool)PRNDLDataTypeCheckBox.IsChecked;
            PRNDLResultCodeCheckBox.IsEnabled = (bool)PRNDLCheckBox.IsChecked;
            PRNDLResultCodeComboBox.IsEnabled = (bool)PRNDLCheckBox.IsChecked && (bool)PRNDLResultCodeCheckBox.IsChecked;
        }

        private void PRNDLDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PRNDLDataTypeComboBox.IsEnabled = (bool)PRNDLDataTypeCheckBox.IsChecked;
        }

        private void PRNDLResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PRNDLResultCodeComboBox.IsEnabled = (bool)PRNDLResultCodeCheckBox.IsChecked;
        }

        private void TurnSignalCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TurnSignalDataTypeCheckBox.IsEnabled = (bool)TurnSignalCheckBox.IsChecked;
            TurnSignalDataTypeComboBox.IsEnabled = (bool)TurnSignalCheckBox.IsChecked && (bool)TurnSignalDataTypeCheckBox.IsChecked;
            TurnSignalResultCodeCheckBox.IsEnabled = (bool)TurnSignalCheckBox.IsChecked;
            TurnSignalResultCodeComboBox.IsEnabled = (bool)TurnSignalCheckBox.IsChecked && (bool)TurnSignalResultCodeCheckBox.IsChecked;
        }

        private void TurnSignalDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TurnSignalDataTypeComboBox.IsEnabled = (bool)TurnSignalDataTypeCheckBox.IsChecked;
        }

        private void TurnSignalResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TurnSignalResultCodeComboBox.IsEnabled = (bool)TurnSignalResultCodeCheckBox.IsChecked;
        }

        private void TirePressureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TirePressureDataTypeCheckBox.IsEnabled = (bool)TirePressureCheckBox.IsChecked;
            TirePressureDataTypeComboBox.IsEnabled = (bool)TirePressureCheckBox.IsChecked && (bool)TirePressureDataTypeCheckBox.IsChecked;
            TirePressureResultCodeCheckBox.IsEnabled = (bool)TirePressureCheckBox.IsChecked;
            TirePressureResultCodeComboBox.IsEnabled = (bool)TirePressureCheckBox.IsChecked && (bool)TirePressureResultCodeCheckBox.IsChecked;
        }

        private void TirePressureLDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TirePressureDataTypeComboBox.IsEnabled = (bool)TirePressureDataTypeCheckBox.IsChecked;
        }

        private void TirePressureResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TirePressureResultCodeComboBox.IsEnabled = (bool)TirePressureResultCodeCheckBox.IsChecked;
        }

        private void OdometerCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OdometerDataTypeCheckBox.IsEnabled = (bool)OdometerCheckBox.IsChecked;
            OdometerDataTypeComboBox.IsEnabled = (bool)OdometerCheckBox.IsChecked && (bool)OdometerDataTypeCheckBox.IsChecked;
            OdometerResultCodeCheckBox.IsEnabled = (bool)OdometerCheckBox.IsChecked;
            OdometerResultCodeComboBox.IsEnabled = (bool)OdometerCheckBox.IsChecked && (bool)OdometerResultCodeCheckBox.IsChecked;
        }

        private void OdometerDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OdometerDataTypeComboBox.IsEnabled = (bool)OdometerDataTypeCheckBox.IsChecked;
        }

        private void OdometerResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OdometerResultCodeComboBox.IsEnabled = (bool)OdometerResultCodeCheckBox.IsChecked;
        }

        private void BeltStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BeltStatusDataTypeCheckBox.IsEnabled = (bool)BeltStatusCheckBox.IsChecked;
            BeltStatusDataTypeComboBox.IsEnabled = (bool)BeltStatusCheckBox.IsChecked && (bool)BeltStatusDataTypeCheckBox.IsChecked;
            BeltStatusResultCodeCheckBox.IsEnabled = (bool)BeltStatusCheckBox.IsChecked;
            BeltStatusResultCodeComboBox.IsEnabled = (bool)BeltStatusCheckBox.IsChecked && (bool)BeltStatusResultCodeCheckBox.IsChecked;
        }

        private void BeltStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BeltStatusDataTypeComboBox.IsEnabled = (bool)BeltStatusDataTypeCheckBox.IsChecked;
        }

        private void BeltStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BeltStatusResultCodeComboBox.IsEnabled = (bool)BeltStatusResultCodeCheckBox.IsChecked;
        }

        private void BodyInformationCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BodyInformationDataTypeCheckBox.IsEnabled = (bool)BodyInformationCheckBox.IsChecked;
            BodyInformationDataTypeComboBox.IsEnabled = (bool)BodyInformationCheckBox.IsChecked && (bool)BodyInformationDataTypeCheckBox.IsChecked;
            BodyInformationResultCodeCheckBox.IsEnabled = (bool)BodyInformationCheckBox.IsChecked;
            BodyInformationResultCodeComboBox.IsEnabled = (bool)BodyInformationCheckBox.IsChecked && (bool)BodyInformationResultCodeCheckBox.IsChecked;
        }

        private void BodyInformationDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BodyInformationDataTypeComboBox.IsEnabled = (bool)BodyInformationDataTypeCheckBox.IsChecked;
        }

        private void BodyInformationResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BodyInformationResultCodeComboBox.IsEnabled = (bool)BodyInformationResultCodeCheckBox.IsChecked;
        }

        private void DeviceStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceStatusDataTypeCheckBox.IsEnabled = (bool)DeviceStatusCheckBox.IsChecked;
            DeviceStatusDataTypeComboBox.IsEnabled = (bool)DeviceStatusCheckBox.IsChecked && (bool)DeviceStatusDataTypeCheckBox.IsChecked;
            DeviceStatusResultCodeCheckBox.IsEnabled = (bool)DeviceStatusCheckBox.IsChecked;
            DeviceStatusResultCodeComboBox.IsEnabled = (bool)DeviceStatusCheckBox.IsChecked && (bool)DeviceStatusResultCodeCheckBox.IsChecked;
        }

        private void DeviceStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceStatusDataTypeComboBox.IsEnabled = (bool)DeviceStatusDataTypeCheckBox.IsChecked;
        }

        private void DeviceStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceStatusResultCodeComboBox.IsEnabled = (bool)DeviceStatusResultCodeCheckBox.IsChecked;
        }

        private void DriverBrakingCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBrakingDataTypeCheckBox.IsEnabled = (bool)DriverBrakingCheckBox.IsChecked;
            DriverBrakingDataTypeComboBox.IsEnabled = (bool)DriverBrakingCheckBox.IsChecked && (bool)DriverBrakingDataTypeCheckBox.IsChecked;
            DriverBrakingResultCodeCheckBox.IsEnabled = (bool)DriverBrakingCheckBox.IsChecked;
            DriverBrakingResultCodeComboBox.IsEnabled = (bool)DriverBrakingCheckBox.IsChecked && (bool)DriverBrakingResultCodeCheckBox.IsChecked;
        }

        private void DriverBrakingDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBrakingDataTypeComboBox.IsEnabled = (bool)DriverBrakingDataTypeCheckBox.IsChecked;
        }

        private void DriverBrakingResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBrakingResultCodeComboBox.IsEnabled = (bool)DriverBrakingResultCodeCheckBox.IsChecked;
        }

        private void WiperStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WiperStatusDataTypeCheckBox.IsEnabled = (bool)WiperStatusCheckBox.IsChecked;
            WiperStatusDataTypeComboBox.IsEnabled = (bool)WiperStatusCheckBox.IsChecked && (bool)WiperStatusDataTypeCheckBox.IsChecked;
            WiperStatusResultCodeCheckBox.IsEnabled = (bool)WiperStatusCheckBox.IsChecked;
            WiperStatusResultCodeComboBox.IsEnabled = (bool)WiperStatusCheckBox.IsChecked && (bool)WiperStatusResultCodeCheckBox.IsChecked;
        }

        private void WiperStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WiperStatusDataTypeComboBox.IsEnabled = (bool)WiperStatusDataTypeCheckBox.IsChecked;
        }

        private void WiperStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WiperStatusResultCodeComboBox.IsEnabled = (bool)WiperStatusResultCodeCheckBox.IsChecked;
        }

        private void HeadLampStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadLampStatusDataTypeCheckBox.IsEnabled = (bool)HeadLampStatusCheckBox.IsChecked;
            HeadLampStatusDataTypeComboBox.IsEnabled = (bool)HeadLampStatusCheckBox.IsChecked && (bool)HeadLampStatusDataTypeCheckBox.IsChecked;
            HeadLampStatusResultCodeCheckBox.IsEnabled = (bool)HeadLampStatusCheckBox.IsChecked;
            HeadLampStatusResultCodeComboBox.IsEnabled = (bool)HeadLampStatusCheckBox.IsChecked && (bool)HeadLampStatusResultCodeCheckBox.IsChecked;
        }

        private void HeadLampStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadLampStatusDataTypeComboBox.IsEnabled = (bool)HeadLampStatusDataTypeCheckBox.IsChecked;
        }

        private void HeadLampStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadLampStatusResultCodeComboBox.IsEnabled = (bool)HeadLampStatusResultCodeCheckBox.IsChecked;
        }

        private void EngineTorqueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineTorqueDataTypeCheckBox.IsEnabled = (bool)EngineTorqueCheckBox.IsChecked;
            EngineTorqueDataTypeComboBox.IsEnabled = (bool)EngineTorqueCheckBox.IsChecked && (bool)EngineTorqueDataTypeCheckBox.IsChecked;
            EngineTorqueResultCodeCheckBox.IsEnabled = (bool)EngineTorqueCheckBox.IsChecked;
            EngineTorqueResultCodeComboBox.IsEnabled = (bool)EngineTorqueCheckBox.IsChecked && (bool)EngineTorqueResultCodeCheckBox.IsChecked;
        }

        private void EngineTorqueDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineTorqueDataTypeComboBox.IsEnabled = (bool)EngineTorqueDataTypeCheckBox.IsChecked;
        }

        private void EngineTorqueResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineTorqueResultCodeComboBox.IsEnabled = (bool)EngineTorqueResultCodeCheckBox.IsChecked;
        }

        private void AccPedalPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AccPedalPositionDataTypeCheckBox.IsEnabled = (bool)AccPedalPositionCheckBox.IsChecked;
            AccPedalPositionDataTypeComboBox.IsEnabled = (bool)AccPedalPositionCheckBox.IsChecked && (bool)AccPedalPositionDataTypeCheckBox.IsChecked;
            AccPedalPositionResultCodeCheckBox.IsEnabled = (bool)AccPedalPositionCheckBox.IsChecked;
            AccPedalPositionResultCodeComboBox.IsEnabled = (bool)AccPedalPositionCheckBox.IsChecked && (bool)AccPedalPositionResultCodeCheckBox.IsChecked;
        }

        private void AccPedalPositionDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AccPedalPositionDataTypeComboBox.IsEnabled = (bool)AccPedalPositionDataTypeCheckBox.IsChecked;
        }

        private void AccPedalPositionResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AccPedalPositionResultCodeComboBox.IsEnabled = (bool)AccPedalPositionResultCodeCheckBox.IsChecked;
        }

        private void SteeringWheelAngleCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SteeringWheelAngleDataTypeCheckBox.IsEnabled = (bool)SteeringWheelAngleCheckBox.IsChecked;
            SteeringWheelAngleDataTypeComboBox.IsEnabled = (bool)SteeringWheelAngleCheckBox.IsChecked && (bool)SteeringWheelAngleDataTypeCheckBox.IsChecked;
            SteeringWheelAngleResultCodeCheckBox.IsEnabled = (bool)SteeringWheelAngleCheckBox.IsChecked;
            SteeringWheelAngleResultCodeComboBox.IsEnabled = (bool)SteeringWheelAngleCheckBox.IsChecked && (bool)SteeringWheelAngleResultCodeCheckBox.IsChecked;
        }

        private void SteeringWheelAngleDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SteeringWheelAngleDataTypeComboBox.IsEnabled = (bool)SteeringWheelAngleDataTypeCheckBox.IsChecked;
        }

        private void SteeringWheelAngleResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SteeringWheelAngleResultCodeComboBox.IsEnabled = (bool)SteeringWheelAngleResultCodeCheckBox.IsChecked;
        }

        private void ECallInfoCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallInfoDataTypeCheckBox.IsEnabled = (bool)ECallInfoCheckBox.IsChecked;
            ECallInfoDataTypeComboBox.IsEnabled = (bool)ECallInfoCheckBox.IsChecked && (bool)ECallInfoDataTypeCheckBox.IsChecked;
            ECallInfoResultCodeCheckBox.IsEnabled = (bool)ECallInfoCheckBox.IsChecked;
            ECallInfoResultCodeComboBox.IsEnabled = (bool)ECallInfoCheckBox.IsChecked && (bool)ECallInfoResultCodeCheckBox.IsChecked;
        }

        private void ECallInfoDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallInfoDataTypeComboBox.IsEnabled = (bool)ECallInfoDataTypeCheckBox.IsChecked;
        }

        private void ECallInfoResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallInfoResultCodeComboBox.IsEnabled = (bool)ECallInfoResultCodeCheckBox.IsChecked;
        }

        private void AirbagStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AirbagStatusDataTypeCheckBox.IsEnabled = (bool)AirbagStatusCheckBox.IsChecked;
            AirbagStatusDataTypeComboBox.IsEnabled = (bool)AirbagStatusCheckBox.IsChecked && (bool)AirbagStatusDataTypeCheckBox.IsChecked;
            AirbagStatusResultCodeCheckBox.IsEnabled = (bool)AirbagStatusCheckBox.IsChecked;
            AirbagStatusResultCodeComboBox.IsEnabled = (bool)AirbagStatusCheckBox.IsChecked && (bool)AirbagStatusResultCodeCheckBox.IsChecked;
        }

        private void AirbagStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AirbagStatusDataTypeComboBox.IsEnabled = (bool)AirbagStatusDataTypeCheckBox.IsChecked;
        }

        private void AirbagStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AirbagStatusResultCodeComboBox.IsEnabled = (bool)AirbagStatusResultCodeCheckBox.IsChecked;
        }

        private void EmergencyEventCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EmergencyEventDataTypeCheckBox.IsEnabled = (bool)EmergencyEventCheckBox.IsChecked;
            EmergencyEventDataTypeComboBox.IsEnabled = (bool)EmergencyEventCheckBox.IsChecked && (bool)EmergencyEventDataTypeCheckBox.IsChecked;
            EmergencyEventResultCodeCheckBox.IsEnabled = (bool)EmergencyEventCheckBox.IsChecked;
            EmergencyEventResultCodeComboBox.IsEnabled = (bool)EmergencyEventCheckBox.IsChecked && (bool)EmergencyEventResultCodeCheckBox.IsChecked;
        }

        private void EmergencyEventDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EmergencyEventDataTypeComboBox.IsEnabled = (bool)EmergencyEventDataTypeCheckBox.IsChecked;
        }

        private void EmergencyEventResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EmergencyEventResultCodeComboBox.IsEnabled = (bool)EmergencyEventResultCodeCheckBox.IsChecked;
        }

        private void ClusterModesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ClusterModesDataTypeCheckBox.IsEnabled = (bool)ClusterModesCheckBox.IsChecked;
            ClusterModesDataTypeComboBox.IsEnabled = (bool)ClusterModesCheckBox.IsChecked && (bool)ClusterModesDataTypeCheckBox.IsChecked;
            ClusterModesResultCodeCheckBox.IsEnabled = (bool)ClusterModesCheckBox.IsChecked;
            ClusterModesResultCodeComboBox.IsEnabled = (bool)ClusterModesCheckBox.IsChecked && (bool)ClusterModesResultCodeCheckBox.IsChecked;
        }

        private void ClusterModesDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ClusterModesDataTypeComboBox.IsEnabled = (bool)ClusterModesDataTypeCheckBox.IsChecked;
        }

        private void ClusterModesResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ClusterModesResultCodeComboBox.IsEnabled = (bool)ClusterModesResultCodeCheckBox.IsChecked;
        }

        private void MyKeyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MyKeyDataTypeCheckBox.IsEnabled = (bool)MyKeyCheckBox.IsChecked;
            MyKeyDataTypeComboBox.IsEnabled = (bool)MyKeyCheckBox.IsChecked && (bool)MyKeyDataTypeCheckBox.IsChecked;
            MyKeyResultCodeCheckBox.IsEnabled = (bool)MyKeyCheckBox.IsChecked;
            MyKeyResultCodeComboBox.IsEnabled = (bool)MyKeyCheckBox.IsChecked && (bool)MyKeyResultCodeCheckBox.IsChecked;
        }

        private void MyKeyDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MyKeyDataTypeComboBox.IsEnabled = (bool)MyKeyDataTypeCheckBox.IsChecked;
        }

        private void MyKeyResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MyKeyResultCodeComboBox.IsEnabled = (bool)MyKeyResultCodeCheckBox.IsChecked;
        }

        private void ElectronicParkBrakeStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ElectronicParkBrakeStatusDataTypeCheckBox.IsEnabled = (bool)ElectronicParkBrakeStatusCheckBox.IsChecked;
            ElectronicParkBrakeStatusDataTypeComboBox.IsEnabled = (bool)ElectronicParkBrakeStatusCheckBox.IsChecked && (bool)ElectronicParkBrakeStatusDataTypeCheckBox.IsChecked;
            ElectronicParkBrakeStatusResultCodeCheckBox.IsEnabled = (bool)ElectronicParkBrakeStatusCheckBox.IsChecked;
            ElectronicParkBrakeStatusResultCodeComboBox.IsEnabled = (bool)ElectronicParkBrakeStatusCheckBox.IsChecked && (bool)ElectronicParkBrakeStatusResultCodeCheckBox.IsChecked;
        }

        private void ElectronicParkBrakeStatusDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ElectronicParkBrakeStatusDataTypeComboBox.IsEnabled = (bool)ElectronicParkBrakeStatusDataTypeCheckBox.IsChecked;
        }

        private void ElectronicParkBrakeStatusResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ElectronicParkBrakeStatusResultCodeComboBox.IsEnabled = (bool)ElectronicParkBrakeStatusResultCodeCheckBox.IsChecked;
        }

        private void EngineOilLifeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineOilLifeDataTypeCheckBox.IsEnabled = (bool)EngineOilLifeCheckBox.IsChecked;
            EngineOilLifeDataTypeComboBox.IsEnabled = (bool)EngineOilLifeCheckBox.IsChecked && (bool)EngineOilLifeDataTypeCheckBox.IsChecked;
            EngineOilLifeResultCodeCheckBox.IsEnabled = (bool)EngineOilLifeCheckBox.IsChecked;
            EngineOilLifeResultCodeComboBox.IsEnabled = (bool)EngineOilLifeCheckBox.IsChecked && (bool)EngineOilLifeResultCodeCheckBox.IsChecked;
        }

        private void EngineOilLifeDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineOilLifeDataTypeComboBox.IsEnabled = (bool)EngineOilLifeDataTypeCheckBox.IsChecked;
        }

        private void EngineOilLifeResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineOilLifeResultCodeComboBox.IsEnabled = (bool)EngineOilLifeResultCodeCheckBox.IsChecked;
        }

        private void FuelRangeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelRangeDataTypeCheckBox.IsEnabled = (bool)FuelRangeCheckBox.IsChecked;
            FuelRangeDataTypeComboBox.IsEnabled = (bool)FuelRangeCheckBox.IsChecked && (bool)FuelRangeDataTypeCheckBox.IsChecked;
            FuelRangeResultCodeCheckBox.IsEnabled = (bool)FuelRangeCheckBox.IsChecked;
            FuelRangeResultCodeComboBox.IsEnabled = (bool)FuelRangeCheckBox.IsChecked && (bool)FuelRangeResultCodeCheckBox.IsChecked;
        }

        private void FuelRangeDataTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelRangeDataTypeComboBox.IsEnabled = (bool)FuelRangeDataTypeCheckBox.IsChecked;
        }

        private void FuelRangeResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelRangeResultCodeComboBox.IsEnabled = (bool)FuelRangeResultCodeCheckBox.IsChecked;
        }
    }
}
