﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class AddFuelRange : UserControl
    {
        public CheckBox fuelTypeCheckBox;
        public ComboBox fuelTypeComboBox;

        public CheckBox rangeCheckBox;
        public TextBox rangeTextBox;

        public AddFuelRange()
        {
            this.InitializeComponent();

            FuelTypeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.FuelType));
            FuelTypeComboBox.SelectedIndex = 0;

            fuelTypeCheckBox = FuelTypeCheckBox;
            fuelTypeComboBox = FuelTypeComboBox;

            rangeCheckBox = RangeCheckBox;
            rangeTextBox = RangeTextBox;
    }

        private void RangeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RangeTextBox.IsEnabled = (bool)RangeCheckBox.IsChecked;
        }

        private void FueltypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelTypeComboBox.IsEnabled = (bool)FuelTypeCheckBox.IsChecked;
        }
    }
}
