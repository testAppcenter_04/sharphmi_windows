﻿using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class AddDIDResult : UserControl
    {
        public CheckBox resultCodeCB;
        public ComboBox resultCombobox;
        public CheckBox didLocationCB;
        public TextBox didLocationTB;
        public CheckBox dataCB;
        public TextBox dataTB;
        public AddDIDResult()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;

            resultCodeCB = ResultCodeCheckBox;
            resultCombobox = ResultCodeComboBox;
            didLocationCB = DIDLocationCheckBox;
            didLocationTB = DIDLocationTextBox;
            dataCB = DataCheckBox;
            dataTB = DataTextBox;
        }

        private void DataCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DataTextBox.IsEnabled = (bool)DataCheckBox.IsChecked;
        }

        private void DIDResultCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DIDLocationTextBox.IsEnabled = (bool)DIDLocationCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
