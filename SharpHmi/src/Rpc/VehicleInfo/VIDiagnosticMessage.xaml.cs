﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIDiagnosticMessage : UserControl
    {
        DiagnosticMessage tmpObj = null;
        public VIDiagnosticMessage()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowDiagnosticMessage()
        {
            ContentDialog viDiagnostocMessageCD = new ContentDialog();
            viDiagnostocMessageCD.Content = this;

            viDiagnostocMessageCD.PrimaryButtonText = Const.TxLater;
            viDiagnostocMessageCD.PrimaryButtonClick += ViDiagnostocMessageCD_PrimaryButtonClick;

            viDiagnostocMessageCD.SecondaryButtonText = Const.Reset;
            viDiagnostocMessageCD.SecondaryButtonClick += ViDiagnostocMessageCD_ResetButtonClick;

            viDiagnostocMessageCD.CloseButtonText = Const.Close;
            viDiagnostocMessageCD.CloseButtonClick += ViDiagnostocMessageCD_CloseButtonClick;

            tmpObj = new DiagnosticMessage();
            tmpObj = (DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<DiagnosticMessage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(DiagnosticMessage);
                tmpObj = (DiagnosticMessage)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getMessageDataResult() != null)
                {
                    List<int> messageList = tmpObj.getMessageDataResult();
                    int count = messageList.Count;
                    String messageDataString = "";
                    for (int i = 0; i < count; i++)
                    {
                        if (i == count - 1)
                        {
                            messageDataString = messageDataString + messageList[i];
                        }
                        else
                        {
                            messageDataString = messageDataString + messageList[i] + ",";
                        }
                    }
                    MessageDataResultTextBox.Text = messageDataString;
                }
                else
                {
                    MessageDataResultCheckBox.IsChecked = false;
                    MessageDataResultTextBox.IsEnabled = false;


                }

                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }
            await viDiagnostocMessageCD.ShowAsync();

        }

        private void ViDiagnostocMessageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            List<int> messageDataList = null;
            if (MessageDataResultCheckBox.IsChecked == true)
            {
                messageDataList = new List<int>();
                if (MessageDataResultTextBox.Text != null)
                {
                    String[] messageList = MessageDataResultTextBox.Text.ToString().Split(',');
                    if (messageList != null && messageList.Length > 0)
                    {
                        foreach (String s in messageList)
                        {
                            try
                            {
                                messageDataList.Add(Int32.Parse(s));
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
            }

            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            RpcResponse rpcResponse = BuildRpc.buildVehicleInfoDiagnosticMessageResponse(
                BuildRpc.getNextId(), resultCode, messageDataList);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViDiagnostocMessageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViDiagnostocMessageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void MessageDataResultCBCheckedChanged(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            MessageDataResultTextBox.IsEnabled = (bool)MessageDataResultCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}