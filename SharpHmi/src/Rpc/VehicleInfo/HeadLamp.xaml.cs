﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class HeadLamp : UserControl
    {
        public CheckBox lowBeamOnCheckBox;
        public CheckBox highBeamONCheckBox;

        public ToggleSwitch lowBeamOnToggle;
        public ToggleSwitch highBeamONToggle;

        public CheckBox ambientLightSensorStatusCheckBox;
        public ComboBox ambientLightSensorStatusComboBox;

        public HeadLamp(HmiApiLib.Common.Structs.HeadLampStatus headLampStatus)
        {
            InitializeComponent();

            AmbientLightSensorStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.AmbientLightStatus));
            AmbientLightSensorStatusComboBox.SelectedIndex = 0;
            lowBeamOnToggle = LowBeamOnToggle;
            highBeamONToggle = HighBeamONToggle;
            lowBeamOnCheckBox = LowBeamOnCheckBox;
            highBeamONCheckBox = HighBeamONCheckBox;

            ambientLightSensorStatusCheckBox = AmbientLightSensorStatusCheckBox;
            ambientLightSensorStatusComboBox = AmbientLightSensorStatusComboBox;

            if (null != headLampStatus)
            {
                if (headLampStatus.getLowBeamsOn() != null)
                {
                    
                    LowBeamOnToggle.IsEnabled = true;
                    LowBeamOnCheckBox.IsChecked = true;

                    LowBeamOnToggle.IsOn =(bool) headLampStatus.getLowBeamsOn();

                }

                if (headLampStatus.getHighBeamsOn() != null)
                {
                
                    HighBeamONToggle.IsEnabled = true;

                    HighBeamONCheckBox.IsChecked = true;

                    HighBeamONToggle.IsOn =(bool) headLampStatus.getHighBeamsOn();


                }

                if (headLampStatus.getAmbientLightSensorStatus() != null)
                {
                    AmbientLightSensorStatusComboBox.SelectedIndex = (int)headLampStatus.getAmbientLightSensorStatus();
                    AmbientLightSensorStatusCheckBox.IsChecked = true;

                }
                else
                {
                    AmbientLightSensorStatusComboBox.IsEnabled = false;

                }
            }
        }

        private void AmbientLightSensorStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AmbientLightSensorStatusComboBox.IsEnabled = (bool)AmbientLightSensorStatusCheckBox.IsChecked;
        }

        private void LowBeamOnCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LowBeamOnToggle.IsEnabled = (bool)LowBeamOnCheckBox.IsChecked;

        }

        private void HighBeamONCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HighBeamONToggle.IsEnabled = (bool)HighBeamONCheckBox.IsChecked;

        }
    }
}