﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class BeltStatus : UserControl
    {
        public CheckBox driverBeltDeployedCheckBox;
        public CheckBox passengerBeltDeployedCheckBox;
        public CheckBox passengerBuckleBeltedCheckBox;
        public CheckBox driverBuckleBeltedCheckBox;
        public CheckBox leftRow2BuckleBeltedCheckBox;
        public CheckBox passengerChildDetectedCheckBox;
        public CheckBox rightRow2BuckleBeltedCheckBox;
        public CheckBox middleRow2BuckleBeltedCheckBox;
        public CheckBox middleRow3BuckleBeltedCheckBox;
        public CheckBox leftRow3BuckleBeltedCheckBox;
        public CheckBox rightRow3BuckleBeltedCheckBox;
        public CheckBox leftRearInflatableBeltedCheckBox;
        public CheckBox rightRearInflatableBeltedCheckBox;
        public CheckBox middleRow1BeltDeployedCheckBox;
        public CheckBox middleRow1BuckleBeltedCheckBox;

        public ComboBox driverBeltDeployedComboBox;
        public ComboBox passengerBeltDeployedComboBox;
        public ComboBox passengerBuckleBeltedComboBox;
        public ComboBox driverBuckleBeltedComboBox;
        public ComboBox leftRow2BuckleBeltedComboBox;
        public ComboBox passengerChildDetectedComboBox;
        public ComboBox rightRow2BuckleBeltedComboBox;
        public ComboBox middleRow2BuckleBeltedComboBox;
        public ComboBox middleRow3BuckleBeltedComboBox;
        public ComboBox leftRow3BuckleBeltedComboBox;
        public ComboBox rightRow3BuckleBeltedComboBox;
        public ComboBox leftRearInflatableBeltedComboBox;
        public ComboBox rightRearInflatableBeltedComboBox;
        public ComboBox middleRow1BeltDeployedComboBox;
        public ComboBox middleRow1BuckleBeltedComboBox;


        public BeltStatus(HmiApiLib.Common.Structs.BeltStatus beltStatus)
        {
            this.InitializeComponent();

            DriverBeltDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverBeltDeployedComboBox.SelectedIndex = 0;

            PassengerBeltDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerBeltDeployedComboBox.SelectedIndex = 0;

            PassengerBuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerBuckleBeltedComboBox.SelectedIndex = 0;

            DriverBuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverBuckleBeltedComboBox.SelectedIndex = 0;

            LeftRow2BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            LeftRow2BuckleBeltedComboBox.SelectedIndex = 0;

            PassengerChildDetectedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            PassengerChildDetectedComboBox.SelectedIndex = 0;

            RightRow2BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            RightRow2BuckleBeltedComboBox.SelectedIndex = 0;

            MiddleRow2BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MiddleRow2BuckleBeltedComboBox.SelectedIndex = 0;

            MiddleRow3BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MiddleRow3BuckleBeltedComboBox.SelectedIndex = 0;

            LeftRow3BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            LeftRow3BuckleBeltedComboBox.SelectedIndex = 0;

            RightRow3BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            RightRow3BuckleBeltedComboBox.SelectedIndex = 0;

            LeftRearInflatableBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            LeftRearInflatableBeltedComboBox.SelectedIndex = 0;

            RightRearInflatableBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            RightRearInflatableBeltedComboBox.SelectedIndex = 0;

            MiddleRow1BeltDeployedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MiddleRow1BeltDeployedComboBox.SelectedIndex = 0;

            MiddleRow1BuckleBeltedComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MiddleRow1BuckleBeltedComboBox.SelectedIndex = 0;

            driverBeltDeployedCheckBox = DriverBeltDepoyedCheckBox;
            passengerBeltDeployedCheckBox = PassengerBeltDeployedCheckBox;
            passengerBuckleBeltedCheckBox = PassengerBuckleBeltedCheckBox;
            driverBuckleBeltedCheckBox = DriverBuckleBeltedCheckBox;
            leftRow2BuckleBeltedCheckBox = LeftRow2BuckleBeltedCheckBox;
            passengerChildDetectedCheckBox = PassengerChildDetectedCheckBox;
            rightRow2BuckleBeltedCheckBox = RightRow2BuckleBeltedCheckBox;
            middleRow2BuckleBeltedCheckBox = MiddleRow2BuckleBeltedCheckBox;
            middleRow3BuckleBeltedCheckBox = MiddleRow3BuckleBeltedCheckBox;
            leftRow3BuckleBeltedCheckBox = LeftRow3BuckleBeltedCheckBox;
            rightRow3BuckleBeltedCheckBox = RightRow3BuckleBeltedCheckBox;
            leftRearInflatableBeltedCheckBox = LeftRearInflatableBeltedCheckBox;
            rightRearInflatableBeltedCheckBox = RightRearInflatableBeltedCheckBox;
            middleRow1BeltDeployedCheckBox = MiddleRow1BeltDeployedCheckBox;
            middleRow1BuckleBeltedCheckBox = MiddleRow1BuckleBeltedCheckBox;

            driverBeltDeployedComboBox = DriverBeltDeployedComboBox;
            passengerBeltDeployedComboBox = PassengerBeltDeployedComboBox;
            passengerBuckleBeltedComboBox = PassengerBuckleBeltedComboBox;
            driverBuckleBeltedComboBox = DriverBuckleBeltedComboBox;
            leftRow2BuckleBeltedComboBox = LeftRow2BuckleBeltedComboBox;
            passengerChildDetectedComboBox = PassengerChildDetectedComboBox;
            rightRow2BuckleBeltedComboBox = RightRow2BuckleBeltedComboBox;
            middleRow2BuckleBeltedComboBox = MiddleRow2BuckleBeltedComboBox;
            middleRow3BuckleBeltedComboBox = MiddleRow3BuckleBeltedComboBox;
            leftRow3BuckleBeltedComboBox = LeftRow3BuckleBeltedComboBox;
            rightRow3BuckleBeltedComboBox = RightRow3BuckleBeltedComboBox;
            leftRearInflatableBeltedComboBox = LeftRearInflatableBeltedComboBox;
            rightRearInflatableBeltedComboBox = RightRearInflatableBeltedComboBox;
            middleRow1BeltDeployedComboBox = MiddleRow1BeltDeployedComboBox;
            middleRow1BuckleBeltedComboBox = MiddleRow1BuckleBeltedComboBox;

            if (null != beltStatus)
            {
                if (beltStatus.getDriverBeltDeployed() != null)
                {
                    DriverBeltDeployedComboBox.SelectedIndex = (int)beltStatus.getDriverBeltDeployed();
                    DriverBeltDepoyedCheckBox.IsChecked = true;

                }
                else
                {
                    DriverBeltDeployedComboBox.IsEnabled = false;
                }

                if (beltStatus.getPassengerBeltDeployed() != null)
                {
                    PassengerBeltDeployedComboBox.SelectedIndex = (int)beltStatus.getPassengerBeltDeployed();
                    PassengerBeltDeployedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerBeltDeployedComboBox.IsEnabled = false;



                }
                if (beltStatus.getPassengerBuckleBelted() != null)
                {
                    PassengerBuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getPassengerBuckleBelted();
                    PassengerBuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerBuckleBeltedComboBox.IsEnabled = false;



                }
                if (beltStatus.getDriverBuckleBelted() != null)
                {
                    DriverBuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getDriverBuckleBelted();
                    DriverBuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    DriverBuckleBeltedComboBox.IsEnabled = false;



                }
                if (beltStatus.getLeftRow2BuckleBelted() != null)
                {
                    LeftRow2BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getLeftRow2BuckleBelted();
                    LeftRow2BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {
                    LeftRow2BuckleBeltedComboBox.IsEnabled = false;

                }
                if (beltStatus.getPassengerChildDetected() != null)
                {
                    PassengerChildDetectedComboBox.SelectedIndex = (int)beltStatus.getPassengerChildDetected();
                    PassengerChildDetectedCheckBox.IsChecked = true;

                }
                else
                {
                    PassengerChildDetectedComboBox.IsEnabled = false;

                }
                if (beltStatus.getRightRow2BuckleBelted() != null)
                {
                    RightRow2BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getRightRow2BuckleBelted();
                    RightRow2BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {
                    RightRow2BuckleBeltedComboBox.IsEnabled = false;

                }
                if (beltStatus.getMiddleRow2BuckleBelted() != null)
                {

                    MiddleRow2BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getMiddleRow2BuckleBelted();
                    MiddleRow2BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    MiddleRow2BuckleBeltedComboBox.IsEnabled = false;

                }

                if (beltStatus.getMiddleRow3BuckleBelted() != null)
                {
                    MiddleRow3BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getMiddleRow3BuckleBelted();
                    MiddleRow3BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    MiddleRow3BuckleBeltedComboBox.IsEnabled = false;

                }
                if (beltStatus.getLeftRow3BuckleBelted() != null)
                {
                    LeftRow3BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getLeftRow3BuckleBelted();
                    LeftRow3BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    LeftRow3BuckleBeltedComboBox.IsEnabled = false;

                }
                if (beltStatus.getRightRow3BuckleBelted() != null)
                {
                    RightRow3BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getRightRow3BuckleBelted();
                    RightRow3BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    RightRow3BuckleBeltedComboBox.IsEnabled = false;

                }
                if (beltStatus.getLeftRearInflatableBelted() != null)
                {
                    LeftRearInflatableBeltedComboBox.SelectedIndex = (int)beltStatus.getLeftRearInflatableBelted();
                    LeftRearInflatableBeltedCheckBox.IsChecked = true;

                }
                else
                {
                    LeftRearInflatableBeltedComboBox.IsEnabled = false;



                }
                if (beltStatus.getRightRearInflatableBelted() != null)
                {
                    RightRearInflatableBeltedComboBox.SelectedIndex = (int)beltStatus.getRightRearInflatableBelted();
                    RightRearInflatableBeltedCheckBox.IsChecked = true;

                }
                else
                {
                    RightRearInflatableBeltedComboBox.IsEnabled = false;



                }
                if (beltStatus.getMiddleRow1BeltDeployed() != null)
                {
                    MiddleRow1BeltDeployedComboBox.SelectedIndex = (int)beltStatus.getMiddleRow1BeltDeployed();
                    MiddleRow1BeltDeployedCheckBox.IsChecked = true;

                }
                else
                {

                    MiddleRow1BeltDeployedComboBox.IsEnabled = false;

                }
                if (beltStatus.getMiddleRow1BuckleBelted() != null)
                {
                    MiddleRow1BuckleBeltedComboBox.SelectedIndex = (int)beltStatus.getMiddleRow1BuckleBelted();
                    MiddleRow1BuckleBeltedCheckBox.IsChecked = true;

                }
                else
                {

                    MiddleRow1BuckleBeltedComboBox.IsEnabled = false;

                }
            }
        }

        private void DriverBeltDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBeltDeployedComboBox.IsEnabled = (bool)DriverBeltDepoyedCheckBox.IsChecked;
        }

        private void PassengerBeltDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerBeltDeployedComboBox.IsEnabled = (bool)PassengerBeltDeployedCheckBox.IsChecked;
        }

        private void PassengerBuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerBuckleBeltedComboBox.IsEnabled = (bool)PassengerBuckleBeltedCheckBox.IsChecked;
        }

        private void DriverBuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBuckleBeltedComboBox.IsEnabled = (bool)DriverBuckleBeltedCheckBox.IsChecked;
        }

        private void LeftRow2BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LeftRow2BuckleBeltedComboBox.IsEnabled = (bool)LeftRow2BuckleBeltedCheckBox.IsChecked;
        }

        private void PassengerChildDetectedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerChildDetectedComboBox.IsEnabled = (bool)PassengerChildDetectedCheckBox.IsChecked;
        }

        private void RightRow2BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RightRow2BuckleBeltedComboBox.IsEnabled = (bool)RightRow2BuckleBeltedCheckBox.IsChecked;
        }

        private void MiddleRow2BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MiddleRow2BuckleBeltedComboBox.IsEnabled = (bool)MiddleRow2BuckleBeltedCheckBox.IsChecked;
        }

        private void MiddleRow3BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MiddleRow3BuckleBeltedComboBox.IsEnabled = (bool)MiddleRow3BuckleBeltedCheckBox.IsChecked;
        }

        private void LeftRow3BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LeftRow3BuckleBeltedComboBox.IsEnabled = (bool)LeftRow3BuckleBeltedCheckBox.IsChecked;
        }

        private void RightRow3BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RightRow3BuckleBeltedComboBox.IsEnabled = (bool)RightRow3BuckleBeltedCheckBox.IsChecked;
        }

        private void LeftRearInflatableBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LeftRearInflatableBeltedComboBox.IsEnabled = (bool)LeftRearInflatableBeltedCheckBox.IsChecked;
        }

        private void RightRearInflatableBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RightRearInflatableBeltedComboBox.IsEnabled = (bool)RightRearInflatableBeltedCheckBox.IsChecked;
        }

        private void MiddleRow1BeltDeployedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MiddleRow1BeltDeployedComboBox.IsEnabled = (bool)MiddleRow1BeltDeployedCheckBox.IsChecked;
        }

        private void MiddleRow1BuckleBeltedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MiddleRow1BuckleBeltedComboBox.IsEnabled = (bool)MiddleRow1BuckleBeltedCheckBox.IsChecked;
        }
    }
}
