﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIGetVehicleTypeResponse : UserControl
    {
        GetVehicleType tmpObj;
        public VIGetVehicleTypeResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetVehicleType()
        {
            ContentDialog viGetVehicleTypeCD = new ContentDialog();
            viGetVehicleTypeCD.Content = this;

            viGetVehicleTypeCD.PrimaryButtonText = Const.TxLater;
            viGetVehicleTypeCD.PrimaryButtonClick += ViGetVehicleTypeCD_PrimaryButtonClick;

            viGetVehicleTypeCD.SecondaryButtonText = Const.Reset;
            viGetVehicleTypeCD.SecondaryButtonClick += ViGetVehicleTypeCD_ResetButtonClick;

            viGetVehicleTypeCD.CloseButtonText = Const.Close;
            viGetVehicleTypeCD.CloseButtonClick += ViGetVehicleTypeCD_CloseButtonClick;

            tmpObj = new GetVehicleType();
            tmpObj = (GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<GetVehicleType>(tmpObj.getMethod());

            if (tmpObj == null)
            {
                tmpObj = (GetVehicleType)BuildDefaults.buildDefaultMessage(typeof(GetVehicleType), 0);
            }

            if(tmpObj != null)
            {
                if (tmpObj.getVehicleType() != null)
                {
                    if (null != tmpObj.getVehicleType().getMake())
                        MakeTextBox.Text = tmpObj.getVehicleType().getMake();
                    else {
                        MakeTextBox.IsEnabled = false;
                        MakeCheckBox.IsChecked = false;

                    }
                    if (null != tmpObj.getVehicleType().getModel())
                        ModelTextBox.Text = tmpObj.getVehicleType().getModel();
                    else
                    {
                        ModelTextBox.IsEnabled = false;
                        ModelCheckBox.IsChecked = false;

                    }

                    if (null != tmpObj.getVehicleType().getModelYear())
                        ModelYearTextBox.Text = tmpObj.getVehicleType().getModelYear();
                    else
                    {
                        ModelYearTextBox.IsEnabled = false;
                        ModelYearCheckBox.IsChecked = false;

                    }
                    if (null != tmpObj.getVehicleType().getTrim())
                        TrimTextBox.Text = tmpObj.getVehicleType().getTrim();
                    else
                    {
                        TrimTextBox.IsEnabled = false;
                        TrimCheckBox.IsChecked = false;

                    }
                }
                else {
                    VehicleTypeCheckBox.IsChecked = false;
                    MakeTextBox.IsEnabled = false;
                    MakeCheckBox.IsEnabled = false;
                    ModelYearTextBox.IsEnabled = false;
                    ModelYearCheckBox.IsEnabled = false;
                    ModelTextBox.IsEnabled = false;
                    ModelCheckBox.IsEnabled = false;
                    TrimTextBox.IsEnabled = false;
                    TrimCheckBox.IsEnabled = false;

                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode()                                                                                                                                                                                                                                                                                                                                                                                                                                                              ;
            }

            await viGetVehicleTypeCD.ShowAsync();
        }

        private void ViGetVehicleTypeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? result = null;
            VehicleType vehicleType = null;

            if(ResultCodeCheckBox.IsChecked == true)
            {
                result = (Result)ResultCodeComboBox.SelectedIndex;
            }

            if(VehicleTypeCheckBox.IsChecked == true)
            {
                vehicleType = new VehicleType();
                if (MakeCheckBox.IsChecked == true)
                    vehicleType.make = MakeTextBox.Text;
                if (ModelCheckBox.IsChecked == true)
                    vehicleType.model = ModelTextBox.Text;
                if (ModelYearCheckBox.IsChecked == true)
                    vehicleType.modelYear = ModelYearTextBox.Text;
                if (TrimCheckBox.IsChecked == true)
                    vehicleType.trim = TrimTextBox.Text;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetVehicleTypeResponse(BuildRpc.getNextId(), result, vehicleType);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViGetVehicleTypeCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if(tmpObj != null)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViGetVehicleTypeCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void VehicleTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MakeCheckBox.IsEnabled = (bool)VehicleTypeCheckBox.IsChecked;
            ModelCheckBox.IsEnabled = (bool)VehicleTypeCheckBox.IsChecked;
            ModelYearCheckBox.IsEnabled = (bool)VehicleTypeCheckBox.IsChecked;
            TrimCheckBox.IsEnabled = (bool)VehicleTypeCheckBox.IsChecked;

            MakeTextBox.IsEnabled = (bool)MakeCheckBox.IsChecked && (bool)VehicleTypeCheckBox.IsChecked;
            ModelTextBox.IsEnabled = (bool)ModelCheckBox.IsChecked && (bool)VehicleTypeCheckBox.IsChecked;
            ModelYearTextBox.IsEnabled = (bool)ModelYearCheckBox.IsChecked && (bool)VehicleTypeCheckBox.IsChecked;
            TrimTextBox.IsEnabled = (bool)TrimCheckBox.IsChecked && (bool)VehicleTypeCheckBox.IsChecked;
        }

        private void MakeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MakeTextBox.IsEnabled = (bool)MakeCheckBox.IsChecked;
        }

        private void ModelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModelTextBox.IsEnabled = (bool)ModelCheckBox.IsChecked;
        }

        private void ModelYearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ModelYearTextBox.IsEnabled = (bool)ModelYearCheckBox.IsChecked;
        }

        private void TrimCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TrimTextBox.IsEnabled = (bool)TrimCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
