﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class DeviceStatus : UserControl
    {
        public CheckBox voiceRecOnCheckBox;
        public CheckBox btIconONCheckBox;
        public CheckBox callActiveCheckBox;
        public CheckBox phoneRoamingCheckBox;
        public CheckBox textMsgAvailableCheckBox;
        
        public CheckBox battLevelStatusCheckBox;
        public ComboBox battLevelStatusComboBox;

        public CheckBox stereoAudioOutputMutedCheckBox;
        public CheckBox monoAudioOutputMutedCheckBox;

        public CheckBox signalLevelStatusCheckBox;
        public ComboBox signalLevelStatusComboBox;

        public CheckBox primaryAudioSourceCheckBox;
        public ComboBox primaryAudioSourcesComboBox;

        public CheckBox eCallEventActiveCheckBox;


        public ToggleSwitch voiceRecOnToggle;
        public ToggleSwitch btIconONToggle;
        public ToggleSwitch callActiveToggle;
        public ToggleSwitch phoneRoamingToggle;
        public ToggleSwitch textMsgAvailableToggle;
        public ToggleSwitch stereoAudioOutputMutedToggle;
        public ToggleSwitch monoAudioOutputMutedToggle;
        public ToggleSwitch eCallEventActiveToggle;

        public DeviceStatus(HmiApiLib.Common.Structs.DeviceStatus deviceStatus)
        {
            this.InitializeComponent();

            BattLevelStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.DeviceLevelStatus));
            BattLevelStatusComboBox.SelectedIndex = 0;

            SignalLevelStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.DeviceLevelStatus));
            SignalLevelStatusComboBox.SelectedIndex = 0;

            PrimaryAudioSourcesComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.PrimaryAudioSource));
            PrimaryAudioSourcesComboBox.SelectedIndex = 0;
            voiceRecOnToggle = VoiceRecOnToggle;
            btIconONToggle = BTIconONToggle;
            callActiveToggle = CallActiveToggle;
            phoneRoamingToggle = PhoneRoamingToggle;
            textMsgAvailableToggle = TextMsgAvailableToggle;
            textMsgAvailableToggle = TextMsgAvailableToggle;
            stereoAudioOutputMutedToggle = StereoAudioOutputMutedToggle;
            monoAudioOutputMutedToggle = MonoAudioOutputMutedToggle;
            eCallEventActiveToggle = ECallEventActiveToggle;
            voiceRecOnCheckBox = VoiceRecOnCheckBox;
            btIconONCheckBox = BTIconONCheckBox;
            callActiveCheckBox = CallActiveCheckBox;
            phoneRoamingCheckBox = PhoneRoamingCheckBox;
            textMsgAvailableCheckBox = TextMsgAvailableCheckBox;

            battLevelStatusCheckBox = BattLevelStatusCheckBox;
            battLevelStatusComboBox = BattLevelStatusComboBox;

            stereoAudioOutputMutedCheckBox = StereoAudioOutputMutedCheckBox;
            monoAudioOutputMutedCheckBox = MonoAudioOutputMutedCheckBox;

            signalLevelStatusCheckBox = SignalLevelStatusCheckBox;
            signalLevelStatusComboBox = SignalLevelStatusComboBox;

            primaryAudioSourceCheckBox = PrimaryAudioSourceCheckBox;
            primaryAudioSourcesComboBox = PrimaryAudioSourcesComboBox;

            eCallEventActiveCheckBox = ECallEventActiveCheckBox;

            if (null != deviceStatus)
            {
                if (deviceStatus.getVoiceRecOn() != null)
                {
                   
                    VoiceRecOnToggle.IsOn =(bool) deviceStatus.getVoiceRecOn();
                    VoiceRecOnToggle.IsEnabled = true;

                    VoiceRecOnCheckBox.IsChecked = true;

                }
                if (deviceStatus.getBtIconOn() != null)
                {
                   
                    BTIconONToggle.IsOn = (bool)deviceStatus.getBtIconOn();
                    BTIconONToggle.IsEnabled = true;

                    BTIconONCheckBox.IsChecked = true;

                }
                if (deviceStatus.getCallActive() != null)
                {
                   
                    CallActiveToggle.IsEnabled = true;

                    CallActiveToggle.IsOn = (bool)deviceStatus.getCallActive();

                    CallActiveCheckBox.IsChecked = true;

                }
                if (deviceStatus.getPhoneRoaming() != null)
                {
                    
                
                
                    PhoneRoamingToggle.IsEnabled = true;

                    PhoneRoamingToggle.IsOn = (bool)deviceStatus.getPhoneRoaming();


                    PhoneRoamingCheckBox.IsChecked = true;

                }
                if (deviceStatus.getTextMsgAvailable() != null)
                {
                
                    TextMsgAvailableToggle.IsEnabled = true;

                    TextMsgAvailableToggle.IsOn = (bool)deviceStatus.getTextMsgAvailable();


                    TextMsgAvailableCheckBox.IsChecked = true;

                }
                if (deviceStatus.getStereoAudioOutputMuted() != null)
                {
                   
                
                    StereoAudioOutputMutedToggle.IsEnabled = true;


                    StereoAudioOutputMutedToggle.IsOn = (bool)deviceStatus.getStereoAudioOutputMuted();


                    StereoAudioOutputMutedCheckBox.IsChecked = true;

                }
                if (deviceStatus.getBattLevelStatus() != null)
                {
                    BattLevelStatusComboBox.SelectedIndex = (int)deviceStatus.getBattLevelStatus();
                    BattLevelStatusCheckBox.IsChecked = true;
                    BattLevelStatusComboBox.IsEnabled = true;
               
                    
                }

                if (deviceStatus.getMonoAudioOutputMuted() != null)
                {
                
                    MonoAudioOutputMutedToggle.IsEnabled = true;

                    MonoAudioOutputMutedCheckBox.IsChecked = true;

                    MonoAudioOutputMutedToggle.IsOn = (bool)deviceStatus.getMonoAudioOutputMuted();

                }
                if (deviceStatus.getSignalLevelStatus() != null)
                {
                    SignalLevelStatusComboBox.SelectedIndex = (int)deviceStatus.getSignalLevelStatus();
                    SignalLevelStatusCheckBox.IsChecked = true;
                    SignalLevelStatusComboBox.IsEnabled = true;

               
                }
                if (deviceStatus.getPrimaryAudioSource() != null)
                {
                    PrimaryAudioSourcesComboBox.SelectedIndex = (int)deviceStatus.getPrimaryAudioSource();
                    PrimaryAudioSourceCheckBox.IsChecked = true;
                    PrimaryAudioSourcesComboBox.IsEnabled = true;
                
                    
                }

                if (deviceStatus.getECallEventActive() != null)
                {
                
                    ECallEventActiveToggle.IsEnabled = true;

                    ECallEventActiveCheckBox.IsChecked = true;

                    ECallEventActiveToggle.IsOn = (bool)deviceStatus.getECallEventActive();
                }
            }
    }

        private void BattLevelStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BattLevelStatusComboBox.IsEnabled = (bool)BattLevelStatusCheckBox.IsChecked;
        }

        private void SignalLevelStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SignalLevelStatusComboBox.IsEnabled = (bool)SignalLevelStatusCheckBox.IsChecked;
        }

        private void PrimaryAudioSourceCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PrimaryAudioSourcesComboBox.IsEnabled = (bool)PrimaryAudioSourceCheckBox.IsChecked;
        }

        private void VoiceRecOnCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VoiceRecOnToggle.IsEnabled = (bool)VoiceRecOnCheckBox.IsChecked;

        }

        private void BTIconONCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BTIconONToggle.IsEnabled = (bool)BTIconONCheckBox.IsChecked;

        }

        private void CallActiveCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CallActiveToggle.IsEnabled = (bool)CallActiveCheckBox.IsChecked;

        }

        private void PhoneRoamingCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PhoneRoamingToggle.IsEnabled = (bool)PhoneRoamingCheckBox.IsChecked;

        }

        private void TextMsgAvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TextMsgAvailableToggle.IsEnabled = (bool)TextMsgAvailableCheckBox.IsChecked;


        }

        private void StereoAudioOutputMutedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            StereoAudioOutputMutedToggle.IsEnabled = (bool)StereoAudioOutputMutedCheckBox.IsChecked;

        }

        private void MonoAudioOutputMutedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MonoAudioOutputMutedToggle.IsEnabled = (bool)MonoAudioOutputMutedCheckBox.IsChecked;

        }

        private void ECallEventActiveCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ECallEventActiveToggle.IsEnabled = (bool)ECallEventActiveCheckBox.IsChecked;

        }
    }
}
