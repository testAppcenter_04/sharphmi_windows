﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class TirePressure : UserControl
    {
        public CheckBox pressureTelltaleCheckBox;
        public ComboBox pressureTelltaleComboBox;

        public CheckBox leftFrontCheckBox;
        public ComboBox leftFrontComboBox;

        public CheckBox rightFrontCheckBox;
        public ComboBox rightFrontComboBox;

        public CheckBox leftRearCheckBox;
        public ComboBox leftRearComboBox;

        public CheckBox rightRearCheckBox;
        public ComboBox rightRearComboBox;

        public CheckBox innerLeftRearCheckBox;
        public ComboBox innerLeftRearComboBox;

        public CheckBox innerRightRearCheckBox;
        public ComboBox innerRightRearComboBox;


        public TirePressure(HmiApiLib.Common.Structs.TireStatus tirePressure)
        {
            this.InitializeComponent();

            PressureTelltaleComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.WarningLightStatus));
            PressureTelltaleComboBox.SelectedIndex = 0;

            LeftFrontComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            LeftFrontComboBox.SelectedIndex = 0;

            RightFrontComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            RightFrontComboBox.SelectedIndex = 0;

            LeftRearComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            LeftRearComboBox.SelectedIndex = 0;

            RightRearComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            RightRearComboBox.SelectedIndex = 0;

            InnerLeftRearComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            InnerLeftRearComboBox.SelectedIndex = 0;

            InnerRightRearComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            InnerRightRearComboBox.SelectedIndex = 0;


            pressureTelltaleCheckBox = PressureTelltaleCheckBox;
            pressureTelltaleComboBox = PressureTelltaleComboBox;

            leftFrontCheckBox = LeftFrontCheckBox;
            leftFrontComboBox = LeftFrontComboBox;

            rightFrontCheckBox = RightFrontCheckBox;
            rightFrontComboBox = RightFrontComboBox;

            leftRearCheckBox = LeftRearCheckBox;
            leftRearComboBox = LeftRearComboBox;

            rightRearCheckBox = RightRearCheckBox;
            rightRearComboBox = RightRearComboBox;

            innerLeftRearCheckBox = InnerLeftRearCheckBox;
            innerLeftRearComboBox = InnerLeftRearComboBox;

            innerRightRearCheckBox = InnerRightRearCheckBox;
            innerRightRearComboBox = InnerRightRearComboBox;

            if (null != tirePressure)
            {   if (tirePressure.getPressureTelltale() != null)
                {
                    PressureTelltaleComboBox.SelectedIndex = (int)tirePressure.getPressureTelltale();
                    PressureTelltaleCheckBox.IsChecked = true;

                }
                else
                {
                    PressureTelltaleComboBox.IsEnabled = false;

                }

                if (null != tirePressure.getleftFront() && null != tirePressure.getleftFront().getStatus())
                {
                    LeftFrontComboBox.SelectedIndex = (int)tirePressure.getleftFront().getStatus();
                    LeftFrontCheckBox.IsChecked = true;

                }
                else
                {
                    LeftFrontComboBox.IsEnabled = false;
                }

                if (null != tirePressure.getRightFront() && null != tirePressure.getRightFront().getStatus())
                {
                    RightFrontComboBox.SelectedIndex = (int)tirePressure.getRightFront().getStatus();
                    RightFrontCheckBox.IsChecked = true;

                }
                else
                {
                    RightFrontComboBox.IsEnabled = false;
                }
                if (null != tirePressure.getLeftRear() && null != tirePressure.getLeftRear().getStatus())
                {
                    LeftRearComboBox.SelectedIndex = (int)tirePressure.getLeftRear().getStatus();
                    LeftRearCheckBox.IsChecked = true;

                }
                else
                {
                    LeftRearComboBox.IsEnabled = false;
                }

                if (null != tirePressure.getRightRear() && null != tirePressure.getRightRear().getStatus())
                {
                    RightRearComboBox.SelectedIndex = (int)tirePressure.getRightRear().getStatus();
                    RightRearCheckBox.IsChecked = true;

                }
                else
                {
                    RightRearComboBox.IsEnabled = false;
                }

                if (null != tirePressure.getInnerLeftRear() && null != tirePressure.getInnerLeftRear().getStatus())
                {
                    InnerLeftRearComboBox.SelectedIndex = (int)tirePressure.getInnerLeftRear().getStatus();
                    InnerLeftRearCheckBox.IsChecked = true;
                }
                else
                {
                    InnerLeftRearComboBox.IsEnabled = false;

                }
                if (null != tirePressure.getInnerRightRear() && null != tirePressure.getInnerRightRear().getStatus())
                {

                    InnerRightRearComboBox.SelectedIndex = (int)tirePressure.getInnerRightRear().getStatus();
                    InnerRightRearCheckBox.IsChecked = true;

                }
                else
                {
                    InnerRightRearComboBox.IsEnabled = false;
                }

            }
        }

        private void PressureTelltaleCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PressureTelltaleComboBox.IsEnabled = (bool)PressureTelltaleCheckBox.IsChecked;
        }

        private void LeftFrontCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LeftFrontComboBox.IsEnabled = (bool)LeftFrontCheckBox.IsChecked;
        }

        private void RightFrontCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RightFrontComboBox.IsEnabled = (bool)RightFrontCheckBox.IsChecked;
        }

        private void LeftRearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LeftRearComboBox.IsEnabled = (bool)LeftRearCheckBox.IsChecked;
        }

        private void RightRearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RightRearComboBox.IsEnabled = (bool)RightRearCheckBox.IsChecked;
        }

        private void InnerLeftRearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InnerLeftRearComboBox.IsEnabled = (bool)InnerLeftRearCheckBox.IsChecked;
        }

        private void InnerRightRearCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InnerRightRearComboBox.IsEnabled = (bool)InnerRightRearCheckBox.IsChecked;
        }
    }
}
