﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class BodyInformation : UserControl
    {
        public CheckBox parkBrakeArchiveCheckBox;

        public CheckBox ignitionStableStatusCheckBox;
        public ComboBox ignitionStableStatusComboBox;

        public CheckBox ignitionStatusCheckBox;
        public ComboBox ignitionStatusComboBox;

        public CheckBox driverDoorAjarCheckBox;
        public CheckBox passengerDoorAjarCheckBox;
        public CheckBox rearLeftDoorAjarCheckBox;
        public CheckBox rearRightDoorAjarCheckBox;

        public ToggleSwitch parkBrakeArchiveToggle;

        

        public ToggleSwitch driverDoorAjarToggle;
        public ToggleSwitch passengerDoorAjarToggle;
        public ToggleSwitch rearLeftDoorAjarToggle;
        public ToggleSwitch rearRightDoorAjarToggle;

        public BodyInformation(HmiApiLib.Common.Structs.BodyInformation bodyInformation)
        {
            InitializeComponent();

            parkBrakeArchiveCheckBox = ParkBrakeArchiveCheckBox;

            IgnitionStableStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.IgnitionStableStatus));
            IgnitionStableStatusComboBox.SelectedIndex = 0;

            IgnitionStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.IgnitionStatus));
            IgnitionStatusComboBox.SelectedIndex = 0;
            parkBrakeArchiveToggle = ParkBrakeArchiveToggle;
            
            driverDoorAjarToggle = DriverDoorAjarToggle;
            passengerDoorAjarToggle = PassengerDoorAjarToggle;
            rearRightDoorAjarToggle = RearRightDoorAjarToggle;
            rearLeftDoorAjarToggle = RearLeftDoorAjarToggle;
            ignitionStableStatusCheckBox = IgnitionStableStatusCheckBox;
            ignitionStableStatusComboBox = IgnitionStableStatusComboBox;

            ignitionStatusCheckBox = IgnitionStatusCheckBox;
            ignitionStatusComboBox = IgnitionStatusComboBox;

            driverDoorAjarCheckBox = DriverDoorAjarCheckBox;
            passengerDoorAjarCheckBox = PassengerDoorAjarCheckBox;
            rearLeftDoorAjarCheckBox = RearLeftDoorAjarCheckBox;
            rearRightDoorAjarCheckBox = RearRightDoorAjarCheckBox;

            if (null != bodyInformation)
            {
                if (bodyInformation.getParkBrakeActive() == null)
                {
                    ParkBrakeArchiveToggle.IsEnabled = false;
                }
                else
                {
                    ParkBrakeArchiveToggle.IsEnabled = true;
                    ParkBrakeArchiveCheckBox.IsChecked = true;
                    ParkBrakeArchiveToggle.IsOn = (bool)bodyInformation.getParkBrakeActive();
                }
                if (bodyInformation.getIgnitionStableStatus() != null)
                {
                    IgnitionStableStatusComboBox.SelectedIndex = (int)bodyInformation.getIgnitionStableStatus();
                    IgnitionStableStatusCheckBox.IsChecked = true;
                    ParkBrakeArchiveToggle.IsEnabled = true;

                }
                else
                {
                    IgnitionStableStatusComboBox.IsEnabled = false;
                }
                if (bodyInformation.getIgnitionStatus() != null)
                {
                    IgnitionStatusComboBox.SelectedIndex = (int)bodyInformation.getIgnitionStatus();
                    IgnitionStatusCheckBox.IsChecked = true;
                    IgnitionStatusComboBox.IsEnabled = true;

                }
                else
                {
                    IgnitionStatusComboBox.IsEnabled = false;
                }
                if (bodyInformation.getDriverDoorAjar() == null)
                {
                    DriverDoorAjarToggle.IsEnabled = false;
                }
                else
                {
                    DriverDoorAjarToggle.IsEnabled = true;

                    DriverDoorAjarCheckBox.IsChecked = true;
                    DriverDoorAjarToggle.IsOn = (bool)bodyInformation.getDriverDoorAjar();
                }

                if (bodyInformation.getPassengerDoorAjar() == null)
                {
                    PassengerDoorAjarToggle.IsEnabled = false;
                }
                else
                {
                    PassengerDoorAjarToggle.IsEnabled = true;

                    PassengerDoorAjarCheckBox.IsChecked = true;
                    PassengerDoorAjarToggle.IsOn = (bool)bodyInformation.getPassengerDoorAjar();
                }

                if (bodyInformation.getRearLeftDoorAjar() == null)
                {
                    RearLeftDoorAjarToggle.IsEnabled = false;
                }
                else
                {
                    RearLeftDoorAjarToggle.IsEnabled = true;

                    RearLeftDoorAjarCheckBox.IsChecked = true;
                    RearLeftDoorAjarToggle.IsOn = (bool)bodyInformation.getRearLeftDoorAjar();
                }
                if (bodyInformation.getRearRightDoorAjar() == null)
                {
                    RearRightDoorAjarToggle.IsEnabled = false;
                }
                else
                {
                    RearRightDoorAjarToggle.IsEnabled = true;

                    RearRightDoorAjarCheckBox.IsChecked = true;
                    RearRightDoorAjarToggle.IsOn = (bool)bodyInformation.getRearRightDoorAjar();
                }
            }
    }

        private void IgnitionStableStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IgnitionStableStatusComboBox.IsEnabled = (bool)IgnitionStableStatusCheckBox.IsChecked;
        }

        private void IgnitionStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IgnitionStatusComboBox.IsEnabled = (bool)IgnitionStatusCheckBox.IsChecked;
        }

        private void RearRightDoorAjarCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RearRightDoorAjarToggle.IsEnabled = (bool)RearRightDoorAjarCheckBox.IsChecked;

        }

        private void RearLeftDoorAjarCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RearLeftDoorAjarToggle.IsEnabled = (bool)RearLeftDoorAjarCheckBox.IsChecked;

        }

        private void PassengerDoorAjarCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PassengerDoorAjarToggle.IsEnabled = (bool)PassengerDoorAjarCheckBox.IsChecked;

        }

        private void DriverDoorAjarCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverDoorAjarToggle.IsEnabled = (bool)DriverDoorAjarCheckBox.IsChecked;

        }

        private void ParkBrakeArchiveCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ParkBrakeArchiveToggle.IsEnabled = (bool)ParkBrakeArchiveCheckBox.IsChecked;

        }
    }
}
