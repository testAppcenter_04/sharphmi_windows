﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications;
using System.Collections.ObjectModel;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIOnVehicleData : UserControl
    {
        OnVehicleData tmpObj;
        ContentDialog viOnVehicleDataCD = new ContentDialog(); 

        HmiApiLib.Common.Structs.GPSData gpsData;
        HmiApiLib.Common.Structs.TireStatus tirePressure;
        HmiApiLib.Common.Structs.BeltStatus beltStatus;
        HmiApiLib.Common.Structs.BodyInformation bodyInformation;
        HmiApiLib.Common.Structs.DeviceStatus deviceStatus;
        HmiApiLib.Common.Structs.HeadLampStatus headLampStatus;
        HmiApiLib.Common.Structs.ECallInfo ecallInfo;
        HmiApiLib.Common.Structs.AirbagStatus airbagStatus;
        HmiApiLib.Common.Structs.EmergencyEvent emergencyEvent;
        HmiApiLib.Common.Structs.ClusterModeStatus clusterModes;
        ObservableCollection<HmiApiLib.Common.Structs.FuelRange> fuelRangeList = new ObservableCollection<HmiApiLib.Common.Structs.FuelRange>();

        public VIOnVehicleData()
        {
            this.InitializeComponent();

            FuelLevelStateComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ComponentVolumeStatus));
            FuelLevelStateComboBox.SelectedIndex = 0;

            PRNDLComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.PRNDL));
            PRNDLComboBox.SelectedIndex = 0;

            TurnSignalComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.TurnSignal));
            TurnSignalComboBox.SelectedIndex = 0;

            DriverBrakingComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            DriverBrakingComboBox.SelectedIndex = 0;

            WiperStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.WiperStatus));
            WiperStatusComboBox.SelectedIndex = 0;

            MyKeyComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataStatus));
            MyKeyComboBox.SelectedIndex = 0;

            ElectronicParkBrakeStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ElectronicParkBrakeStatus));
            ElectronicParkBrakeStatusComboBox.SelectedIndex = 0;

            AddFuelRangeList.ItemsSource = fuelRangeList;
        }

        public async void ShowOnVehicleData()
        {
            viOnVehicleDataCD = new ContentDialog();
            viOnVehicleDataCD.Content = this;

            viOnVehicleDataCD.PrimaryButtonText = Const.TxNow;
            viOnVehicleDataCD.PrimaryButtonClick += ViOnVehicleDataCD_PrimaryButtonClick;

            viOnVehicleDataCD.SecondaryButtonText = Const.Reset;
            viOnVehicleDataCD.SecondaryButtonClick += ViOnVehicleDataCD_SecondaryButtonClick;

            viOnVehicleDataCD.CloseButtonText = Const.Close;
            viOnVehicleDataCD.CloseButtonClick += ViOnVehicleDataCD_CloseButtonClick;

            tmpObj = new OnVehicleData();
            tmpObj = (OnVehicleData)AppUtils.getSavedPreferenceValueForRpc<OnVehicleData>(tmpObj.getMethod());
            if (tmpObj == null)
            {
                tmpObj = (OnVehicleData)BuildDefaults.buildDefaultMessage(typeof(OnVehicleData), 0);
            }

            if (tmpObj != null)
            {
                if (tmpObj.getGps() != null)
                {
                    gpsData = tmpObj.getGps();
                    GPSCheckBox.IsChecked = true;
                    GPSButton.IsEnabled = true;

                }
                
                if (null != tmpObj.getSpeed())
                {
                    SpeedTextBox.Text = tmpObj.getSpeed().ToString();
                    SpeedCheckBox.IsChecked = true;
                }
               
                if (null != tmpObj.getRpm())
                {
                    RPMTextBox.Text = tmpObj.getRpm().ToString();
                    RPMCheckBox.IsChecked = true;

                }
                else
                {
                    RPMTextBox.IsEnabled = false;

                }
                if (null != tmpObj.getFuelLevel())
                {
                    FuelLevelTextBox.Text = tmpObj.getFuelLevel().ToString();
                    FuelLevelCheckBox.IsChecked = true;
                }
                else
                {
                    FuelLevelTextBox.IsEnabled = false;
                   
                }

                if (null != tmpObj.getFuelLevel_State())
                {
                    FuelLevelStateComboBox.SelectedIndex = (int)tmpObj.getFuelLevel_State();
                    FuelLevelStateCheckBox.IsChecked = true;

                }
                else
                {
                    FuelLevelStateComboBox.IsEnabled = false;
                }
                if (null != tmpObj.getInstantFuelConsumption())
                {
                    InstantFuelConsumptionTextBox.Text = tmpObj.getInstantFuelConsumption().ToString();
                    InstantFuelConsumptionCheckBox.IsChecked = true;

                }
                else
                {
                    InstantFuelConsumptionTextBox.IsEnabled = false;
                    InstantFuelConsumptionCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getExternalTemperature())
                {
                    ExternalTemperatureTextBox.Text = tmpObj.getExternalTemperature().ToString();
                    ExternalTemperatureCheckBox.IsChecked = true;

                }
                else
                {
                    ExternalTemperatureTextBox.IsEnabled = false;

                }

                if (null != tmpObj.getVin())
                {
                    VinTextBox.Text = tmpObj.getVin().ToString();
                    VinCheckBox.IsChecked = true;

                }
                else
                {
                    VinTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getPrndl())
                {
                    PRNDLComboBox.SelectedIndex = (int)tmpObj.getPrndl();
                    PRNDLCheckBox.IsChecked = true;

                }
                else
                {
                    PRNDLComboBox.IsEnabled = false;
                }
                if (tmpObj.getTirePressure() != null)
                {
                    tirePressure = tmpObj.getTirePressure();
                    PRNDLCheckBox.IsChecked = true;

                }

                else
                {
                    TirePressureButton.IsEnabled = false;
                    TirePressureCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getOdometer())
                {
                    OdometerTextBox.Text = tmpObj.getOdometer().ToString();
                    OdometerCheckBox.IsChecked = true;

                }

                else
                {
                    OdometerTextBox.IsEnabled = false;
                }
                if (tmpObj.getBeltStatus() != null)
                {
                    beltStatus = tmpObj.getBeltStatus();
                    BeltStatusCheckBox.IsChecked = true;
                    BeltStatusButton.IsEnabled = true;

                }
                
                if (tmpObj.getBodyInformation() != null)
                {
                    bodyInformation = tmpObj.getBodyInformation();
                    BodyInformationCheckBox.IsChecked = true;
                    BodyInformationButton.IsEnabled = true;

                }
                
                if (tmpObj.getDeviceStatus() != null)
                {
                    deviceStatus = tmpObj.getDeviceStatus();
                    DeviceStatusCheckBox.IsChecked = true;
                    DeviceStatusButton.IsEnabled = true;

                }
                
                if (tmpObj.getHeadLampStatus() != null)
                {
                    headLampStatus = tmpObj.getHeadLampStatus();
                    DeviceStatusCheckBox.IsChecked = true;
                    HeadLampStatusButton.IsEnabled = true;

                }
                
                if (null != tmpObj.getDriverBraking())
                {
                    DriverBrakingComboBox.SelectedIndex = (int)tmpObj.getDriverBraking();
                    DriverBrakingCheckBox.IsChecked = true;

                }
                else
                {
                    DriverBrakingComboBox.IsEnabled = false;
                }

                if (null != tmpObj.getWiperStatus())
                {
                    WiperStatusComboBox.SelectedIndex = (int)tmpObj.getWiperStatus();
                    WiperStatusCheckBox.IsChecked = true;

                }
                else
                {
                    WiperStatusComboBox.IsEnabled = false;
                }
                if (null != tmpObj.getEngineTorque())
                {
                    EngineTorqueTextBox.Text = tmpObj.getEngineTorque().ToString();
                    EngineTorqueCheckBox.IsChecked = true;

                }
                else
                {
                    EngineTorqueTextBox.IsEnabled = false;
                }

                if (null != tmpObj.getAccPedalPosition())
                {
                    AccPedalPositionTextBox.Text = tmpObj.getAccPedalPosition().ToString();
                    AccPadelPositionCheckBox.IsChecked = true;

                }
                else
                {
                    AccPedalPositionTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getSteeringWheelAngle())
                {
                    SteeringWheelAngleTextBox.Text = tmpObj.getSteeringWheelAngle().ToString();
                    SteeringWheelAngleCheckBox.IsChecked = true;

                }
                else
                {
                    SteeringWheelAngleTextBox.IsEnabled = false;
                }
                if (tmpObj.getECallInfo() != null)
                {
                    ecallInfo = tmpObj.getECallInfo();
                    EcallInfoCheckBox.IsChecked = true;
                    EcallInfoButton.IsEnabled = true;

                }
                
                if (tmpObj.getAirbagStatus() != null)
                {
                    airbagStatus = tmpObj.getAirbagStatus();
                    AirbagStatusCheckBox.IsChecked = true;
                    AirbagStatusButton.IsEnabled = true;

                }
               
                if (tmpObj.getEmergencyEvent() != null)
                {
                    emergencyEvent = tmpObj.getEmergencyEvent();
                    EmergencyEventCheckBox.IsChecked = true;
                    EmergencyEventButton.IsEnabled = true;

                }
                
                if (tmpObj.getClusterModes() != null)
                {
                    clusterModes = tmpObj.getClusterModes();
                    ClusterModesCheckBox.IsChecked = true;
                    ClusterModesButton.IsEnabled = true;

                }
               
                if (null != tmpObj.getMyKey().getE911Override())
                {
                    MyKeyComboBox.SelectedIndex = (int)tmpObj.getMyKey().getE911Override();
                    MyKeyCheckBox.IsChecked = true;

                }
                else
                {
                    MyKeyComboBox.IsEnabled = false;
                }
                if (null != tmpObj.getElectronicParkBrakeStatus())
                {
                    ElectronicParkBrakeStatusComboBox.SelectedIndex = (int)tmpObj.getElectronicParkBrakeStatus();
                    ElectronicParkBrakeStatusCheckBox.IsChecked = true;

                }
                else
                {
                    ElectronicParkBrakeStatusComboBox.IsEnabled = false;
                }
                if (null != tmpObj.getTurnSignal())
                {
                    TurnSignalComboBox.SelectedIndex = (int)tmpObj.getTurnSignal();
                    TurnSignalCheckBox.IsChecked = true;

                }
                else
                {
                    TurnSignalComboBox.IsEnabled = false;
                }

                if (null != tmpObj.getEngineOilLife())
                {
                    EngineOilLifeCheckBox.IsChecked = true;
                    EngineOilLifeTextBox.Text = tmpObj.getEngineOilLife().ToString();
                }
                else
                {

                    EngineOilLifeTextBox.IsEnabled = false;
                }
                if (tmpObj.getFuelRange() != null)
                {
                    List<HmiApiLib.Common.Structs.FuelRange> fuelRange = tmpObj.getFuelRange();
                    foreach (HmiApiLib.Common.Structs.FuelRange item in fuelRange)
                    {
                        fuelRangeList.Add(item);
                    }
                    AddFuelRangeCheckBox.IsChecked = true;
                    AddFuelRangeButton.IsEnabled = true;

                }
                

            }

            await viOnVehicleDataCD.ShowAsync();
        }

        private void ViOnVehicleDataCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

            if (GPSCheckBox.IsChecked == false)
            {
                gpsData = null;
            }

            float? speed = null;
            if (SpeedCheckBox.IsChecked == true && SpeedTextBox.Text != "")
            {
                try
                {
                    speed = float.Parse(SpeedTextBox.Text);
                }
                catch
                {
                    speed = 0;
                }
            }

            int? rpm = null;
            if (RPMCheckBox.IsChecked == true && RPMTextBox.Text != "")
            {
                try
                {
                    rpm = int.Parse(RPMTextBox.Text);
                }
                catch
                {
                    rpm = 0;
                }
            }

            float? fuelLevel = null;
            if (FuelLevelCheckBox.IsChecked == true && FuelLevelTextBox.Text != "")
            {
                try
                {
                    fuelLevel = float.Parse(FuelLevelTextBox.Text);
                }
                catch
                {
                    fuelLevel = 0;
                }
            }

            ComponentVolumeStatus? fuelLevelState = null;
            if (FuelLevelStateCheckBox.IsChecked == true)
            {
                fuelLevelState = (ComponentVolumeStatus)FuelLevelStateComboBox.SelectedIndex;
            }

            float? instantfuelConsumption = null;
            if (InstantFuelConsumptionCheckBox.IsChecked == true && InstantFuelConsumptionTextBox.Text != "")
            {
                try
                {
                    instantfuelConsumption = float.Parse(InstantFuelConsumptionTextBox.Text);
                }
                catch
                {
                    instantfuelConsumption = 0;
                }
            }

            float? externalTemperature = null;
            if (ExternalTemperatureCheckBox.IsChecked == true && ExternalTemperatureTextBox.Text != "")
            {
                try
                {
                    externalTemperature = int.Parse(ExternalTemperatureTextBox.Text);
                }
                catch
                {
                    externalTemperature = 0;
                }
            }

            String vin = null;
            if (VinCheckBox.IsChecked == true && VinTextBox.Text != "")
            {
                vin = VinTextBox.Text;
            }

            PRNDL? prndl = null;
            if (PRNDLCheckBox.IsChecked == true)
            {
                prndl = (PRNDL)PRNDLComboBox.SelectedIndex;
            }

            TurnSignal? turnSignal = null;
            if (TurnSignalCheckBox.IsChecked == true)
            {
                turnSignal = (TurnSignal)TurnSignalComboBox.SelectedIndex;
            }

            if (TirePressureCheckBox.IsChecked == false)
            {
                tirePressure = null;
            }

            int? odometer = null;
            if (OdometerCheckBox.IsChecked == true && OdometerTextBox.Text != "")
            {
                try
                {
                    odometer = int.Parse(OdometerTextBox.Text);
                }
                catch
                {
                    odometer = 0;
                }
            }

            if (BeltStatusCheckBox.IsChecked == false)
            {
                beltStatus = null;
            }

            if (BodyInformationCheckBox.IsChecked == false)
            {
                bodyInformation = null;
            }

            if (DeviceStatusCheckBox.IsChecked == false)
            {
                deviceStatus = null;
            }

            VehicleDataEventStatus? driverBraking = null;
            if (DriverBrakingCheckBox.IsChecked == true)
            {
                driverBraking = (VehicleDataEventStatus)DriverBrakingComboBox.SelectedIndex;
            }

            WiperStatus? wiperStatus = null;
            if (WiperStatusCheckBox.IsChecked == true)
            {
                wiperStatus = (WiperStatus)WiperStatusComboBox.SelectedIndex;
            }

            if (HeadLampStatusCheckBox.IsChecked == false)
            {
                headLampStatus = null;
            }

            float? engineTorque = null;
            if (EngineTorqueCheckBox.IsChecked == true && EngineTorqueTextBox.Text != "")
            {
                try
                {
                    engineTorque = float.Parse(EngineTorqueTextBox.Text);
                }
                catch
                {
                    engineTorque = 0;
                }
            }

            float? accPedal = null;
            if (AccPadelPositionCheckBox.IsChecked == true && AccPedalPositionTextBox.Text != "")
            {
                try
                {
                    accPedal = float.Parse(AccPedalPositionTextBox.Text);
                }
                catch
                {
                    accPedal = 0;
                }
            }

            float? steeringWheelAngle = null;
            if (SteeringWheelAngleCheckBox.IsChecked == true && SteeringWheelAngleTextBox.Text != "")
            {
                try
                {
                    steeringWheelAngle = float.Parse(SteeringWheelAngleTextBox.Text);
                }
                catch
                {
                    steeringWheelAngle = 0;
                }
            }

            if (EcallInfoCheckBox.IsChecked == false)
            {
                ecallInfo = null;
            }

            if (AirbagStatusCheckBox.IsChecked == false)
            {
                airbagStatus = null;
            }

            if (EmergencyEventCheckBox.IsChecked == false)
            {
                emergencyEvent = null;
            }

            if (ClusterModesCheckBox.IsChecked == false)
            {
                clusterModes = null;
            }

            MyKey myKey = new MyKey();
            if (MyKeyCheckBox.IsChecked == true)
            {
                myKey.e911Override = (VehicleDataStatus)MyKeyComboBox.SelectedIndex;
            }

            ElectronicParkBrakeStatus? electronicParkBrakeStatus = null;
            if (ElectronicParkBrakeStatusCheckBox.IsChecked == true)
            {
                electronicParkBrakeStatus = (ElectronicParkBrakeStatus)ElectronicParkBrakeStatusComboBox.SelectedIndex;
            }

            float? engineOilLife = null;
            if (EngineOilLifeCheckBox.IsChecked == true && EngineOilLifeTextBox.Text != "")
            {
                try
                {
                    engineOilLife = float.Parse(EngineOilLifeTextBox.Text);
                }
                catch
                {
                    engineOilLife = null;
                }
            }


            List<FuelRange> fuelRange = null;
            if (AddFuelRangeCheckBox.IsChecked == true)
            {
                fuelRange = new List<FuelRange>();
                foreach (FuelRange item in fuelRangeList)
                {
                    fuelRange.Add(item);
                }
            }



            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildVehicleInfoOnVehicleData(
                gpsData, speed, rpm, fuelLevel, fuelLevelState, instantfuelConsumption, externalTemperature,
                vin, prndl, turnSignal, tirePressure, odometer, beltStatus, bodyInformation, deviceStatus, driverBraking, wiperStatus,
                headLampStatus, engineTorque, accPedal, steeringWheelAngle, ecallInfo, airbagStatus, emergencyEvent, clusterModes,
                myKey, electronicParkBrakeStatus, engineOilLife, fuelRange);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void ViOnVehicleDataCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (tmpObj != null)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViOnVehicleDataCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void GPSCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            GPSButton.IsEnabled = (bool)GPSCheckBox.IsChecked;
            if (GPSCheckBox.IsChecked == true)
            {
                GPSButton.IsEnabled = true;
            }
            else
            {
                GPSButton.IsEnabled = false;
            }
        }

        private async void GPSTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var gpsdata = new GPS(gpsData);
            ContentDialog gpsCD = new ContentDialog();
            gpsCD.Content = gpsdata;

            gpsCD.PrimaryButtonText = "Ok";
            gpsCD.PrimaryButtonClick += GpsCD_PrimaryButtonClick;

            gpsCD.SecondaryButtonText = "Cancel";
            gpsCD.SecondaryButtonClick += GpsCD_SecondaryButtonClick;

            await gpsCD.ShowAsync();
        }

        private async void GpsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void GpsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var gps = sender.Content as GPS;
            gpsData = new GPSData();
            if (gps.longitudeCheckBox.IsChecked == true && gps.longitudeTextBox.Text != "")
            {
                try
                {
                    gpsData.longitudeDegrees = float.Parse(gps.longitudeTextBox.Text);
                }
                catch
                {
                    gpsData.longitudeDegrees = 0;
                }
            }
            if (gps.latitudeCheckBox.IsChecked == true && gps.longitudeTextBox.Text != "")
            {
                try
                {
                    gpsData.latitudeDegrees = float.Parse(gps.latitudeTextBox.Text);
                }
                catch
                {
                    gpsData.latitudeDegrees = 0;
                }
            }
            if (gps.utcYearCheckBox.IsChecked == true && gps.longitudeTextBox.Text != "")
            {
                try
                {
                    gpsData.utcYear = int.Parse(gps.utcYearTextBox.Text);
                }
                catch
                {
                    gpsData.utcYear = 0;
                }
            }
            if (gps.utcMonthCheckBox.IsChecked == true && gps.utcMonthTextBox.Text != "")
            {
                try
                {
                    gpsData.utcMonth = int.Parse(gps.utcMonthTextBox.Text);
                }
                catch
                {
                    gpsData.utcMonth = 0;
                }
            }
            if (gps.utcDayCheckBox.IsChecked == true && gps.utcDayTextBox.Text != "")
            {
                try
                {
                    gpsData.utcDay = int.Parse(gps.utcDayTextBox.Text);
                }
                catch
                {
                    gpsData.utcDay = 0;
                }
            }
            if (gps.utcHoursCheckBox.IsChecked == true && gps.utcHoursTextBox.Text != "")
            {
                try
                {
                    gpsData.utcHours = int.Parse(gps.utcHoursTextBox.Text);
                }
                catch
                {
                    gpsData.utcHours = 0;
                }
            }
            if (gps.utcMinutesCheckBox.IsChecked == true && gps.utcMinutesTextBox.Text != "")
            {
                try
                {
                    gpsData.utcMinutes = int.Parse(gps.utcMinutesTextBox.Text);
                }
                catch
                {
                    gpsData.utcMinutes = 0;
                }
            }
            if (gps.utcSecondsCheckBox.IsChecked == true && gps.utcSecondsTextBox.Text != "")
            {
                try
                {
                    gpsData.utcSeconds = int.Parse(gps.utcSecondsTextBox.Text);
                }
                catch
                {
                    gpsData.utcSeconds = 0;
                }
            }

            if (gps.compassDirectionCheckBox.IsChecked == true)
            {
                gpsData.compassDirection = (CompassDirection)gps.compassDirectionComboBox.SelectedIndex;
            }

            if (gps.pdopCheckBox.IsChecked == true && gps.pdopTextBox.Text != "")
            {
                try
                {
                    gpsData.pdop = float.Parse(gps.pdopTextBox.Text);
                }
                catch
                {
                    gpsData.pdop = 0;
                }
            }
            if (gps.hdopCheckBox.IsChecked == true && gps.hdopTextBox.Text != "")
            {
                try
                {
                    gpsData.hdop = float.Parse(gps.hdopTextBox.Text);
                }
                catch
                {
                    gpsData.hdop = 0;
                }
            }
            if (gps.vdopCheckBox.IsChecked == true && gps.vdopTextBox.Text != "")
            {
                try
                {
                    gpsData.vdop = float.Parse(gps.vdopTextBox.Text);
                }
                catch
                {
                    gpsData.vdop = 0;
                }
            }

            gpsData.actual = (bool)gps.actualCheckBox.IsChecked;

            if (gps.satelitesCheckBox.IsChecked == true && gps.satellitesTextBox.Text != "")
            {
                try
                {
                    gpsData.satellites = int.Parse(gps.satellitesTextBox.Text);
                }
                catch
                {
                    gpsData.satellites = 0;
                }
            }

            if (gps.dimensionChecKbox.IsChecked == true)
            {
                gpsData.dimension = (Dimension)gps.dimensionComboBox.SelectedIndex;
            }

            if (gps.altitudeChecKBox.IsChecked == true && gps.altitudeTextBox.Text != "")
            {
                try
                {
                    gpsData.altitude = float.Parse(gps.altitudeTextBox.Text);
                }
                catch
                {
                    gpsData.altitude = 0;
                }
            }
            if (gps.headingChecKBox.IsChecked == true && gps.headingTextBox.Text != "")
            {
                try
                {
                    gpsData.heading = float.Parse(gps.headingTextBox.Text);
                }
                catch
                {
                    gpsData.heading = 0;
                }
            }
            if (gps.speedChecKBox.IsChecked == true && gps.speedTextBox.Text != "")
            {
                try
                {
                    gpsData.speed = float.Parse(gps.speedTextBox.Text);
                }
                catch
                {
                    gpsData.speed = 0;
                }
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void SpeedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SpeedTextBox.IsEnabled = (bool)SpeedCheckBox.IsChecked;
        }

        private void RPMCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RPMTextBox.IsEnabled = (bool)RPMCheckBox.IsChecked;
        }

        private void FuelLevelCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelTextBox.IsEnabled = (bool)FuelLevelCheckBox.IsChecked;
        }

        private void FuelLevelStateCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelLevelStateComboBox.IsEnabled = (bool)FuelLevelStateCheckBox.IsChecked;
        }

        private void InstantFuelConsumptionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            InstantFuelConsumptionTextBox.IsEnabled = (bool)InstantFuelConsumptionCheckBox.IsChecked;
        }

        private void ExternalTemperatureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ExternalTemperatureTextBox.IsEnabled = (bool)ExternalTemperatureCheckBox.IsChecked;
        }

        private void VinCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            VinTextBox.IsEnabled = (bool)VinCheckBox.IsChecked;
        }

        private void PRNDLCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PRNDLComboBox.IsEnabled = (bool)PRNDLCheckBox.IsChecked;
        }
        private void TurnSignalCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TurnSignalComboBox.IsEnabled = (bool)TurnSignalCheckBox.IsChecked;
        }

        private void TirePressureCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TirePressureButton.IsEnabled = (bool)TirePressureCheckBox.IsChecked;
        }

        private async void TirePressureTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var tirepressure = new TirePressure(tirePressure);

            ContentDialog tirePressureCD = new ContentDialog();

            tirePressureCD.Content = tirepressure;

            tirePressureCD.PrimaryButtonText = "Ok";
            tirePressureCD.PrimaryButtonClick += TirePressureCD_PrimaryButtonClick;

            tirePressureCD.SecondaryButtonText = "Cancel";
            tirePressureCD.SecondaryButtonClick += TirePressureCD_SecondaryButtonClick;

            await tirePressureCD.ShowAsync();
        }

        private async void TirePressureCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();

        }

        private async void TirePressureCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var tirePressureData = sender.Content as TirePressure;

            tirePressure = new HmiApiLib.Common.Structs.TireStatus();
            if (tirePressureData.pressureTelltaleCheckBox.IsChecked == true)
            {
                tirePressure.pressureTelltale = (WarningLightStatus)tirePressureData.pressureTelltaleComboBox.SelectedIndex;
            }
            if (tirePressureData.leftFrontCheckBox.IsChecked == true)
            {
                SingleTireStatus leftFront = new SingleTireStatus();
                leftFront.status = (ComponentVolumeStatus)tirePressureData.leftFrontComboBox.SelectedIndex;
                tirePressure.leftFront = leftFront;
            }
            if (tirePressureData.rightFrontCheckBox.IsChecked == true)
            {
                SingleTireStatus rightFront = new SingleTireStatus();
                rightFront.status = (ComponentVolumeStatus)tirePressureData.rightFrontComboBox.SelectedIndex;
                tirePressure.rightFront = rightFront;
            }
            if (tirePressureData.leftRearCheckBox.IsChecked == true)
            {
                SingleTireStatus leftRear = new SingleTireStatus();
                leftRear.status = (ComponentVolumeStatus)tirePressureData.leftRearComboBox.SelectedIndex;
                tirePressure.leftRear = leftRear;
            }
            if (tirePressureData.rightRearCheckBox.IsChecked == true)
            {
                SingleTireStatus rightRear = new SingleTireStatus();
                rightRear.status = (ComponentVolumeStatus)tirePressureData.rightRearComboBox.SelectedIndex;
                tirePressure.rightRear = rightRear;
            }
            if (tirePressureData.innerLeftRearCheckBox.IsChecked == true)
            {
                SingleTireStatus innerLeftRear = new SingleTireStatus(); 
                innerLeftRear.status = (ComponentVolumeStatus)tirePressureData.innerLeftRearComboBox.SelectedIndex;
                tirePressure.innerLeftRear = innerLeftRear;
            }
            if (tirePressureData.innerRightRearCheckBox.IsChecked == true)
            {
                SingleTireStatus innerRightRear = new SingleTireStatus(); 
                innerRightRear.status = (ComponentVolumeStatus)tirePressureData.innerRightRearComboBox.SelectedIndex;
                tirePressure.innerRightRear = innerRightRear;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();

        }

        private void OdometerCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OdometerTextBox.IsEnabled = (bool)OdometerCheckBox.IsChecked;
        }

        private void BeltStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BeltStatusButton.IsEnabled = (bool)BeltStatusCheckBox.IsChecked;
        }

        private async void BeltStatusTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var beltstatus = new BeltStatus(beltStatus);

            ContentDialog beltStatusCD = new ContentDialog();
            beltStatusCD.Content = beltstatus;

            beltStatusCD.PrimaryButtonText = "Ok";
            beltStatusCD.PrimaryButtonClick += BeltStatusCD_PrimaryButtonClick;

            beltStatusCD.SecondaryButtonText = "Cancel";
            beltStatusCD.SecondaryButtonClick += BeltStatusCD_SecondaryButtonClick;

            await beltStatusCD.ShowAsync();
        }

        private async void BeltStatusCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void BeltStatusCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var beltStatusData = sender.Content as BeltStatus;

            beltStatus = new HmiApiLib.Common.Structs.BeltStatus();
            if (beltStatusData.driverBeltDeployedCheckBox.IsChecked == true)
            {
                beltStatus.driverBeltDeployed = (VehicleDataEventStatus)beltStatusData.driverBeltDeployedComboBox.SelectedIndex;
            }
            if (beltStatusData.passengerBeltDeployedCheckBox.IsChecked == true)
            {
                beltStatus.passengerBeltDeployed = (VehicleDataEventStatus)beltStatusData.passengerBeltDeployedComboBox.SelectedIndex;
            }
            if (beltStatusData.passengerBuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.passengerBuckleBelted = (VehicleDataEventStatus)beltStatusData.passengerBuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.driverBuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.driverBuckleBelted = (VehicleDataEventStatus)beltStatusData.driverBuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.leftRow2BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.leftRow2BuckleBelted = (VehicleDataEventStatus)beltStatusData.leftRow2BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.passengerChildDetectedCheckBox.IsChecked == true)
            {
                beltStatus.passengerChildDetected = (VehicleDataEventStatus)beltStatusData.passengerChildDetectedComboBox.SelectedIndex;
            }
            if (beltStatusData.rightRow2BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.rightRow2BuckleBelted = (VehicleDataEventStatus)beltStatusData.rightRow2BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.middleRow2BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.middleRow2BuckleBelted = (VehicleDataEventStatus)beltStatusData.middleRow2BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.middleRow3BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.middleRow3BuckleBelted = (VehicleDataEventStatus)beltStatusData.middleRow3BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.leftRow3BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.leftRow3BuckleBelted = (VehicleDataEventStatus)beltStatusData.leftRow3BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.rightRow3BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.rightRow3BuckleBelted = (VehicleDataEventStatus)beltStatusData.rightRow3BuckleBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.leftRearInflatableBeltedCheckBox.IsChecked == true)
            {
                beltStatus.leftRearInflatableBelted = (VehicleDataEventStatus)beltStatusData.leftRearInflatableBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.rightRearInflatableBeltedCheckBox.IsChecked == true)
            {
                beltStatus.rightRearInflatableBelted = (VehicleDataEventStatus)beltStatusData.rightRearInflatableBeltedComboBox.SelectedIndex;
            }
            if (beltStatusData.middleRow1BeltDeployedCheckBox.IsChecked == true)
            {
                beltStatus.middleRow1BeltDeployed = (VehicleDataEventStatus)beltStatusData.middleRow1BeltDeployedComboBox.SelectedIndex;
            }
            if (beltStatusData.middleRow1BuckleBeltedCheckBox.IsChecked == true)
            {
                beltStatus.middleRow1BuckleBelted = (VehicleDataEventStatus)beltStatusData.middleRow1BuckleBeltedComboBox.SelectedIndex;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();

        }

        private void BodyInformationCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            BodyInformationButton.IsEnabled = (bool)BodyInformationCheckBox.IsChecked;
        }

        private async void BodyInformationTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var bodyinformation = new BodyInformation(bodyInformation);

            ContentDialog bodyInformationCD = new ContentDialog();
            bodyInformationCD.Content = bodyinformation;

            bodyInformationCD.PrimaryButtonText = "Ok";
            bodyInformationCD.PrimaryButtonClick += BodyInformationCD_PrimaryButtonClick;

            bodyInformationCD.SecondaryButtonText = "Cancel";
            bodyInformationCD.SecondaryButtonClick += BodyInformationCD_SecondaryButtonClick;

            await bodyInformationCD.ShowAsync();
        }

        private async void BodyInformationCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void BodyInformationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var bodyInformationData = sender.Content as BodyInformation;
            bodyInformation = new HmiApiLib.Common.Structs.BodyInformation();
            if (bodyInformationData.parkBrakeArchiveCheckBox.IsChecked == true)
            {
                bodyInformation.parkBrakeActive = (bool)bodyInformationData.parkBrakeArchiveToggle.IsOn;
            }

            if (bodyInformationData.ignitionStableStatusCheckBox.IsChecked == true)
            {
                bodyInformation.ignitionStableStatus = (IgnitionStableStatus)bodyInformationData.ignitionStableStatusComboBox.SelectedIndex;
            }

            if (bodyInformationData.ignitionStatusCheckBox.IsChecked == true)
            {
                bodyInformation.ignitionStatus = (IgnitionStatus)bodyInformationData.ignitionStatusComboBox.SelectedIndex;
            }
            if (bodyInformationData.driverDoorAjarCheckBox.IsChecked == true)
            {
                bodyInformation.driverDoorAjar = (bool)bodyInformationData.driverDoorAjarToggle.IsOn;
            }
            if (bodyInformationData.passengerDoorAjarCheckBox.IsChecked == true)
            {
                bodyInformation.passengerDoorAjar = (bool)bodyInformationData.passengerDoorAjarToggle.IsOn;
            }
            if (bodyInformationData.rearLeftDoorAjarCheckBox.IsChecked == true)
            {
                bodyInformation.rearLeftDoorAjar = (bool)bodyInformationData.rearLeftDoorAjarToggle.IsOn;
            }
            if (bodyInformationData.rearRightDoorAjarCheckBox.IsChecked == true)
            {
                bodyInformation.rearRightDoorAjar = (bool)bodyInformationData.rearRightDoorAjarToggle.IsOn;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void DeviceStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceStatusButton.IsEnabled = (bool)DeviceStatusCheckBox.IsChecked;
        }

        private async void DeviceStatusTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var devicestatus = new DeviceStatus(deviceStatus);

            ContentDialog deviceStatusCD = new ContentDialog();
            deviceStatusCD.Content = devicestatus;

            deviceStatusCD.PrimaryButtonText = "Ok";
            deviceStatusCD.PrimaryButtonClick += DeviceStatusCD_PrimaryButtonClick;

            deviceStatusCD.SecondaryButtonText = "Cancel";
            deviceStatusCD.SecondaryButtonClick += DeviceStatusCD_SecondaryButtonClick;

            await deviceStatusCD.ShowAsync();
        }

        private async void DeviceStatusCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void DeviceStatusCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var deviceStatusData = sender.Content as DeviceStatus;
            deviceStatus = new HmiApiLib.Common.Structs.DeviceStatus();
            if (deviceStatusData.voiceRecOnCheckBox.IsChecked == true)
                deviceStatus.voiceRecOn = (bool)deviceStatusData.voiceRecOnToggle.IsOn;
            if (deviceStatusData.btIconONCheckBox.IsChecked == true)
                deviceStatus.btIconOn = (bool)deviceStatusData.btIconONToggle.IsOn;
            if (deviceStatusData.callActiveCheckBox.IsChecked == true)
                deviceStatus.callActive = (bool)deviceStatusData.callActiveToggle.IsOn;
            if (deviceStatusData.phoneRoamingCheckBox.IsChecked == true)
                deviceStatus.phoneRoaming = (bool)deviceStatusData.phoneRoamingToggle.IsOn;
            if (deviceStatusData.textMsgAvailableCheckBox.IsChecked == true)
                deviceStatus.textMsgAvailable = (bool)deviceStatusData.textMsgAvailableToggle.IsOn;


            if (deviceStatusData.battLevelStatusCheckBox.IsChecked == true)
            {
                deviceStatus.battLevelStatus = (DeviceLevelStatus)deviceStatusData.battLevelStatusComboBox.SelectedIndex;
            }
            if (deviceStatusData.stereoAudioOutputMutedCheckBox.IsChecked == true)
                deviceStatus.stereoAudioOutputMuted = (bool)deviceStatusData.stereoAudioOutputMutedToggle.IsOn;
            if (deviceStatusData.monoAudioOutputMutedCheckBox.IsChecked == true)
                deviceStatus.monoAudioOutputMuted = (bool)deviceStatusData.monoAudioOutputMutedToggle.IsOn;

            if (deviceStatusData.signalLevelStatusCheckBox.IsChecked == true)
            {
                deviceStatus.signalLevelStatus = (DeviceLevelStatus)deviceStatusData.signalLevelStatusComboBox.SelectedIndex;
            }

            if (deviceStatusData.primaryAudioSourceCheckBox.IsChecked == true)
            {
                deviceStatus.primaryAudioSource = (PrimaryAudioSource)deviceStatusData.primaryAudioSourcesComboBox.SelectedIndex;
            }
            if (deviceStatusData.eCallEventActiveCheckBox.IsChecked == true)
                deviceStatus.eCallEventActive = (bool)deviceStatusData.eCallEventActiveToggle.IsOn;
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void DriverBrakingCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DriverBrakingComboBox.IsEnabled = (bool)DriverBrakingCheckBox.IsChecked;
        }

        private void WiperStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WiperStatusComboBox.IsEnabled = (bool)WiperStatusCheckBox.IsChecked;
        }

        private void HeadLampStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            HeadLampStatusButton.IsEnabled = (bool)HeadLampStatusCheckBox.IsChecked;
        }

        private async void HeadLampStatusTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var headlampStatus = new HeadLamp(headLampStatus);

            ContentDialog headLampStatusCD = new ContentDialog();
            headLampStatusCD.Content = headlampStatus;

            headLampStatusCD.PrimaryButtonText = "Ok";
            headLampStatusCD.PrimaryButtonClick += HeadLampStatusCD_PrimaryButtonClick;

            headLampStatusCD.SecondaryButtonText = "Cancel";
            headLampStatusCD.SecondaryButtonClick += HeadLampStatusCD_SecondaryButtonClick;

            await headLampStatusCD.ShowAsync();
        }

        private async void HeadLampStatusCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void HeadLampStatusCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var headLampStatusData = sender.Content as HeadLamp;
            headLampStatus = new HeadLampStatus();
            if (headLampStatusData.lowBeamOnCheckBox.IsChecked == true)
                headLampStatus.lowBeamsOn = (bool)headLampStatusData.lowBeamOnToggle.IsOn;
            if (headLampStatusData.highBeamONCheckBox.IsChecked == true)
                headLampStatus.highBeamsOn = (bool)headLampStatusData.highBeamONToggle.IsOn;

            if (headLampStatusData.ambientLightSensorStatusCheckBox.IsChecked == true)
                headLampStatus.ambientLightSensorStatus = (AmbientLightStatus)headLampStatusData.ambientLightSensorStatusComboBox.SelectedIndex;
            
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void EngineTorqueCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineTorqueTextBox.IsEnabled = (bool)EngineTorqueCheckBox.IsChecked;
        }

        private void AccPadelPositionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AccPedalPositionTextBox.IsEnabled = (bool)AccPadelPositionCheckBox.IsChecked;
        }

        private void SteeringWheelAngleCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            SteeringWheelAngleTextBox.IsEnabled = (bool)SteeringWheelAngleCheckBox.IsChecked;
        }

        private void EcallInfoCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EcallInfoButton.IsEnabled = (bool)EcallInfoCheckBox.IsChecked;
        }

        private async void EcallInfoTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var ecallinfo = new ECallInfo(ecallInfo);

            ContentDialog ecallInfoCD = new ContentDialog();
            ecallInfoCD.Content = ecallinfo;

            ecallInfoCD.PrimaryButtonText = "Ok";
            ecallInfoCD.PrimaryButtonClick += EcallInfoCD_PrimaryButtonClick;

            ecallInfoCD.SecondaryButtonText = "Cancel";
            ecallInfoCD.SecondaryButtonClick += EcallInfoCD_SecondaryButtonClick;

            await ecallInfoCD.ShowAsync();
        }

        private async void EcallInfoCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void EcallInfoCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var ecallInfoData = sender.Content as ECallInfo;
            ecallInfo = new HmiApiLib.Common.Structs.ECallInfo();
            if (ecallInfoData.eCallNotificationStatusCheckBox.IsChecked == true)
            {
                ecallInfo.eCallNotificationStatus = (VehicleDataNotificationStatus)ecallInfoData.eCallNotificationStatusComboBox.SelectedIndex;
            }
            if (ecallInfoData.auxECallNotificationStatusCheckBox.IsChecked == true)
            {
                ecallInfo.auxECallNotificationStatus = (VehicleDataNotificationStatus)ecallInfoData.auxECallNotificationStatusComboBox.SelectedIndex;
            }
            if (ecallInfoData.eCallConfirmationStatusCheckBox.IsChecked == true)
            {
                ecallInfo.eCallConfirmationStatus = (ECallConfirmationStatus)ecallInfoData.eCallConfirmationStatusComboBox.SelectedIndex;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void AirbagStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AirbagStatusButton.IsEnabled = (bool)AirbagStatusCheckBox.IsChecked;
        }

        private async void AirbagStatusTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var airbagstatus = new AirbagStatus(airbagStatus);

            ContentDialog airbagStatusCD = new ContentDialog();
            airbagStatusCD.Content = airbagstatus;

            airbagStatusCD.PrimaryButtonText = "Ok";
            airbagStatusCD.PrimaryButtonClick += AirbagStatusCD_PrimaryButtonClick;

            airbagStatusCD.SecondaryButtonText = "Cancel";
            airbagStatusCD.SecondaryButtonClick += AirbagStatusCD_SecondaryButtonClick;

            await airbagStatusCD.ShowAsync();
        }

        private async void AirbagStatusCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void AirbagStatusCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var airbagStatusData = sender.Content as AirbagStatus;
            airbagStatus = new HmiApiLib.Common.Structs.AirbagStatus();
            if (airbagStatusData.driverAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.driverAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.driverAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.driverSideAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.driverSideAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.driverSideAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.driverCurtainAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.driverCurtainAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.driverCurtainAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.passengerAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.passengerAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.passengerAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.passengerCurtainAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.passengerCurtainAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.passengerCurtainAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.driverKneeAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.driverKneeAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.driverKneeAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.passengerSideAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.passengerSideAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.passengerSideAirbagDeployedComboBox.SelectedIndex;
            }
            if (airbagStatusData.passengerKneeAirbagDeployedCheckBox.IsChecked == true)
            {
                airbagStatus.passengerKneeAirbagDeployed = (VehicleDataEventStatus)airbagStatusData.passengerKneeAirbagDeployedComboBox.SelectedIndex;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void EmergencyEventCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EmergencyEventButton.IsEnabled = (bool)EmergencyEventCheckBox.IsChecked;
        }

        private async void EmergencyEventTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var emergencyevent = new EmergencyEvent(emergencyEvent);

            ContentDialog emergencyEventCD = new ContentDialog();
            emergencyEventCD.Content = emergencyevent;

            emergencyEventCD.PrimaryButtonText = "Ok";
            emergencyEventCD.PrimaryButtonClick += EmergencyEventCD_PrimaryButtonClick;

            emergencyEventCD.SecondaryButtonText = "Cancel";
            emergencyEventCD.SecondaryButtonClick += EmergencyEventCD_SecondaryButtonClick;

            await emergencyEventCD.ShowAsync();
        }

        private async void EmergencyEventCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void EmergencyEventCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var emergencyEventData = sender.Content as EmergencyEvent;

            emergencyEvent = new HmiApiLib.Common.Structs.EmergencyEvent();
            if (emergencyEventData.emergencyEventTypeCheckBox.IsChecked == true)
            {
                emergencyEvent.emergencyEventType = (EmergencyEventType)emergencyEventData.emergencyEventTypeComboBox.SelectedIndex;
            }
            if (emergencyEventData.fuelCutoffStatusCheckBox.IsChecked == true)
            {
                emergencyEvent.fuelCutoffStatus = (FuelCutoffStatus)emergencyEventData.fuelCutoffStatusComboBox.SelectedIndex;
            }
            if (emergencyEventData.rolloverEventCheckBox.IsChecked == true)
            {
                emergencyEvent.rolloverEvent = (VehicleDataEventStatus)emergencyEventData.rolloverEventComboBox.SelectedIndex;
            }
            if (emergencyEventData.maximumChangeVelocityCheckBox.IsChecked == true)
            {
                emergencyEvent.maximumChangeVelocity = (VehicleDataEventStatus)emergencyEventData.maximumChangeVelocityComboBox.SelectedIndex;
            }
            if (emergencyEventData.multipleEventsCheckBox.IsChecked == true)
            {
                emergencyEvent.multipleEvents = (VehicleDataEventStatus)emergencyEventData.multipleEventsComboBox.SelectedIndex;
            }

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void ClusterModesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ClusterModesButton.IsEnabled = (bool)ClusterModesCheckBox.IsChecked;
        }

        private async void ClusterModesTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var clustermodes = new ClusterModes(clusterModes);

            ContentDialog clusterModesCD = new ContentDialog();
            clusterModesCD.Content = clustermodes;

            clusterModesCD.PrimaryButtonText = "Ok";
            clusterModesCD.PrimaryButtonClick += ClusterModesCD_PrimaryButtonClick;

            clusterModesCD.SecondaryButtonText = "Cancel";
            clusterModesCD.SecondaryButtonClick += ClusterModesCD_SecondaryButtonClick;

            await clusterModesCD.ShowAsync();
        }

        private async void ClusterModesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void ClusterModesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var clusterModesData = sender.Content as ClusterModes;

            clusterModes = new ClusterModeStatus();
            clusterModes.powerModeActive = (bool)clusterModesData.powerModeActiveCheckBox.IsChecked;

            if (clusterModesData.powerModeActiveCheckBox.IsChecked == true)
                clusterModes.powerModeActive = (bool)clusterModesData.powerModeActiveToggle.IsOn;

            if (clusterModesData.powerModeQualificationStatusCheckBox.IsChecked == true)
            {
                clusterModes.powerModeQualificationStatus = (PowerModeQualificationStatus)clusterModesData.powerModeQualificationStatusComboBox.SelectedIndex;
            }
            if (clusterModesData.carModeStatusCheckBox.IsChecked == true)
            {
                clusterModes.carModeStatus = (CarModeStatus)clusterModesData.carModeStatusComboBox.SelectedIndex;
            }
            if (clusterModesData.powerModeStatusCheckBox.IsChecked == true)
            {
                clusterModes.powerModeStatus = (PowerModeStatus)clusterModesData.powerModeStatusComboBox.SelectedIndex;
            }


            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private void MyKeyCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MyKeyComboBox.IsEnabled = (bool)MyKeyCheckBox.IsChecked;
        }

        private void ElectronicParkBrakeStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ElectronicParkBrakeStatusComboBox.IsEnabled = (bool)ElectronicParkBrakeStatusCheckBox.IsChecked;
        }

        private void EngineOilLifeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EngineOilLifeTextBox.IsEnabled = (bool)EngineOilLifeCheckBox.IsChecked;
        }

        private void AddFuelRangeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddFuelRangeButton.IsEnabled = (bool)AddFuelRangeCheckBox.IsChecked;
        }

        private async void AddFuelRangeTapped(object sender, TappedRoutedEventArgs e)
        {
            viOnVehicleDataCD.Hide();

            var addFuelRange = new AddFuelRange();

            ContentDialog addFuelRangeCD = new ContentDialog();
            addFuelRangeCD.Content = addFuelRange;

            addFuelRangeCD.PrimaryButtonText = "Ok";
            addFuelRangeCD.PrimaryButtonClick += AddFuelRangeCD_PrimaryButtonClick;

            addFuelRangeCD.SecondaryButtonText = "Cancel";
            addFuelRangeCD.SecondaryButtonClick += AddFuelRangeCD_SecondaryButtonClick;

            await addFuelRangeCD.ShowAsync();
        }

        private async void AddFuelRangeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }

        private async void AddFuelRangeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addFuelRange = sender.Content as AddFuelRange;

            FuelRange fuelRangeData = new FuelRange();

            if (addFuelRange.fuelTypeCheckBox.IsChecked == true)
                fuelRangeData.type = (FuelType)addFuelRange.fuelTypeComboBox.SelectedIndex;

            if (addFuelRange.rangeCheckBox.IsChecked == true && addFuelRange.rangeTextBox.Text != "")
            {
                try
                {
                    fuelRangeData.range = int.Parse(addFuelRange.rangeTextBox.Text);
                }
                catch
                {
                    fuelRangeData.range = 0;
                }
            }

            fuelRangeList.Add(fuelRangeData);

            sender.Hide();
            await viOnVehicleDataCD.ShowAsync();
        }
    }
}

