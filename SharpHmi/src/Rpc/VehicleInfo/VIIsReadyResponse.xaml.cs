﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class VIIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;
        public VIIsReadyResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            ContentDialog viIsReadyCD = new ContentDialog();

            viIsReadyCD.Content = this;

            viIsReadyCD.PrimaryButtonText = Const.TxLater;
            viIsReadyCD.PrimaryButtonClick += ViIsReadyCD_PrimaryButtonClick;

            viIsReadyCD.SecondaryButtonText = Const.Reset;
            viIsReadyCD.SecondaryButtonClick += ViIsReadyCD_ResetButtonClick;

            viIsReadyCD.CloseButtonText = Const.Close;
            viIsReadyCD.CloseButtonClick += ViIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());

            if(tmpObj == null)
            {
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(typeof(IsReady), 0);
            }
            if(tmpObj != null)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                AvailableCheckBox.IsChecked = tmpObj.getAvailable();
                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableToggle.IsEnabled = false;
                }
                else
                {
                    AvailableCheckBox.IsChecked = true;
                    AvailableToggle.IsOn = (bool)tmpObj.getAvailable();
                }
            }

            await viIsReadyCD.ShowAsync();
        }

        private void ViIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? result = null;

            if(ResultCodeCheckBox.IsChecked == true)
            {
                result = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableToggle.IsOn;
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.VehicleInfo, toggle, result);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (tmpObj != null)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (ResultCodeCheckBox.IsChecked == true)
            {
                ResultCodeComboBox.IsEnabled = true;
            }
            else
            {
                ResultCodeComboBox.IsEnabled = false;
            }
        }

        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableToggle.IsEnabled = (bool)AvailableCheckBox.IsChecked;
        }
    }
}
