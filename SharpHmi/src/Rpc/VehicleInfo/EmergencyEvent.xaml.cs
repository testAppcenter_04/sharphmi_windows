﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class EmergencyEvent : UserControl
    {

        public CheckBox emergencyEventTypeCheckBox;
        public ComboBox emergencyEventTypeComboBox;

        public CheckBox fuelCutoffStatusCheckBox;
        public ComboBox fuelCutoffStatusComboBox;

        public CheckBox rolloverEventCheckBox;
        public ComboBox rolloverEventComboBox;

        public CheckBox maximumChangeVelocityCheckBox;
        public ComboBox maximumChangeVelocityComboBox;

        public CheckBox multipleEventsCheckBox;
        public ComboBox multipleEventsComboBox;

        public EmergencyEvent(HmiApiLib.Common.Structs.EmergencyEvent emergencyEvent)
        {
            InitializeComponent();

            EmergencyEventTypeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.EmergencyEventType));
            EmergencyEventTypeComboBox.SelectedIndex = 0;

            FuelCutoffStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.FuelCutoffStatus));
            FuelCutoffStatusComboBox.SelectedIndex = 0;

            RolloverEventComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            RolloverEventComboBox.SelectedIndex = 0;

            MaximumChangeVelocityComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MaximumChangeVelocityComboBox.SelectedIndex = 0;

            MultipleEventsComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VehicleDataEventStatus));
            MultipleEventsComboBox.SelectedIndex = 0;

            emergencyEventTypeCheckBox = EmergencyEventTypeCheckBox;
            emergencyEventTypeComboBox = EmergencyEventTypeComboBox;

            fuelCutoffStatusCheckBox = FuelCutoffStatusCheckBox;
            fuelCutoffStatusComboBox = FuelCutoffStatusComboBox;

            rolloverEventCheckBox = RolloverEventCheckBox;
            rolloverEventComboBox = RolloverEventComboBox;

            maximumChangeVelocityCheckBox = MaximumChangeVelocityCheckBox;
            maximumChangeVelocityComboBox = MaximumChangeVelocityComboBox;

            multipleEventsCheckBox = MultipleEventsCheckBox;
            multipleEventsComboBox = MultipleEventsComboBox;

            if (null != emergencyEvent)
            {   if (emergencyEvent.getEmergencyEventType() != null)
                {
                    EmergencyEventTypeComboBox.SelectedIndex = (int)emergencyEvent.getEmergencyEventType();
                    EmergencyEventTypeCheckBox.IsChecked = true;

                }
                else
                {
                    EmergencyEventTypeComboBox.IsEnabled = false;
                }
                if (emergencyEvent.getFuelCutoffStatus() != null)
                {
                    FuelCutoffStatusComboBox.SelectedIndex = (int)emergencyEvent.getFuelCutoffStatus();
                    FuelCutoffStatusCheckBox.IsChecked = true;

                }
                else
                {
                    FuelCutoffStatusComboBox.IsEnabled = false;
                }
                if (emergencyEvent.getRolloverEvent() != null)
                {
                    RolloverEventComboBox.SelectedIndex = (int)emergencyEvent.getRolloverEvent();
                    RolloverEventCheckBox.IsChecked = true;

                }
                else
                {
                    RolloverEventComboBox.IsEnabled = false;
                }
                if (emergencyEvent.getMaximumChangeVelocity() != null)
                {
                    MaximumChangeVelocityComboBox.SelectedIndex = (int)emergencyEvent.getMaximumChangeVelocity();
                    MaximumChangeVelocityCheckBox.IsChecked = true;

                }
                else
                {
                    MaximumChangeVelocityComboBox.IsEnabled = false;
                }
                if (emergencyEvent.getMultipleEvents() != null)
                {
                    MultipleEventsComboBox.SelectedIndex = (int)emergencyEvent.getMultipleEvents();
                    MultipleEventsCheckBox.IsChecked = true;

                }
                else
                {
                    MultipleEventsComboBox.IsEnabled = false;

                }
            }
            
    }

    private void EmergencyEventTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EmergencyEventTypeComboBox.IsEnabled = (bool)EmergencyEventTypeCheckBox.IsChecked;
        }

        private void FuelCutoffStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FuelCutoffStatusComboBox.IsEnabled = (bool)FuelCutoffStatusCheckBox.IsChecked;
        }

        private void RolloverEventCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RolloverEventComboBox.IsEnabled = (bool)RolloverEventCheckBox.IsChecked;
        }

        private void MaximumChangeVelocityCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MaximumChangeVelocityComboBox.IsEnabled = (bool)MaximumChangeVelocityCheckBox.IsChecked;
        }

        private void MultipleEventsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            MultipleEventsComboBox.IsEnabled = (bool)MultipleEventsCheckBox.IsChecked;
        }
    }
}
