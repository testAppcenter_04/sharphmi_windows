﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class ClusterModes : UserControl
    {
        public CheckBox powerModeActiveCheckBox;
        public ToggleSwitch powerModeActiveToggle;

        public CheckBox powerModeQualificationStatusCheckBox;
        public ComboBox powerModeQualificationStatusComboBox;

        public CheckBox carModeStatusCheckBox;
        public ComboBox carModeStatusComboBox;

        public CheckBox powerModeStatusCheckBox;
        public ComboBox powerModeStatusComboBox;

        public ClusterModes(HmiApiLib.Common.Structs.ClusterModeStatus clusterModes)
        {
            InitializeComponent();

            PowerModeQualificationStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.PowerModeQualificationStatus));
            PowerModeQualificationStatusComboBox.SelectedIndex = 0;

            CarModeStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.CarModeStatus));
            CarModeStatusComboBox.SelectedIndex = 0;

            PowerModeStatusComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.PowerModeStatus));
            PowerModeStatusComboBox.SelectedIndex = 0;

            powerModeActiveCheckBox = PowerModeActiveCheckBox;

            powerModeQualificationStatusCheckBox = PowerModeQualificationStatusCheckBox;
            powerModeQualificationStatusComboBox = PowerModeQualificationStatusComboBox;
            powerModeActiveToggle = PowerModeActiveToggle;
            carModeStatusCheckBox = CarModeStatusCheckBox;
            carModeStatusComboBox = CarModeStatusComboBox;

            powerModeStatusCheckBox = PowerModeStatusCheckBox;
            powerModeStatusComboBox = PowerModeStatusComboBox;

            if (null != clusterModes)
            {
                if (clusterModes.getPowerModeActive() == null)
                {
                    PowerModeActiveToggle.IsEnabled = false;
                }
                else
                {
                    PowerModeActiveCheckBox.IsChecked = true;
                    PowerModeActiveToggle.IsOn = (bool)clusterModes.getPowerModeActive();

                }
                if (clusterModes.getPowerModeQualificationStatus() != null)
                {
                    PowerModeQualificationStatusComboBox.SelectedIndex = (int)clusterModes.getPowerModeQualificationStatus();
                    PowerModeQualificationStatusCheckBox.IsChecked = true;

                }
                else
                {
                    PowerModeQualificationStatusComboBox.IsEnabled = false;

                }
                if (clusterModes.getCarModeStatus() != null)
                {
                    CarModeStatusComboBox.SelectedIndex = (int)clusterModes.getCarModeStatus();
                    CarModeStatusCheckBox.IsChecked = true;

                }
                else
                {
                    CarModeStatusComboBox.IsEnabled = false;

                }
                if (clusterModes.getPowerModeStatus() != null)
                {
                    PowerModeStatusComboBox.SelectedIndex = (int)clusterModes.getPowerModeStatus();
                    PowerModeStatusCheckBox.IsChecked = true;

                }
                else
                {
                    PowerModeStatusComboBox.IsEnabled = false;

                }
            }
        }

        private void PowerModeQualificationStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PowerModeQualificationStatusComboBox.IsEnabled = (bool)PowerModeQualificationStatusCheckBox.IsChecked;
        }

        private void CarModeStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CarModeStatusComboBox.IsEnabled = (bool)CarModeStatusCheckBox.IsChecked;
        }

        private void PowerModeStatusCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PowerModeStatusComboBox.IsEnabled = (bool)PowerModeStatusCheckBox.IsChecked;
        }

        private void PowerModeActiveCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            PowerModeActiveToggle.IsEnabled = (bool)PowerModeActiveCheckBox.IsChecked;

        }
    }
}