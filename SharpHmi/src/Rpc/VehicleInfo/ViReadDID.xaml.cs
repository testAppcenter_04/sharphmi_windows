﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.VehicleInfo.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VehicleInfo
{
    public sealed partial class ViReadDID : UserControl
    {
        private ObservableCollection<DIDResult> didResultList = new ObservableCollection<DIDResult>();
        ReadDID tmpObj = null;
        ContentDialog viReadDidCD = new ContentDialog();

        public ViReadDID()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;

        }

        public async void ShowViReadDid()
        {
            viReadDidCD.Content = this;

            viReadDidCD.PrimaryButtonText = Const.TxLater;
            viReadDidCD.PrimaryButtonClick += ViReadDidCD_PrimaryButtonClick;

            viReadDidCD.SecondaryButtonText = Const.Reset;
            viReadDidCD.SecondaryButtonClick += ViReadDidCD_ResetButtonClick;

            viReadDidCD.CloseButtonText = Const.Close;
            viReadDidCD.CloseButtonClick += ViReadDidCD_CloseButtonClick;

            DidResultListView.ItemsSource = didResultList;

            tmpObj = new ReadDID();
            tmpObj = (ReadDID)AppUtils.getSavedPreferenceValueForRpc<ReadDID>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ReadDID);
                tmpObj = (ReadDID)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();

                List<DIDResult> didResult = tmpObj.getDidResult();


                if (didResult != null)
                {
                    foreach (DIDResult tmp in didResult)
                    {
                        didResultList.Add(tmp);
                    }
                }
                else {
                    AddDidResultCheckBox.IsChecked = false;
                    AddDidResultButton.IsEnabled = false;
                }
            }

            await viReadDidCD.ShowAsync();
        }

        private void ViReadDidCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            List<DIDResult> didResult = null;
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            if (AddDidResultCheckBox.IsChecked == true)
            {
                didResult = new List<DIDResult>();
                didResult.AddRange(didResultList);
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVehicleInfoReadDIDResponse(
                BuildRpc.getNextId(), resultCode, didResult);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void ViReadDidCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ViReadDidCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AddDidResultCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddDidResultButton.IsEnabled = (bool)AddDidResultCheckBox.IsChecked;
        }

        private async void AddDidResultButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            viReadDidCD.Hide();

            ContentDialog addDIDResultsCD = new ContentDialog();

            var addTextFields = new AddDIDResult();
            addDIDResultsCD.Content = addTextFields;

            addDIDResultsCD.PrimaryButtonText = Const.OK;
            addDIDResultsCD.PrimaryButtonClick += AddDIDResultCD_PrimaryButtonClick;

            addDIDResultsCD.SecondaryButtonText = Const.Close;
            addDIDResultsCD.SecondaryButtonClick += AddDIDResultCD_SecondaryButtonClick;

            await addDIDResultsCD.ShowAsync();
        }

        private async void AddDIDResultCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addDidResult = sender.Content as AddDIDResult;
            DIDResult dIDResult = new DIDResult();
            if (addDidResult.resultCodeCB.IsChecked == true)
                dIDResult.resultCode = (VehicleDataResultCode)addDidResult.resultCombobox.SelectedIndex;
            if (addDidResult.didLocationCB.IsChecked == true)
            {
                try
                {
                    if (null != addDidResult.didLocationTB.Text)
                    {
                        dIDResult.didLocation = Int32.Parse(addDidResult.didLocationTB.Text.ToString());
                    }
                }
                catch (Exception e)
                {

                }
            }
            if (addDidResult.dataCB.IsChecked == true)
                if (null != addDidResult.dataTB.Text)
                    dIDResult.data = addDidResult.dataTB.Text.ToString();

            didResultList.Add(dIDResult);

            sender.Hide();
            await viReadDidCD.ShowAsync();
        }

        private async void AddDIDResultCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await viReadDidCD.ShowAsync();
        }
    }
}
