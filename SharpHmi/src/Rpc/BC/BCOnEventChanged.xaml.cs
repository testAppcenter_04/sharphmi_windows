﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnEventChanged : UserControl
    {
        OnEventChanged tmpObj = null;
        public BCOnEventChanged()
        {
            InitializeComponent();
            EventTypesComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            EventTypesComboBox.SelectedIndex = 0;
        }

        public async void ShowOnEventChanged()
        {
            ContentDialog bcOnEventChangedCD = new ContentDialog();
            bcOnEventChangedCD.Content = this;

            bcOnEventChangedCD.PrimaryButtonText = Const.TxNow;
            bcOnEventChangedCD.PrimaryButtonClick += BcOnEventChangedCD_PrimaryButtonClick;

            bcOnEventChangedCD.SecondaryButtonText = Const.Reset;
            bcOnEventChangedCD.SecondaryButtonClick += BcOnEventChangedCD_SecondaryButtonClick;

            bcOnEventChangedCD.CloseButtonText = Const.Close;
            bcOnEventChangedCD.CloseButtonClick += BcOnEventChanged_CloseButtonClick;

            tmpObj = new OnEventChanged();
            tmpObj = (OnEventChanged)AppUtils.getSavedPreferenceValueForRpc<OnEventChanged>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (tmpObj.getActive() == null)
                {
                    IsActiveCheckBox.IsChecked = false;
                    IsActiveTgl.IsEnabled = false;
                }
                else
                {
                    IsActiveTgl.IsOn = (bool)tmpObj.getActive();
                }

                if (tmpObj.getEventName() != null)
                    EventTypesComboBox.SelectedIndex = (int)tmpObj.getEventName();
                else
                {
                    EventTypesCheckBox.IsChecked = false;
                    EventTypesComboBox.IsEnabled = false;
                }
            }

            await bcOnEventChangedCD.ShowAsync();
        }
       
        private void BcOnEventChangedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            EventTypes? eventTypes = null;
            if (EventTypesCheckBox.IsChecked == true)
            {
                eventTypes = (EventTypes)EventTypesComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (IsActiveCheckBox.IsChecked == true)
            { 
                toggle = IsActiveTgl.IsOn;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = 
                BuildRpc.buildBasicCommunicationOnEventChangedNotification(eventTypes, toggle);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnEventChangedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }


        private void EventTypesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            EventTypesComboBox.IsEnabled = (bool)EventTypesCheckBox.IsChecked;
        }

        private void BcOnEventChanged_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void IsActiveCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IsActiveTgl.IsEnabled = (bool)IsActiveCheckBox.IsChecked;
        }
    }
}
