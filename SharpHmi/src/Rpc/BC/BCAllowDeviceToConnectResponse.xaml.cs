﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCAllowDeviceToConnectResponse : UserControl
    {
        AllowDeviceToConnect tmpObj = null;
        public BCAllowDeviceToConnectResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowAllowDeviceToConnect()
        {
            ContentDialog bcAllowDeviceToConnectCD = new ContentDialog();

            bcAllowDeviceToConnectCD.Content = this;

            bcAllowDeviceToConnectCD.PrimaryButtonText = Const.TxLater;
            bcAllowDeviceToConnectCD.PrimaryButtonClick += BcAllowDeviceToConnectCD_PrimaryButtonClick;

            bcAllowDeviceToConnectCD.SecondaryButtonText = Const.Reset;
            bcAllowDeviceToConnectCD.SecondaryButtonClick += BcAllowDeviceToConnectCD_SecondaryButtonClick;

            bcAllowDeviceToConnectCD.CloseButtonText = Const.Close;
            bcAllowDeviceToConnectCD.CloseButtonClick += BcAllowDeviceToConnectCD_CloseButtonClick;

            tmpObj = new AllowDeviceToConnect();
            tmpObj = (AllowDeviceToConnect)AppUtils.getSavedPreferenceValueForRpc<AllowDeviceToConnect>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(AllowDeviceToConnect);
                tmpObj = (AllowDeviceToConnect)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getAllow() == null)
                {
                    AllowCheckBox.IsChecked = false;
                    AllowToggle.IsEnabled = false;
                }
                else
                {
                    AllowCheckBox.IsChecked = true;
                    AllowToggle.IsOn = (bool)tmpObj.getAllow();
                }

                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcAllowDeviceToConnectCD.ShowAsync();
        }
             

        private void BcAllowDeviceToConnectCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if(AllowCheckBox.IsChecked==true) {
                toggle = AllowToggle.IsOn;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(
                BuildRpc.getNextId(), resultCode,toggle);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcAllowDeviceToConnectCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcAllowDeviceToConnectCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool) ResultCodeCheckBox.IsChecked;
        }

        private void AllowCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AllowToggle.IsEnabled = (bool)AllowCheckBox.IsChecked;
        }
    }
}
