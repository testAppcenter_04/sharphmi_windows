﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnReady : UserControl
    {
        //OnReady tmpObj = null;
        public BCOnReady()
        {
            InitializeComponent();
        }

        public async void ShowOnReady()
        {
            ContentDialog bcOnReadyCD = new ContentDialog();
            bcOnReadyCD.Content = this;

            bcOnReadyCD.PrimaryButtonText = Const.TxNow;
            bcOnReadyCD.PrimaryButtonClick += BcOnReadyCD_PrimaryButtonClick;

            bcOnReadyCD.SecondaryButtonText = Const.Reset;
            bcOnReadyCD.SecondaryButtonClick += BcOnReadyCD_SecondaryButtonClick;

            bcOnReadyCD.CloseButtonText = Const.Close;
            bcOnReadyCD.CloseButtonClick += BcOnReady_CloseButtonClick;

            //OnReady tmpObj = new OnReady();
            //tmpObj = (OnReady)AppUtils.getSavedPreferenceValueForRpc<OnReady>(tmpObj.getMethod());

            //if (null != tmpObj)
            //{

            //}

            await bcOnReadyCD.ShowAsync();
        }

        private static void BcOnReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnReady();
            //AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private static void BcOnReadyCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //if (null != tmpObj)
            //{
            //    AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            //}
        }

        private void BcOnReady_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
