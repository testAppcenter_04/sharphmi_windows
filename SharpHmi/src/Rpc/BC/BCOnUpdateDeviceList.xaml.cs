﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnUpdateDeviceList : UserControl
    {
        //OnUpdateDeviceList tmpObj = null;
        public BCOnUpdateDeviceList()
        {
            InitializeComponent();
        }

        public async void ShowOnUpdateDeviceList()
        {
            ContentDialog bcOnUpdateDeviceListCD = new ContentDialog();
            bcOnUpdateDeviceListCD.Content = this;

            bcOnUpdateDeviceListCD.PrimaryButtonText = Const.TxNow;
            bcOnUpdateDeviceListCD.PrimaryButtonClick += BcOnUpdateDeviceListCD_PrimaryButtonClick;

            bcOnUpdateDeviceListCD.SecondaryButtonText = Const.Reset;
            bcOnUpdateDeviceListCD.SecondaryButtonClick += BcOnUpdateDeviceListCD_SecondaryButtonClick;

            bcOnUpdateDeviceListCD.CloseButtonText = Const.Close;
            bcOnUpdateDeviceListCD.CloseButtonClick += BcOnUpdateDeviceList_CloseButtonClick;

            //tmpObj = new OnUpdateDeviceList();
            //tmpObj = (OnUpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<OnUpdateDeviceList>(tmpObj.getMethod());

            //if (null != tmpObj)
            //{

            //}

            await bcOnUpdateDeviceListCD.ShowAsync();
        }

        private static void BcOnUpdateDeviceListCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnUpdateDeviceList();
            //AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private static void BcOnUpdateDeviceListCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //if (null != tmpObj)
            //{
            //    AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            //}
        }

        private void BcOnUpdateDeviceList_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}