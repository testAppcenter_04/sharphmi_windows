﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCDialNumberResponse : UserControl
    {
        DialNumber tmpObj = null;
        public BCDialNumberResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowDialNumber()
        {
            ContentDialog bcDialNumberCD = new ContentDialog();
            bcDialNumberCD.Content = this;

            bcDialNumberCD.PrimaryButtonText = Const.TxLater;
            bcDialNumberCD.PrimaryButtonClick += BcDialNumberCD_PrimaryButtonClick;

            bcDialNumberCD.SecondaryButtonText = Const.Reset;
            bcDialNumberCD.SecondaryButtonClick += BcDialNumberCD_SecondaryButtonClick;

            bcDialNumberCD.CloseButtonText = Const.Close;
            bcDialNumberCD.CloseButtonClick += BcDialNumberCD_CloseButtonClick;

            tmpObj = new DialNumber();
            tmpObj = (DialNumber)AppUtils.getSavedPreferenceValueForRpc<DialNumber>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(DialNumber);
                tmpObj = (DialNumber)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcDialNumberCD.ShowAsync();
        }

      

        private void BcDialNumberCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationDialNumberResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcDialNumberCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcDialNumberCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}