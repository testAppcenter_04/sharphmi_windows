﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCUpdateDeviceListResponse : UserControl
    {
        UpdateDeviceList tmpObj = null;
        public BCUpdateDeviceListResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowUpdateDeviceList()
        {
            ContentDialog bcUpdateDeviceListCD = new ContentDialog();
            bcUpdateDeviceListCD.Content = this;

            bcUpdateDeviceListCD.PrimaryButtonText = Const.TxLater;
            bcUpdateDeviceListCD.PrimaryButtonClick += BcUpdateDeviceListCD_PrimaryButtonClick;

            bcUpdateDeviceListCD.SecondaryButtonText = Const.Reset;
            bcUpdateDeviceListCD.SecondaryButtonClick += BcUpdateDeviceListCD_SecondaryButtonClick;

            bcUpdateDeviceListCD.CloseButtonText = Const.Close;
            bcUpdateDeviceListCD.CloseButtonClick += BcUpdateDeviceList_CloseButtonClick;

            tmpObj = new UpdateDeviceList();
            tmpObj = (UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<UpdateDeviceList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(UpdateDeviceList);
                tmpObj = (UpdateDeviceList)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcUpdateDeviceListCD.ShowAsync();
        }

        private void BcUpdateDeviceListCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateDeviceListResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcUpdateDeviceListCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool) ResultCodeCheckBox.IsChecked;
        }

        private void BcUpdateDeviceList_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
