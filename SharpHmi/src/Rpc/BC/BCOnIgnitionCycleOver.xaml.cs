﻿using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnIgnitionCycleOver : UserControl
    {
        //OnIgnitionCycleOver tmpObj = null;
        public BCOnIgnitionCycleOver()
        {
            InitializeComponent();
        }

        public async void ShowOnIgnitionCycleOver()
        {
            ContentDialog bcOnIgnitionCycleOverCD = new ContentDialog();
            bcOnIgnitionCycleOverCD.Content = this ;

            bcOnIgnitionCycleOverCD.PrimaryButtonText = Const.TxNow;
            bcOnIgnitionCycleOverCD.PrimaryButtonClick += BcOnIgnitionCycleOverCD_PrimaryButtonClick;

            bcOnIgnitionCycleOverCD.SecondaryButtonText = Const.Reset;
            bcOnIgnitionCycleOverCD.SecondaryButtonClick += BcOnIgnitionCycleOverCD_SecondaryButtonClick;

            bcOnIgnitionCycleOverCD.CloseButtonText = Const.Close;
            bcOnIgnitionCycleOverCD.CloseButtonClick += BcOnIgnitionCycleOver_CloseButtonClick;

            //tmpObj = new OnIgnitionCycleOver();
            //tmpObj = (OnIgnitionCycleOver)AppUtils.getSavedPreferenceValueForRpc<OnIgnitionCycleOver>(tmpObj.getMethod());

            //if (null != tmpObj)
            //{

            //}

            await bcOnIgnitionCycleOverCD.ShowAsync();
        }

        private void BcOnIgnitionCycleOverCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnIgnitionCycleOver();
            //AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnIgnitionCycleOverCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //if (null != tmpObj)
            //{
            //    AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            //}
        }

        private void BcOnIgnitionCycleOver_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
