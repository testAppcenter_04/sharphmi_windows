﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCActivateAppResponse : UserControl
    {
        ActivateApp tmpObj = null;
        public BCActivateAppResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowActivateApp()
        {
            ContentDialog bcActivateAppCD = new ContentDialog();
            bcActivateAppCD.Content = this;

            bcActivateAppCD.PrimaryButtonText = Const.TxLater;
            bcActivateAppCD.PrimaryButtonClick += BcActivateAppCD_PrimaryButtonClick;

            bcActivateAppCD.SecondaryButtonText = Const.Reset;
            bcActivateAppCD.SecondaryButtonClick += BcActivateAppCD_SecondaryButtonClick;

            bcActivateAppCD.CloseButtonText = Const.Close;
            bcActivateAppCD.CloseButtonClick += BcActivateAppCD_CloseButtonClick;

            tmpObj = new ActivateApp();
            tmpObj = (ActivateApp)AppUtils.getSavedPreferenceValueForRpc<ActivateApp>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ActivateApp);
                tmpObj = (ActivateApp)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcActivateAppCD.ShowAsync();
        }
        private void BcActivateAppCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationActivateAppResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcActivateAppCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcActivateAppCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
