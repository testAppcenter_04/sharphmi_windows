﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCSystemRequestResponse : UserControl
    {
        SystemRequest tmpObj = null;
        public BCSystemRequestResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowSystemRequest()
        {
            ContentDialog bcSystemRequestCD = new ContentDialog();
            bcSystemRequestCD.Content = this;

            bcSystemRequestCD.PrimaryButtonText = Const.TxLater;
            bcSystemRequestCD.PrimaryButtonClick += BcSystemRequestCD_PrimaryButtonClick;

            bcSystemRequestCD.SecondaryButtonText = Const.Reset;
            bcSystemRequestCD.SecondaryButtonClick += BcSystemRequestCD_SecondaryButtonClick;

            bcSystemRequestCD.CloseButtonText = Const.Close;
            bcSystemRequestCD.CloseButtonClick += BcSystemRequest_CloseButtonClick;

            tmpObj = new SystemRequest();
            tmpObj = (SystemRequest)AppUtils.getSavedPreferenceValueForRpc<SystemRequest>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(SystemRequest);
                tmpObj = (SystemRequest)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcSystemRequestCD.ShowAsync();
        }

        private void BcSystemRequestCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationSystemRequestResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcSystemRequestCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool) ResultCodeCheckBox.IsChecked;
        }

        private void BcSystemRequest_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
