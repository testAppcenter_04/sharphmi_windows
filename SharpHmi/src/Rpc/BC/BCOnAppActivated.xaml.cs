﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnAppActivated : UserControl
    {
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        OnAppActivated tmpObj = null;
        public BCOnAppActivated()
        {
            InitializeComponent();
            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if(appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnAppActivated()
        {
            ContentDialog bcOnAppActiavtedCD = new ContentDialog();
            bcOnAppActiavtedCD.Content = this;

            bcOnAppActiavtedCD.PrimaryButtonText = Const.TxNow;
            bcOnAppActiavtedCD.PrimaryButtonClick += BcOnAppActiavtedCD_PrimaryButtonClick;

            bcOnAppActiavtedCD.SecondaryButtonText = Const.Reset;
            bcOnAppActiavtedCD.SecondaryButtonClick += BcOnAppActiavtedCD_SecondaryButtonClick;

            bcOnAppActiavtedCD.CloseButtonText = Const.Close;
            bcOnAppActiavtedCD.CloseButtonClick += BcOnAppActiavted_CloseButtonClick;

            tmpObj = new OnAppActivated();
            tmpObj = (OnAppActivated)AppUtils.getSavedPreferenceValueForRpc<OnAppActivated>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnAppActivated);
                tmpObj = (OnAppActivated)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if ((null != tmpObj))
            {
                if (tmpObj.getAppId() != null)
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();

                else
                {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }
            }
            //RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            await bcOnAppActiavtedCD.ShowAsync();
        }

        private void BcOnAppActiavtedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true && ManualAppIdTextBox.Text != null)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch
                    {
                        selectedAppID = null;
                    }
                    
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        Console.WriteLine("Listindex", (RegisteredAppIdComboBox.SelectedIndex));

                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnAppActivated(selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnAppActiavtedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnAppActiavted_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdTextBox != null)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if(RegisteredAppIdComboBox != null)
            {
                if(RegisteredAppIdRadioButton.IsChecked == true)
                {
                    RegisteredAppIdComboBox.IsEnabled = true;
                    ManualAppIdTextBox.IsEnabled = false;
                }
                else
                {
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = true;
                    
                }
            }
        }
    }
}
