﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCUpdateAppListResponse : UserControl
    {
        UpdateAppList tmpObj = null;
        public BCUpdateAppListResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowUpdateAppList()
        {
            ContentDialog bcUpdateAppListCD = new ContentDialog();
            bcUpdateAppListCD.Content = this;

            bcUpdateAppListCD.PrimaryButtonText = Const.TxLater;
            bcUpdateAppListCD.PrimaryButtonClick += BcUpdateAppListCD_PrimaryButtonClick;

            bcUpdateAppListCD.SecondaryButtonText = Const.Reset;
            bcUpdateAppListCD.SecondaryButtonClick += BcUpdateAppListCD_SecondaryButtonClick;

            bcUpdateAppListCD.CloseButtonText = Const.Close;
            bcUpdateAppListCD.CloseButtonClick += BcUpdateAppList_CloseButtonClick;

            tmpObj = new UpdateAppList();
            tmpObj = (UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<UpdateAppList>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(UpdateAppList);
                tmpObj = (UpdateAppList)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcUpdateAppListCD.ShowAsync();
        }

        private void BcUpdateAppListCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateAppListResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcUpdateAppListCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcUpdateAppList_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
