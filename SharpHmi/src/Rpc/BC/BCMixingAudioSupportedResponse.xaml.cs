﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCMixingAudioSupportedResponse : UserControl
    {
        MixingAudioSupported tmpObj = null;
        public BCMixingAudioSupportedResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowMixingAudioSupported()
        {
            ContentDialog bcMixingAudioSupportedCD = new ContentDialog();
            bcMixingAudioSupportedCD.Content = this;

            bcMixingAudioSupportedCD.PrimaryButtonText = Const.TxLater;
            bcMixingAudioSupportedCD.PrimaryButtonClick += BcMixingAudioSupportedCD_PrimaryButtonClick;

            bcMixingAudioSupportedCD.SecondaryButtonText = Const.Reset;
            bcMixingAudioSupportedCD.SecondaryButtonClick += BcMixingAudioSupportedCD_SecondaryButtonClick;

            bcMixingAudioSupportedCD.CloseButtonText = Const.Close;
            bcMixingAudioSupportedCD.CloseButtonClick += BcMixingAudioSupportedCD_CloseButtonClick;

            tmpObj = new MixingAudioSupported();
            tmpObj = (MixingAudioSupported) AppUtils.getSavedPreferenceValueForRpc<MixingAudioSupported>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(MixingAudioSupported);
                tmpObj = (MixingAudioSupported)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                if (tmpObj.getAttenuatedSupported() == null)
                {
                    AttenuatedSupportedCheckBox.IsChecked = false;
                    AttenuatedSupportTgl.IsEnabled = false;
                }
                else
                {
                    AttenuatedSupportedCheckBox.IsChecked = true;
                    AttenuatedSupportTgl.IsOn = (bool)tmpObj.getAttenuatedSupported();
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcMixingAudioSupportedCD.ShowAsync();
        }

        private void BcMixingAudioSupportedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result) ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AttenuatedSupportedCheckBox.IsChecked == true)
            {
                toggle = AttenuatedSupportTgl.IsOn;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationMixingAudioSupportedResponse(
                BuildRpc.getNextId(), toggle, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }
        
        private void BcMixingAudioSupportedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void BcMixingAudioSupportedCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AttenuatedSupportCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AttenuatedSupportTgl.IsEnabled = (bool)AttenuatedSupportedCheckBox.IsChecked;
        }
    }
}
