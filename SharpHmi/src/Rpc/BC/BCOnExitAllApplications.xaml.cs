﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnExitAllApplications : UserControl
    {
        OnExitAllApplications tmpObj = null;
        public BCOnExitAllApplications()
        {
            InitializeComponent();
            AppCloseReasonComboBox.ItemsSource = Enum.GetNames(typeof(ApplicationsCloseReason));
            AppCloseReasonComboBox.SelectedIndex = 0;
        }

        public async void ShowOnExitAllApplications()
        {
            ContentDialog bcOnExitAllApplicationsCD = new ContentDialog();
            bcOnExitAllApplicationsCD.Content = this;

            bcOnExitAllApplicationsCD.PrimaryButtonText = Const.TxNow;
            bcOnExitAllApplicationsCD.PrimaryButtonClick += BcOnExitAllApplicationsCD_PrimaryButtonClick;

            bcOnExitAllApplicationsCD.SecondaryButtonText = Const.Reset;
            bcOnExitAllApplicationsCD.SecondaryButtonClick += BcOnExitAllApplicationsCD_SecondaryButtonClick;

            bcOnExitAllApplicationsCD.CloseButtonText = Const.Close;
            bcOnExitAllApplicationsCD.CloseButtonClick += BcOnExitAllApplications_CloseButtonClick;

            tmpObj = new OnExitAllApplications();
            tmpObj = (OnExitAllApplications)AppUtils.getSavedPreferenceValueForRpc<OnExitAllApplications>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnExitAllApplications);
                tmpObj = (OnExitAllApplications)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                if (null != tmpObj.getApplicationsCloseReason())
                     AppCloseReasonComboBox.SelectedIndex = (int)tmpObj.getApplicationsCloseReason();
                else
                {
                    AppCloseReasonCheckBox.IsChecked = false;
                    AppCloseReasonComboBox.IsEnabled = false;
                }
            }
            await bcOnExitAllApplicationsCD.ShowAsync();
        }

        private void BcOnExitAllApplicationsCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            ApplicationsCloseReason? closeReason = null;
            if (AppCloseReasonCheckBox.IsChecked == true)
            {
                closeReason = (ApplicationsCloseReason)AppCloseReasonComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification =
                BuildRpc.buildBasicCommunicationOnExitAllApplications(closeReason);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnExitAllApplicationsCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnExitAllApplications_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AppCloseReasonCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AppCloseReasonComboBox.IsEnabled = (bool)AppCloseReasonCheckBox.IsChecked;
        }       
    }
}
