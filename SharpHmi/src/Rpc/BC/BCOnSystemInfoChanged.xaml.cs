﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnSystemInfoChanged : UserControl
    {
        OnSystemInfoChanged tmpObj = null;
        public BCOnSystemInfoChanged()
        {
            InitializeComponent();

            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;
        }

        public async void ShowOnSystemInfoChanged()
        {
            ContentDialog bcOnSystemInfoChangedCD = new ContentDialog();
            bcOnSystemInfoChangedCD.Content = this;

            bcOnSystemInfoChangedCD.PrimaryButtonText = Const.TxNow;
            bcOnSystemInfoChangedCD.PrimaryButtonClick += BcOnSystemInfoChangedCD_PrimaryButtonClick;

            bcOnSystemInfoChangedCD.SecondaryButtonText = Const.Reset;
            bcOnSystemInfoChangedCD.SecondaryButtonClick += BcOnSystemInfoChangedCD_SecondaryButtonClick;

            bcOnSystemInfoChangedCD.CloseButtonText = Const.Close;
            bcOnSystemInfoChangedCD.CloseButtonClick += BcOnSystemInfo_CloseButtonClick;

            tmpObj = new OnSystemInfoChanged();
            tmpObj = (OnSystemInfoChanged)AppUtils.getSavedPreferenceValueForRpc<OnSystemInfoChanged>(tmpObj.getMethod());
            if (null != tmpObj)
            {
                if (null != tmpObj.getLanguage())
                {
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                }
                else
                {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }
            }

            await bcOnSystemInfoChangedCD.ShowAsync();
        }

        private void BcOnSystemInfoChangedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Language? language = null;
            if ((bool)LanguageCheckBox.IsChecked)
            {
                language = (Language)LanguageComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnSystemInfoChanged(language);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnSystemInfoChangedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnSystemInfo_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }
    }
}
