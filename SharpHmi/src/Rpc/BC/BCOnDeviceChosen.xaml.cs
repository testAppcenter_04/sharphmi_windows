﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnDeviceChosen : UserControl
    {
        OnDeviceChosen tmpObj = null;
        public BCOnDeviceChosen()
        {
            InitializeComponent();

            TransportTypeComboBox.ItemsSource = Enum.GetNames(typeof(TransportType));
            TransportTypeComboBox.SelectedIndex = 0;
        }

        public async void ShowOnDeviceChosen()
        {
            ContentDialog bcOnDeviceChosenCd = new ContentDialog();
            bcOnDeviceChosenCd.Content = this;

            bcOnDeviceChosenCd.PrimaryButtonText = Const.TxNow;
            bcOnDeviceChosenCd.PrimaryButtonClick += BcOnDeviceChosenCd_PrimaryButtonClick;

            bcOnDeviceChosenCd.SecondaryButtonText = Const.Reset;
            bcOnDeviceChosenCd.SecondaryButtonClick += BcOnDeviceChosenCd_SecondaryButtonClick;

            bcOnDeviceChosenCd.CloseButtonText = Const.Close;
            bcOnDeviceChosenCd.CloseButtonClick += BcOnDeviceChosen_CloseButtonClick;

            tmpObj = new OnDeviceChosen();
            tmpObj = (OnDeviceChosen)AppUtils.getSavedPreferenceValueForRpc<OnDeviceChosen>(tmpObj.getMethod());

            if (null == tmpObj)
            {
                Type type = typeof(OnDeviceChosen);
                tmpObj = (OnDeviceChosen)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                if (null != tmpObj.getDeviceInfo())
                {
                    if (null != tmpObj.getDeviceInfo().getName())
                    {
                        DeviceNameTextBox.Text = tmpObj.getDeviceInfo().getName();
                    }
                    else
                    {
                        DeviceNameCheckBox.IsChecked = false;
                        DeviceNameTextBox.IsEnabled = false;

                    }
                    if (null != tmpObj.getDeviceInfo().getId())
                    {
                        DeviceIdTextBox.Text = tmpObj.getDeviceInfo().getId();
                    }
                    else
                    {
                        DeviceIdCheckBox.IsChecked = false;
                        DeviceIdTextBox.IsEnabled = false;
                    }
                    if (tmpObj.getDeviceInfo().getTransportType() != null)
                    {
                        TransportTypeComboBox.SelectedIndex = (int)tmpObj.getDeviceInfo().getTransportType();
                    }
                    else
                    {
                        TransportTypeCheckBox.IsChecked = false;
                        TransportTypeComboBox.IsEnabled = false;
                    }

                    if (tmpObj.getDeviceInfo().getIsSDLAllowed() == null)
                    {
                        IsSDLAllowedCheckBox.IsChecked = false;
                        IsSDLAllowedToggle.IsEnabled = false;
                    }
                    else
                    {
                        IsSDLAllowedCheckBox.IsChecked = true;
                        IsSDLAllowedToggle.IsOn = (bool)tmpObj.getDeviceInfo().getIsSDLAllowed();
                    }
                }
                else
                {
                    DeviceInfoCheckBox.IsChecked = false;
                    DeviceNameCheckBox.IsEnabled = false;
                    DeviceIdCheckBox.IsEnabled = false;
                    TransportTypeCheckBox.IsEnabled = false;
                    IsSDLAllowedCheckBox.IsEnabled = false;
                    IsSDLAllowedToggle.IsEnabled = false;
                    DeviceNameTextBox.IsEnabled = false;
                    DeviceIdTextBox.IsEnabled = false;
                    TransportTypeComboBox.IsEnabled = false;
                }
            }
            await bcOnDeviceChosenCd.ShowAsync();
        }

        private void BcOnDeviceChosenCd_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            DeviceInfo deviceInfo = null;
            if ((bool)DeviceInfoCheckBox.IsChecked)
            {
                deviceInfo = new DeviceInfo();
                if ((bool)DeviceNameCheckBox.IsChecked)
                {
                    deviceInfo.name = DeviceNameTextBox.Text;
                }
                if ((bool)DeviceIdCheckBox.IsChecked)
                {
                    deviceInfo.id = DeviceIdTextBox.Text;
                }
                if ((bool)TransportTypeCheckBox.IsChecked)
                {
                    deviceInfo.transportType = (TransportType) TransportTypeComboBox.SelectedIndex;
                }
                if (IsSDLAllowedCheckBox.IsChecked == true)
                {
                    deviceInfo.isSDLAllowed = IsSDLAllowedToggle.IsOn;
                }

            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnDeviceChosenNotification(deviceInfo);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnDeviceChosenCd_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnDeviceChosen_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void DeviceInfoCbCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceNameCheckBox.IsEnabled = (bool) DeviceInfoCheckBox.IsChecked;
            DeviceIdCheckBox.IsEnabled = (bool) DeviceInfoCheckBox.IsChecked;
            TransportTypeCheckBox.IsEnabled = (bool) DeviceInfoCheckBox.IsChecked;
            IsSDLAllowedCheckBox.IsEnabled = (bool) DeviceInfoCheckBox.IsChecked;
            IsSDLAllowedToggle.IsEnabled = false;

            DeviceNameTextBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked && (bool)DeviceNameCheckBox.IsChecked;
            DeviceIdTextBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked && (bool)DeviceIdCheckBox.IsChecked;
            TransportTypeComboBox.IsEnabled = (bool)DeviceInfoCheckBox.IsChecked && (bool)TransportTypeCheckBox.IsChecked;
        }

        private void DeviceNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceNameTextBox.IsEnabled = (bool)DeviceNameCheckBox.IsChecked && (bool)DeviceInfoCheckBox.IsChecked;
        }

        private void DeviceIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            DeviceIdTextBox.IsEnabled = (bool)DeviceIdCheckBox.IsChecked && (bool)DeviceInfoCheckBox.IsChecked;
        }

        private void TransportTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TransportTypeComboBox.IsEnabled = (bool)TransportTypeCheckBox.IsChecked && (bool)DeviceInfoCheckBox.IsChecked;
        }

        private void IsSDLAllowedCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            IsSDLAllowedToggle.IsEnabled = (bool)IsSDLAllowedCheckBox.IsChecked && (bool)DeviceInfoCheckBox.IsChecked;

        }
    }
}
