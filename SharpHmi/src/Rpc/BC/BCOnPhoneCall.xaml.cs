﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnPhoneCall : UserControl
    {
        OnPhoneCall tmpObj = null;
        public BCOnPhoneCall()
        {
            InitializeComponent();
        }

        public async void ShowOnPhoneCall()
        {
            ContentDialog bcOnPhoneCallCd = new ContentDialog();
            bcOnPhoneCallCd.Content = this;

            bcOnPhoneCallCd.PrimaryButtonText = Const.TxNow;
            bcOnPhoneCallCd.PrimaryButtonClick += BcOnPhoneCallCd_PrimaryButtonClick;

            bcOnPhoneCallCd.SecondaryButtonText = Const.Reset;
            bcOnPhoneCallCd.SecondaryButtonClick += BcOnPhoneCallCd_SecondaryButtonClick;

            bcOnPhoneCallCd.CloseButtonText = Const.Close;
            bcOnPhoneCallCd.CloseButtonClick += BcOnPhoneCall_CloseButtonClick;

            tmpObj = new OnPhoneCall();
            tmpObj = (OnPhoneCall)AppUtils.getSavedPreferenceValueForRpc<OnPhoneCall>(tmpObj.getMethod());

            if (null != tmpObj )
            {
                if (tmpObj.getActive() == null)
                {
                    DeactivateCheckBox.IsChecked = false;
                    DeactivateToggle.IsEnabled = false;
                }
                else
                {
                    DeactivateCheckBox.IsChecked = true;
                    DeactivateToggle.IsOn = (bool)tmpObj.getActive();
                }
                
            }
            await bcOnPhoneCallCd.ShowAsync();
        }

        private void BcOnPhoneCallCd_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            bool? toggle = null;
            if (DeactivateCheckBox.IsChecked == true)
            {
                toggle = DeactivateToggle.IsOn;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnPhoneCallNotification(toggle);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnPhoneCallCd_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnPhoneCall_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void DeactivateCBCheckedChanged(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            DeactivateToggle.IsEnabled = (bool)DeactivateCheckBox.IsChecked;

        }
    }
}