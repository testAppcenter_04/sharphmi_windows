﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnDeactivatHMI : UserControl
    {
        OnDeactivateHMI tmpObj = null;
        public BCOnDeactivatHMI()
        {
            InitializeComponent();
        }

        public async void ShowOnDeactivateHMI()
        {
            ContentDialog bCOnDeactivatHMICd = new ContentDialog();
            bCOnDeactivatHMICd.Content = this;

            bCOnDeactivatHMICd.PrimaryButtonText = Const.TxNow;
            bCOnDeactivatHMICd.PrimaryButtonClick += BCOnDeactivatHMICd_PrimaryButtonClick;

            bCOnDeactivatHMICd.SecondaryButtonText = Const.Reset;
            bCOnDeactivatHMICd.SecondaryButtonClick += BCOnDeactivatHMICd_SecondaryButtonClick;

            bCOnDeactivatHMICd.CloseButtonText = Const.Close;
            bCOnDeactivatHMICd.CloseButtonClick += BcOnDeactivatHMI_CloseButtonClick;

            tmpObj = new OnDeactivateHMI();
            tmpObj = (OnDeactivateHMI)AppUtils.getSavedPreferenceValueForRpc<OnDeactivateHMI>(tmpObj.getMethod());

            if (null != tmpObj)
            {
               if (tmpObj.getDeactivatedStatus() == null)
                {
                    DeactivateCheckBox.IsChecked = false;
                    DeactivateToggle.IsEnabled = false;
                }
                else
                {
                    DeactivateToggle.IsOn = (bool)tmpObj.getDeactivatedStatus();
                }
            }
            await bCOnDeactivatHMICd.ShowAsync();
        }

        private void BCOnDeactivatHMICd_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            bool? toggle = null;
            if (DeactivateCheckBox.IsChecked == true)
            {
                toggle = DeactivateToggle.IsOn;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnDeactivateHMINotification(toggle);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BCOnDeactivatHMICd_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnDeactivatHMI_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void DeactivateCBCheckedChanged(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            DeactivateToggle.IsEnabled = (bool)DeactivateCheckBox.IsChecked;

        }
    }
}
