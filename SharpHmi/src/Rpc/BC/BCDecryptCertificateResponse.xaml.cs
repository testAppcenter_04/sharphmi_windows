﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCDecryptCertificateResponse : UserControl
    {
        DecryptCertificate tmpObj = null;
        public BCDecryptCertificateResponse()
        {
            InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowDecryptCertificate()
        {
            ContentDialog bcDecryptCertificateCD = new ContentDialog();
            bcDecryptCertificateCD.Content = this;

            bcDecryptCertificateCD.PrimaryButtonText = Const.TxLater;
            bcDecryptCertificateCD.PrimaryButtonClick += BcDecryptCertificateCD_PrimaryButtonClick;

            bcDecryptCertificateCD.SecondaryButtonText = Const.Reset;
            bcDecryptCertificateCD.SecondaryButtonClick += BcDecryptCertificateCD_SecondaryButtonClick;

            bcDecryptCertificateCD.CloseButtonText = Const.Close;
            bcDecryptCertificateCD.CloseButtonClick += BcDecryptCertificateCD_CloseButtonClick;

            tmpObj = new DecryptCertificate();
            tmpObj = (DecryptCertificate)AppUtils.getSavedPreferenceValueForRpc<DecryptCertificate>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(DecryptCertificate);
                tmpObj = (DecryptCertificate)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcDecryptCertificateCD.ShowAsync();
        }

        private void BcDecryptCertificateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationDecryptCertificateResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcDecryptCertificateCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcDecryptCertificateCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
