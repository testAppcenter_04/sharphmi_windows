﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnAwakeSDL : UserControl
    {
        //OnAwakeSDL tmpObj = null;
        public BCOnAwakeSDL()
        {
            InitializeComponent();
        }

        public async void ShowOnAwakeSDL()
        {
            ContentDialog bcOnAwakeSDlCD = new ContentDialog();
            bcOnAwakeSDlCD.Content = this;

            bcOnAwakeSDlCD.PrimaryButtonText = Const.TxNow;
            bcOnAwakeSDlCD.PrimaryButtonClick += BcOnAwakeSDlCD_PrimaryButtonClick;

            bcOnAwakeSDlCD.SecondaryButtonText = Const.Reset;
            bcOnAwakeSDlCD.SecondaryButtonClick += BcOnAwakeSDlCD_SecondaryButtonClick;

            bcOnAwakeSDlCD.CloseButtonText = Const.Close;
            bcOnAwakeSDlCD.CloseButtonClick += BcOnAwakeSDl_CloseButtonClick;

            //tmpObj = new OnAwakeSDL();
            //tmpObj = (OnAwakeSDL)AppUtils.getSavedPreferenceValueForRpc<OnAwakeSDL>(tmpObj.getMethod());

            //if (null != tmpObj)
            //{

            //}

            await bcOnAwakeSDlCD.ShowAsync();
        }

        private void BcOnAwakeSDlCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnAwakeSDL();
            //AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnAwakeSDlCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //if (null != tmpObj)
            //{
            //    AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            //}
        }

        private void BcOnAwakeSDl_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
