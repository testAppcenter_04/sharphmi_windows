﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnExitApplication : UserControl
    {
        OnExitApplication tmpObj = null;
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        public BCOnExitApplication()
        {
            InitializeComponent();

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;

            AppExitReasonsComboBox.ItemsSource = Enum.GetNames(typeof(ApplicationExitReason));
            AppExitReasonsComboBox.SelectedIndex = 0;
        }

        public async void ShowOnExitApplication()
        {
            ContentDialog bcOnExitApplicationCD = new ContentDialog();
            bcOnExitApplicationCD.Content = this;

            bcOnExitApplicationCD.PrimaryButtonText = Const.TxNow;
            bcOnExitApplicationCD.PrimaryButtonClick += OnExitApplicationCD_PrimaryButtonClick;

            bcOnExitApplicationCD.SecondaryButtonText = Const.Reset;
            bcOnExitApplicationCD.SecondaryButtonClick += OnExitApplicationCD_SecondaryButtonClick;

            bcOnExitApplicationCD.CloseButtonText = Const.Close;
            bcOnExitApplicationCD.CloseButtonClick += BcOnExitApplications_CloseButtonClick;

            tmpObj = new OnExitApplication();
            tmpObj = (OnExitApplication)AppUtils.getSavedPreferenceValueForRpc<OnExitApplication>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (tmpObj.getAppId() != null)
                {
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                }
                else
                {
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                    AppIdCheckBox.IsChecked = false;
                   
                }
                if (null != tmpObj.getApplicationExitReason())
                {
                    AppExitReasonsComboBox.SelectedIndex = (int)tmpObj.getApplicationExitReason();
                }
                else
                {
                    AppExitReasonsComboBox.IsEnabled = false;

                    AppExitReasonsCheckBox.IsChecked = false;
                }


            }

            await bcOnExitApplicationCD.ShowAsync();
        }

        private void OnExitApplicationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true && ManualAppIdTextBox.Text != null)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch
                    {
                        selectedAppID = null;
                    }

                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }
            ApplicationExitReason? applicationExitReason = null;
            if (AppExitReasonsCheckBox.IsChecked == true)
            {
                applicationExitReason = (ApplicationExitReason)AppExitReasonsComboBox.SelectedIndex;
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification =
                BuildRpc.buildBasicCommunicationOnExitApplication(applicationExitReason, selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void OnExitApplicationCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnExitApplications_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }

        private void AppExitReasonsCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AppExitReasonsComboBox.IsEnabled = (bool)AppExitReasonsCheckBox.IsChecked;
        }
    }
}
