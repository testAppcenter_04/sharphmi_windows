﻿using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnEmergencyEvent : UserControl
    {
        OnEmergencyEvent tmpObj = null;
        public BCOnEmergencyEvent()
        {
            InitializeComponent();
        }
        public async void ShowOnEmergencyEvent()
        {
            ContentDialog bCOnEmergencyEventCd = new ContentDialog();
            bCOnEmergencyEventCd.Content = this;

            bCOnEmergencyEventCd.PrimaryButtonText = Const.TxNow;
            bCOnEmergencyEventCd.PrimaryButtonClick += BCOnEmergencyEventCd_PrimaryButtonClick;

            bCOnEmergencyEventCd.SecondaryButtonText = Const.Reset;
            bCOnEmergencyEventCd.SecondaryButtonClick += BCOnEmergencyEventCd_SecondaryButtonClick;

            bCOnEmergencyEventCd.CloseButtonText = Const.Close;
            bCOnEmergencyEventCd.CloseButtonClick += BcOnEmergencyEvent_CloseButtonClick;

            tmpObj = new OnEmergencyEvent();
            tmpObj = (OnEmergencyEvent)AppUtils.getSavedPreferenceValueForRpc<OnEmergencyEvent>(tmpObj.getMethod());

            if (null != tmpObj)
            {

                if (tmpObj.getEnabled() == null)
                {
                    EnableCheckBox.IsChecked = false;
                    EnableToggle.IsEnabled = false;
                }
                else
                {
                    EnableToggle.IsOn = (bool)tmpObj.getEnabled();
                }
            }
            await bCOnEmergencyEventCd.ShowAsync();
        }

        private void BCOnEmergencyEventCd_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            bool? toggle = null;
            if (EnableCheckBox.IsChecked == true)
            {
                toggle = EnableToggle.IsOn;
            }

            HmiApiLib.Base.RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnEmergencyEventNotification(toggle);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BCOnEmergencyEventCd_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnEmergencyEvent_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void EnabledCBCheckedChanged(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            EnableToggle.IsEnabled = (bool)EnableCheckBox.IsChecked;

        }
    }
}
