﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnStartDeviceDiscovery : UserControl
    {
        //OnStartDeviceDiscovery tmpObj = null;
        public BCOnStartDeviceDiscovery()
        {
            InitializeComponent();
        }

        public async void ShowOnStartDeviceDiscovery()
        {
            ContentDialog bcOnStartDeviceDiscoveryCD = new ContentDialog();
            bcOnStartDeviceDiscoveryCD.Content = this;

            bcOnStartDeviceDiscoveryCD.PrimaryButtonText = Const.TxNow;
            bcOnStartDeviceDiscoveryCD.PrimaryButtonClick += BcOnStartDeviceDiscoveryCD_PrimaryButtonClick;

            bcOnStartDeviceDiscoveryCD.SecondaryButtonText = Const.Reset;
            bcOnStartDeviceDiscoveryCD.SecondaryButtonClick += BcOnStartDeviceDiscoveryCD_SecondaryButtonClick;

            bcOnStartDeviceDiscoveryCD.CloseButtonText = Const.Close;
            bcOnStartDeviceDiscoveryCD.CloseButtonClick += BcOnStartDeviceDiscovery_CloseButtonClick;

            //tmpObj = new OnStartDeviceDiscovery();
            //tmpObj = (OnStartDeviceDiscovery)AppUtils.getSavedPreferenceValueForRpc<OnStartDeviceDiscovery>(tmpObj.getMethod());

            //if (null != tmpObj)
            //{

            //}

            await bcOnStartDeviceDiscoveryCD.ShowAsync();
        }

        private static void BcOnStartDeviceDiscoveryCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildBasicCommunicationOnStartDeviceDiscovery();
            //AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private static void BcOnStartDeviceDiscoveryCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //if (null != tmpObj)
            //{
            //    AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            //}
        }

        private void BcOnStartDeviceDiscovery_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
