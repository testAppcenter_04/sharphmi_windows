﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using System.Runtime.Serialization;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCGetSystemInfoResponse : UserControl
    {
        GetSystemInfo tmpObj = null;
        public BCGetSystemInfoResponse()
        {
            this.InitializeComponent();

            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Language));
            //LanguageComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetSystemInfo()
        {
            ContentDialog bcGetSystemInfoCD = new ContentDialog();
            bcGetSystemInfoCD.Content = this;

            bcGetSystemInfoCD.PrimaryButtonText = Const.TxLater;
            bcGetSystemInfoCD.PrimaryButtonClick += BcGetSystemInfoCD_PrimaryButtonClick;

            bcGetSystemInfoCD.SecondaryButtonText = Const.Reset;
            bcGetSystemInfoCD.SecondaryButtonClick += BcGetSystemInfoCD_SecondaryButtonClick;

            bcGetSystemInfoCD.CloseButtonText = Const.Close;
            bcGetSystemInfoCD.CloseButtonClick += BcGetSystemInfoCD_CloseButtonClick;

            tmpObj = new GetSystemInfo();
            tmpObj = (GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<GetSystemInfo>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetSystemInfo);
                tmpObj = (GetSystemInfo)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getCcpuVersion() != null)
                {
                    CCPUVersionTextBox.Text = tmpObj.getCcpuVersion();
                }
                else
                {
                    CCPUVersionTextBox.IsEnabled = false;
                    CCPUVersionCheckBox.IsChecked = false;
                }
                if (tmpObj.getLanguage() != null)
                {
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                }
                else
                {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }


                if (tmpObj.getWersCountryCode() != null)
                {
                    WersCountryCodeTextBox.Text = tmpObj.getWersCountryCode();
                }
                else
                {
                    WersCountryCodeTextBox.IsEnabled = false;
                    WersCountryCodeCheckBox.IsChecked = false;
                }
               ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcGetSystemInfoCD.ShowAsync();
        }
        
        private void BcGetSystemInfoCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            String countryCode = null;
            String ccpuVersion = null;
            if (CCPUVersionCheckBox.IsChecked == true)
            {
                ccpuVersion = CCPUVersionTextBox.Text;
            }

            if (WersCountryCodeCheckBox.IsChecked == true)
            {
                countryCode = WersCountryCodeTextBox.Text;
            }

            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            HmiApiLib.Common.Enums.Language? language = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                language = (HmiApiLib.Common.Enums.Language)LanguageComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationGetSystemInfoResponse(
                BuildRpc.getNextId(), resultCode, ccpuVersion, language, countryCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcGetSystemInfoCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcGetSystemInfoCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void CCPUVersionCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CCPUVersionTextBox.IsEnabled = (bool)CCPUVersionCheckBox.IsChecked;
        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }

        private void WersCountryCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            WersCountryCodeTextBox.IsEnabled = (bool)WersCountryCodeCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool) ResultCodeCheckBox.IsChecked;
        }
    }
}
