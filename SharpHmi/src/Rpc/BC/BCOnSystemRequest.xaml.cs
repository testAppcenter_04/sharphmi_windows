﻿using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCOnSystemRequest : UserControl
    {
        OnSystemRequest tmpObj = null;
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);
        public BCOnSystemRequest()
        {
            InitializeComponent();
                        
            RequestTypeComboBox.ItemsSource = Enum.GetNames(typeof(RequestType));
            RequestTypeComboBox.SelectedIndex = 0;

            FileTypeComboBox.ItemsSource = Enum.GetNames(typeof(FileType));
            FileTypeComboBox.SelectedIndex = 0;

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnSystemRequest()
        {
            ContentDialog bcOnSystemRequestCD = new ContentDialog();
            bcOnSystemRequestCD.Content = this;

            bcOnSystemRequestCD.PrimaryButtonText = Const.TxNow;
            bcOnSystemRequestCD.PrimaryButtonClick += BcOnSystemRequestCD_PrimaryButtonClick;

            bcOnSystemRequestCD.SecondaryButtonText = Const.Reset;
            bcOnSystemRequestCD.SecondaryButtonClick += BcOnSystemRequestCD_SecondaryButtonClick;

            bcOnSystemRequestCD.CloseButtonText = Const.Close;
            bcOnSystemRequestCD.CloseButtonClick += BcOnSystemRequest_CloseButtonClick;

            tmpObj = new OnSystemRequest();
            tmpObj = (OnSystemRequest)AppUtils.getSavedPreferenceValueForRpc<OnSystemRequest>(tmpObj.getMethod());

            if (null != tmpObj)
            {
                if (tmpObj.getRequestType() != null)
                {
                    RequestTypeComboBox.SelectedIndex = (int)tmpObj.getRequestType();
                }
                else
                {
                    RequestTypeCheckBox.IsChecked = false;
                    RequestTypeComboBox.IsEnabled = false;
                }
               
                if (null != tmpObj.getUrl())
                {
                    UrlTextBox.Text = tmpObj.getUrl();
                }
                else
                {
                    UrlCheckBox.IsChecked = false;
                    UrlTextBox.IsEnabled = false;
                }
                if (tmpObj.getFileType() != null)
                    FileTypeComboBox.SelectedIndex = (int)tmpObj.getFileType();
                else
                {
                    FileTypeComboBox.IsEnabled = false;
                    FileTypeCheckBox.IsChecked = false;
                }

                if (null != tmpObj.getOffset())
                {
                    OffsetTextBox.Text = tmpObj.getOffset().ToString();
                }
                else
                {
                    OffsetTextBox.IsEnabled = false;
                    OffsetCheckBox.IsChecked = false;
                }

                if (null != tmpObj.getLength())
                {
                    LengthTextBox.Text = tmpObj.getLength().ToString();
                }
                else
                {
                    LengthTextBox.IsEnabled = false;
                    LengthCheckBox.IsChecked = false;
                }
                if (null != tmpObj.getTimeout())
                {
                    TimeOutTextBox.Text = tmpObj.getTimeout().ToString();
                }
                else
                {
                    TimeOutCheckBox.IsChecked = false;
                    LengthTextBox.IsEnabled = false;
                }
                if (null != tmpObj.getFileName())
                {
                    FileNameTextBox.Text = tmpObj.getFileName();
                }
                else
                {
                    FileNameCheckBox.IsChecked = false;
                    FileNameTextBox.IsEnabled = false; ;
                }

                if (null != tmpObj.getAppId())
                {
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                }
                else
                {
                    AppIdCheckBox.IsChecked = false;
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                }
            }

            await bcOnSystemRequestCD.ShowAsync();
        }

        private void BcOnSystemRequestCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestType? requestType = null;
            if (RequestTypeCheckBox.IsChecked == true)
            {
                requestType = (RequestType) RequestTypeComboBox.SelectedIndex;
            }
            String url = null;
            if (UrlCheckBox.IsChecked == true)
            {
                url = UrlTextBox.Text;
            }
            FileType? selectedFileType = null;
            if (FileTypeCheckBox.IsChecked == true)
            {
                selectedFileType = (FileType)FileTypeComboBox.SelectedIndex;
            }
            int? offset = null;
            if (OffsetCheckBox.IsChecked == true)
            {
                try
                {
                    offset = Int32.Parse(OffsetTextBox.Text);
                }
                catch
                {
                }
            }
            int? length = null;
            if (LengthCheckBox.IsChecked == true)
            {
                try
                {
                    length = Int32.Parse(LengthTextBox.Text);
                }
                catch
                {
                }
            }
            int? timeout = null;
            if (TimeOutCheckBox.IsChecked == true)
            {
                try
                {
                    timeout = Int32.Parse(TimeOutTextBox.Text);
                }
                catch
                {
                }
            }
            String fileName = null;
            if (FileNameCheckBox.IsChecked == true)
            {
                fileName = FileNameTextBox.Text;
            }
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch
                    {
                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }
            HmiApiLib.Base.RequestNotifyMessage rpcNotification = 
                BuildRpc.buildBasicCommunicationOnSystemRequestNotification(requestType, url, selectedFileType,
                offset, length, timeout, fileName, selectedAppID, null);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void BcOnSystemRequestCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void BcOnSystemRequest_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void RequestTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            RequestTypeComboBox.IsEnabled = (bool)RequestTypeCheckBox.IsChecked;
        }

        private void UrlCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            UrlTextBox.IsEnabled = (bool)UrlCheckBox.IsChecked;
        }

        private void FileTypeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FileTypeComboBox.IsEnabled = (bool)FileTypeCheckBox.IsChecked;
        }

        private void OffsetCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            OffsetTextBox.IsEnabled = (bool)OffsetCheckBox.IsChecked;
        }

        private void LengthCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LengthTextBox.IsEnabled = (bool)LengthCheckBox.IsChecked;
        }

        private void TimeOutCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            TimeOutTextBox.IsEnabled = (bool)TimeOutCheckBox.IsChecked;
        }

        private void FileNameCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            FileNameTextBox.IsEnabled = (bool)FileNameCheckBox.IsChecked;
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }
    }
}
