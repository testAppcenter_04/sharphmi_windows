﻿using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Controllers.BasicCommunication.OutgoingResponses;
using SharpHmi.src.Utility;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.BC
{
    public sealed partial class BCPolicyUpdateResponse : UserControl
    {
        PolicyUpdate tmpObj = null;
        public BCPolicyUpdateResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowPolicyUpdate()
        {
            ContentDialog bcPolicyUpdateCD = new ContentDialog();
            bcPolicyUpdateCD.Content = this;

            bcPolicyUpdateCD.PrimaryButtonText = Const.TxLater;
            bcPolicyUpdateCD.PrimaryButtonClick += BcPolicyUpdateCD_PrimaryButtonClick;

            bcPolicyUpdateCD.SecondaryButtonText = Const.Reset;
            bcPolicyUpdateCD.SecondaryButtonClick += BcPolicyUpdateCD_SecondaryButtonClick;

            bcPolicyUpdateCD.CloseButtonText = Const.Close;
            bcPolicyUpdateCD.CloseButtonClick += BcPolicyUpdate_CloseButtonClick;

            tmpObj = new PolicyUpdate();
            tmpObj = (PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<PolicyUpdate>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(PolicyUpdate);
                tmpObj = (PolicyUpdate)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await bcPolicyUpdateCD.ShowAsync();
        }

        private void BcPolicyUpdateCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationPolicyUpdateResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void BcPolicyUpdateCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool) ResultCodeCheckBox.IsChecked;
        }

        private void BcPolicyUpdate_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
