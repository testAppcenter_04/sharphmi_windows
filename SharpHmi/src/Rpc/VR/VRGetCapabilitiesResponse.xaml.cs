﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using System.Collections.ObjectModel;
using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRGetCapabilitiesResponse : UserControl
    {
       HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities getCapability = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();

        GetCapabilities tmpObj = null;
        ContentDialog vrGetCapabilitiesCD = new ContentDialog();
        ObservableCollection<HmiApiLib.Common.Enums.VrCapabilities> vrCapabilitiesList = new ObservableCollection<HmiApiLib.Common.Enums.VrCapabilities>();
        public VRGetCapabilitiesResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetCapabilities()
        {
            vrGetCapabilitiesCD = new ContentDialog();
            vrGetCapabilitiesCD.Content = this;

            vrGetCapabilitiesCD.PrimaryButtonText = Const.TxLater;
            vrGetCapabilitiesCD.PrimaryButtonClick += VrGetCapabilitiesCD_PrimaryButtonClick;

            vrGetCapabilitiesCD.SecondaryButtonText = Const.Reset;
            vrGetCapabilitiesCD.SecondaryButtonClick += VrGetCapabilitiesCD_ResetButtonClick;

            vrGetCapabilitiesCD.CloseButtonText = Const.Close;
            vrGetCapabilitiesCD.CloseButtonClick += VrGetCapabilitiesCD_CloseButtonClick;

            VRGetCapabilitiesListView.ItemsSource = vrCapabilitiesList;

            tmpObj = new GetCapabilities();
            tmpObj = (GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<GetCapabilities>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetCapabilities);
                tmpObj = (GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getVrCapabilities() != null)
                {
                    List<HmiApiLib.Common.Enums.VrCapabilities> speechList = tmpObj.getVrCapabilities();
                    foreach (HmiApiLib.Common.Enums.VrCapabilities item in speechList)
                    {
                        vrCapabilitiesList.Add(item);
                    }
                }
                else {
                    AddVRCapabilitiesCheckBox.IsChecked = false;
                    AddVRCapabilitiesButton.IsEnabled = false;
                }

            }

            await vrGetCapabilitiesCD.ShowAsync();
        }

        private void VrGetCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            getCapability = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();

            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            List<HmiApiLib.Common.Enums.VrCapabilities> vrCapabilities=null;
            if (AddVRCapabilitiesCheckBox.IsChecked == true)
            {
                vrCapabilities = new List<HmiApiLib.Common.Enums.VrCapabilities>();
                vrCapabilities.AddRange(vrCapabilitiesList);
                //getCapability. = vrCapabilities;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVrGetCapabilitiesResponse(
                BuildRpc.getNextId(), resultCode, vrCapabilities);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrGetCapabilitiesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrGetCapabilitiesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void AddVRCapabilitiesCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddVRCapabilitiesButton.IsEnabled = (bool)AddVRCapabilitiesCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private async void AddVRCapabilitiesTapped(object sender, TappedRoutedEventArgs e)
        {
            vrGetCapabilitiesCD.Hide();
            ContentDialog addVrCapabilitiesCD = new ContentDialog();

            var addSpeechCapabilities = new VrCapabilities();
            addVrCapabilitiesCD.Content = addSpeechCapabilities;

            addVrCapabilitiesCD.PrimaryButtonText = Const.OK;
            addVrCapabilitiesCD.PrimaryButtonClick += AddVrCapabilitiesCD_PrimaryButtonClick;

            addVrCapabilitiesCD.SecondaryButtonText = Const.Close;
            addVrCapabilitiesCD.SecondaryButtonClick += AddVrCapabilitiesCD_SecondaryButtonClick;

            await addVrCapabilitiesCD.ShowAsync();
        }

        private async void AddVrCapabilitiesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            VrCapabilities vrCapabilities = sender.Content as VrCapabilities;
            foreach (HmiApiLib.Common.Enums.VrCapabilities vrCapabilties in vrCapabilities.vrCapabilitiesCheckedList)
            {
                vrCapabilitiesList.Add(vrCapabilties);
            }

            sender.Hide();
            await vrGetCapabilitiesCD.ShowAsync();
        }

        private async void AddVrCapabilitiesCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await vrGetCapabilitiesCD.ShowAsync();
        }
    }
}
