﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRAddCommandResponse : UserControl
    {
        AddCommand tmpObj;
        public VRAddCommandResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowAddCommand()
        {
            ContentDialog vrAddCommandCD = new ContentDialog();
            vrAddCommandCD.Content = this;

            vrAddCommandCD.PrimaryButtonText = Const.TxLater;
            vrAddCommandCD.PrimaryButtonClick += VrAddCommandCD_PrimaryButtonClick;

            vrAddCommandCD.SecondaryButtonText = Const.Reset;
            vrAddCommandCD.SecondaryButtonClick += VrAddCommandCD_SecondaryButtonClick;

            vrAddCommandCD.CloseButtonText = Const.Close;
            vrAddCommandCD.CloseButtonClick += VrAddCommandCD_CloseButtonClick;

            tmpObj = new AddCommand();
            tmpObj = (AddCommand)AppUtils.getSavedPreferenceValueForRpc<AddCommand>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(AddCommand);
                tmpObj = (AddCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await vrAddCommandCD.ShowAsync();
        }

        private void VrAddCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildVrAddCommandResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrAddCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrAddCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
