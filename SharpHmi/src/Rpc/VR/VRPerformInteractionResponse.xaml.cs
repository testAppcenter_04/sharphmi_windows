﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRPerformInteractionResponse : UserControl
    {
        PerformInteraction tmpObj;

        public VRPerformInteractionResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowPerformInteraction()
        {
            ContentDialog vrPerformInteractionCD = new ContentDialog();
            vrPerformInteractionCD.Content = this;

            vrPerformInteractionCD.PrimaryButtonText = Const.TxLater;
            vrPerformInteractionCD.PrimaryButtonClick += VrPerformInteractionCD_PrimaryButtonClick;

            vrPerformInteractionCD.SecondaryButtonText = Const.Reset;
            vrPerformInteractionCD.SecondaryButtonClick += VrPerformInteractionCD_SecondaryButtonClick;

            vrPerformInteractionCD.CloseButtonText = Const.Close;
            vrPerformInteractionCD.CloseButtonClick += VrPerformInteractionCD_CloseButtonClick;

            tmpObj = new PerformInteraction();
            tmpObj = (PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<PerformInteraction>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(PerformInteraction);
                tmpObj = (PerformInteraction)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getChoiceID() != null)
                    ChoiceIDTextBox.Text = tmpObj.getChoiceID().ToString();
                else
                {
                    ChoiceIDTextBox.IsEnabled = false;
                    ChoiceIDCheckBox.IsChecked = false;
                }
                
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await vrPerformInteractionCD.ShowAsync();
        }

        private void VrPerformInteractionCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }

            int? choiceId = null;
            if (ChoiceIDCheckBox.IsChecked == true)
            {
                try
                {
                    choiceId = Int32.Parse(ChoiceIDTextBox.Text);
                }
                catch
                {
                    choiceId = 0;
                }
            }
            
            RpcResponse rpcResponse = BuildRpc.buildVrPerformInteractionResponse(
            BuildRpc.getNextId(), choiceId, resultCode);

            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrPerformInteractionCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrPerformInteractionCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ChoiceIDCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ChoiceIDTextBox.IsEnabled = (bool)ChoiceIDCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
