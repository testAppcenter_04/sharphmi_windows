﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SharpHmi.src.Utility;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRGetLanguageResponse : UserControl
    {
        GetLanguage tmpObj = null;

        public VRGetLanguageResponse()
        {
            this.InitializeComponent();
            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(Language));
            LanguageComboBox.SelectedIndex = 0;

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetLanguage()
        {
            ContentDialog vrGetLanguageCD = new ContentDialog();
            vrGetLanguageCD.Content = this;

            vrGetLanguageCD.PrimaryButtonText = Const.TxLater;
            vrGetLanguageCD.PrimaryButtonClick += VRGetLanguageCD_PrimaryButtonClick;

            vrGetLanguageCD.SecondaryButtonText = Const.Reset;
            vrGetLanguageCD.SecondaryButtonClick += VRGetLanguageCD_ResetButtonClick;

            vrGetLanguageCD.CloseButtonText = Const.Close;
            vrGetLanguageCD.CloseButtonClick += VRGetLanguageCD_CloseButtonClick;

            tmpObj = new GetLanguage();
            tmpObj = (GetLanguage)AppUtils.getSavedPreferenceValueForRpc<GetLanguage>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetLanguage);
                tmpObj = (GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getLanguage() != null)
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                else {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }
                
            }

            await vrGetLanguageCD.ShowAsync();
        }

        private void VRGetLanguageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            Language? lang = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                lang = (Language)LanguageComboBox.SelectedIndex;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVrGetLanguageResponse(
                BuildRpc.getNextId(), lang, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VRGetLanguageCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VRGetLanguageCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
