﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRDeleteCommandResponse : UserControl
    {
        DeleteCommand tmpObj;

        public VRDeleteCommandResponse()
        {
            this.InitializeComponent();

            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowDeleteCommand()
        {
            ContentDialog vrDeleteCommandCD = new ContentDialog();
            vrDeleteCommandCD.Content = this;

            vrDeleteCommandCD.PrimaryButtonText = Const.TxLater;
            vrDeleteCommandCD.PrimaryButtonClick += VrDeleteCommandCD_PrimaryButtonClick;

            vrDeleteCommandCD.SecondaryButtonText = Const.Reset;
            vrDeleteCommandCD.SecondaryButtonClick += VrDeleteCommandCD_SecondaryButtonClick;

            vrDeleteCommandCD.CloseButtonText = Const.Close;
            vrDeleteCommandCD.CloseButtonClick += VrDeleteCommandCD_CloseButtonClick;

            tmpObj = new DeleteCommand();
            tmpObj = (DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<DeleteCommand>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(DeleteCommand);
                tmpObj = (DeleteCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await vrDeleteCommandCD.ShowAsync();
        }

        private void VrDeleteCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildVrDeleteCommandResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrDeleteCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrDeleteCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
