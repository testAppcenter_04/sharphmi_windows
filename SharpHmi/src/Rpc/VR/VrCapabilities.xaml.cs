﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VrCapabilities : UserControl
    {
        private static String[] vrCapabilitiesArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VrCapabilities));
        public ObservableCollection<HmiApiLib.Common.Enums.VrCapabilities> vrCapabilitiesCheckedList = new ObservableCollection<HmiApiLib.Common.Enums.VrCapabilities>();

        public VrCapabilities()
        {
            InitializeComponent();
            VrCapabilitiesListBox.ItemsSource = vrCapabilitiesArray;
            

        }

        private void VrCapabilitiesCheckBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Common.Enums.VrCapabilities vrCapabilities = (HmiApiLib.Common.Enums.VrCapabilities)HmiApiLib.Utils.getEnumValue(typeof(HmiApiLib.Common.Enums.VrCapabilities), (sender as CheckBox).Content.ToString());
            
                if (((CheckBox)sender).IsChecked == true)
                    vrCapabilitiesCheckedList.Add(vrCapabilities);
                else
                {
                    if (vrCapabilitiesCheckedList.Contains(vrCapabilities))
                        vrCapabilitiesCheckedList.Remove(vrCapabilities);
                }
            
        }
    }
}