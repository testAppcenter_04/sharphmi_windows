﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using SharpHmi.TTS;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{    
    public sealed partial class VRGetSupportedLanguagesResponse : UserControl
    {
        static ContentDialog vrGetSupportedLanguagesCD = new ContentDialog();
        ObservableCollection<Language> languageList = new ObservableCollection<Language>();

        GetSupportedLanguages tmpObj = null;

        public VRGetSupportedLanguagesResponse()
        {
            InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowGetSupportedLanguages()
        {
            vrGetSupportedLanguagesCD.Content = this;

            vrGetSupportedLanguagesCD.PrimaryButtonText = Const.TxLater;
            vrGetSupportedLanguagesCD.PrimaryButtonClick += VrGetSupportedLanguagesCD_PrimaryButtonClick;

            vrGetSupportedLanguagesCD.SecondaryButtonText = Const.Reset;
            vrGetSupportedLanguagesCD.SecondaryButtonClick += VrGetSupportedLanguagesCD_ResetButtonClick;

            vrGetSupportedLanguagesCD.CloseButtonText = Const.Close;
            vrGetSupportedLanguagesCD.CloseButtonClick += VrGetSupportedLanguagesCD_CloseButtonClick;

            GetSupportedLanguageList.ItemsSource = languageList;

            tmpObj = new GetSupportedLanguages();
            tmpObj = (GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<GetSupportedLanguages>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(GetSupportedLanguages);
                tmpObj = (GetSupportedLanguages)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
                if (tmpObj.getLanguages() != null)
                {
                    List<Language> langList = tmpObj.getLanguages();
                    foreach (Language item in langList)
                    {
                        languageList.Add(item);
                    }
                }
                else
                {
                    AddLanguageButton.IsEnabled = false;
                    AddLanguageCheckBox.IsChecked = false;
                }
            }

            await vrGetSupportedLanguagesCD.ShowAsync();
        }

        private void VrGetSupportedLanguagesCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            List<Language> lngList =null;
            if (AddLanguageCheckBox.IsChecked == true)
            {
                lngList = new List<Language>();
                lngList.AddRange(languageList);
            }
            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildVrGetSupportedLanguagesResponse(
                BuildRpc.getNextId(), resultCode, lngList);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrGetSupportedLanguagesCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrGetSupportedLanguagesCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }


        private void AddLanguageCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AddLanguageButton.IsEnabled = (bool)AddLanguageCheckBox.IsChecked;
        }

        private async void AddLanguageTapped(object sender, TappedRoutedEventArgs e)
        {
            vrGetSupportedLanguagesCD.Hide();

            ContentDialog vrAddLanguageCD = new ContentDialog();

            var vrAddLanguage = new AddLanguage();
            vrAddLanguageCD.Content = vrAddLanguage;

            vrAddLanguageCD.PrimaryButtonText = Const.OK;
            vrAddLanguageCD.PrimaryButtonClick += VrAddLanguageCD_PrimaryButtonClick;

            vrAddLanguageCD.SecondaryButtonText = Const.Close;
            vrAddLanguageCD.SecondaryButtonClick += VrAddLanguageCD_SecondaryButtonClick;

            await vrAddLanguageCD.ShowAsync();
        }

        private async void VrAddLanguageCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var addLanguage = sender.Content as AddLanguage;
            foreach (Language lang in addLanguage.innerLanguageList)
            {
                languageList.Add(lang);
            }

            sender.Hide();
            await vrGetSupportedLanguagesCD.ShowAsync();
        }

        private async void VrAddLanguageCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
            await vrGetSupportedLanguagesCD.ShowAsync();
        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}