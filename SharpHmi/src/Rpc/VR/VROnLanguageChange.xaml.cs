﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutGoingNotifications;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VROnLanguageChange : UserControl
    {
        OnLanguageChange tmpObj;
        public VROnLanguageChange()
        {
            this.InitializeComponent();

            LanguageComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Language));
            LanguageComboBox.SelectedIndex = 0;
        }

        public async void ShowOnLanguageChange()
        {
            ContentDialog vrOnLanguageChangeCD = new ContentDialog();
            vrOnLanguageChangeCD.Content = this;

            vrOnLanguageChangeCD.PrimaryButtonText = Const.TxNow;
            vrOnLanguageChangeCD.PrimaryButtonClick += VrOnLanguageChangeCD_PrimaryButtonClick;

            vrOnLanguageChangeCD.SecondaryButtonText = Const.Reset;
            vrOnLanguageChangeCD.SecondaryButtonClick += VrOnLanguageChangeCD_SecondaryButtonClick;

            vrOnLanguageChangeCD.CloseButtonText = Const.Close;
            vrOnLanguageChangeCD.CloseButtonClick += VrOnLanguageChangeCD_CloseButtonClick;

            tmpObj = new OnLanguageChange();
            tmpObj = (OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<OnLanguageChange>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnLanguageChange);
                tmpObj = (OnLanguageChange)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (tmpObj.getLanguage() != null)
                {
                    LanguageComboBox.SelectedIndex = (int)tmpObj.getLanguage();
                }
                else
                {
                    LanguageComboBox.IsEnabled = false;
                    LanguageCheckBox.IsChecked = false;
                }
            }

            await vrOnLanguageChangeCD.ShowAsync();
        }

        private void VrOnLanguageChangeCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Language? language = null;
            if (LanguageCheckBox.IsChecked == true)
            {
                language = (HmiApiLib.Common.Enums.Language)LanguageComboBox.SelectedIndex;
            }
            RequestNotifyMessage rpcNotification = BuildRpc.buildVROnLanguageChange(language);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void VrOnLanguageChangeCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrOnLanguageChangeCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void LanguageCBCheckedChange(object sender, TappedRoutedEventArgs e)
        {
            LanguageComboBox.IsEnabled = (bool)LanguageCheckBox.IsChecked;
        }
    }
}
