﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRStarted : UserControl
    {
        public VRStarted()
        {
            this.InitializeComponent();
        }

        public async void ShowStarted()
        {
            ContentDialog vrStartedCD = new ContentDialog();
            vrStartedCD.Content = this;

            vrStartedCD.PrimaryButtonText = Const.TxNow;
            vrStartedCD.PrimaryButtonClick += VrStartedCD_PrimaryButtonClick;

            vrStartedCD.SecondaryButtonText = Const.Reset;
            vrStartedCD.SecondaryButtonClick += VrStartedCD_SecondaryButtonClick;

            vrStartedCD.CloseButtonText = Const.Close;
            vrStartedCD.CloseButtonClick += VrStartedCD_CloseButtonClick;

            await vrStartedCD.ShowAsync();
        }

        private void VrStartedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildVRStartedNotification();
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void VrStartedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void VrStartedCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }
    }
}
