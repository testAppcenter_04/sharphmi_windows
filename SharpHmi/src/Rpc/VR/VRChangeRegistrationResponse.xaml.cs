﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRChangeRegistrationResponse : UserControl
    {
        ChangeRegistration tmpObj;
        public VRChangeRegistrationResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowChangeRegistration()
        {
            ContentDialog vrChangeRegistrationCD = new ContentDialog();
            vrChangeRegistrationCD.Content = this;

            vrChangeRegistrationCD.PrimaryButtonText = Const.TxLater;
            vrChangeRegistrationCD.PrimaryButtonClick += VrChangeRegistrationCD_PrimaryButtonClick;

            vrChangeRegistrationCD.SecondaryButtonText = Const.Reset;
            vrChangeRegistrationCD.SecondaryButtonClick += VrChangeRegistrationCD_SecondaryButtonClick;

            vrChangeRegistrationCD.CloseButtonText = Const.Close;
            vrChangeRegistrationCD.CloseButtonClick += VrChangeRegistrationCD_CloseButtonClick;

            tmpObj = new ChangeRegistration();
            tmpObj = (ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<ChangeRegistration>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(ChangeRegistration);
                tmpObj = (ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await vrChangeRegistrationCD.ShowAsync();
        }

        private void VrChangeRegistrationCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            HmiApiLib.Common.Enums.Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (HmiApiLib.Common.Enums.Result)ResultCodeComboBox.SelectedIndex;
            }
            RpcResponse rpcResponse = BuildRpc.buildVrChangeRegistrationResponse(
                BuildRpc.getNextId(), resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrChangeRegistrationCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrChangeRegistrationCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }
    }
}
