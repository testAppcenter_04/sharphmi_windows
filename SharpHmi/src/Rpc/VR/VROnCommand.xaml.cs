﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HmiApiLib.Controllers.VR.OutGoingNotifications;
using System.Collections.ObjectModel;
using SharpHmi.src.Models;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;
using HmiApiLib.Base;
using System.Collections.Generic;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VROnCommand : UserControl
    {
        OnCommand tmpObj;

        private ObservableCollection<int> cmdIdObservableCollection = new ObservableCollection<int>();
        private ObservableCollection<AppItem> appListObservableCollection = new ObservableCollection<AppItem>(AppInstanceManager.AppInstance.appList);

        public VROnCommand()
        {
            InitializeComponent();

            if (AppInstanceManager.AppInstance.commandIdList.Count > 0)
            {
                List<int?> completeList = new List<int?>();
                foreach (int i in AppInstanceManager.AppInstance.commandIdList.Keys)
                {
                    completeList.AddRange(AppInstanceManager.AppInstance.commandIdList[i]);
                }

                foreach (int i in completeList)
                    cmdIdObservableCollection.Add(i);
            }

            CommandIdComboBox.ItemsSource = cmdIdObservableCollection;
            if (cmdIdObservableCollection.Count > 0)
                CommandIdComboBox.SelectedIndex = 0;

            RegisteredAppIdComboBox.ItemsSource = appListObservableCollection;
            if (appListObservableCollection.Count > 0)
                RegisteredAppIdComboBox.SelectedIndex = 0;
        }

        public async void ShowOnCommand()
        {
            ContentDialog vrOnCommandCD = new ContentDialog();
            vrOnCommandCD.Content = this;

            vrOnCommandCD.PrimaryButtonText = Const.TxNow;
            vrOnCommandCD.PrimaryButtonClick += VrOnCommandCD_PrimaryButtonClick;

            vrOnCommandCD.SecondaryButtonText = Const.Reset;
            vrOnCommandCD.SecondaryButtonClick += VrOnCommandCD_SecondaryButtonClick;

            vrOnCommandCD.CloseButtonText = Const.Close;
            vrOnCommandCD.CloseButtonClick += VrOnCommandCD_CloseButtonClick;

            tmpObj = new OnCommand();
            tmpObj = (OnCommand)AppUtils.getSavedPreferenceValueForRpc<OnCommand>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(OnCommand);
                tmpObj = (OnCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (null != tmpObj)
            {
                if (null != tmpObj.getCmdID())
                    CommandIdComboBox.SelectedIndex = cmdIdObservableCollection.IndexOf((int)tmpObj.getCmdID());
                else {
                    CommandIdComboBox.IsEnabled = false;
                    CommandIdCheckBox.IsChecked = false;
                }

                if (null != tmpObj.getAppId())
                    ManualAppIdTextBox.Text = tmpObj.getAppId().ToString();
                else {
                    ManualAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdRadioButton.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = false;
                    ManualAppIdTextBox.IsEnabled = false;
                    AppIdCheckBox.IsChecked = false;

                }
            }

            await vrOnCommandCD.ShowAsync();
        }

        private void VrOnCommandCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            int? selectedAppID = null;
            if (AppIdCheckBox.IsChecked == true)
            {
                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    try
                    {
                        selectedAppID = Int32.Parse(ManualAppIdTextBox.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (RegisteredAppIdRadioButton.IsChecked == true)
                {
                    if (AppInstanceManager.AppInstance.appList.Count != 0)
                    {
                        selectedAppID = appListObservableCollection[RegisteredAppIdComboBox.SelectedIndex].getApplication().getAppID();
                    }
                }
            }

            int? cmdId = null;
            if (CommandIdCheckBox.IsChecked == true && CommandIdComboBox.SelectedIndex != -1)
                cmdId = cmdIdObservableCollection.ElementAt(CommandIdComboBox.SelectedIndex);

            RequestNotifyMessage rpcNotification = BuildRpc.buildVROnCommand(cmdId, selectedAppID);
            AppUtils.savePreferenceValueForRpc(rpcNotification.getMethod(), rpcNotification);
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);
        }

        private void VrOnCommandCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrOnCommandCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void CommandIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            CommandIdComboBox.IsEnabled = (bool)CommandIdCheckBox.IsChecked;
        }

        private void AppIdCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            if (AppIdCheckBox.IsChecked == true)
            {
                ManualAppIdRadioButton.IsEnabled = true;
                RegisteredAppIdRadioButton.IsEnabled = true;

                if (ManualAppIdRadioButton.IsChecked == true)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
                else
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                ManualAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdRadioButton.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = false;
            }
        }

        private void ManualAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (ManualAppIdRadioButton.IsChecked == true)
            {
                if (ManualAppIdTextBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = true;
                    RegisteredAppIdComboBox.IsEnabled = false;
                }
            }
            else
            {
                ManualAppIdTextBox.IsEnabled = false;
                RegisteredAppIdComboBox.IsEnabled = true;
            }
        }

        private void RegisteredAppIdRadioButtonCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (RegisteredAppIdRadioButton.IsChecked == true)
            {
                if (RegisteredAppIdComboBox != null)
                {
                    ManualAppIdTextBox.IsEnabled = false;
                    RegisteredAppIdComboBox.IsEnabled = true;
                }
            }
            else
            {
                RegisteredAppIdComboBox.IsEnabled = false;
                ManualAppIdTextBox.IsEnabled = true;
            }
        }
    }
}