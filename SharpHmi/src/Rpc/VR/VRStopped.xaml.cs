﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Builder;
using SharpHmi.src.Utility;
using HmiApiLib.Base;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRStopped : UserControl
    {
        public VRStopped()
        {
            this.InitializeComponent();
        }

        public async void ShowStopped()
        {
            ContentDialog vrStoppedCD = new ContentDialog();
            vrStoppedCD.Content = this;

            vrStoppedCD.PrimaryButtonText = Const.TxNow;
            vrStoppedCD.PrimaryButtonClick += VrStoppedCD_PrimaryButtonClick;

            vrStoppedCD.SecondaryButtonText = Const.Reset;
            vrStoppedCD.SecondaryButtonClick += VrStoppedCD_SecondaryButtonClick;

            vrStoppedCD.CloseButtonText = Const.Close;
            vrStoppedCD.CloseButtonClick += VrStoppedCD_CloseButtonClick;

            await vrStoppedCD.ShowAsync();
        }

        private void VrStoppedCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            RequestNotifyMessage rpcNotification = BuildRpc.buildVRStoppedNotification();
            AppInstanceManager.AppInstance.sendRpc(rpcNotification);

        }

        private void VrStoppedCD_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }

        private void VrStoppedCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            
        }
    }
}
