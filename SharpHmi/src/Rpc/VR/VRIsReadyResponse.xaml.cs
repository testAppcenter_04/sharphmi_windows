﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Controllers.VR.OutgoingResponses;
using HmiApiLib.Common.Enums;
using SharpHmi.src.Utility;
using HmiApiLib.Builder;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SharpHmi.src.Rpc.VR
{
    public sealed partial class VRIsReadyResponse : UserControl
    {
        IsReady tmpObj = null;

        public VRIsReadyResponse()
        {
            this.InitializeComponent();
            ResultCodeComboBox.ItemsSource = Enum.GetNames(typeof(Result));
            ResultCodeComboBox.SelectedIndex = 0;
        }

        public async void ShowIsReady()
        {
            ContentDialog vrIsReadyCD = new ContentDialog();
            vrIsReadyCD.Content = this;

            vrIsReadyCD.PrimaryButtonText = Const.TxLater;
            vrIsReadyCD.PrimaryButtonClick += VrIsReadyCD_PrimaryButtonClick;

            vrIsReadyCD.SecondaryButtonText = Const.Reset;
            vrIsReadyCD.SecondaryButtonClick += VrIsReadyCD_ResetButtonClick;

            vrIsReadyCD.CloseButtonText = Const.Close;
            vrIsReadyCD.CloseButtonClick += VrIsReadyCD_CloseButtonClick;

            tmpObj = new IsReady();
            tmpObj = (IsReady)AppUtils.getSavedPreferenceValueForRpc<IsReady>(tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(IsReady);
                tmpObj = (IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (null != tmpObj)
            {
                if (tmpObj.getAvailable() == null)
                {
                    AvailableCheckBox.IsChecked = false;
                    AvailableToggle.IsEnabled = false;
                }
                else
                {
                    AvailableToggle.IsOn = (bool)tmpObj.getAvailable();
                }
                ResultCodeComboBox.SelectedIndex = (int)tmpObj.getResultCode();
            }

            await vrIsReadyCD.ShowAsync();
        }

        private void VrIsReadyCD_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Result? resultCode = null;
            if (ResultCodeCheckBox.IsChecked == true)
            {
                resultCode = (Result)ResultCodeComboBox.SelectedIndex;
            }
            bool? toggle = null;
            if (AvailableCheckBox.IsChecked == true)
            {
                toggle = AvailableToggle.IsOn;
            }

            HmiApiLib.Base.RpcResponse rpcResponse = BuildRpc.buildIsReadyResponse(
                BuildRpc.getNextId(), HmiApiLib.Types.InterfaceType.VR, toggle, resultCode);
            AppUtils.savePreferenceValueForRpc(rpcResponse.getMethod(), rpcResponse);
        }

        private void VrIsReadyCD_ResetButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (null != tmpObj)
            {
                AppUtils.RemoveSavedPreferencesValueForRpc(tmpObj.getMethod());
            }
        }

        private void VrIsReadyCD_CloseButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

        }

        private void ResultCodeCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            ResultCodeComboBox.IsEnabled = (bool)ResultCodeCheckBox.IsChecked;
        }

        private void AvailableCBCheckedChanged(object sender, TappedRoutedEventArgs e)
        {
            AvailableToggle.IsEnabled = (bool)AvailableCheckBox.IsChecked;
        }
    }
}
