
using HmiApiLib;
using HmiApiLib.Common.Structs;
using SharpHmi.src.interfaces;
using SharpHmi.src.Utility;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HmiApiLib.Common.Enums;
using SharpHmi.src.Models;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;

namespace SharpHmi
{
    public sealed partial class ProjectionTemplate : Page, IRefreshListener
    {
        int touchId = 0;
        public static string media = "";
        private bool formShowing = false;

        public ProjectionTemplate()
        {
            this.InitializeComponent();
            AppInstanceManager.AppInstance.addRefreshListener(this, typeof(OnButtonSubscription));
            AppInstanceManager.AppInstance.addRefreshListener(this, typeof(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand));
            AppInstanceManager.AppInstance.addRefreshListener(this, typeof(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand));
            AppInstanceManager.AppInstance.addRefreshListener(this, typeof(OnAppUnregistered));
            
            AppInstanceManager.mediaElement = MainPage.vidPlayer;
            MainPage.vidPlayer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            MainPage.vidPlayer.AutoPlay = false;
            MainPage.vidPlayer.Background = new SolidColorBrush(Windows.UI.Colors.Black);
            MainPage.vidPlayer.Width = 800;
            MainPage.vidPlayer.Height = 480;
            MainPage.vidPlayer.HardwareAcceleration = true;
            media = "http://" + AppInstanceManager.AppInstance.getAppSetting().getIPAddress() + ":" + AppInstanceManager.AppInstance.getAppSetting().getVideoPort();

            Dictionary<string, object> options = new Dictionary<string, object>();
            options.Add("demux", "h264");
            options.Add("h264-fps", "60");
            options.Add("clock-jitter", "0");
            options.Add("clock-synchro", "1");
            options.Add("network-caching", "240");

            MainPage.vidPlayer.Options = options;

            MainPage.vidPlayer.Source = media;
            setTouchListeners();       

            searchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private void HmiMediaPlayer_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (MainPage.vidPlayer == null) return;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent myTouchEvent = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();

            TouchCoord touchCoord = new TouchCoord();
            touchCoord.x = (int)e.GetPosition(MainPage.vidPlayer).X;
            touchCoord.y = (int)e.GetPosition(MainPage.vidPlayer).Y;


            List<TouchCoord> touchCoordTappedList = new List<TouchCoord>();
            touchCoordTappedList.Add(touchCoord);

            List<TouchEvent> touchEventsTappedList = new List<TouchEvent>();

            TouchEvent touchEvent = new TouchEvent();
            touchEvent.c = touchCoordTappedList;
            touchEvent.id = touchId;

            List<int> tsList = new List<int>();
            tsList.Add((int)DateTimeOffset.Now.ToUnixTimeSeconds());

            touchEvent.ts = tsList;

            touchEventsTappedList.Add(touchEvent);

            myTouchEvent.setTouchEvent(touchEventsTappedList);

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.BEGIN);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.MOVE);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.END);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);
        }

        private void HmiMediaPlayer_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            if (MainPage.vidPlayer == null) return;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent myTouchEvent = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.BEGIN);
            TouchCoord touchCoord = new TouchCoord();
            touchCoord.x = (int)e.Position.X;
            touchCoord.y = (int)e.Position.Y;


            List<TouchCoord> touchCoordStartedList = new List<TouchCoord>();
            touchCoordStartedList.Add(touchCoord);

            List<TouchEvent> touchEventsStartedList = new List<TouchEvent>();

            TouchEvent touchEvent = new TouchEvent();
            touchEvent.c = touchCoordStartedList;
            touchEvent.id = touchId;

            List<int> tsList = new List<int>();
            tsList.Add((int)DateTimeOffset.Now.ToUnixTimeSeconds());

            touchEvent.ts = tsList;

            touchEventsStartedList.Add(touchEvent);

            myTouchEvent.setTouchEvent(touchEventsStartedList);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);
        }

        private void PageLayoutGrid_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (MainPage.vidPlayer == null) return;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent myTouchEvent = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.MOVE);
            TouchCoord touchCoord = new TouchCoord();
            touchCoord.x = (int)e.Position.X;
            touchCoord.y = (int)e.Position.Y;


            List<TouchCoord> touchCoordMovingList = new List<TouchCoord>();
            touchCoordMovingList.Add(touchCoord);

            List<TouchEvent> touchEventsMovingList = new List<TouchEvent>();

            TouchEvent touchEvent = new TouchEvent();
            touchEvent.c = touchCoordMovingList;
            touchEvent.id = touchId;

            List<int> tsList = new List<int>();
            tsList.Add((int)DateTimeOffset.Now.ToUnixTimeSeconds());

            touchEvent.ts = tsList;

            touchEventsMovingList.Add(touchEvent);

            myTouchEvent.setTouchEvent(touchEventsMovingList);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);
        }

        private void LayoutManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            if (MainPage.vidPlayer == null) return;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent myTouchEvent = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();

            myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.END);

            TouchCoord touchCoord = new TouchCoord();
            touchCoord.x = (int)e.Position.X;
            touchCoord.y = (int)e.Position.Y;


            List<TouchCoord> touchCoordCompletedList = new List<TouchCoord>();
            touchCoordCompletedList.Add(touchCoord);

            List<TouchEvent> touchEventsCompletedList = new List<TouchEvent>();

            TouchEvent touchEvent = new TouchEvent();
            touchEvent.c = touchCoordCompletedList;
            touchEvent.id = touchId;

            List<int> tsList = new List<int>();
            tsList.Add((int)DateTimeOffset.Now.ToUnixTimeSeconds());

            touchEvent.ts = tsList;

            touchEventsCompletedList.Add(touchEvent);

            myTouchEvent.setTouchEvent(touchEventsCompletedList);
            AppInstanceManager.AppInstance.sendRpc(myTouchEvent);
        }

        public void setTouchListeners()
        {
            if (MainPage.vidPlayer != null)
            {
                MainPage.vidPlayer.ManipulationMode = ManipulationModes.TranslateX;
                MainPage.vidPlayer.Tapped += HmiMediaPlayer_Tapped;
                MainPage.vidPlayer.ManipulationStarted += HmiMediaPlayer_ManipulationStarted;
                MainPage.vidPlayer.ManipulationDelta += PageLayoutGrid_ManipulationDelta;
                MainPage.vidPlayer.ManipulationCompleted += LayoutManipulationCompleted;
            }
        }

        private void MenuClicked(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(src.MainScreen.MenuItems), AppInstanceManager.activatedAppId);
            }
            catch (Exception ex) { }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            formShowing = true;
            if (AppInstanceManager.lastProjectedAppId > 0)
            {
                appName.Visibility = Windows.UI.Xaml.Visibility.Visible;
                appName.Text = AppInstanceManager.activatedAppName;
                handleButtonsSubscription();
                handleMenuRefresh();
                MainPage.vidPlayer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                appName.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                appName.Text = "";
                searchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Menu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                MainPage.vidPlayer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            return;
        }                      

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            formShowing = false;
            MainPage.vidPlayer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
       
        private void SearchButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent searchButtonDown = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent();
            searchButtonDown.setName(ButtonName.SEARCH);
            searchButtonDown.setMode(ButtonEventMode.BUTTONDOWN);
            searchButtonDown.setAppId(AppInstanceManager.activatedAppId);
            searchButtonDown.setCustomButtonID(null);
            AppInstanceManager.AppInstance.sendRpc(searchButtonDown);

            HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent searchButtonUp = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent();
            searchButtonUp.setName(ButtonName.SEARCH);
            searchButtonUp.setMode(ButtonEventMode.BUTTONUP);
            searchButtonUp.setAppId(AppInstanceManager.activatedAppId);
            searchButtonUp.setCustomButtonID(null);
            AppInstanceManager.AppInstance.sendRpc(searchButtonUp);

            HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress searchButtonPress = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress();

            searchButtonPress.setName(ButtonName.SEARCH);
            searchButtonPress.setMode(ButtonPressMode.SHORT);
            searchButtonPress.setAppId(AppInstanceManager.activatedAppId);
            searchButtonPress.setCustomButtonID(null);
            AppInstanceManager.AppInstance.sendRpc(searchButtonPress);            
        }
        public async void subscribeToSearch(bool subscribed)
        {
            if (subscribed)
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                 {
                     searchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;

                 });
            }
            else
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    searchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                });
            }
        }

        public void handleButtonsSubscription()
        {
            subscribeToSearch(false);
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.lastProjectedAppId);
            if (appItem == null)
                return;

            if (appItem.getButtonSubscriptions() != null && appItem.getButtonSubscriptions().Count > 0)
            {
                foreach (OnButtonSubscription onButtonSubscription in appItem.getButtonSubscriptions())
                {
                    if (onButtonSubscription.getName() == ButtonName.SEARCH)
                    {
                        subscribeToSearch((bool)onButtonSubscription.getSubscribe());
                    }
                }
            }
        }

        public async void handleMenuRefresh()
        {
            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                Menu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.lastProjectedAppId);
                if (appItem == null)
                    return;
                if (appItem.getMenuCommandsList().Count > 0)
                    Menu.Visibility = Windows.UI.Xaml.Visibility.Visible;
            });
        }

        public void backToAppPage()
        {
            if (AppInstanceManager.AppInstance != null && formShowing) //no need to refresh the app list if the app is not currently showing (e.g. navigated to this form)
            {
                AppInstanceManager.AppInstance.onRefreshRegisteredApps();
            }
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState dataState)
        {
            if (appID != AppInstanceManager.lastProjectedAppId)
                return;

            if (rpcType == typeof(OnButtonSubscription))
                handleButtonsSubscription();
            else if (rpcType == typeof(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand))
                handleMenuRefresh();
            else if (rpcType == typeof(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand))
                handleMenuRefresh();
            else if (rpcType == typeof(OnAppUnregistered))
                backToAppPage();
        }
    }
}
