﻿using System;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using Windows.UI.Xaml;
using SharpHmi.src.Models;
using Windows.UI.Xaml.Controls;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Media.Imaging;
using HmiApiLib;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using System.Linq;
using HmiApiLib.Builder;
using HmiApiLib.Base;
using Windows.UI.Xaml.Input;
using SharpHmi.src.Utility;
using SharpHmi.src.interfaces;
using SharpHmi.src.CachedRPC;
using static SharpHmi.src.Manager.GraphicManager;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;

namespace SharpHmi.src.MainScreen
{
    public partial class MediaTemplate : IRefreshListener
    {
        public int[] softButtonsId = new int[8];
        public bool[] softButtonHighlighted = new bool[8];

        AppInstanceManager appInstanceManager;        
        public static Frame frame;
        public static Type whatpageisit;

        public static DispatcherTimer timer;

        string displayLayout = "DEFAULT";
        Action<OutputGraphic> mainGraphicDelegate = null;
        Action<OutputGraphic> menuGraphicDelegate = null;
        Action<OutputGraphic> iconGraphicDelegate = null;

        private bool formShowing = false;


        public MediaTemplate()
        {
            InitializeComponent();

            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;

            appInstanceManager = AppInstanceManager.AppInstance;
            this.DataContext = this;

            appInstanceManager.addRefreshListener(this, typeof(Show));
            appInstanceManager.addRefreshListener(this, typeof(CachedShow));
            appInstanceManager.addRefreshListener(this, typeof(OnButtonSubscription));
            appInstanceManager.addRefreshListener(this, typeof(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties));
            appInstanceManager.addRefreshListener(this, typeof(SetAppIcon));
            appInstanceManager.addRefreshListener(this, typeof(SetDisplayLayout));
            appInstanceManager.addRefreshListener(this, typeof(SetMediaClockTimer));
            appInstanceManager.addRefreshListener(this, typeof(OnAppUnregistered));

            //wait and listen for updates for our main graphic bitmap from putfile, this is our main graphic
            mainGraphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != AppInstanceManager.activatedAppId) return;

                    Windows.UI.Xaml.Controls.Image image = (Windows.UI.Xaml.Controls.Image) graphicStruct.holder;
                    image.Visibility = Visibility.Visible;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.Source = bitmapUpdate;
                    //hide the static graphic since dynamic is visible
                    MainStaticIcon1.Visibility = Visibility.Collapsed;
                }
            };

            //wait and listen for updates for our main graphic bitmap from putfile, this is our menu graphic
            menuGraphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != AppInstanceManager.activatedAppId) return;
                    Windows.UI.Xaml.Controls.Image image = (Windows.UI.Xaml.Controls.Image)graphicStruct.holder;
                    image.Visibility = Visibility.Visible;
                    //hide our static icon since dynamic is visible
                    MenuIcon.Visibility = Visibility.Collapsed;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.Source = bitmapUpdate;
                }
            };
        }

        public void initTimer()
        {
            timer = new DispatcherTimer();
        }

        public void clearTimer()
        {
            if (timer != null)
            {
                timer.Tick -= Timer_Tick;
                timer.Stop();
                timer = null;
            }
        }

        public async void onUiSetGlobalPropertiesCallback()
        {
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

            if (appItem == null) return;

            SetGlobalProperties msg = appItem.getGlobalProperties();

            if (msg == null) return;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                if (msg.getMenuIcon() != null)
                {

                    try
                    {
                        if (msg.getMenuIcon().getImageType() == ImageType.DYNAMIC)
                        {
                            //try to get the currently cached bitmap
                            BitmapImage myImage = appItem.graphicManager.getGraphic(msg.getMenuIcon().getValue());
                            if (myImage != null)
                            {
                                //hide the static icon
                                MenuIcon.Visibility = Visibility.Collapsed;
                                //show the dynamic icon
                                MenuImage.Visibility = Visibility.Visible;
                                MenuImage.Source = myImage;
                            }
                            InputGraphic inputGraphic = new InputGraphic();
                            inputGraphic.holder = MenuImage;
                            inputGraphic.fileName = msg.getMenuIcon().getValue();
                            //subscribe for updates for our main graphic bitmap from putfile
                            appItem.graphicManager.addRefreshListener(menuGraphicDelegate, inputGraphic);
                        }
                        else
                        {
                            string icon = msg.getMenuIcon().getValue();
                            string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(icon);

                            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                            {
                                MenuImage.Visibility = Visibility.Collapsed;
                                MenuIcon.Visibility = Visibility.Visible;

                                if (staticImage != "" && null != staticImage)
                                {
                                    MenuIcon.Text = staticImage;
                                }
                                else
                                {
                                    MenuIcon.Text = "\uEB9F";
                                }
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }

                if (msg.getMenuTitle() != null)
                {
                    MenuTitle.Text = msg.getMenuTitle();
                }
            });
        }

        public void clearMediaClockDisplay()
        {
            ClearTimerUIElements();
            clearTimer();
        }

        private void ClearTimerUIElements()
        {
            mediaStartText.Visibility = Visibility.Collapsed;
            mediaEndText.Visibility = Visibility.Collapsed;
            mediaSlider.Visibility = Visibility.Collapsed;
            mediaClockText.Visibility = Visibility.Collapsed;
            mediaClockText.Text = "";
            mediaSlider.IsEnabled = false;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            formShowing = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (MainPage.appsMenuListItem != null)
            {
                MainPage.appsMenuListItem.IsSelected = true;
            }
            formShowing = true;
            try
            {
                var parameters = (AppItem)e.Parameter;
                appName.Text = parameters.getApplication().getAppName();

                AppInstanceManager.activatedAppId = (int)parameters.getApplication().getAppID();
                AppInstanceManager.activatedAppName = appName.Text;
                if (parameters.getDynamicIconVisisble() == Visibility.Visible)
                {
                    StaticIcon.Visibility = Visibility.Collapsed;
                    IconImage.Source = parameters.appsIcon;
                    IconImage.Visibility = Visibility.Visible;
                }
                else if (parameters.getStaticIconVisisble() == Visibility.Visible)
                {
                    IconImage.Visibility = Visibility.Collapsed;
                    StaticIcon.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            setupAppIconListner();
            checkForMediaClockTimer();
            initializeButtonSubscribed();
            clearMediaTemplate();
            displayCachedShow();
            //todo create cached setglobal properties to display menu image next

        }

        public void setupAppIconListner()
        {
            //wait and listen for updates for our app icon graphic bitmap from putfile, this is our menu graphic
            iconGraphicDelegate = delegate (OutputGraphic graphicStruct)
            {
                if (graphicStruct.holder != null && graphicStruct.appItem != null)
                {
                    if (graphicStruct.appItem.getAppId() != AppInstanceManager.activatedAppId) return;
                    Windows.UI.Xaml.Controls.Image image = (Windows.UI.Xaml.Controls.Image)graphicStruct.holder;
                    image.Visibility = Visibility.Visible;
                    //hide our static icon since dynamic is visible
                    MenuIcon.Visibility = Visibility.Collapsed;
                    BitmapImage bitmapUpdate = graphicStruct.graphic;
                    image.Source = bitmapUpdate;
                }
            };

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

            if (appItem == null || appItem.getSetAppIcon() == null || appItem.getSetAppIcon().getAppIcon() == null || appItem.getSetAppIcon().getAppIcon().value == null)
                return;

            InputGraphic inputGraphic = new InputGraphic();
            inputGraphic.holder = IconImage;
            inputGraphic.fileName = appItem.getSetAppIcon().getAppIcon().value;

            //subscribe for updates for our main graphic bitmap from putfile
            appItem.graphicManager.addRefreshListener(iconGraphicDelegate, inputGraphic);
        }

        public async void displayCachedShow()
        {            
            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);
            CachedShow cachedShow = appItem.getCachedShow();

            if (cachedShow.getShowStrings() == null) return;

            if (cachedShow.getSoftButtons() != null && cachedShow.getSoftButtons().Count == 0) //if our softbutton list was empty, clear all softbuttons
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    clearSoftButtons();
                });                    
            }

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                //display all the text fields, make them visibile only when needed
                foreach (TextFieldStruct tfs in cachedShow.getShowStrings())
                {
                    switch (tfs.fieldName)
                    {
                        case TextFieldName.mediaClock:
                            if (tfs.fieldText != null && tfs.fieldText.Trim().Length > 0
                                && (appItem != null)
                                && ((appItem.getMediaClock() == null) || appItem.getMediaClock().getMediaClockTimer() == null))
                            {
                                clearMediaClockDisplay();
                                mediaClockText.Visibility = Visibility.Visible;
                                mediaClockText.Text = String.Format(tfs.fieldText);
                            }
                            break;
                        case TextFieldName.mainField1:
                            ShowMainField1.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.mainField2:
                            ShowMainField2.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.mainField3:
                            ShowMainField3.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.mainField4:
                            ShowMainField4.Text = String.Format(tfs.fieldText);
                            break;
                        case TextFieldName.mediaTrack:
                            ShowMediaTrack.Text = String.Format(tfs.fieldText);
                            break;
                        default:
                            break;
                    }
                }
                //handle the dynamic graphics from our show request
                if (cachedShow != null && cachedShow.getGraphic() != null && cachedShow.getGraphic().getValue() != "" && cachedShow.getGraphic().getImageType() == ImageType.DYNAMIC)
                {
                    //clear and hide the static icon
                    MainStaticIcon1.Text = "";
                    MainStaticIcon1.Visibility = Visibility.Collapsed;

                    //try to get the currently cached bitmap
                    BitmapImage myImage = appItem.graphicManager.getGraphic(cachedShow.getGraphic().getValue());
                    if (myImage != null)
                    {
                        MainIcon1.Visibility = Visibility.Visible;
                        MainIcon1.Source = myImage;
                    }

                    InputGraphic inputGraphic = new InputGraphic();
                    inputGraphic.holder = MainIcon1;
                    inputGraphic.fileName = cachedShow.getGraphic().getValue();

                    //subscribe for updates for our main graphic bitmap from putfile
                    appItem.graphicManager.addRefreshListener(mainGraphicDelegate, inputGraphic);

                }
                //handle the static graphics from our show request
                else if (cachedShow != null && cachedShow.getGraphic() != null && cachedShow.getGraphic().getValue() != "" && cachedShow.getGraphic().getImageType() == ImageType.STATIC)
                {
                    //clear and hide the dynamic icon
                    MainIcon1.Source = null;
                    MainIcon1.Visibility = Visibility.Collapsed;

                    string icon = cachedShow.getGraphic().getValue();
                    string staticImage = (string)AppInstanceManager.AppInstance.staticImageVal(icon);

                    MainStaticIcon1.Visibility = Visibility.Visible;

                    if (staticImage != "" && staticImage != null)
                    {
                        MainStaticIcon1.Text = staticImage;
                    }
                    else
                    {
                        MainStaticIcon1.Text = "\uEB9F";
                    }
                }

            });

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => 
            {
                //update the softbuttons from our show request
                displaySoftButtons(appItem);
            });
        }

        public void checkForMediaClockTimer()
        {
            onUiSetMediaClockTimerRequestCallback();
        }
        
        public async void onUiSetMediaClockTimerRequestCallback()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => updateSetMediaTimerUI());

        }

        private void Timer_Tick(object sender, object e)
        {
            mediaClockTimerCallback();
        }

        private void updateSetMediaTimerUI()
        {
            clearMediaClockDisplay();

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

            if (appItem != null && appItem.getMediaClock() != null && appItem.getMediaClock().getMediaClockTimer() != null)
            {
                int? startSeconds = null;
                int? endSeconds = null;
                string startText = "";
                string endText = "";

                initTimer();

                if (null != appItem.getMediaClock().getMediaClockTimer().getStartTime())
                {
                    startText = AppUtils.getMediaClockTimeText(appItem.getMediaClock().getMediaClockTimer().getStartTime());
                    startSeconds = AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getStartTime());

                    if (appItem.getMediaClock().getCurTime() == null)
                        appItem.getMediaClock().setCurTime(startSeconds);
                }

                if (null != appItem.getMediaClock().getMediaClockTimer().getEndTime())
                {
                    endText = AppUtils.getMediaClockTimeText(appItem.getMediaClock().getMediaClockTimer().getEndTime());
                    endSeconds = AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime());
                }

                if ((appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTUP) || (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTDOWN))
                {
                    if (((appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTUP)
                        && (appItem.getMediaClock().getMediaClockTimer().getEndTime() != null)
                        && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime()))
                        ||
                        ((appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTDOWN)
                        && (appItem.getMediaClock().getMediaClockTimer().getEndTime() != null)
                        && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime())))
                    {

                        if (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTUP)
                        {
                            if (endSeconds < startSeconds)
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (endSeconds > startSeconds)
                            {
                                return;
                            }
                        }

                        if (displayLayout.Equals("DEFAULT") || displayLayout.Equals("MEDIA"))
                        {
                            mediaSlider.Visibility = Visibility.Visible;
                            mediaEndText.Visibility = Visibility.Visible;
                            mediaStartText.Visibility = Visibility.Visible;
                        }
                    }
                    else if (((appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTDOWN)
                        && (appItem.getMediaClock().getMediaClockTimer().getEndTime() == null)
                        && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime()))
                        ||
                        ((appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.COUNTUP)
                        && (appItem.getMediaClock().getMediaClockTimer().getEndTime() == null)
                        && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime())))
                    {
                        mediaSlider.Visibility = Visibility.Collapsed;
                        mediaEndText.Visibility = Visibility.Collapsed;
                        if (displayLayout.Equals("DEFAULT") || displayLayout.Equals("MEDIA"))
                        {
                            mediaStartText.Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        return;
                    }

                    timer.Tick += Timer_Tick;
                    timer.Interval = new TimeSpan(0, 0, 1);
                    timer.Start();
                }
                else if (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.RESUME
                    || appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.PAUSE)
                {
                    if (startSeconds == null && endSeconds == null)
                    {
                        return;
                    }

                    if ((null != appItem.getMediaClock().getMediaClockTimer().getStartTime())
                        && (displayLayout.Equals("DEFAULT") || displayLayout.Equals("MEDIA")))
                    {
                        mediaStartText.Visibility = Visibility.Visible;
                    }

                    if ((null != appItem.getMediaClock().getMediaClockTimer().getEndTime())
                        && (displayLayout.Equals("DEFAULT") || displayLayout.Equals("MEDIA")))
                    {
                        mediaSlider.Visibility = Visibility.Visible;
                        mediaEndText.Visibility = Visibility.Visible;
                    }

                    timer.Tick += Timer_Tick;
                    timer.Interval = new TimeSpan(0, 0, 1);
                    timer.Start();
                }
                else if (appItem.getMediaClock().getMediaClockTimer().getUpdateMode() == ClockUpdateMode.CLEAR)
                {
                    clearMediaClockDisplay();
                    return;
                }
            }
        }

        void mediaClockTimerCallback()
        {
            try
            { 
                AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

                if (appItem != null && appItem.getMediaClock() != null 
                    && appItem.getMediaClock().getMediaClockTimer() != null
                    && timer != null)
                {
                    int? currentTime = -1;
                    double initialProgress = -1;

                    ClockUpdateMode clockUpdateMode = appItem.getMediaClock().getMediaClockTimer().getUpdateMode();

                    currentTime = appItem.getMediaClock().getCurTime();

                    if (clockUpdateMode == ClockUpdateMode.PAUSE)
                    {
                        appItem.getMediaClock().setCurTime(currentTime);

                        if ((appItem.getMediaClock().getMediaClockTimer().getEndTime() != null)
                            && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime()))
                        {
                            if (appItem.getMediaClock().getLastClockUpdateMode() == ClockUpdateMode.COUNTUP)
                            {
                                initialProgress = ((((double) currentTime) / AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime())) * 100);

                                mediaEndText.Text = "-" + AppUtils.getMediaClockTimeText(AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime()) - currentTime);
                            }
                            else
                            {
                                initialProgress = ((((double)currentTime) / AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getStartTime())) * 100);

                                mediaEndText.Text = "-" + AppUtils.getMediaClockTimeText(currentTime - AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime()));
                            }

                            mediaStartText.Text = AppUtils.getMediaClockTimeText(currentTime);

                            mediaSlider.Value = initialProgress;
                        }
                        else if ((appItem.getMediaClock().getMediaClockTimer().getEndTime() == null)
                            && (null != appItem.getMediaClock().getMediaClockTimer().getStartTime()))
                        {
                            mediaStartText.Text = AppUtils.getMediaClockTimeText(currentTime);
                        }

                        timer.Stop();
                    }
                    else if (clockUpdateMode == ClockUpdateMode.COUNTUP || clockUpdateMode == ClockUpdateMode.COUNTDOWN || clockUpdateMode == ClockUpdateMode.RESUME)
                    {
                        mediaStartText.Text = AppUtils.getMediaClockTimeText(currentTime);

                        if (appItem.getMediaClock().getMediaClockTimer().getEndTime() != null)
                        {
                            if (clockUpdateMode == ClockUpdateMode.COUNTUP || ((clockUpdateMode == ClockUpdateMode.RESUME) && (appItem.getMediaClock().getLastClockUpdateMode() == ClockUpdateMode.COUNTUP)))
                            {
                                initialProgress = ((((double)currentTime) / AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime())) * 100);

                                mediaEndText.Text = "-" + AppUtils.getMediaClockTimeText(AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime()) - currentTime);

                                if((currentTime - AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime())) >= 0)
                                {
                                    timer.Stop();
                                } else
                                {
                                    appItem.getMediaClock().setCurTime(appItem.getMediaClock().getCurTime() + 1);
                                }
                            }
                            else
                            {
                                initialProgress = ((((double)currentTime) / AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getStartTime())) * 100);

                                mediaEndText.Text = "-" + AppUtils.getMediaClockTimeText(currentTime - AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime()));

                                if ((currentTime - AppUtils.getMediaClockTime(appItem.getMediaClock().getMediaClockTimer().getEndTime())) <= 0)
                                {
                                    timer.Stop();
                                }
                                else
                                {
                                    appItem.getMediaClock().setCurTime(appItem.getMediaClock().getCurTime() - 1);
                                }
                            }
                            mediaSlider.Value = initialProgress;
                        } else
                        {
                            if (clockUpdateMode == ClockUpdateMode.COUNTUP || ((clockUpdateMode == ClockUpdateMode.RESUME) && (appItem.getMediaClock().getLastClockUpdateMode() == ClockUpdateMode.COUNTUP)))
                            {
                                appItem.getMediaClock().setCurTime(appItem.getMediaClock().getCurTime() + 1);
                            }
                            else
                            {
                                if (appItem.getMediaClock().getCurTime() <= 0)
                                {
                                    timer.Stop();
                                }
                                else
                                {
                                    appItem.getMediaClock().setCurTime(appItem.getMediaClock().getCurTime() - 1);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private async void presetsClick(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

                    Show showRequest = null;
                    if (appItem != null && appItem.getCachedShow() != null)
                    {
                        showRequest = appItem.getCachedShow();
                        if (showRequest == null) return;
                    }
                    else
                    {
                        return;
                    }

                    var navigateFrom = new PageNavigate()
                    {
                        customPresets = showRequest.getCustomPresets(),
                        buttonSubscription = appItem.getButtonSubscriptions(),
                        Info = appName.Text
                    };
                    Frame.Navigate(typeof(Preset), navigateFrom);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
        }

        public async void onUiSetAppIcon()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
            {
                AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);
                if (appItem != null)
                {
                    StaticIcon.Visibility = Visibility.Collapsed;
                    IconImage.Source = appItem.appsIcon;
                    IconImage.Visibility = Visibility.Visible;
                }
            });           
        }

        private void MenuClicked(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(MenuItems), AppInstanceManager.activatedAppId);
                frame = Window.Current.Content as Frame;

                whatpageisit = Frame.SourcePageType;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void iconClicked(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private void PlayPauseButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ButtonName centerButton = ButtonName.OK;

            foreach (OnButtonSubscription btnSubsc in AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId).getButtonSubscriptions())
            {
                if (btnSubsc.getName() == ButtonName.PLAY_PAUSE)
                {
                    centerButton = ButtonName.PLAY_PAUSE;
                    break;
                }
            }

            RequestNotifyMessage buttonDown = BuildRpc.buildButtonsOnButtonEvent(centerButton, ButtonEventMode.BUTTONDOWN, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonDown);

            RequestNotifyMessage buttonUp = BuildRpc.buildButtonsOnButtonEvent(centerButton, ButtonEventMode.BUTTONUP, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonUp);

            RequestNotifyMessage onOkShortPress = BuildRpc.buildButtonsOnButtonPress(centerButton, ButtonPressMode.SHORT, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(onOkShortPress);
        }

        private void NextButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            RequestNotifyMessage buttonDown = BuildRpc.buildButtonsOnButtonEvent(ButtonName.SEEKRIGHT, ButtonEventMode.BUTTONDOWN, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonDown);

            RequestNotifyMessage buttonUp = BuildRpc.buildButtonsOnButtonEvent(ButtonName.SEEKRIGHT, ButtonEventMode.BUTTONUP, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonUp);

            RequestNotifyMessage onSeekRightShortPress = BuildRpc.buildButtonsOnButtonPress(ButtonName.SEEKRIGHT, ButtonPressMode.SHORT, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(onSeekRightShortPress);
        }

        private void PreviousButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            RequestNotifyMessage buttonDown = BuildRpc.buildButtonsOnButtonEvent(ButtonName.SEEKLEFT, ButtonEventMode.BUTTONDOWN, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonDown);

            RequestNotifyMessage buttonUp = BuildRpc.buildButtonsOnButtonEvent(ButtonName.SEEKLEFT, ButtonEventMode.BUTTONUP, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(buttonUp);

            RequestNotifyMessage onSeekLeftShortPressUp = BuildRpc.buildButtonsOnButtonPress(ButtonName.SEEKLEFT, ButtonPressMode.SHORT, null, AppInstanceManager.activatedAppId);
            appInstanceManager.sendRpc(onSeekLeftShortPressUp);
        }

        private void SoftButton1_Click(object sender, RoutedEventArgs e)
        {
            SoftButton1.IsChecked = softButtonHighlighted[0];
        }

        private void SoftButton2_Click(object sender, RoutedEventArgs e)
        {
            SoftButton2.IsChecked = softButtonHighlighted[1];
        }

        private void SoftButton3_Click(object sender, RoutedEventArgs e)
        {
            SoftButton3.IsChecked = softButtonHighlighted[2];
        }

        private void SoftButton4_Click(object sender, RoutedEventArgs e)
        {
            SoftButton4.IsChecked = softButtonHighlighted[3];
        }

        private void SoftButton5_Click(object sender, RoutedEventArgs e)
        {
            SoftButton5.IsChecked = softButtonHighlighted[4];
        }

        private void SoftButton6_Click(object sender, RoutedEventArgs e)
        {
            SoftButton6.IsChecked = softButtonHighlighted[5];
        }

        private void SoftButton7_Click(object sender, RoutedEventArgs e)
        {
            SoftButton7.IsChecked = softButtonHighlighted[6];
        }

        private void SoftButton8_Click(object sender, RoutedEventArgs e)
        {
            SoftButton8.IsChecked = softButtonHighlighted[7];
        }

        private void SoftButton1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton1.IsChecked = softButtonHighlighted[0];
            try
            {
                RequestNotifyMessage buttonPress1Down = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[0], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress1Down);

                RequestNotifyMessage buttonPress1Up = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[0], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress1Up);

                RequestNotifyMessage buttonPress1 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[0], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton2.IsChecked = softButtonHighlighted[1];
            try
            {
                RequestNotifyMessage buttonPress2Down = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[1], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress2Down);

                RequestNotifyMessage buttonPress2Up = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[1], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress2Up);

                RequestNotifyMessage buttonPress2 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[1], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton3.IsChecked = softButtonHighlighted[2];
            try
            {
                RequestNotifyMessage buttonPress3Down = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[2], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress3Down);

                RequestNotifyMessage buttonPress3Up = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[2], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress3Up);

                RequestNotifyMessage buttonPress3 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[2], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress3);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton4.IsChecked = softButtonHighlighted[3];
            try
            {
                RequestNotifyMessage buttonPress4Down = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[3], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress4Down);

                RequestNotifyMessage buttonPress4Up = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[3], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress4Up);

                RequestNotifyMessage buttonPress4 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[3], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress4);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton5.IsChecked = softButtonHighlighted[4];
            try
            {
                RequestNotifyMessage buttonPress5Down = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[4], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress5Down);

                RequestNotifyMessage buttonPress5Up = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[4], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress5Up);

                RequestNotifyMessage buttonPress5 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[4], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress5);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton6.IsChecked = softButtonHighlighted[5];
            try
            {
                RequestNotifyMessage buttonPressDown = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[5], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressDown);

                RequestNotifyMessage buttonPressUp = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[5], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressUp);

                RequestNotifyMessage buttonPress6 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[5], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress6);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton7.IsChecked = softButtonHighlighted[6];
            try
            {
                RequestNotifyMessage buttonPressDown = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[6], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressDown);

                RequestNotifyMessage buttonPressUp = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[6], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressUp);

                RequestNotifyMessage buttonPress7 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[6], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress7);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SoftButton8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SoftButton8.IsChecked = softButtonHighlighted[7];
            try
            {
                RequestNotifyMessage buttonPressDown = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONDOWN, softButtonsId[7], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressDown);

                RequestNotifyMessage buttonPressUp = BuildRpc.buildButtonsOnButtonEvent(ButtonName.CUSTOM_BUTTON, ButtonEventMode.BUTTONUP, softButtonsId[7], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPressUp);

                RequestNotifyMessage buttonPress8 = BuildRpc.buildButtonsOnButtonPress(ButtonName.CUSTOM_BUTTON, ButtonPressMode.SHORT, softButtonsId[7], AppInstanceManager.activatedAppId);
                appInstanceManager.sendRpc(buttonPress8);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void clearMainFields()
        {
            mediaClockText.Text = "";
            ShowMainField1.Text = "";
            ShowMainField2.Text = "";
            ShowMainField3.Text = "";
            ShowMainField4.Text = "";
            ShowMediaTrack.Text = "";
        }

        private void clearMainGraphics()
        {
            MainStaticIcon1.Text = "";
            MainIcon1.Source = null;

            MainStaticIcon1.Visibility = Visibility.Collapsed;
            MainIcon1.Visibility = Visibility.Collapsed;
        }

        private void clearSoftButtons()
        {
            SoftButton1.Visibility = Visibility.Collapsed;
            SoftButton2.Visibility = Visibility.Collapsed;
            SoftButton3.Visibility = Visibility.Collapsed;
            SoftButton4.Visibility = Visibility.Collapsed;
            SoftButton5.Visibility = Visibility.Collapsed;
            SoftButton6.Visibility = Visibility.Collapsed;
            SoftButton7.Visibility = Visibility.Collapsed;
            SoftButton8.Visibility = Visibility.Collapsed;
        }

        private void clearMenuFields()
        {
            MenuTitle.Text = "Menu";
            MenuIcon.Text = "\xe8FD";
            MenuImage.Source = null;
            MenuIcon.Visibility = Visibility.Visible;
            MenuImage.Visibility = Visibility.Collapsed;
        }

        private async void clearMediaTemplate()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    clearMainFields();
                    clearMenuFields();
                    clearMainGraphics();
                    clearSoftButtons();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
        }

        public void initializeButtonSubscribed()
        {
            playPauseButton.Visibility = Visibility.Collapsed;
            previousButton.Visibility = Visibility.Collapsed;
            nextButton.Visibility = Visibility.Collapsed;
            buttonPreset.Visibility = Visibility.Collapsed;

            AppItem appItem = AppInstanceManager.AppInstance.getAppItem(AppInstanceManager.activatedAppId);

            if (appItem.getButtonSubscriptions() != null && appItem.getButtonSubscriptions().Count > 0)
            {
                foreach(OnButtonSubscription onButtonSubscription in appItem.getButtonSubscriptions().ToList())
                {
                    if (onButtonSubscription.getName() == ButtonName.OK || onButtonSubscription.getName() == ButtonName.PLAY_PAUSE)
                    {
                        if (appItem.getApplication().getAppType().Contains(AppHMIType.DEFAULT) && appItem.getApplication().getIsMediaApplication() == false)
                            playPauseButton.Visibility = Visibility.Collapsed;
                        else
                            playPauseButton.Visibility = Visibility.Visible;
                    }
                    else if (onButtonSubscription.getName() == ButtonName.SEEKLEFT)
                    {
                        previousButton.Visibility = Visibility.Visible;
                    }
                    else if (onButtonSubscription.getName() == ButtonName.SEEKRIGHT)
                    {
                        nextButton.Visibility = Visibility.Visible;
                    }
                    else if (onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_0 || onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_1 ||
                        onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_2 || onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_3 ||
                        onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_4 || onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_5 ||
                        onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_6 || onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_7 ||
                        onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_8 || onButtonSubscription.getName() == HmiApiLib.ButtonName.PRESET_9)
                    {
                            buttonPreset.Visibility = Visibility.Visible;
                    }
                }
            }
        }

        private void displaySoftButtons(AppItem appItem)
        {
            CachedShow msg = appItem.getCachedShow();

            if (msg.getSoftButtons() != null && (msg.getSoftButtons().Count > 0))
            {
                for (int i = 0; i < msg.getSoftButtons().Count; i++)
                {                       
                    switch (i)
                    {
                        case 0:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton1, SoftButton1Text, SoftButton1Image, SoftButton1StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 1:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton2, SoftButton2Text, SoftButton2Image, SoftButton2StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 2:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton3, SoftButton3Text, SoftButton3Image, SoftButton3StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 3:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton4, SoftButton4Text, SoftButton4Image, SoftButton4StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 4:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton5, SoftButton5Text, SoftButton5Image, SoftButton5StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 5:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton6, SoftButton6Text, SoftButton6Image, SoftButton6StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 6:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton7, SoftButton7Text, SoftButton7Image, SoftButton7StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        case 7:
                            RpcUtils.handleSoftButton(appItem, msg.getSoftButtons()[i], SoftButton8, SoftButton8Text, SoftButton8Image, SoftButton8StaticImage, i, softButtonsId, softButtonHighlighted);
                            break;
                        default:
                            break;
                    }
                }                
            }
        }

        public void backToAppPage()
        {
            if (appInstanceManager != null && formShowing) //no need to refresh the app list if the app is not currently showing (e.g. navigated to this form)
            {
                appInstanceManager.onRefreshRegisteredApps();
            }
        }

        public async void onRefreshDisplayLayout()
        {
            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
            () =>
            {
                Frame.Navigate(typeof(MediaTemplate), AppInstanceManager.AppInstance.appList[AppUtils.getAppIdInstanceIndex()]);
            });
        }

        public async void onButtonSubscriptionNotificationCallback()
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => initializeButtonSubscribed());
        }

        public void onRefreshAvailable(Type rpcType, int? appID, RefreshDataState dataState)
        {
            if (appID != AppInstanceManager.activatedAppId)
                return;

            if (rpcType == typeof(CachedShow))
                displayCachedShow();
            else if (rpcType == typeof(OnButtonSubscription))
                onButtonSubscriptionNotificationCallback();
            else if (rpcType == typeof(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties))
                onUiSetGlobalPropertiesCallback();
            else if (rpcType == typeof(SetAppIcon))
                onUiSetAppIcon();
            else if (rpcType == typeof(SetDisplayLayout))
                onRefreshDisplayLayout();
            else if (rpcType == typeof(SetMediaClockTimer))
                onUiSetMediaClockTimerRequestCallback();
            else if (rpcType == typeof(OnAppUnregistered))
                backToAppPage();
            else if (rpcType == typeof(OnAppRegistered))
                onUiSetAppIcon();
        }
    }
}