﻿using HmiApiLib;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Controllers.UI.IncomingRequests;
using SharpHmi.src.CachedClasses;
using SharpHmi.src.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace SharpHmi.src.CachedRPC
{
    // Cached show represents data that should be displayed on screen and is accessible by all our layouts.
    // Show info on screen consists of a collection of show data that we have received from core.
    // The last show message received may not have all data needed by our layout, instead we are extending show to store all data related to it.
     
    public class CachedShow : Show
    {
        ICachedClassListener listener = null;

        public CachedShow(ICachedClassListener listener)
        {
            this.listener = listener;
            this.@params = new InternalData();
        }

        public void onUpdateReceived(Show rpc)
        {
            if (rpc == null) return;

            updateCommonFields(rpc);
            updateAppID(rpc.getAppId());
            updateTextFieldList(rpc.getShowStrings());
            updateTextAlignment(rpc.getTextAlignment());
            updateGraphic(rpc.getGraphic());
            updateSecondaryGraphic(rpc.getSecondaryGraphic());
            updateSoftButtons(rpc.getSoftButtons());
            updateCustomPresets(rpc.getCustomPresets());
            if (listener != null)
                this.listener.onCachedShowUpdated(this);
        }

        public void updateCommonFields(Show rpc)
        {
            if (rpc == null) return;

            this.id = rpc.id;
            this.method = rpc.method;
        }

        public void updateAppID(int? appID)
        {
            this.@params.appID = appID;
        }

        public List<TextFieldStruct> updateTextFieldItem(List<TextFieldStruct> masterList, TextFieldStruct item)
        {
            if (masterList != null)
            {
                foreach (TextFieldStruct original in masterList)
                {
                    if (original.fieldName == item.fieldName) //found a match, update it and return
                    {
                        original.fieldText = item.fieldText;
                        return masterList;
                    }
                }
            }
            else
            {
                masterList = new List<TextFieldStruct>();
            }

            masterList.Add(item); //we couldn't find a match, go ahead and add the item to our list
            return masterList;
        }

        public void updateTextFieldList(List<TextFieldStruct> updateList) //when new data is received, overwrite the old data and preserve existing fields that are empty in the new
        {
            if (updateList == null) return;

            List<TextFieldStruct> masterList = this.getShowStrings();

            foreach (TextFieldStruct item in updateList)
            {
                masterList = updateTextFieldItem(masterList, item);
            }

            this.@params.showStrings = masterList;
        }

        public void updateTextAlignment(TextAlignment? alignment) //only update the original text alignment if our new data is not empty
        {
            if (alignment == null) return;

            this.@params.alignment = alignment;
        }

        public void updateGraphic(Image graphic) //only update the original graphic if our new data is not empty
        {
            if (graphic == null) return;

            this.@params.graphic = graphic;               
        }

        public void updateSecondaryGraphic(Image secondaryGraphic) //only update the secondary graphic if our new data is not empty
        {
            if (secondaryGraphic == null) return;

            this.@params.secondaryGraphic = secondaryGraphic;
        }

        public void updateSoftButtons(List<SoftButton> softButtons) //only update the softbuttons on screen if the new data is not empty
        {
            if (softButtons == null) return;

            this.@params.softButtons = softButtons;
        }

        public void updateCustomPresets(List<string> customPresets) //only update the softbuttons on screen if the new data is not empty
        {
            if (customPresets == null) return;

            this.@params.customPresets = customPresets;
        }
    }
}
