﻿using SharpHmi.src.CachedRPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHmi.src.CachedClasses
{
    public interface ICachedClassListener
    {
        void onCachedShowUpdated(CachedShow cachedShow);
    }
}
